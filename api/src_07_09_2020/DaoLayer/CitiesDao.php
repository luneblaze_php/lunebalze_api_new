<?php

namespace App\DaoLayer;

use App\Utils\Codes;
use Cake\Core\Exception\Exception;
use Cake\Datasource\ConnectionManager;
use Cake\Log\Log;

class CitiesDao
{
    /**
     * Get cities by city id (primary key)
     */
    public function getCitiesByCityId($cityId)
    {   
        Log::debug("Started ...getCities Dao");

        try{
            
            $conn = ConnectionManager::get('default');
            $codes = new Codes;

            $sql=sprintf("SELECT `cityid`, `cityname`, `added_on`, `modified_on` FROM `cities` where cityid = '%s'",
            $cityId);

            Log::debug("SQL : ".$sql);

            $stmt = $conn->execute($sql);

            $result = $stmt->fetch("assoc");

            return $result;

        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }

}