<?php

namespace App\DaoLayer;

use App\Utils\Codes;
use Cake\Core\Exception\Exception;
use Cake\Datasource\ConnectionManager;
use Cake\Log\Log;

class FollowingsDao
{


    /**
     * 
     */
    public function saveFollowings($userId, $followingId)
    {
        Log::debug("Started ...saveFollowings Dao : User Id ".$userId." ,Following Id : ".$followingId);

        try{
            
            $conn = ConnectionManager::get('default');

            $sql=sprintf("INSERT INTO followings
            (user_id, following_id)
            VALUES('%s', '%s')", $userId,$followingId);

            Log::debug("SQL : ".$sql);

            $conn->execute($sql);

            Log::debug("Ended ...saveFollowings Dao");
            
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }

    }
}