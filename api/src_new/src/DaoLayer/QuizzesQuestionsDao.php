<?php

namespace App\DaoLayer;

use App\Utils\Codes;
use Cake\Core\Exception\Exception;
use Cake\Datasource\ConnectionManager;
use Cake\Log\Log;

class QuizzesQuestionsDao
{


    
    /**
     * Save Qwiz Question
     */
    public function saveQuizzesQuestions($args, $quiz_id)
    {
        
        try{
            $args = (array) $args;
            $conn = ConnectionManager::get('default');

            $sql=sprintf("INSERT INTO `quizzes_questions`(`quiz_id`, `question`, `picture`, `time_alloted`) VALUES ('%s', '%s', '%s', '%s')", 
            $quiz_id, $args['question'], $args['picture'], $args['time_alloted']);

            Log::debug("SQL : ".$sql);

            $conn->execute($sql);
            
            $sql="SELECT LAST_INSERT_ID()";
            
            $stmt = $conn->execute($sql);
            
            $res = $stmt->fetch();
           
            Log::debug("Ended ...saveQwizQuestion Dao");
            
            return $res[0];
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }

    public function deleteQuestionsByQuizId($quiz_id)
    {
        Log::debug("Started ...deleteQuestionsByQuizId Dao");

        try{
            
            $conn = ConnectionManager::get('default');
            //deleting questions
            $sql=sprintf("DELETE FROM `quizzes_questions` WHERE quiz_id = '%s'", 
            $quiz_id);
            Log::debug("SQL : ".$sql);
            $conn->execute($sql);

            //deleting questions options
            $sql=sprintf("DELETE FROM `quizzes_questions_options` WHERE quiz_id = '%s'", 
            $quiz_id);
            Log::debug("SQL : ".$sql);
            $conn->execute($sql);

            Log::debug("Ended ...deleteQuestionsByQuizId Dao");
            
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }


}