<?php

namespace App\DaoLayer;

use App\Utils\Codes;
use Cake\Core\Exception\Exception;
use Cake\Datasource\ConnectionManager;
use Cake\Log\Log;

class PostsCommentsDao
{

	/**
	 * Get Posts Comments by Comment Id
	 */
	public function getPostsCommentsByCommentId($commentId)
	{
		Log::debug("Started ...getPostsCommentsByCommentId Dao : Comment Id : ".$commentId);

		try{
			
			$conn = ConnectionManager::get('default');

			$sql=sprintf("SELECT * FROM posts_comments WHERE comment_id = %s", $commentId);
		
			Log::debug("SQL : ".$sql);

			$stmt = $conn->execute($sql);

			$result = $stmt->fetch("assoc");

			Log::debug("Ended ...getPostsCommentsByCommentId Dao");

			return $result;

		}catch(\Exception $e){
			Log::debug($e);
			throw new Exception($e);
		}
	}   

}