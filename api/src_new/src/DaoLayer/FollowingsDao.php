<?php

namespace App\DaoLayer;

use App\Utils\Codes;
use Cake\Core\Exception\Exception;
use Cake\Datasource\ConnectionManager;
use Cake\Log\Log;

class FollowingsDao
{


    /**
     * 
     */
    public function saveFollowings($userId, $followingId)
    {
        Log::debug("Started ...saveFollowings Dao : User Id ".$userId." ,Following Id : ".$followingId);

        try{
            
            $conn = ConnectionManager::get('default');

            $sql=sprintf("INSERT INTO followings
            (user_id, following_id)
            VALUES('%s', '%s')", $userId,$followingId);

            Log::debug("SQL : ".$sql);

            $conn->execute($sql);

            Log::debug("Ended ...saveFollowings Dao");
            
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }

    }



    	/**
	 * Delete followings using user_id and following_id
	 */
	public function deleteFollowingsByUserIdAndFollowingId($userId, $followingId)
	{
		Log::debug("Started ...deleteFollowingsByUserIdAndFollowingId Dao : User Id : ".$userId.", Following Id : ".$followingId);

		try{
			
			$conn = ConnectionManager::get('default');

			$sql=sprintf("DELETE FROM followings WHERE user_id = %s AND following_id = %s", $userId, $followingId);

			Log::debug("SQL : ".$sql);

			$conn->execute($sql);

			Log::debug("Ended ...deleteFollowingsByUserIdAndFollowingId Dao");

		}catch(\Exception $e){
			Log::debug($e);
			throw new Exception($e);
		}
	}
}