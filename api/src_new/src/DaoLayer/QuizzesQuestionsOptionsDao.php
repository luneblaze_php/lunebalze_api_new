<?php

namespace App\DaoLayer;

use App\Utils\Codes;
use Cake\Core\Exception\Exception;
use Cake\Datasource\ConnectionManager;
use Cake\Log\Log;

class QuizzesQuestionsOptionsDao
{


    
    /**
     * Save Qwiz
     */
    public function saveQuizzesQuestionsOptions($args, $quiz_id, $question_id)
    {
        
        try{

            $args = (array) $args;
            $conn = ConnectionManager::get('default');
            
            $sql=sprintf("INSERT INTO `quizzes_questions_options`(`quiz_id`, `question_id`, `option_text`, `is_correct`) VALUES ('%s', '%s', '%s', '%s')", 
            $quiz_id, $question_id, $args['option_text'], $args['is_correct']);

            Log::debug("SQL : ".$sql);

            $conn->execute($sql);
            
            $sql="SELECT LAST_INSERT_ID()";
            
            $stmt = $conn->execute($sql);
            
            $res = $stmt->fetch();
           
            Log::debug("Ended ...saveQwizQuestionOptions Dao");
            
            return $res[0];
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }



}