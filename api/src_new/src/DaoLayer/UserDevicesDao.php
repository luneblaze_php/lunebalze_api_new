<?php

namespace App\DaoLayer;

use App\Utils\Codes;
use Cake\Core\Exception\Exception;
use Cake\Datasource\ConnectionManager;
use Cake\Log\Log;

class UserDevicesDao
{


    /**
     * 
     */
    public function getUserDevicesByUserId($userId)
    {
        Log::debug("Started ...getUserDevicesByUserId Dao : User Id : ".$userId);

        try{
            
            $conn = ConnectionManager::get('default');

            $sql=sprintf("SELECT * FROM user_devices WHERE user_id = %d", $userId);

            Log::debug("SQL : ".$sql);

            $stmt = $conn->execute($sql);

            $results = array();

            while($result = $stmt->fetch("assoc")){
                array_push($results,$result);
            }

            Log::debug("Ended ...getUserDevicesByUserId Dao");

            return $results;
            
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }

    }
}