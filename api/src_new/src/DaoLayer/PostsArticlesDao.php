<?php

namespace App\DaoLayer;

use App\Utils\Codes;
use Cake\Core\Exception\Exception;
use Cake\Datasource\ConnectionManager;
use Cake\Log\Log;

class PostsArticlesDao
{

	/**
	 * Save Posts Articles
	 */
	public function savePostsArticles($postId, $articlesId, $coverPhoto)
	{
		Log::debug("Started ...savePostsArticles Dao : Post Id : ".$postId.", Article Id : ".$articlesId.", Source : ".$coverPhoto);

		try{
			
			$conn = ConnectionManager::get('default');

			$sql=sprintf("INSERT INTO posts_articles (post_id, article_id, source) VALUES (%s, %s, %s)", $postId, $articlesId, $coverPhoto);

			Log::debug("SQL : ".$sql);

			$conn->execute($sql);

			Log::debug("Ended ...savePostsArticles Dao");

		}catch(\Exception $e){
			Log::debug($e);
			throw new Exception($e);
		}
	}

   
}
