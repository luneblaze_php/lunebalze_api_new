<?php

namespace App\ServiceLayer;

use App\Utils\Codes;
use App\Utils\DataValidation;
use Cake\Core\Exception\Exception;
use App\DaoLayer\InterestMstDao;
use App\DaoLayer\UserInterestsDao;
use App\Utils\ImageUtils;
use Cake\Log\Log;

class ConstantService
{

    /**
     * 
     */
    public function get_user_interest($inputJson)
    {
           Log::debug("Started ... get_user_interest Service : ".json_encode($inputJson));
            try{
                $userInterestDao = new UserInterestsDao;
                $codes = new Codes;
                $dataValidation = new DataValidation;
                $imageUtils = new ImageUtils;

                //Validation to be done here ---
                if(!isset($inputJson['user_id']))
                {
                    $response = [
                        'status'=>$codes->RC_ERROR,
                        'message'=>$codes->RM_ALL_FIELDS_TO_BE_PRESENT,
                        'data'=>null
                    ];
    
                    return $response;                
                }
    
                if($dataValidation->isEmpty($inputJson['user_id']))
                {
                    
                    $response = [
                        'status'=>$codes->RC_ERROR,
                        'message'=>"Please enter user id",
                        'data'=>null
                    ];
    
                    return $response;
                }
                //Validation to be done here ---

                $userInterests = $userInterestDao->getUserInterestsByUserId($inputJson['user_id']);


                $userInterestResponses = array();


                foreach($userInterests as $ui)
                {
                    //Fetch the total attendees
                    $totalAttendees = $userInterestDao->getTotalAttendees($ui['interest']);

                    //Get Picture 
                    $picture = $imageUtils->getPicture($ui['image'],'');

                    
                    $isFollow = count($userInterestDao->getUserInterestsByInterestUserId($ui['interest'],$inputJson['user_id'])) > 0 ? '1' : '0';

                    
                    $data = [
                        'interest_id'=>$ui['interest'],
                        'text'=>$ui['text'],
                        'image'=>$picture,
                        'no_of_followers'=>$totalAttendees,
                        'i_follow_related'=>$isFollow
                    ];

                    array_push($userInterestResponses,$data);

                }

                $response = [
                    'status'=>$codes->RC_SUCCESS,
                    'message'=>"user interests",
                    'data'=>$userInterestResponses
                ];
    
                Log::debug("Ended ... get_user_interest Service : ");
    
                return $response;            
            }catch(\Exception $e){
                Log::debug($e);
                throw new Exception($e);
            }
    }





    public function get_interest($inputJson)
    {
           Log::debug("Started ... get_interest Service : ".json_encode($inputJson));
            try{
                $interestMstDao = new InterestMstDao;
                $codes = new Codes;
                $dataValidation = new DataValidation;
                
                //Fetch the interests
                $interests = $interestMstDao->getInterestMasters(null,$codes->MAX_LIMIT);
                
                $response = [
                    'status'=>$codes->RC_SUCCESS,
                    'message'=>"Interest List",
                    'data'=>$interests
                ];
    
                Log::debug("Ended ... get_interest Service : ");
    
                return $response;            
            }catch(\Exception $e){
                Log::debug($e);
                throw new Exception($e);
            }
    }

    /**
     * 
     */
    public function search_interest($inputJson)
    {
        Log::debug("Started ... search_interest Service : ".json_encode($inputJson));
        try{
            $interestMstDao = new InterestMstDao;
            $codes = new Codes;
            $dataValidation = new DataValidation;

            //Validation to be done here

            if(!isset($inputJson['text']) || !isset($inputJson['offset']))
            {
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>$codes->RM_ALL_FIELDS_TO_BE_PRESENT,
                    'data'=>null
                ];

                return $response;                
            }

            if($dataValidation->isEmpty($inputJson['text']))
            {
                
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>"Please enter text",
                    'data'=>null
                ];

                return $response;
            }
            if($dataValidation->isEmpty($inputJson['offset']))
            {
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>"Please enter offset",
                    'data'=>null
                ];

                return $response;
            }
            //Validation done -- 

            $offset = $inputJson['offset'] * $codes->MAX_RESULTS;
            
            //Fetch the interests
            $interests = $interestMstDao->getInterestMasters($inputJson['text'],$offset);

            $data = array();

            foreach($interests as $int)
            {
                array_push($data, [
                    'interest_id'=>$int['interest_id'],
                    'parent_id'=>$int['parent_id'],
                    'text'=>$int['text']
                ]);
            }
            
            $response = [
                'status'=>$codes->RC_SUCCESS,
                'message'=>"",
                'data'=>$data
            ];

            Log::debug("Ended ... search_interest Service : ");

            return $response;            
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }
}