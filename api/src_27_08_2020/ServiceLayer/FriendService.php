<?php

namespace App\ServiceLayer;

use App\Utils\Codes;
use App\Utils\DataValidation;
use Cake\Core\Exception\Exception;
use App\DaoLayer\UsersDao;
use App\Utils\ImageUtils;
use Cake\Log\Log;

class FriendService
{

    /**
     * 
     */
    public function friend_requests_sent($inputJson)
    {
        Log::debug("Started ... friend_requests_sent Service : ".json_encode($inputJson));
        try{
            $usersDao = new UsersDao;
            $codes = new Codes;
            $dataValidation = new DataValidation;
            $imageUtils = new ImageUtils;

            //Validation to be done here

            if(!isset($inputJson['user_id']))
            {
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>$codes->RM_ALL_FIELDS_TO_BE_PRESENT,
                    'data'=>null
                ];

                return $response;                
            }

            if($dataValidation->isEmpty($inputJson['user_id']))
            {
                
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>"Please enter user id",
                    'data'=>null
                ];

                return $response;
            }
            
            //Validation done -- 

            $offset = $codes->STARTING_OFFSET * $codes->MAX_RESULTS;

            //Check if the user is a valid one
            $user = $usersDao->getUserByUserId($inputJson['user_id']);

            
            if($user == null)
            {
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>"Invalid User",
                    'data'=>null
                ];

                return $response;
            }

            Log::debug("User Fetched : ".json_encode($user));

            //Fetch the friend requests sent
            $friendRequestsSent = $usersDao->getFriendRequestsSentByUserId($inputJson['user_id'],$offset,$codes->MAX_RESULTS);
            $data = array();

            foreach($friendRequestsSent as $frnd)
            {
                array_push($data, [
                    'user_picture'=>$imageUtils->getPicture($frnd['user_picture'], $frnd['user_gender']),
                    'mutual_friends_count'=>$this->getMutualFriendsCount($inputJson['user_id'],$frnd['user_id'])
                ]);
            }
            
            $response = [
                'status'=>$codes->RC_SUCCESS,
                'message'=>"",
                'data'=>$data
            ];

            Log::debug("Ended ... friend_requests_sent Service : ");

            return $response;            
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }

    }

    private function getMutualFriendsCount($userId1, $userId2)
    {
        $usersDao = new UsersDao;
        $friendUserIdsForUser1 = $usersDao->getFriendIdsByUserId($userId1);

        Log::debug(json_encode($friendUserIdsForUser1));


        $friendUserIdsForUser2 = $usersDao->getFriendIdsByUserId($userId2);

        Log::debug(json_encode($friendUserIdsForUser2));

        if(count($friendUserIdsForUser1) == 0 || count($friendUserIdsForUser2) == 0)
            return 0;

        $commonFriendUserIds = array_intersect($friendUserIdsForUser1,$friendUserIdsForUser2);
        
        return count($commonFriendUserIds);
    }


}
