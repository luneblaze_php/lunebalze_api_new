<?php

namespace App\Utils;

class DataValidation
{

    /**
     *
     */
    public function isValidInteger($var)
    {
        if (is_numeric($var))
            return true;
        return false;
    }

    /**
     * 
     */
    function isEmpty($value)
    {

        if (!isset($value))
            return true;

        if (strlen(trim(preg_replace('/\xc2\xa0/', ' ', $value))) == 0) {
            return true;
        } else {
            return false;
        }
    }



    /**
     * 
     */
    function isValidPrivacy($privacy, $author_id, $userGroup, $userId, $friendIds)
    {
        if ($privacy == 'public') {
            return true;
        }
        /* check if the viewer is the system admin */
        if ($userGroup < 3) {
            return true;
        }
        /* check if the viewer is the target */
        if ($author_id == $userId) {
            return true;
        }
        /* check if the viewer and the target are friends */
        if ($privacy == 'friends' && in_array($author_id, $friendIds)) {
            return true;
        }

        return false;
    }



    /**
     * valid_email
     *
     * @param string $email
     * @return boolean
     */

    public function isValidEmail($email)
    {
        if (filter_var($email, FILTER_VALIDATE_EMAIL) !== false) {
            return true;
        } else {
            return false;
        }
    }

    public function validatePostType($postType)
    {
        if (!isset($postType))
            return "Post type is not present";
        if (empty($postType))
            return "Post type is not present";
        if (!in_array($postType, ['home', 'posts', 'ads']))
            return "Post type must be either home,posts or ads";
        return 'Success';
    }


    /**
     * 
     */
    public function validatePassword($password)
    {
        if (!isset($password))
            return "Your password must be at least 6 characters long. Please try another.";
        if ($password == '')
            return "Your password must be at least 6 characters long. Please try another.";
        if (strlen($password) < 6)
            return "Your password must be at least 6 characters long. Please try another.";

        return "Success";
    }

    /**
     * 
     */
    public function validateBirthDate($day, $month, $year)
    {
        if (!isset($day) || !isset($month) || !isset($year))
            return 'null';
        if ($month == "none" && $day == "none" && $year == "none")
            return 'null';

        if (!in_array($month, range(1, 12)))
            return "Please select a valid birth month";

        if (!in_array($day, range(1, 31)))
            return "Please select a valid birth day";

        if (!in_array($year, range(1905, date('Y'))))
            return "Please select a valid birth year";

        return "Success";
    }


    /**
     * 
     */
    public function validateName($name, $isFirstName)
    {
        $flag = $isFirstName == 1 ? 'First Name' : 'Last Name';

        if (!isset($name))
            return "Your " . $flag . " must be at least 3 characters long. Please try another";

        if ($name == '')
            return "Your " . $flag . " must be at least 3 characters long. Please try another";

        if (strlen($name) < 3)
            return "Your " . $flag . " must be at least 3 characters long. Please try another";

        if (!preg_match("/^[\\p{L} ]+$/ui", $name))
            return "Your " . $flag . " contains invalid characters";

        return "Success";
    }

    /**
     * 
     */
    public function validateGender($gender)
    {
        if (!isset($gender))
            return "Please select either Male , Female or Other";
        if ($gender == '')
            return "Please select either Male , Female or Other";
        if (!in_array($gender, array('male', 'female', 'other')))
            return "Please select either Male , Female or Other";

        return "Success";
    }

    /**
     * valid_url
     *
     * @param string $url
     * @return boolean
     */

    public function valid_url($url)
    {
        if (filter_var($url, FILTER_VALIDATE_URL, FILTER_FLAG_HOST_REQUIRED) !== false) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * valid_username
     *
     * @param string $username
     * @return boolean
     */

    public function valid_username($username)
    {
        if (strlen($username) >= 3 && preg_match('/^[a-zA-Z0-9]+([_|.]?[a-zA-Z0-9])*$/', $username)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * valid_name
     *
     * @param string $name
     * @return boolean
     */

    public function valid_name($name)
    {
        if (preg_match("/^[\\p{L} ]+$/ui", $name)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * valid_location
     *
     * @param string $location
     * @return boolean
     */

    public function valid_location($location)
    {
        if (preg_match("/^[\\p{L} ,()0-9]+$/ui", $location)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * valid_extension
     *
     * @param string $extension
     * @param string $allowed_extensions
     * @return boolean
     */

    public function valid_extension($extension, $allowed_extensions)
    {
        $extensions = explode(',', $allowed_extensions);
        foreach ($extensions as $key => $value) {
            $extensions[$key] = strtolower(trim($value));
        }
        if (is_array($extensions) && in_array($extension, $extensions)) {
            return true;
        }
        return false;
    }

    /* ------------------------------- */
    /* Date */
    /* ------------------------------- */

    /**
     * set_datetime
     *
     * @param string $date
     * @return string
     */

    public function set_datetime($date)
    {
        return date('Y-m-d H:i:s', strtotime($date));
    }

    /**
     * get_datetime
     *
     * @param string $date
     * @return string
     */

    public function get_datetime($date)
    {
        return date('m/d/Y g:i A', strtotime($date));
    }

    /**
     * get_hash_key
     *
     * @param integer $length
     * @return string
     */

    public function get_hash_key($length = 8)
    {
        $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        $count = mb_strlen($chars);
        for ($i = 0, $result = ''; $i < $length; $i++) {
            $index = rand(0, $count - 1);
            $result .= mb_substr($chars, $index, 1);
        }
        return $result;
    }

    /**
     * get_hash_key number
     *
     * @param integer $length
     * @return string
     */

    public function get_hash_keyn($length = 8)
    {
        $chars = '0123456789';
        $count = mb_strlen($chars);
        for ($i = 0, $result = ''; $i < $length; $i++) {
            $index = rand(0, $count - 1);
            $result .= mb_substr($chars, $index, 1);
        }
        return $result;
    }

    /**
     * get_hash_token
     *
     * @return string
     */

    public function get_hash_token()
    {
        return md5(time() * rand(1, 9999));
    }
}
