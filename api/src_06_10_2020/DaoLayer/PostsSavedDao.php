<?php

namespace App\DaoLayer;

use App\Utils\Codes;
use Cake\Core\Exception\Exception;
use Cake\Datasource\ConnectionManager;
use Cake\Log\Log;

class PostsSavedDao
{

    /**
     * 
     */
    public function getPostsSavedByUserIdPostId($userId,$postId)
    {
        Log::debug("Started ...getPostsSavedByUserIdPostId Dao : Post Id : ".$postId.", User Id : ".$userId);

        try{

            $conn = ConnectionManager::get('default');

            $sql = sprintf("SELECT * FROM posts_saved WHERE user_id = '%s' AND post_id = '%s'",$userId,$postId);

            Log::debug("SQL : ".$sql);

            $stmt = $conn->execute($sql);

            $result = $stmt->fetch("assoc");

            Log::debug("Ended ...getPostsSavedByUserIdPostId Dao");

            return $result;
            
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }
    
}