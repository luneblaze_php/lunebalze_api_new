<?php

namespace App\DaoLayer;

use App\Utils\Codes;
use Cake\Core\Exception\Exception;
use Cake\Datasource\ConnectionManager;
use Cake\Log\Log;

class PostRelevancyDao
{

	/**
	 * Get Post Relevancy by id
	 */
	public function getPostRelevancyById($id)
	{
		Log::debug("Started ...getPostRelevancyById Dao : Id : ".$id);

		try{
			
			$conn = ConnectionManager::get('default');

			$sql=sprintf("SELECT * FROM `post_relevancy` where id = '%s'", $id);
			
			Log::debug("SQL : ".$sql);

			$stmt = $conn->execute($sql);

			$result = $stmt->fetch("assoc");

			Log::debug("Ended ...getPostRelevancyById Dao");

			return $result;

		}catch(\Exception $e){
			Log::debug($e);
			throw new Exception($e);
		}
	}        

}