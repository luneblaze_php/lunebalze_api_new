<?php

namespace App\DaoLayer;

use App\Utils\Codes;
use Cake\Core\Exception\Exception;
use Cake\Datasource\ConnectionManager;
use Cake\Log\Log;

class OrganizationTypeDao
{


    /**
     * 
     */
    public function getOrganizationTypeByOrganizationTypeId($organizationTypeId)
    {
        Log::debug("Started ...getOrganizationTypeByOrganizationTypeId Dao : Organization Type Id : ".$organizationTypeId);

        try{
            
            $conn = ConnectionManager::get('default');

            $sql=sprintf("select * from organizationtype where id='%s'",$organizationTypeId);          

            Log::debug("SQL : ".$sql);

            $stmt = $conn->execute($sql);

            $result = $stmt->fetch("assoc");

            Log::debug("Ended ...getOrganizationTypeByOrganizationTypeId Dao");

            return $result;
            
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }

    }
}