<?php

namespace App\DaoLayer;

use App\Utils\Codes;
use Cake\Core\Exception\Exception;
use Cake\Datasource\ConnectionManager;
use Cake\Log\Log;

class UsersVenueDao
{

	/**
	 * Save Users Venue
	 */
	public function saveUsersVenue($userId, $venueId)
	{
		Log::debug("Started ...saveUsersVenue Dao : User Id : ".$userId.", Blocked Id : ".$venueId);

		try{
			
			$conn = ConnectionManager::get('default');

			$sql=sprintf("INSERT INTO users_venue (user_id, venue_id) VALUES (%s, %s)", $userId, $venueId);

			Log::debug("SQL : ".$sql);

			$conn->execute($sql);

			Log::debug("Ended ...saveUsersVenue Dao");

		}catch(\Exception $e){
			Log::debug($e);
			throw new Exception($e);
		}
	}

	/**
	 * Delete Users Venue using user_id and venue_id
	 */
	public function deleteUsersVenueByUserIdAndVenueId($userId, $venueId)
	{
		Log::debug("Started ...deleteUsersVenueByUserIdAndVenueId Dao : User Id : ".$userId.", Blocked Id : ".$venueId);

		try{
			
			$conn = ConnectionManager::get('default');

			$sql=sprintf("DELETE FROM users_venue WHERE user_id = %s AND venue_id = %s", $userId, $venueId);

			Log::debug("SQL : ".$sql);

			$conn->execute($sql);

			Log::debug("Ended ...deleteUsersVenueByUserIdAndVenueId Dao");

		}catch(\Exception $e){
			Log::debug($e);
			throw new Exception($e);
		}
	}
   
}