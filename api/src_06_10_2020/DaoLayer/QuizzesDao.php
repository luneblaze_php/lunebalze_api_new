<?php

namespace App\DaoLayer;

use App\Utils\Codes;
use Cake\Core\Exception\Exception;
use Cake\Datasource\ConnectionManager;
use Cake\Log\Log;

class QuizzesDao
{


    
    /**
     * Save Qwiz
     */
    public function saveQuizzes($args)
    {

        try{

            $conn = ConnectionManager::get('default');

            $sql=sprintf("INSERT INTO `quizzes`(`title`, `description`, `picture`, `user_type`, `creator_id`, `start_time`, `live_duration`) VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s')", 
            $args['title'], $args['description'], $args['picture'], $args['user_type'], $args['creator_id'], $args['start_time'], $args['live_duration']);

            Log::debug("SQL : ".$sql);

            $conn->execute($sql);
            
            $sql="SELECT LAST_INSERT_ID()";
            
            $stmt = $conn->execute($sql);
            
            $res = $stmt->fetch();
           
            Log::debug("Ended ...saveQwiz Dao");
            
            return $res[0];
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }    
    /**
     * Update Qwiz
     */
    public function updateQuizzes($args)
    {
        Log::debug("Started ...updateQuizzes Dao");

        try{

            $conn = ConnectionManager::get('default');

            $sql=sprintf("UPDATE quizzes SET title = '%s', description = '%s', picture = '%s', user_type = '%s', start_time = '%s', live_duration = '%s'
                WHERE id = '%s' AND creator_id = '%s'", $args['title'], $args['description'], $args['picture'], $args['user_type'], $args['start_time'], $args['live_duration'], $args['quiz_id'], $args['creator_id']);

            Log::debug("SQL : ".$sql);

            $conn->execute($sql);
           
            Log::debug("Ended ...updateQuizzes Dao");
            
            return 1;
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }
    /**
     * Delete Qwiz
     */
    public function deleteQuizzes($quiz_id, $creator_id)
    {
        Log::debug("Started ...deleteQuizzes Dao");

        try{

            $conn = ConnectionManager::get('default');

            //deleting questions
            $sql=sprintf("DELETE FROM `quizzes` WHERE id = '%s' AND creator_id = '%s'", 
            $quiz_id, $creator_id);
            Log::debug("SQL : ".$sql);
            $conn->execute($sql);
           
            Log::debug("Ended ...deleteQuizzes Dao");
            
            return 1;
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }
    /**
     * Get Qwizzes
     */
    public function getQuizzesList()
    {
        Log::debug("Started ...getQuizzesList Dao");

        try{

            $conn = ConnectionManager::get('default');

            //quiz
            $sql=sprintf('SELECT * FROM quizzes where 1');
            Log::debug("SQL : ".$sql);
            $stmt = $conn->execute($sql);
            
            $results = array();

            while($result = $stmt->fetch("assoc")) {
                array_push($results,$result);
            }
           
            Log::debug("Ended ...getQuizzesList Dao");

            return $results;
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }
    
    /**
     * getQuizCountByUserId
     */
    public function getQuizCountByUserId($userId)
    {
        Log::debug("Started ...getQuizCountByUserId Dao : User Id : ".$userId);

        try{
            
            $conn = ConnectionManager::get('default');

            $sql=sprintf("SELECT count(*) as total FROM `quizzes` WHERE `creator_id` =  '%s'", $userId);

            Log::debug("SQL : ".$sql);

            $stmt = $conn->execute($sql);

            $result = $stmt->fetch("assoc");

            Log::debug("Ended ...getUserByUserIdAndLoginOtp Dao");

            return $result;
            
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }

    }



}