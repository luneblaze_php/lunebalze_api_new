<?php

namespace App\DaoLayer;

use App\Utils\Codes;
use Cake\Core\Exception\Exception;
use Cake\Datasource\ConnectionManager;
use Cake\Log\Log;

class MarketCategoriesDao
{

	/**
	 * Get Market Categories
	 */
	public function getMarketCategories($sessionId)
	{
		Log::debug("Started ...getMarketCategories Dao");

		try{
			
			$conn = ConnectionManager::get('default');

			$sql=sprintf("SELECT * FROM market_categories");
					
			Log::debug("SQL : ".$sql);

			$stmt = $conn->execute($sql);

			$result = $stmt->fetch("assoc");

			Log::debug("Ended ...getMarketCategories Dao");

			return $result;

		}catch(\Exception $e){
			Log::debug($e);
			throw new Exception($e);
		}
	}   

}