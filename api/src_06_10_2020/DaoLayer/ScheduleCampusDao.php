<?php

namespace App\DaoLayer;

use App\Utils\Codes;
use Cake\Core\Exception\Exception;
use Cake\Datasource\ConnectionManager;
use Cake\Log\Log;

class ScheduleCampusDao
{


    /**
     * 
     */
    public function getScheduleCampusByOrganizationId($organizationId)
    {
        Log::debug("Started ...getScheduleCampusByOrganizationId Dao : Organization Id : ".$organizationId);

        try{
            
            $conn = ConnectionManager::get('default');

            $sql=sprintf("SELECT * FROM schedule_campus WHERE organization='%s' order by added_on desc",$organizationId);          

            Log::debug("SQL : ".$sql);

            $stmt = $conn->execute($sql);

            $results = array();

            while($result = $stmt->fetch("assoc"))
                array_push($results,$result);

            Log::debug("Ended ...getScheduleCampusByOrganizationId Dao");

            return $results;
            
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }

    }
}