<?php

namespace App\DaoLayer;

use App\Utils\Codes;
use Cake\Core\Exception\Exception;
use Cake\Datasource\ConnectionManager;
use Cake\Log\Log;

class VenueDao
{

	/**
	 * Get Venue by Venue Id
	 */
	public function getVenueByVenueId($venueId)
	{
		Log::debug("Started ...getVenueByVenueId Dao : Venue Id : ".$venueId);

		try{
			
			$conn = ConnectionManager::get('default');

			$sql=sprintf("SELECT * FROM `venue` WHERE `venue_id` = %s", $venueId);
		
			Log::debug("SQL : ".$sql);

			$stmt = $conn->execute($sql);

			$result = $stmt->fetch("assoc");

			Log::debug("Ended ...getVenueByVenueId Dao");

			return $result;

		}catch(\Exception $e){
			Log::debug($e);
			throw new Exception($e);
		}
	}   

}