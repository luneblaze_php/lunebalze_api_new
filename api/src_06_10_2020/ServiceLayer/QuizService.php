<?php

namespace App\ServiceLayer;

use App\Utils\Codes;
use App\Utils\DataValidation;
use Cake\Core\Exception\Exception;
use App\DaoLayer\UsersDao;
use App\DaoLayer\UserSessionDao;
use App\Utils\DateUtils;
use App\Utils\ImageUtils;
use Cake\Log\Log;
use App\DaoLayer\QuizzesDao;
use App\DaoLayer\QuizzesQuestionsDao;
use App\DaoLayer\QuizzesQuestionsOptionsDao;
use App\DaoLayer\PostsDao;

class QuizService
{

    public function createQwiz($inputJson)
    {
        Log::debug("Started ... createQwiz Service : " . json_encode($inputJson));
        try {
            $codes = new Codes;
            $dataValidation = new DataValidation;
            $userSessionDao = new UserSessionDao;
            $usersDao = new UsersDao;
            $imageUtils = new ImageUtils;
            $dateUtils = new DateUtils;
            $quizzesDao = new QuizzesDao;
            $quizzesQuestionsDao = new QuizzesQuestionsDao;
            $quizzesQuestionsOptionsDao = new QuizzesQuestionsOptionsDao;
            $postsDao = new PostsDao;

            //Validations to be done
            //Title validation
            if (!isset($inputJson['title']) || $dataValidation->isEmpty($inputJson['title'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your title",
                    'data' => null
                ];
                return $response;
            }
            //Description validation
            if (!isset($inputJson['description']) || $dataValidation->isEmpty($inputJson['description'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your description",
                    'data' => null
                ];
                return $response;
            }
            //Picture validation
            if (!isset($inputJson['picture']) || $dataValidation->isEmpty($inputJson['picture'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your picture",
                    'data' => null
                ];
                return $response;
            }
            //User type validation
            if (!isset($inputJson['user_type']) || $dataValidation->isEmpty($inputJson['user_type'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your user type",
                    'data' => null
                ];
                return $response;
            }
            //Qwiz start time validation
            if (!isset($inputJson['start_time']) || $dataValidation->isEmpty($inputJson['start_time'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your user type",
                    'data' => null
                ];
                return $response;
            }
            //Qwiz live duration validation
            if (!isset($inputJson['live_duration']) || $dataValidation->isEmpty($inputJson['live_duration'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter live duration",
                    'data' => null
                ];
                return $response;
            }
            //creator validation
            if (!isset($inputJson['creator_id']) || $dataValidation->isEmpty($inputJson['creator_id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter creator",
                    'data' => null
                ];
                return $response;
            }
            //Questions validation
            if (!isset($inputJson['questionlist_item']) || $dataValidation->isEmpty($inputJson['questionlist_item'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter questions",
                    'data' => null
                ];
                return $response;
            }

            // decoding questions data
            $questionData = json_decode($inputJson['questionlist_item']);
            if (empty($questionData)) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter questions",
                    'data' => null
                ];
                return $response;
            }

            Log::debug("Saving the qwiz : ");
            $quizId = $quizzesDao->saveQuizzes($inputJson);
            Log::debug("Saved the qwiz ");
            if($quizId){
                
                foreach ($questionData as $key => $question) {
                    $quizQuestionsId = $quizzesQuestionsDao->saveQuizzesQuestions($question, $quizId);

                    if($quizQuestionsId){
                        foreach ($question->options as $key => $optionRow) {
                            $quizQuestionsId = $quizzesQuestionsOptionsDao->saveQuizzesQuestionsOptions($optionRow, $quizId, $quizQuestionsId);
                        }
                    }
                }
                $postsDao->saveQuizPosts($quizId, $inputJson['creator_id'], $inputJson['title'], $dateUtils->getCurrentDate());
                // checking total quiz of user
                $countRes = $quizzesDao->getQuizCountByUserId($inputJson['creator_id']);
                //updating total quiz of user
                $usersDao->updateUserQuizCountByUserId($countRes['total'], $inputJson['creator_id']);
            }


            Log::debug('qwiz : ' . json_encode($quizId));

            //Send the success response now
            $response = [
                'status' => $codes->RC_SUCCESS,
                'message' => $codes->RM_SUCCESS,
                'data' => ['quizId'=>$quizId]
            ];
            return $response;

            Log::debug("Ended ... qwiz Service : ");
        } catch (\Exception $e) {
            Log::debug($e);
            throw new Exception($e);
        }
    }

    public function updateQwiz($inputJson)
    {
        Log::debug("Started ... updateQwiz Service : " . json_encode($inputJson));
        try {
            $codes = new Codes;
            $dataValidation = new DataValidation;
            $userSessionDao = new UserSessionDao;
            $usersDao = new UsersDao;
            $imageUtils = new ImageUtils;
            $dateUtils = new DateUtils;
            $quizzesDao = new QuizzesDao;
            $quizzesQuestionsDao = new QuizzesQuestionsDao;
            $quizzesQuestionsOptionsDao = new QuizzesQuestionsOptionsDao;
            $postsDao = new PostsDao;

            //Validations to be done
            //Id validation
            if (!isset($inputJson['quiz_id']) || $dataValidation->isEmpty($inputJson['quiz_id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your quiz ID",
                    'data' => null
                ];
                return $response;
            }
            //Title validation
            if (!isset($inputJson['title']) || $dataValidation->isEmpty($inputJson['title'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your title",
                    'data' => null
                ];
                return $response;
            }
            //Description validation
            if (!isset($inputJson['description']) || $dataValidation->isEmpty($inputJson['description'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your description",
                    'data' => null
                ];
                return $response;
            }
            //Picture validation
            if (!isset($inputJson['picture']) || $dataValidation->isEmpty($inputJson['picture'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your picture",
                    'data' => null
                ];
                return $response;
            }
            //User type validation
            if (!isset($inputJson['user_type']) || $dataValidation->isEmpty($inputJson['user_type'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your user type",
                    'data' => null
                ];
                return $response;
            }
            //Qwiz start time validation
            if (!isset($inputJson['start_time']) || $dataValidation->isEmpty($inputJson['start_time'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your user type",
                    'data' => null
                ];
                return $response;
            }
            //Qwiz live duration validation
            if (!isset($inputJson['live_duration']) || $dataValidation->isEmpty($inputJson['live_duration'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter live duration",
                    'data' => null
                ];
                return $response;
            }
            //creator validation
            if (!isset($inputJson['creator_id']) || $dataValidation->isEmpty($inputJson['creator_id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter creator",
                    'data' => null
                ];
                return $response;
            }
            //Questions validation
            if (!isset($inputJson['questionlist_item']) || $dataValidation->isEmpty($inputJson['questionlist_item'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter questions",
                    'data' => null
                ];
                return $response;
            }

            // decoding questions data
            $questionData = json_decode($inputJson['questionlist_item']);
            if (empty($questionData)) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter questions",
                    'data' => null
                ];
                return $response;
            }

            Log::debug("Saving the qwiz : ");
            $qryStatus = $quizzesDao->updateQuizzes($inputJson);
            Log::debug("Saved the qwiz ");
            if($qryStatus){
                $quizzesQuestionsDao->deleteQuestionsByQuizId($inputJson['quiz_id']);
                foreach ($questionData as $key => $question) {
                    //inserting questions
                    $quizQuestionsId = $quizzesQuestionsDao->saveQuizzesQuestions($question, $inputJson['quiz_id']);

                    if($quizQuestionsId){
                        foreach ($question->options as $key => $optionRow) {
                            //inserting questions options
                            $quizQuestionsId = $quizzesQuestionsOptionsDao->saveQuizzesQuestionsOptions($optionRow, $inputJson['quiz_id'], $quizQuestionsId);
                        }
                    }
                }
                // updating post
                $postsDao->updatePostsTitleByQuizId($inputJson['quiz_id'], $inputJson['title'], $dateUtils->getCurrentDate());
            }


            Log::debug('qwiz : ' . json_encode($inputJson['quiz_id']));

            //Send the success response now
            $response = [
                'status' => $codes->RC_SUCCESS,
                'message' => $codes->RM_SUCCESS,
                'data' => ['quizId'=>$inputJson['quiz_id']]
            ];
            return $response;

            Log::debug("Ended ... qwiz Service : ");
        } catch (\Exception $e) {
            Log::debug($e);
            throw new Exception($e);
        }
    }
    public function deleteQwiz($inputJson)
    {
        Log::debug("Started ... deleteQwiz Service : " . json_encode($inputJson));
        try {
            $codes = new Codes;
            $dataValidation = new DataValidation;
            $userSessionDao = new UserSessionDao;
            $usersDao = new UsersDao;
            $dateUtils = new DateUtils;
            $quizzesDao = new QuizzesDao;
            $quizzesQuestionsDao = new QuizzesQuestionsDao;
            $quizzesQuestionsOptionsDao = new QuizzesQuestionsOptionsDao;
            $postsDao = new PostsDao;

            //Validations to be done
            //Id validation
            if (!isset($inputJson['quiz_id']) || $dataValidation->isEmpty($inputJson['quiz_id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your quiz ID",
                    'data' => null
                ];
                return $response;
            }
           
            //creator validation
            if (!isset($inputJson['creator_id']) || $dataValidation->isEmpty($inputJson['creator_id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter creator",
                    'data' => null
                ];
                return $response;
            }

            Log::debug("Deleting the qwiz : ");
            $qryStatus = $quizzesDao->deleteQuizzes($inputJson['quiz_id'], $inputJson['creator_id']);
            Log::debug("Deleted the qwiz ");
            if($qryStatus){
                // deleting questions an their options
                $quizzesQuestionsDao->deleteQuestionsByQuizId($inputJson['quiz_id']);
                // deleting post
                $postsDao->deletePostsByQuizId($inputJson['quiz_id']);
            }


            Log::debug('qwiz : ' . json_encode($inputJson['quiz_id']));

            //Send the success response now
            $response = [
                'status' => $codes->RC_SUCCESS,
                'message' => $codes->RM_SUCCESS,
                'data' => ['quizId'=>$inputJson['quiz_id']]
            ];
            return $response;

            Log::debug("Ended ... qwiz Service : ");
        } catch (\Exception $e) {
            Log::debug($e);
            throw new Exception($e);
        }
    }

    public function getQwizzesList($inputJson)
    {
        Log::debug("Started ... getQwizzesList Service : " . json_encode($inputJson));
        try {
            $codes = new Codes;
            $quizzesDao = new QuizzesDao;

            Log::debug("getting the qwizzes : ");
            $quizzesList = $quizzesDao->getQuizzesList();
            Log::debug("Got the qwiz ");

            //Send the success response now
            $response = [
                'status' => $codes->RC_SUCCESS,
                'message' => $codes->RM_SUCCESS,
                'data' => ['quizzesList'=>$quizzesList]
            ];
            return $response;

            Log::debug("Ended ... qwiz Service : ");
        } catch (\Exception $e) {
            Log::debug($e);
            throw new Exception($e);
        }
    }

}
