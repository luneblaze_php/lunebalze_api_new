<?php 

namespace App\Authentication;

use App\Utils\Codes;
use Cake\Log\Log; 

class AuthService {

    public function hashPassword($password, $salt){
        return md5($password.$salt);
    }

    public function createSalt(){
        $str=rand(); 
        return md5($str);
    }


    public function authenticatePasswordBased($password, $hash, $salt){
        return (md5($password.$salt) == $hash);
    }

    
    public function authenticateTokenBased($token,$mobilenumber){

        //Decrypt the token using the issuer
        $codes = new Codes;

        $decryptedToken=openssl_decrypt ($token,$codes->encryption, $codes->ISSUER,0,$codes->IV); 
        
        //Check if the first 10 digits match with the decryptedToken here

        $mNumber = substr($decryptedToken,0,10);

        if($mNumber != $mobilenumber)
            return false;

        //Check if the next timestamp has not expired

        $timestamp = substr($decryptedToken,10,strlen($decryptedToken)-1);

        if(((float)$timestamp + $codes->expiryts) <= $this->currentTime())
            return false;

        return true;

    }

    public function createToken($mobileNumber){

        $codes = new Codes;

        $encryption = openssl_encrypt($mobileNumber.$this->currentTime(), $codes->encryption, $codes->ISSUER,0, $codes->IV); 

        return $encryption;
    }


    function currentTime()
    {
        list($usec, $sec) = explode(" ", microtime());
        return ((float)$usec + (float)$sec);
    }


    public function getOtp(){
        $str=rand(1000,9999);
        return $str;
    }
}
?>