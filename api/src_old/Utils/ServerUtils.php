<?php
namespace App\Utils;

use App\Utils\Codes;

class ServerUtils {

    public static function getSystemUpload()
    {
        $codes = new Codes;
        $systemUploads = null;

        if($codes->S3_ENABLED) {
            switch ($codes->S3_REGION) {
                case 'us-east-1':
                    $endpoint = "https://s3.amazonaws.com/";
                    break;
        
                case 'us-west-2':
                    $endpoint = "https://s3-us-west-2.amazonaws.com/";
                    break;
        
                case 'ap-northeast-2':
                    $endpoint = "https://s3.ap-northeast-2.amazonaws.com/";
                    break;
        
                case 'ap-southeast-1':
                    $endpoint = "https://s3-ap-southeast-1.amazonaws.com/";
                    break;
        
                case 'ap-southeast-2':
                    $endpoint = "https://s3-ap-southeast-2.amazonaws.com/";
                    break;
        
                case 'ap-northeast-1':
                    $endpoint = "https://s3-ap-northeast-1.amazonaws.com/";
                    break;
        
                case 'eu-central-1':
                    $endpoint = "https://s3.eu-central-1.amazonaws.com/";
                    break;
                
                case 'eu-west-1':
                    $endpoint = "https://s3-eu-west-1.amazonaws.com/";
                    break;
            }
            $systemUploads = $endpoint.$codes->S3_BUCKET."/uploads";
        } else {
            $systemUploads = $codes->SYSTEM_URL.'/'.$codes->UPLOADS_DIRECTORY;
        }

        return $systemUploads;
    }

}