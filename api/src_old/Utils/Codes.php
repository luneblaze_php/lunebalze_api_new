<?php 
    namespace App\Utils;

    class Codes {

        public $ISSUER = "LUNEBLAZE_ISSUER@2020";
        public $encryption = "AES-128-CTR";

        public $expiryts = 86400 * 30 * 12 * 5; //5 years


        public $IV = '1234567891011121'; 


        public $RC_ERROR="0";
        public $RC_SUCCESS="1";
        public $RC_DATA_VALIDATION_ERROR="100";
        


        public $RM_SUCCESS="Success";
        public $RM_DATA_VALIDATION_ERROR="Data Validation Error.";
        public $RM_ALL_FIELDS_TO_BE_PRESENT="You must provide all the fields.";
        
        

        public $MAX_RESULTS=10;

        public $MAX_LIMIT=10000000;


        public $STARTING_OFFSET=0;




        //System Variables
        public $SYSTEM_URL = "http://strotam.com";

        public $S3_ENABLED=false;
        public $S3_REGION=null;
        public $S3_BUCKET=null;

        public $UPLOADS_DIRECTORY="content/uploads";

        public $SYSTEM_THEME="default";




    }

?>