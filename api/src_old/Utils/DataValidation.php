<?php
namespace App\Utils;

class DataValidation {

    /**
    *
    */
    public function isValidInteger( $var ) {
        if ( is_numeric( $var ) )
        return true;
        return false;
    }

    /**
     * 
     */
    function isEmpty( $value ) {

        if(!isset($value))
            return true;

        if ( strlen( trim( preg_replace( '/\xc2\xa0/', ' ', $value ) ) ) == 0 ) {
            return true;
        } else {
            return false;
        }
    }

    /**
    * valid_email
    *
    * @param string $email
    * @return boolean
    */

    public function valid_email( $email ) {
        if ( filter_var( $email, FILTER_VALIDATE_EMAIL ) !== false ) {
            return true;
        } else {
            return false;
        }
    }

    /**
    * valid_url
    *
    * @param string $url
    * @return boolean
    */

    public function valid_url( $url ) {
        if ( filter_var( $url, FILTER_VALIDATE_URL, FILTER_FLAG_HOST_REQUIRED ) !== false ) {
            return true;
        } else {
            return false;
        }
    }

    /**
    * valid_username
    *
    * @param string $username
    * @return boolean
    */

    public function valid_username( $username ) {
        if ( strlen( $username ) >= 3 && preg_match( '/^[a-zA-Z0-9]+([_|.]?[a-zA-Z0-9])*$/', $username ) ) {
            return true;
        } else {
            return false;
        }
    }

    /**
    * valid_name
    *
    * @param string $name
    * @return boolean
    */

    public function valid_name( $name ) {
        if ( preg_match( "/^[\\p{L} ]+$/ui", $name ) ) {
            return true;
        } else {
            return false;
        }
    }

    /**
    * valid_location
    *
    * @param string $location
    * @return boolean
    */

    public function valid_location( $location ) {
        if ( preg_match( "/^[\\p{L} ,()0-9]+$/ui", $location ) ) {
            return true;
        } else {
            return false;
        }
    }

    /**
    * valid_extension
    *
    * @param string $extension
    * @param string $allowed_extensions
    * @return boolean
    */

    public function valid_extension( $extension, $allowed_extensions ) {
        $extensions = explode( ',', $allowed_extensions );
        foreach ( $extensions as $key => $value ) {
            $extensions[$key] = strtolower( trim( $value ) );
        }
        if ( is_array( $extensions ) && in_array( $extension, $extensions ) ) {
            return true;
        }
        return false;
    }

    /* ------------------------------- */
    /* Date */
    /* ------------------------------- */

    /**
    * set_datetime
    *
    * @param string $date
    * @return string
    */

    public function set_datetime( $date ) {
        return date( 'Y-m-d H:i:s', strtotime( $date ) );
    }

    /**
    * get_datetime
    *
    * @param string $date
    * @return string
    */

    public function get_datetime( $date ) {
        return date( 'm/d/Y g:i A', strtotime( $date ) );
    }

    /**
    * get_hash_key
    *
    * @param integer $length
    * @return string
    */

    public function get_hash_key( $length = 8 ) {
        $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        $count = mb_strlen( $chars );
        for ( $i = 0, $result = ''; $i < $length; $i++ ) {
            $index = rand( 0, $count - 1 );
            $result .= mb_substr( $chars, $index, 1 );
        }
        return $result;
    }

    /**
    * get_hash_key number
    *
    * @param integer $length
    * @return string
    */

    public function get_hash_keyn( $length = 8 ) {
        $chars = '0123456789';
        $count = mb_strlen( $chars );
        for ( $i = 0, $result = ''; $i < $length; $i++ ) {
            $index = rand( 0, $count - 1 );
            $result .= mb_substr( $chars, $index, 1 );
        }
        return $result;
    }

    /**
    * get_hash_token
    *
    * @return string
    */

    public function get_hash_token() {
        return md5( time()*rand( 1, 9999 ) );
    }

}