<?php

namespace App\ServiceLayer;

use App\ServiceLayer\SMSService;
use App\Authentication\AuthService;
use App\Utils\Codes;
use App\Utils\DataValidation;
use Cake\Core\Exception\Exception;
use App\DaoLayer\CitiesDao;
use App\DaoLayer\UsersDao;

use Cake\Log\Log;

class RegistrationService
{

    

    /**
     * 
     */
    public function signup_current_status($inputJson)
    {
        Log::debug("Started ... signup_current_status Service : ".json_encode($inputJson));
        try{
            $usersDao = new UsersDao;
            $codes = new Codes;
            $dataValidation = new DataValidation;

            //Validation to be done here

            if(!isset($inputJson['user_id']) || !isset($inputJson['user_work']) || !isset($inputJson['work_title']) || !isset($inputJson['work_place']))
            {
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>$codes->RM_ALL_FIELDS_TO_BE_PRESENT,
                    'data'=>null
                ];

                return $response;                
            }

            if($dataValidation->isEmpty($inputJson['user_id']))
            {
                
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>"Please enter your user id",
                    'data'=>null
                ];

                return $response;
            }
            if($dataValidation->isEmpty($inputJson['user_work']))
            {
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>"Please enter your profession",
                    'data'=>null
                ];

                return $response;
            }
            if($dataValidation->isEmpty($inputJson['work_title']))
            {
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>"Please enter place of work",
                    'data'=>null
                ];

                return $response;
            }            
            if($dataValidation->isEmpty($inputJson['work_place']))
            {
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>"Please enter designation",
                    'data'=>null
                ];

                return $response;
            }

            //Validation done -- 
            
            //Step 1 : Check if the user id is present
            $user = $usersDao->getUserByUserId($inputJson['user_id']);

            if($user == null)
            {
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>"Invalid User",
                    'data'=>null
                ];

                return $response;
            }

            Log::debug("User Fetched : ".json_encode($user));

            //Step 2 : Venue Id 
            $venue_id=(isset($inputJson['venue_id']))?$inputJson['venue_id']:0;
            
            //Step 3 : Update users by user Id
            $usersDao->updateUserByUserId($inputJson,$venue_id,$inputJson['user_id']);


            //Step 4 : Return the response
            $data = array(
                'user_id'=>$inputJson['user_id']
            );

            $response = [
                'status'=>$codes->RC_SUCCESS,
                'message'=>"",
                'data'=>$data
            ];

            Log::debug("Ended ... signup_current_status Service : ");

            return $response;            
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }

    }

    
    public function getcitiesbycityid($inputJson)
    {

        Log::debug("Started ... getcitiesbycityid Service : ".json_encode($inputJson));
        try{
            $citiesDao = new CitiesDao;
            $codes = new Codes;
            $dataValidation = new DataValidation;

            //Validation to be done here
            if(!isset($inputJson['_CityId']))
            {
                $response = [
                    '_ReturnCode'=>$codes->RC_DATA_VALIDATION_ERROR,
                    '_ReturnMessage'=>$codes->RM_DATA_VALIDATION_ERROR,
                    '_Field'=>"_CityId"
                ];

                return $response;
            }
            if(!$dataValidation->isValidInteger($inputJson['_CityId']))
            {
                $response = [
                    '_ReturnCode'=>$codes->RC_DATA_VALIDATION_ERROR,
                    '_ReturnMessage'=>$codes->RM_DATA_VALIDATION_ERROR,
                    '_Field'=>"_CityId"
                ];

                return $response;
            }

            $response = [
                '_ReturnCode'=>$codes->RC_SUCCESS,
                '_ReturnMessage'=>$codes->RM_SUCCESS,
                'cities'=> array()
            ];

            $cities = $citiesDao->getCitiesByCityId($inputJson['_CityId']);

            $response['cities'] = $cities;


            Log::debug("Ended ... getcitiesbycityid Service : ");
            return $response;
            
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }
    

}
