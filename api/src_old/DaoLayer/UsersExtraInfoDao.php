<?php

namespace App\DaoLayer;

use App\Utils\Codes;
use Cake\Core\Exception\Exception;
use Cake\Datasource\ConnectionManager;
use Cake\Log\Log;

class UsersExtraInfoDao
{

   

    /**
     * 
     */
    public function updateActivationKeyAddedOnByUserId($activationKey,$addedOn, $user_id, $type)
    {
        Log::debug("Started ...updateActivationKeyAddedOnByUserId Dao : Activation Key : ".$activationKey.", User Id : ".$user_id.
        " Added On : ".$addedOn." User Type : ".$type);

        try{
            
            $conn = ConnectionManager::get('default');

            $sql=sprintf("UPDATE `users_extra_info` SET `activation_key`=%s,`added_on`=%s WHERE `user_id`=%s and type='%s'",$activationKey,$addedOn,$user_id,$type);

            Log::debug("SQL : ".$sql);

            $conn->execute($sql);

            Log::debug("Ended ...updateActivationKeyAddedOnByUserId Dao");

        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }

    

    /**
     * 
     */
    public function getUsersExtraInfoByDataAndType($data,$type)
    {
        Log::debug("Started ...getUsersExtraInfoByDataAndType Dao");

        try{
            
            $conn = ConnectionManager::get('default');

            $sql=sprintf("SELECT * FROM users_extra_info WHERE type = '%s' and data = %s and status='1'", $type,$data);

            Log::debug("SQL : ".$sql);

            $stmt = $conn->execute($sql);

            $result = $stmt->fetch("assoc");

            Log::debug("Ended ...getUsersExtraInfoByDataAndType Dao");

            return $result;

        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }        
    

    /**
     * Upsert Users Extra Info
     * Insert 
     * on duplicate key
     * update
     */
    public function upsertUsersExtraInfo($cityId)
    {   
        Log::debug("Started ...upsertUsersExtraInfo Dao");

        try{
            
            $conn = ConnectionManager::get('default');
            $codes = new Codes;


        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }

}