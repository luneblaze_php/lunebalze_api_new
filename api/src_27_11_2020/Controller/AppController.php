<?php
namespace App\Controller;

use App\Authentication\AuthService;
use App\ServiceLayer\ConstantService;
use App\ServiceLayer\FriendService;
use App\ServiceLayer\ProductService;
use App\ServiceLayer\QuizService;
use App\ServiceLayer\DebateService;
use App\ServiceLayer\CommunityService;
use Cake\Controller\Controller;
use App\ServiceLayer\CommentService;
use Cake\Log\Log; 
use Cake\Event\Event;


use App\ServiceLayer\RegistrationService;

   
    class AppController extends Controller
    {
        public function initialize()
        {
            parent::initialize();

            $this->loadComponent('RequestHandler', [
                'enableBeforeRedirect' => false,
            ]);
            $this->loadComponent('Flash');
        }

        public function friendRequestsSent()
        {

            Log::debug("Started ... friend_requests_sent : ");
            $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $frndService = new FriendService;

                $response = $frndService->friend_requests_sent($jsonInput);

            }catch(\Exception $e){
                $response = [
                    'status'=>'999',
                    'message'=>'Fatal Error'
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }   


        public function getInterestList()
        {

            Log::debug("Started ... get_interest_list : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $constService = new ConstantService;

                $response = $constService->get_interest($jsonInput);

            }catch(\Exception $e){
                $response = [
                    'status'=>'999',
                    'message'=>'Fatal Error'
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function getUserInterest()
        {

            Log::debug("Started ... get_user_interest : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $constService = new ConstantService;

                $response = $constService->get_user_interest($jsonInput);

            }catch(\Exception $e){
                $response = [
                    'status'=>'999',
                    'message'=>'Fatal Error'
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }


        public function searchInterest()
        {

            Log::debug("Started ... search_interest : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $constService = new ConstantService;

                $response = $constService->search_interest($jsonInput);

            }catch(\Exception $e){
                $response = [
                    'status'=>'999',
                    'message'=>'Fatal Error'
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }


        public function signupCurrentStatus()
        {

            Log::debug("Started ... signup_current_status : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $regService = new RegistrationService;

                $response = $regService->signup_current_status($jsonInput);

            }catch(\Exception $e){
                $response = [
                    'status'=>'999',
                    'message'=>'Fatal Error'
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function login()
        {
            Log::debug("Started ... login : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $regService = new RegistrationService;

                $response = $regService->sign_in($jsonInput);

            }catch(\Exception $e){
                $response = [
                    'status'=>'999',
                    'message'=>'Fatal Error'
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function signup()
        {
            Log::debug("Started ... signup : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $regService = new RegistrationService;

                $response = $regService->sign_up($jsonInput);

            }catch(\Exception $e){
                $response = [
                    'status'=>'999',
                    'message'=>'Fatal Error'.$e->getMessage()
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function signupverify()
        {
            Log::debug("Started ... signupverify : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $regService = new RegistrationService;

                $response = $regService->signup_verify($jsonInput);

            }catch(\Exception $e){
                $response = [
                    'status'=>'999',
                    'message'=>'Fatal Error'
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function loginTwoStep()
        {
            Log::debug("Started ... login_two_step : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $regService = new RegistrationService;

                $response = $regService->login_two_step($jsonInput);

            }catch(\Exception $e){
                $response = [
                    'status'=>'999',
                    'message'=>'Fatal Error'
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function forgetPassword()
        {
            Log::debug("Started ... forget_password : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $regService = new RegistrationService;

                $response = $regService->forgot_password($jsonInput);

            }catch(\Exception $e){
                $response = [
                    'status'=>'999',
                    'message'=>'Fatal Error'
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }


        public function forgetPasswordReset()
        {
            Log::debug("Started ... forget_password_reset : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $regService = new RegistrationService;

                $response = $regService->forgot_password_reset($jsonInput);

            }catch(\Exception $e){
                $response = [
                    'status'=>'999',
                    'message'=>'Fatal Error'
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function getFriendRequest()
        {
            Log::debug("Started ... get_friend_request : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $frndService = new FriendService;

                $response = $frndService->get_friend_request($jsonInput);

            }catch(\Exception $e){
                $response = [
                    'status'=>'999',
                    'message'=>'Fatal Error'
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function getOrganization()
        {
            Log::debug("Started ... get_organization : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $constantService = new ConstantService;

                $response = $constantService->get_organization($jsonInput);

            }catch(\Exception $e){
                $response = [
                    'status'=>'999',
                    'message'=>'Fatal Error'
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function getCities()
        {

            Log::debug("Started ... getCities : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $regService = new RegistrationService;

                $response = $regService->getcitiesbycityid($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function createQuiz()
        {

            Log::debug("Started ... newQuiz : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $regService = new QuizService;

                $response = $regService->createQuiz($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function updateQuiz()
        {

            Log::debug("Started ... updateQuiz : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $regService = new QuizService;

                $response = $regService->updateQuiz($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }
        public function deleteQuiz()
        {

            Log::debug("Started ... deleteQuiz : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $regService = new QuizService;

                $response = $regService->deleteQuiz($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function getMyQuiz()
        {

            Log::debug("Started ... getMyQuiz : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $regService = new QuizService;

                $response = $regService->getMyQuiz($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }
        public function getQuizzesList()
        {

            Log::debug("Started ... getQuizzesList : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $regService = new QuizService;

                $response = $regService->getQuizzesList($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }
        public function updateQuizParticipation()
        {

            Log::debug("Started ... updateQuizParticipation : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $regService = new QuizService;

                $response = $regService->updateQuizParticipation($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function updateQuizLastSeenQuestion()
        {

            Log::debug("Started ... updateQuizLastSeenQuestion : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $regService = new QuizService;

                $response = $regService->updateQuizLastSeenQuestion($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }


        public function addQuizParticipantsAnswer()
        {

            Log::debug("Started ... addQuizParticipantsAnswer : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $regService = new QuizService;

                $response = $regService->addQuizParticipantsAnswer($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function submitQuizParticipation()
        {

            Log::debug("Started ... submitQuizParticipation : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $regService = new QuizService;

                $response = $regService->calculateQuizScore($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function inviteUserForQuiz()
        {

            Log::debug("Started ... inviteUserForQuiz : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $regService = new QuizService;

                $response = $regService->inviteUserForQuiz($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function getInvitedUserForQuiz()
        {

            Log::debug("Started ... getInvitedUserForQuiz : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $regService = new QuizService;

                $response = $regService->getInvitedUserListForQuiz($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }



        public function createDebate()
        {

            Log::debug("Started ... newDebate : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $regService = new DebateService;

                $response = $regService->createDebate($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function updateDebate()
        {

            Log::debug("Started ... updateDebate : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $regService = new DebateService;

                $response = $regService->updateDebate($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }
        public function deleteDebate()
        {

            Log::debug("Started ... deleteDebate : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $regService = new DebateService;

                $response = $regService->deleteDebate($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function getMyDebate()
        {

            Log::debug("Started ... getMyDebate : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $regService = new DebateService;

                $response = $regService->getMyDebate($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }
        public function getDebatesList()
        {

            Log::debug("Started ... getDebatesList : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $regService = new DebateService;

                $response = $regService->getDebatesList($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }
        public function updateDebateParticipation()
        {

            Log::debug("Started ... updateDebateParticipation : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $regService = new DebateService;

                $response = $regService->updateDebateParticipation($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function addDebateParticipantsProsAndCons()
        {

            Log::debug("Started ... addDebateParticipantsProsAndCons : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $regService = new DebateService;

                $response = $regService->addDebateParticipantsProsAndCons($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }
        public function updateDebateParticipantsProsAndCons()
        {

            Log::debug("Started ... updateDebateParticipantsProsAndCons : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $regService = new DebateService;

                $response = $regService->updateDebateParticipantsProsAndCons($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }
        public function deleteDebateParticipantsProsAndCons()
        {

            Log::debug("Started ... deleteDebateParticipantsProsAndCons : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $regService = new DebateService;

                $response = $regService->deleteDebateParticipantsProsAndCons($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }
        public function addDebateProsAndConsRating()
        {

            Log::debug("Started ... addDebateProsAndConsRating : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $regService = new DebateService;

                $response = $regService->addDebateProsAndConsRating($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function submitDebateParticipation()
        {

            Log::debug("Started ... submitDebateParticipation : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $regService = new DebateService;

                $response = $regService->calculateDebateScore($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function inviteUserForDebate()
        {

            Log::debug("Started ... inviteUserForDebate : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $regService = new DebateService;

                $response = $regService->inviteUserForDebate($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function getInvitedUserForDebate()
        {

            Log::debug("Started ... getInvitedUserForDebate : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $regService = new DebateService;

                $response = $regService->getInvitedUserListForDebate($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function updateDebateProsAndConsFollowers()
        {

            Log::debug("Started ... updateDebateProsAndConsFollowers : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $regService = new DebateService;

                $response = $regService->updateDebateProsAndConsFollowers($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function getDebateProsAndConsList()
        {

            Log::debug("Started ... getDebateProsAndConsList : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $regService = new DebateService;

                $response = $regService->getDebateProsAndConsList($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function createCommunity()
        {

            Log::debug("Started ... newCommunity : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $regService = new CommunityService;

                $response = $regService->createCommunity($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function updateCommunity()
        {

            Log::debug("Started ... updateCommunity : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $regService = new CommunityService;

                $response = $regService->updateCommunity($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }
        public function deleteCommunity()
        {

            Log::debug("Started ... deleteCommunity : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $regService = new CommunityService;

                $response = $regService->deleteCommunity($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function getMyCommunity()
        {

            Log::debug("Started ... getMyCommunity : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $regService = new CommunityService;

                $response = $regService->getMyCommunity($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }
        public function getCommunityList()
        {

            Log::debug("Started ... getCommunityList : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $regService = new CommunityService;

                $response = $regService->getCommunityList($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function addRemoveCommunityParticipation()
        {

            Log::debug("Started ... addRemoveCommunityParticipation : ");
            $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $regService = new CommunityService;

                $response = $regService->addRemoveCommunityParticipation($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }
        public function promoteDemoteCommunityUser()
        {

            Log::debug("Started ... submitCommunityParticipation : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $regService = new CommunityService;

                $response = $regService->promoteDemoteCommunityUser($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function inviteUserForCommunity()
        {

            Log::debug("Started ... inviteUserForCommunity : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $regService = new CommunityService;

                $response = $regService->inviteUserForCommunity($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function getInvitedUserForCommunity()
        {

            Log::debug("Started ... getInvitedUserForCommunity : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $regService = new CommunityService;

                $response = $regService->getInvitedUserListForCommunity($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function comment(){
          
            header('Content-Type: application/json');
            //For proper json response in any condition
            Log::debug("Started ... comment : ");

            $response = null;
            try{
                $jsonInput = $this->request->getData();

                $commentService = new CommentService;

                $response = $commentService->get_comment($jsonInput);

            }catch(\Exception $e){
                /*Log::debug($e);
                throw new \Exception($e);*/
                $response = [
                    'status'=>'999',
                    'message'=>'Fatal Error'
                ];
            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }
        }
    }
