<?php

namespace App\ServiceLayer;

use App\Utils\Codes;
use App\Utils\DataValidation;
use Cake\Core\Exception\Exception;
use App\DaoLayer\UsersDao;
use App\DaoLayer\UserSessionDao;
use App\Utils\DateUtils;
use App\Utils\ImageUtils;
use Cake\Log\Log;
use App\DaoLayer\QuizzesDao;
use App\DaoLayer\QuizzesQuestionsDao;
use App\DaoLayer\QuizzesQuestionsOptionsDao;
use App\DaoLayer\PostsDao;

class QuizService
{

    private function uploadImage($UPLOADS_DIRECTORY){
        $folder = 'photos';
        $depth = '../';
        if(!file_exists($depth.$UPLOADS_DIRECTORY.'/'.$folder)) {
            @mkdir($depth.$UPLOADS_DIRECTORY.'/'.$folder, 0777, true);
        }
        if(!file_exists($depth.$UPLOADS_DIRECTORY.'/'.$folder.'/' . date('Y'))) {
            @mkdir($depth.$UPLOADS_DIRECTORY.'/'.$folder.'/' . date('Y'), 0777, true);
        }
        if(!file_exists($UPLOADS_DIRECTORY.'/'.$folder.'/' . date('Y') . '/' . date('m'))) {
            @mkdir($depth.$UPLOADS_DIRECTORY.'/'.$folder.'/' . date('Y') . '/' . date('m'), 0777, true);
        }

        // valid inputs
        if(!isset($_FILES["quiz_picture"]) || $_FILES["quiz_picture"]["error"] != UPLOAD_ERR_OK) {
            throw new Exception("Something wrong with upload! Is 'upload_max_filesize' set correctly?");
        }
        $directory = $folder.'/'. date('Y') . '/' . date('m') . '/';
        $prefix = md5(time()*rand(1, 9999));
        $imgArr = explode('.', $_FILES['quiz_picture']['name']);
        $extension = end($imgArr);
        $get_dimensions = getimagesize($_FILES['quiz_picture']['tmp_name']);
        $dimensions = $get_dimensions[1].'X'.$get_dimensions[0];
            
        $image_new_name = $directory.$prefix.'.'.$extension;
        $path_new = $depth.$UPLOADS_DIRECTORY.'/'.$image_new_name;

        /* check if the file uploaded successfully */
        if(!@move_uploaded_file($_FILES['quiz_picture']['tmp_name'], $path_new)) {
            throw new Exception("Sorry, can not upload the photos");
        }
                
        /* return */
        $return_files['image'] = $image_new_name;
        $return_files['dimensions'] = $dimensions;
        return $return_files;
    }
    private function uploadMultiImage($UPLOADS_DIRECTORY){


        $folder = 'photos';
        $depth = '../';
        if(!file_exists($depth.$UPLOADS_DIRECTORY.'/'.$folder)) {
            @mkdir($depth.$UPLOADS_DIRECTORY.'/'.$folder, 0777, true);
        }
        if(!file_exists($depth.$UPLOADS_DIRECTORY.'/'.$folder.'/' . date('Y'))) {
            @mkdir($depth.$UPLOADS_DIRECTORY.'/'.$folder.'/' . date('Y'), 0777, true);
        }
        if(!file_exists($UPLOADS_DIRECTORY.'/'.$folder.'/' . date('Y') . '/' . date('m'))) {
            @mkdir($depth.$UPLOADS_DIRECTORY.'/'.$folder.'/' . date('Y') . '/' . date('m'), 0777, true);
        }

        $files = array();
        if(isset($_FILES['picture']) && !empty($_FILES['picture'])){
            foreach($_FILES['picture'] as $key => $val) {
                for($i=0; $i < count($val); $i++) {
                    $files[$i][$key] = @$val[$i];
                }
            }
        }
      
        
        $files_num = count($files);
        foreach ($files as $key=>$file) {
            if(isset($file["tmp_name"]) && !empty($file["tmp_name"])){
                // valid inputs
                if(!isset($file) || $file["error"] != UPLOAD_ERR_OK) {
                    if($files_num > 1) {
                        continue;
                    } else {
                        throw new Exception("Something wrong with upload! Is 'upload_max_filesize' set correctly?");
                    }
                }

                /* prepare new file name */
                $directory = $folder.'/'. date('Y') . '/' . date('m') . '/';
                $prefix = md5(time()*rand(1, 9999));

                $imgArr = explode('.', $file["name"]);
                $extension = end($imgArr);
                $get_dimensions = getimagesize($file["tmp_name"]);
                $dimensions =  $get_dimensions[1].'X'.$get_dimensions[0];
                $image_new_name = $directory.$prefix.'.'.$extension;
                $path_new = $depth.$UPLOADS_DIRECTORY.'/'.$image_new_name;

                /* check if the file uploaded successfully */
                if(!@move_uploaded_file($file['tmp_name'], $path_new)) {
                    if($files_num > 1) {
                        continue;
                    } else {
                        throw new Exception("Sorry, can not upload the file");
                    }
                } 

            }
            /* return */
            $return_files[$key]['image'] = @$image_new_name;
            $return_files[$key]['dimensions'] = @$dimensions;
        }
        
        return @$return_files;
    }

    private function base64ToImage($imageData, $UPLOADS_DIRECTORY){
        $folder = 'photos';
        $depth = '../';
        if(!file_exists($depth.$UPLOADS_DIRECTORY.'/'.$folder)) {
            @mkdir($depth.$UPLOADS_DIRECTORY.'/'.$folder, 0777, true);
        }
        if(!file_exists($depth.$UPLOADS_DIRECTORY.'/'.$folder.'/' . date('Y'))) {
            @mkdir($depth.$UPLOADS_DIRECTORY.'/'.$folder.'/' . date('Y'), 0777, true);
        }
        if(!file_exists($UPLOADS_DIRECTORY.'/'.$folder.'/' . date('Y') . '/' . date('m'))) {
            @mkdir($depth.$UPLOADS_DIRECTORY.'/'.$folder.'/' . date('Y') . '/' . date('m'), 0777, true);
        }

        /* prepare new file name */
        $directory = $folder.'/'. date('Y') . '/' . date('m') . '/';
        list($type, $imageData) = explode(';', $imageData);
        list(,$extension) = explode('/',$type);
        list(,$imageData)      = explode(',', $imageData);
        $fileName = md5(time()*rand(1, 9999)).'.'.$extension;
        $imageData = base64_decode($imageData);
        if(file_put_contents($depth.$UPLOADS_DIRECTORY.'/'.$directory.$fileName, $imageData))
            return $directory.$fileName;
        else
            return '';
    }

    public function createQuiz($inputJson)
    {
        Log::debug("Started ... createQuiz Service : " . json_encode($inputJson));
        try {
            $codes = new Codes;
            $dataValidation = new DataValidation;
            $userSessionDao = new UserSessionDao;
            $usersDao = new UsersDao;
            $imageUtils = new ImageUtils;
            $dateUtils = new DateUtils;
            $quizzesDao = new QuizzesDao;
            $quizzesQuestionsDao = new QuizzesQuestionsDao;
            $quizzesQuestionsOptionsDao = new QuizzesQuestionsOptionsDao;
            $postsDao = new PostsDao;

            //Validations to be done
            //Title validation
            if (!isset($inputJson['title']) || $dataValidation->isEmpty($inputJson['title'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your title",
                    'data' => null
                ];
                return $response;
            }
            //Description validation
            if (!isset($inputJson['description']) || $dataValidation->isEmpty($inputJson['description'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your description",
                    'data' => null
                ];
                return $response;
            }
            // //Picture validation
            // if (!isset($inputJson['quiz_picture']) || $dataValidation->isEmpty($inputJson['quiz_picture'])) {
            //     $response = [
            //         'status' => $codes->RC_ERROR,
            //         'message' => "Please enter your quiz picture",
            //         'data' => null
            //     ];
            //     return $response;
            // }
            //User type validation
            if (!isset($inputJson['user_type']) || $dataValidation->isEmpty($inputJson['user_type'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your user type",
                    'data' => null
                ];
                return $response;
            }
            //Quiz start time validation
            if (!isset($inputJson['start_time']) || $dataValidation->isEmpty($inputJson['start_time'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your user type",
                    'data' => null
                ];
                return $response;
            }
            //Quiz live duration validation
            if (!isset($inputJson['live_duration']) || $dataValidation->isEmpty($inputJson['live_duration'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter live duration",
                    'data' => null
                ];
                return $response;
            }
            //creator validation
            if (!isset($inputJson['creator_id']) || $dataValidation->isEmpty($inputJson['creator_id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter creator",
                    'data' => null
                ];
                return $response;
            }
            //Questions validation
            if (!isset($inputJson['question']) || !is_array($inputJson['question']) || count($inputJson['question']) < 1) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter questions",
                    'data' => null
                ];
                return $response;
            }
            //Answers validation
            if (!isset($inputJson['answers']) || !is_array($inputJson['answers']) || count($inputJson['answers']) < 1) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter answers",
                    'data' => null
                ];
                return $response;
            }
            //correct answers validation
            if (!isset($inputJson['correct']) || !is_array($inputJson['correct']) || count($inputJson['correct']) < 1) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter correct answers",
                    'data' => null
                ];
                return $response;
            }
            //time alloted validation
            if (!isset($inputJson['time_alloted']) || !is_array($inputJson['time_alloted']) || count($inputJson['time_alloted']) < 1) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter time alloted",
                    'data' => null
                ];
                return $response;
            }
            //time alloted validation
            if (!is_numeric($inputJson['time_alloted'][0])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Time alloted should be number in seconds",
                    'data' => null
                ];
                return $response;
            }


            //uploading images
            $quiz_picture = $this->uploadImage($codes->UPLOADS_DIRECTORY);

            Log::debug("Saving the quiz : ");
            $quizId = $quizzesDao->saveQuizzes($inputJson, $quiz_picture['image'], $quiz_picture['dimensions']);
            Log::debug("Saved the quiz ");
            if($quizId){
                //uploading image
                $question_pictures = $this->uploadMultiImage($codes->UPLOADS_DIRECTORY);
                foreach ($inputJson['question'] as $i => $q) {
                    // checking answers options 
                    $answers = [];
                    if (isset($inputJson['answers'][$i]) && !empty($inputJson['answers'][$i])) {
                        $aswArr = explode('#*#', $inputJson['answers'][$i]);
                        foreach ($aswArr as $aswkey => $ans)
                            array_push($answers, ["option_text"=>$ans, "is_correct"=>((isset($inputJson['correct'][$i]) && $aswkey == ($inputJson['correct'][$i]-1))?1:0)]);
                    }
                    // seting array of answers in current format
                    $question = [
                        "question"=>$q,
                        "picture"=>'',
                        "time_alloted"=> @$inputJson['time_alloted'][$i],
                        "options"=> $answers,
                        "image_file_name"=> @$question_pictures[$i]['image'],
                        "image_file_dimensions"=> @$question_pictures[$i]['dimensions'],
                    ];
                    $quizQuestionsId = $quizzesQuestionsDao->saveQuizzesQuestions($question, $quizId);

                    if($quizQuestionsId && $question['options'] && count($question['options']) > 0){
                        foreach ($question['options'] as $key => $optionRow) {
                            $quizzesQuestionsOptionsDao->saveQuizzesQuestionsOptions($optionRow, $quizId, $quizQuestionsId);
                        }
                    }
                }
                $postsDao->saveQuizPosts($quizId, $inputJson['creator_id'], $inputJson['title'], $dateUtils->getCurrentDate());
                // checking total quiz of user
                $countRes = $quizzesDao->getQuizCountByUserId($inputJson['creator_id']);
                //updating total quiz of user
                $usersDao->updateUserQuizCountByUserId($countRes['total'], $inputJson['creator_id']);
            }


            Log::debug('quiz : ' . json_encode($quizId));

            //Send the success response now
            $response = [
                'status' => $codes->RC_SUCCESS,
                'message' => $codes->RM_SUCCESS,
                'data' => $quizzesDao->getQwizByUserAndId($quizId, $inputJson['creator_id'], $dateUtils->getCurrentDate())
            ];
            return $response;

            Log::debug("Ended ... quiz Service : ");
        } catch (\Exception $e) {
            Log::debug($e);
            throw new Exception($e);
        }
    }

    public function updateQuiz($inputJson)
    {
        Log::debug("Started ... updateQuiz Service : " . json_encode($inputJson));
        try {
            $codes = new Codes;
            $dataValidation = new DataValidation;
            $userSessionDao = new UserSessionDao;
            $usersDao = new UsersDao;
            $imageUtils = new ImageUtils;
            $dateUtils = new DateUtils;
            $quizzesDao = new QuizzesDao;
            $quizzesQuestionsDao = new QuizzesQuestionsDao;
            $quizzesQuestionsOptionsDao = new QuizzesQuestionsOptionsDao;
            $postsDao = new PostsDao;

            //Validations to be done
            //Id validation
            if (!isset($inputJson['quiz_id']) || $dataValidation->isEmpty($inputJson['quiz_id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your quiz ID",
                    'data' => null
                ];
                return $response;
            }
            //Title validation
            if (!isset($inputJson['title']) || $dataValidation->isEmpty($inputJson['title'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your title",
                    'data' => null
                ];
                return $response;
            }
            //Description validation
            if (!isset($inputJson['description']) || $dataValidation->isEmpty($inputJson['description'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your description",
                    'data' => null
                ];
                return $response;
            }
            //Picture validation
            // if (!isset($inputJson['picture']) || $dataValidation->isEmpty($inputJson['picture'])) {
            //     $response = [
            //         'status' => $codes->RC_ERROR,
            //         'message' => "Please enter your picture",
            //         'data' => null
            //     ];
            //     return $response;
            // }
            //User type validation
            if (!isset($inputJson['user_type']) || $dataValidation->isEmpty($inputJson['user_type'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your user type",
                    'data' => null
                ];
                return $response;
            }
            //Quiz start time validation
            if (!isset($inputJson['start_time']) || $dataValidation->isEmpty($inputJson['start_time'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your user type",
                    'data' => null
                ];
                return $response;
            }
            //Quiz live duration validation
            if (!isset($inputJson['live_duration']) || $dataValidation->isEmpty($inputJson['live_duration'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter live duration",
                    'data' => null
                ];
                return $response;
            }
            //creator validation
            if (!isset($inputJson['creator_id']) || $dataValidation->isEmpty($inputJson['creator_id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter creator",
                    'data' => null
                ];
                return $response;
            }
           

            //uploading images
            if(isset($_FILES["quiz_picture"]['name']) && !empty($_FILES["quiz_picture"]['name'])) {
                $quiz_picture = $this->uploadImage($codes->UPLOADS_DIRECTORY);
            }

            Log::debug("Saving the quiz : ");
            $qryStatus = $quizzesDao->updateQuizzes($inputJson, @$quiz_picture['image'], @$quiz_picture['dimensions']);
            Log::debug("Saved the quiz ");
            if($qryStatus){
                $quizzesQuestionsDao->deleteQuestionsByQuizId($inputJson['quiz_id']);
                //uploading image
                $question_pictures = $this->uploadMultiImage($codes->UPLOADS_DIRECTORY);
                foreach ($inputJson['question'] as $i => $q) {
                    // checking answers options 
                    $answers = [];
                    if (isset($inputJson['answers'][$i]) && !empty($inputJson['answers'][$i])) {
                        $aswArr = explode('#*#', $inputJson['answers'][$i]);
                        foreach ($aswArr as $aswkey => $ans)
                            array_push($answers, ["option_text"=>$ans, "is_correct"=>((isset($inputJson['correct'][$i]) && $aswkey == ($inputJson['correct'][$i]-1))?1:0)]);
                    }
                    // seting array of answers in current format
                    $question = [
                        "question"=>$q,
                        "picture"=>'',
                        "time_alloted"=> @$inputJson['time_alloted'][$i],
                        "options"=> $answers,
                        "image_file_name"=> @$question_pictures[$i]['image'],
                        "image_file_dimensions"=> @$question_pictures[$i]['dimensions'],
                    ];
                    $quizQuestionsId = $quizzesQuestionsDao->saveQuizzesQuestions($question, $inputJson['quiz_id']);

                    if($quizQuestionsId && $question['options'] && count($question['options']) > 0){
                        foreach ($question['options'] as $key => $optionRow) {
                            $quizzesQuestionsOptionsDao->saveQuizzesQuestionsOptions($optionRow, $inputJson['quiz_id'], $quizQuestionsId);
                        }
                    }
                }

                // updating post
                $postsDao->updatePostsTitleByQuizId($inputJson['quiz_id'], $inputJson['title'], $dateUtils->getCurrentDate());
            }


            Log::debug('quiz : ' . json_encode($inputJson['quiz_id']));

            //Send the success response now
            $response = [
                'status' => $codes->RC_SUCCESS,
                'message' => $codes->RM_SUCCESS,
                'data' => $quizzesDao->getQwizByUserAndId($inputJson['quiz_id'], $inputJson['creator_id'], $dateUtils->getCurrentDate())
            ];
            return $response;

            Log::debug("Ended ... quiz Service : ");
        } catch (\Exception $e) {
            Log::debug($e);
            throw new Exception($e);
        }
    }
    public function deleteQuiz($inputJson)
    {
        Log::debug("Started ... deleteQuiz Service : " . json_encode($inputJson));
        try {
            $codes = new Codes;
            $dataValidation = new DataValidation;
            $userSessionDao = new UserSessionDao;
            $usersDao = new UsersDao;
            $dateUtils = new DateUtils;
            $quizzesDao = new QuizzesDao;
            $quizzesQuestionsDao = new QuizzesQuestionsDao;
            $quizzesQuestionsOptionsDao = new QuizzesQuestionsOptionsDao;
            $postsDao = new PostsDao;

            //Validations to be done
            //Id validation
            if (!isset($inputJson['quiz_id']) || $dataValidation->isEmpty($inputJson['quiz_id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your quiz ID",
                    'data' => null
                ];
                return $response;
            }
           
            //creator validation
            if (!isset($inputJson['creator_id']) || $dataValidation->isEmpty($inputJson['creator_id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter creator",
                    'data' => null
                ];
                return $response;
            }

            Log::debug("Deleting the quiz : ");
            $qryStatus = $quizzesDao->deleteQuizzes($inputJson['quiz_id'], $inputJson['creator_id']);
            Log::debug("Deleted the quiz ");
            if($qryStatus){
                // deleting questions an their options
                $quizzesQuestionsDao->deleteQuestionsByQuizId($inputJson['quiz_id']);
                // deleting post
                $postsDao->deletePostsByQuizId($inputJson['quiz_id']);
            }


            Log::debug('quiz : ' . json_encode($inputJson['quiz_id']));

            //Send the success response now
            $response = [
                'status' => $codes->RC_SUCCESS,
                'message' => $codes->RM_SUCCESS,
                'data' => ['deleted'=>true]
            ];
            return $response;

            Log::debug("Ended ... quiz Service : ");
        } catch (\Exception $e) {
            Log::debug($e);
            throw new Exception($e);
        }
    }

    public function getMyQuiz($inputJson)
    {
        Log::debug("Started ... getMyQuiz Service : " . json_encode($inputJson));
        try {
            $codes = new Codes;
            $dataValidation = new DataValidation;
            $quizzesDao = new QuizzesDao;
            $dateUtils = new DateUtils;

            //Validations to be done
            //Id validation
            if (!isset($inputJson['quiz_id']) || $dataValidation->isEmpty($inputJson['quiz_id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your quiz ID",
                    'data' => null
                ];
                return $response;
            }
           
            //creator validation
            if (!isset($inputJson['user_id']) || $dataValidation->isEmpty($inputJson['user_id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter user ID",
                    'data' => null
                ];
                return $response;
            }
            Log::debug("getting the quiz : ");
            $quiz = $quizzesDao->getQwizByUserAndId($inputJson['quiz_id'], $inputJson['user_id'], $dateUtils->getCurrentDate());
            Log::debug("Got the quiz ");

            //Send the success response now
            $response = [
                'status' => $codes->RC_SUCCESS,
                'message' => $codes->RM_SUCCESS,
                'data' => ['quiz'=>empty($quiz)?(object)[]:$quiz]
            ];
            return $response;

            Log::debug("Ended ... quiz Service : ");
        } catch (\Exception $e) {
            Log::debug($e);
            throw new Exception($e);
        }
    }
    public function getQuizzesList($inputJson)
    {
        Log::debug("Started ... getQuizzesList Service : " . json_encode($inputJson));
        try {
            $codes = new Codes;
            $quizzesDao = new QuizzesDao;
            $dateUtils = new DateUtils;
            $dataValidation = new DataValidation;

            //User validation
            if (!isset($inputJson['user_id']) || $dataValidation->isEmpty($inputJson['user_id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your user ID",
                    'data' => null
                ];
                return $response;
            }

            Log::debug("getting the quizzes : ");
            $quizzesList = $quizzesDao->getQuizzesList($inputJson, $dateUtils->getCurrentDate());
            Log::debug("Got the quiz ");

            //Send the success response now
            $response = [
                'status' => $codes->RC_SUCCESS,
                'message' => $codes->RM_SUCCESS,
                'data' => ['quizzesList'=>$quizzesList?$quizzesList:[]]
            ];
            return $response;

            Log::debug("Ended ... quiz Service : ");
        } catch (\Exception $e) {
            Log::debug($e);
            throw new Exception($e);
        }
    }

    public function updateQuizParticipation($inputJson)
    {
        Log::debug("Started ... updateQuizParticipation Service : " . json_encode($inputJson));
        try {
            $codes = new Codes;
            $dataValidation = new DataValidation;
            $userSessionDao = new UserSessionDao;
            $dateUtils = new DateUtils;
            $quizzesDao = new QuizzesDao;

            //Validations to be done
            //Id validation
            if (!isset($inputJson['quiz_id']) || $dataValidation->isEmpty($inputJson['quiz_id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your quiz ID",
                    'data' => null
                ];
                return $response;
            }
            //User validation
            if (!isset($inputJson['user_id']) || $dataValidation->isEmpty($inputJson['user_id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your user ID",
                    'data' => null
                ];
                return $response;
            }
            //User validation
            if (!isset($inputJson['status']) || $dataValidation->isEmpty($inputJson['status'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your status",
                    'data' => null
                ];
                return $response;
            }
           
            Log::debug("Saving the Participation : ");
            $qryResponse = $quizzesDao->addRemoveQuizParticipation($inputJson, $dateUtils->getCurrentDate());
            Log::debug("Saved the Participation ");

            Log::debug('quiz : ' . json_encode($inputJson['quiz_id']));

            if($qryResponse["valid"]){
                //Send the success response now
                $response = [
                    'status' => $codes->RC_SUCCESS,
                    'message' => $codes->RM_SUCCESS,
                    'data' => [(($inputJson['status'] == 1)?'participated':'deleted')=>true]
                ];
            }else{                
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => $qryResponse["msg"],
                    'data' => null
                ];
            }
            return $response;

            Log::debug("Ended ... quiz Service : ");
        } catch (\Exception $e) {
            Log::debug($e);
            throw new Exception($e);
        }
    }

    public function updateQuizLastSeenQuestion($inputJson)
    {
        Log::debug("Started ... updateQuizLastSeenQuestion Service : " . json_encode($inputJson));
        try {
            $codes = new Codes;
            $dataValidation = new DataValidation;
            $quizzesDao = new QuizzesDao;
            $dateUtils = new DateUtils;

            //Validations to be done
            //Id validation
            if (!isset($inputJson['quiz_id']) || $dataValidation->isEmpty($inputJson['quiz_id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your quiz ID",
                    'data' => null
                ];
                return $response;
            }
            //User validation
            if (!isset($inputJson['user_id']) || $dataValidation->isEmpty($inputJson['user_id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your user ID",
                    'data' => null
                ];
                return $response;
            }
            //quiestion validation
            if (!isset($inputJson['question_id']) || $dataValidation->isEmpty($inputJson['question_id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your question ID",
                    'data' => null
                ];
                return $response;
            }
                       
            Log::debug("Saving the Participation Last question : ");
            $qryResponse = $quizzesDao->updateQuizLastSeenQuestion($inputJson, $dateUtils->getCurrentDate());
            Log::debug("Saved the Participation Last Question ");

            Log::debug('quiz : ' . json_encode($inputJson['quiz_id']));
            if($qryResponse["valid"]){
                //Send the success response now
                $response = [
                    'status' => $codes->RC_SUCCESS,
                    'message' => $codes->RM_SUCCESS,
                    'data' => null
                ];
            }else{                
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => $qryResponse["msg"],
                    'data' => null
                ];
            }
            return $response;

            Log::debug("Ended ... quiz updateQuizLastSeenQuestion Service : ");
        } catch (\Exception $e) {
            Log::debug($e);
            throw new Exception($e);
        }
    }

    public function addQuizParticipantsAnswer($inputJson)
    {
        Log::debug("Started ... addQuizParticipantsAnswer Service : " . json_encode($inputJson));
        try {
            $codes = new Codes;
            $dataValidation = new DataValidation;
            $userSessionDao = new UserSessionDao;
            $dateUtils = new DateUtils;
            $quizzesDao = new QuizzesDao;
            $dateUtils = new DateUtils;

            //Validations to be done
            //Id validation
            if (!isset($inputJson['quiz_id']) || $dataValidation->isEmpty($inputJson['quiz_id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your quiz ID",
                    'data' => null
                ];
                return $response;
            }
            //User validation
            if (!isset($inputJson['user_id']) || $dataValidation->isEmpty($inputJson['user_id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your user ID",
                    'data' => null
                ];
                return $response;
            }
            //quiestion validation
            if (!isset($inputJson['question_id']) || $dataValidation->isEmpty($inputJson['question_id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your question ID",
                    'data' => null
                ];
                return $response;
            }
            //option validation
            if (!isset($inputJson['option_id']) || $dataValidation->isEmpty($inputJson['option_id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your option ID",
                    'data' => null
                ];
                return $response;
            }
            //time left validation
            if (!isset($inputJson['time_left'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your time left",
                    'data' => null
                ];
                return $response;
            }
           
            Log::debug("Saving the Participation Answer : ");
            $qryResponse = $quizzesDao->sendQuizParticipationAnswer($inputJson, $dateUtils->getCurrentDate());
            Log::debug("Saved the Participation Answer ");

            Log::debug('quiz : ' . json_encode($inputJson['quiz_id']));
            if($qryResponse["valid"]){
                //Send the success response now
                $response = [
                    'status' => $codes->RC_SUCCESS,
                    'message' => $codes->RM_SUCCESS,
                    'data' => $qryResponse["data"]
                ];
            }else{                
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => $qryResponse["msg"],
                    'data' => null
                ];
            }
            return $response;

            Log::debug("Ended ... quiz Service : ");
        } catch (\Exception $e) {
            Log::debug($e);
            throw new Exception($e);
        }
    }

    public function calculateQuizScore($inputJson)
    {
        Log::debug("Started ... calculateQuizScore Service : " . json_encode($inputJson));
        try {
            $codes = new Codes;
            $dataValidation = new DataValidation;
            $userSessionDao = new UserSessionDao;
            $dateUtils = new DateUtils;
            $quizzesDao = new QuizzesDao;
            $dateUtils = new DateUtils;

            //Validations to be done
            //Id validation
            if (!isset($inputJson['quiz_id']) || $dataValidation->isEmpty($inputJson['quiz_id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your quiz ID",
                    'data' => null
                ];
                return $response;
            }
            //User validation
            if (!isset($inputJson['user_id']) || $dataValidation->isEmpty($inputJson['user_id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your user ID",
                    'data' => null
                ];
                return $response;
            }
            
            Log::debug("Saving the Participation Answer : ");
            $scoreData = $quizzesDao->getQuizScoreCalculation($inputJson, $dateUtils->getCurrentDate());
            Log::debug("Saved the Participation Answer ");

            Log::debug('quiz : ' . json_encode($inputJson['quiz_id']));

            //Send the success response now
            $response = [
                'status' => $codes->RC_SUCCESS,
                'message' => $codes->RM_SUCCESS,
                'data' => ['score'=>@$scoreData['score'], 'correctlyAnsweredQuestions'=>@$scoreData['correctlyAnsweredQuestions']]
            ];
            return $response;

            Log::debug("Ended ... calculateQuizScore Service: ");
        } catch (\Exception $e) {
            Log::debug($e);
            throw new Exception($e);
        }
    }
    public function inviteUserForQuiz($inputJson)
    {
        Log::debug("Started ... inviteUserForQuiz Service : " . json_encode($inputJson));
        try {
            $codes = new Codes;
            $dataValidation = new DataValidation;
            $userSessionDao = new UserSessionDao;
            $dateUtils = new DateUtils;
            $quizzesDao = new QuizzesDao;
            $dateUtils = new DateUtils;

            //Validations to be done
            //Id validation
            if (!isset($inputJson['quiz_id']) || $dataValidation->isEmpty($inputJson['quiz_id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your quiz ID",
                    'data' => null
                ];
                return $response;
            }
            //User validation
            if (!isset($inputJson['user_id']) || $dataValidation->isEmpty($inputJson['user_id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your user ID",
                    'data' => null
                ];
                return $response;
            }
            //Sender User validation
            if (!isset($inputJson['sender_user_id']) || $dataValidation->isEmpty($inputJson['sender_user_id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your sender user ID",
                    'data' => null
                ];
                return $response;
            }
            
            Log::debug("Saving the invitation : ");
            $qryResponse = $quizzesDao->sendQuizInvitaion($inputJson, $dateUtils->getCurrentDate());
            Log::debug("Saved the invitation ");

            Log::debug('quiz : ' . json_encode($inputJson['quiz_id']));

            if($qryResponse["valid"]){
                //Send the success response now
                $response = [
                    'status' => $codes->RC_SUCCESS,
                    'message' => $codes->RM_SUCCESS,
                    'data' => null
                ];
            }else{                
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => $qryResponse["msg"],
                    'data' => null
                ];
            }
            return $response;

            Log::debug("Ended ... inviteUserForQuiz Service: ");
        } catch (\Exception $e) {
            Log::debug($e);
            throw new Exception($e);
        }
    }
    public function getInvitedUserListForQuiz($inputJson)
    {
        Log::debug("Started ... getInvitedUserListForQuiz Service : " . json_encode($inputJson));
        try {
            $codes = new Codes;
            $dataValidation = new DataValidation;
            $userSessionDao = new UserSessionDao;
            $quizzesDao = new QuizzesDao;

            //Validations to be done
            //Id validation
            if (!isset($inputJson['quiz_id']) || $dataValidation->isEmpty($inputJson['quiz_id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your quiz ID",
                    'data' => null
                ];
                return $response;
            }
            //User validation
            if (!isset($inputJson['user_id']) || $dataValidation->isEmpty($inputJson['user_id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your user ID",
                    'data' => null
                ];
                return $response;
            }
            
            Log::debug("Searching the invitations : ");
            $list = $quizzesDao->getQuizInvitedUserList($inputJson);
            Log::debug("Sending the invitations ");

            Log::debug('quiz : ' . json_encode($inputJson['quiz_id']));

            //Send the success response now
            $response = [
                'status' => $codes->RC_SUCCESS,
                'message' => $codes->RM_SUCCESS,
                'data' => ($list)?$list:[]
            ];
            return $response;

            Log::debug("Ended ... getInvitedUserListForQuiz Service: ");
        } catch (\Exception $e) {
            Log::debug($e);
            throw new Exception($e);
        }
    }

}
