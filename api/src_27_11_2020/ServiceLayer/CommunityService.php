<?php

namespace App\ServiceLayer;

use App\Utils\Codes;
use App\Utils\DataValidation;
use Cake\Core\Exception\Exception;
use App\DaoLayer\UsersDao;
use App\Utils\DateUtils;
use Cake\Log\Log;
use App\DaoLayer\CommunityDao;
use App\DaoLayer\PostsDao;

class CommunityService
{

    private function uploadImage($FILE_NAME, $UPLOADS_DIRECTORY){

        if(isset($_FILES[$FILE_NAME]['tmp_name']) && !empty($_FILES[$FILE_NAME]['tmp_name'])) {
            $folder = 'photos';
            $depth = '../';
            if(!file_exists($depth.$UPLOADS_DIRECTORY.'/'.$folder)) {
                @mkdir($depth.$UPLOADS_DIRECTORY.'/'.$folder, 0777, true);
            }
            if(!file_exists($depth.$UPLOADS_DIRECTORY.'/'.$folder.'/' . date('Y'))) {
                @mkdir($depth.$UPLOADS_DIRECTORY.'/'.$folder.'/' . date('Y'), 0777, true);
            }
            if(!file_exists($UPLOADS_DIRECTORY.'/'.$folder.'/' . date('Y') . '/' . date('m'))) {
                @mkdir($depth.$UPLOADS_DIRECTORY.'/'.$folder.'/' . date('Y') . '/' . date('m'), 0777, true);
            }

            // valid inputs
            if($_FILES[$FILE_NAME]["error"] != UPLOAD_ERR_OK) {
                throw new Exception("Something wrong with upload! Is 'upload_max_filesize' set correctly?");
            }
            $directory = $folder.'/'. date('Y') . '/' . date('m') . '/';
            $prefix = md5(time()*rand(1, 9999));
            $imgArr = explode('.', $_FILES[$FILE_NAME]['name']);
            $extension = end($imgArr);
            $get_dimensions = getimagesize($_FILES[$FILE_NAME]['tmp_name']);
            $dimensions = $get_dimensions[1].'X'.$get_dimensions[0];
                
            $image_new_name = $directory.$prefix.'.'.$extension;
            $path_new = $depth.$UPLOADS_DIRECTORY.'/'.$image_new_name;

            /* check if the file uploaded successfully */
            if(!@move_uploaded_file($_FILES[$FILE_NAME]['tmp_name'], $path_new)) {
                throw new Exception("Sorry, can not upload the photos");
            }
        }
                    
            /* return */
            $return_files['image'] = @$image_new_name;
            $return_files['dimensions'] = @$dimensions;
            return $return_files;
    }


    public function createCommunity($inputJson)
    {
        Log::debug("Started ... createCommunity Service : " . json_encode($inputJson));
        try {
            $codes = new Codes;
            $dataValidation = new DataValidation;
            $dateUtils = new DateUtils;
            $communityDao = new CommunityDao;

            //Validations to be done
            //Title validation
            if (!isset($inputJson['title']) || $dataValidation->isEmpty($inputJson['title'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your title",
                    'data' => null
                ];
                return $response;
            }
            //Description validation
            if (!isset($inputJson['description']) || $dataValidation->isEmpty($inputJson['description'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your description",
                    'data' => null
                ];
                return $response;
            }
            //User type validation
            if (!isset($inputJson['user_type']) || $dataValidation->isEmpty($inputJson['user_type'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your user type",
                    'data' => null
                ];
                return $response;
            }
            //Community start time validation
            if (!isset($inputJson['bio']) || $dataValidation->isEmpty($inputJson['bio'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your bio",
                    'data' => null
                ];
                return $response;
            }
            //creator validation
            if (!isset($inputJson['creator_id']) || $dataValidation->isEmpty($inputJson['creator_id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter creator",
                    'data' => null
                ];
                return $response;
            }
           
            //uploading images
            $community_picture = $this->uploadImage('community_picture', $codes->UPLOADS_DIRECTORY);
            //uploading cou=ver images
            $cover_picture = $this->uploadImage('cover_picture', $codes->UPLOADS_DIRECTORY);

            if(!isset($community_picture['image']) || empty($community_picture['image'])) {
                throw new Exception("Community picture is required");
            }

            Log::debug("Saving the community : ");
            $communityId = $communityDao->saveCommunity($inputJson, $community_picture, $cover_picture);
            Log::debug("Saved the community ");


            Log::debug('community : ' . json_encode($communityId));

            //Send the success response now
            $response = [
                'status' => $codes->RC_SUCCESS,
                'message' => $codes->RM_SUCCESS,
                'data' => $communityDao->getCommunityByUserAndId($communityId, $inputJson['creator_id'], $dateUtils->getCurrentDate())
            ];
            return $response;

            Log::debug("Ended ... community Service : ");
        } catch (\Exception $e) {
            Log::debug($e);
            throw new Exception($e);
        }
    }

    public function updateCommunity($inputJson)
    {
        Log::debug("Started ... updateCommunity Service : " . json_encode($inputJson));
        try {
            $codes = new Codes;
            $dataValidation = new DataValidation;
            $dateUtils = new DateUtils;
            $communityDao = new CommunityDao;

            //Validations to be done
            //Id validation
            if (!isset($inputJson['community_id']) || $dataValidation->isEmpty($inputJson['community_id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your community ID",
                    'data' => null
                ];
                return $response;
            }
            //Title validation
            if (!isset($inputJson['title']) || $dataValidation->isEmpty($inputJson['title'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your title",
                    'data' => null
                ];
                return $response;
            }
            //Description validation
            if (!isset($inputJson['description']) || $dataValidation->isEmpty($inputJson['description'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your description",
                    'data' => null
                ];
                return $response;
            }
            //User type validation
            if (!isset($inputJson['user_type']) || $dataValidation->isEmpty($inputJson['user_type'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your user type",
                    'data' => null
                ];
                return $response;
            }
            //Community start time validation
            if (!isset($inputJson['bio']) || $dataValidation->isEmpty($inputJson['bio'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your bio",
                    'data' => null
                ];
                return $response;
            }
            //creator validation
            if (!isset($inputJson['creator_id']) || $dataValidation->isEmpty($inputJson['creator_id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter creator",
                    'data' => null
                ];
                return $response;
            }

            //uploading images
            $community_picture = $this->uploadImage('community_picture', $codes->UPLOADS_DIRECTORY);
            //uploading cou=ver images
            $cover_picture = $this->uploadImage('cover_picture', $codes->UPLOADS_DIRECTORY);
           
            Log::debug("Saving the community : ");
            $qryStatus = $communityDao->updateCommunity($inputJson, $community_picture, $cover_picture);
            Log::debug("Saved the community ");

            Log::debug('community : ' . json_encode($inputJson['community_id']));

            //Send the success response now
            $response = [
                'status' => $codes->RC_SUCCESS,
                'message' => $codes->RM_SUCCESS,
                'data' => $communityDao->getCommunityByUserAndId($inputJson['community_id'], $inputJson['creator_id'], $dateUtils->getCurrentDate())
            ];
            return $response;

            Log::debug("Ended ... community Service : ");
        } catch (\Exception $e) {
            Log::debug($e);
            throw new Exception($e);
        }
    }
    public function deleteCommunity($inputJson)
    {
        Log::debug("Started ... deleteCommunity Service : " . json_encode($inputJson));
        try {
            $codes = new Codes;
            $dataValidation = new DataValidation;
            $dateUtils = new DateUtils;
            $communityDao = new CommunityDao;

            //Validations to be done
            //Id validation
            if (!isset($inputJson['community_id']) || $dataValidation->isEmpty($inputJson['community_id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your community ID",
                    'data' => null
                ];
                return $response;
            }
           
            //creator validation
            if (!isset($inputJson['creator_id']) || $dataValidation->isEmpty($inputJson['creator_id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter creator",
                    'data' => null
                ];
                return $response;
            }

            Log::debug("Deleting the community : ");
            $qryStatus = $communityDao->deleteCommunity($inputJson['community_id'], $inputJson['creator_id']);
            Log::debug("Deleted the community ");
            // if($qryStatus){
            //     // deleting post
            //     $postsDao->deletePostsByCommunityId($inputJson['community_id']);
            // }


            Log::debug('community : ' . json_encode($inputJson['community_id']));

            //Send the success response now
            $response = [
                'status' => $codes->RC_SUCCESS,
                'message' => $codes->RM_SUCCESS,
                'data' => ['deleted'=>true]
            ];
            return $response;

            Log::debug("Ended ... community Service : ");
        } catch (\Exception $e) {
            Log::debug($e);
            throw new Exception($e);
        }
    }

    public function getMyCommunity($inputJson)
    {
        Log::debug("Started ... getMyCommunity Service : " . json_encode($inputJson));
        try {
            $codes = new Codes;
            $dataValidation = new DataValidation;
            $communityDao = new CommunityDao;
            $dateUtils = new DateUtils;

            //Validations to be done
            //Id validation
            if (!isset($inputJson['community_id']) || $dataValidation->isEmpty($inputJson['community_id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your community ID",
                    'data' => null
                ];
                return $response;
            }
           
            //creator validation
            if (!isset($inputJson['user_id']) || $dataValidation->isEmpty($inputJson['user_id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter user ID",
                    'data' => null
                ];
                return $response;
            }
            Log::debug("getting the community : ");
            $community = $communityDao->getCommunityByUserAndId($inputJson['community_id'], $inputJson['user_id'], $dateUtils->getCurrentDate());
            Log::debug("Got the community ");

            //Send the success response now
            $response = [
                'status' => $codes->RC_SUCCESS,
                'message' => $codes->RM_SUCCESS,
                'data' => empty($community)?(object)[]:$community
            ];
            return $response;

            Log::debug("Ended ... community Service : ");
        } catch (\Exception $e) {
            Log::debug($e);
            throw new Exception($e);
        }
    }
    public function getCommunityList($inputJson)
    {
        Log::debug("Started ... getCommunityList Service : " . json_encode($inputJson));
        try {
            $codes = new Codes;
            $communityDao = new CommunityDao;
            $dateUtils = new DateUtils;
            $dataValidation = new DataValidation;

            //User validation
            if (!isset($inputJson['user_id']) || $dataValidation->isEmpty($inputJson['user_id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your user ID",
                    'data' => null
                ];
                return $response;
            }

            Log::debug("getting the communitys : ");
            $communitysList = $communityDao->getCommunityList($inputJson, $dateUtils->getCurrentDate());
            Log::debug("Got the community ");

            //Send the success response now
            $response = [
                'status' => $codes->RC_SUCCESS,
                'message' => $codes->RM_SUCCESS,
                'data' => $communitysList?$communitysList:[]
            ];
            return $response;

            Log::debug("Ended ... community Service : ");
        } catch (\Exception $e) {
            Log::debug($e);
            throw new Exception($e);
        }
    }

    public function addRemoveCommunityParticipation($inputJson)
    {
        Log::debug("Started ... addRemoveCommunityParticipation Service : " . json_encode($inputJson));
        try {
            $codes = new Codes;
            $dataValidation = new DataValidation;
            $dateUtils = new DateUtils;
            $communityDao = new CommunityDao;

            //Validations to be done
            //Id validation
            if (!isset($inputJson['community_id']) || $dataValidation->isEmpty($inputJson['community_id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your community ID",
                    'data' => null
                ];
                return $response;
            }
            //User validation
            if (!isset($inputJson['user_id']) || $dataValidation->isEmpty($inputJson['user_id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your user ID",
                    'data' => null
                ];
                return $response;
            }
            //status validation
            if (!isset($inputJson['status']) || $dataValidation->isEmpty($inputJson['status'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your status",
                    'data' => null
                ];
                return $response;
            }
           
            Log::debug("Saving the Participation : ");
            $qryResponse = $communityDao->addRemoveCommunityParticipation($inputJson, $dateUtils->getCurrentDate());
            Log::debug("Saved the Participation ");

            Log::debug('community : ' . json_encode($inputJson['community_id']));

            if($qryResponse["valid"]){
                //Send the success response now
                $response = [
                    'status' => $codes->RC_SUCCESS,
                    'message' => $codes->RM_SUCCESS,
                    'data' => [(($inputJson['status'] == 1)?'participated':'deleted')=>true]
                ];
            }else{                
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => $qryResponse["msg"],
                    'data' => null
                ];
            }
            return $response;

            Log::debug("Ended ... community Service : ");
        } catch (\Exception $e) {
            Log::debug($e);
            throw new Exception($e);
        }
    }

    public function promoteDemoteCommunityUser($inputJson)
    {
        Log::debug("Started ... promoteDemoteCommunityUser Service : " . json_encode($inputJson));
        try {
            $codes = new Codes;
            $dataValidation = new DataValidation;
            $dateUtils = new DateUtils;
            $communityDao = new CommunityDao;

            //Validations to be done
            //Id validation
            if (!isset($inputJson['community_id']) || $dataValidation->isEmpty($inputJson['community_id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your community ID",
                    'data' => null
                ];
                return $response;
            }
            //Member validation
            if (!isset($inputJson['member_id']) || $dataValidation->isEmpty($inputJson['member_id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your member ID",
                    'data' => null
                ];
                return $response;
            }
            //User validation
            if (!isset($inputJson['user_id']) || $dataValidation->isEmpty($inputJson['user_id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your user ID",
                    'data' => null
                ];
                return $response;
            }
            //status validation
            if (!isset($inputJson['status']) || $dataValidation->isEmpty($inputJson['status']) || !in_array($inputJson['status'], [1,2,3,4])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your status",
                    'data' => null
                ];
                return $response;
            }
           
            Log::debug("Saving the Participation : ");
            $qryResponse = $communityDao->promoteDemoteCommunityUser($inputJson, $dateUtils->getCurrentDate());
            Log::debug("Saved the Participation ");

            Log::debug('community : ' . json_encode($inputJson['community_id']));

            if($qryResponse["valid"]){
                //Send the success response now
                $response = [
                    'status' => $codes->RC_SUCCESS,
                    'message' => $codes->RM_SUCCESS,
                    'data' => [(($inputJson['status'] == 1)?'Admin':($inputJson['status'] == 2 ? 'Moderator':($inputJson['status'] == 3 ? 'Member':($inputJson['status'] == 4 ? 'Removed':'Status'))))=>true]
                ];
            }else{                
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => $qryResponse["msg"],
                    'data' => null
                ];
            }
            return $response;

            Log::debug("Ended ... community Service : ");
        } catch (\Exception $e) {
            Log::debug($e);
            throw new Exception($e);
        }
    }

    public function inviteUserForCommunity($inputJson)
    {
        Log::debug("Started ... inviteUserForCommunity Service : " . json_encode($inputJson));
        try {
            $codes = new Codes;
            $dataValidation = new DataValidation;
            $dateUtils = new DateUtils;
            $communityDao = new CommunityDao;
            $dateUtils = new DateUtils;

            //Validations to be done
            //Id validation
            if (!isset($inputJson['community_id']) || $dataValidation->isEmpty($inputJson['community_id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your community ID",
                    'data' => null
                ];
                return $response;
            }
            //User validation
            if (!isset($inputJson['user_id']) || $dataValidation->isEmpty($inputJson['user_id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your user ID",
                    'data' => null
                ];
                return $response;
            }
            //Sender User validation
            if (!isset($inputJson['sender_user_id']) || $dataValidation->isEmpty($inputJson['sender_user_id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your sender user ID",
                    'data' => null
                ];
                return $response;
            }
            
            Log::debug("Saving the invitation : ");
            $qryResponse = $communityDao->sendCommunityInvitaion($inputJson, $dateUtils->getCurrentDate());
            Log::debug("Saved the invitation ");

            Log::debug('community : ' . json_encode($inputJson['community_id']));

            if($qryResponse["valid"]){
                //Send the success response now
                $response = [
                    'status' => $codes->RC_SUCCESS,
                    'message' => $codes->RM_SUCCESS,
                    'data' => null
                ];
            }else{                
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => $qryResponse["msg"],
                    'data' => null
                ];
            }
            return $response;

            Log::debug("Ended ... inviteUserForCommunity Service: ");
        } catch (\Exception $e) {
            Log::debug($e);
            throw new Exception($e);
        }
    }
    public function getInvitedUserListForCommunity($inputJson)
    {
        Log::debug("Started ... getInvitedUserListForCommunity Service : " . json_encode($inputJson));
        try {
            $codes = new Codes;
            $dataValidation = new DataValidation;
            $communityDao = new CommunityDao;

            //Validations to be done
            //Id validation
            if (!isset($inputJson['community_id']) || $dataValidation->isEmpty($inputJson['community_id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your community ID",
                    'data' => null
                ];
                return $response;
            }
            //User validation
            if (!isset($inputJson['user_id']) || $dataValidation->isEmpty($inputJson['user_id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your user ID",
                    'data' => null
                ];
                return $response;
            }
            
            Log::debug("Searching the invitations : ");
            $list = $communityDao->getCommunityInvitedUserList($inputJson);
            Log::debug("Sending the invitations ");

            Log::debug('community : ' . json_encode($inputJson['community_id']));

            //Send the success response now
            $response = [
                'status' => $codes->RC_SUCCESS,
                'message' => $codes->RM_SUCCESS,
                'data' => ($list)?$list:[]
            ];
            return $response;

            Log::debug("Ended ... getInvitedUserListForCommunity Service: ");
        } catch (\Exception $e) {
            Log::debug($e);
            throw new Exception($e);
        }
    }


}
