<?php

namespace App\DaoLayer;

use App\Utils\Codes;
use Cake\Core\Exception\Exception;
use Cake\Datasource\ConnectionManager;
use Cake\Log\Log;

class QuizzesQuestionsDao
{


    
    /**
     * Save Quiz Question
     */
    public function saveQuizzesQuestions($args, $quiz_id)
    {
        
        try{
            $conn = ConnectionManager::get('default');

            $imgQry1 = ($args['image_file_name'])?", `picture`, `picture_dimensions`":"";
            $imgQry2 = ($args['image_file_name'])?", '".$args['image_file_name']."', '".$args['image_file_dimensions']."'":"";
            Log::debug("Started ...saveQuizQuestion Dao");
            $sql=sprintf("INSERT INTO `quizzes_questions`(`quiz_id`, `question`, `time_alloted` $imgQry1) VALUES ('%s', '%s', '%s' $imgQry2)", 
            $quiz_id, $args['question'], $args['time_alloted']);

            Log::debug("SQL : ".$sql);

            $conn->execute($sql);
            
            $sql="SELECT LAST_INSERT_ID()";
            
            $stmt = $conn->execute($sql);
            
            $res = $stmt->fetch();
           
            Log::debug("Ended ...saveQuizQuestion Dao");
            
            return $res[0];
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }

    public function deleteQuestionsByQuizId($quiz_id)
    {
        Log::debug("Started ...deleteQuestionsByQuizId Dao");

        try{
            
            $conn = ConnectionManager::get('default');
            //deleting questions
            $sql=sprintf("DELETE FROM `quizzes_questions` WHERE quiz_id = '%s'", 
            $quiz_id);
            Log::debug("SQL : ".$sql);
            $conn->execute($sql);

            //deleting questions options
            $sql=sprintf("DELETE FROM `quizzes_questions_options` WHERE quiz_id = '%s'", 
            $quiz_id);
            Log::debug("SQL : ".$sql);
            $conn->execute($sql);

            Log::debug("Ended ...deleteQuestionsByQuizId Dao");
            
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }


}