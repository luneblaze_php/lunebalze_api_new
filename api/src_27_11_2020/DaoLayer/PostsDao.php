<?php

namespace App\DaoLayer;

use App\Utils\Codes;
use Cake\Core\Exception\Exception;
use Cake\Datasource\ConnectionManager;
use Cake\Log\Log;

class PostsDao
{

    /**
     * 
     */
    public function getPostDetailByPostIdPostType($postId,$postType)
    {
        Log::debug("Started ...getPostDetailByPostIdPostType Dao : Post Id : ".$postId.", Post Type : ".$postType);

        try{

            $conn = ConnectionManager::get('default');

            $sql = sprintf("SELECT posts.*, users.user_name,users.privacy_connection_request,users.user_work_title, 
            users.user_fullname, users.user_gender, users.user_picture, 
            users.user_picture_id, users.user_cover_id, users.user_verified, 
            users.user_subscribed, users.user_pinned_post, pages.*, 
            groups.group_name, groups.group_picture_id, 
            groups.group_cover_id, groups.group_title, 
            groups.group_admin, groups.group_pinned_post 
            FROM posts LEFT JOIN users ON posts.user_id = users.user_id 
            AND posts.user_type='%s' LEFT JOIN pages ON posts.user_id = pages.page_id 
            AND posts.user_type = 'page' LEFT JOIN groups ON posts.in_group = '1' 
            AND posts.group_id = groups.group_id WHERE NOT (users.user_name <=> NULL 
            AND pages.page_name <=> NULL) AND posts.post_id = '%s'",$postType,$postId);

            Log::debug("SQL : ".$sql);

            $stmt = $conn->execute($sql);

            $result = $stmt->fetch("assoc");

            Log::debug("Ended ...getPostDetailByPostIdPostType Dao");

            return $result;
            
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }

    }



    /**
     * 
     */
    public function getPostsByOrganizationIdUserTypePaymentFlag($organizationId, $userType, $paymentFlag, $offset)
    {
        Log::debug("Started ...getPostsByOrganizationIdUserTypePaymentFlag Dao : Organization Id : ".$organizationId.", User Type : ".$userType.", Payment Flag : ".$paymentFlag);

        try{

            $codes = new Codes;
            
            $conn = ConnectionManager::get('default');

            if(isset($paymentFlag))
                $sql=sprintf("SELECT * FROM `posts` WHERE `organisation_id`='%s' and user_type='%s' and payment=%s order by post_id desc limit %s,%s",$organizationId,$userType,$paymentFlag,$offset,$codes->MAX_RESULTS);          
            else 
                $sql=sprintf("SELECT * FROM `posts` WHERE `organisation_id`='%s' and user_type='%s' order by post_id desc limit %s,%s",$organizationId,$userType,$offset,$codes->MAX_RESULTS);          

            Log::debug("SQL : ".$sql);

            $stmt = $conn->execute($sql);

            $results = array();

            while($result = $stmt->fetch("assoc"))
                array_push($results,$result);

            Log::debug("Ended ...getPostsByOrganizationIdUserTypePaymentFlag Dao");

            return $results;
            
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }

    }


    
	/**
	 * Save Posts
	 */
	public function savePosts($userId, $articlesId, $date, $discussionPost, $article, $commentId)
	{
		Log::debug("Started ...savePosts Dao : User Id : ".$userId.", Total User Ids : ".$userId.", Origin Id : ".$articlesId.", User Type : user, Post Type : article_comment".", Time : ".$date. ", Privacy : public".", Text :".$discussionPost.", Post Type Value : ".$article.", Updated At : ".$date.", Comments : 1".", Comment Ids : ".$commentId.", Total : 1");

		try{
			
			$conn = ConnectionManager::get('default');

			$sql=sprintf("INSERT INTO posts (user_id, total_user_ids, origin_id, user_type, post_type, time, privacy, text, post_type_value,updated_at,comments,comment_ids,total) VALUES (%s, %s, %s, 'user', 'article_comment', %s, 'public', %s, %s,%s,1,%s,1)", $userId, $userId, $articlesId, $date, $discussionPost, $article,$date,$commentId);
			
			Log::debug("SQL : ".$sql);

			$conn->execute($sql);

			Log::debug("Ended ...savePosts Dao");

		}catch(\Exception $e){
			Log::debug($e);
			throw new Exception($e);
		}
	}
    
    /**
     * Save Posts from quiz
     */
    public function saveQuizPosts($quiz_id, $user_id, $post_title, $date)
    {
        Log::debug("Started ...savePosts Dao ");
        try{
            
            $conn = ConnectionManager::get('default');

            $sql=sprintf("INSERT INTO posts (user_id, post_title, quiz_id, post_type, privacy, text_tagged, origin_type, organisation_id, payment,total,updated_at, time) VALUES (%s, '%s', %s, 'quiz', 'public', '', '', 0, 0, 0, '%s', '%s')", $user_id, $post_title, $quiz_id, $date, $date);
            
            Log::debug("SQL : ".$sql);

            $conn->execute($sql);

            Log::debug("Ended ...savePosts Dao");

        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }

    public function updatePostsTitleByQuizId($quiz_id, $post_title, $date)
    {
        Log::debug("Started ...updatePostsTitleByQuizId Dao");

        try{

            $conn = ConnectionManager::get('default');

            $sql=sprintf("UPDATE posts SET post_title = '%s', updated_at = '%s'
                WHERE quiz_id = '%s'", $post_title, $date, $quiz_id);

            Log::debug("SQL : ".$sql);

            $conn->execute($sql);
           
            Log::debug("Ended ...updatePostsTitleByQuizId Dao");
            
            return 1;
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }
    /**
     * Delete Quiz
     */
    public function deletePostsByQuizId($quiz_id)
    {
        Log::debug("Started ...deletePostsByQuizId Dao");

        try{

            $conn = ConnectionManager::get('default');

            //deleting questions
            $sql=sprintf("DELETE FROM `posts` WHERE quiz_id = '%s'", 
            $quiz_id);
            Log::debug("SQL : ".$sql);
            $conn->execute($sql);
           
            Log::debug("Ended ...deletePostsByQuizId Dao");
            
            return 1;
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }

        /**
     * Save Posts from debate
     */
    public function saveDebatePosts($debate_id, $user_id, $post_title, $date)
    {
        Log::debug("Started ...savePosts Dao ");
        try{
            
            $conn = ConnectionManager::get('default');

            $sql=sprintf("INSERT INTO posts (user_id, post_title, debate_id, post_type, privacy, text_tagged, origin_type, organisation_id, payment,total,updated_at, time) VALUES (%s, '%s', %s, 'debate', 'public', '', '', 0, 0, 0, '%s', '%s')", $user_id, $post_title, $debate_id, $date, $date);
            
            Log::debug("SQL : ".$sql);

            $conn->execute($sql);

            Log::debug("Ended ...savePosts Dao");

        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }
    public function updatePostsTitleByDebateId($debate_id, $post_title, $date)
    {
        Log::debug("Started ...updatePostsTitleByDebateId Dao");

        try{

            $conn = ConnectionManager::get('default');

            $sql=sprintf("UPDATE posts SET post_title = '%s', updated_at = '%s'
                WHERE debate_id = '%s'", $post_title, $date, $debate_id);

            Log::debug("SQL : ".$sql);

            $conn->execute($sql);
           
            Log::debug("Ended ...updatePostsTitleByDebateId Dao");
            
            return 1;
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }
    /**
     * Delete Debate
     */
    public function deletePostsByDebateId($debate_id)
    {
        Log::debug("Started ...deletePostsByDebateId Dao");

        try{

            $conn = ConnectionManager::get('default');

            //deleting questions
            $sql=sprintf("DELETE FROM `posts` WHERE debate_id = '%s'", 
            $debate_id);
            Log::debug("SQL : ".$sql);
            $conn->execute($sql);
           
            Log::debug("Ended ...deletePostsByDebateId Dao");
            
            return 1;
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }



    
}