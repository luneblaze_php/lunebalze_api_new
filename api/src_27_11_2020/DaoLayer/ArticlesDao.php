<?php

namespace App\DaoLayer;

use App\Utils\Codes;
use Cake\Core\Exception\Exception;
use Cake\Datasource\ConnectionManager;
use Cake\Log\Log;

class ArticlesDao
{

	/**
	 * Get Articles By articles id
	 */
	public function getArticlesByArticlesId($articlesId)
	{
		Log::debug("Started ...getArticlesByArticlesId Dao : Id : ".$articlesId);

		try{
			
			$conn = ConnectionManager::get('default');

			$sql=sprintf("SELECT * FROM `articles` WHERE `articles_id` = %s ", $articlesId);
			
			Log::debug("SQL : ".$sql);

			$stmt = $conn->execute($sql);

			$result = $stmt->fetch("assoc");

			Log::debug("Ended ...getArticlesByArticlesId Dao");

			return $result;

		}catch(\Exception $e){
			Log::debug($e);
			throw new Exception($e);
		}
	}
}
