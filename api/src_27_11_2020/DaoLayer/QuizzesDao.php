<?php

namespace App\DaoLayer;

use App\Utils\Codes;
use Cake\Core\Exception\Exception;
use Cake\Datasource\ConnectionManager;
use Cake\Log\Log;

class QuizzesDao
{

    private function getImgFullPath($img = '', $newPath = 0){
        $codes = new Codes;
        return !empty($img) ?$codes->SYSTEM_URL.'/'.($newPath?'Luneblaze-API/api/':'').$codes->UPLOADS_DIRECTORY.'/'.$img:'';
    }

    
    /**
     * Save Quiz
     */
    public function saveQuizzes($args, $quiz_picture, $quiz_picture_dimensions)
    {

        try{

            $conn = ConnectionManager::get('default');

            Log::debug("Started ...saveQuiz Dao");
            $sql=sprintf("INSERT INTO `quizzes`(`title`, `description`, `picture`, `picture_dimensions`, `user_type`, `creator_id`, `start_time`, `live_duration`, `interests`) VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')", 
            $args['title'], $args['description'], $quiz_picture, $quiz_picture_dimensions, $args['user_type'], $args['creator_id'], $args['start_time'], $args['live_duration'], @$args['interests']);

            Log::debug("SQL : ".$sql);

            $conn->execute($sql);
            
            $sql="SELECT LAST_INSERT_ID()";
            
            $stmt = $conn->execute($sql);
            
            $res = $stmt->fetch();
           
            Log::debug("Ended ...saveQuiz Dao");
            
            return $res[0];
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }    
    /**
     * Update Quiz
     */
    public function updateQuizzes($args, $quiz_picture = '', $quiz_picture_dimensions = '')
    {
        Log::debug("Started ...updateQuizzes Dao");

        try{

            $conn = ConnectionManager::get('default');
            $imgQry = ($quiz_picture)?", picture = '".$quiz_picture."', picture_dimensions ='".$quiz_picture_dimensions."'":"";
            $sql=sprintf("UPDATE quizzes SET title = '%s', description = '%s', user_type = '%s', start_time = '%s', live_duration = '%s', `interests` = '%s' $imgQry
                WHERE id = '%s' AND creator_id = '%s'", $args['title'], $args['description'], $args['user_type'], $args['start_time'], $args['live_duration'], @$args['interests'], $args['quiz_id'], $args['creator_id']);

            Log::debug("SQL : ".$sql);

            $conn->execute($sql);
           
            Log::debug("Ended ...updateQuizzes Dao");
            
            return 1;
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }
    /**
     * Delete Quiz
     */
    public function deleteQuizzes($quiz_id, $creator_id)
    {
        Log::debug("Started ...deleteQuizzes Dao");

        try{

            $conn = ConnectionManager::get('default');

            //deleting questions
            $sql=sprintf("UPDATE quizzes SET deleted = 1 WHERE id = '%s' AND creator_id = '%s'", $quiz_id, $creator_id);
            Log::debug("SQL : ".$sql);
            $conn->execute($sql);
           
            Log::debug("Ended ...deleteQuizzes Dao");
            
            return 1;
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }
    /**
     * Get Quizzes
     */
    public function getQuizzesList($args, $date)
    {
        Log::debug("Started ...getQuizzesList Dao");
        $codes = new Codes;

        try{

            $conn = ConnectionManager::get('default');
            $cond = "";
            $having = "";
            $join_cond = " AND user_id = '".$args['user_id']."'";
            //checking filter params
            if (isset($args['my_role']) && !empty($args['my_role'])) {
                //options: 
                // 1. Attempted/submitted (already attempted)
                // 2. Participating (i am going to attempt)
                // 3. Created (i created the quiz)
               if($args['my_role'] =='Attempted/submitted'){
                    $having = " is_attained = 1";
                }
                elseif($args['my_role'] =='Attempted'){
                    $having = " is_attained = 1";
                }
                elseif($args['my_role'] =='Submitted'){
                    $having = " is_submitted = 1";
                }
                else if($args['my_role'] =='Participating'){
                    $having = " qp_id > 0";
                }
                else if($args['my_role'] =='Created')
                    $cond .= " AND qu.creator_id = '".$args['user_id']."'";

            }
            if (isset($args['interests']) && !empty($args['interests'])) {
                //options: 
                // 1. My interests
                // 2. Custom list of interest Ids
                $interestCond = '';
                if($args['interests'] == 'My interests'){
                    
                    $sql=sprintf("SELECT * FROM user_interest where user_id= '%s'", $args['user_id']);
                    Log::debug("SQL : ".$sql);
                    $stmt = $conn->execute($sql);
                    $key = 0;
                    while($result = $stmt->fetch("assoc")) {
                        $interestCond .= ($key?' OR':'')." FIND_IN_SET('".$result['interest']."', qu.interests)";
                        $key++;
                    }
                }
                else if(is_array($args['interests'])){
                    $interestCond = '';
                    foreach ($args['interests'] as $key => $interest) {
                        $interestCond .= ($key?' OR':'')." FIND_IN_SET('".$interest."', qu.interests) > 0";
                    }
                }
                // condition according to intrest
                $cond .= ($interestCond)?" AND (".$interestCond.")":'';

            }
            // condition for query text
            if (isset($args['query_text']) && !empty($args['query_text'])) {
                $cond .= " AND qu.title like '%".$args['query_text']."%'";
            }
            // condition for live status of quiz
            if (isset($args['live_status']) && !empty($args['live_status'])) {
                //options: 
                // 1. Before live duration
                // 2. During live duration
                // 3. After live duration

                if($args['live_status'] =='Before live duration')
                    $having .= ($having?' AND ':'')." quiz_status = 'before live'";
                else if($args['live_status'] =='During live duration')
                    $having .= ($having?' AND ':'')." quiz_status = 'live'";
                else if($args['live_status'] =='After live duration')
                    $having .= ($having?' AND ':'')." quiz_status = 'after live'";

            }


            //checking keys for pagination
            $args['page'] = (isset($args['page']) && $args['page'] > 1)?$args['page']:1;
            $args['pageSize'] = (isset($args['pageSize']) && $args['pageSize'] > 0)?$args['pageSize']:10;

            $cond .= ($having)?" HAVING ( ". $having." ) ":'';
            $cond .= " order by quiz_status desc limit ".(($args['page']- 1) * $args['pageSize']).", ".$args['pageSize'];

            //quiz query
            $sql="SELECT qu.*, us.user_fullname, IF(qp.is_attained = 1, 1, 0) as is_attained, IF(qp.is_submitted = 1, 1, 0) as is_submitted, qp.score, qp.id as qp_id,
                (case
                    when DATE_ADD(start_time, INTERVAL `live_duration` second) < '".$date."' then 'after live'
                    when start_time > '".$date."' then 'before live'
                else 'live' end) as quiz_status,
                (SELECT count(*) as questionCount FROM quizzes_questions WHERE quiz_id = qu.id) as questionCount
                FROM  `quizzes` as qu left join users as us on us.user_id = qu.creator_id left join quizzes_participants as qp on qp.id = (SELECT id FROM quizzes_participants WHERE  is_last = 1 AND quiz_id = qu.id  $join_cond limit 0, 1) WHERE qu.deleted = 0 $cond";
            Log::debug("SQL : ".$sql);
            // echo $sql; exit;
            $stmt = $conn->execute($sql);
            
            $results = array();

            while($result = $stmt->fetch("assoc")) {
                //seting image full path
                $result['picture'] = $this->getImgFullPath($result['picture'], 1);

                $result['goingToParticipate'] = ($result['qp_id'] > 0)?1:0;
                $result['quizInterestArr'] = [];
                $result['participatingFriendsCount'] = 0;
                $result['participatingFriend'] = null;

                //checking frient participants
                $sql="SELECT users.user_id, users.user_work, users.user_work_title, users.user_work_place, users.user_name, users.user_fullname, users.user_firstname,user_lastname, users.user_gender, users.user_picture, qp.is_attained, qp.score, qp.id as qp_id FROM friends INNER JOIN users ON ((friends.user_one_id = users.user_id AND friends.user_one_id != $args[user_id]) OR (friends.user_two_id = users.user_id AND friends.user_two_id != $args[user_id])) left join quizzes_participants as qp on qp.id =(SELECT id FROM quizzes_participants WHERE is_last = 1 AND quiz_id = '$result[id]' AND user_id = users.user_id limit 0, 1) WHERE friends.status = 1 AND (user_one_id = $args[user_id] OR user_two_id = $args[user_id]) having qp_id > 0 ";
                Log::debug("SQL : ".$sql);
                $fellowsStmt = $conn->execute($sql);

                while($fellowResult = $fellowsStmt->fetch("assoc")) {
                    if(empty($result['participatingFriend'])){

                        $fellowResult['user_picture'] = $this->getImgFullPath($fellowResult['user_picture']);
                        $fellowResult['is_attained'] = $fellowResult['is_attained']?1:0;
                        $fellowResult['score'] = $fellowResult['score']?$fellowResult['score']:0;
                        $result['participatingFriend'] = $fellowResult;
                    }
                    $result['participatingFriendsCount']++;
                }
                // checking creater info
                $sql=sprintf("SELECT user_id, user_email, user_name, user_fullname, user_firstname, user_lastname, user_gender, user_picture, user_work, user_work_title , user_work_place , '' as connection FROM users WHERE user_id = '%s' limit 0, 1", $result['creator_id']);

                Log::debug("SQL : ".$sql);
                $createrStmt = $conn->execute($sql);
                $result['created_by'] = $createrStmt->fetch("assoc");
                $result['created_by'] = $result['created_by']?$result['created_by']:[];
                //setting user image
                if(isset($result['created_by']['user_picture']) && !empty($result['created_by']['user_picture']))
                    $result['created_by']['user_picture'] = $this->getImgFullPath(@$result['created_by']['user_picture']);
                

                if($result['interests']){

                    // checking interests
                    $sql=sprintf("SELECT interest_id, `text` as interest_name FROM interest_mst WHERE FIND_IN_SET(interest_id, '%s')", $result['interests']);

                    Log::debug("SQL : ".$sql);
                    $interestStmt = $conn->execute($sql);

                    while($interestResult = $interestStmt->fetch("assoc"))
                        array_push($result['quizInterestArr'], $interestResult);

                }
                array_push($results,$result);
            }
           
            Log::debug("Ended ...getQuizzesList Dao");

            return $results;
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }
    public function getQwizByUserAndId($quiz_id, $user_id, $date)
    {
        Log::debug("Started ...getQwizByUserAndId Dao");
        $codes = new Codes;

        try{

            $conn = ConnectionManager::get('default');
            $join_cond = ($user_id)?" AND qp.user_id = '".$user_id."'":"";

            //getting quiz details
            $sql=sprintf("SELECT qu.*, us.user_fullname, IF(qp.is_attained = 1, 1, 0) as is_attained, IF(qp.is_submitted = 1, 1, 0) as is_submitted, qp.score, qp.last_seen_question_id, qp.id as qp_id,
                (case
                    when DATE_ADD(start_time, INTERVAL `live_duration` second) < '%s' then 'after live'
                    when start_time > '%s' then 'before live'
                else 'live' end) as quiz_status
                FROM `quizzes` as qu left join users as us on us.user_id = qu.creator_id left join quizzes_participants as qp on ( qp.is_last = 1 AND qp.quiz_id = qu.id $join_cond) WHERE qu.id = '%s' limit 0, 1", $date, $date, $quiz_id);

            Log::debug("SQL : ".$sql);
            $quizStmt = $conn->execute($sql);
            $quizData = $quizStmt->fetch("assoc");
            if($quizData){
                //checking quiz is deleted
                if($quizData['deleted'])
                    return ['deleted'=>1];

                $quizData['picture'] = $this->getImgFullPath($quizData['picture'], 1);
                $quizData['goingToParticipate'] = ($quizData['qp_id'] > 0)?1:0;
                $quizData['lastAnsweredQuestionId'] = '';
                $quizData['participantsData'] = [];
                $quizData['topParticipantsData'] = [];
                $quizData['quizInterestArr'] = [];
                $quizData['quizQuestionArr'] = [];
                $quizData['questionCount'] = 0;

                // checking creater info
                $sql=sprintf("SELECT user_id, user_email, user_name, user_fullname, user_firstname, user_lastname, user_gender, user_picture, user_work, user_work_title , user_work_place , '' as connection FROM users WHERE user_id = '%s' limit 0, 1", $quizData['creator_id']);

                Log::debug("SQL : ".$sql);
                $createrStmt = $conn->execute($sql);
                $quizData['created_by'] = $createrStmt->fetch("assoc");
                $quizData['created_by'] = $quizData['created_by']?$quizData['created_by']:[];
                //setting user image
                if(isset($quizData['created_by']['user_picture']) && !empty($quizData['created_by']['user_picture']))
                    $quizData['created_by']['user_picture'] = $this->getImgFullPath($quizData['created_by']['user_picture']);

                // checking questions
                $sql=sprintf("SELECT * FROM quizzes_questions WHERE quiz_id = '%s' order by id", $quizData['id']);

                Log::debug("SQL : ".$sql);
                $questionStmt = $conn->execute($sql);

                while($result = $questionStmt->fetch("assoc")){
                    $result['picture'] = $this->getImgFullPath($result['picture'], 1);
                    $result['answerArr'] = [];
                    $sql=sprintf("SELECT * FROM quizzes_questions_options WHERE quiz_id = '%s' AND question_id = '%s' order by id ", $quizData['id'], $result['id']);

                    Log::debug("SQL : ".$sql);
                    $answerStmt = $conn->execute($sql);

                    while($answerResult = $answerStmt->fetch("assoc"))
                        array_push($result['answerArr'], $answerResult);

                    array_push($quizData['quizQuestionArr'], $result);
                }
                //setting queston count
                $quizData['questionCount'] = count($quizData['quizQuestionArr']);

                //setting interests
                if($quizData['interests']){

                    // checking interests
                    $sql=sprintf("SELECT interest_id, `text` as interest_name FROM interest_mst WHERE FIND_IN_SET(interest_id, '%s')", $quizData['interests']);

                    Log::debug("SQL : ".$sql);
                    $interestStmt = $conn->execute($sql);

                    while($result = $interestStmt->fetch("assoc"))
                        array_push($quizData['quizInterestArr'], $result);

                }

                
                if($quizData['quiz_status'] == 'after live'){

                    // checking attempted user and top scorer
                    $sql=sprintf("SELECT qp.*, us.user_fullname, qp.score as correctlyAnsweredQuestions FROM quizzes_participants as qp left join users as us on us.user_id = qp.user_id WHERE qp.quiz_id = '%s' group by qp.user_id order by qp.score desc", $quiz_id);

                    Log::debug("SQL : ".$sql);
                    $participantsStmt = $conn->execute($sql);

                    while($result = $participantsStmt->fetch("assoc"))
                        array_push($quizData['participantsData'], $result);

                    if($quizData['participantsData']){
                        $quizData['topParticipantsData'] = array_slice($quizData['participantsData'], 0, 10);
                    }

                }

                $quizData['totalParticipants'] = count($quizData['participantsData']);

                if($quizData['is_submitted'] == 0){

                    //checking last question attepmted
                    $sql=sprintf("SELECT qpa.*, qq.question FROM quizzes_participants_answers as qpa left join quizzes_questions as qq on qq.id = qpa.question_id WHERE qpa.quiz_id = '%s' AND qpa.user_id = '%s' order by id desc limit 0, 1", $quiz_id, $user_id);
                    Log::debug("SQL : ".$sql);
                    $stmt = $conn->execute($sql);
                    $quizData['lastQuestionAnswered'] = $stmt->fetch("assoc");
                    $quizData['lastAnsweredQuestionId'] = isset($quizData['lastQuestionAnswered']['question_id'])?$quizData['lastQuestionAnswered']['question_id']:'';

                }

            }

            Log::debug("Ended ...getQwizByUserAndId Dao");

            return $quizData;
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }
    
    /**
     * getQuizCountByUserId
     */
    public function getQuizCountByUserId($userId)
    {
        Log::debug("Started ...getQuizCountByUserId Dao : User Id : ".$userId);

        try{
            
            $conn = ConnectionManager::get('default');

            $sql=sprintf("SELECT count(*) as total FROM `quizzes` WHERE `creator_id` =  '%s'", $userId);

            Log::debug("SQL : ".$sql);

            $stmt = $conn->execute($sql);

            $result = $stmt->fetch("assoc");

            Log::debug("Ended ...getUserByUserIdAndLoginOtp Dao");

            return $result;
            
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }

    }

    //updating user quiz participation
    public function addRemoveQuizParticipation($args, $date)
    {

        Log::debug("Started ...addRemoveQuizParticipation Dao");
        try{
            $conn = ConnectionManager::get('default');
             $response = array("valid" => false, "msg"=>"This quiz has been expired.");

            //checking quiz live duration
            $sql=sprintf("SELECT * FROM quizzes WHERE deleted = 0 AND id = '%s' AND DATE_ADD(start_time, INTERVAL `live_duration` second) >= '%s'", $args['quiz_id'], $date, $date);
            Log::debug("SQL : ".$sql);
            $stmt = $conn->execute($sql);
            $quizData = $stmt->fetch("assoc");

            if($quizData){
                //checking quiz already participant
                $sql=sprintf("SELECT * FROM quizzes_participants WHERE quiz_id = '%s' AND user_id = '%s'", $args['quiz_id'], $args['user_id']);
                Log::debug("SQL : ".$sql);
                $stmt = $conn->execute($sql);
                $participantData = $stmt->fetch("assoc");

                if($args['status'] == 2){
                    if(!empty($participantData)){
                        //deleting participation of user
                        $sql=sprintf("DELETE FROM `quizzes_participants` WHERE quiz_id = '%s' AND user_id = '%s'", $args['quiz_id'], $args['user_id']);
                        Log::debug("SQL : ".$sql);
                        if($conn->execute($sql)){
                            $response = array("valid" => true, "msg"=>"Participation removed");
                        }else
                            $response["msg"] = "Something went wrong.";
                    }else
                        $response["msg"] = "Not participated.";

                }else if($args['status'] == 1){
                    if(empty($participantData)){
                        //inserting participation of user
                        $sql=sprintf("INSERT INTO `quizzes_participants`(`quiz_id`, `user_id`) VALUES ('%s', '%s')", $args['quiz_id'], $args['user_id']);
                        Log::debug("SQL : ".$sql);
                        if($conn->execute($sql)){
                            $response = array("valid" => true, "msg"=>"Participated successfully");
                        }else
                            $response["msg"] = "Something went wrong.";
                    }else
                        $response["msg"] = "Already participated.";
                }
            }

           
            Log::debug("Ended ...addRemoveQuizParticipation Dao");
            
            return $response;
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }
    //updating user quiz participation answer
    public function sendQuizParticipationAnswer($args, $date)
    {

        Log::debug("Started ...sendQuizParticipationAnswer Dao");
        $response = array("valid" => false, "msg"=>"This quiz is not live currently.");
        try{
            $conn = ConnectionManager::get('default');
            //checking quiz live duration
            // AND DATE_ADD(start_time, INTERVAL `live_duration` second) >= '%s'
            $sql=sprintf("SELECT * FROM quizzes WHERE deleted = 0 AND id = '%s' AND start_time <= '%s'", $args['quiz_id'], $date, $date);
            Log::debug("SQL : ".$sql);
            $stmt = $conn->execute($sql);
            $quizData = $stmt->fetch("assoc");

            if($quizData){
                //checking if participation already exist
                $sql=sprintf("SELECT * from quizzes_participants WHERE is_last = 1 AND quiz_id = '%s' AND user_id = '%s' limit 0, 1", $args['quiz_id'], $args['user_id']);
                Log::debug("SQL : ".$sql);
                $stmt = $conn->execute($sql);
                $participantsData = $stmt->fetch("assoc");
                if(empty($participantsData)){
                    //inserting participation of user
                    $sql=sprintf("INSERT INTO `quizzes_participants`(`quiz_id`, `user_id`) VALUES ('%s', '%s')", $args['quiz_id'], $args['user_id']);
                    Log::debug("SQL : ".$sql);
                    $conn->execute($sql);

                    $sql="SELECT LAST_INSERT_ID()";                    
                    $stmt = $conn->execute($sql);                    
                    $res = $stmt->fetch();
                    $participantsData['id'] = @$res[0];
                }
                if(isset($participantsData['id']) && !empty($participantsData['id'])){
                    //checking if answer already exist
                    $sql=sprintf("SELECT * FROM quizzes_participants_answers WHERE participation_id = '%s' AND quiz_id = '%s' AND user_id = '%s' AND question_id = '%s'", $participantsData['id'], $args['quiz_id'], $args['user_id'], $args['question_id']);
                    Log::debug("SQL : ".$sql);
                    $stmt = $conn->execute($sql);
                    $participantAnswerData = $stmt->fetch("assoc");
                    if(empty($participantAnswerData)){
                        //inserting participation asnswer of user
                        $sql=sprintf("INSERT INTO `quizzes_participants_answers`(`participation_id`,`quiz_id`, `user_id`, `question_id`, `option_id`, `time_left`, `created`) VALUES ('%s','%s', '%s','%s', '%s','%s', '%s')",$participantsData['id'], $args['quiz_id'], $args['user_id'], $args['question_id'], $args['option_id'], $args['time_left'], $date);
                        Log::debug("SQL : ".$sql);

                        if($conn->execute($sql)){

                            $scoreData = $this->getQuizScoreCalculation($args, $date, 1);

                            $response = array("valid" => true, "msg"=>"Updated", "data"=> $scoreData);
                        }
                        else
                            $response["msg"] = "Something went wrong.";

                    }else
                        $response["msg"] = "You have already answered.";
                }else
                    $response["msg"] = "Something went wrong.";
            }
           
            Log::debug("Ended ...sendQuizParticipationAnswer Dao");
            
            return $response;
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }
    //updating user quiz participation answer
    public function updateQuizLastSeenQuestion($args, $date)
    {

        Log::debug("Started ...updateQuizLastSeenQuestion Dao");
        $response = array("valid" => false, "msg"=>"Quiz not found.");
        try{
            $conn = ConnectionManager::get('default');
            //checking quiz live duration
            $sql=sprintf("SELECT id, (case
                    when DATE_ADD(start_time, INTERVAL `live_duration` second) < '%s' then 'after live'
                    when start_time > '%s' then 'before live'
                else 'live' end) as quiz_status FROM quizzes WHERE deleted = 0 AND id = '%s'", $date, $date, $args['quiz_id']);
            Log::debug("SQL : ".$sql);
            $stmt = $conn->execute($sql);
            $quizData = $stmt->fetch("assoc");
            // print_r($quizData); exit;
            if($quizData){

                //checking if already participated
                $sql=sprintf("SELECT * FROM quizzes_participants WHERE is_last = 1 AND quiz_id = '%s' AND user_id = '%s' limit 0, 1", $args['quiz_id'], $args['user_id']);
                Log::debug("SQL : ".$sql);
                $participantsstmt = $conn->execute($sql);
                $participantsData = $participantsstmt->fetch("assoc");
                if(empty($participantsData)){
                    //inserting participation of user
                    $sql=sprintf("INSERT INTO `quizzes_participants`(`quiz_id`, `user_id`, `last_seen_question_id`) VALUES ('%s', '%s', '%s')", $args['quiz_id'], $args['user_id'], $args['question_id']);
                    Log::debug("SQL : ".$sql);
                    $conn->execute($sql);

                    $response = array("valid" => true, "msg"=>"Updated", "data"=> null);
                }else{
                    if($participantsData['is_submitted'] != 1){
                        $sql=sprintf("UPDATE quizzes_participants SET last_seen_question_id = '%s', is_attained = 1 WHERE is_last = 1 AND user_id = '%s' AND quiz_id = '%s'", $args['question_id'], $args['user_id'], $args['quiz_id']);

                        Log::debug("SQL : ".$sql);
                        $conn->execute($sql);

                        $response = array("valid" => true, "msg"=>"Updated", "data"=> null);                   
                    }else{
                        if($quizData['quiz_status']  != 'live'){
                            $sql=sprintf("UPDATE quizzes_participants SET is_last = 0 WHERE is_last = 1 AND quiz_id = '%s' AND user_id = '%s'", $args['quiz_id'], $args['user_id']);
                            Log::debug("SQL : ".$sql);
                            $conn->execute($sql);

                            //inserting participation of user
                            $sql=sprintf("INSERT INTO `quizzes_participants`(`quiz_id`, `user_id`, `last_seen_question_id`) VALUES ('%s', '%s', '%s')", $args['quiz_id'], $args['user_id'], $args['question_id']);
                            Log::debug("SQL : ".$sql);
                            $conn->execute($sql);

                            $response = array("valid" => true, "msg"=>"Updated", "data"=> null);
                        }else
                            $response['msg'] = "You can not attempt the quiz more than one while it's live.";

                    }
                }
            }
           
            Log::debug("Ended ...updateQuizLastSeenQuestion Dao");
            
            return $response;
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }

    //calculation of quiz score 
    public function getQuizScoreCalculation($args, $date, $onlyCalculateScore = 0)
    {

        Log::debug("Started ...getQuizScoreCalculation Dao");
        try{
            $conn = ConnectionManager::get('default');
            //checking quiz score
            // before expired= correctAnsCount*A
            // after expired=correctAnsCount*B

            $scoreData['score'] = 0;
            $scoreData['correctlyAnsweredQuestions'] = 0;

            $sql=sprintf("SELECT DATE_ADD(start_time, INTERVAL `live_duration` second) as quizExpiry FROM quizzes WHERE id = '%s' limit 0, 1", $args['quiz_id']);
            Log::debug("SQL : ".$sql);
            $stmt = $conn->execute($sql);
            $quizData = $stmt->fetch("assoc");

            if(isset($quizData['quizExpiry']) && !empty($quizData['quizExpiry'])){

                //checking if participation already exist
                $sql=sprintf("SELECT * from quizzes_participants WHERE is_last = 1 AND quiz_id = '%s' AND user_id = '%s' limit 0, 1", $args['quiz_id'], $args['user_id']);
                Log::debug("SQL : ".$sql);
                $stmt = $conn->execute($sql);
                $participantsData = $stmt->fetch("assoc");
                if(!empty($participantsData)){

                    $sql=sprintf("SELECT * FROM quizzes_participants_answers as qpa inner join quizzes_questions_options as qqo on qqo.id = qpa.option_id WHERE qpa.participation_id = '%s' AND qqo.is_correct = 1", $participantsData['id']);
                    Log::debug("SQL : ".$sql);
                    $stmt = $conn->execute($sql);
                    while($result = $stmt->fetch("assoc")) {
                        
                        $scoreData['score'] += ($result['time_left']?$result['time_left']:1)*((strtotime($quizData['quizExpiry']) > strtotime($result['created']))?1:5);
                        $scoreData['correctlyAnsweredQuestions']++;

                    }

                    //setting as submitted quiz
                    $subQry = $onlyCalculateScore?"":" is_submitted = 1, last_seen_question_id = 0, ";
                    $sql=sprintf("UPDATE quizzes_participants SET score = '%s', $subQry is_attained = 1
                    WHERE id = '%s'", $scoreData['score'], $participantsData['id']);

                    Log::debug("SQL : ".$sql);
                    $conn->execute($sql);
                }
            }
           
            Log::debug("Ended ...getQuizScoreCalculation Dao");
            
            return $scoreData;
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }
    //sending invitaion for quiz
    public function sendQuizInvitaion($args, $date)
    {

        $response = array("valid" => false, "msg"=>"This quiz is not open of invitation.");
        Log::debug("Started ...sendQuizInvitaion Dao");
        try{
            $conn = ConnectionManager::get('default');

            //checking quiz live duration
            // AND DATE_ADD(start_time, INTERVAL `live_duration` second) >= '%s'
            $sql=sprintf("SELECT * FROM quizzes WHERE deleted = 0 AND id = '%s'", $args['quiz_id']);
            Log::debug("SQL : ".$sql);
            $stmt = $conn->execute($sql);
            $quizData = $stmt->fetch("assoc");

            //checking quiz invitations
            $sql=sprintf("SELECT * FROM quizzes_invitations WHERE quiz_id = '%s' AND user_id = '%s' AND sender_user_id = '%s'", $args['quiz_id'], $args['user_id'], $args['sender_user_id']);
            Log::debug("SQL : ".$sql);
            $stmt = $conn->execute($sql);
            $invitationData = $stmt->fetch("assoc");

            if(!empty($quizData)){
                if(empty($invitationData)){
                    //inserting invitation of user
                    $sql=sprintf("INSERT INTO `quizzes_invitations`(`quiz_id`, `user_id`, `sender_user_id`, `created`) VALUES ('%s', '%s','%s', '%s')", $args['quiz_id'], $args['user_id'], $args['sender_user_id'], $date);
                    Log::debug("SQL : ".$sql);

                     if($conn->execute($sql)){   
                        $response = array("valid" => true, "msg"=>"Updated");
                    }
                    else
                        $response["msg"] = "Something went wrong.";              
                }else
                    $response["msg"] = "Already invited";
            }
           
            Log::debug("Ended ...sendQuizInvitaion Dao");
            
            return $response;
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }

    //checking invitation for quiz
    public function getQuizInvitedUserList($args)
    {

        Log::debug("Started ...getQuizInvitedUserList Dao");
        $codes = new Codes;
        try{
            $conn = ConnectionManager::get('default');

            //checking keys for pagination
            $args['page'] = (isset($args['page']) && $args['page'] > 1)?$args['page']:1;
            $args['pageSize'] = (isset($args['pageSize']) && $args['pageSize'] > 0)?$args['pageSize']:10;

            //checking quiz invitations
            $cond = (isset($args['search_keyword']) && !empty($args['search_keyword']))?" AND (users.user_fullname LIKE '%".$args['search_keyword']."%' OR users.user_name LIKE '%".$args['search_keyword']."%')":"";
            $cond .= " order by qp.is_attained asc limit ".(($args['page']- 1) * $args['pageSize']).", ".$args['pageSize'];

            //checking quiz invitations users
            // $invitationArr = [];
            // $sql=sprintf("SELECT user_id FROM quizzes_invitations WHERE quiz_id = '%s' AND sender_user_id = '%s'", $args['quiz_id'], $args['user_id']);
            // Log::debug("SQL : ".$sql);
            // $invitationStmt = $conn->execute($sql);

            // while($result = $invitationStmt->fetch("assoc")) {
            //     $invitationArr[] = $result['user_id'];
            // }
            //checking user's fellow 

            $followings_ids = [];
            $friends_ids = [];
            $friend_requests_ids = [];
            $friend_requests_sent_ids = [];
            $fellowsArr = [];

            //checking quiz invitations users
            $sql=sprintf("SELECT following_id FROM followings WHERE user_id = '%s'", $args['user_id']);
            Log::debug("SQL : ".$sql);
            $followingsStmt = $conn->execute($sql);

            while($result = $followingsStmt->fetch("assoc")) {
                $followings_ids[] = $result['following_id'];
            }
            //checking quiz invitations users
            $sql=sprintf('SELECT users.user_id FROM friends INNER JOIN users ON (friends.user_one_id = users.user_id AND friends.user_one_id != %1$s) OR (friends.user_two_id = users.user_id AND friends.user_two_id != %1$s) WHERE status = 1 AND (user_one_id = %1$s OR user_two_id = %1$s)', $args['user_id']);
            Log::debug("SQL : ".$sql);
            $friendsStmt = $conn->execute($sql);

            while($result = $friendsStmt->fetch("assoc")) {
                $friends_ids[] = $result['user_id'];
            }
            //checking quiz invitations users
            $sql=sprintf('SELECT user_one_id FROM friends WHERE status = 0 AND user_two_id = %s', $args['user_id']);
            Log::debug("SQL : ".$sql);
            $friend_requestsStmt = $conn->execute($sql);

            while($result = $friend_requestsStmt->fetch("assoc")) {
                $friend_requests_ids[] = $result['user_one_id'];
            }

            //checking quiz friend_requests_sent users
            $sql=sprintf('SELECT user_one_id FROM friends WHERE status = 0 AND user_two_id = %s', $args['user_id']);
            Log::debug("SQL : ".$sql);
            $friend_requests_sentStmt = $conn->execute($sql);

            while($result = $friend_requests_sentStmt->fetch("assoc")) {
                $friend_requests_sent_ids[] = $result['user_one_id'];
            }

            // $sql=sprintf("SELECT qi.user_id, us.user_work,us.user_work_title,us.user_work_place, us.user_name, us.user_fullname,us.user_firstname,user_lastname, us.user_gender, us.user_picture, qp.is_attained, qp.score FROM quizzes_invitations as qi left join users as us on us.user_id = qi.user_id left join quizzes_participants as qp on (qp.user_id = qi.user_id AND qp.quiz_id = qi.quiz_id) WHERE qi.quiz_id = '%s' AND qi.sender_user_id = '%s'".$cond, $args['quiz_id'], $args['user_id']);

            $sql="SELECT users.user_id, users.user_work, users.user_work_title, users.user_work_place, users.user_name, users.user_fullname, users.user_firstname,user_lastname, users.user_gender, users.user_picture, qp.is_attained, qp.score FROM friends INNER JOIN users ON ((friends.user_one_id = users.user_id AND friends.user_one_id != $args[user_id]) OR (friends.user_two_id = users.user_id AND friends.user_two_id != $args[user_id])) left join quizzes_participants as qp on qp.id =(SELECT id FROM quizzes_participants WHERE is_last = 1 AND quiz_id = '$args[quiz_id]' AND user_id = users.user_id limit 0, 1) WHERE friends.status = 1 AND (user_one_id = $args[user_id] OR user_two_id = $args[user_id]) $cond";
            Log::debug("SQL : ".$sql);
            $fellowsStmt = $conn->execute($sql);

            while($result = $fellowsStmt->fetch("assoc")) {
                $result['user_picture'] = $this->getImgFullPath($result['user_picture']);

                $result['connection'] = $this->connection($args['user_id'], $result['user_id'], $followings_ids, $friends_ids, $friend_requests_ids, $friend_requests_sent_ids);
                $result['is_friend'] = in_array($args['user_id'], $friends_ids)?1:0;
                $result['is_following'] = in_array($args['user_id'], $followings_ids)?1:0;
                $result['is_attained'] = $result['is_attained']?1:0;
                $result['score'] = $result['score']?$result['score']:0;
                $fellowsArr[] = $result;
            }
            // $users = array_intersect($invitationArr,$fellowsArr);
            
            // $results = array();
            // if(!empty($fellowsArr)){
            //     $sql=sprintf("SELECT qi.user_id, us.user_name, us.user_picture, us.user_work_title, us.user_work_place, us.user_fullname, '' as connection, '0' as is_friend, '0' as is_following, IF(qp.is_attained = 1, 1, 0) as is_attained, qp.score FROM quizzes_invitations as qi left join users as us on us.user_id = qi.user_id left join quizzes_participants as qp on (qp.user_id = qi.user_id AND qp.quiz_id = qi.quiz_id) WHERE qi.quiz_id = '%s' AND qi.sender_user_id = '%s'".$cond, $args['quiz_id'], $args['user_id']);
            //     // Log::debug("SQL : ".$sql);
            //     $stmt = $conn->execute($sql);


            //     while($result = $stmt->fetch("assoc")) {
            //         $result['user_picture'] = $codes->SYSTEM_URL.'/api/'.$codes->UPLOADS_DIRECTORY.'/'.$result['user_picture'];

            //         array_push($results,$result);
            //     }
            // }
            Log::debug("Ended ...getQuizInvitedUserList Dao");
            
            return $fellowsArr;
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }

    private function connection($logged_in_user_id, $user_id, $followings_ids = [], $friends_ids = [], $friend_requests_ids = [], $friend_requests_sent_ids = [])
    {
        /* check if the viewer is the target */
        if ($user_id == $logged_in_user_id) {
            return "me";
        }
        // Arnab mohanty:: on 2nd Jan 2019
        if (in_array($user_id, $followings_ids)) {
            /* the viewer follow the target */
            return "followed";
        }
        /* check if the viewer & the target are friends */
        if (in_array($user_id, $friends_ids)) {
            return "remove";
        }
        /* check if the target sent a request to the viewer */
        if (in_array($user_id, $friend_requests_ids)) {
            return "request";
        }
        /* check if the viewer sent a request to the target */
        if (in_array($user_id, $friend_requests_sent_ids)) {
            return "cancel";
        }
        
        
        /* there is no relation between the viewer & the target */
        return "add";
        
    }
    



}