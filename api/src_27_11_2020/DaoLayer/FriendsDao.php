<?php

namespace App\DaoLayer;

use App\Utils\Codes;
use Cake\Core\Exception\Exception;
use Cake\Datasource\ConnectionManager;
use Cake\Log\Log;

class FriendsDao
{

	/**
	 * Get Friends using user_one_id and user_two_id and status
	 */
	public function getFriendsByUserOneIdAndUserTwoIdAndStatus($userOneId, $userTwoId, $status)
	{
		Log::debug("Started ...getFriendsByUserOneIdAndUserTwoIdAndStatus Dao : User One Id : ".$userOneId.", User Two Id : ".$userTwoId.", Status : ".$status);

		try{
			
			$conn = ConnectionManager::get('default');

			$sql=sprintf("SELECT * FROM friends WHERE user_one_id = %s AND user_two_id = %s AND status = %s", $userOneId, $userTwoId, $status);

			Log::debug("SQL : ".$sql);

			$conn->execute($sql);

			Log::debug("Ended ...getFriendsByUserOneIdAndUserTwoIdAndStatus Dao");

		}catch(\Exception $e){
			Log::debug($e);
			throw new Exception($e);
		}
	}

	/**
	 * Update Friends status user_one_id and user_two_id
	 */
	public function updateFriendsStatusByUserId($status, $userOneId, $userTwoId)
	{
		Log::debug("Started ...updateFriendsStatusByUserId Dao : Status Id : ".$status.", User One Id : ".$userOneId.", User Two Id : ".$userTwoId);

		try{
			
			$conn = ConnectionManager::get('default');

			$sql=sprintf("UPDATE friends SET status = %s WHERE user_one_id = %s AND user_two_id = %s", $status, $userOneId, $userTwoId);

			Log::debug("SQL : ".$sql);

			$conn->execute($sql);

			Log::debug("Ended ...updateFriendsStatusByUserId Dao");

		}catch(\Exception $e){
			Log::debug($e);
			throw new Exception($e);
		}
	}

	/**
	 * Get Friends using user_one_id and user_two_id
	 */
	public function getFriendsByUserOneIdAndUserTwoId($userOneId, $userTwoId)
	{
		Log::debug("Started ...getFriendsByUserOneIdAndUserTwoId Dao : User One Id : ".$userOneId.", User Two Id : ".$userTwoId);

		try{
			
			$conn = ConnectionManager::get('default');

			$sql=sprintf('SELECT * FROM friends WHERE (user_one_id = %1$s AND user_two_id = %2$s) OR (user_one_id = %2$s AND user_two_id = %1$s)', $userOneId, $userTwoId);

			Log::debug("SQL : ".$sql);

			$conn->execute($sql);

			Log::debug("Ended ...getFriendsByUserOneIdAndUserTwoId Dao");

		}catch(\Exception $e){
			Log::debug($e);
			throw new Exception($e);
		}
	}
   
}