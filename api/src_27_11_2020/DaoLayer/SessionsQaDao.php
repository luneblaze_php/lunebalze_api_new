<?php

namespace App\DaoLayer;

use App\Utils\Codes;
use Cake\Core\Exception\Exception;
use Cake\Datasource\ConnectionManager;
use Cake\Log\Log;

class SessionsQaDao
{

	/**
	 * Get Sessions Qa By Sessions Qa id
	 */
	public function getSessionsQaBySessionsQaId($sessionsQaId)
	{
		Log::debug("Started ...getSessionsQaBySessionsQaId Dao : Sessions Qa Id : ".$sessionsQaId);

		try{
			
			$conn = ConnectionManager::get('default');

			$sql=sprintf("SELECT * FROM sessions_qa WHERE sessions_qa_id = %s", $sessionsQaId);
			
			Log::debug("SQL : ".$sql);

			$stmt = $conn->execute($sql);

			$result = $stmt->fetch("assoc");

			Log::debug("Ended ...getSessionsQaBySessionsQaId Dao");

			return $result;

		}catch(\Exception $e){
			Log::debug($e);
			throw new Exception($e);
		}
	}
	
	/**
	 * Save Sessions Qa
	 */
	public function saveSessionsQa($sessionsId, $commentPost, $userId, $id, $date)
	{
		Log::debug("Started ...saveArticlesDiscussion Dao : sessions Id : ".$sessionsId.", Post : ".$commentPost.", User Id : ".$userId.", Qa Type : 2, Parent Question Id : ".$id.", status : 1, Added On : ".$date.", Modified On : ".$date);

		try{
			
			$conn = ConnectionManager::get('default');

			$sql=sprintf("INSERT INTO `sessions_qa` (sessions_id, post,user_id,qa_type,parent_question_id,status,added_on,modified_on) VALUES (%s, %s, %s,2, %s,1,%s, %s)", $sessionsId, $commentPost, $userId, $id, $date, $date);

			Log::debug("SQL : ".$sql);

			$conn->execute($sql);

			Log::debug("Ended ...saveArticlesDiscussion Dao");

		}catch(\Exception $e){
			Log::debug($e);
			throw new Exception($e);
		}
	}

}
