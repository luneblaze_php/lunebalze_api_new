<?php

namespace App\DaoLayer;

use App\Utils\Codes;
use Cake\Core\Exception\Exception;
use Cake\Datasource\ConnectionManager;
use Cake\Log\Log;

class QuizzesQuestionsOptionsDao
{


    
    /**
     * Save Quiz
     */
    public function saveQuizzesQuestionsOptions($args, $quiz_id, $question_id)
    {
        
        try{
            Log::debug("Started ...saveQuizzesQuestionsOptions Dao");

            $conn = ConnectionManager::get('default');
            
            $sql=sprintf("INSERT INTO `quizzes_questions_options`(`quiz_id`, `question_id`, `option_text`, `is_correct`) VALUES ('%s', '%s', '%s', '%s')", 
            $quiz_id, $question_id, $args['option_text'], $args['is_correct']);

            Log::debug("SQL : ".$sql);

            $conn->execute($sql);
           
            Log::debug("Ended ...saveQuizzesQuestionsOptions Dao");
            
            return 1;
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }



}