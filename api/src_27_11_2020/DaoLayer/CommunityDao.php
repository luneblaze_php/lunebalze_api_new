<?php

namespace App\DaoLayer;

use App\Utils\Codes;
use Cake\Core\Exception\Exception;
use Cake\Datasource\ConnectionManager;
use Cake\Log\Log;

class CommunityDao
{

    private function getImgFullPath($img = '', $newPath = 0){
        $codes = new Codes;
        return !empty($img) ?$codes->SYSTEM_URL.'/'.($newPath?'Luneblaze-API/api/':'').$codes->UPLOADS_DIRECTORY.'/'.$img:'';
    }
    
    /**
     * Save Community
     */
    public function saveCommunity($args, $community_picture, $cover_picture)
    {

        try{

            $conn = ConnectionManager::get('default');

            Log::debug("Started ...saveCommunity Dao");
            $sql=sprintf("INSERT INTO `communities`(`title`, `description`, `bio`, `picture`, `picture_dimensions`, `cover_picture`, `cover_dimensions`, `user_type`, `creator_id`, `interests`) VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')", 
            $args['title'], $args['description'], $args['bio'], $community_picture['image'], $community_picture['dimensions'], @$cover_picture['image'], @$cover_picture['dimensions'], $args['user_type'], $args['creator_id'], @$args['interests']);

            Log::debug("SQL : ".$sql);

            $conn->execute($sql);
            
            $sql="SELECT LAST_INSERT_ID()";            
            $stmt = $conn->execute($sql);            
            $res = $stmt->fetch();

            if($res[0]){
                $sql=sprintf("INSERT INTO `communities_members`(`community_id`, `user_id`, `role`) VALUES ('%s', '%s', '%s')", $res[0], $args['creator_id'],1);
                Log::debug("SQL : ".$sql);
                $conn->execute($sql);
            }
           
            Log::debug("Ended ...saveCommunity Dao");
            
            return $res[0];
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }    
    /**
     * Update Community
     */
    public function updateCommunity($args, $community_picture = '', $cover_picture = '')
    {
        Log::debug("Started ...updateCommunitys Dao");

        try{

            $conn = ConnectionManager::get('default');

            $imgQry = (@$community_picture['image'])?", picture = '".$community_picture['image']."', picture_dimensions ='".$community_picture['dimensions']."'":"";
            $imgQry .= (@$cover_picture['image'])?", cover_picture = '".$cover_picture['image']."', cover_dimensions ='".$cover_picture['dimensions']."'":"";
            $sql=sprintf("UPDATE communities SET title = '%s', description = '%s', user_type = '%s', bio = '%s', `interests` = '%s' $imgQry
                WHERE id = '%s' AND creator_id = '%s'", $args['title'], $args['description'], $args['user_type'], $args['bio'], @$args['interests'], $args['community_id'], $args['creator_id']);

            Log::debug("SQL : ".$sql);

            $conn->execute($sql);
           
            Log::debug("Ended ...updateCommunitys Dao");
            
            return 1;
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }
    /**
     * Delete Community
     */
    public function deleteCommunity($community_id, $creator_id)
    {
        Log::debug("Started ...deleteCommunitys Dao");

        try{

            $conn = ConnectionManager::get('default');

            //deleting questions
            $sql=sprintf("UPDATE communities SET deleted = 1 WHERE id = '%s' AND creator_id = '%s'", 
            $community_id, $creator_id);
            Log::debug("SQL : ".$sql);
            $conn->execute($sql);
           
            Log::debug("Ended ...deleteCommunitys Dao");
            
            return 1;
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }
    /**
     * Get Communitys
     */
    public function getCommunityList($args, $date)
    {
        Log::debug("Started ...getCommunityList Dao");
        $codes = new Codes;

        try{

            $conn = ConnectionManager::get('default');
            $cond = "";
            $having = "";
            //checking filter params
            if (isset($args['my_role']) && !empty($args['my_role'])) {
                //options: 
                //Member/Admin/Moderator
                if($args['my_role'] =='Member'){
                    $having = " cmm.role = 3";
                }
                elseif($args['my_role'] =='Admin'){
                    $having = " cmm.role = 1";
                }
                elseif($args['my_role'] =='Moderator'){
                    $having = " cmm.role = 2";
                }

            }
            if (isset($args['interests']) && !empty($args['interests'])) {
                //options: 
                // 1. My interests
                // 2. Custom list of interest Ids
                $interestCond = '';
                if($args['interests'] == 'My interests'){
                    
                    $sql=sprintf("SELECT * FROM user_interest where user_id= '%s'", $args['user_id']);
                    Log::debug("SQL : ".$sql);
                    $stmt = $conn->execute($sql);
                    $key = 0;
                    while($result = $stmt->fetch("assoc")) {
                        $interestCond .= ($key?' OR':'')." FIND_IN_SET('".$result['interest']."', cm.interests)";
                        $key++;
                    }
                }
                else if(is_array($args['interests'])){
                    $interestCond = '';
                    foreach ($args['interests'] as $key => $interest) {
                        $interestCond .= ($key?' OR':'')." FIND_IN_SET('".$interest."', cm.interests) > 0";
                    }
                }
                // condition according to intrest
                $cond .= ($interestCond)?" AND (".$interestCond.")":'';

            }
            // condition for query text
            if (isset($args['query_text']) && !empty($args['query_text'])) {
                $cond .= " AND cm.title like '%".$args['query_text']."%'";
            }

            //checking keys for pagination
            $args['page'] = (isset($args['page']) && $args['page'] > 1)?$args['page']:1;
            $args['pageSize'] = (isset($args['pageSize']) && $args['pageSize'] > 0)?$args['pageSize']:10;

            $cond .= ($having)?" HAVING ( ". $having." ) ":'';
            $cond .= " order by id desc limit ".(($args['page']- 1) * $args['pageSize']).", ".$args['pageSize'];

            //community query
            $sql="SELECT cm.*, us.user_fullname, cmm.role
                FROM  `communities` as cm left join users as us on us.user_id = cm.creator_id left join communities_members as cmm on (cmm.community_id = cm.id AND cmm.user_id = $args[user_id]) WHERE  deleted = 0 $cond";
            Log::debug("SQL : ".$sql);
            // echo $sql; exit;
            $stmt = $conn->execute($sql);
            
            $responseData = array();

            while($result = $stmt->fetch("assoc")) {
                $result['picture'] = $this->getImgFullPath($result['picture'],1);
                $result['cover_picture'] = $this->getImgFullPath($result['cover_picture'],1);
                $result['role'] = ($result['role'] == 1)?'Admin':($result['role'] == 2 ? 'Moderator':'Member');
                $result['communityInterestArr'] = [];

                
                // checking creater info
                $sql=sprintf("SELECT user_id, user_email, user_name, user_fullname, user_firstname, user_lastname, user_gender, user_picture, user_work, user_work_title , user_work_place , '' as connection FROM users WHERE user_id = '%s' limit 0, 1", $result['creator_id']);

                Log::debug("SQL : ".$sql);
                $createrStmt = $conn->execute($sql);
                $result['created_by'] = $createrStmt->fetch("assoc");
                $result['created_by'] = $result['created_by']?$result['created_by']:[];
                //setting user image
                $result['created_by']['user_picture'] = $this->getImgFullPath(@$result['created_by']['user_picture']);

                //setting interests
                if($result['interests']){

                    // checking interests
                    $sql=sprintf("SELECT interest_id, `text` as interest_name FROM interest_mst WHERE FIND_IN_SET(interest_id, '%s')", $result['interests']);

                    Log::debug("SQL : ".$sql);
                    $interestStmt = $conn->execute($sql);

                    while($interestResult = $interestStmt->fetch("assoc"))
                        array_push($result['communityInterestArr'], $interestResult);

                }

                array_push($responseData,$result);
            }
           
            Log::debug("Ended ...getCommunityList Dao");

            return $responseData;
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }

    public function getCommunityByUserAndId($community_id, $user_id, $date)
    {
        Log::debug("Started ...getCommunityByUserAndId Dao");
        $codes = new Codes;

        try{

            $conn = ConnectionManager::get('default');
            $join_cond = " AND dp.user_id = '".$user_id."'";
            //getting community details
            $sql=sprintf("SELECT cm.*, us.user_fullname, cmm.role
                FROM  `communities` as cm left join users as us on us.user_id = cm.creator_id left join communities_members as cmm on (cmm.community_id = cm.id AND cmm.user_id = $user_id) WHERE cm.id = '%s' limit 0, 1", $community_id);

            Log::debug("SQL : ".$sql);
            // echo $sql; exit;
            $communityStmt = $conn->execute($sql);
            $communityData = $communityStmt->fetch("assoc");
            if($communityData){
                //checking community is deleted
                if($communityData['deleted'])
                    return ['deleted'=>1];

                $communityData['picture'] = $this->getImgFullPath($communityData['picture'],1);
                $communityData['cover_picture'] = $this->getImgFullPath($communityData['cover_picture'],1);
                $communityData['role'] = ($communityData['role'] == 1)?'Admin':($communityData['role'] == 2 ? 'Moderator':'Member');
                $communityData['memberData'] = [];
                $communityData['communityInterestArr'] = [];
                $communityData['created_by'] = [];

                // checking creater info
                $sql=sprintf("SELECT user_id, user_email, user_name, user_fullname, user_firstname, user_lastname, user_gender, user_picture, user_work, user_work_title , user_work_place , '' as connection FROM users WHERE user_id = '%s' limit 0, 1", $communityData['creator_id']);

                Log::debug("SQL : ".$sql);
                $createrStmt = $conn->execute($sql);
                $communityData['created_by'] = $createrStmt->fetch("assoc");
                $communityData['created_by'] = $communityData['created_by']?$communityData['created_by']:[];
                //setting user image
                $communityData['created_by']['user_picture'] = $this->getImgFullPath(@$communityData['created_by']['user_picture']);

                //setting interests
                if($communityData['interests']){

                    // checking interests
                    $sql=sprintf("SELECT interest_id, `text` as interest_name FROM interest_mst WHERE FIND_IN_SET(interest_id, '%s')", $communityData['interests']);

                    Log::debug("SQL : ".$sql);
                    $interestStmt = $conn->execute($sql);

                    while($result = $interestStmt->fetch("assoc"))
                        array_push($communityData['communityInterestArr'], $result);

                }

                // checking attempted user and top scorer
                $sql=sprintf("SELECT cmm.*, us.user_fullname, us.user_email, us.user_name, us.user_firstname, us.user_lastname, us.user_gender, us.user_picture, us.user_work, us.user_work_title  FROM communities_members as cmm left join users as us on us.user_id = cmm.user_id WHERE cmm.community_id = '%s' ", $community_id);

                Log::debug("SQL : ".$sql);
                $participantsStmt = $conn->execute($sql);

                while($result = $participantsStmt->fetch("assoc")){
                    $result['user_picture'] = $this->getImgFullPath($result['user_picture']);
                    $result['role'] = ($result['role'] == 1)?'Admin':($result['role'] == 2 ? 'Moderator':'Member');
                    array_push($communityData['memberData'], $result);
                }


            }

            Log::debug("Ended ...getCommunityByUserAndId Dao");

            return $communityData;
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }
    
    //updating user community participation
    public function addRemoveCommunityParticipation($args, $date)
    {

        Log::debug("Started ...addRemoveCommunityParticipation Dao");
        try{
            $conn = ConnectionManager::get('default');
             $response = array("valid" => false, "msg"=>"Community not found.");

            //checking community
            $sql=sprintf("SELECT * FROM communities WHERE id = '%s'", $args['community_id']);
            Log::debug("SQL : ".$sql);
            $stmt = $conn->execute($sql);
            $communityData = $stmt->fetch("assoc");

            if($communityData){
                //checking community already participant
                $sql=sprintf("SELECT * FROM communities_members WHERE community_id = '%s' AND user_id = '%s'", $args['community_id'], $args['user_id']);
                Log::debug("SQL : ".$sql);
                $stmt = $conn->execute($sql);
                $participantData = $stmt->fetch("assoc");

                if($args['status'] == 2){
                    if(!empty($participantData)){
                        //deleting participation of user
                        $sql=sprintf("DELETE FROM `communities_members` WHERE community_id = '%s' AND user_id = '%s'", $args['community_id'], $args['user_id']);
                        Log::debug("SQL : ".$sql);
                        if($conn->execute($sql)){
                            $response = array("valid" => true, "msg"=>"Participation removed");
                        }else
                            $response["msg"] = "Something went wrong.";
                    }else
                        $response["msg"] = "Not participated.";
                    
                }else if($args['status'] == 1){
                    if(empty($participantData)){
                        //inserting participation of user
                        $sql=sprintf("INSERT INTO `communities_members`(`community_id`, `user_id`) VALUES ('%s', '%s')", $args['community_id'], $args['user_id']);
                        Log::debug("SQL : ".$sql);
                        if($conn->execute($sql)){
                            $response = array("valid" => true, "msg"=>"Participated successfully");
                        }else
                            $response["msg"] = "Something went wrong.";
                    }else
                        $response["msg"] = "Already participated.";

                    
                }
            }

           
            Log::debug("Ended ...addRemoveCommunityParticipation Dao");
            
            return $response;
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }
    
    //updating user community participation role
    public function promoteDemoteCommunityUser($args, $date)
    {

        Log::debug("Started ...promoteDemoteCommunityUser Dao");
        try{
            $conn = ConnectionManager::get('default');
             $response = array("valid" => false, "msg"=>"You do not have authority.");

            //checking community
            $sql=sprintf("SELECT cm.*, us.user_fullname, cmm.role
                FROM  `communities` as cm left join users as us on us.user_id = cm.creator_id left join communities_members as cmm on (cmm.community_id = cm.id AND cmm.user_id = $args[user_id]) WHERE  deleted = 0 and id ='%s' limit 0, 1", $args['community_id']);
            Log::debug("SQL : ".$sql);
            $stmt = $conn->execute($sql);
            $communityData = $stmt->fetch("assoc");

            if($communityData && $communityData['role'] == 1){

                //checking community already participant
                $sql=sprintf("SELECT * FROM communities_members WHERE community_id = '%s' AND user_id = '%s'", $args['community_id'], $args['member_id']);
                Log::debug("SQL : ".$sql);
                $stmt = $conn->execute($sql);
                $participantData = $stmt->fetch("assoc");

                if(!empty($participantData)){
                    if($args['status'] == 4){
                            //deleting participation of user
                            $sql=sprintf("DELETE FROM `communities_members` WHERE community_id = '%s' AND user_id = '%s'", $args['community_id'], $args['member_id']);
                            Log::debug("SQL : ".$sql);
                            if($conn->execute($sql)){
                                $response = array("valid" => true, "msg"=>"Participation removed");
                            }else
                                $response["msg"] = "Something went wrong.";
                        
                    }else if(in_array($args['status'], [1,2,3])){
                        //inserting participation role of user
                        $sql=sprintf("UPDATE communities_members SET role = '%s'  WHERE community_id = '%s' AND user_id = '%s'",$args['status'], $args['community_id'], $args['member_id']);
                        Log::debug("SQL : ".$sql);
                        if($conn->execute($sql)){
                            $response = array("valid" => true, "msg"=>"Updated successfully");
                        }else
                            $response["msg"] = "Something went wrong.";
                    }
                }else
                    $response["msg"] = "Not participated.";
                Log::debug("SQL : ".$sql);
                $conn->execute($sql);
            }

           
            Log::debug("Ended ...promoteDemoteCommunityUser Dao");
            
            return $response;
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }


    //sending invitaion for community
    public function sendCommunityInvitaion($args, $date)
    {

        Log::debug("Started ...sendCommunityInvitaion Dao");
        $response = array("valid" => false, "msg"=>"This community is not open of invitation.");
        try{
            $conn = ConnectionManager::get('default');

            //checking community live duration
            // AND DATE_ADD(start_time, INTERVAL `live_duration` second) >= '%s'
            $sql=sprintf("SELECT * FROM communities WHERE deleted = 0 AND id = '%s'", $args['community_id']);
            Log::debug("SQL : ".$sql);
            $stmt = $conn->execute($sql);
            $communityData = $stmt->fetch("assoc");

            //checking community invitations
            $sql=sprintf("SELECT * FROM communities_invitations WHERE community_id = '%s' AND user_id = '%s' AND sender_user_id = '%s'", $args['community_id'], $args['user_id'], $args['sender_user_id']);
            Log::debug("SQL : ".$sql);
            $stmt = $conn->execute($sql);
            $invitationData = $stmt->fetch("assoc");

            if(!empty($communityData)){

                if(empty($invitationData)){               
                    //inserting invitation of user
                    $sql=sprintf("INSERT INTO `communities_invitations`(`community_id`, `user_id`, `sender_user_id`, `created`) VALUES ('%s', '%s','%s', '%s')", $args['community_id'], $args['user_id'], $args['sender_user_id'], $date);
                    Log::debug("SQL : ".$sql);

                    if($conn->execute($sql)){   
                        $response = array("valid" => true, "msg"=>"Updated");
                    }else
                        $response["msg"] = "Something went wrong.";              
                }else
                    $response["msg"] = "Already invited";
            }
           
            Log::debug("Ended ...sendCommunityInvitaion Dao");
            
            return $response;
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }

    //checking invitation for community
    public function getCommunityInvitedUserList($args)
    {

        Log::debug("Started ...getCommunityInvitedUserList Dao");
        try{
            $conn = ConnectionManager::get('default');
            $codes = new Codes;

            //checking keys for pagination
            $args['page'] = (isset($args['page']) && $args['page'] > 1)?$args['page']:1;
            $args['pageSize'] = (isset($args['pageSize']) && $args['pageSize'] > 0)?$args['pageSize']:10;

            //checking community invitations
            $cond = (isset($args['search_keyword']) && !empty($args['search_keyword']))?" AND (users.user_fullname LIKE '%".$args['search_keyword']."%' OR users.user_name LIKE '%".$args['search_keyword']."%')":"";
            $cond .= " limit ".(($args['page']- 1) * $args['pageSize']).", ".$args['pageSize'];

            $followings_ids = [];
            $friends_ids = [];
            $friend_requests_ids = [];
            $friend_requests_sent_ids = [];
            $fellowsArr = [];

            //checking community invitations users
            $sql=sprintf("SELECT following_id FROM followings WHERE user_id = '%s'", $args['user_id']);
            Log::debug("SQL : ".$sql);
            $followingsStmt = $conn->execute($sql);

            while($result = $followingsStmt->fetch("assoc")) {
                $followings_ids[] = $result['following_id'];
            }
            //checking community invitations users
            $sql=sprintf('SELECT users.user_id FROM friends INNER JOIN users ON (friends.user_one_id = users.user_id AND friends.user_one_id != %1$s) OR (friends.user_two_id = users.user_id AND friends.user_two_id != %1$s) WHERE status = 1 AND (user_one_id = %1$s OR user_two_id = %1$s)', $args['user_id']);
            Log::debug("SQL : ".$sql);
            $friendsStmt = $conn->execute($sql);

            while($result = $friendsStmt->fetch("assoc")) {
                $friends_ids[] = $result['user_id'];
            }
            //checking community invitations users
            $sql=sprintf('SELECT user_one_id FROM friends WHERE status = 0 AND user_two_id = %s', $args['user_id']);
            Log::debug("SQL : ".$sql);
            $friend_requestsStmt = $conn->execute($sql);

            while($result = $friend_requestsStmt->fetch("assoc")) {
                $friend_requests_ids[] = $result['user_one_id'];
            }

            //checking community friend_requests_sent users
            $sql=sprintf('SELECT user_one_id FROM friends WHERE status = 0 AND user_two_id = %s', $args['user_id']);
            Log::debug("SQL : ".$sql);
            $friend_requests_sentStmt = $conn->execute($sql);

            while($result = $friend_requests_sentStmt->fetch("assoc")) {
                $friend_requests_sent_ids[] = $result['user_one_id'];
            }

            $sql="SELECT users.user_id, users.user_work, users.user_work_title, users.user_work_place, users.user_name, users.user_fullname, users.user_firstname, user_lastname, users.user_gender, users.user_picture, cmm.role FROM friends INNER JOIN users ON ((friends.user_one_id = users.user_id AND friends.user_one_id != $args[user_id]) OR (friends.user_two_id = users.user_id AND friends.user_two_id != $args[user_id])) left join communities_members as cmm on (cmm.community_id = $args[community_id] AND cmm.user_id = $args[user_id]) WHERE friends.status = 1 AND (user_one_id = $args[user_id] OR user_two_id = $args[user_id]) $cond";
            
            Log::debug("SQL : ".$sql);
            // echo $sql; exit;
            $fellowsStmt = $conn->execute($sql);

            while($result = $fellowsStmt->fetch("assoc")) {
                $result['user_picture'] = $this->getImgFullPath($result['user_picture']);

                $result['connection'] = $this->connection($args['user_id'], $result['user_id'], $followings_ids, $friends_ids, $friend_requests_ids, $friend_requests_sent_ids);
                $result['is_friend'] = in_array($args['user_id'], $friends_ids)?1:0;
                $result['is_following'] = in_array($args['user_id'], $followings_ids)?1:0;
                $result['role'] = ($result['role'] == 1)?'Admin':($result['role'] == 2 ? 'Moderator':'Member');
                $fellowsArr[] = $result;
            }

            Log::debug("Ended ...getCommunityInvitedUserList Dao");
            
            return $fellowsArr;
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }
    
    private function connection($logged_in_user_id, $user_id, $followings_ids = [], $friends_ids = [], $friend_requests_ids = [], $friend_requests_sent_ids = [])
    {
        /* check if the viewer is the target */
        if ($user_id == $logged_in_user_id) {
            return "me";
        }
        // Arnab mohanty:: on 2nd Jan 2019
        if (in_array($user_id, $followings_ids)) {
            /* the viewer follow the target */
            return "followed";
        }
        /* check if the viewer & the target are friends */
        if (in_array($user_id, $friends_ids)) {
            return "remove";
        }
        /* check if the target sent a request to the viewer */
        if (in_array($user_id, $friend_requests_ids)) {
            return "request";
        }
        /* check if the viewer sent a request to the target */
        if (in_array($user_id, $friend_requests_sent_ids)) {
            return "cancel";
        }
        
        
        /* there is no relation between the viewer & the target */
        return "add";
        
    }




}