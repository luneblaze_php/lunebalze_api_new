<?php

namespace App\DaoLayer;

use App\Utils\Codes;
use Cake\Core\Exception\Exception;
use Cake\Datasource\ConnectionManager;
use Cake\Log\Log;

class NotificationsDao
{

	/**
	 * Get Users Notifiactions by to_user_id and time
	 */
	public function getUsersNotificationsByToUserIdAndTime($toUserId, $time, $limitStart, $limitEnd)
	{
		Log::debug("Started ...getUsersNotificationsByToUserIdAndTime Dao : To User Id : ".$toUserId.", Time : ".$time.", Limit Start : ".$limitStart.
			", Limit End : ".$limitEnd);

		try{
			
			$conn = ConnectionManager::get('default');

			$sql=sprintf("SELECT notifications.*, users.user_id, users.user_name, users.user_firstname, users.user_lastname,users.user_fullname, users.user_gender, users.user_picture FROM notifications LEFT JOIN users ON notifications.from_user_id = users.user_id WHERE (notifications.to_user_id = %s OR notifications.to_user_id = '0') AND notifications.time > %s ORDER BY notifications.time DESC LIMIT %s, %s", $toUserId, $time, $limitStart, $limitEnd);
		
			Log::debug("SQL : ".$sql);

			$stmt = $conn->execute($sql);

			$result = $stmt->fetch("assoc");

			Log::debug("Ended ...getUsersNotificationsByToUserIdAndTime Dao");

			return $result;

		}catch(\Exception $e){
			Log::debug($e);
			throw new Exception($e);
		}
	}   

	/**
	 * Get Notifiaction by to_user_id and action and node_type and unique_id
	 */
	public function getNotificationByToUserIdAndActionAnsNodeTypeAndUniqueId($toUserId, $action, $nodeType, $uniqueId)
	{
		Log::debug("Started ...getNotificationByToUserIdAndActionAnsNodeTypeAndUniqueId Dao : To User Id : ".$toUserId.", Action : ".$action.", Node Type : ".$nodeType.
			", Unique Id : ".$uniqueId);

		try{
			
			$conn = ConnectionManager::get('default');

			$sql=sprintf("SELECT * FROM notifications WHERE to_user_id = %s and action=%s and node_type=%s and unique_id=%s", $toUserId, $action, $nodeType, $uniqueId);
	
			Log::debug("SQL : ".$sql);

			$stmt = $conn->execute($sql);

			$result = $stmt->fetch("assoc");

			Log::debug("Ended ...getNotificationByToUserIdAndActionAnsNodeTypeAndUniqueId Dao");

			return $result;

		}catch(\Exception $e){
			Log::debug($e);
			throw new Exception($e);
		}
	}   

	/**
	 * Save Notifications
	 */
	public function saveNotifications($toUserId, $userId, $action, $nodeType, $nodeUrl, $notifyId, $replyId, $date, $uniqueId)
	{
		Log::debug("Started ...saveNotifications Dao : To User Id : ".$toUserId.", From User Id : ".$userId.", Action : ".$action.", Node Type : ".$nodeType.", Node Url : ".$nodeUrl.", Notify Id : ".$notifyId.", Reply Id : ".$replyId.", Time : ".$date.", Total User Ids : ".$userId.", Unique Id : ".$uniqueId);

		try{
			
			$conn = ConnectionManager::get('default');

			$sql=sprintf("INSERT INTO notifications (to_user_id, from_user_id, action, node_type, node_url, notify_id, reply_id, time, total_user_ids,unique_id) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s,%s)", $toUserId, $userId, $action, $nodeType, $nodeUrl, $notifyId, $replyId, $date, $userId,$uniqueId);

			Log::debug("SQL : ".$sql);

			$conn->execute($sql);

			Log::debug("Ended ...saveNotifications Dao");

		}catch(\Exception $e){
			Log::debug($e);
			throw new Exception($e);
		}
	}     

}