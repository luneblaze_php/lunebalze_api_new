<?php

namespace App\DaoLayer;

use App\Utils\Codes;
use Cake\Core\Exception\Exception;
use Cake\Datasource\ConnectionManager;
use Cake\Log\Log;

class SessionsQaFollowsDao
{

	/**
	 * Save Sessions QA Follows
	 */
	public function saveSessionsQaFollows($userId, $sessionsQaId)
	{
		Log::debug("Started ...saveSessionsQaFollows Dao : User Id : ".$userId.", Blocked Id : ".$sessionsQaId);

		try{
			
			$conn = ConnectionManager::get('default');

			$sql=sprintf("INSERT INTO sessions_qa_follows (user_id, sessions_qa_id) VALUES (%s, %s)", $userId, $sessionsQaId);

			Log::debug("SQL : ".$sql);

			$conn->execute($sql);

			Log::debug("Ended ...saveSessionsQaFollows Dao");

		}catch(\Exception $e){
			Log::debug($e);
			throw new Exception($e);
		}
	}

	/**
	 * Delete Sessions QA Follows using user_id and sessions_qa_id
	 */
	public function deleteSessionsQaFollowsByUserIdAndSessionsQaId($userId, $sessionsQaId)
	{
		Log::debug("Started ...deleteSessionsQaFollowsByUserIdAndSessionsQaId Dao : User Id : ".$userId.", Blocked Id : ".$sessionsQaId);

		try{
			
			$conn = ConnectionManager::get('default');

			$sql=sprintf("DELETE FROM sessions_qa_follows WHERE user_id = %s AND sessions_qa_id = %s", $userId, $sessionsQaId);

			Log::debug("SQL : ".$sql);

			$conn->execute($sql);

			Log::debug("Ended ...deleteSessionsQaFollowsByUserIdAndSessionsQaId Dao");

		}catch(\Exception $e){
			Log::debug($e);
			throw new Exception($e);
		}
	}
   
}