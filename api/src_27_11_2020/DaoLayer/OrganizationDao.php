<?php

namespace App\DaoLayer;

use App\Utils\Codes;
use Cake\Core\Exception\Exception;
use Cake\Datasource\ConnectionManager;
use Cake\Log\Log;

class OrganizationDao
{


    /**
     * 
     */
    public function getOrganizationByOrganizationId($organizationId)
    {
        Log::debug("Started ...getOrganizationByOrganizationId Dao : Organization Id : ".$organizationId);

        try{
            
            $conn = ConnectionManager::get('default');

            $sql=sprintf("select * from organization where id='%s'",$organizationId);          

            Log::debug("SQL : ".$sql);

            $stmt = $conn->execute($sql);

            $result = $stmt->fetch("assoc");

            Log::debug("Ended ...getOrganizationByOrganizationId Dao");

            return $result;
            
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }

    }
}