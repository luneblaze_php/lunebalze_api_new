<?php 
    namespace App\Utils;

use DateInterval;
use DateTime;

class DateUtils {

    /**
     * 
     */
        public function getCurrentDate()
        {
            date_default_timezone_set( 'UTC' );
            $time = time();
            $minutes_to_add = 0;
            $DateTime = new DateTime();
            $DateTime->add(new DateInterval('PT' . $minutes_to_add . 'M'));
            $date = $DateTime->format('Y-m-d H:i:s');   

            return $date;
        }
    }
?>