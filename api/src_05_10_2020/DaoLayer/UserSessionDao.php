<?php

namespace App\DaoLayer;

use App\Utils\Codes;
use App\Utils\DateUtils;
use Cake\Core\Exception\Exception;
use Cake\Datasource\ConnectionManager;
use Cake\Log\Log;
use DateInterval;
use DateTime;

class UserSessionDao
{


    /**
     * 
     */
    public function deleteUserSessionsIfExists($userID,$deviceId)
    {
        Log::debug("Started ...deleteUserSessionsIfExists Dao "." :  User ID :  ".$userID.", Device Id : ".$deviceId);

        try{
            
            $conn = ConnectionManager::get('default');

            $sql=sprintf("DELETE FROM users_sessions
            WHERE user_id='%s' AND device_id='%s'",$userID,$deviceId);
            
            Log::debug("SQL : ".$sql);

            $conn->execute($sql);

            Log::debug("Ended ...deleteUserSessionsIfExists Dao");
            
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }


    /**
     * 
     */
    public function saveUserSessions($sessionToken,$userBrowser,$userOS,$userIP,$userID,$deviceId)
    {
        Log::debug("Started ...saveUserSessions Dao : Session Token : ".$sessionToken.", User Browser : ".$userBrowser.", User OS : ".$userOS.", User IP : ".$userIP.", User ID :  ".$userID.", Device Id : ".$deviceId);

        try{
            
            $conn = ConnectionManager::get('default');

            $dateUtils=  new DateUtils;

            $sql=sprintf("INSERT INTO users_sessions
            (session_token, session_date, user_id, 
            user_browser, user_os, user_ip, device_id)
            VALUES('%s', '%s', '%s', '%s', '%s', '%s', '%s')",
            $sessionToken,
            $dateUtils->getCurrentDate(),
            $userID,
            $userBrowser,
            $userOS,
            $userIP,
            $deviceId);

            $conn->execute($sql);

            $sql="SELECT LAST_INSERT_ID()";

            $stmt = $conn->execute($sql);

            $result = $stmt->fetch("assoc");

            return $result['LAST_INSERT_ID()'];

            Log::debug("Ended ...saveUserSessions Dao");
            
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }

}