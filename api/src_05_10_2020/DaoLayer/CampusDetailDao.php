<?php

namespace App\DaoLayer;

use App\Utils\Codes;
use Cake\Core\Exception\Exception;
use Cake\Datasource\ConnectionManager;
use Cake\Log\Log;

class CampusDetailDao
{
    /**
     * 
     */
    public function getCampusDetailByScheduleId($scheduleId, $offset)
    {
        Log::debug("Started ...getCampusDetailByScheduleId Dao : Schedule Id : ".$scheduleId);

        try{

            $codes = new Codes;
            
            $conn = ConnectionManager::get('default');

            $sql=sprintf("SELECT * FROM `campus_detail` WHERE `scheduleid`='%s' order by id desc limit %s ,%s",$scheduleId,$offset,$codes->MAX_RESULTS);          

            Log::debug("SQL : ".$sql);

            $stmt = $conn->execute($sql);

            $results = array();

            while($result = $stmt->fetch("assoc"))
                array_push($results,$result);

            Log::debug("Ended ...getCampusDetailByScheduleId Dao");

            return $results;
            
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }

    }
}