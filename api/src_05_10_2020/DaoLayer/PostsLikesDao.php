<?php

namespace App\DaoLayer;

use App\Utils\Codes;
use Cake\Core\Exception\Exception;
use Cake\Datasource\ConnectionManager;
use Cake\Log\Log;

class PostsLikesDao
{

    /**
     * 
     */
    public function getPostsLikesByUserIdPostId($userId,$postId)
    {
        Log::debug("Started ...getPostsLikesByUserIdPostId Dao : Post Id : ".$postId.", User Id : ".$userId);

        try{

            $conn = ConnectionManager::get('default');
            $sql = null;

            if(isset($userId))
                $sql = sprintf("SELECT * FROM posts_likes WHERE user_id = '%s' AND post_id = '%s'",$userId,$postId);
            else
                $sql = sprintf("SELECT * FROM posts_likes WHERE post_id = '%s'",$postId);


            Log::debug("SQL : ".$sql);

            $results = array();

            $stmt = $conn->execute($sql);

            while($result = $stmt->fetch("assoc"))
                array_push($results,$result);

            Log::debug("Ended ...getPostsLikesByUserIdPostId Dao");

            return $results;
            
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }
    
}