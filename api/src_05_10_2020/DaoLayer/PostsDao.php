<?php

namespace App\DaoLayer;

use App\Utils\Codes;
use Cake\Core\Exception\Exception;
use Cake\Datasource\ConnectionManager;
use Cake\Log\Log;

class PostsDao
{

    /**
     * 
     */
    public function getPostDetailByPostIdPostType($postId,$postType)
    {
        Log::debug("Started ...getPostDetailByPostIdPostType Dao : Post Id : ".$postId.", Post Type : ".$postType);

        try{

            $conn = ConnectionManager::get('default');

            $sql = sprintf("SELECT posts.*, users.user_name,users.privacy_connection_request,users.user_work_title, 
            users.user_fullname, users.user_gender, users.user_picture, 
            users.user_picture_id, users.user_cover_id, users.user_verified, 
            users.user_subscribed, users.user_pinned_post, pages.*, 
            groups.group_name, groups.group_picture_id, 
            groups.group_cover_id, groups.group_title, 
            groups.group_admin, groups.group_pinned_post 
            FROM posts LEFT JOIN users ON posts.user_id = users.user_id 
            AND posts.user_type='%s' LEFT JOIN pages ON posts.user_id = pages.page_id 
            AND posts.user_type = 'page' LEFT JOIN groups ON posts.in_group = '1' 
            AND posts.group_id = groups.group_id WHERE NOT (users.user_name <=> NULL 
            AND pages.page_name <=> NULL) AND posts.post_id = '%s'",$postType,$postId);

            Log::debug("SQL : ".$sql);

            $stmt = $conn->execute($sql);

            $result = $stmt->fetch("assoc");

            Log::debug("Ended ...getPostDetailByPostIdPostType Dao");

            return $result;
            
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }

    }



    /**
     * 
     */
    public function getPostsByOrganizationIdUserTypePaymentFlag($organizationId, $userType, $paymentFlag, $offset)
    {
        Log::debug("Started ...getPostsByOrganizationIdUserTypePaymentFlag Dao : Organization Id : ".$organizationId.", User Type : ".$userType.", Payment Flag : ".$paymentFlag);

        try{

            $codes = new Codes;
            
            $conn = ConnectionManager::get('default');

            if(isset($paymentFlag))
                $sql=sprintf("SELECT * FROM `posts` WHERE `organisation_id`='%s' and user_type='%s' and payment=%s order by post_id desc limit %s,%s",$organizationId,$userType,$paymentFlag,$offset,$codes->MAX_RESULTS);          
            else 
                $sql=sprintf("SELECT * FROM `posts` WHERE `organisation_id`='%s' and user_type='%s' order by post_id desc limit %s,%s",$organizationId,$userType,$offset,$codes->MAX_RESULTS);          

            Log::debug("SQL : ".$sql);

            $stmt = $conn->execute($sql);

            $results = array();

            while($result = $stmt->fetch("assoc"))
                array_push($results,$result);

            Log::debug("Ended ...getPostsByOrganizationIdUserTypePaymentFlag Dao");

            return $results;
            
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }

    }
}