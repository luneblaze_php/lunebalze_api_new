<?php

namespace App\DaoLayer;

use App\Utils\Codes;
use Cake\Core\Exception\Exception;
use Cake\Datasource\ConnectionManager;
use Cake\Log\Log;

class UserOrganizerDao
{


    /**
     * 
     */
    public function getUserOrganizerByOrganizationIdAndUserId($organizationId,$userId)
    {
        Log::debug("Started ...getUserOrganizerByOrganizationIdAndUserId Dao : Organization Id : ".$organizationId.", User Id : ".$userId);

        try{
            
            $conn = ConnectionManager::get('default');

            $sql = null;
            if(isset($userId))
                $sql=sprintf("SELECT * FROM user_organizer WHERE organizer='%s' AND user_id='%s'",$organizationId,$userId);          
            else 
            $sql=sprintf("SELECT * FROM user_organizer WHERE organizer='%s'",$organizationId);          

            Log::debug("SQL : ".$sql);

            $stmt = $conn->execute($sql);

            $results = array();

            while($result = $stmt->fetch("assoc"))
                array_push($results,$result);

            Log::debug("Ended ...getUserOrganizerByOrganizationIdAndUserId Dao");

            return $results;
            
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }

    }
}