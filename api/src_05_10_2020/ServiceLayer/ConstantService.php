<?php

namespace App\ServiceLayer;

use App\DaoLayer\CampusDetailDao;
use App\Utils\Codes;
use App\Utils\DataValidation;
use Cake\Core\Exception\Exception;
use App\DaoLayer\InterestMstDao;
use App\DaoLayer\OrganizationDao;
use App\DaoLayer\OrganizationTypeDao;
use App\DaoLayer\PostsDao;
use App\DaoLayer\PostsLikesDao;
use App\DaoLayer\PostsSavedDao;
use App\DaoLayer\ScheduleCampusDao;
use App\DaoLayer\UserInterestsDao;
use App\DaoLayer\UserOrganizerDao;
use App\DaoLayer\UsersDao;
use App\Utils\ImageUtils;
use Cake\Log\Log;

class ConstantService
{

    /**
     * 
     */
    public function get_user_interest($inputJson)
    {
           Log::debug("Started ... get_user_interest Service : ".json_encode($inputJson));
            try{
                $userInterestDao = new UserInterestsDao;
                $codes = new Codes;
                $dataValidation = new DataValidation;
                $imageUtils = new ImageUtils;

                //Validation to be done here ---
                if(!isset($inputJson['user_id']))
                {
                    $response = [
                        'status'=>$codes->RC_ERROR,
                        'message'=>$codes->RM_ALL_FIELDS_TO_BE_PRESENT,
                        'data'=>null
                    ];
    
                    return $response;                
                }
    
                if($dataValidation->isEmpty($inputJson['user_id']))
                {
                    
                    $response = [
                        'status'=>$codes->RC_ERROR,
                        'message'=>"Please enter user id",
                        'data'=>null
                    ];
    
                    return $response;
                }
                //Validation to be done here ---

                $userInterests = $userInterestDao->getUserInterestsByUserId($inputJson['user_id']);


                $userInterestResponses = array();


                foreach($userInterests as $ui)
                {
                    //Fetch the total attendees
                    $totalAttendees = $userInterestDao->getTotalAttendees($ui['interest']);

                    //Get Picture 
                    $picture = $imageUtils->getPicture($ui['image'],'');

                    
                    $isFollow = count($userInterestDao->getUserInterestsByInterestUserId($ui['interest'],$inputJson['user_id'])) > 0 ? '1' : '0';

                    
                    $data = [
                        'interest_id'=>$ui['interest'],
                        'text'=>$ui['text'],
                        'image'=>$picture,
                        'no_of_followers'=>$totalAttendees,
                        'i_follow_related'=>$isFollow
                    ];

                    array_push($userInterestResponses,$data);

                }

                $response = [
                    'status'=>$codes->RC_SUCCESS,
                    'message'=>"user interests",
                    'data'=>$userInterestResponses
                ];
    
                Log::debug("Ended ... get_user_interest Service : ");
    
                return $response;            
            }catch(\Exception $e){
                Log::debug($e);
                throw new Exception($e);
            }
    }





    public function get_interest($inputJson)
    {
           Log::debug("Started ... get_interest Service : ".json_encode($inputJson));
            try{
                $interestMstDao = new InterestMstDao;
                $codes = new Codes;
                $dataValidation = new DataValidation;
                
                //Fetch the interests
                $interests = $interestMstDao->getInterestMasters(null,$codes->MAX_LIMIT);
                
                $response = [
                    'status'=>$codes->RC_SUCCESS,
                    'message'=>"Interest List",
                    'data'=>$interests
                ];
    
                Log::debug("Ended ... get_interest Service : ");
    
                return $response;            
            }catch(\Exception $e){
                Log::debug($e);
                throw new Exception($e);
            }
    }

    /**
     * 
     */
    public function search_interest($inputJson)
    {
        Log::debug("Started ... search_interest Service : ".json_encode($inputJson));
        try{
            $interestMstDao = new InterestMstDao;
            $codes = new Codes;
            $dataValidation = new DataValidation;

            //Validation to be done here

            if(!isset($inputJson['text']) || !isset($inputJson['offset']))
            {
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>$codes->RM_ALL_FIELDS_TO_BE_PRESENT,
                    'data'=>null
                ];

                return $response;                
            }

            if($dataValidation->isEmpty($inputJson['text']))
            {
                
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>"Please enter text",
                    'data'=>null
                ];

                return $response;
            }
            if($dataValidation->isEmpty($inputJson['offset']))
            {
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>"Please enter offset",
                    'data'=>null
                ];

                return $response;
            }
            //Validation done -- 

            $offset = $inputJson['offset'] * $codes->MAX_RESULTS;
            
            //Fetch the interests
            $interests = $interestMstDao->getInterestMasters($inputJson['text'],$offset);

            $data = array();

            foreach($interests as $int)
            {
                array_push($data, [
                    'interest_id'=>$int['interest_id'],
                    'parent_id'=>$int['parent_id'],
                    'text'=>$int['text']
                ]);
            }
            
            $response = [
                'status'=>$codes->RC_SUCCESS,
                'message'=>"",
                'data'=>$data
            ];

            Log::debug("Ended ... search_interest Service : ");

            return $response;            
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }

    /**
     * 
     */
    public function get_organization($inputJson)
    {
        Log::debug("Started ... get_organization Service : ".json_encode($inputJson));
        try{

            $codes = new Codes;
            $dataValidation = new DataValidation;
            $organizationDao = new OrganizationDao;
            $imageUtils = new ImageUtils;
            $organizationTypeDao = new OrganizationTypeDao;
            $userOrganizerDao = new UserOrganizerDao;
            $scheduleCampusDao = new ScheduleCampusDao;
            $campusDetailDao = new CampusDetailDao;
            $postsDao = new PostsDao;



            //Validation to be done here
            if(!isset($inputJson['organization_id']))
            {
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'Organization Id is not present',
                    'data'=>null
                ];

                return $response;            
            }

            if($dataValidation->isEmpty($inputJson['organization_id']))
            {
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'Organization Id is not present',
                    'data'=>null
                ];

                return $response;     
            }

            if(!isset($inputJson['user_id']))
            {
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'User Id is not present',
                    'data'=>null
                ];

                return $response;            
            }

            if($dataValidation->isEmpty($inputJson['user_id']))
            {
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'User Id is not present',
                    'data'=>null
                ];

                return $response;     
            }

            if(!isset($inputJson['posttype']))
            {
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'Post Type is not present',
                    'data'=>null
                ];

                return $response;                
            }

            $validatePostType = $dataValidation->validatePostType($inputJson['posttype']);

            if($validatePostType != 'Success'){
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => $validatePostType,
                    'data' => null
                ];
                return $response;
            }

            //End of Validation -- Starting business logic
            if(!isset($inputJson['offset'])){
                $inputJson['offset']=0;
            }

            $offset = $inputJson['offset'] * $codes->MAX_RESULTS;

            //Initialize data as null
            $data = null;

            switch($inputJson['posttype'])
            {
                case 'home':
                    $organization = $organizationDao->getOrganizationByOrganizationId($inputJson['organization_id']);

                    if($organization == null)
                    {
                        $response = [
                            'status' => $codes->RC_ERROR,
                            'message' => 'No organization found',
                            'data' => null
                        ];
                        return $response;
                    }

                    Log::debug("Fetched Organization : ".json_encode($organization));

                    //Fetch the logo
                    $organization['Logo'] = $imageUtils->getPicture($organization['Logo'],'');
                    
                    $organizationType = $organizationTypeDao->getOrganizationTypeByOrganizationTypeId($organization['Type']);
                    
                    Log::debug(" Organization Type : ".json_encode($organizationType));

                    $organization['Typename']=$organizationType['type'];

                    $userOrganizer = $userOrganizerDao->getUserOrganizerByOrganizationIdAndUserId($inputJson['organization_id'],$inputJson['user_id']);

                    Log::debug(" User Organizer : ".json_encode($userOrganizer));

                    if(count($userOrganizer) > 0)
                        $organization['is_following'] = 1;

                    $organization['totalfollower'] = count($userOrganizer);

                    //Fetch schedule campus using organization id

                    $scheduleCampuses = $scheduleCampusDao->getScheduleCampusByOrganizationId($inputJson['organization_id']);

                    Log::debug(" Schedule Campuses : ".json_encode($scheduleCampuses));

                    $organization['campus'] = array();

                    foreach($scheduleCampuses as $scheduleCampus)
                    {
                        if(!isset($scheduleCampus['scheduleid'])){
                            Log::debug(" Schedule Id is null : ".json_encode($scheduleCampus));
                            continue;
                        }
                        
                        $campusDetails = $campusDetailDao->getCampusDetailByScheduleId($scheduleCampus['scheduleid'],$offset);
                    
                        Log::debug(" Campus Details : ".json_encode($campusDetails));

                        $organization['jobs'] = array();

                        foreach($campusDetails as $campusDetail)
                            array_push($organization['jobs'],$campusDetail);

                        array_push($organization['campus'],$scheduleCampus);
                    }

                    $data = $organization;

                break;


                case 'ads':
                    $posts = $postsDao->getPostsByOrganizationIdUserTypePaymentFlag($inputJson['organization_id'],'ad','1',$offset);
                    
                    $data['ads'] = array();

                    Log::debug("Posts : ".json_encode($posts));

                    foreach($posts as $post)
                    {
                        if(!isset($post)){
                            Log::debug(" Post is null : ");
                            continue;
                        }

                        //Fetch the post details
                        $postDetail = $this->getOrganizationPost($inputJson['user_id'],$post['post_id'],'ad');

                        if($postDetail == null){
                            Log::debug("Post detail not found");
                            continue;
                        }

                        $postDetail['post_type']='ad';

                        $organization = $organizationDao->getOrganizationByOrganizationId($inputJson['organization_id']);
                        
                        $organization['Logo']=$imageUtils->getPicture($organization['Logo'],'');

                        $organizationType = $organizationTypeDao->getOrganizationTypeByOrganizationTypeId($organization['Type']);
                        
                        $organization['Typename']=$organizationType['type'];
                        
						$post['organization_data']=$organization;
						
						array_push($data['ads'], $post);
                    }

                break;

                case 'posts':
                    $posts = $postsDao->getPostsByOrganizationIdUserTypePaymentFlag($inputJson['organization_id'],'organization',null,$offset);

                    Log::debug("Posts : ".json_encode($posts));

                    $data['post'] = array();

                    foreach($posts as $post)
                    {
                        if(!isset($post)){
                            Log::debug(" Post is null : ");
                            continue;
                        }

                        //Fetch the post details
                        $postDetail = $this->getOrganizationPost($inputJson['user_id'],$post['post_id'],'organization');

                        if($postDetail == null){
                            Log::debug("Post detail not found");
                            continue;
                        }

                        $postDetail['post_type']='organisation_post';

                        $organization = $organizationDao->getOrganizationByOrganizationId($inputJson['organization_id']);
                        
                        $organization['Logo']=$imageUtils->getPicture($organization['Logo'],'');

                        $organizationType = $organizationTypeDao->getOrganizationTypeByOrganizationTypeId($organization['Type']);
                        
                        $organization['Typename']=$organizationType['type'];
                        
						$post['organization_data']=$organization;
						
						array_push($data['post'], $post);
                    }

                break;

            }
            
            $response = [
                'status'=>$codes->RC_SUCCESS,
                'message'=>"",
                'data'=>$data
            ];

            Log::debug("Ended ... get_organization Service : ");

            return $response;            
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }



    private function getOrganizationPost($userId,$postId,$postType)
    {

        $postsDao = new PostsDao;
        $postsLikesDao = new PostsLikesDao;
        $postsSavedDao = new PostsSavedDao;
        $codes = new Codes;
        $imageUtils = new ImageUtils;
        $usersDao = new UsersDao;


        //Fetch the posts dao
        $post = $postsDao->getPostDetailByPostIdPostType($postId,$postType);

        Log::debug("Post : ".json_encode($post));
        
        if(!isset($post))
            return null;
        if(count($post) == 0)
            return null;

        // Check if the page has been deleted
        if ($post['user_type'] == "page" && !$post['page_admin']) 
            return null;
        // Get the author
        $post['author_id'] = ($post['user_type'] == "page") ? $post['page_admin'] : $post['user_id'];
        $post['is_page_admin'] = ($userId == $post['page_admin']) ? true : false;
        $post['is_group_admin'] = ($userId == $post['group_admin']) ? true : false;

        // Check the post author type 
        if ($post['user_type'] == "organization") {
            /* user */
            $post['post_author_picture'] = $imageUtils->getPicture($post['user_picture'], $post['user_gender']);
            $post['post_author_url'] = $codes->SYSTEM_URL . '/' . $post['user_name'];
            $post['post_author_name'] = $post['user_fullname'];
            $post['post_author_verified'] = $post['user_verified'];
            $post['pinned'] = ((!$post['in_group'] && $post['post_id'] == $post['user_pinned_post']) || ($post['in_group'] && $post['post_id'] == $post['group_pinned_post'])) ? true : false;
        } else {
            /* page */
            $post['post_author_picture'] = $imageUtils->getPicture($post['page_picture'], "page");
            $post['post_author_url'] = $codes->SYSTEM_URL . '/pages/' . $post['page_name'];
            $post['post_author_name'] = $post['page_title'];
            $post['post_author_verified'] = $post['page_verified'];
            $post['pinned'] = ($post['post_id'] == $post['page_pinned_post']) ? true : false;
        }

        // Check if viewer can manage post [Edit|Pin|Delete] 
        $post['manage_post'] = false;
        // if ($this->_logged_in) {
        //     // Viewer is (admins|moderators)] 
        //     //TODO: 
        //     // if ($this->_data['user_group'] < 3) {
        //     //     $post['manage_post'] = true;
        //     // }
        //     // Viewer is the author of post || page admin 
        //     if ($userId == $post['author_id']) {
        //         $post['manage_post'] = true;
        //     }
        //     // Viewer is the admin of the group of the post 
        //     if ($post['in_group'] && $post['is_group_admin']) {
        //         $post['manage_post'] = true;
        //     }
        // }
        // Check if viewer [liked|saved] this post 
        $post['i_save'] = false;
        $post['i_like'] = 0;
        $post['liked_users'] = array();

        $postsLikes = $postsLikesDao->getPostsLikesByUserIdPostId($userId,$postId);

        Log::debug("Posts Likes : ".json_encode($postsLikes));

        foreach($postsLikes as $postLike)
        {
            $userLiked = $usersDao->getUserByUserId($postLike['user_id']);

            if(!isset($userLiked)){
                Log::debug("User who liked is not found");
                continue;
            }

            array_push($post['liked_users'],$userLiked);
        }

        $post['total_likes'] = count($post['liked_users']);
        
        // if ($this->_logged_in) {
            // Save 
            $postsSaved = $postsSavedDao->getPostsSavedByUserIdPostId($userId,$postId);

            if(count($postsSaved) > 0)
                $post['i_save'] = true;

            // Like 
            $postsLiked = $postsLikesDao->getPostsLikesByUserIdPostId($userId,$postId);

            if (count($postsLiked) > 0) 
                $post['i_like'] = 1;
        // }

        return $post;

        /* check privacy */
        // if ($pass_privacy_check || $this->_check_privacy($post['privacy'], $post['author_id'])) {
        //     return $post;
        // }
        // return false;
    }




}