<?php

namespace App\DaoLayer;

use App\Utils\Codes;
use Cake\Core\Exception\Exception;
use Cake\Datasource\ConnectionManager;
use Cake\Log\Log;

class InterestMstDao
{


    /**
     * 
     */
    public function getInterestMasters($text, $offset)
    {
        Log::debug("Started ...getInterestMasters Dao");

        try{
            
            $conn = ConnectionManager::get('default');

            $sql=null;

            if($text==null)
            {
                $sql=sprintf("SELECT * FROM `interest_mst` AS im WHERE im.`status` = '1' 
                order by text asc LIMIT %s",$offset);
            }
            else 
            {
                $str = $text.'%';
                $sql=sprintf("SELECT * FROM `interest_mst` AS im WHERE im.`status` = '1' 
                AND im.`text` LIKE '%s' order by text asc LIMIT %s",$str,$offset);
            }

            Log::debug("SQL : ".$sql);

            $stmt = $conn->execute($sql);

            $results = array();

            while($result = $stmt->fetch("assoc")){
                array_push($results,$result);
            }

            Log::debug("Ended ...getInterestMasters Dao");

            return $results;
            
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }

    }
}