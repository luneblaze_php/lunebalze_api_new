<?php

namespace App\DaoLayer;

use App\Utils\Codes;
use App\Utils\DateUtils;
use Cake\Core\Exception\Exception;
use Cake\Datasource\ConnectionManager;
use Cake\Log\Log;
use DateInterval;
use DateTime;

class UsersDao
{


    /**
     * 
     */
    public function updateStatusByUserId($user_id)
    {
        Log::debug("Started ...updateStatusByUserId Dao : User Id : ".$user_id);

        try{
            
            $conn = ConnectionManager::get('default');

            $sql=sprintf("UPDATE `users` SET `user_activated`='1' WHERE `user_id`='%s'",$user_id);

            Log::debug("SQL : ".$sql);

            $conn->execute($sql);

            Log::debug("Ended ...updateStatusByUserId Dao");

        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }


    public function getFriendIdsByUserId($userId)
    {
        Log::debug("Started ...getFriendIdsByUserId Dao");

        try{
            
            $conn = ConnectionManager::get('default');

            $sql=sprintf('SELECT users.user_id FROM friends INNER JOIN users 
            ON (friends.user_one_id = users.user_id 
            AND friends.user_one_id != %1$s) 
            OR (friends.user_two_id = users.user_id 
            AND friends.user_two_id != %1$s) 
            WHERE status = 1 
            AND (user_one_id = %1$s OR user_two_id = %1$s)',$userId);

            Log::debug("SQL : ".$sql);

            $stmt = $conn->execute($sql);

            $results = array();

            while($result = $stmt->fetch("assoc")) {
                array_push($results,$result);
            }

            Log::debug("Ended ...getFriendIdsByUserId Dao");

            return $results;
            
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }

    /**
     * 
     */
    public function getFriendRequestsSentByUserId($userId,$offset,$maxResults)
    {
        Log::debug("Started ...getFriendRequestsSentByUserId Dao");

        try{
            
            $conn = ConnectionManager::get('default');

            $sql=sprintf("SELECT friends.user_two_id as user_id, users.user_name, 
            users.user_fullname, users.user_gender, users.user_picture,
            users.user_work,users.user_work_title,users.user_work_place,
            users.user_current_city 
            FROM friends INNER JOIN users 
            ON friends.user_two_id = users.user_id 
            WHERE friends.status = 0 AND friends.user_one_id = %s 
            LIMIT %s, %s", 
            $userId, 
            $offset, 
            $maxResults);

            Log::debug("SQL : ".$sql);

            $stmt = $conn->execute($sql);

            $results = array();

            while($result = $stmt->fetch("assoc")) {
                array_push($results,$result);
            }

            Log::debug("Ended ...getFriendRequestsSentByUserId Dao");

            return $results;
            
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }



    public function getUserByUserId($userId)
    {
        Log::debug("Started ...updateUserByUserId Dao");

        try{
            
            $conn = ConnectionManager::get('default');

            $sql=sprintf("SELECT * from users where user_id = '%s'", $userId);

            Log::debug("SQL : ".$sql);

            $stmt = $conn->execute($sql);

            $result = $stmt->fetch("assoc");

            Log::debug("Ended ...updateUserByUserId Dao");

            return $result;
            
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }

    }
    


    /**
     * 
     */
    public function updateUserByUserId($args,$venue_id,$user_id)
    {
        Log::debug("Started ...updateUserByUserId Dao");

        try{
            
            $conn = ConnectionManager::get('default');

            $sql=sprintf("UPDATE users SET user_work = '%s' ,
            user_work_title = '%s' ,  user_work_place = '%s',oraganisation_id='%s'
            WHERE user_id = '%s'", $args['user_work'],$args['work_title'],
            $args['work_place'],$venue_id, 
            $user_id);

            Log::debug("SQL : ".$sql);

            $stmt = $conn->execute($sql);

            Log::debug("Ended ...updateUserByUserId Dao");
            
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }


    /**
     * 
     */
    public function updateLoginOtp($userId,$otp)
    {
        Log::debug("Started ...updateLoginOtp Dao : User Id : ".$userId.", OTP : ".$otp);

        try{            
            $conn = ConnectionManager::get('default');

            $sql=sprintf("UPDATE `users` SET login_otp = '%s' WHERE user_id = '%s' ", 
            $otp, $userId);

            Log::debug("SQL : ".$sql);

            $conn->execute($sql);

            Log::debug("Ended ...updateLoginOtp Dao");
            
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }

    public function updateLastLoginByUserId($userId)
    {
        Log::debug("Started ...updateLastLoginByUserId Dao : User Id : ".$userId);

        try{

            $dateUtils = new DateUtils;
            
            $conn = ConnectionManager::get('default');

            $sql=sprintf("UPDATE users SET user_last_login ='%s'
            WHERE user_id = '%s'", $dateUtils->getCurrentDate(), $userId);

            Log::debug("SQL : ".$sql);

            $conn->execute($sql);

            Log::debug("Ended ...updateLastLoginByUserId Dao");
            
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }



    /**
     * 
     */
    public function getUsersByUserEmailOrUserPhoneAndPassword($emPhone,$password)
    {
        Log::debug("Started ...getUsersByUserEmailOrUserPhoneAndPassword Dao : Email / Phone  : ".$emPhone." : Password : ".$password);

        try{
            
            $conn = ConnectionManager::get('default');

            $sql=sprintf("SELECT * FROM users WHERE  (user_email = '%s' OR user_phone= '%s') AND user_password = '%s'", $emPhone,$emPhone,md5($password));

            Log::debug("SQL : ".$sql);

            $stmt = $conn->execute($sql);

            $result = $stmt->fetch("assoc");

            Log::debug("Ended ...getUsersByUserEmailOrUserPhoneAndPassword Dao");

            return $result;
            
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }        

    /**
     * 
     */
    public function getUsersByUserEmail($email)
    {
        Log::debug("Started ...getUsersByUserEmail Dao");

        try{
            
            $conn = ConnectionManager::get('default');

            $sql=sprintf("SELECT * FROM users WHERE  user_email = '%s' and user_activated='1'", $email);

            Log::debug("SQL : ".$sql);

            $stmt = $conn->execute($sql);

            $result = $stmt->fetch("assoc");

            Log::debug("Ended ...getUsersByUserEmail Dao");

            return $result;
            
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }        
    /**
     * 
     */
    public function getUsersByUserPhone($mobile)
    {
        Log::debug("Started ...getUsersByUserPhone Dao");

        try{
            
            $conn = ConnectionManager::get('default');

            $sql=sprintf("SELECT * FROM users WHERE  user_phone = '%s' and user_activated='1'", $mobile);

            Log::debug("SQL : ".$sql);

            $stmt = $conn->execute($sql);

            $result = $stmt->fetch("assoc");

            Log::debug("Ended ...getUsersByUserPhone Dao");

            return $result;
            
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }        

    /**
     * Save user
     */
    public function saveUser($user_name, $args, $user_full_name, $date, $activation_key)
    {   
        Log::debug("Started ...saveUser Dao:  Username : ".$user_name.", User Full Name : ".$user_full_name." Date : ".$date.", Activation Key : ".$activation_key.", Arguments : ".json_encode($args));

        try{

        $conn = ConnectionManager::get('default');

        $sql=sprintf("INSERT INTO users (user_name, user_email, user_phone, user_password, current_FOI, user_firstname, user_lastname, user_fullname, user_gender, user_registered, user_activation_key, user_birthdate,privacy_connection_request,user_privacy_friends,privacy_tag,privacy_invite_session,privacy_invite_conduct_session,privacy_invite_answer,privacy_invite_interest,privacy_message,privacy_choices,user_privacy_basic,privacy_content) VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s','public','friends','friends','friends','friends','friends','friends','public','public','public','public')", 
        $user_name.uniqid(), $args['email'], $args['phone'], md5($args['password']), $args['current_FOI'], ucwords($args['first_name']), ucwords($args['last_name']), $user_full_name, $args['gender'], $date, $activation_key, $args['birth_date']);

        Log::debug("SQL : ".$sql);

        $conn->execute($sql);
        
        $sql="SELECT LAST_INSERT_ID()";
        
        $stmt = $conn->execute($sql);
        
        $res = $stmt->fetch();
       
        Log::debug("Ended ...saveUser Dao");
        
        return $res[0];
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }

}