<?php

namespace App\DaoLayer;

use App\Utils\Codes;
use Cake\Core\Exception\Exception;
use Cake\Datasource\ConnectionManager;
use Cake\Log\Log;

class UserInterestsDao
{

    

    /**
     * 
     */
    public function getUserInterestsByInterestUserId($interestId,$userId)
    {
        Log::debug("Started ...getUserInterestsByInterestUserId Dao : Interest Id : ".$interestId." , User Id : ".$userId);

        try{
            
            $conn = ConnectionManager::get('default');

            $sql=sprintf("SELECT * FROM `user_interest` WHERE interest = %s AND user_id = %s", 
            $interestId, $userId);

            Log::debug("SQL : ".$sql);

            $stmt = $conn->execute($sql);

            $result = $stmt->fetch("assoc");

            Log::debug("Ended ...getUserInterestsByInterestUserId Dao");

            return $result;
            
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }

    public function getTotalAttendees($interestId)
    {
        Log::debug("Started ...getTotalAttendees Dao : Interest Id : ".$interestId);

        try{
            
            $conn = ConnectionManager::get('default');

            $sql=sprintf("SELECT count(*) AS cnt2 FROM `user_interest` WHERE
            `interest` = %s AND `status` = '1' ", $interestId);

            Log::debug("SQL : ".$sql);

            $stmt = $conn->execute($sql);


            $result = $stmt->fetch("assoc");


            Log::debug("Ended ...getTotalAttendees Dao");

            return $result;
            
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }

    }


    /**
     * 
     */
    public function getUserInterestsByUserId($userId)
    {
        Log::debug("Started ...getUserInterests Dao");

        try{
            
            $conn = ConnectionManager::get('default');

            $sql=sprintf('SELECT * FROM user_interest 
            INNER JOIN interest_mst ON user_interest.interest = interest_mst.interest_id
            WHERE user_interest.user_id = %s AND user_interest.status = 1 ',
            $userId);

            Log::debug("SQL : ".$sql);

            $stmt = $conn->execute($sql);

            $results = array();

            while($result = $stmt->fetch("assoc")){
                array_push($results,$result);
            }

            Log::debug("Ended ...getUserInterests Dao");

            return $results;
            
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }

    }
}