<?php
namespace App\Utils;

use App\Utils\Codes;
use App\Utils\ServerUtils;

class ImageUtils {

    public static function getPicture($picture, $type)
    {
        $codes = new Codes;
        $serverUtils = new ServerUtils;

        $systemUpload = $serverUtils->getSystemUpload();
        
        if ($picture == "") {
            switch ($type) {
                case 'male':
                    $picture = $codes->SYSTEM_URL . '/content/themes/' . $codes->SYSTEM_THEME . '/images/blank_profile_male.jpg';
                    break;

                case 'female':
                    $picture = $codes->SYSTEM_URL . '/content/themes/' . $codes->SYSTEM_THEME . '/images/blank_profile_female.jpg';
                    break;

                case 'page':
                    $picture = $codes->SYSTEM_URL . '/content/themes/' . $codes->SYSTEM_THEME . '/images/blank_page.png';
                    break;

                case 'group':
                    $picture = $codes->SYSTEM_URL . '/content/themes/' . $codes->SYSTEM_THEME . '/images/blank_group.png';
                    break;

                case 'game':
                    $picture = $codes->SYSTEM_URL . '/content/themes/' . $codes->SYSTEM_THEME . '/images/blank_game.png';
                    break;

                case 'package':
                    $picture = $codes->SYSTEM_URL . '/content/themes/' . $codes->SYSTEM_THEME . '/images/blank_package.png';
                    break;
            }
        } else {
            $picture = $systemUpload . '/' . $picture;
        }
        return $picture;
    }

}