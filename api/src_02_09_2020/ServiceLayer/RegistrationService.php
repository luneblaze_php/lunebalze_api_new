<?php

namespace App\ServiceLayer;

use App\ServiceLayer\SMSService;
use App\Authentication\AuthService;
use App\Utils\Codes;
use App\Utils\DataValidation;
use Cake\Core\Exception\Exception;
use App\DaoLayer\CitiesDao;
use App\DaoLayer\FollowingsDao;
use App\DaoLayer\PhoneOtpLogDao;
use App\DaoLayer\UserDevicesDao;
use App\DaoLayer\UsersDao;
use App\DaoLayer\UserSessionDao;
use App\DaoLayer\UsersExtraInfoDao;
use App\Utils\DateUtils;
use App\Utils\ImageUtils;
use App\Utils\NotificationUtils;
use Cake\Log\Log;

class RegistrationService
{

    /**
     * 
     */
    public function signup_verify($inputJson)
    {
        Log::debug("Started ... signup_verify Service : " . json_encode($inputJson));
        try {
            $codes = new Codes;
            $dataValidation = new DataValidation;
            $usersDao = new UsersDao;
            $userExtraInfoDao = new UsersExtraInfoDao;
            $followingsDao = new FollowingsDao;
            $dateUtils = new DateUtils;

            //Validations to be done
            //User Id validation
            if (!isset($inputJson['user_id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter user id",
                    'data' => null
                ];
                return $response;
            }

            if ($dataValidation->isEmpty($inputJson['user_id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter user id",
                    'data' => null
                ];
                return $response;
            }
            //User Id validation
            if (!isset($inputJson['otp'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter otp",
                    'data' => null
                ];
                return $response;
            }

            if ($dataValidation->isEmpty($inputJson['otp'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter otp",
                    'data' => null
                ];
                return $response;
            }

            //Validation ends here.. Business Logic starts here
            $userExtraInfo = $userExtraInfoDao->getUsersExtraInfoByActivationKeyAndUserId($inputJson['otp'],$inputJson['user_id']);

            //Send response for incorrect otp
            if ($userExtraInfo == null) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Wrong OTP or user id",
                    'data' => null
                ];
                return $response;
            }
            
            
            //Check if the account has already been activated
            if ($userExtraInfo['status'] == 1) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Account already activated",
                    'data' => null
                ];
                return $response;
            }


            $dateAddedOn = $userExtraInfo['added_on'];
            $currentDate = $dateUtils->getCurrentDate();

            //Check if the otp has not exceeded the valid time limit
            if (strtotime($currentDate) - strtotime($dateAddedOn) < $codes->OTP_VALID_TIME) {
                //Update the status and user activated flag in user extra info and users table
                $userExtraInfoDao->updateStatusByUserId($inputJson['user_id']);
                $usersDao->updateStatusByUserId($inputJson['user_id']);

                //Follow the default users
                foreach ($codes->TO_BE_FOLLOWED as $flIds) {
                    $followingsDao->saveFollowings($inputJson['user_id'], $flIds);
                }
            } else {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "OTP expired",
                    'data' => null
                ];
                return $response;
            }

            //Return the success response
            $response = [
                'status' => $codes->RC_SUCCESS,
                'message' => "Account activated successfully",
                'data' => null
            ];
            return $response;

            Log::debug("Ended ... signup_verify Service : ");
        } catch (\Exception $e) {
            Log::debug($e);
            throw new Exception($e);
        }
    }

    public function sign_up($inputJson)
    {
        Log::debug("Started ... sign_up Service : " . json_encode($inputJson));
        try {
            $codes = new Codes;
            $dataValidation = new DataValidation;
            $userSessionDao = new UserSessionDao;
            $usersDao = new UsersDao;
            $userExtraInfoDao = new UsersExtraInfoDao;
            $userDevicesDao = new UserDevicesDao;
            $phoneOtpLog = new PhoneOtpLogDao;
            $notificationUtils = new NotificationUtils;
            $imageUtils = new ImageUtils;
            $dateUtils = new DateUtils;
            $authService = new AuthService;

            //Validations to be done
            //First name validation
            if (!isset($inputJson['first_name'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your first name",
                    'data' => null
                ];
                return $response;
            }

            if ($dataValidation->isEmpty($inputJson['first_name'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your first name",
                    'data' => null
                ];
                return $response;
            }

            //Validate both the names according to Luneblaze standard
            $validationMessage = $dataValidation->validateName($inputJson['first_name'], 1);

            if ($validationMessage != 'Success') {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => $validationMessage,
                    'data' => null
                ];
                return $response;
            }

            $validationMessage = $dataValidation->validateName($inputJson['last_name'], 0);

            if ($validationMessage != 'Success') {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => $validationMessage,
                    'data' => null
                ];
                return $response;
            }


            //Password Validation
            if (!isset($inputJson['password'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your password",
                    'data' => null
                ];
                return $response;
            }

            if ($dataValidation->isEmpty($inputJson['password'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your password",
                    'data' => null
                ];
                return $response;
            }

            //Email Id / Phone Number Validation
            if (!isset($inputJson['email']) && !isset($inputJson['phone'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Email id / Phone Number is required",
                    'data' => null
                ];
                return $response;
            }

            if ($dataValidation->isEmpty($inputJson['email']) && $dataValidation->isEmpty($inputJson['phone'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Email id / Phone Number is required",
                    'data' => null
                ];
                return $response;
            }
            //Email Validation
            if (isset($inputJson['email']) && !$dataValidation->isEmpty($inputJson['email'])) {
                if (!$dataValidation->isValidEmail($inputJson['email'])) {
                    $response = [
                        'status' => $codes->RC_ERROR,
                        'message' => "Email id is invalid",
                        'data' => null
                    ];
                    return $response;
                }

                $user = $usersDao->getUsersByUserEmail($inputJson['email']);

                $userExtraInfo = $userExtraInfoDao->getUsersExtraInfoByDataAndTypeAndStatus($inputJson['email'], 'user_email', '1');

                Log::debug("Email Check : User  " . ($user != null) . " " . " User Extra Info " . ($userExtraInfo != null));

                if ($user != null || $userExtraInfo != null) {
                    $response = [
                        'status' => $codes->RC_ERROR,
                        'message' => "Sorry, it looks like " . $inputJson['email'] .  " belongs to an existing account",
                        'data' => null
                    ];
                    return $response;
                }
            }
            //Phone Validation
            if (isset($inputJson['phone']) && !$dataValidation->isEmpty($inputJson['phone'])) {
                $user = $usersDao->getUsersByUserPhone($inputJson['phone']);
                $userExtraInfo = $userExtraInfoDao->getUsersExtraInfoByDataAndTypeAndStatus($inputJson['phone'], 'user_mobile', '1');

                Log::debug("Email Check : User  " . ($user != null) . " " . " User Extra Info " . ($userExtraInfo != null));

                if ($user != null || $userExtraInfo != null) {
                    $response = [
                        'status' => $codes->RC_ERROR,
                        'message' => "Sorry, it looks like " . $inputJson['phone'] .  " belongs to an existing account",
                        'data' => null
                    ];
                    return $response;
                }
            }

            // //Gender Validation
            // if(!isset($inputJson['gender']))
            // {
            //     $response = [
            //         'status'=>$codes->RC_ERROR,
            //         'message'=>"Please enter your gender",
            //         'data'=>null
            //     ];
            //     return $response;
            // }

            // if($dataValidation->isEmpty($inputJson['gender']))
            // {
            //     $response = [
            //         'status'=>$codes->RC_ERROR,
            //         'message'=>"Please enter your gender",
            //         'data'=>null
            //     ];
            //     return $response;
            // }

            //Gender Validation according to Luneblaze Standards
            // $validationMessage = $dataValidation->validateGender($inputJson['gender']);

            // if($validationMessage != 'Success')
            // {
            //     $response = [
            //         'status'=>$codes->RC_ERROR,
            //         'message'=>$validationMessage,
            //         'data'=>null
            //     ];
            //     return $response;
            // }

            //Password validation according to Luneblaze standards
            $validationMessage = $dataValidation->validatePassword($inputJson['password']);

            if ($validationMessage != 'Success') {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => $validationMessage,
                    'data' => null
                ];
                return $response;
            }

            //Birthdate validation according to Luneblaze standards
            //Assign the birthdate string to null
            // $birthdate = '';
            // $validationMessage = $dataValidation->validateBirthDate($inputJson['birth_day'],$inputJson['birth_month'],$inputJson['birth_year']);

            // if($validationMessage != 'Success' && $validationMessage != 'null')
            // {
            //     $response = [
            //         'status'=>$codes->RC_ERROR,
            //         'message'=>$validationMessage,
            //         'data'=>null
            //     ];
            //     return $response;
            // }

            // if($validationMessage == 'null')
            //     $birthdate = 'null';


            //Check if the user had entered the system before hand but did not complete the registration
            if (isset($inputJson['email']))
                $userExtraInfo = $userExtraInfoDao->getUsersExtraInfoByDataAndTypeAndStatus($inputJson['email'], 'user_email', '0');

            Log::debug("User Extra Info : " . json_encode($userExtraInfo) . ' ' . ($userExtraInfo == null));

            if (isset($userExtraInfo) && $userExtraInfo != null) {

                //Generate the OTP
                $otp = rand(100000, 999999);
                //Store the OTP
                //Update the user extra info with the activation otp
                $userExtraInfoDao->updateActivationKeyAddedOnByUserId($otp, $dateUtils->getCurrentDate(), $userExtraInfo['user_id'], 'user_email');

                //Send the otp through email
                $res = $notificationUtils->sendOTPUsingEmail($otp, $inputJson['email'], $inputJson['first_name'], $inputJson['last_name']);

                if ($res == false) {
                    $response = [
                        'status' => $codes->RC_ERROR,
                        'message' => "Activation email could not be sent. But you can login now",
                        'data' => null
                    ];
                    return $response;
                }

                //Send the success response now
                $response = [
                    'status' => $codes->RC_SUCCESS,
                    'message' => $codes->RM_SUCCESS,
                    'data' => [
                        'user_id' => $userExtraInfo['user_id'],
                        'user_firstname' => $inputJson['first_name'],
                        'user_lastname' => $inputJson['last_name']
                    ]
                ];

                return $response;
            }

            //Check if the user had entered the system before hand but did not complete the registration
            if (isset($inputJson['phone']))
                $userExtraInfo = $userExtraInfoDao->getUsersExtraInfoByDataAndTypeAndStatus($inputJson['phone'], 'user_mobile', '0');

            Log::debug("User Extra Info : " . json_encode($userExtraInfo) . ' ' . ($userExtraInfo == null));

            if (isset($userExtraInfo)   && $userExtraInfo != null) {
                //Check the number of otps sent in the past hour
                $numberOfOtpsSent = $phoneOtpLog->getNumberOfOtpsSentPast1Hour($inputJson['phone']);

                if ($numberOfOtpsSent >= $codes->MAX_NUMBER_OF_OTPS_SENT_IN_1_HOUR) {
                    $response = [
                        'status' => $codes->RC_ERROR,
                        'message' => "OTP Limit exceeded. Please contact support or try after some time.",
                        'data' => null
                    ];
                    return $response;
                }

                //Generate the OTP
                $otp = rand(100000, 999999);
                //Store the OTP
                //Update the user extra info with the activation otp
                $userExtraInfoDao->updateActivationKeyAddedOnByUserId($otp, $dateUtils->getCurrentDate(), $userExtraInfo['user_id'], 'user_mobile');

                //Send the otp through email
                $notificationUtils->sendOTPUsingSMS($otp, $inputJson['phone']);

                //Send the success response now
                $response = [
                    'status' => $codes->RC_SUCCESS,
                    'message' => $codes->RM_SUCCESS,
                    'data' => [
                        'user_id' => $userExtraInfo['user_id'],
                        'user_firstname' => $inputJson['first_name'],
                        'user_lastname' => $inputJson['last_name']
                    ]
                ];

                return $response;
            }


            //Create the user name
            $emailArray = explode('@', $inputJson['email']);
            $user_name = str_replace('.', '', $emailArray[0]);

            //Generate activation_key 
            // ($codes->ACTIVATION_TYPE == "email") ? $authService->getHashToken() : $authService->getHashKey();
            $activation_key = rand(100000, 999999);

            //Get the user full name
            $user_full_name = ucwords($inputJson['first_name']) . ' ' . ucwords($inputJson['last_name']);

            //Check if birthdate is null
            // if($birthdate == 'null')
            //     $inputJson['birth_date'] = 'NULL';
            // else 
            //     $inputJson['birth_date'] = $inputJson['birth_year'] . '-' . $inputJson['birth_month'] . '-' . $inputJson['birth_day'];

            if (!isset($inputJson['current_FOI']))
                $inputJson['current_FOI'] = 'NULL';

            //Save the user

            //Update the birthdate and gender, email/phone here
            $inputJson['birth_date'] = '';
            $inputJson['gender'] = '';
            if (!isset($inputJson['phone']))
                $inputJson['phone'] = '';
            if (!isset($inputJson['email']))
                $inputJson['phone'] = '';

            Log::debug("Saving the user : ");
            $userId = $usersDao->saveUser($user_name, $inputJson, $user_full_name, $dateUtils->getCurrentDate(), $activation_key);
            Log::debug("Saved the user ");

            //Save the Activation Key in User Extra Dao
            Log::debug("Saving the user extra info : ");
            $userExtraInfoDao->saveUsersExtraInfo($inputJson, $userId, $dateUtils->getCurrentDate(), $activation_key);
            Log::debug("Saved the user extra info : ");

            if (!$dataValidation->isEmpty($inputJson['email'])) {
                $notificationUtils->sendOTPUsingEmail($activation_key, $inputJson['email'], $inputJson['first_name'], $inputJson['last_name']);
            }
            if (!$dataValidation->isEmpty($inputJson['phone'])) {
                //Check the number of otps sent in the past hour
                $numberOfOtpsSent = $phoneOtpLog->getNumberOfOtpsSentPast1Hour($inputJson['phone']);

                if ($numberOfOtpsSent >= $codes->MAX_NUMBER_OF_OTPS_SENT_IN_1_HOUR) {
                    $response = [
                        'status' => $codes->RC_ERROR,
                        'message' => "OTP Limit exceeded. Please contact support or try after some time.",
                        'data' => null
                    ];
                    return $response;
                }

                $notificationUtils->sendOTPUsingSMS($activation_key, $inputJson['phone']);

                //Update the sms sending log table
                $phoneOtpLog->savePhoneOtpSentLog($inputJson['phone']);
            }

            //Finally fetch the users data to return to the front end
            $user = $usersDao->getUserByUserId($userId);

            Log::debug('User : ' . json_encode($user));

            //Send the success response now
            $response = [
                'status' => $codes->RC_SUCCESS,
                'message' => $codes->RM_SUCCESS,
                'data' => [
                    'user_id' => $user['user_id'],
                    'user_firstname' => $inputJson['first_name'],
                    'user_lastname' => $inputJson['last_name']
                ]
            ];
            return $response;

            Log::debug("Ended ... sign_up Service : ");
        } catch (\Exception $e) {
            Log::debug($e);
            throw new Exception($e);
        }
    }

    /**
     * 
     */
    public function sign_in($inputJson)
    {
        Log::debug("Started ... sign_in Service : " . json_encode($inputJson));

        try {
            $codes = new Codes;
            $dataValidation = new DataValidation;
            $userSessionDao = new UserSessionDao;
            $usersDao = new UsersDao;
            $userDevicesDao = new UserDevicesDao;
            $phoneOtpLog = new PhoneOtpLogDao;
            $notificationUtils = new NotificationUtils;
            $imageUtils = new ImageUtils;

            //Validations
            //Email Validation
            if (!isset($inputJson['email'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your email id",
                    'data' => null
                ];
                return $response;
            }

            if ($dataValidation->isEmpty($inputJson['email'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your email id",
                    'data' => null
                ];
                return $response;
            }

            //Password Validation
            if (!isset($inputJson['password'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your password",
                    'data' => null
                ];
                return $response;
            }

            if ($dataValidation->isEmpty($inputJson['password'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your password",
                    'data' => null
                ];
                return $response;
            }

            //User OS Validation
            if (!isset($inputJson['user_os'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "User OS is required",
                    'data' => null
                ];
                return $response;
            }

            if ($dataValidation->isEmpty($inputJson['user_os'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "User OS is required",
                    'data' => null
                ];
                return $response;
            }

            //Device Id Validation
            if (!isset($inputJson['deviceid'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Device Id is required",
                    'data' => null
                ];
                return $response;
            }

            if ($dataValidation->isEmpty($inputJson['deviceid'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Device Id is required",
                    'data' => null
                ];
                return $response;
            }

            //User Ip Validation
            if (!isset($inputJson['user_ip'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "User Ip is required",
                    'data' => null
                ];
                return $response;
            }

            if ($dataValidation->isEmpty($inputJson['user_ip'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "User Ip is required",
                    'data' => null
                ];
                return $response;
            }

            //Session Token Validation
            if (!isset($inputJson['session_token'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Session Token is required",
                    'data' => null
                ];
                return $response;
            }

            if ($dataValidation->isEmpty($inputJson['session_token'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Session Token is required",
                    'data' => null
                ];
                return $response;
            }


            //End of validation -- Starting business logic
            $user_browser = isset($inputJson['user_browser']) && !empty($inputJson['user_browser']) ? $inputJson['user_browser'] : 'APP';

            Log::debug("User Browser : " . $user_browser);

            //Fetch the user details using email id and password
            $user = $usersDao->getUsersByUserEmailOrUserPhoneAndPassword($inputJson['email'], $inputJson['password']);

            if ($user == null) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Invalid Email/Phone or password.",
                    'data' => null
                ];
                return $response;
            }

            //Check if the user account is yet activated
            if ($user['user_activated'] == 0) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please activate your account.",
                    'data' => null
                ];
                return $response;
            }

            //Assign the user to the response object
            $response['data'] = $user;

            //Check for the two step verifications
            if ($user['two_step_verifications'] == 'no') {
                $response['data']['two_step'] = false;
                $response['message'] = "You have successfully logged in.";

                //Update the last login timestamp
                $usersDao->updateLastLoginByUserId($user['user_id']);
            } else {
                //Go for the two step verification
                //Check the number of otps sent in the past hour
                $numberOfOtpsSent = $phoneOtpLog->getNumberOfOtpsSentPast1Hour($user['user_phone']);

                if ($numberOfOtpsSent >= $codes->MAX_NUMBER_OF_OTPS_SENT_IN_1_HOUR) {
                    $response = [
                        'status' => $codes->RC_ERROR,
                        'message' => "OTP Limit exceeded. Please contact support or try after some time.",
                        'data' => null
                    ];
                    return $response;
                }
                //Generate the OTP
                $otp = rand(100000, 999999);
                //Store the OTP
                $usersDao->updateLoginOtp($user['user_id'], $otp);
                //Send the otp
                $notificationUtils->sendOTPUsingSMS($otp, $user['user_phone']);

                //Update the sms sending log table
                $phoneOtpLog->savePhoneOtpSentLog($user['user_phone']);

                //Update the response 
                $response['data']['two_step'] = true;
                $response['message'] = "Please enter the OTP sent to your mobile number.";
            }

            //Delete any previous user sessions if exists
            $userSessionDao->deleteUserSessionsIfExists($user['user_id'], $inputJson['deviceid']);

            //Save the present User session
            $sessionId = $userSessionDao->saveUserSessions(
                $inputJson['session_token'],
                $user_browser,
                $inputJson['user_os'],
                $inputJson['user_ip'],
                $user['user_id'],
                $inputJson['deviceid']
            );

            //Update the response with the session id
            $response['data']['session_id'] = $sessionId;

            //Update the response user picture
            $response['data']['user_picture'] = $imageUtils->getPicture($user['user_picture'], $user['user_gender']);

            //Check for logged in condition in other devices
            $userDevices = $userDevicesDao->getUserDevicesByUserId($user['user_id']);

            if (count($userDevices) > 0)
                $response['data']['logged_in'] = true;
            else
                $response['data']['logged_in'] = false;

            $response['status'] = $codes->RC_SUCCESS;

            return $response;

            Log::debug("Ended ... sign_in Service : ");
        } catch (\Exception $e) {
            Log::debug($e);
            throw new Exception($e);
        }
    }

    /**
     * 
     */
    public function signup_current_status($inputJson)
    {
        Log::debug("Started ... signup_current_status Service : " . json_encode($inputJson));
        try {
            $usersDao = new UsersDao;
            $codes = new Codes;
            $dataValidation = new DataValidation;

            //Validation to be done here

            if (!isset($inputJson['user_id']) || !isset($inputJson['user_work']) || !isset($inputJson['work_title']) || !isset($inputJson['work_place'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => $codes->RM_ALL_FIELDS_TO_BE_PRESENT,
                    'data' => null
                ];

                return $response;
            }

            if ($dataValidation->isEmpty($inputJson['user_id'])) {

                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your user id",
                    'data' => null
                ];

                return $response;
            }
            if ($dataValidation->isEmpty($inputJson['user_work'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your profession",
                    'data' => null
                ];

                return $response;
            }
            if ($dataValidation->isEmpty($inputJson['work_title'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter place of work",
                    'data' => null
                ];

                return $response;
            }
            if ($dataValidation->isEmpty($inputJson['work_place'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter designation",
                    'data' => null
                ];

                return $response;
            }

            //Validation done -- 

            //Step 1 : Check if the user id is present
            $user = $usersDao->getUserByUserId($inputJson['user_id']);

            if ($user == null) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Invalid User",
                    'data' => null
                ];

                return $response;
            }

            Log::debug("User Fetched : " . json_encode($user));

            //Step 2 : Venue Id 
            $venue_id = (isset($inputJson['venue_id'])) ? $inputJson['venue_id'] : 0;

            //Step 3 : Update users by user Id
            $usersDao->updateUserByUserId($inputJson, $venue_id, $inputJson['user_id']);


            //Step 4 : Return the response
            $data = array(
                'user_id' => $inputJson['user_id']
            );

            $response = [
                'status' => $codes->RC_SUCCESS,
                'message' => "",
                'data' => $data
            ];

            Log::debug("Ended ... signup_current_status Service : ");

            return $response;
        } catch (\Exception $e) {
            Log::debug($e);
            throw new Exception($e);
        }
    }


    public function getcitiesbycityid($inputJson)
    {

        Log::debug("Started ... getcitiesbycityid Service : " . json_encode($inputJson));
        try {
            $citiesDao = new CitiesDao;
            $codes = new Codes;
            $dataValidation = new DataValidation;

            //Validation to be done here
            if (!isset($inputJson['_CityId'])) {
                $response = [
                    '_ReturnCode' => $codes->RC_DATA_VALIDATION_ERROR,
                    '_ReturnMessage' => $codes->RM_DATA_VALIDATION_ERROR,
                    '_Field' => "_CityId"
                ];

                return $response;
            }
            if (!$dataValidation->isValidInteger($inputJson['_CityId'])) {
                $response = [
                    '_ReturnCode' => $codes->RC_DATA_VALIDATION_ERROR,
                    '_ReturnMessage' => $codes->RM_DATA_VALIDATION_ERROR,
                    '_Field' => "_CityId"
                ];

                return $response;
            }

            $response = [
                '_ReturnCode' => $codes->RC_SUCCESS,
                '_ReturnMessage' => $codes->RM_SUCCESS,
                'cities' => array()
            ];

            $cities = $citiesDao->getCitiesByCityId($inputJson['_CityId']);

            $response['cities'] = $cities;


            Log::debug("Ended ... getcitiesbycityid Service : ");
            return $response;
        } catch (\Exception $e) {
            Log::debug($e);
            throw new Exception($e);
        }
    }
}
