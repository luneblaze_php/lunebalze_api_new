<?php
namespace App\Controller;

use App\Authentication\AuthService;
use App\ServiceLayer\ConstantService;
use App\ServiceLayer\FriendService;
use App\ServiceLayer\ProductService;
use Cake\Controller\Controller;
use Cake\Log\Log; 
use Cake\Event\Event;

use App\ServiceLayer\RegistrationService;

   
    class AppController extends Controller
    {
        public function initialize()
        {
            parent::initialize();

            $this->loadComponent('RequestHandler', [
                'enableBeforeRedirect' => false,
            ]);
            $this->loadComponent('Flash');
        }

        public function friendRequestsSent()
        {

            Log::debug("Started ... friend_requests_sent : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $frndService = new FriendService;

                $response = $frndService->friend_requests_sent($jsonInput);

            }catch(\Exception $e){
                $response = [
                    'status'=>'999',
                    'message'=>'Fatal Error'
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }   

        public function getInterestList()
        {

            Log::debug("Started ... get_interest_list : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $constService = new ConstantService;

                $response = $constService->get_interest($jsonInput);

            }catch(\Exception $e){
                $response = [
                    'status'=>'999',
                    'message'=>'Fatal Error'
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function getUserInterest()
        {

            Log::debug("Started ... get_user_interest : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $constService = new ConstantService;

                $response = $constService->get_user_interest($jsonInput);

            }catch(\Exception $e){
                $response = [
                    'status'=>'999',
                    'message'=>'Fatal Error'
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }


        public function searchInterest()
        {

            Log::debug("Started ... search_interest : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $constService = new ConstantService;

                $response = $constService->search_interest($jsonInput);

            }catch(\Exception $e){
                $response = [
                    'status'=>'999',
                    'message'=>'Fatal Error'
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }


        public function signupCurrentStatus()
        {

            Log::debug("Started ... signup_current_status : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $regService = new RegistrationService;

                $response = $regService->signup_current_status($jsonInput);

            }catch(\Exception $e){
                $response = [
                    'status'=>'999',
                    'message'=>'Fatal Error'
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function login()
        {
            Log::debug("Started ... login : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $regService = new RegistrationService;

                $response = $regService->sign_in($jsonInput);

            }catch(\Exception $e){
                $response = [
                    'status'=>'999',
                    'message'=>'Fatal Error'
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function signup()
        {
            Log::debug("Started ... signup : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $regService = new RegistrationService;

                $response = $regService->sign_up($jsonInput);

            }catch(\Exception $e){
                $response = [
                    'status'=>'999',
                    'message'=>'Fatal Error'
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function signupverify()
        {
            Log::debug("Started ... signupverify : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $regService = new RegistrationService;

                $response = $regService->signup_verify($jsonInput);

            }catch(\Exception $e){
                $response = [
                    'status'=>'999',
                    'message'=>'Fatal Error'
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function getCities()
        {

            Log::debug("Started ... getCities : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $regService = new RegistrationService;

                $response = $regService->getcitiesbycityid($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }


        

    }
