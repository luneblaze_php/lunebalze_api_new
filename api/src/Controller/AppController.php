<?php
namespace App\Controller;

use App\Authentication\AuthService;
use App\ServiceLayer\ConstantService;
use App\ServiceLayer\SessionService;
use App\ServiceLayer\OrganizationService;
use App\ServiceLayer\ChatService;
use App\ServiceLayer\FriendService;
use App\ServiceLayer\ProductService;
// vivek start
use App\ServiceLayer\QuizService;
use App\ServiceLayer\DebateService;
use App\ServiceLayer\CommunityService;
use App\ServiceLayer\QuickBitsService;
use App\ServiceLayer\VenueService;
// vivek end
use Cake\Controller\Controller;
use App\ServiceLayer\CommentService;
use Cake\Log\Log; 
use Cake\Event\Event;


use App\ServiceLayer\RegistrationService;

   
    class AppController extends Controller
    {
        public function initialize()
        {
            parent::initialize();

            $this->loadComponent('RequestHandler', [
                'enableBeforeRedirect' => false,
            ]);
            $this->loadComponent('Flash');
        }

        public function friendRequestsSent()
        {

            Log::debug("Started ... friend_requests_sent : ");
            $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $frndService = new FriendService;

                $response = $frndService->friend_requests_sent($jsonInput);

            }catch(\Exception $e){
                $response = [
                    'status'=>'999',
                    'message'=>'Fatal Error'
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }   


        public function getInterestList()
        {

            Log::debug("Started ... get_interest_list : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $constService = new ConstantService;

                $response = $constService->get_interest($jsonInput);

            }catch(\Exception $e){
                $response = [
                    'status'=>'999',
                    'message'=>'Fatal Error'
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function getUserInterest()
        {

            Log::debug("Started ... get_user_interest : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $constService = new ConstantService;

                $response = $constService->get_user_interest($jsonInput);

            }catch(\Exception $e){
                $response = [
                    'status'=>'999',
                    'message'=>'Fatal Error'
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }


        public function searchInterest()
        {

            Log::debug("Started ... search_interest : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $constService = new ConstantService;

                $response = $constService->search_interest($jsonInput);

            }catch(\Exception $e){
                $response = [
                    'status'=>'999',
                    'message'=>'Fatal Error'
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }


        public function signupCurrentStatus()
        {

            Log::debug("Started ... signup_current_status : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $regService = new RegistrationService;

                $response = $regService->signup_current_status($jsonInput);

            }catch(\Exception $e){
                $response = [
                    'status'=>'999',
                    'message'=>'Fatal Error'
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function login()
        {
            Log::debug("Started ... login : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $regService = new RegistrationService;

                $response = $regService->sign_in($jsonInput);

            }catch(\Exception $e){
                $response = [
                    'status'=>'999',
                    'message'=>'Fatal Error'
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function signup()
        {
            Log::debug("Started ... signup : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $regService = new RegistrationService;

                $response = $regService->sign_up($jsonInput);

            }catch(\Exception $e){
                $response = [
                    'status'=>'999',
                    'message'=>'Fatal Error'.$e->getMessage()
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function signupverify()
        {
            Log::debug("Started ... signupverify : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $regService = new RegistrationService;

                $response = $regService->signup_verify($jsonInput);

            }catch(\Exception $e){
                $response = [
                    'status'=>'999',
                    'message'=>'Fatal Error',
                    'exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function loginTwoStep()
        {
            Log::debug("Started ... login_two_step : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $regService = new RegistrationService;

                $response = $regService->login_two_step($jsonInput);

            }catch(\Exception $e){
                $response = [
                    'status'=>'999',
                    'message'=>'Fatal Error'
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function forgetPassword()
        {
            Log::debug("Started ... forget_password : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $regService = new RegistrationService;

                $response = $regService->forgot_password($jsonInput);

            }catch(\Exception $e){
                $response = [
                    'status'=>'999',
                    'message'=>'Fatal Error'
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }


        public function forgetPasswordReset()
        {
            Log::debug("Started ... forget_password_reset : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $regService = new RegistrationService;

                $response = $regService->forgot_password_reset($jsonInput);

            }catch(\Exception $e){
                $response = [
                    'status'=>'999',
                    'message'=>'Fatal Error'
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function getFriendRequest()
        {
            Log::debug("Started ... get_friend_request : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $frndService = new FriendService;

                $response = $frndService->get_friend_request($jsonInput);

            }catch(\Exception $e){
                $response = [
                    'status'=>'999',
                    'message'=>'Fatal Error'
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function getFriendRequestSent()
        {
            Log::debug("Started ... get_friend_request_sent : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $frndService = new FriendService;

                $response = $frndService->get_friend_request_sent($jsonInput);

            }catch(\Exception $e){
                $response = [
                    'status'=>'999',
                    'message'=>'Fatal Error'
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function getOrganization()
        {
            Log::debug("Started ... get_organization : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $constantService = new ConstantService;

                $response = $constantService->get_organization($jsonInput);

            }catch(\Exception $e){
                $response = [
                    'status'=>'999',
                    'message'=>'Fatal Error'
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function getCities()
        {

            Log::debug("Started ... getCities : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $regService = new RegistrationService;

                $response = $regService->getcitiesbycityid($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }
        // vivek start
        public function createQuiz()
        {

            Log::debug("Started ... newQuiz : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $regService = new QuizService;

                $response = $regService->createQuiz($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function updateQuiz()
        {

            Log::debug("Started ... updateQuiz : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $regService = new QuizService;

                $response = $regService->updateQuiz($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }
        public function deleteQuiz()
        {

            Log::debug("Started ... deleteQuiz : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $regService = new QuizService;

                $response = $regService->deleteQuiz($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function getMyQuiz()
        {

            Log::debug("Started ... getMyQuiz : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $regService = new QuizService;

                $response = $regService->getMyQuiz($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }
        public function getQuizzesList()
        {

            Log::debug("Started ... getQuizzesList : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $regService = new QuizService;

                $response = $regService->getQuizzesList($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }
        public function updateQuizParticipation()
        {

            Log::debug("Started ... updateQuizParticipation : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $regService = new QuizService;

                $response = $regService->updateQuizParticipation($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function updateQuizLastSeenQuestion()
        {

            Log::debug("Started ... updateQuizLastSeenQuestion : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $regService = new QuizService;

                $response = $regService->updateQuizLastSeenQuestion($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }


        public function addQuizParticipantsAnswer()
        {

            Log::debug("Started ... addQuizParticipantsAnswer : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $regService = new QuizService;

                $response = $regService->addQuizParticipantsAnswer($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function submitQuizParticipation()
        {

            Log::debug("Started ... submitQuizParticipation : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $regService = new QuizService;

                $response = $regService->calculateQuizScore($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function inviteUserForQuiz()
        {

            Log::debug("Started ... inviteUserForQuiz : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $regService = new QuizService;

                $response = $regService->inviteUserForQuiz($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function getInvitedUserForQuiz()
        {

            Log::debug("Started ... getInvitedUserForQuiz : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $regService = new QuizService;

                $response = $regService->getInvitedUserListForQuiz($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }



        public function createDebate()
        {

            Log::debug("Started ... newDebate : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $regService = new DebateService;

                $response = $regService->createDebate($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function updateDebate()
        {

            Log::debug("Started ... updateDebate : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $regService = new DebateService;

                $response = $regService->updateDebate($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }
        public function deleteDebate()
        {

            Log::debug("Started ... deleteDebate : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $regService = new DebateService;

                $response = $regService->deleteDebate($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function getMyDebate()
        {

            Log::debug("Started ... getMyDebate : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $regService = new DebateService;

                $response = $regService->getMyDebate($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }
        public function getDebatesList()
        {

            Log::debug("Started ... getDebatesList : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $regService = new DebateService;

                $response = $regService->getDebatesList($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }
        public function updateDebateParticipation()
        {

            Log::debug("Started ... updateDebateParticipation : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $regService = new DebateService;

                $response = $regService->updateDebateParticipation($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function addDebateParticipantsProsAndCons()
        {

            Log::debug("Started ... addDebateParticipantsProsAndCons : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $regService = new DebateService;

                $response = $regService->addDebateParticipantsProsAndCons($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }
        public function updateDebateParticipantsProsAndCons()
        {

            Log::debug("Started ... updateDebateParticipantsProsAndCons : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $regService = new DebateService;

                $response = $regService->updateDebateParticipantsProsAndCons($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }
        public function deleteDebateParticipantsProsAndCons()
        {

            Log::debug("Started ... deleteDebateParticipantsProsAndCons : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $regService = new DebateService;

                $response = $regService->deleteDebateParticipantsProsAndCons($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }
        public function addDebateProsAndConsRating()
        {

            Log::debug("Started ... addDebateProsAndConsRating : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $regService = new DebateService;

                $response = $regService->addDebateProsAndConsRating($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function submitDebateParticipation()
        {

            Log::debug("Started ... submitDebateParticipation : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $regService = new DebateService;

                $response = $regService->calculateDebateScore($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function inviteUserForDebate()
        {

            Log::debug("Started ... inviteUserForDebate : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $regService = new DebateService;

                $response = $regService->inviteUserForDebate($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function getInvitedUserForDebate()
        {

            Log::debug("Started ... getInvitedUserForDebate : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $regService = new DebateService;

                $response = $regService->getInvitedUserListForDebate($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function updateDebateProsAndConsFollowers()
        {

            Log::debug("Started ... updateDebateProsAndConsFollowers : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $regService = new DebateService;

                $response = $regService->updateDebateProsAndConsFollowers($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function getDebateProsAndConsList()
        {

            Log::debug("Started ... getDebateProsAndConsList : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $regService = new DebateService;

                $response = $regService->getDebateProsAndConsList($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function createCommunity()
        {

            Log::debug("Started ... newCommunity : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $regService = new CommunityService;

                $response = $regService->createCommunity($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function updateCommunity()
        {

            Log::debug("Started ... updateCommunity : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $regService = new CommunityService;

                $response = $regService->updateCommunity($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }


        //adding api 1
        public function getCommunityAdminModList()
        {
            Log::debug("Started ... getCommunityAdminModList : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $regService = new CommunityService;

                $response = $regService->getCommunityAdminModList($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }
        }

        //adding api 2
        public function getBlockedCommunityUsers()
        {
            Log::debug("Started ... getCommunityAdminModList : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $regService = new CommunityService;

                $response = $regService->getBlockedCommunityUsers($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }
        }


        public function deleteCommunity()
        {

            Log::debug("Started ... deleteCommunity : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $regService = new CommunityService;

                $response = $regService->deleteCommunity($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function getMyCommunity()
        {

            Log::debug("Started ... getMyCommunity : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $regService = new CommunityService;

                $response = $regService->getMyCommunity($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }
        public function getCommunityList()
        {

            Log::debug("Started ... getCommunityList : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $regService = new CommunityService;

                $response = $regService->getCommunityList($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function addRemoveCommunityParticipation()
        {

            Log::debug("Started ... addRemoveCommunityParticipation : ");
            $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $regService = new CommunityService;

                $response = $regService->addRemoveCommunityParticipation($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }
        public function promoteDemoteCommunityUser()
        {

            Log::debug("Started ... submitCommunityParticipation : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $regService = new CommunityService;

                $response = $regService->promoteDemoteCommunityUser($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function inviteUserForCommunity()
        {

            Log::debug("Started ... inviteUserForCommunity : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $regService = new CommunityService;

                $response = $regService->inviteUserForCommunity($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function getInvitedUserForCommunity()
        {

            Log::debug("Started ... getInvitedUserForCommunity : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $regService = new CommunityService;

                $response = $regService->getInvitedUserListForCommunity($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function getPostsRequestsForCommunity()
        {

            Log::debug("Started ... getPostsRequestsForCommunity : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $regService = new CommunityService;

                $response = $regService->getPostsRequestsForCommunity($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function approveOrDeclinePostsRequestsForCommunity()
        {

            Log::debug("Started ... approveOrDeclinePostsRequestsForCommunity : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $regService = new CommunityService;

                $response = $regService->approveOrDeclinePostsRequestsForCommunity($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }



        // quick bit start

        public function createQuickBit()
        {

            Log::debug("Started ... newQuickBit : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $regService = new QuickBitsService;

                $response = $regService->createQuickBit($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function updateQuickBit()
        {

            Log::debug("Started ... updateQuickBit : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $regService = new QuickBitsService;

                $response = $regService->updateQuickBit($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }
        public function deleteQuickBit()
        {

            Log::debug("Started ... deleteQuickBit : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $regService = new QuickBitsService;

                $response = $regService->deleteQuickBit($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function getMyQuickBit()
        {

            Log::debug("Started ... getMyQuickBit : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $regService = new QuickBitsService;

                $response = $regService->getMyQuickBit($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }
        public function getQuickBitsList()
        {

            Log::debug("Started ... getQuickBitsList : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $regService = new QuickBitsService;

                $response = $regService->getQuickBitsList($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        // vivek end


        public function saveComment(){
          
            header('Content-Type: application/json');
            //For proper json response in any condition
            Log::debug("Started ... comment : ");

            $response = null;
            try{
                $jsonInput = $this->request->getData();

                $commentService = new CommentService;

                $response = $commentService->get_comment($jsonInput);

            }catch(\Exception $e){
                /*Log::debug($e);
                throw new \Exception($e);*/
                $response = [
                    'status'=>'999',
                    'message'=>'Fatal Error'
                ];
            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }
        }

        public function getOrganizationType(){

            Log::debug("Started ... get_organization : ");
            $response = null;
           try{
               $jsonInput = $this->request->getData();
               
               $organizationService = new OrganizationService;

               $response = $organizationService->get_organization_type($jsonInput);

           }catch(\Exception $e){
               $response = [
                   'status'=>'999',
                   'message'=>'Fatal Error'
               ];

           }finally{
               $this->setResponse($this->response->withStatus(200));
               $this->set([
                   'response' => $response,
                   '_serialize' => 'response',
               ]);
               $this->RequestHandler->renderAs($this, 'json');
           }  

        }

        public function addOrganization()
        {

            Log::debug("Started ... addOrganization : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $organizationService = new OrganizationService;

                $response = $organizationService->addOrganization($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }    
        
        public function editOrganization()
        {

            Log::debug("Started ... editOrganization : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $organizationService = new OrganizationService;

                $response = $organizationService->editOrganization($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }
        
        public function organizationOtpVerify()
        {

            Log::debug("Started ... organization otp verify : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $organizationService = new OrganizationService;

                $response = $organizationService->organizationOtpVerify($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function campusBooked()
        {

            Log::debug("Started ... getCampusBookedDate : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $organizationService = new OrganizationService;

                $response = $organizationService->getCampusBookedDate($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function getAssessmentType()
        {

            Log::debug("Started ... getAssessmentTypes : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $organizationService = new OrganizationService;

                $response = $organizationService->getAssessmentTypes($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function getAssessmentCategories()
        {

            Log::debug("Started ... getAssessmentTypes : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $organizationService = new OrganizationService;

                $response = $organizationService->getAssessmentCategories($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function getAssessmentList()
        {

            Log::debug("Started ... getAssessmentList : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $organizationService = new OrganizationService;

                $response = $organizationService->getAssessmentList($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function getAssessmentPaymentStatus()
        {

            Log::debug("Started ... getAssessmentPaymentStatus : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $organizationService = new OrganizationService;

                $response = $organizationService->getAssessmentPaymentStatus($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function campusDrivePrice()
        {

            Log::debug("Started ... getCampusDrivePrice : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $organizationService = new OrganizationService;

                $response = $organizationService->getCampusDrivePrice($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function getCollege()
        {

            Log::debug("Started ... getStudentsPerCollege : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $organizationService = new OrganizationService;

                $response = $organizationService->getStudentsPerCollege($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function scheduleCampus()
        {

            Log::debug("Started ... schedule_campus : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $organizationService = new OrganizationService;

                $response = $organizationService->schedule_campus($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function getCampusdrive()
        {

            Log::debug("Started ... schedule_campus : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $organizationService = new OrganizationService;

                $response = $organizationService->getCampusDriveList($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function userCampusInterest()
        {

            Log::debug("Started ... user_campus_interest : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $organizationService = new OrganizationService;

                $response = $organizationService->applyCampusDrive($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function initiateAssessmentPayment()
        {

            Log::debug("Started ... initiateAssessmentPayment : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $organizationService = new OrganizationService;

                $response = $organizationService->initiateAssessmentPayment($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function makeAssessmentPayment()
        {

            Log::debug("Started ... makeAssessmentPayment : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $organizationService = new OrganizationService;

                $response = $organizationService->makeAssessmentPayment($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }
        //chats
        public function usersStatus()
        {

            Log::debug("Started ... getUserOnlineStatus : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                $chatService = new ChatService;

                $response = $chatService->getUsersStatus($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function deleteConversation()
        {

            Log::debug("Started ... deleteConversation : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $chatService = new ChatService;

                $response = $chatService->deleteConversation($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function getUserChatMessages()
        {

            Log::debug("Started ... getUserChatMessages : ");
             $response = null;
            try{
                
                $jsonInput = $this->request->getData();

                $chatService = new ChatService;

                $response = $chatService->getUserChatMessages($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function createChatGroup()
        {

            Log::debug("Started ... createChatGroup : ");
             $response = null;
            try{
                
                $jsonInput = $this->request->getData();

                $chatService = new ChatService;

                $response = $chatService->createChatGroup($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function editChatGroup()
        {

            Log::debug("Started ... createChatGroup : ");
             $response = null;
            try{
                
                $jsonInput = $this->request->getData();

                $chatService = new ChatService;

                $response = $chatService->editChatGroup($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function createConversation()
        {

            Log::debug("Started ... createConversation : ");
             $response = null;
            try{
                
                $jsonInput = $this->request->getData();

                $chatService = new ChatService;

                $response = $chatService->createGetConversation($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function replyToMessage()
        {

            Log::debug("Started ... replyToMessage : ");
             $response = null;
            try{
                
                $jsonInput = $this->request->getData();

                $chatService = new ChatService;

                $response = $chatService->replyToMessage($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function uploadChatImage()
        {

            Log::debug("Started ... uploadChatImage : ");
             $response = null;
            try{
                
                $jsonInput = $this->request->getData();

                $chatService = new ChatService;

                $response = $chatService->uploadChatImage($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function forwardChatMessage()
        {

            Log::debug("Started ... forwardChatMessage : ");
             $response = null;
            try{
                
                $jsonInput = $this->request->getData();

                $chatService = new ChatService;

                $response = $chatService->forwardChatMessage($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function conversationSeen()
        {

            Log::debug("Started ... conversationSeen : ");
             $response = null;
            try{
                
                $jsonInput = $this->request->getData();

                $chatService = new ChatService;

                $response = $chatService->conversationSeen($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function leaveGroup()
        {

            Log::debug("Started ... leaveGroup : ");
             $response = null;
            try{
                
                $jsonInput = $this->request->getData();

                $chatService = new ChatService;

                $response = $chatService->leaveGroup($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function addMemberToGroup()
        {

            Log::debug("Started ... addMemberToGroup : ");
             $response = null;
            try{
                
                $jsonInput = $this->request->getData();

                $chatService = new ChatService;

                $response = $chatService->addMemberToGroup($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function removeGroupMember()
        {

            Log::debug("Started ... removeGroupMember : ");
             $response = null;
            try{
                
                $jsonInput = $this->request->getData();

                $chatService = new ChatService;

                $response = $chatService->removeGroupMember($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function makeUserAdmin()
        {

            Log::debug("Started ... makeUserAdmin : ");
             $response = null;
            try{
                
                $jsonInput = $this->request->getData();

                $chatService = new ChatService;

                $response = $chatService->makeUserAdmin($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function revokeAdminStatus()
        {

            Log::debug("Started ... revokeAdminStatus : ");
             $response = null;
            try{
                
                $jsonInput = $this->request->getData();

                $chatService = new ChatService;

                $response = $chatService->revokeAdminStatus($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function getGroupInfo()
        {

            Log::debug("Started ... getGroupInfo : ");
             $response = null;
            try{
                
                $jsonInput = $this->request->getData();

                $chatService = new ChatService;

                $response = $chatService->getGroupInfo($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }
        //end chats
        public function readNotification()
        {

            Log::debug("Started ... readNotification : ");
             $response = null;
            try{
                
                $jsonInput = $this->request->getData();

                $commentService = new CommentService;

                $response = $commentService->readNotification($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function notificationSeen()
        {

            Log::debug("Started ... notificationSeen : ");
             $response = null;
            try{
                
                $jsonInput = $this->request->getData();

                $commentService = new CommentService;

                $response = $commentService->notificationSeen($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function notifications()
        {

            Log::debug("Started ... notifications : ");
             $response = null;
            try{
                
                $jsonInput = $this->request->getData();

                $commentService = new CommentService;

                $response = $commentService->notifications($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function connect()
        {

            Log::debug("Started ... connect : ");
             $response = null;
            try{
                
                $jsonInput = $this->request->getData();

                $commentService = new CommentService;

                $response = $commentService->connect($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function sendInterestRequest()
        {

            Log::debug("Started ... sendInterestRequest : ");
             $response = null;
            try{
                
                $jsonInput = $this->request->getData();

                $commentService = new CommentService;

                $response = $commentService->sendInterestRequest($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function updateToken()
        {

            Log::debug("Started ... updateToken : ");
             $response = null;
            try{
                
                $jsonInput = $this->request->getData();

                $commentService = new CommentService;

                $response = $commentService->updateToken($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function suggestInterestEdit()
        {

            Log::debug("Started ... suggest_interest_edit : ");
             $response = null;
            try{
                
                $jsonInput = $this->request->getData();

                $commentService = new CommentService;

                $response = $commentService->suggestInterestEdit($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function invite()
        {

            Log::debug("Started ... invite : ");
             $response = null;
            try{
                
                $jsonInput = $this->request->getData();

                $commentService = new CommentService;

                $response = $commentService->invite($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function invitations()
        {

            Log::debug("Started ... invitations : ");
             $response = null;
            try{
                
                $jsonInput = $this->request->getData();

                $commentService = new CommentService;

                $response = $commentService->invitations($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function getSubInterestList()
        {

            Log::debug("Started ... getSubInterestList : ");
             $response = null;
            try{
                
                $jsonInput = $this->request->getData();

                $commentService = new CommentService;

                $response = $commentService->getSubInterestList($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function getAllinterest()
        {

            Log::debug("Started ... getAllinterest : ");
             $response = null;
            try{
                
                $jsonInput = $this->request->getData();

                $commentService = new CommentService;

                $response = $commentService->getAllinterest($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function getInterestSiblings()
        {

            Log::debug("Started ... getAllinterest : ");
             $response = null;
            try{
                
                $jsonInput = $this->request->getData();

                $commentService = new CommentService;

                $response = $commentService->getInterestSiblings($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function loggedInDevices()
        {

            Log::debug("Started ... loggedInDevices : ");
             $response = null;
            try{
                
                $jsonInput = $this->request->getData();

                $commentService = new CommentService;

                $response = $commentService->loggedInDevices($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function getUserByPhone()
        {

            Log::debug("Started ... getUserByPhone : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $commentService = new CommentService;

                $response = $commentService->getUserByPhone($jsonInput);

            }catch(\Exception $e){
                $response = [
                    'status'=>'999',
                    'message'=>'Fatal Error'
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function comments()
        {

            Log::debug("Started ... comments : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $commentService = new CommentService;

                $response = $commentService->comments($jsonInput);

            }catch(\Exception $e){
                $response = [
                    'status'=>'999',
                    'message'=>'Fatal Error'
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function postCommentReply()
        {

            Log::debug("Started ... postCommentReply : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $commentService = new CommentService;

                $response = $commentService->postCommentReply($jsonInput);

            }catch(\Exception $e){
                $response = [
                    'status'=>'999',
                    'message'=>'Fatal Error'
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function question()
        {

            Log::debug("Started ... question : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $commentService = new CommentService;

                $response = $commentService->question($jsonInput);

            }catch(\Exception $e){
                $response = [
                    'status'=>'999',
                    'message'=>'Fatal Error'
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function postCommentDelete()
        {

            Log::debug("Started ... question : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $commentService = new CommentService;

                $response = $commentService->postCommentDelete($jsonInput);

            }catch(\Exception $e){
                $response = [
                    'status'=>'999',
                    'message'=>'Fatal Error'
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        
        public function getPostComments()
        {

            Log::debug("Started ... question : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $commentService = new CommentService;

                $response = $commentService->getPostComments($jsonInput);

            }catch(\Exception $e){
                $response = [
                    'status'=>'999',
                    'message'=>'Fatal Error'
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function editType()
        {

            Log::debug("Started ... editType : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $commentService = new CommentService;

                $response = $commentService->editType($jsonInput);

            }catch(\Exception $e){
                $response = [
                    'status'=>'999',
                    'message'=>'Fatal Error'
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function getArticleComments()
        {

            Log::debug("Started ... editType : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $commentService = new CommentService;

                $response = $commentService->getArticleComments($jsonInput);

            }catch(\Exception $e){
                $response = [
                    'status'=>'999',
                    'message'=>'Fatal Error'
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function getArticleCommentReply()
        {

            Log::debug("Started ... editType : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $commentService = new CommentService;

                $response = $commentService->getArticleCommentReply($jsonInput);

            }catch(\Exception $e){
                $response = [
                    'status'=>'999',
                    'message'=>'Fatal Error'
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function editArticleDiscussion()
        {

            Log::debug("Started ... session editArticleDiscussion : ");
                $response = null;
            try{

                $jsonInput = $this->request->getData();
                
                $sessionService = new SessionService;

                $response = $sessionService->editArticleDiscussion($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function deleteArticleDiscussion()
        {

            Log::debug("Started ... session deleteArticleDiscussion : ");
                $response = null;
            try{

                $jsonInput = $this->request->getData();
                
                $sessionService = new SessionService;

                $response = $sessionService->deleteArticleDiscussion($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function search()
        {

            Log::debug("Started ... session search : ");
                $response = null;
            try{

                $jsonInput = $this->request->getData();
                
                $commentService = new ConstantService;

                $response = $commentService->search($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function getPosts()
        {

            Log::debug("Started ... session getPosts : ");
                $response = null;
            try{

                $jsonInput = $this->request->getData();
                
                $commentService = new CommentService;

                $response = $commentService->getPosts($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function myActivity()
        {

            Log::debug("Started ... session myActivity : ");
                $response = null;
            try{

                $jsonInput = $this->request->getData();
                
                $commentService = new CommentService;

                $response = $commentService->myActivity($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function allLikes()
        {

            Log::debug("Started ... session allLikes : ");
                $response = null;
            try{

                $jsonInput = $this->request->getData();
                
                $commentService = new CommentService;

                $response = $commentService->allLikes($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function articleComment()
        {

            Log::debug("Started ... editType : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $commentService = new CommentService;

                $response = $commentService->articleComment($jsonInput);

            }catch(\Exception $e){
                $response = [
                    'status'=>'999',
                    'message'=>'Fatal Error'
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function articleCommentReply()
        {

            Log::debug("Started ... editType : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $commentService = new CommentService;

                $response = $commentService->articleCommentReply($jsonInput);

            }catch(\Exception $e){
                $response = [
                    'status'=>'999',
                    'message'=>'Fatal Error'
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function articleReaction()
        {

            Log::debug("Started ... editType : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $commentService = new CommentService;

                $response = $commentService->articleReaction($jsonInput);

            }catch(\Exception $e){
                $response = [
                    'status'=>'999',
                    'message'=>'Fatal Error'
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        //rhythm changes 07-05
        
        //adding api3 rhythm
        public function getOrganizationPosts()
        {
            Log::debug("Started ... getOrganizationPosts : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $orgService = new OrganizationService;

                $response = $orgService->getOrganizationPosts($jsonInput);

            }catch(\Exception $e){
                $response = [
                    'status'=>'999',
                    'message'=>'Fatal Error'
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }
        }
        //adding api4 rhythm
        public function getDriveDetail(){
            Log::debug("Started ... getOrganizationPosts : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $orgService = new OrganizationService;

                $response = $orgService->getDriveDetail($jsonInput);

            }catch(\Exception $e){
                $response = [
                    'status'=>'999',
                    'message'=>'Fatal Error'
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }
        }
        //adding api5 rhythm
        public function getCandidatesList(){
            Log::debug("Started ... getCandidatesList : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $orgService = new OrganizationService;

                $response = $orgService->getCandidatesList($jsonInput);

            }catch(\Exception $e){
                $response = [
                    'status'=>'999',
                    'message'=>'Fatal Error'
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }
        }

        //adding api6 rhythm
        public function updateCandidateStatus(){
            Log::debug("Started ... updateCandidateStatus : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $orgService = new OrganizationService;

                $response = $orgService->updateCandidateStatus($jsonInput);

            }catch(\Exception $e){
                $response = [
                    'status'=>'999',
                    'message'=>'Fatal Error'
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }
        }
        
        //adding api7 rhythm
        public function applyCampus(){
            Log::debug("Started ... updateCandidateStatus : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $orgService = new OrganizationService;

                $response = $orgService->applyCampus($jsonInput);

            }catch(\Exception $e){
                $response = [
                    'status'=>'999',
                    'message'=>'Fatal Error'
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }
        }

        //adding api8 rhythm
        public function getClusterList(){
            Log::debug("Started ... getClusterList : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $orgService = new OrganizationService;

                $response = $orgService->getClusterList($jsonInput);

            }catch(\Exception $e){
                $response = [
                    'status'=>'999',
                    'message'=>'Fatal Error'
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }
        }


        //adding api9 rhythm
        public function getSkillsList(){
            Log::debug("Started ... getSkillsList : ");
            $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $orgService = new OrganizationService;

                $response = $orgService->getSkillsList($jsonInput);

            }catch(\Exception $e){
                $response = [
                    'status'=>'999',
                    'message'=>'Fatal Error'
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }
        }

        //adding api10 rhythm
        public function pushQuery(){
            Log::debug("Started ... getSkillsList : ");
            $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $orgService = new OrganizationService;

                $response = $orgService->push_query($jsonInput);

            }catch(\Exception $e){
                $response = [
                    'status'=>'999',
                    'message'=>'Fatal Error',
                    'exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }
        }
        public function sendVerifyOtp(){
            Log::debug("Started ... getSkillsList : ");
            $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $orgService = new OrganizationService;

                $response = $orgService->send_verify_otp($jsonInput);

            }catch(\Exception $e){
                $response = [
                    'status'=>'999',
                    'message'=>'Fatal Error',
                    'exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }
        }

        //adding api 11 rhythm
        public function updateDrive(){
            Log::debug("Started ... updateDrive : ");
            $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $orgService = new OrganizationService;

                $response = $orgService->updateDrive($jsonInput);

            }catch(\Exception $e){
                $response = [
                    'status'=>'999',
                    'message'=>'Fatal Error'
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }
        }

        //adding api 12 rhythm
        public function saveEducationDetails(){
            Log::debug("Started ... saveEducationDetails : ");
            $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $orgService = new OrganizationService;

                $response = $orgService->saveEducationDetails($jsonInput);

            }catch(\Exception $e){
                $response = [
                    'status'=>'999',
                    'message'=>'Fatal Error'
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }
        }
        //adding api 13 rhythm
        public function getCourseList(){
            Log::debug("Started ... getCourseList : ");
            $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $orgService = new OrganizationService;

                $response = $orgService->getCourseList($jsonInput);

            }catch(\Exception $e){
                $response = [
                    'status'=>'999',
                    'message'=>'Fatal Error'
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }
        }

        //adding api 14 rhythm
        public function getFieldofstudyList(){
            Log::debug("Started ... getFieldofstudyList : ");
            $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $orgService = new OrganizationService;

                $response = $orgService->getFieldofstudyList($jsonInput);

            }catch(\Exception $e){
                $response = [
                    'status'=>'999',
                    'message'=>'Fatal Error'
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }
        }

        //venue apis
        public function addVenue(){
            Log::debug("Started ... addVenue : ");
            $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $orgService = new VenueService;

                $response = $orgService->addVenue($jsonInput);

            }catch(\Exception $e){
                $response = [
                    'status'=>'999',
                    'message'=>'Fatal Error'
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }
        }

        public function venueOtpVerify(){
            Log::debug("Started ... venueOtpVerify : ");
            $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $orgService = new VenueService;

                $response = $orgService->venueOtpVerify($jsonInput);

            }catch(\Exception $e){
                $response = [
                    'status'=>'999',
                    'message'=>'Fatal Error'
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }
        }

        public function editVenue(){
            Log::debug("Started ... editVenue : ");
            $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $orgService = new VenueService;

                $response = $orgService->editVenue($jsonInput);

            }catch(\Exception $e){
                $response = [
                    'status'=>'999',
                    'message'=>'Fatal Error'
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }
        }
        public function deleteVenueCover(){
            Log::debug("Started ... deleteVenueCover : ");
            $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $orgService = new VenueService;

                $response = $orgService->deleteVenueCover($jsonInput);

            }catch(\Exception $e){
                $response = [
                    'status'=>'999',
                    'message'=>'Fatal Error'
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }
        }
        public function addVenueCover(){
            Log::debug("Started ... addVenueCover : ");
            $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $orgService = new VenueService;

                $response = $orgService->addVenueCover($jsonInput);

            }catch(\Exception $e){
                $response = [
                    'status'=>'999',
                    'message'=>'Fatal Error'
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }
        }


        public function getVenue(){
            Log::debug("Started ... getVenue : ");
            $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $orgService = new VenueService;

                $response = $orgService->getVenue($jsonInput);

            }catch(\Exception $e){
                $response = [
                    'status'=>'999',
                    'message'=>'Fatal Error'
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }
        }

        //new dated 17/05
        public function venueDriveList()
        {

            Log::debug("Started ... schedule_campus : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $venueService = new VenueService;

                $response = $venueService->getCampusDriveList($jsonInput);

            }catch(\Exception $e){
                $response = [
                    '_ReturnCode'=>'999',
                    '_ReturnMessage'=>'Fatal Error',
                    '_Exception'=>$e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }  
        }

        public function getVenuePosts()
        {
            Log::debug("Started ... getOrganizationPosts : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $orgService = new VenueService;

                $response = $orgService->getVenuePosts($jsonInput);

            }catch(\Exception $e){
                $response = [
                    'status'=>'999',
                    'message'=>'Fatal Error'
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }
        }
        

        public function signupAdditional()
        {
            Log::debug("Started ... getOrganizationPosts : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $orgService = new RegistrationService;

                $response = $orgService->signupAdditional($jsonInput);

            }catch(\Exception $e){
                $response = [
                    'status'=>'999',
                    'message'=>'Fatal Error'
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }
        }


        public function currentVersion(){
            Log::debug("Started ... getOrganizationPosts : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $orgService = new RegistrationService;

                $response = $orgService->currentVersion($jsonInput);

            }catch(\Exception $e){
                $response = [
                    'status'=>'999',
                    'message'=>'Fatal Error',
                    'exception' => $e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }
        }

        public function siteMail(){
            Log::debug("Started ... getOrganizationPosts : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $orgService = new RegistrationService;

                $response = $orgService->siteMail($jsonInput);

            }catch(\Exception $e){
                $response = [
                    'status'=>'999',
                    'message'=>'Fatal Error',
                    'exception' => $e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }
        }

        public function createArticle(){
            Log::debug("Started ... getOrganizationPosts : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $orgService = new CommentService;

                $response = $orgService->createArticle($jsonInput);

            }catch(\Exception $e){
                $response = [
                    'status'=>'999',
                    'message'=>'Fatal Error',
                    'exception' => $e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }
        }

        public function getArticle(){
            Log::debug("Started ... getOrganizationPosts : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $orgService = new CommentService;

                $response = $orgService->getArticle($jsonInput);

            }catch(\Exception $e){
                $response = [
                    'status'=>'999',
                    'message'=>'Fatal Error',
                    'exception' => $e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }
        }

        public function editArticle(){
            Log::debug("Started ... getOrganizationPosts : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $orgService = new CommentService;

                $response = $orgService->editArticle($jsonInput);

            }catch(\Exception $e){
                $response = [
                    'status'=>'999',
                    'message'=>'Fatal Error',
                    'exception' => $e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }
        }

        public function followUnfollowOrganization(){
            Log::debug("Started ... getOrganizationPosts : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $orgService = new OrganizationService;

                $response = $orgService->followUnfollowOrg($jsonInput);

            }catch(\Exception $e){
                $response = [
                    'status'=>'999',
                    'message'=>'Fatal Error',
                    'exception' => $e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }
        }


        public function deleteNotification(){
            Log::debug("Started ... getOrganizationPosts : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $orgService = new CommentService;

                $response = $orgService->deleteNotification($jsonInput);

            }catch(\Exception $e){
                $response = [
                    'status'=>'999',
                    'message'=>'Fatal Error',
                    'exception' => $e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }
        }

        public function createPoll(){
            Log::debug("Started ... getOrganizationPosts : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $orgService = new ConstantService;

                $response = $orgService->createPoll($jsonInput);

            }catch(\Exception $e){
                $response = [
                    'status'=>'999',
                    'message'=>'Fatal Error',
                    'exception' => $e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }
        }
        public function getReactionTypes(){
            Log::debug("Started ... getOrganizationPosts : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $orgService = new CommentService;

                $response = $orgService->getReactions($jsonInput);

            }catch(\Exception $e){
                $response = [
                    'status'=>'999',
                    'message'=>'Fatal Error',
                    'exception' => $e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }
        }

        public function react(){
            Log::debug("Started ... getOrganizationPosts : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $orgService = new ConstantService;

                $response = $orgService->react($jsonInput);

            }catch(\Exception $e){
                $response = [
                    'status'=>'999',
                    'message'=>'Fatal Error',
                    'exception' => $e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }
        }

        public function collegeStudentData(){
            Log::debug("Started ... getOrganizationPosts : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $orgService = new VenueService;

                $response = $orgService->collegeStudentData($jsonInput);

            }catch(\Exception $e){
                $response = [
                    'status'=>'999',
                    'message'=>'Fatal Error',
                    'exception' => $e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }
        }

        public function collegeStudentList(){
            Log::debug("Started ... getOrganizationPosts : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $orgService = new VenueService;

                $response = $orgService->collegeStudentList($jsonInput);

            }catch(\Exception $e){
                $response = [
                    'status'=>'999',
                    'message'=>'Fatal Error',
                    'exception' => $e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }
        }

        public function orgClusterData(){
            Log::debug("Started ... orgClusterData : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $orgService = new OrganizationService;

                $response = $orgService->orgClusterData($jsonInput);

            }catch(\Exception $e){
                $response = [
                    'status'=>'999',
                    'message'=>'Fatal Error',
                    'exception' => $e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }
        }

        public function report(){
            Log::debug("Started ... report : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $orgService = new ConstantService;

                $response = $orgService->report($jsonInput);

            }catch(\Exception $e){
                $response = [
                    'status'=>'999',
                    'message'=>'Fatal Error',
                    'exception' => $e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }
        }

        public function reportMaster(){
            Log::debug("Started ... reportMaster : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $orgService = new ConstantService;

                $response = $orgService->reportMaster($jsonInput);

            }catch(\Exception $e){
                $response = [
                    'status'=>'999',
                    'message'=>'Fatal Error',
                    'exception' => $e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }
        }


        public function settings(){
            Log::debug("Started ... settings : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $orgService = new ConstantService;

                $response = $orgService->settings($jsonInput);

            }catch(\Exception $e){
                $response = [
                    'status'=>'999',
                    'message'=>'Fatal Error',
                    'exception' => $e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }
        }

        public function getRewardsBadges(){
            Log::debug("Started ... getRewardsBadges : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $orgService = new CommentService;

                $response = $orgService->getRewardsBadges($jsonInput);

            }catch(\Exception $e){
                $response = [
                    'status'=>'999',
                    'message'=>'Fatal Error',
                    'exception' => $e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }
        }

        public function addUseReward(){
            Log::debug("Started ... addUseReward : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $orgService = new CommentService;

                $response = $orgService->addUseReward($jsonInput);

            }catch(\Exception $e){
                $response = [
                    'status'=>'999',
                    'message'=>'Fatal Error',
                    'exception' => $e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }
        }
        public function getParticipantsLeaderBoard(){
            Log::debug("Started ... addUseReward : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $orgService = new QuizService;

                $response = $orgService->getParticipantsLeaderBoard($jsonInput);

            }catch(\Exception $e){
                $response = [
                    'status'=>'999',
                    'message'=>'Fatal Error',
                    'exception' => $e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }
        }
        public function newsFeed(){
            Log::debug("Started ... addUseReward : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $orgService = new CommentService;

                $response = $orgService->newsFeed($jsonInput);

            }catch(\Exception $e){
                $response = [
                    'status'=>'999',
                    'message'=>'Fatal Error',
                    'exception' => $e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }
        }


        public function profile(){
            Log::debug("Started ... addUseReward : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $orgService = new RegistrationService;

                $response = $orgService->profile($jsonInput);

            }catch(\Exception $e){
                $response = [
                    'status'=>'999',
                    'message'=>'Fatal Error',
                    'exception' => $e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }
        }

        public function getPoll(){
            Log::debug("Started ... addUseReward : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $orgService = new CommentService;

                $response = $orgService->getPoll($jsonInput);

            }catch(\Exception $e){
                $response = [
                    'status'=>'999',
                    'message'=>'Fatal Error',
                    'exception' => $e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }
        }

        public function faq(){
            Log::debug("Started ... addUseReward : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $orgService = new ConstantService;

                $response = $orgService->faq($jsonInput);

            }catch(\Exception $e){
                $response = [
                    'status'=>'999',
                    'message'=>'Fatal Error',
                    'exception' => $e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }
        }


        public function share(){
            Log::debug("Started ... addUseReward : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $orgService = new CommentService;

                $response = $orgService->share($jsonInput);

            }catch(\Exception $e){
                $response = [
                    'status'=>'999',
                    'message'=>'Fatal Error',
                    'exception' => $e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }
        }

        public function updateUserInterest(){
            Log::debug("Started ... addUseReward : ");
             $response = null;
            try{
                $jsonInput = $this->request->getData();
                
                $orgService = new ConstantService;

                $response = $orgService->updateUserInterest($jsonInput);

            }catch(\Exception $e){
                $response = [
                    'status'=>'999',
                    'message'=>'Fatal Error',
                    'exception' => $e
                ];

            }finally{
                $this->setResponse($this->response->withStatus(200));
                $this->set([
                    'response' => $response,
                    '_serialize' => 'response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }
        }



        //end changes 07-05 rhythm

       
    }
