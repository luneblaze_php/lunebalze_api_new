<?php
namespace App\Controller;

use App\Authentication\AuthService;
use App\ServiceLayer\ConstantService;
use App\ServiceLayer\OrganizationService;
use App\ServiceLayer\ChatService;
use App\ServiceLayer\FriendService;
use App\ServiceLayer\ProductService;
use App\ServiceLayer\QuizService;
use App\ServiceLayer\DebateService;
use App\ServiceLayer\CommunityService;
use Cake\Controller\Controller;
use App\ServiceLayer\CommentService;
use Cake\Log\Log; 
use Cake\Event\Event;


use App\ServiceLayer\RegistrationService;

   
class ChatController extends AppController
{


    public function delete()
    {

        Log::debug("Started ... chat delete : ");
            $response = null;
        try{

            $jsonInput = $this->request->getData();
            
            $chatService = new ChatService;

            $response = $chatService->delete($jsonInput);

        }catch(\Exception $e){
            $response = [
                '_ReturnCode'=>'999',
                '_ReturnMessage'=>'Fatal Error',
                '_Exception'=>$e
            ];

        }finally{
            $this->setResponse($this->response->withStatus(200));
            $this->set([
                'response' => $response,
                '_serialize' => 'response',
            ]);
            $this->RequestHandler->renderAs($this, 'json');
        }  
    }

    public function getMessagenew()
    {

        Log::debug("Started ... chat getMessageNew : ");
            $response = null;
        try{

            $jsonInput = $this->request->getData();
            
            $chatService = new ChatService;

            $response = $chatService->getMessageNew($jsonInput);

        }catch(\Exception $e){
            $response = [
                '_ReturnCode'=>'999',
                '_ReturnMessage'=>'Fatal Error',
                '_Exception'=>$e
            ];

        }finally{
            $this->setResponse($this->response->withStatus(200));
            $this->set([
                'response' => $response,
                '_serialize' => 'response',
            ]);
            $this->RequestHandler->renderAs($this, 'json');
        }  
    }

    public function createGroup()
    {

        Log::debug("Started ... chat createGroup : ");
            $response = null;
        try{

            $jsonInput = $this->request->getData();
            
            $chatService = new ChatService;

            $response = $chatService->createGroup($jsonInput);

        }catch(\Exception $e){
            $response = [
                '_ReturnCode'=>'999',
                '_ReturnMessage'=>'Fatal Error',
                '_Exception'=>$e
            ];

        }finally{
            $this->setResponse($this->response->withStatus(200));
            $this->set([
                'response' => $response,
                '_serialize' => 'response',
            ]);
            $this->RequestHandler->renderAs($this, 'json');
        }  
    }

    public function editGroup()
    {

        Log::debug("Started ... chat createGroup : ");
            $response = null;
        try{

            $jsonInput = $this->request->getData();
            
            $chatService = new ChatService;

            $response = $chatService->editGroup($jsonInput);

        }catch(\Exception $e){
            $response = [
                '_ReturnCode'=>'999',
                '_ReturnMessage'=>'Fatal Error',
                '_Exception'=>$e
            ];

        }finally{
            $this->setResponse($this->response->withStatus(200));
            $this->set([
                'response' => $response,
                '_serialize' => 'response',
            ]);
            $this->RequestHandler->renderAs($this, 'json');
        }  
    }

    public function getGroup()
    {

        Log::debug("Started ... chat getGroup : ");
            $response = null;
        try{

            $jsonInput = $this->request->getData();
            
            $chatService = new ChatService;

            $response = $chatService->getGroup($jsonInput);

        }catch(\Exception $e){
            $response = [
                '_ReturnCode'=>'999',
                '_ReturnMessage'=>'Fatal Error',
                '_Exception'=>$e
            ];

        }finally{
            $this->setResponse($this->response->withStatus(200));
            $this->set([
                'response' => $response,
                '_serialize' => 'response',
            ]);
            $this->RequestHandler->renderAs($this, 'json');
        }  
    }

    public function leaveGroup()
    {

        Log::debug("Started ... chat leaveGroup : ");
            $response = null;
        try{

            $jsonInput = $this->request->getData();
            
            $chatService = new ChatService;

            $response = $chatService->leaveGroup($jsonInput);

        }catch(\Exception $e){
            $response = [
                '_ReturnCode'=>'999',
                '_ReturnMessage'=>'Fatal Error',
                '_Exception'=>$e
            ];

        }finally{
            $this->setResponse($this->response->withStatus(200));
            $this->set([
                'response' => $response,
                '_serialize' => 'response',
            ]);
            $this->RequestHandler->renderAs($this, 'json');
        }  
    }

    public function createGetConversation()
    {

        Log::debug("Started ... chat createGetConversation : ");
            $response = null;
        try{

            $jsonInput = $this->request->getData();
            
            $chatService = new ChatService;

            $response = $chatService->createGetConversation($jsonInput);

        }catch(\Exception $e){
            $response = [
                '_ReturnCode'=>'999',
                '_ReturnMessage'=>'Fatal Error',
                '_Exception'=>$e
            ];

        }finally{
            $this->setResponse($this->response->withStatus(200));
            $this->set([
                'response' => $response,
                '_serialize' => 'response',
            ]);
            $this->RequestHandler->renderAs($this, 'json');
        }  
    }

    public function addGroupMembers()
    {

        Log::debug("Started ... chat addGroupMembers : ");
            $response = null;
        try{

            $jsonInput = $this->request->getData();
            
            $chatService = new ChatService;

            $response = $chatService->addGroupMembers($jsonInput);

        }catch(\Exception $e){
            $response = [
                '_ReturnCode'=>'999',
                '_ReturnMessage'=>'Fatal Error',
                '_Exception'=>$e
            ];

        }finally{
            $this->setResponse($this->response->withStatus(200));
            $this->set([
                'response' => $response,
                '_serialize' => 'response',
            ]);
            $this->RequestHandler->renderAs($this, 'json');
        }  
    }

    public function removeMembers()
    {

        Log::debug("Started ... chat removeMembers : ");
            $response = null;
        try{

            $jsonInput = $this->request->getData();
            
            $chatService = new ChatService;

            $response = $chatService->removeMembers($jsonInput);

        }catch(\Exception $e){
            $response = [
                '_ReturnCode'=>'999',
                '_ReturnMessage'=>'Fatal Error',
                '_Exception'=>$e
            ];

        }finally{
            $this->setResponse($this->response->withStatus(200));
            $this->set([
                'response' => $response,
                '_serialize' => 'response',
            ]);
            $this->RequestHandler->renderAs($this, 'json');
        }  
    }

    public function addAdmin()
    {

        Log::debug("Started ... chat addAdmin : ");
            $response = null;
        try{

            $jsonInput = $this->request->getData();
            
            $chatService = new ChatService;

            $response = $chatService->addAdmin($jsonInput);

        }catch(\Exception $e){
            $response = [
                '_ReturnCode'=>'999',
                '_ReturnMessage'=>'Fatal Error',
                '_Exception'=>$e
            ];

        }finally{
            $this->setResponse($this->response->withStatus(200));
            $this->set([
                'response' => $response,
                '_serialize' => 'response',
            ]);
            $this->RequestHandler->renderAs($this, 'json');
        }  
    }

    public function removeAdmin()
    {

        Log::debug("Started ... chat removeAdmin : ");
            $response = null;
        try{

            $jsonInput = $this->request->getData();
            
            $chatService = new ChatService;

            $response = $chatService->removeAdmin($jsonInput);

        }catch(\Exception $e){
            $response = [
                '_ReturnCode'=>'999',
                '_ReturnMessage'=>'Fatal Error',
                '_Exception'=>$e
            ];

        }finally{
            $this->setResponse($this->response->withStatus(200));
            $this->set([
                'response' => $response,
                '_serialize' => 'response',
            ]);
            $this->RequestHandler->renderAs($this, 'json');
        }  
    }

    public function messageSeen()
    {

        Log::debug("Started ... chat messageSeen : ");
            $response = null;
        try{

            $jsonInput = $this->request->getData();
            
            $chatService = new ChatService;

            $response = $chatService->messageSeen($jsonInput);

        }catch(\Exception $e){
            $response = [
                '_ReturnCode'=>'999',
                '_ReturnMessage'=>'Fatal Error',
                '_Exception'=>$e
            ];

        }finally{
            $this->setResponse($this->response->withStatus(200));
            $this->set([
                'response' => $response,
                '_serialize' => 'response',
            ]);
            $this->RequestHandler->renderAs($this, 'json');
        }  
    }

    public function createMessage()
    {

        Log::debug("Started ... chat createMessage : ");
            $response = null;
        try{

            $jsonInput = $this->request->getData();
            
            $chatService = new ChatService;

            $response = $chatService->createMessage($jsonInput);

        }catch(\Exception $e){
            $response = [
                '_ReturnCode'=>'999',
                '_ReturnMessage'=>'Fatal Error',
                '_Exception'=>$e
            ];

        }finally{
            $this->setResponse($this->response->withStatus(200));
            $this->set([
                'response' => $response,
                '_serialize' => 'response',
            ]);
            $this->RequestHandler->renderAs($this, 'json');
        }  
    }

    public function createMultimessage()
    {

        Log::debug("Started ... chat createMultimessage : ");
            $response = null;
        try{

            $jsonInput = $this->request->getData();
            
            $chatService = new ChatService;

            $response = $chatService->createMultimessage($jsonInput);

        }catch(\Exception $e){
            $response = [
                '_ReturnCode'=>'999',
                '_ReturnMessage'=>'Fatal Error',
                '_Exception'=>$e
            ];

        }finally{
            $this->setResponse($this->response->withStatus(200));
            $this->set([
                'response' => $response,
                '_serialize' => 'response',
            ]);
            $this->RequestHandler->renderAs($this, 'json');
        }  
    }
}