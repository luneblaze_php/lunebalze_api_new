<?php
namespace App\Controller;

use App\Authentication\AuthService;
use App\ServiceLayer\ConstantService;
use App\ServiceLayer\OrganizationService;
use App\ServiceLayer\SessionService;
use App\ServiceLayer\FriendService;
use App\ServiceLayer\ProductService;
use App\ServiceLayer\QuizService;
use App\ServiceLayer\DebateService;
use App\ServiceLayer\CommunityService;
use Cake\Controller\Controller;
use App\ServiceLayer\CommentService;
use Cake\Log\Log; 
use Cake\Event\Event;


use App\ServiceLayer\RegistrationService;

   
class SessionController extends AppController
{
    public function getVenueList()
    {

        Log::debug("Started ... session getVenueList : ");
            $response = null;
        try{

            $jsonInput = $this->request->getData();
            
            $sessionService = new SessionService;

            $response = $sessionService->getVenueList($jsonInput);

        }catch(\Exception $e){
            $response = [
                '_ReturnCode'=>'999',
                '_ReturnMessage'=>'Fatal Error',
                '_Exception'=>$e
            ];

        }finally{
            $this->setResponse($this->response->withStatus(200));
            $this->set([
                'response' => $response,
                '_serialize' => 'response',
            ]);
            $this->RequestHandler->renderAs($this, 'json');
        }  
    }

    public function setSessionStatus()
    {

        Log::debug("Started ... session setSessionStatus : ");
            $response = null;
        try{

            $jsonInput = $this->request->getData();
            
            $sessionService = new SessionService;

            $response = $sessionService->setSessionStatus($jsonInput);

        }catch(\Exception $e){
            $response = [
                '_ReturnCode'=>'999',
                '_ReturnMessage'=>'Fatal Error',
                '_Exception'=>$e
            ];

        }finally{
            $this->setResponse($this->response->withStatus(200));
            $this->set([
                'response' => $response,
                '_serialize' => 'response',
            ]);
            $this->RequestHandler->renderAs($this, 'json');
        }  
    }

    public function suggestPollVenue()
    {

        Log::debug("Started ... session suggestPollVenue : ");
            $response = null;
        try{

            $jsonInput = $this->request->getData();
            
            $sessionService = new SessionService;

            $response = $sessionService->suggestPollVenue($jsonInput);

        }catch(\Exception $e){
            $response = [
                '_ReturnCode'=>'999',
                '_ReturnMessage'=>'Fatal Error',
                '_Exception'=>$e
            ];

        }finally{
            $this->setResponse($this->response->withStatus(200));
            $this->set([
                'response' => $response,
                '_serialize' => 'response',
            ]);
            $this->RequestHandler->renderAs($this, 'json');
        }  
    }

    public function allSessionsV1()
    {

        Log::debug("Started ... session allSessionsV1 : ");
            $response = null;
        try{

            $jsonInput = $this->request->getData();
            
            $sessionService = new SessionService;

            $response = $sessionService->allSessionsV1($jsonInput);

        }catch(\Exception $e){
            $response = [
                '_ReturnCode'=>'999',
                '_ReturnMessage'=>'Fatal Error',
                '_Exception'=>$e
            ];

        }finally{
            $this->setResponse($this->response->withStatus(200));
            $this->set([
                'response' => $response,
                '_serialize' => 'response',
            ]);
            $this->RequestHandler->renderAs($this, 'json');
        }  
    }

    public function markUserAttendStatus()
    {

        Log::debug("Started ... session markUserAttendStatus : ");
            $response = null;
        try{

            $jsonInput = $this->request->getData();
            
            $sessionService = new SessionService;

            $response = $sessionService->markUserAttendStatus($jsonInput);

        }catch(\Exception $e){
            $response = [
                '_ReturnCode'=>'999',
                '_ReturnMessage'=>'Fatal Error',
                '_Exception'=>$e
            ];

        }finally{
            $this->setResponse($this->response->withStatus(200));
            $this->set([
                'response' => $response,
                '_serialize' => 'response',
            ]);
            $this->RequestHandler->renderAs($this, 'json');
        }  
    }

    public function deleteSessionComment()
    {

        Log::debug("Started ... session deleteSessionComment : ");
            $response = null;
        try{

            $jsonInput = $this->request->getData();
            
            $sessionService = new SessionService;

            $response = $sessionService->deleteSessionComment($jsonInput);

        }catch(\Exception $e){
            $response = [
                '_ReturnCode'=>'999',
                '_ReturnMessage'=>'Fatal Error',
                '_Exception'=>$e
            ];

        }finally{
            $this->setResponse($this->response->withStatus(200));
            $this->set([
                'response' => $response,
                '_serialize' => 'response',
            ]);
            $this->RequestHandler->renderAs($this, 'json');
        }  
    }

    public function editSessionComment()
    {

        Log::debug("Started ... session editSessionComment : ");
            $response = null;
        try{

            $jsonInput = $this->request->getData();
            
            $sessionService = new SessionService;

            $response = $sessionService->editSessionComment($jsonInput);

        }catch(\Exception $e){
            $response = [
                '_ReturnCode'=>'999',
                '_ReturnMessage'=>'Fatal Error',
                '_Exception'=>$e
            ];

        }finally{
            $this->setResponse($this->response->withStatus(200));
            $this->set([
                'response' => $response,
                '_serialize' => 'response',
            ]);
            $this->RequestHandler->renderAs($this, 'json');
        }  
    }

    public function uploadVideo()
    {

        Log::debug("Started ... session uploadVideo : ");
            $response = null;
        try{

            $jsonInput = $this->request->getData();
            
            $sessionService = new SessionService;

            $response = $sessionService->uploadVideo($jsonInput);

        }catch(\Exception $e){
            $response = [
                '_ReturnCode'=>'999',
                '_ReturnMessage'=>'Fatal Error',
                '_Exception'=>$e
            ];

        }finally{
            $this->setResponse($this->response->withStatus(200));
            $this->set([
                'response' => $response,
                '_serialize' => 'response',
            ]);
            $this->RequestHandler->renderAs($this, 'json');
        }  
    }

    public function sessionPhotodelete()
    {

        Log::debug("Started ... session sessionPhotodelete : ");
            $response = null;
        try{

            $jsonInput = $this->request->getData();
            
            $sessionService = new SessionService;

            $response = $sessionService->sessionPhotodelete($jsonInput);

        }catch(\Exception $e){
            $response = [
                '_ReturnCode'=>'999',
                '_ReturnMessage'=>'Fatal Error',
                '_Exception'=>$e
            ];

        }finally{
            $this->setResponse($this->response->withStatus(200));
            $this->set([
                'response' => $response,
                '_serialize' => 'response',
            ]);
            $this->RequestHandler->renderAs($this, 'json');
        }  
    }

    public function suggestNewVenues()
    {

        Log::debug("Started ... session suggestNewVenues : ");
            $response = null;
        try{

            $jsonInput = $this->request->getData();
            
            $sessionService = new SessionService;

            $response = $sessionService->suggestNewVenues($jsonInput);

        }catch(\Exception $e){
            $response = [
                '_ReturnCode'=>'999',
                '_ReturnMessage'=>'Fatal Error',
                '_Exception'=>$e
            ];

        }finally{
            $this->setResponse($this->response->withStatus(200));
            $this->set([
                'response' => $response,
                '_serialize' => 'response',
            ]);
            $this->RequestHandler->renderAs($this, 'json');
        }  
    }

    public function suggestNewDates()
    {

        Log::debug("Started ... session suggestNewDates : ");
            $response = null;
        try{

            $jsonInput = $this->request->getData();
            
            $sessionService = new SessionService;

            $response = $sessionService->suggestNewDates($jsonInput);

        }catch(\Exception $e){
            $response = [
                '_ReturnCode'=>'999',
                '_ReturnMessage'=>'Fatal Error',
                '_Exception'=>$e
            ];

        }finally{
            $this->setResponse($this->response->withStatus(200));
            $this->set([
                'response' => $response,
                '_serialize' => 'response',
            ]);
            $this->RequestHandler->renderAs($this, 'json');
        }  
    }

    public function poll()
    {

        Log::debug("Started ... session poll : ");
            $response = null;
        try{

            $jsonInput = $this->request->getData();
            
            $sessionService = new SessionService;

            $response = $sessionService->poll($jsonInput);

        }catch(\Exception $e){
            $response = [
                '_ReturnCode'=>'999',
                '_ReturnMessage'=>'Fatal Error',
                '_Exception'=>$e
            ];

        }finally{
            $this->setResponse($this->response->withStatus(200));
            $this->set([
                'response' => $response,
                '_serialize' => 'response',
            ]);
            $this->RequestHandler->renderAs($this, 'json');
        }  
    }

    public function deletePoll()
    {

        Log::debug("Started ... delete Poll : ");
            $response = null;
        try{

            $jsonInput = $this->request->getData();
            
            $sessionService = new SessionService;

            $response = $sessionService->deletePoll($jsonInput);

        }catch(\Exception $e){
            $response = [
                '_ReturnCode'=>'999',
                '_ReturnMessage'=>'Fatal Error',
                '_Exception'=>$e
            ];

        }finally{
            $this->setResponse($this->response->withStatus(200));
            $this->set([
                'response' => $response,
                '_serialize' => 'response',
            ]);
            $this->RequestHandler->renderAs($this, 'json');
        }  
    }

    public function getPollVotingUsers()
    {

        Log::debug("Started ... get Poll Voting Users : ");
            $response = null;
        try{

            $jsonInput = $this->request->getData();
            
            $sessionService = new SessionService;

            $response = $sessionService->getPollVotingUsers($jsonInput);

        }catch(\Exception $e){
            $response = [
                '_ReturnCode'=>'999',
                '_ReturnMessage'=>'Fatal Error',
                '_Exception'=>$e
            ];

        }finally{
            $this->setResponse($this->response->withStatus(200));
            $this->set([
                'response' => $response,
                '_serialize' => 'response',
            ]);
            $this->RequestHandler->renderAs($this, 'json');
        }  
    }

    public function edit()
    {

        Log::debug("Started ...edit : ");
            $response = null;
        try{

            $jsonInput = $this->request->getData();
            
            $sessionService = new SessionService;

            $response = $sessionService->edit($jsonInput);

        }catch(\Exception $e){
            $response = [
                '_ReturnCode'=>'999',
                '_ReturnMessage'=>'Fatal Error',
                '_Exception'=>$e
            ];

        }finally{
            $this->setResponse($this->response->withStatus(200));
            $this->set([
                'response' => $response,
                '_serialize' => 'response',
            ]);
            $this->RequestHandler->renderAs($this, 'json');
        }  
    }

    public function postCommentReply()
    {

        Log::debug("Started ...postCommentReply : ");
            $response = null;
        try{

            $jsonInput = $this->request->getData();
            
            $sessionService = new SessionService;

            $response = $sessionService->postCommentReply($jsonInput);

        }catch(\Exception $e){
            $response = [
                '_ReturnCode'=>'999',
                '_ReturnMessage'=>'Fatal Error',
                '_Exception'=>$e
            ];

        }finally{
            $this->setResponse($this->response->withStatus(200));
            $this->set([
                'response' => $response,
                '_serialize' => 'response',
            ]);
            $this->RequestHandler->renderAs($this, 'json');
        }  
    }

    public function assignRoom()
    {

        Log::debug("Started ...assignRoom : ");
            $response = null;
        try{

            $jsonInput = $this->request->getData();
            
            $sessionService = new SessionService;

            $response = $sessionService->assignRoom($jsonInput);

        }catch(\Exception $e){
            $response = [
                '_ReturnCode'=>'999',
                '_ReturnMessage'=>'Fatal Error',
                '_Exception'=>$e
            ];

        }finally{
            $this->setResponse($this->response->withStatus(200));
            $this->set([
                'response' => $response,
                '_serialize' => 'response',
            ]);
            $this->RequestHandler->renderAs($this, 'json');
        }  
    }

    public function getAnswerReplies()
    {

        Log::debug("Started ...getAnswerReplies : ");
            $response = null;
        try{

            $jsonInput = $this->request->getData();
            
            $sessionService = new SessionService;

            $response = $sessionService->getAnswerReplies($jsonInput);

        }catch(\Exception $e){
            $response = [
                '_ReturnCode'=>'999',
                '_ReturnMessage'=>'Fatal Error',
                '_Exception'=>$e
            ];

        }finally{
            $this->setResponse($this->response->withStatus(200));
            $this->set([
                'response' => $response,
                '_serialize' => 'response',
            ]);
            $this->RequestHandler->renderAs($this, 'json');
        }  
    }

    public function editSessionAnswer()
    {

        Log::debug("Started ...editSessionAnswer : ");
            $response = null;
        try{

            $jsonInput = $this->request->getData();
            
            $sessionService = new SessionService;

            $response = $sessionService->editSessionAnswer($jsonInput);

        }catch(\Exception $e){
            $response = [
                '_ReturnCode'=>'999',
                '_ReturnMessage'=>'Fatal Error',
                '_Exception'=>$e
            ];

        }finally{
            $this->setResponse($this->response->withStatus(200));
            $this->set([
                'response' => $response,
                '_serialize' => 'response',
            ]);
            $this->RequestHandler->renderAs($this, 'json');
        }  
    }

    public function deleteSessionAnswer()
    {

        Log::debug("Started ...deleteSessionAnswer : ");
            $response = null;
        try{

            $jsonInput = $this->request->getData();
            
            $sessionService = new SessionService;

            $response = $sessionService->deleteSessionAnswer($jsonInput);

        }catch(\Exception $e){
            $response = [
                '_ReturnCode'=>'999',
                '_ReturnMessage'=>'Fatal Error',
                '_Exception'=>$e
            ];

        }finally{
            $this->setResponse($this->response->withStatus(200));
            $this->set([
                'response' => $response,
                '_serialize' => 'response',
            ]);
            $this->RequestHandler->renderAs($this, 'json');
        }  
    }

    public function assignCoordinators()
    {

        Log::debug("Started ...assignCoordinators : ");
            $response = null;
        try{

            $jsonInput = $this->request->getData();
            
            $sessionService = new SessionService;

            $response = $sessionService->assignCoordinator($jsonInput);

        }catch(\Exception $e){
            $response = [
                '_ReturnCode'=>'999',
                '_ReturnMessage'=>'Fatal Error',
                '_Exception'=>$e
            ];

        }finally{
            $this->setResponse($this->response->withStatus(200));
            $this->set([
                'response' => $response,
                '_serialize' => 'response',
            ]);
            $this->RequestHandler->renderAs($this, 'json');
        }  
    }

    public function conduct()
    {

        Log::debug("Started ...conduct : ");
            $response = null;
        try{

            $jsonInput = $this->request->getData();
            
            $sessionService = new SessionService;

            $response = $sessionService->conduct($jsonInput);

        }catch(\Exception $e){
            $response = [
                '_ReturnCode'=>'999',
                '_ReturnMessage'=>'Fatal Error',
                '_Exception'=>$e
            ];

        }finally{
            $this->setResponse($this->response->withStatus(200));
            $this->set([
                'response' => $response,
                '_serialize' => 'response',
            ]);
            $this->RequestHandler->renderAs($this, 'json');
        }  
    }

    public function create()
    {

        Log::debug("Started ...create : ");
            $response = null;
        try{

            $jsonInput = $this->request->getData();
            
            $sessionService = new SessionService;

            $response = $sessionService->create($jsonInput);

        }catch(\Exception $e){
            $response = [
                '_ReturnCode'=>'999',
                '_ReturnMessage'=>'Fatal Error',
                '_Exception'=>$e
            ];

        }finally{
            $this->setResponse($this->response->withStatus(200));
            $this->set([
                'response' => $response,
                '_serialize' => 'response',
            ]);
            $this->RequestHandler->renderAs($this, 'json');
        }  
    }

    public function getMySession()
    {

        Log::debug("Started ... session allSessionsV1 : ");
            $response = null;
        try{

            $jsonInput = $this->request->getData();
            
            $sessionService = new SessionService;

            $response = $sessionService->getMySession($jsonInput);

        }catch(\Exception $e){
            $response = [
                '_ReturnCode'=>'999',
                '_ReturnMessage'=>'Fatal Error',
                '_Exception'=>$e
            ];

        }finally{
            $this->setResponse($this->response->withStatus(200));
            $this->set([
                'response' => $response,
                '_serialize' => 'response',
            ]);
            $this->RequestHandler->renderAs($this, 'json');
        }  
    }
}