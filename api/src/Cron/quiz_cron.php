<?php

namespace App\Cron;

use App\Utils\Codes;
use Cake\Core\Exception\Exception;
use Cake\Datasource\ConnectionManager;
use App\DaoLayer\PostsDao;
use App\DaoLayer\QuizzesDao;
use App\Utils\DateUtils;
use Cake\Log\Log;
use Cake\Core\Configure;
require dirname(__DIR__) . ‘/vendor/autoload.php’;
use App\Controller\AppController;

if (!defined(‘DS’)) { 

    define(‘DS’, DIRECTORY_SEPARATOR);

} 

require_once(dirname(dirname(__FILE__)).DS.’config’.DS.’bootstrap.php’);

require_once(dirname(dirname(__FILE__)).DS.’src’.DS.’Controller’.DS.’AppController.php’);


$qd = new QuizzesDao;

$qd->sendNotify();
$qd->calcEnd();







?>