<?php 
    namespace App\Utils;

    class Codes {

        public $ISSUER = "LUNEBLAZE_ISSUER@2020";
        public $encryption = "AES-128-CTR";

        public $expiryts = 86400 * 30 * 12 * 5; //5 years


        public $IV = '1234567891011121'; 


        public $RC_ERROR="0";
        public $RC_SUCCESS="1";
        public $RC_DATA_VALIDATION_ERROR="100";
        


        public $RM_SUCCESS="Success";
        public $RM_DATA_VALIDATION_ERROR="Data Validation Error.";
        public $RM_ALL_FIELDS_TO_BE_PRESENT="You must provide all the fields.";
        
        

        public $MAX_FILE_SIZE= 5120;
        public $MAX_RESULTS=10;

        public $MAX_LIMIT=10000000;

        public $MAX_NUMBER_OF_OTPS_SENT_IN_1_HOUR=10;


        public $STARTING_OFFSET=0;

        public $OTP_VALID_TIME=301;

        public $EMAIL_NOTIFICATIONS=1;
        public $EMAIL_FRIEND_NOTIFICATIONS=1;
        public $EMAIL_WALL_POSTS = 1;
        public $EMAIL_PROFILE_VISITS = 1;
        public $EMAIL_MENTIONS = 1;
        public $EMAIL_POST_SHARES = 1;
        public $EMAIL_POST_COMMENTS = 1;
        public $EMAIL_POST_LIKES = 1;

        public $ACTIVATION_ENABLED = 0;
        public $CHAT_ENABLED = 1;



        //System Variables
        public $UPLOADES_PREFIX = "luneblaze";
        public $SYSTEM_URL = "http://luneblaze.com/new/Luneblaze-API";
        public $ROOT_URL = "http://luneblaze.com/new";
        

        public $SYSTEM_TITLE="Strotam-The Source";

        public $ACTIVATION_TYPE = "email";

        public $S3_ENABLED=false;
        public $S3_REGION=null;
        public $S3_BUCKET=null;

        public $MAX_PHOTO_SIZE = 5120;
        public $MAX_VIDEO_SIZE = 5120;
        public $UPLOADS_DIRECTORY="content/uploads";
        public $FILE_ENABLED = 1;
        public $FILE_EXTENSIONS = 'txt, zip, php,pdf,ppt,xlxs,xls,doc';

        public $SYSTEM_THEME="default";

        public $SMS_CODE="ngS71wnwltu";

        public $TO_BE_FOLLOWED=["1","4"];

        public $CAMPUS_DRIVE_FEE = "10.00";
        
        public $DISCOUNT_PERCENTAGE = "20.00";

        public $OFF_CAMPUS_FEE = "500.00";



        //rhythm change
        public $intern_credits = 50;

        public $job_credits = 100;
        public $RC_CONST = 8;

        public $QUIZ_MULTI = 5;

        //points system
        public $INITIAL_PTS = 5000;
        public $ARTICLES_K = 4000;
        public $ARTICLES_C = 500;
        public $POLLS_K = 4000;
        public $POLLS_C = 500;
        public $COMMENT_K = 70;

        public $BONUS_1 = 100;//participants ranking bonus
        public $BONUS_2 = 50;
        public $BONUS_3 = 10;
        public $PARTICIPANT_C1 = 100;//quiz participants
        public $PARTICIPANT_C2 = 100;
        public $CREATOR_C0 = 1200; //CREATOR POINTS
        public $CREATOR_C1 = 1200;
        public $CREATOR_C2 = 1200;
        public $CREATOR_C3 = 1200;

        public $DEBATES_K = 1000;//debates points constants
        public $DEBATES_C1 = 1000;
        public $DEBATES_C2 = 1000;

        public $QUICKBITS_k = 200;//debates points constants
        public $QUICKBITS_c = 100;
        //public $DEBATES_C2 = 1000;

        public $DEDUCT_DEBATE = 100;
        public $DEDUCT_ARTICLE = 100;
        public $DEDUCT_QB = 100;
        public $DEDUCT_POLL = 100;
        public $DEDUCT_QUIZ = 100;

        public $DATE_THRESHOLD = 25;//in days
        public $MF_MULTI = 30;//in percentage
        public $INTEREST_THRESHOLD = 0;
        public $MAX_RESULTS_NF = 50; //newsfeed

    }

?>