<?php 
    namespace App\Utils;

use DateInterval;
use DateTime;

class NotificationUtils {


    public function sendOTPUsingEmail($otp,$emailId,$firstName,$lastName)
    {
        $codes = new Codes;
        $otp = ''.$otp.'';
        $header  = "MIME-Version: 1.0\r\n";
        $header .= "Mailer: ".$codes->SYSTEM_TITLE."\r\n";
        $header .= "Content-Type: text/html; charset=\"utf-8\"\r\n";
        $header .= "Content-Transfer-Encoding: 7bit\r\n";

        $subject = "Just one more step to get started on" . " " . $codes->SYSTEM_TITLE;
        $body = "Hi" . " " . ucwords($firstName).' '.ucwords($lastName) . ",<br/><br/>";
        $body .= "\r\n\r\n" . "Welcome to" . " " . $codes->SYSTEM_TITLE. "<br/><br/>";
		$body .= "\r\n\r\n" . "To verify your email, Please enter the OTP:<strong>" .$otp. "</strong><br/><br/>";
        $body .= "\r\n\r" . $codes->SYSTEM_TITLE . " " . "Team";

        if(!mail($emailId, $subject, $body, $header)) {
            return false;
        }
        return true;
    }

    public function siteMail($collegename,$personname,$email,$subject1,$message)
    {
        $codes = new Codes;
        $admemailId = "admin@luneblaze.com";
        //$otp = ''.$otp.'';
        $header  = "MIME-Version: 1.0\r\n";
        $header .= "Mailer: ".$codes->SYSTEM_TITLE."\r\n";
        $header .= "Content-Type: text/html; charset=\"utf-8\"\r\n";
        $header .= "Content-Transfer-Encoding: 7bit\r\n";

        $subject = "New form submission";
        $body = "College-Name: $collegename ."."\n"."User-Name: $personname ."."\n"."Email: $email ."."\n"."Subject: $subject1 ."."\n"."Message: $message ."."\n";
        

        if(!mail($admemailId, $subject, $body, $header)) {
            return ["message"=> "mail to admin failed"];
        }


        //to filler

        $subject = "We have received your submission." . " " . $codes->SYSTEM_TITLE;
        $body = "Hi" . " " . ucwords($personname). "\n";
        $body .= "\r\n\r\n" . "Thank you for reaching out to us !<br/><br/>";
        $body .= "\r\n\r\n" . "you will be contacted shortly .<br/><br/>";

        if(!mail($email, $subject, $body, $header)) {
            return  ["message"=> "mail to admin failed"];
        }
        return ["message" => "Done"];
    }




    public function sendResetKeyUsingEmail($resetKey,$emailId)
    {
        $codes = new Codes;
        $resetKey = ''.$resetKey;
        $header  = "MIME-Version: 1.0\r\n";
        $header .= "Mailer: ".$codes->SYSTEM_TITLE."\r\n";
        $header .= "Content-Type: text/html; charset=\"utf-8\"\r\n";
        $header .= "Content-Transfer-Encoding: 7bit\r\n";

        $subject = "Forget password activation key!";
        $body = "Hi" . " " . $emailId . ",";
        $body .= "\r\n\r\n" . "To complete the reset password process, please copy this token:";
        $body .= "\r\n\r\n" . "Token:" . " " . $resetKey;
        $body .= "\r\n\r\n" . $codes->SYSTEM_TITLE . " \r\n\r\n" . "Team";

        if(!mail($emailId, $subject, $body, $header)) {
            return false;
        }
        return true;
    }


    /**
     * 
     */
        public function sendOTPUsingSMS($otp,$mobileNumber)
        {
            $codes = new Codes;

            //$sms = "<#> Your OTP for login is ".$otp." ".$codes->SMS_CODE; 
            $sms = "<#> Your OTP for account verification is ".$otp." ".$codes->SMS_CODE; 
			$sms = urlencode($sms); 
            $url = "https://www.prpsms.co.in/API/SendMsg.aspx?uname=20180428&pass=EL99rtrG&send=LUNBLZ&dest=".$mobileNumber."&msg=".$sms."&priority=1&schtm=2018-05-23%2021:45";
			$xml = file_get_contents($url);
			if(empty($xml)){
				$xml = file_get_contents($url);
            }
            
        }

    /**
     * 
     */
    public function sendResetKeyUsingSMS($resetKey,$mobileNumber)
    {
        $codes = new Codes;

        $sms = "<#>  To complete the reset password process, please copy this token: ".$resetKey." ".$codes->SMS_CODE; 
        $sms = urlencode($sms); 
        $url = "https://www.prpsms.co.in/API/SendMsg.aspx?uname=20180428&pass=EL99rtrG&send=LUNBLZ&dest=".$mobileNumber."&msg=".$sms."&priority=1&schtm=2018-05-23%2021:45";
        $xml = file_get_contents($url);
        if(empty($xml)){
            $xml = file_get_contents($url);
        }
        
    }
    }
?>