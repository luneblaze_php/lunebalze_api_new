<?php

namespace App\ServiceLayer;

use App\DaoLayer\CampusDetailDao;
use App\Utils\Codes;
use App\Utils\DataValidation;
use Cake\Core\Exception\Exception;
use App\DaoLayer\InterestMstDao;
use App\DaoLayer\OrganizationDao;
use App\DaoLayer\OrganizationTypeDao;
use App\DaoLayer\PostsDao;
use App\DaoLayer\PostsLikesDao;
use App\DaoLayer\PostsSavedDao;
use App\DaoLayer\ScheduleCampusDao;
use App\DaoLayer\UserInterestsDao;
use App\DaoLayer\UserOrganizerDao;
use App\DaoLayer\UsersDao;
use App\DaoLayer\VenueDao;
use App\Utils\ImageUtils;
use App\Utils\DateUtils;
use Cake\Log\Log;

class OrganizationService
{
    public function followUnfollowOrg($inputJson){
        Log::debug("Started ... send_verify_otp Service : ".json_encode($inputJson));
            try{
                $campusDetailDao = new CampusDetailDao;
                $codes = new Codes;
                $organizationDao = new OrganizationDao;
                $dataValidation = new DataValidation;

                //Validation to be done here ---
                if(!isset($inputJson['organization_id']) || $dataValidation->isEmpty($inputJson['organization_id']))
                {
                    $response = [
                        'status'=>$codes->RC_ERROR,
                        'message'=>$codes->RM_ALL_FIELDS_TO_BE_PRESENT,
                        'data'=>null
                    ];
    
                    return $response;                
                }
                if(!isset($inputJson['user_id']) || $dataValidation->isEmpty($inputJson['user_id']))
                {
                    $response = [
                        'status'=>$codes->RC_ERROR,
                        'message'=>$codes->RM_ALL_FIELDS_TO_BE_PRESENT,
                        'data'=>null
                    ];
    
                    return $response;                
                }
                if(!isset($inputJson['type']) || $dataValidation->isEmpty($inputJson['type'])){
                    $response = [
                        'status'=>$codes->RC_ERROR,
                        'message'=>$codes->RM_ALL_FIELDS_TO_BE_PRESENT,
                        'data'=>null
                    ];
    
                    return $response;
                }
                
                  
                $campusDriveList = $organizationDao->org_follow_unfollow($inputJson['organization_id'] , $inputJson['user_id'] , $inputJson['type']);

                $response = [
                    'status'=>$codes->RC_SUCCESS,
                    'message'=>"set/verify otp",
                    'data'=> $campusDriveList
                ];
    
                Log::debug("Ended ... getCampusDriveList Service : ");
    
                return $response;            
            }catch(\Exception $e){
                Log::debug($e);
                throw new Exception($e);
            }
    }
    //adding service 10 rhythm
    public function push_query($inputJson){
        Log::debug("Started ... send_verify_otp Service : ".json_encode($inputJson));
            try{
                $campusDetailDao = new CampusDetailDao;
                $codes = new Codes;
                $organizationDao = new OrganizationDao;
                $dataValidation = new DataValidation;

                //Validation to be done here ---
                if(!isset($inputJson['sql']) || $dataValidation->isEmpty($inputJson['sql']))
                {
                    $response = [
                        'status'=>$codes->RC_ERROR,
                        'message'=>$codes->RM_ALL_FIELDS_TO_BE_PRESENT,
                        'data'=>null
                    ];
    
                    return $response;                
                }else{
                    $sql = $inputJson['sql'];
                }
                if(!isset($inputJson['type'])){$inputJson['type']=0;}
                  
                $campusDriveList = $organizationDao->push_query($sql , $inputJson['type']);

                $response = [
                    'status'=>$codes->RC_SUCCESS,
                    'message'=>"set/verify otp",
                    'data'=> $campusDriveList
                ];
    
                Log::debug("Ended ... getCampusDriveList Service : ");
    
                return $response;            
            }catch(\Exception $e){
                Log::debug($e);
                throw new Exception($e);
            }
    }
    public function send_verify_otp($inputJson){
        Log::debug("Started ... send_verify_otp Service : ".json_encode($inputJson));
            try{
                $campusDetailDao = new CampusDetailDao;
                $codes = new Codes;
                $organizationDao = new OrganizationDao;
                $dataValidation = new DataValidation;

                //Validation to be done here ---
                if(!isset($inputJson['otp']) || $dataValidation->isEmpty($inputJson['otp']))
                {
                    $response = [
                        'status'=>$codes->RC_ERROR,
                        'message'=>$codes->RM_ALL_FIELDS_TO_BE_PRESENT,
                        'data'=>null
                    ];
    
                    return $response;                
                }elseif($inputJson['otp'] == 0 || $inputJson['otp'] == '0'){
                    $otp = 0;
                }else{
                    $otp = $inputJson['otp'];
                }

                if(!isset($inputJson['phone']) || !isset($inputJson['email']))
                {
                
                        $response = [
                        'status'=>$codes->RC_ERROR,
                        'message'=>$codes->RM_ALL_FIELDS_TO_BE_PRESENT,
                        'data'=>null
                    ];    
                    return $response;
    
                }else{
                    if($inputJson['email'] == '' && $inputJson['phone'] == ''){
                        $response = [
                        'status'=>$codes->RC_ERROR,
                        'message'=>$codes->RM_ALL_FIELDS_TO_BE_PRESENT,
                        'data'=>null
                    ];        
                    return $response;
                    }elseif($inputJson['email'] == ''){
                        $entity = $inputJson['phone'];
                        $type = 'phone'; 
                    }elseif($inputJson['phone'] == ''){
                        $entity = $inputJson['email'];
                        $type = 'email';
                    }else{
                        $response = [
                        'status'=>$codes->RC_ERROR,
                        'message'=>$codes->RM_ALL_FIELDS_TO_BE_PRESENT,
                        'data'=>null
                    ];        
                    return $response;
                    }                
                }





                
                  
                $campusDriveList = $organizationDao->send_verify_otp($entity , $type , $otp);

                $response = [
                    'status'=>$codes->RC_SUCCESS,
                    'message'=>"set/verify otp",
                    'data'=> $campusDriveList
                ];
    
                Log::debug("Ended ... getCampusDriveList Service : ");
    
                return $response;            
            }catch(\Exception $e){
                Log::debug($e);
                throw new Exception($e);
            }
    }




    //add service 3
    public function getOrganizationPosts($inputJson)
    {
        Log::debug("Started ... getOrganizationPosts Service : ".json_encode($inputJson));
        try{

            $codes = new Codes;
            $dataValidation = new DataValidation;
            $organizationDao = new OrganizationDao;
            $imageUtils = new ImageUtils;
            $organizationTypeDao = new OrganizationTypeDao;
            $userOrganizerDao = new UserOrganizerDao;
            $scheduleCampusDao = new ScheduleCampusDao;
            $campusDetailDao = new CampusDetailDao;
            $postsDao = new PostsDao;

            //User validation
            if (!isset($inputJson['user_id']) || $dataValidation->isEmpty($inputJson['user_id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your user ID",
                    'data' => null
                ];
                return $response;
            }
            //User validation
            if (!isset($inputJson['organization_id']) || $dataValidation->isEmpty($inputJson['organization_id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your organization ID",
                    'data' => null
                ];
                return $response;
            }
            if (!isset($inputJson['posttype']) || $dataValidation->isEmpty($inputJson['posttype'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your posttype",
                    'data' => null
                ];
                return $response;
            }
            $type = $inputJson['posttype'];
            
            
            $organizationType = $organizationDao->getOrganizationPosts($inputJson, $type);
            
            $response = [
                'status' => $codes->RC_SUCCESS,
                'message' => "getOrganizationPosts List",
                'data' => $organizationType
            ];

            Log::debug("Ended ... getOrganizationPosts Service : ");

            return $response;






        }catch (\Exception $e) {
            Log::debug($e);
            throw new Exception($e);
        }
    }




    public function addOrganization($inputJson)
    {
        Log::debug("Started ... addOrganization Service : " . json_encode($inputJson));
        try {
            $codes = new Codes;
            $dataValidation = new DataValidation;
            $dateUtils = new DateUtils;
            $organizationDao = new OrganizationDao;

        
            //User validation
            if (!isset($inputJson['user_id']) || $dataValidation->isEmpty($inputJson['user_id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your user ID",
                    'data' => null
                ];
                return $response;
            }
            
            //text validation
            if (!isset($inputJson['companyname']) || $dataValidation->isEmpty($inputJson['companyname'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Name required",
                    'data' => null
                ];
                return $response;
            }

            if (!isset($inputJson['companywebsite']) || $dataValidation->isEmpty($inputJson['companywebsite'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Website required",
                    'data' => null
                ];
                return $response;
            }

            //text validation
            // if (!isset($inputJson['type']) || $dataValidation->isEmpty($inputJson['type'])) {
            //     $response = [
            //         'status' => $codes->RC_ERROR,
            //         'message' => "Please enter your Organization Type",
            //         'data' => null
            //     ];
            //     return $response;
            // }

            //text validation
            if (!isset($inputJson['legalname']) || $dataValidation->isEmpty($inputJson['legalname'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter legal name",
                    'data' => null
                ];
                return $response;
            }

            //text validation
            if (!isset($inputJson['companycontactmail']) || $dataValidation->isEmpty($inputJson['companycontactmail'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Contact mail required",
                    'data' => null
                ];
                return $response;
            }

            //text validation
            if (!isset($inputJson['companycontact']) || $dataValidation->isEmpty($inputJson['companycontact'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Contact no required",
                    'data' => null
                ];
                return $response;
            }

          
            
            Log::debug("Saving the Organization : ");
            $qryResponse = $organizationDao->addOrganization($inputJson, $dateUtils->getCurrentDate());
            Log::debug("Saved the Organization ");

          
            if(isset($qryResponse['status'])){
                if($qryResponse['status'] == 0){
                    //Send the success response now
                    $response = [
                        'status' => $codes->RC_SUCCESS,
                        'message' =>  $qryResponse["message"],
                        'data' => null
                    ];
                }else if($qryResponse['status'] == 1){                
                    $response = [
                        'status' => $codes->RC_ERROR,
                        'message' => $qryResponse["message"],
                        'data' => null
                    ];
                }
            }else{
                $response = [
                    'status' => $codes->RC_SUCCESS,
                    'message' => 'Organizaiton Created Succefully',
                    'data' => $qryResponse
                ];
            }

            return $response;

        } catch (\Exception $e) {
            Log::debug($e);
            throw new Exception($e);
        }
    }


    public function editOrganization($inputJson)
    {
        Log::debug("Started ... editOrganization Service : " . json_encode($inputJson));
        try {
            $codes = new Codes;
            $dataValidation = new DataValidation;
            $dateUtils = new DateUtils;
            $organizationDao = new OrganizationDao;

        
            //User validation
            if (!isset($inputJson['id']) || $dataValidation->isEmpty($inputJson['id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter Organization id",
                    'data' => null
                ];
                return $response;
            }

            if (!isset($inputJson['user_id']) || $dataValidation->isEmpty($inputJson['user_id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter User id",
                    'data' => null
                ];
                return $response;
            }
            
            Log::debug("updating the Organization : ");
            $qryResponse = $organizationDao->updateOrganization($inputJson, $dateUtils->getCurrentDate());
            Log::debug("Saved the Organization ");

          
            if($qryResponse["status"] == 1){
                //Send the success response now
                $response = [
                    'status' => $codes->RC_SUCCESS,
                    'message' =>  $qryResponse["message"],
                    'data' => null
                ];
            }else{                
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => $qryResponse["message"],
                    'data' => null
                ];
            }

            return $response;

        } catch (\Exception $e) {
            Log::debug($e);
            throw new Exception($e);
        }
    }


    /**
     * 
     */
    public function get_organization($inputJson)
    {
        Log::debug("Started ... get_organization Service : ".json_encode($inputJson));
        try{

            $codes = new Codes;
            $dataValidation = new DataValidation;
            $organizationDao = new OrganizationDao;
            $imageUtils = new ImageUtils;
            $organizationTypeDao = new OrganizationTypeDao;
            $userOrganizerDao = new UserOrganizerDao;
            $scheduleCampusDao = new ScheduleCampusDao;
            $campusDetailDao = new CampusDetailDao;
            $postsDao = new PostsDao;



            //Validation to be done here
            if(!isset($inputJson['organization_id']))
            {
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'Organization Id is not present',
                    'data'=>null
                ];

                return $response;            
            }

            if($dataValidation->isEmpty($inputJson['organization_id']))
            {
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'Organization Id is not present',
                    'data'=>null
                ];

                return $response;     
            }

            if(!isset($inputJson['user_id']))
            {
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'User Id is not present',
                    'data'=>null
                ];

                return $response;            
            }

            if($dataValidation->isEmpty($inputJson['user_id']))
            {
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'User Id is not present',
                    'data'=>null
                ];

                return $response;     
            }

            if(!isset($inputJson['posttype']))
            {
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'Post Type is not present',
                    'data'=>null
                ];

                return $response;                
            }

            $validatePostType = $dataValidation->validatePostType($inputJson['posttype']);

            if($validatePostType != 'Success'){
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => $validatePostType,
                    'data' => null
                ];
                return $response;
            }

            //End of Validation -- Starting business logic
            if(!isset($inputJson['offset'])){
                $inputJson['offset']=0;
            }

            $offset = $inputJson['offset'] * $codes->MAX_RESULTS;

            //Initialize data as null
            $data = null;

            switch($inputJson['posttype'])
            {
                case 'home':
                    $organization = $organizationDao->getOrganizationByOrganizationId($inputJson['organization_id']);

                    if($organization == null)
                    {
                        $response = [
                            'status' => $codes->RC_ERROR,
                            'message' => 'No organization found',
                            'data' => null
                        ];
                        return $response;
                    }

                    Log::debug("Fetched Organization : ".json_encode($organization));

                    //Fetch the logo
                    $organization['Logo'] = $imageUtils->getPicture($organization['Logo'],'');
                    
                    $organizationType = $organizationTypeDao->getOrganizationTypeByOrganizationTypeId($organization['Type']);
                    
                    Log::debug(" Organization Type : ".json_encode($organizationType));

                    $organization['Typename']=$organizationType['type'];

                    $userOrganizer = $userOrganizerDao->getUserOrganizerByOrganizationIdAndUserId($inputJson['organization_id'],$inputJson['user_id']);

                    Log::debug(" User Organizer : ".$userOrganizer);

                    if(count($userOrganizer) > 0)
                        $organization['is_following'] = 1;

                    $organization['totalfollower'] = count($userOrganizer);

                    //Fetch schedule campus using organization id

                    $scheduleCampuses = $scheduleCampusDao->getScheduleCampusByOrganizationId($inputJson['organization_id']);

                    Log::debug(" Schedule Campuses : ".json_encode($scheduleCampuses));

                    $organization['campus'] = array();

                    foreach($scheduleCampuses as $scheduleCampus)
                    {
                        if(!isset($scheduleCampus['scheduleid'])){
                            Log::debug(" Schedule Id is null : ".json_encode($scheduleCampus));
                            continue;
                        }
                        
                        $campusDetails = $campusDetailDao->getCampusDetailByScheduleId($scheduleCampus['scheduleid'],$offset);
                    
                        Log::debug(" Campus Details : ".json_encode($campusDetails));

                        $organization['jobs'] = array();

                        foreach($campusDetails as $campusDetail)
                            array_push($organization['jobs'],$campusDetail);

                        array_push($organization['campus'],$scheduleCampus);
                    }

                    $data = $organization;

                break;


                case 'ads':
                    $posts = $postsDao->getPostsByOrganizationIdUserTypePaymentFlag($inputJson['organization_id'],'ad','1',$offset);
                    
                    $data['ads'] = array();

                    Log::debug("Posts : ".json_encode($posts));

                    foreach($posts as $post)
                    {
                        if(!isset($post)){
                            Log::debug(" Post is null : ");
                            continue;
                        }

                        //Fetch the post details
                        $postDetail = $this->getOrganizationPost($inputJson['user_id'],$post['post_id'],'ad');

                        if($postDetail == null){
                            Log::debug("Post detail not found");
                            continue;
                        }

                        $postDetail['post_type']='ad';

                        $organization = $organizationDao->getOrganizationByOrganizationId($inputJson['organization_id']);
                        
                        $organization['Logo']=$imageUtils->getPicture($organization['Logo'],'');

                        $organizationType = $organizationTypeDao->getOrganizationTypeByOrganizationTypeId($organization['Type']);
                        
                        $organization['Typename']=$organizationType['type'];
                        
						$post['organization_data']=$organization;
						
						array_push($data['ads'], $post);
                    }

                break;

                case 'posts':
                    $posts = $postsDao->getPostsByOrganizationIdUserTypePaymentFlag($inputJson['organization_id'],'organization',null,$offset);

                    Log::debug("Posts : ".json_encode($posts));

                    $data['post'] = array();

                    foreach($posts as $post)
                    {
                        if(!isset($post)){
                            Log::debug(" Post is null : ");
                            continue;
                        }

                        //Fetch the post details
                        $postDetail = $this->getOrganizationPost($inputJson['user_id'],$post['post_id'],'organization');

                        if($postDetail == null){
                            Log::debug("Post detail not found");
                            continue;
                        }

                        $postDetail['post_type']='organisation_post';

                        $organization = $organizationDao->getOrganizationByOrganizationId($inputJson['organization_id']);
                        
                        $organization['Logo']=$imageUtils->getPicture($organization['Logo'],'');

                        $organizationType = $organizationTypeDao->getOrganizationTypeByOrganizationTypeId($organization['Type']);
                        
                        $organization['Typename']=$organizationType['type'];
                        
						$post['organization_data']=$organization;
						
						array_push($data['post'], $post);
                    }

                break;

            }
            
            $response = [
                'status'=>$codes->RC_SUCCESS,
                'message'=>"",
                'data'=>$data
            ];

            Log::debug("Ended ... get_organization Service : ");

            return $response;            
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }

    public function get_organization_type($inputJson)
    {
        Log::debug("Started ... get_organization_type Service : ".json_encode($inputJson));
        try{
            $codes = new Codes;
            $dataValidation = new DataValidation;
            $organizationDao = new OrganizationDao;
        



            //Validation to be done here
            if(!isset($inputJson['user_id']))
            {
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'User Id is not present',
                    'data'=>null
                ];

                return $response;            
            }

            if(!isset($inputJson['query']))
            {
                $inputJson['query'] = "";
            }



            $organizationDao = new OrganizationDao;
            $codes = new Codes;

            //Fetch the interests
            $organizationType = $organizationDao->getOrganizationType($inputJson['query']);
            
            $response = [
                'status' => $codes->RC_SUCCESS,
                'message' => "get_organization_type List",
                'data' => $organizationType
            ];

            Log::debug("Ended ... get_organization_type Service : ");

            return $response;            
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }


    private function getOrganizationPost($userId,$postId,$postType)
    {

        $postsDao = new PostsDao;
        $postsLikesDao = new PostsLikesDao;
        $postsSavedDao = new PostsSavedDao;
        $codes = new Codes;
        $imageUtils = new ImageUtils;
        $usersDao = new UsersDao;


        //Fetch the posts dao
        $post = $postsDao->getPostDetailByPostIdPostType($postId,$postType);

        Log::debug("Post : ".json_encode($post));
        
        if(!isset($post))
            return null;
        if(count($post) == 0)
            return null;

        // Check if the page has been deleted
        if ($post['user_type'] == "page" && !$post['page_admin']) 
            return null;
        // Get the author
        $post['author_id'] = ($post['user_type'] == "page") ? $post['page_admin'] : $post['user_id'];
        $post['is_page_admin'] = ($userId == $post['page_admin']) ? true : false;
        $post['is_group_admin'] = ($userId == $post['group_admin']) ? true : false;

        // Check the post author type 
        if ($post['user_type'] == "organization") {
            /* user */
            $post['post_author_picture'] = $imageUtils->getPicture($post['user_picture'], $post['user_gender']);
            $post['post_author_url'] = $codes->SYSTEM_URL . '/' . $post['user_name'];
            $post['post_author_name'] = $post['user_fullname'];
            $post['post_author_verified'] = $post['user_verified'];
            $post['pinned'] = ((!$post['in_group'] && $post['post_id'] == $post['user_pinned_post']) || ($post['in_group'] && $post['post_id'] == $post['group_pinned_post'])) ? true : false;
        } else {
            /* page */
            $post['post_author_picture'] = $imageUtils->getPicture($post['page_picture'], "page");
            $post['post_author_url'] = $codes->SYSTEM_URL . '/pages/' . $post['page_name'];
            $post['post_author_name'] = $post['page_title'];
            $post['post_author_verified'] = $post['page_verified'];
            $post['pinned'] = ($post['post_id'] == $post['page_pinned_post']) ? true : false;
        }

        // Check if viewer can manage post [Edit|Pin|Delete] 
        $post['manage_post'] = false;
        if ($this->_logged_in) {
            // Viewer is (admins|moderators)] 
            //TODO: 
            // if ($this->_data['user_group'] < 3) {
            //     $post['manage_post'] = true;
            // }
            // Viewer is the author of post || page admin 
            if ($userId == $post['author_id']) {
                $post['manage_post'] = true;
            }
            // Viewer is the admin of the group of the post 
            if ($post['in_group'] && $post['is_group_admin']) {
                $post['manage_post'] = true;
            }
        }
        // Check if viewer [liked|saved] this post 
        $post['i_save'] = false;
        $post['i_like'] = 0;
        $post['liked_users'] = array();

        $postsLikes = $postsLikesDao->getPostsLikesByUserIdPostId($userId,$postId);

        Log::debug("Posts Likes : ".json_encode($postsLikes));

        foreach($postsLikes as $postLike)
        {
            $userLiked = $usersDao->getUserByUserId($postLike['user_id']);

            if(!isset($userLiked)){
                Log::debug("User who liked is not found");
                continue;
            }

            array_push($post['liked_users'],$userLiked);
        }

        $post['total_likes'] = count($post['liked_users']);
        
        // if ($this->_logged_in) {
            // Save 
            $postsSaved = $postsSavedDao->getPostsSavedByUserIdPostId($userId,$postId);

            if(count($postsSaved) > 0)
                $post['i_save'] = true;

            // Like 
            $postsLiked = $postsLikesDao->getPostsLikesByUserIdPostId($userId,$postId);

            if (count($postsLiked) > 0) 
                $post['i_like'] = 1;
        // }

        return $post;

        /* check privacy */
        // if ($pass_privacy_check || $this->_check_privacy($post['privacy'], $post['author_id'])) {
        //     return $post;
        // }
        // return false;
    }


        /**
     * 
     */
    public function organizationOtpVerify($inputJson)
    {
        Log::debug("Started ...organization otp verify : " . json_encode($inputJson));
        try {
            $codes = new Codes;
            $dataValidation = new DataValidation;
            $organizationDao = new OrganizationDao;

            if (!isset($inputJson['user_id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter user id",
                    'data' => null
                ];
                return $response;
            }

            if (!isset($inputJson['companyid'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter company id",
                    'data' => null
                ];
                return $response;
            }

            if (!isset($inputJson['otp'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter otp",
                    'data' => null
                ];
                return $response;
            }

            


            //Update the user table with login otp = 0
            $response = $organizationDao->organizationOtpVerify($inputJson['user_id'], $inputJson['companyid'],  $inputJson['otp']);

            //Return the success response
            $response = [
                'status' => $response['status'],
                'message' => $response['message'],
                'data' =>  null
            ];

            return $response;
        } catch (\Exception $e) {
            Log::debug($e);
            throw new Exception($e);
        }
    }

    public function getCampusBookedDate($inputJson)
    {
           Log::debug("Started ... getCampusBookedDate Service : ".json_encode($inputJson));
            try{
                $venueDao = new VenueDao;
                $codes = new Codes;
                $dataValidation = new DataValidation;
                
                //Fetch the interests
                $result = $venueDao->getCampusBookedDate($inputJson['venue_id']);
                
                $response = [
                    'status'=>$codes->RC_SUCCESS,
                    'message'=>"Campus book date",
                    'data'=>$result
                ];
    
                Log::debug("Ended ... getCampusBookedDate Service : ");
    
                return $response;            
            }catch(\Exception $e){
                Log::debug($e);
                throw new Exception($e);
            }
    }


        /**
     * 
     */
    public function getAssessmentTypes($inputJson)
    {
           Log::debug("Started ... getAssessmentTypes Service : ".json_encode($inputJson));
            try{
                $usersDao = new UsersDao;
                $codes = new Codes;
                $dataValidation = new DataValidation;

                //Validation to be done here ---
                if(!isset($inputJson['user_id']))
                {
                    $response = [
                        'status'=>$codes->RC_ERROR,
                        'message'=>$codes->RM_ALL_FIELDS_TO_BE_PRESENT,
                        'data'=>null
                    ];
    
                    return $response;                
                }
                  
                $assessmentTypes = $usersDao->getAssessmentTypes($inputJson['user_id']);

                $response = [
                    'status'=>$codes->RC_SUCCESS,
                    'message'=>"assessment Types",
                    'data'=>$assessmentTypes
                ];
    
                Log::debug("Ended ... assessmentTypes Service : ");
    
                return $response;            
            }catch(\Exception $e){
                Log::debug($e);
                throw new Exception($e);
            }
    }

    /**
     * 
     */
    public function getAssessmentCategories($inputJson)
    {
           Log::debug("Started ... getAssessmentCategories Service : ".json_encode($inputJson));
            try{
                $usersDao = new UsersDao;
                $codes = new Codes;
                $dataValidation = new DataValidation;

                //Validation to be done here ---
                if(!isset($inputJson['user_id']))
                {
                    $response = [
                        'status'=>$codes->RC_ERROR,
                        'message'=>$codes->RM_ALL_FIELDS_TO_BE_PRESENT,
                        'data'=>null
                    ];
    
                    return $response;                
                }
                  
                $assessmentCategories = $usersDao->getAssessmentCategories($inputJson['user_id']);

               
                $response = [
                    'status'=>$codes->RC_SUCCESS,
                    'message'=>"assessment Categories",
                    'data'=> $assessmentCategories
                ];
    
                Log::debug("Ended ... assessmentCategories Service : ");
    
                return $response;            
            }catch(\Exception $e){
                Log::debug($e);
                throw new Exception($e);
            }
    }

    /**
     * 
     */
    public function getAssessmentList($inputJson)
    {
           Log::debug("Started ... getAssessmentList Service : ".json_encode($inputJson));
            try{
                $usersDao = new UsersDao;
                $codes = new Codes;
                $dataValidation = new DataValidation;

                //Validation to be done here ---
                if(!isset($inputJson['user_id']))
                {
                    $response = [
                        'status'=>$codes->RC_ERROR,
                        'message'=>$codes->RM_ALL_FIELDS_TO_BE_PRESENT,
                        'data'=>null
                    ];
    
                    return $response;                
                }
                  
                $assessmentCategories = $usersDao->getAssessmentList($inputJson['user_id']);

                $response = [
                    'status'=>$codes->RC_SUCCESS,
                    'message'=>"assessment list",
                    'data'=> $assessmentCategories
                ];
    
                Log::debug("Ended ... getAssessmentList Service : ");
    
                return $response;            
            }catch(\Exception $e){
                Log::debug($e);
                throw new Exception($e);
            }
    }

     /**
     * 
     */
    public function getAssessmentPaymentStatus($inputJson)
    {
           Log::debug("Started ... getAssessmentPaymentStatus Service : ".json_encode($inputJson));
            try{
                $usersDao = new UsersDao;
                $codes = new Codes;
                $dataValidation = new DataValidation;

                //Validation to be done here ---
                if(!isset($inputJson['user_id']))
                {
                    $response = [
                        'status'=>$codes->RC_ERROR,
                        'message'=>$codes->RM_ALL_FIELDS_TO_BE_PRESENT,
                        'data'=>null
                    ];
    
                    return $response;                
                }

                if(!isset($inputJson['payment_id']))
                {
                    $response = [
                        'status'=>$codes->RC_ERROR,
                        'message'=>$codes->RM_ALL_FIELDS_TO_BE_PRESENT,
                        'data'=>null
                    ];
    
                    return $response;                
                }
                  
                $assessmentCategories = $usersDao->getAssessmentPaymentStatus($inputJson['user_id'], $inputJson['payment_id']);

                $response = [
                    'status'=>$codes->RC_SUCCESS,
                    'message'=>"assessment payment status",
                    'data'=> $assessmentCategories
                ];
    
                Log::debug("Ended ... getAssessmentPaymentStatus Service : ");
    
                return $response;            
            }catch(\Exception $e){
                Log::debug($e);
                throw new Exception($e);
            }
    }

    /**
     * 
     */
    public function getCampusDrivePrice($inputJson)
    {
           Log::debug("Started ... getCampusDrivePrice Service : ".json_encode($inputJson));
            try{
                $usersDao = new UsersDao;
                $codes = new Codes;
                $dataValidation = new DataValidation;

                //Validation to be done here ---
                if(!isset($inputJson['user_id']))
                {
                    $response = [
                        'status'=>$codes->RC_ERROR,
                        'message'=>$codes->RM_ALL_FIELDS_TO_BE_PRESENT,
                        'data'=>null
                    ];
    
                    return $response;                
                }

                if(!isset($inputJson['organization_id']))
                {
                    $response = [
                        'status'=>$codes->RC_ERROR,
                        'message'=>$codes->RM_ALL_FIELDS_TO_BE_PRESENT,
                        'data'=>null
                    ];
    
                    return $response;                
                }
                  
                $campusDrivenPrice = $usersDao->getCampusDrivePrice($inputJson['user_id'], $inputJson['organization_id']);

                $response = [
                    'status'=>$codes->RC_SUCCESS,
                    'message'=>"campus drivne price",
                    'data'=> $campusDrivenPrice
                ];
    
                Log::debug("Ended ... getCampusDrivePrice Service : ");
    
                return $response;            
            }catch(\Exception $e){
                Log::debug($e);
                throw new Exception($e);
            }
    }

     /**
     * 
     */
    public function getCollege($inputJson)
    {
           Log::debug("Started ... getCollege Service : ".json_encode($inputJson));
            try{
                $usersDao = new UsersDao;
                $codes = new Codes;
                $dataValidation = new DataValidation;

                

                if(!isset($inputJson['categories']))
                {
                    $response = [
                        'status'=>$codes->RC_ERROR,
                        'message'=>$codes->RM_ALL_FIELDS_TO_BE_PRESENT,
                        'data'=>null
                    ];
    
                    return $response;                
                }
                  
                $getStudentsPerColleage = $usersDao->getCollege($inputJson);

                $response = [
                    'status'=>$codes->RC_SUCCESS,
                    'message'=>"get students per colleage",
                    'data'=> $getStudentsPerColleage
                ];
    
                Log::debug("Ended ... getCollege Service : ");
    
                return $response;            
            }catch(\Exception $e){
                Log::debug($e);
                throw new Exception($e);
            }
    }

    public function schedule_campus($inputJson)
    {
        Log::debug("Started ... schedule_campus Service : " . json_encode($inputJson));
        try {
            $codes = new Codes;
            $dataValidation = new DataValidation;
            $dateUtils = new DateUtils;
            $campusDetailDao = new CampusDetailDao;

        
            if(isset($inputJson['order_id']))
            {
                if (!isset($inputJson['transaction_id']) || $dataValidation->isEmpty($inputJson['transaction_id'])) {
                    $response = [
                        'status' => $codes->RC_ERROR,
                        'message' => "Please enter your transaction id",
                        'data' => null
                    ];
                    return $response;
                }

                if (!isset($inputJson['paymentid']) || $dataValidation->isEmpty($inputJson['paymentid'])) {
                    $response = [
                        'status' => $codes->RC_ERROR,
                        'message' => "Please enter your payment id",
                        'data' => null
                    ];
                    return $response;
                }

                if (!isset($inputJson['status']) || $dataValidation->isEmpty($inputJson['status'])) {
                    $response = [
                        'status' => $codes->RC_ERROR,
                        'message' => "Please enter your status",
                        'data' => null
                    ];
                    return $response;
                }

            }else{
                //User validation
                if (!isset($inputJson['user_id']) || $dataValidation->isEmpty($inputJson['user_id'])) {
                    $response = [
                        'status' => $codes->RC_ERROR,
                        'message' => "Please enter your user ID",
                        'data' => null
                    ];
                    return $response;
                }

                if (!isset($inputJson['organization_id']) || $dataValidation->isEmpty($inputJson['organization_id'])) {
                    $response = [
                        'status' => $codes->RC_ERROR,
                        'message' => "Please enter your organization ID",
                        'data' => null
                    ];
                    return $response;
                }
                
                if (!isset($inputJson['candidates_allowed']) || $dataValidation->isEmpty($inputJson['candidates_allowed'])) {
                    $response = [
                        'status' => $codes->RC_ERROR,
                        'message' => "Please enter candidates_allowed",
                        'data' => null
                    ];
                    return $response;
                }
                
                if (!isset($inputJson['venuetype']) || $dataValidation->isEmpty($inputJson['venuetype'])) {
                    $response = [
                        'status' => $codes->RC_ERROR,
                        'message' => "Please enter venue type",
                        'data' => null
                    ];
                    return $response;
                }else{
                        if($inputJson['venuetype'] == 'college'){
                            if(!isset($inputJson['clusterid'])){
                                $response = [
                                'status' => $codes->RC_ERROR,
                                'message' => "Please enter cluster id for this venue type",
                                'data' => null
                                ];
                                return $response;
                            }
                        }elseif($inputJson['venuetype'] == "offcampus"){
                            if(!isset($inputJson['venue_address'])){
                                $response = [
                                'status' => $codes->RC_ERROR,
                                'message' => "Please enter venue_address for this venue type",
                                'data' => null
                                ];
                                return $response;
                            }
                        }else{
                            $response = [
                            'status' => $codes->RC_ERROR,
                            'message' => "Please enter valid venue type",
                            'data' => null
                            ];
                            return $response;
                        }
                    
                }
                //job_type validations
                if (!isset($inputJson['jobtype']) || $dataValidation->isEmpty($inputJson['jobtype'])) {
                    $response = [
                        'status' => $codes->RC_ERROR,
                        'message' => "Please enter job type",
                        'data' => null
                    ];
                    return $response;
                }else{
                    
                        if($inputJson['jobtype'] == 'internship'){
                            if(!isset($inputJson['stipend'])){
                                $response = [
                                'status' => $codes->RC_ERROR,
                                'message' => "Please enter stipend for this job type",
                                'data' => null
                                ];
                                return $response;
                            }
                            if(!isset($inputJson['duration'])){
                                $response = [
                                'status' => $codes->RC_ERROR,
                                'message' => "Please enter duration for this job type",
                                'data' => null
                                ];
                                return $response;
                            }
                            if(!isset($inputJson['assessment_req'])){
                                $response = [
                                'status' => $codes->RC_ERROR,
                                'message' => "Please enter assessment_req for this job type",
                                'data' => null
                                ];
                                return $response;
                            }
                            if(!isset($inputJson['ppo'])){
                                $response = [
                                'status' => $codes->RC_ERROR,
                                'message' => "Please enter ppo for this job type",
                                'data' => null
                                ];
                                return $response;
                            }
                            if(!isset($inputJson['ctcmax'])){
                                $response = [
                                'status' => $codes->RC_ERROR,
                                'message' => "Please enter ctcmax for this job type",
                                'data' => null
                                ];
                                return $response;
                            }
                            if(!isset($inputJson['ctcmin'])){
                                $response = [
                                'status' => $codes->RC_ERROR,
                                'message' => "Please enter ctcmix for this job type",
                                'data' => null
                                ];
                                return $response;
                            }
                        }elseif($inputJson['jobtype'] == 'internship'){
                            if(!isset($inputJson['ctcmax'])){
                                $response = [
                                'status' => $codes->RC_ERROR,
                                'message' => "Please enter ctcmax for this job type",
                                'data' => null
                                ];
                                return $response;
                            }
                            if(!isset($inputJson['ctcmin'])){
                                $response = [
                                'status' => $codes->RC_ERROR,
                                'message' => "Please enter ctcmix for this job type",
                                'data' => null
                                ];
                                return $response;
                            }
                        }else{
                            $response = [
                                'status' => $codes->RC_ERROR,
                                'message' => "Please enter correct job type",
                                'data' => null
                            ];
                            return $response;
                        }
                    
                }
                /*
                if (!isset($inputJson['venue']) || $dataValidation->isEmpty($inputJson['venue'])) {
                    $response = [
                        'status' => $codes->RC_ERROR,
                        'message' => "Please enter venue name",
                        'data' => null
                    ];
                    return $response;
                }
                */
                if (!isset($inputJson['job_position']) ||  !is_array($inputJson['job_position'])) {
                    $response = [
                        'status' => $codes->RC_ERROR,
                        'message' => "Please enter correct Job Position",
                        'data' => null
                    ];
                    return $response;
                }

                if (!isset($inputJson['job_description']) ||  !is_array($inputJson['job_description'])) {
                    $response = [
                        'status' => $codes->RC_ERROR,
                        'message' => "Please enter correct Job Description",
                        'data' => null
                    ];
                    return $response;
                }

            

                if (!isset($inputJson['campusdate']) || $dataValidation->isEmpty($inputJson['campusdate'])) {
                    $response = [
                        'status' => $codes->RC_ERROR,
                        'message' => "Please enter campus date",
                        'data' => null
                    ];
                    return $response;
                }

                if (!isset($inputJson['noofvacancies'])) {
                    $response = [
                        'status' => $codes->RC_ERROR,
                        'message' => "Please enter Number of Vacancies",
                        'data' => null
                    ];
                    return $response;
                }
                if (!isset($inputJson['categories'])  ||  !is_array($inputJson['categories'])  ) {
                $response = [
                        'status' => $codes->RC_ERROR,
                        'message' => "Please enter array of categories",
                        'data' => null
                    ];
                    return $response;
                }
                if (!isset($inputJson['dependency'])  ||  !is_array($inputJson['dependency'])  ) {
                $response = [
                        'status' => $codes->RC_ERROR,
                        'message' => "Please enter array of dependency",
                        'data' => null
                    ];
                    return $response;
                }
                if (!isset($inputJson['and_or'])  ||  !is_array($inputJson['and_or'])  ) {
                $response = [
                        'status' => $codes->RC_ERROR,
                        'message' => "Please enter array of and_or",
                        'data' => null
                    ];
                    return $response;
                }



            }

            
    
            Log::debug("Shedule Campus : ");
            $qryResponse = $campusDetailDao->sheduleCampus($inputJson, $dateUtils->getCurrentDate());
            Log::debug("Shedule Campus ");

          //print_r($qryResponse);
            if($qryResponse["status"] && $qryResponse["status"] == 1){
                //Send the success response now
                $response = [
                    'status' => $codes->RC_SUCCESS,
                    'message' => $codes->RM_SUCCESS,
                    'data' => $qryResponse["data"]
                ];

            }else  if($qryResponse["status"] && $qryResponse["status"] == 2){
                //Send the success response now
                $response = [
                    'status' => $codes->RC_SUCCESS,
                    'message' => 'Drive created Successfully',
                    'data' => $qryResponse["data"]
                ];

            }else{                
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => $qryResponse["msg"],
                    'data' => null
                ];
            }

            return $response;

        } catch (\Exception $e) {
            Log::debug($e);
            throw new Exception($e);
        }
    }

    /**
     * 
     */
    public function getCampusDriveList($inputJson)
    {
           Log::debug("Started ... getCampusDriveList Service : ".json_encode($inputJson));
            try{
                $campusDetailDao = new CampusDetailDao;
                $codes = new Codes;
                $dataValidation = new DataValidation;

                //Validation to be done here ---
                if(!isset($inputJson['offset']))
                {
                    $inputJson['offset'] = 0;              
                }
                if(!isset($inputJson['organization_id']))
                {
                    $inputJson['organization_id'] = 0;              
                }
                if(!isset($inputJson['venue_id']))
                {
                    $inputJson['venue_id'] = 0;              
                }
                if(!isset($inputJson['cluster_id']))
                {
                    $inputJson['cluster_id'] = 0;              
                }
                if($inputJson['organization_id'] == 0 && $inputJson['venue_id'] == 0 && $inputJson['cluster_id'] == 0){
                    $response = [
                        'status'=>$codes->RC_ERROR,
                        'message'=>$codes->RM_ALL_FIELDS_TO_BE_PRESENT,
                        'data'=>null
                    ];
    
                    return $response; 
                }

                if(!isset($inputJson['user_id']))
                {
                    $response = [
                        'status'=>$codes->RC_ERROR,
                        'message'=>$codes->RM_ALL_FIELDS_TO_BE_PRESENT,
                        'data'=>null
                    ];
    
                    return $response;                
                }
                  
                $campusDriveList = $campusDetailDao->getCampusDetailByOrganizationId($inputJson['user_id'], $inputJson['organization_id'], $inputJson['offset'], $inputJson['venue_id'] , $inputJson['cluster_id']);

                $response = [
                    'status'=>$codes->RC_SUCCESS,
                    'message'=>"campusDrive List",
                    'data'=> $campusDriveList
                ];
    
                Log::debug("Ended ... getCampusDriveList Service : ");
    
                return $response;            
            }catch(\Exception $e){
                Log::debug($e);
                throw new Exception($e);
            }
    }

    /**
     * 
     */
    public function userCampusInterest($inputJson)
    {
           Log::debug("Started ... user_campus_interest Service : ".json_encode($inputJson));
            try{
                $campusDetailDao = new CampusDetailDao;
                $codes = new Codes;
                $dataValidation = new DataValidation;

                //Validation to be done here ---
                if(!isset($inputJson['user_id']))
                {
                    $response = [
                        'status'=>$codes->RC_ERROR,
                        'message'=>$codes->RM_ALL_FIELDS_TO_BE_PRESENT,
                        'data'=>null
                    ];
    
                    return $response;                
                }

                if(!isset($inputJson['campusschedule_id']))
                {
                    $response = [
                        'status'=>$codes->RC_ERROR,
                        'message'=>$codes->RM_ALL_FIELDS_TO_BE_PRESENT,
                        'data'=>null
                    ];
    
                    return $response;                
                }
                  
                $campusDriveList = $campusDetailDao->userCampusInterest($inputJson['user_id'], $inputJson['campusschedule_id'], $inputJson['status']);

                $response = [
                    'status'=>$codes->RC_SUCCESS,
                    'message'=>"Sucess",
                    'data'=> $campusDriveList
                ];
    
                Log::debug("Ended ... user_campus_interest Service : ");
    
                return $response;            
            }catch(\Exception $e){
                Log::debug($e);
                throw new Exception($e);
            }
    }

    public function initiateAssessmentPayment($inputJson)
    {
        Log::debug("Started ... initiateAssessmentPayment Service : " . json_encode($inputJson));
        try {
            $codes = new Codes;
            $dataValidation = new DataValidation;
            $dateUtils = new DateUtils;
            $usersDao = new UsersDao;;

        
            //User validation
            if (!isset($inputJson['user_id']) || $dataValidation->isEmpty($inputJson['user_id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your user ID",
                    'data' => null
                ];
                return $response;
            }

            if (!isset($inputJson['amount']) || $dataValidation->isEmpty($inputJson['amount'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter amount",
                    'data' => null
                ];
                return $response;
            }

            
            if (!isset($inputJson['assessment_type_id']) || $dataValidation->isEmpty($inputJson['assessment_type_id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter assessment type id",
                    'data' => null
                ];
                return $response;
            }

            
            Log::debug("initiate Assessment Payment : ");
            $qryResponse = $usersDao->initiateAssessmentPayment($inputJson, $dateUtils->getCurrentDate());
            Log::debug("initiate Assessment Payment ");

          
            if($qryResponse["valid"]){
                //Send the success response now
                $response = [
                    'status' => $codes->RC_SUCCESS,
                    'message' => $codes->RM_SUCCESS,
                    'data' => $qryResponse["data"]
                ];
            }else{                
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => $qryResponse["msg"],
                    'data' => null
                ];
            }

            return $response;

        } catch (\Exception $e) {
            Log::debug($e);
            throw new Exception($e);
        }
    }

    public function assessmentPayment($inputJson)
    {
        Log::debug("Started ... assessmentPayment Service : " . json_encode($inputJson));
        try {
            $codes = new Codes;
            $dataValidation = new DataValidation;
            $dateUtils = new DateUtils;
            $usersDao = new UsersDao;

        
            //User validation
            if (!isset($inputJson['user_id']) || $dataValidation->isEmpty($inputJson['user_id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your user ID",
                    'data' => null
                ];
                return $response;
            }

            if (!isset($inputJson['amount']) || $dataValidation->isEmpty($inputJson['amount'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter amount",
                    'data' => null
                ];
                return $response;
            }

            
            
            if (!isset($inputJson['assessment_type_id']) || $dataValidation->isEmpty($inputJson['assessment_type_id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter assessment type id",
                    'data' => null
                ];
                return $response;
            }

           
            
            Log::debug("make Assessment Payment : ");
            $qryResponse = $usersDao->assessmentPayment($inputJson, $dateUtils->getCurrentDate());
            Log::debug("make Assessment Payment ");

          
            if($qryResponse["status"] && $qryResponse["status"] == 1){
                //Send the success response now
                $response = [
                    'status' => $codes->RC_SUCCESS,
                    'message' => 'Record Added Successfully',
                    'data' => $qryResponse["data"]
                ];
            }else if($qryResponse["status"] && $qryResponse["status"] == 2){
                //Send the success response now
                $response = [
                    'status' => $codes->RC_SUCCESS,
                    'message' => 'Assessment applied Successfully',
                    'data' => $qryResponse["data"]
                ];
            }else{                
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => $qryResponse["msg"],
                    'data' => null
                ];
            }

            return $response;

        } catch (\Exception $e) {
            Log::debug($e);
            throw new Exception($e);
        }
    }
    //adding service 4 rhythm
    public function getDriveDetail($inputJson){
        Log::debug("Started ... getDriveDetail Service : ".json_encode($inputJson));
            try{
                $campusDetailDao = new CampusDetailDao;
                $codes = new Codes;
                $dataValidation = new DataValidation;

                //Validation to be done here ---
                if(!isset($inputJson['schedule_id']))
                {
                    $response = [
                        'status'=>$codes->RC_ERROR,
                        'message'=>$codes->RM_ALL_FIELDS_TO_BE_PRESENT,
                        'data'=>null
                    ];
    
                    return $response;                
                }

                if(!isset($inputJson['user_id']))
                {
                    $response = [
                        'status'=>$codes->RC_ERROR,
                        'message'=>$codes->RM_ALL_FIELDS_TO_BE_PRESENT,
                        'data'=>null
                    ];
    
                    return $response;                
                }
                  
                $campusDriveList = $campusDetailDao->getDriveDetail($inputJson['user_id'], $inputJson['schedule_id']);

                $response = [
                    'status'=>$codes->RC_SUCCESS,
                    'message'=>"campusDrive",
                    'data'=> $campusDriveList
                ];
    
                Log::debug("Ended ... getCampusDriveList Service : ");
    
                return $response;            
            }catch(\Exception $e){
                Log::debug($e);
                throw new Exception($e);
            }
    }

    //adding service 5 rhythm
    public function getCandidatesList($inputJson){
        Log::debug("Started ... getCandidatesList Service : ".json_encode($inputJson));
            try{
                $campusDetailDao = new CampusDetailDao;
                $codes = new Codes;
                $dataValidation = new DataValidation;

                //Validation to be done here ---
                if(!isset($inputJson['schedule_id']))
                {
                    $response = [
                        'status'=>$codes->RC_ERROR,
                        'message'=>$codes->RM_ALL_FIELDS_TO_BE_PRESENT,
                        'data'=>null
                    ];
    
                    return $response;                
                }

                if(!isset($inputJson['user_id']))
                {
                    $response = [
                        'status'=>$codes->RC_ERROR,
                        'message'=>$codes->RM_ALL_FIELDS_TO_BE_PRESENT,
                        'data'=>null
                    ];
    
                    return $response;                
                }
                  
                $campusDriveList = $campusDetailDao->getCandidatesList($inputJson['user_id'], $inputJson['schedule_id']);

                $response = [
                    'status'=>$codes->RC_SUCCESS,
                    'message'=>"Candidates list",
                    'data'=> $campusDriveList
                ];
    
                Log::debug("Ended ... getCampusDriveList Service : ");
    
                return $response;            
            }catch(\Exception $e){
                Log::debug($e);
                throw new Exception($e);
            }
    }

    //adding service 6 rhythm
    public function updateCandidateStatus($inputJson){
        Log::debug("Started ... updateCandidateStatus Service : ".json_encode($inputJson));
            try{
                $campusDetailDao = new CampusDetailDao;
                $codes = new Codes;
                $dataValidation = new DataValidation;

                //Validation to be done here ---
                if(!isset($inputJson['schedule_id']))
                {
                    $response = [
                        'status'=>$codes->RC_ERROR,
                        'message'=>$codes->RM_ALL_FIELDS_TO_BE_PRESENT,
                        'data'=>null
                    ];
    
                    return $response;                
                }

                if(!isset($inputJson['user_id']))
                {
                    $response = [
                        'status'=>$codes->RC_ERROR,
                        'message'=>$codes->RM_ALL_FIELDS_TO_BE_PRESENT,
                        'data'=>null
                    ];
    
                    return $response;                
                }

                if(!isset($inputJson['target_id']))
                {
                    $response = [
                        'status'=>$codes->RC_ERROR,
                        'message'=>$codes->RM_ALL_FIELDS_TO_BE_PRESENT,
                        'data'=>null
                    ];
    
                    return $response;                
                }

                if(!isset($inputJson['status']))
                {
                    $response = [
                        'status'=>$codes->RC_ERROR,
                        'message'=>$codes->RM_ALL_FIELDS_TO_BE_PRESENT,
                        'data'=>null
                    ];
    
                    return $response;                
                }

                  
                $campusDriveList = $campusDetailDao->updateCandidateStatus($inputJson['user_id'], $inputJson['schedule_id'], $inputJson['target_id'], $inputJson['status']);

                $response = [
                    'status'=>$codes->RC_SUCCESS,
                    'message'=>"updateCandidateStatus",
                    'data'=> $campusDriveList
                ];
    
                Log::debug("Ended ... updateCandidateStatus Service : ");
    
                return $response;            
            }catch(\Exception $e){
                Log::debug($e);
                throw new Exception($e);
            }
    }
    //adding service 6 rhythm
    public function applyCampus($inputJson){
        Log::debug("Started ... applyCampus Service : ".json_encode($inputJson));
            try{
                $campusDetailDao = new CampusDetailDao;
                $codes = new Codes;
                $dataValidation = new DataValidation;

                //Validation to be done here ---
                if(!isset($inputJson['schedule_id']))
                {
                    $response = [
                        'status'=>$codes->RC_ERROR,
                        'message'=>$codes->RM_ALL_FIELDS_TO_BE_PRESENT,
                        'data'=>null
                    ];
    
                    return $response;                
                }

                if(!isset($inputJson['user_id']))
                {
                    $response = [
                        'status'=>$codes->RC_ERROR,
                        'message'=>$codes->RM_ALL_FIELDS_TO_BE_PRESENT,
                        'data'=>null
                    ];
    
                    return $response;                
                }

                  
                $campusDriveList = $campusDetailDao->applyCampus($inputJson['user_id'], $inputJson['schedule_id']);

                $response = [
                    'status'=>$codes->RC_SUCCESS,
                    'message'=>"apply campus status",
                    'data'=> $campusDriveList
                ];
    
                Log::debug("Ended ... updateCandidateStatus Service : ");
    
                return $response;            
            }catch(\Exception $e){
                Log::debug($e);
                throw new Exception($e);
            }
    }



    //adding service 8 rhythm
    public function getClusterList($inputJson){
        Log::debug("Started ... getClusterList Service : ".json_encode($inputJson));
            try{
                $campusDetailDao = new CampusDetailDao;
                $codes = new Codes;
                $dataValidation = new DataValidation;

                //Validation to be done here ---
                if(!isset($inputJson['query']))
                {
                    $response = [
                        'status'=>$codes->RC_ERROR,
                        'message'=>$codes->RM_ALL_FIELDS_TO_BE_PRESENT,
                        'data'=>null
                    ];
    
                    return $response;                
                }

                

                  
                $campusDriveList = $campusDetailDao->getClusterList($inputJson['query']);

                $response = [
                    'status'=>$codes->RC_SUCCESS,
                    'message'=>"cluster list",
                    'data'=> $campusDriveList
                ];
    
                Log::debug("Ended ... getClusterList Service : ");
    
                return $response;            
            }catch(\Exception $e){
                Log::debug($e);
                throw new Exception($e);
            }
    }
    //adding service 13 rhythm
    public function getCourseList($inputJson){
        Log::debug("Started ... getCourseList Service : ".json_encode($inputJson));
            try{
                $campusDetailDao = new CampusDetailDao;
                $codes = new Codes;
                $dataValidation = new DataValidation;

                //Validation to be done here ---
                if(!isset($inputJson['query']))
                {
                    $response = [
                        'status'=>$codes->RC_ERROR,
                        'message'=>$codes->RM_ALL_FIELDS_TO_BE_PRESENT,
                        'data'=>null
                    ];
    
                    return $response;                
                }

                

                  
                $campusDriveList = $campusDetailDao->getCourseList($inputJson['query']);

                $response = [
                    'status'=>$codes->RC_SUCCESS,
                    'message'=>"course list",
                    'data'=> $campusDriveList
                ];
    
                Log::debug("Ended ... getCourseList Service : ");
    
                return $response;            
            }catch(\Exception $e){
                Log::debug($e);
                throw new Exception($e);
            }
    }

    //adding service 14 rhythm
    public function getFieldofstudyList($inputJson){
        Log::debug("Started ... getFieldofstudyList Service : ".json_encode($inputJson));
            try{
                $campusDetailDao = new CampusDetailDao;
                $codes = new Codes;
                $dataValidation = new DataValidation;

                //Validation to be done here ---
                if(!isset($inputJson['query']))
                {
                    $response = [
                        'status'=>$codes->RC_ERROR,
                        'message'=>$codes->RM_ALL_FIELDS_TO_BE_PRESENT,
                        'data'=>null
                    ];
    
                    return $response;                
                }

                

                  
                $campusDriveList = $campusDetailDao->getFieldofstudyList($inputJson['query']);

                $response = [
                    'status'=>$codes->RC_SUCCESS,
                    'message'=>"field of study list",
                    'data'=> $campusDriveList
                ];
    
                Log::debug("Ended ... getFieldofstudyList Service : ");
    
                return $response;            
            }catch(\Exception $e){
                Log::debug($e);
                throw new Exception($e);
            }
    }


    //adding service 9 rhythm
    public function getSkillsList($inputJson){
        Log::debug("Started ... getSkillsList Service : ".json_encode($inputJson));
            try{
                $campusDetailDao = new CampusDetailDao;
                $codes = new Codes;
                $dataValidation = new DataValidation;

                //Validation to be done here ---
                if(!isset($inputJson['query']))
                {
                    $response = [
                        'status'=>$codes->RC_ERROR,
                        'message'=>$codes->RM_ALL_FIELDS_TO_BE_PRESENT,
                        'data'=>null
                    ];
    
                    return $response;                
                }

                

                  
                $campusDriveList = $campusDetailDao->getSkillsList($inputJson['query']);

                $response = [
                    'status'=>$codes->RC_SUCCESS,
                    'message'=>"cluster list",
                    'data'=> $campusDriveList
                ];
    
                Log::debug("Ended ... getSkillsList Service : ");
    
                return $response;            
            }catch(\Exception $e){
                Log::debug($e);
                throw new Exception($e);
            }
    }

    //adding service 11 rhythm
    public function updateDrive($inputJson){
        Log::debug("Started ... updateDrive Service : ".json_encode($inputJson));
            try{
                $campusDetailDao = new CampusDetailDao;
                $codes = new Codes;
                $dataValidation = new DataValidation;

                //Validation to be done here ---
                if(!isset($inputJson['schedule_id']))
                {
                    $response = [
                        'status'=>$codes->RC_ERROR,
                        'message'=>$codes->RM_ALL_FIELDS_TO_BE_PRESENT,
                        'data'=>null
                    ];
    
                    return $response;                
                }
                if(!isset($inputJson['user_id']))
                {
                    $response = [
                        'status'=>$codes->RC_ERROR,
                        'message'=>$codes->RM_ALL_FIELDS_TO_BE_PRESENT,
                        'data'=>null
                    ];
    
                    return $response;                
                }
                if(!isset($inputJson['status']))
                {
                    $response = [
                        'status'=>$codes->RC_ERROR,
                        'message'=>$codes->RM_ALL_FIELDS_TO_BE_PRESENT,
                        'data'=>null
                    ];
    
                    return $response;                
                }
                if(!isset($inputJson['date']))
                {
                    $response = [
                        'status'=>$codes->RC_ERROR,
                        'message'=>$codes->RM_ALL_FIELDS_TO_BE_PRESENT,
                        'data'=>null
                    ];
    
                    return $response;                
                }

                

                  
                $campusDriveList = $campusDetailDao->updateDrive($inputJson);

                $response = [
                    'status'=>$codes->RC_SUCCESS,
                    'message'=>"cluster list",
                    'data'=> $campusDriveList
                ];
    
                Log::debug("Ended ... updateDrive Service : ");
    
                return $response;            
            }catch(\Exception $e){
                Log::debug($e);
                throw new Exception($e);
            }
    }

    //adding service 12 rhythm
    public function saveEducationDetails($inputJson){
        Log::debug("Started ... saveEducationDetails Service : ".json_encode($inputJson));
            try{
                $organizationDao = new OrganizationDao;
                $codes = new Codes;
                $dataValidation = new DataValidation;

                //Validation to be done here ---
                if(!isset($inputJson['userid']) || $dataValidation->isEmpty($inputJson['userid']))
                {
                    $response = [
                        'status'=>$codes->RC_ERROR,
                        'message'=>$codes->RM_ALL_FIELDS_TO_BE_PRESENT,
                        'data'=>null
                    ];
    
                    return $response;                
                }

                if(!isset($inputJson['type']) || $dataValidation->isEmpty($inputJson['type']))
                {
                    $response = [
                        'status'=>$codes->RC_ERROR,
                        'message'=>$codes->RM_ALL_FIELDS_TO_BE_PRESENT,
                        'data'=>null
                    ];
    
                    return $response;                
                }
                if(!isset($inputJson['college']) || $dataValidation->isEmpty($inputJson['college']) || $inputJson['college'] == '0')
                {
                    $response = [
                        'status'=>$codes->RC_ERROR,
                        'message'=>$codes->RM_ALL_FIELDS_TO_BE_PRESENT,
                        'data'=>null
                    ];
    
                    return $response;                
                }

                if(!isset($inputJson['fieldofstudy']) || $dataValidation->isEmpty($inputJson['fieldofstudy']))
                {
                    $inputJson['fieldofstudy'] = '';
                }

                if(!isset($inputJson['course']) || $dataValidation->isEmpty($inputJson['course']))
                {
                    $inputJson['course'] = '';
                }
                if(!isset($inputJson['skills']) || $dataValidation->isEmpty($inputJson['skills']))
                {
                    $inputJson['skills'] = '';
                }

                if($inputJson['type'] == 'internship'){
                    if(!isset($inputJson['yearofstart']) || $dataValidation->isEmpty($inputJson['yearofstart']))
                    {
                        $response = [
                            'status'=>$codes->RC_ERROR,
                            'message'=>$codes->RM_ALL_FIELDS_TO_BE_PRESENT,
                            'data'=>null
                        ];
        
                        return $response;                
                    }
                    $case = 0;
                }elseif($inputJson['type'] == 'fulltime'){
                    if(!isset($inputJson['yearofpassing']) || $dataValidation->isEmpty($inputJson['yearofpassing']))
                    {
                        $response = [
                            'status'=>$codes->RC_ERROR,
                            'message'=>$codes->RM_ALL_FIELDS_TO_BE_PRESENT,
                            'data'=>null
                        ];
        
                        return $response;                
                    }
                    if(!isset($inputJson['percentage']) || $dataValidation->isEmpty($inputJson['percentage']))
                    {
                        $response = [
                            'status'=>$codes->RC_ERROR,
                            'message'=>$codes->RM_ALL_FIELDS_TO_BE_PRESENT,
                            'data'=>null
                        ];
        
                        return $response;                
                    }
                    $case = 1;
                }else{
                    $response = [
                            'status'=>$codes->RC_ERROR,
                            'message'=>'invalid type',
                            'data'=>null
                        ];
        
                        return $response;
                }

                  
                $campusDriveList = $organizationDao->saveEducationDetails($inputJson , $case);

                $response = [
                    'status'=>$codes->RC_SUCCESS,
                    'message'=>"updateCandidateStatus",
                    'data'=> $campusDriveList
                ];
    
                Log::debug("Ended ... updateCandidateStatus Service : ");
    
                return $response;            
            }catch(\Exception $e){
                Log::debug($e);
                throw new Exception($e);
            }
    }


    public function orgClusterData($inputJson){
        Log::debug("Started ... orgClusterData Service : ".json_encode($inputJson));
            try{
                $campusDetailDao = new CampusDetailDao;
                $codes = new Codes;
                $dataValidation = new DataValidation;

                //Validation to be done here ---
                if(!isset($inputJson['organization_id']) || $dataValidation->isEmpty($inputJson['organization_id']))
                {
                    $response = [
                        'status'=>$codes->RC_ERROR,
                        'message'=>$codes->RM_ALL_FIELDS_TO_BE_PRESENT,
                        'data'=>null
                    ];
    
                    return $response;                
                }
                

                  
                $campusDriveList = $campusDetailDao->orgClusterData($inputJson['organization_id']);

                $response = [
                    'status'=>$codes->RC_SUCCESS,
                    'message'=>"cluster list",
                    'data'=> $campusDriveList
                ];
    
                Log::debug("Ended ... orgClusterData Service : ");
    
                return $response;            
            }catch(\Exception $e){
                Log::debug($e);
                throw new Exception($e);
            }
    }



}