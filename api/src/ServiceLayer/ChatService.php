<?php

namespace App\ServiceLayer;

use App\DaoLayer\CampusDetailDao;
use App\Utils\Codes;
use App\Utils\DataValidation;
use Cake\Core\Exception\Exception;
use App\Model\Image;
use App\Model\UserModel;
use App\Model\ConversationGroup;
use App\DaoLayer\InterestMstDao;
use App\DaoLayer\OrganizationDao;
use App\DaoLayer\OrganizationTypeDao;
use App\DaoLayer\PostsDao;
use App\DaoLayer\PostsLikesDao;
use App\DaoLayer\PostsSavedDao;
use App\DaoLayer\ScheduleCampusDao;
use App\DaoLayer\UserInterestsDao;
use App\DaoLayer\UserOrganizerDao;
use App\DaoLayer\UsersDao;
use App\DaoLayer\VenueDao;
use App\Utils\ImageUtils;
use Cake\Log\Log;
use Cake\Datasource\ConnectionManager;

class ChatService
{
    /**
     * 
     */
    public function getUsersStatus($input)
    {
           Log::debug("Started ... getUsersStatus Service : ");
            try{
                $usersDao = new UsersDao;
                $codes = new Codes;
                $dataValidation = new DataValidation;

                if( !isset($input['user_ids']) || !is_array($input['user_ids']) || empty($input['user_ids'])){
                    $response['status'] = 0;
                    $response['message'] = "user_ids is required in array format";
                    $response['data'] = null;
                } else {
                    try {
                        $user = new UserModel;  
                        $user_ids = $input['user_ids'];
            
                        $data = $user->get_user_login_status($user_ids);
                        $response['status'] = 1;
                        $response['message'] = '';
                        $response['data'] = $data;
            
                    } catch (Exception $e) {
                        $response['status'] = 0;
                        $response['message'] = $e->getMessage();
                        $response['data'] = null;
                    }
                }

    
                Log::debug("Ended ... getUsersStatus Service : ");
    
                return $response;            
            }catch(\Exception $e){
                Log::debug($e);
                throw new Exception($e);
            }
    }

    /**
     * 
     */
    public function delete($input)
    {
           Log::debug("Started ... delete chat Service : ".json_encode($input));
            try{
                $codes = new Codes;
                if(!isset($input['user_id']) || empty($input['user_id'])){
                    $response = [
                        'status'=>$codes->RC_ERROR,
                        'message'=>'user id is required',
                        'data'=>null
                    ];
    
                    return $response;
                }

                $user = new UserModel($input['user_id']);
                if($codes->ACTIVATION_ENABLED && !$user->_data['user_activated']) {
                    $response = [
                        'status'=>$codes->RC_ERROR,
                        'message'=>'Before you can interact with other users, you need to confirm your email address',
                        'data'=>null
                    ];
    
                    return $response;
                }

                if(!isset($input['conversation_id']) || !is_numeric($input['conversation_id'])){
                    $response = [
                        'status'=>$codes->RC_ERROR,
                        'message'=>'conversation id is required',
                        'data'=>null
                    ];
    
                    return $response;
                }

                $res = $user->delete_conversation($input['conversation_id']);

                if(!$res){
                    $response = [
                        'status'=>$codes->RC_ERROR,
                        'message'=>'You are not authorized to do this',
                        'data'=>null
                    ];
                }else{

                    $response['status'] = 1;
                    $response['message'] = 'Conversation deleted successfully.';
                    $response['data'] = [];
                }

                return $response; 

            }catch(\Exception $e){
                print_r($e);
                Log::debug($e);
                throw new Exception($e);
            }
    }

    /**
     * 
     */
    public function getMessageNew($input)
    {
        Log::debug("Started ... getMessageNew chat Service : ".json_encode($input));
        try{
            $codes = new Codes;
            $dataValidation = new DataValidation;
            if(!isset($input['user_id']) || empty($input['user_id'])){
				$response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'user id is required',
                    'data'=>null
                ];

                return $response;
			}
			$conversation = false;
			global $system;

			$final_result = array();

			//$group 	= new ConversationGroup();
			$user 	= new UserModel($input['user_id']);
            //return $user;
			$offset = isset($input['offset']) ? $input['offset'] : 0;
            $query=isset($input['query'])?$input['query']:'';
            $last_message_id=isset($input['last_message_id'])?$input['last_message_id']:'';
            $limit=isset($input['limit'])?$input['limit']:10;
            $order=isset($input['order'])?$input['order']:'prev';

			if (!isset($input['cid'])) {

				//if(count($user->_data['conversations']) > 0) {
					//pr($user->_data['conversations']);die;
					$final_result['messages'] = $user->get_conversations1($offset,$query,$last_message_id,$order);
					//return $final_result;
					/* foreach($user->_data['conversations'] as $key => $data ){

						if ( $key >= $offset ) {

							if(!empty($data['image'])){
								$data['image'] = $system['system_uploads'].'/'.$data['image'];
							}

							$final_result['messages'][$key] = $data;

							$group_data = $group->get_group_data_by_conversation_id( $data['conversation_id'] );

							if ( empty($group_data) ) {
								$group_data = new stdClass();
							}

							$final_result['messages'][$key]['group_data'] = $group_data;

						}

					}

					$final_result = array_map('array_values', $final_result); */
				//}

			} else {

				/* check cid is valid */
				if($dataValidation->isEmpty($input['cid']) || !is_numeric($input['cid'])) {
					_apiError(404, "cid is required");
				}

				$recipient_data = $user->get_conversation_recipient_data( $input['cid'] );
				if ( !empty($recipient_data) ) {
					$final_result["user_id"]		= $recipient_data['user_id'];
					$final_result["user_name"]		= $recipient_data['user_name'];
					$final_result["user_fullname"]	= $recipient_data['user_fullname'];
					$final_result["user_gender"]	= $recipient_data['user_gender'];
					$final_result["user_picture"]	= $recipient_data['user_picture'];
                    
                    if($user->_data['user_id']!=$recipient_data['user_id'])
                    {
                        $final_result['blocked']=$user->isblocked($user->_data['user_id'],$recipient_data['user_id']);
                    }
                    else
                    {
                        $final_result['blocked']='';
                    }
				}

				$final_result['messages'] 		= $user->get_conversation_messages($input['cid'], $offset,@$input['last_message_id'],$limit,$order);
				
				$group_data = (array) $user->get_group_data_by_conversation_id( $input['cid'] );
				
				if (empty($group_data)) {
					//$group_data = new stdClass();
				} else {
					$final_result['group_data'] = $group_data;
				}
			}

			if($final_result != null or $final_result != []){
				$response['status'] = 1;
			    $response['message'] = '';
			    $response['data'] = $final_result;
			} else{
				$response['status'] = 0;
				$response['message'] = 'No Result found.';
				$response['data'] = null;
			}
            return $response;

        }catch(\Exception $e){
            print_r($e);
            Log::debug($e);
            throw new Exception($e);
        }
    }

    function aws_s3_upload($path, $file_name) {
        $codes = new Codes;
        require_once(ABSPATH.'includes/libs/AWS/aws-autoloader.php');
        $s3Client = Aws\S3\S3Client::factory(array(
            'version'    => 'latest',
            'region'      => $system['s3_region'],
            'credentials' => array(
                'key'    => $system['s3_key'],
                'secret' => $system['s3_secret'],
            )
        ));
        $Key = 'uploads/'.$file_name;
        $s3Client->putObject([
            'Bucket' => $codes->S3_BUCKET,
            'Key'    => $Key,
            'Body'   => fopen($path, 'r+'),
            'ACL'    => 'public-read',
        ]);
        /* remove local file */
        gc_collect_cycles();
        if($s3Client->doesObjectExist($codes->S3_BUCKET, $Key)) {
            unlink($path);
        }
    }

    /**
     * 
     */
    public function createGroup($input)
    {
        Log::debug("Started ... getMessageNew chat Service : ".json_encode($input));
        try{
            $codes = new Codes;

            if(!isset($input['user_id']) || empty($input['user_id'])){
				$response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'user id is required',
                    'data'=>null
                ];

                return $response;
            }
            
            $user = new UserModel($input['user_id']);
			
			/* if group_name not set */
			if(!isset($input['group_name'])) {
				$response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'group name is required',
                    'data'=>null
                ];

                return $response;				
			};


			/* if recipients not set */
			if(!isset($input['recipients'])) {
				$recipients = array();		
			} else {
				$recipients = $input['recipients'];
			}

            $photo = '';
           // print_r($_FILES);
			if(isset($_FILES['photo']) && !empty($_FILES["photo"]["name"])) {
				$max_allowed_size = $codes->MAX_PHOTO_SIZE * 1024;
				/* check & create uploads dir */
				$folder = 'photos';
				$depth = './../../';
				if(!file_exists($depth.$codes->UPLOADS_DIRECTORY.'/'.$folder)) {
					@mkdir($depth.$codes->UPLOADS_DIRECTORY.'/'.$folder, 0777, true);
				}
				if(!file_exists($depth.$codes->UPLOADS_DIRECTORY.'/'.$folder.'/' . date('Y'))) {
					@mkdir($depth.$codes->UPLOADS_DIRECTORY.'/'.$folder.'/' . date('Y'), 0777, true);
				}
				if(!file_exists($codes->UPLOADS_DIRECTORY.'/'.$folder.'/' . date('Y') . '/' . date('m'))) {
					@mkdir($depth.$codes->UPLOADS_DIRECTORY.'/'.$folder.'/' . date('Y') . '/' . date('m'), 0777, true);
				}
				// valid inputs
				if(!isset($_FILES["photo"]) || $_FILES["photo"]["error"] != UPLOAD_ERR_OK) {
                    $response = [
                        'status'=>$codes->RC_ERROR,
                        'message'=>"Something wrong with upload! Is 'upload_max_filesize' set correctly?",
                        'data'=>null
                    ];
    
                    return $response;	
					//throw new Exception("Something wrong with upload! Is 'upload_max_filesize' set correctly?");
				}

				// check file size
				if($_FILES["photo"]["size"] > $max_allowed_size) {
                    $response = [
                        'status'=>$codes->RC_ERROR,
                        'message'=>"The file size is so big",
                        'data'=>null
                    ];
    
                    return $response;	
					//throw new Exception("The file size is so big");
				}
				
				/* prepare new file name */
				$directory = $folder.'/'. date('Y') . '/' . date('m') . '/';
				$prefix = $codes->UPLOADES_PREFIX.'_'.md5(time()*rand(1, 9999));
				$image = new Image($_FILES["photo"]["tmp_name"]);
				$image_tmp_name = $directory.$prefix.'_tmp'.$image->_img_ext;
				$image_new_name = $directory.$prefix.$image->_img_ext;
				$path_tmp = $depth.$codes->UPLOADS_DIRECTORY.'/'.$image_tmp_name;
				$path_new = $depth.$codes->UPLOADS_DIRECTORY.'/'.$image_new_name;

				/* check if the file uploaded successfully */
				if(!@move_uploaded_file($_FILES['photo']['tmp_name'], $path_tmp)) {
                    $response = [
                        'status'=>$codes->RC_ERROR,
                        'message'=>"Sorry, can not upload the photo",
                        'data'=>null
                    ];
    
                    return $response;	
					//throw new Exception("Sorry, can not upload the photo");
				}
				
				/* save the new image */
				$resize =  true;
				$image->save($path_new, $path_tmp, $resize);
               
				/* delete the tmp image */
				unlink($path_tmp);

				/* upload to amazon s3 */
				if($codes->S3_ENABLED) {
					aws_s3_upload($path_new, $image_new_name);
				}
				
                /* return */
				$photo = $image_new_name;
			}
			
			$conversation = $user->add_conversation_group( $input['user_id'], $input['group_name'], $photo, $recipients);

			if($conversation){
				$response['status'] = 1;
			    $response['message'] = '';
			    $response['data'] = $conversation;
			} else {
				$response['status'] = 0;
			    $response['message'] = '';
			    $response['data'] = $conversation;
            }
            
            return $response;

        }catch(\Exception $e){
            print_r($e);
            Log::debug($e);
            throw new Exception($e);
        }
    }

   
    /**
     * 
     */
    public function editGroup($input)
    {
        Log::debug("Started ... editGroup chat Service : ".json_encode($input));
        try{
            $codes = new Codes;

            if(!isset($input['user_id']) || empty($input['user_id'])){
				$response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'user id is required',
                    'data'=>null
                ];

                return $response;
            }
            
            $user = new UserModel($input['user_id']);
            $member_id = $input["user_id"];


            if(!isset($input['group_id']) || empty($input['group_id'])){
				$response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'group id is required',
                    'data'=>null
                ];

                return $response;
            }

            $group = new ConversationGroup($input['group_id']);
            
            $update = array();
			/* if group_name not set */
			if(isset($input['group_name'])) {
				$update['group_name'] = $input['group_name'];				
			};


			$photo = '';
			if(isset($_FILES['photo']) && !empty($_FILES["photo"]["name"])) {
				$max_allowed_size = $codes->MAX_PHOTO_SIZE * 1024;
				/* check & create uploads dir */
				$folder = 'photos';
				$depth = './../../';
				if(!file_exists($depth.$codes->UPLOADS_DIRECTORY.'/'.$folder)) {
					@mkdir($depth.$codes->UPLOADS_DIRECTORY.'/'.$folder, 0777, true);
				}
				if(!file_exists($depth.$codes->UPLOADS_DIRECTORY.'/'.$folder.'/' . date('Y'))) {
					@mkdir($depth.$codes->UPLOADS_DIRECTORY.'/'.$folder.'/' . date('Y'), 0777, true);
				}
				if(!file_exists($codes->UPLOADS_DIRECTORY.'/'.$folder.'/' . date('Y') . '/' . date('m'))) {
					@mkdir($depth.$codes->UPLOADS_DIRECTORY.'/'.$folder.'/' . date('Y') . '/' . date('m'), 0777, true);
				}
				// valid inputs
				if(!isset($_FILES["photo"]) || $_FILES["photo"]["error"] != UPLOAD_ERR_OK) {
                    $response = [
                        'status'=>$codes->RC_ERROR,
                        'message'=>"Something wrong with upload! Is 'upload_max_filesize' set correctly?",
                        'data'=>null
                    ];
    
                    return $response;	
					//throw new Exception("Something wrong with upload! Is 'upload_max_filesize' set correctly?");
				}

				// check file size
				if($_FILES["photo"]["size"] > $max_allowed_size) {
                    $response = [
                        'status'=>$codes->RC_ERROR,
                        'message'=>"The file size is so big",
                        'data'=>null
                    ];
    
                    return $response;	
					//throw new Exception("The file size is so big");
				}
				
				/* prepare new file name */
				$directory = $folder.'/'. date('Y') . '/' . date('m') . '/';
				$prefix = $codes->UPLOADES_PREFIX.'_'.md5(time()*rand(1, 9999));
				$image = new Image($_FILES["photo"]["tmp_name"]);
				$image_tmp_name = $directory.$prefix.'_tmp'.$image->_img_ext;
				$image_new_name = $directory.$prefix.$image->_img_ext;
				$path_tmp = $depth.$codes->UPLOADS_DIRECTORY.'/'.$image_tmp_name;
				$path_new = $depth.$codes->UPLOADS_DIRECTORY.'/'.$image_new_name;

				/* check if the file uploaded successfully */
				if(!@move_uploaded_file($_FILES['photo']['tmp_name'], $path_tmp)) {
                    $response = [
                        'status'=>$codes->RC_ERROR,
                        'message'=>"Sorry, can not upload the photo",
                        'data'=>null
                    ];
    
                    return $response;	
					//throw new Exception("Sorry, can not upload the photo");
				}
				
				/* save the new image */
				$resize =  true;
				$image->save($path_new, $path_tmp, $resize);
               
				/* delete the tmp image */
				unlink($path_tmp);

				/* upload to amazon s3 */
				if($codes->S3_ENABLED) {
					aws_s3_upload($path_new, $image_new_name);
				}
				
                /* return */
				$photo = $image_new_name;
                $update['photo'] = $photo;
			}
			
			$conversation = $group->update_conversation_group($member_id, $update );

			if($conversation){
				$response['status'] = 1;
			    $response['message'] = '';
			    $response['data'] = $conversation;
			} else {
				$response['status'] = 0;
			    $response['message'] = '';
			    $response['data'] = $conversation;
			}
            
            return $response;

        }catch(\Exception $e){
            print_r($e);
            Log::debug($e);
            throw new Exception($e);
        }
    }

    /**
     * 
     */
    public function getGroup($input)
    {
        Log::debug("Started ... getGroup chat Service : ".json_encode($input));
        try{
            $codes = new Codes;

            if(!isset($input['group_id']) || empty($input['group_id'])){
				$response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'group id is required',
                    'data'=>null
                ];

                return $response;
            }

            $group = new ConversationGroup($input['group_id']);


            $group_data = $group->get_group_data();
		
            if($group_data){
                $response['status'] = 1;
                $response['message'] = '';
                $response['data'] = $group_data;
            }
            
            
            return $response;

        }catch(\Exception $e){
            //print_r($e);
            Log::debug($e);
            throw new Exception($e);
        }
    }

    /**
     * 
     */
    public function leaveGroup($input)
    {
        Log::debug("Started ... leaveGroup chat Service : ".json_encode($input));
        try{
            $codes = new Codes;

            if(!isset($input['group_id']) || empty($input['group_id'])){
				$response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'group id is required',
                    'data'=>null
                ];

                return $response;
            }

            $group = new ConversationGroup($input['group_id']);

            if(!isset($input['user_id']) || empty($input['user_id'])){
				$response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'user id is required',
                    'data'=>null
                ];

                return $response;
            }

            
            $result = $group->leave_group( $input['user_id'] );
		
            if($result){
                $response['status'] = 1;
                $response['message'] = '';
                $response['data'] = $result;
            }
            
            
            return $response;


        }catch(\Exception $e){
            //print_r($e);
            Log::debug($e);
            throw new Exception($e);
        }
    }

    /**
     * 
     */
    public function createGetConversation($input)
    {
        Log::debug("Started ... createGetConversation chat Service : ".json_encode($input));
        try{
            $codes = new Codes;

            if(!isset($input['user_id']) || empty($input['user_id'])){
				$response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'user id is required',
                    'data'=>null
                ];

                return $response;
            }

            if(!isset($input['recipient_id']) || empty($input['recipient_id'])){
				$response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'recipient_id is required',
                    'data'=>null
                ];

                return $response;
            }

            $user = new UserModel($input['user_id']);
            
            $conversation = $user->create_get_conversation($input['recipient_id']);
		
            if($conversation){
                $response['status'] = 1;
                $response['message'] = '';
                $response['data'] = $conversation;
            }
            
            
            return $response;

        }catch(\Exception $e){
            print_r($e);
            Log::debug($e);
            throw new Exception($e);
        }
    }

    /**
     * 
     */
    public function addGroupMembers($input)
    {
        Log::debug("Started ... addGroupMembers chat Service : ".json_encode($input));
        try{
            $codes = new Codes;

            if(!isset($input['group_id']) || empty($input['group_id'])){
				$response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'group id is required',
                    'data'=>null
                ];

                return $response;
            }

            if(!isset($input['user_id']) || empty($input['user_id'])){
				$response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'user id is required',
                    'data'=>null
                ];

                return $response;
            }

            if( !isset($input['recipients']) || !is_array($input['recipients']) || empty($input['recipients'])){
                $response['status'] = 0;
                $response['message'] = "recipients is required in array format";
                $response['data'] = null;
                return $response;
            }

            $group = new ConversationGroup($input['group_id']);
            
            $members = $group->add_group_members($input['recipients'],$input['user_id']);


			if($members){
				$response['status'] = 1;
			    $response['message'] = '';
			    $response['data'] = $members;
			}
			
            
            
            return $response;

        }catch(\Exception $e){
            print_r($e);
            Log::debug($e);
            throw new Exception($e);
        }
    }

    /**
     * 
     */
    public function removeMembers($input)
    {
        Log::debug("Started ... removeMembers chat Service : ".json_encode($input));
        try{
            $codes = new Codes;

            if(!isset($input['group_id']) || empty($input['group_id'])){
				$response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'group id is required',
                    'data'=>null
                ];

                return $response;
            }

            if(!isset($input['user_id']) || empty($input['user_id'])){
				$response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'user id is required',
                    'data'=>null
                ];

                return $response;
            }

            if( !isset($input['members']) || !is_array($input['members']) || empty($input['members'])){
                $response['status'] = 0;
                $response['message'] = "members is required in array format";
                $response['data'] = null;
            }

            $group = new ConversationGroup($input['group_id']);
            
            $result = $group->remove_members( $input['user_id'], $input['members'] );

			if($result){
                //echo $result;exit;
                if($result === 'user_not_exsit'){
                    $response['status'] = 0;
                    $response['message'] = 'given user id is not exsit';
                    $response['data'] = $result;
                }else{
                    $response['status'] = 1;
                    $response['message'] = '';
                    $response['data'] = $result;
                }
			}else{
                $response['status'] = 0;
			    $response['message'] = 'given user id is not owner of group';
			    $response['data'] = $result;
            }
			

            return $response;

        }catch(\Exception $e){
            print_r($e);
            Log::debug($e);
            throw new Exception($e);
        }
    }

     /**
     * 
     */
    public function addAdmin($input)
    {
        Log::debug("Started ... addAdmin chat Service : ".json_encode($input));
        try{
            $codes = new Codes;

            if(!isset($input['group_id']) || empty($input['group_id'])){
				$response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'group id is required',
                    'data'=>null
                ];

                return $response;
            }

            if(!isset($input['user_id']) || empty($input['user_id'])){
				$response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'user id is required',
                    'data'=>null
                ];

                return $response;
            }

            if(!isset($input['member_id']) || empty($input['member_id'])){
				$response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'member id is required',
                    'data'=>null
                ];

                return $response;
            }

            $group = new ConversationGroup($input['group_id']);
            
            $result = $group->add_admin($input['user_id'],  $input['member_id'] );

			if($result){
                if($result === 'user_not_exsit'){
                    $response['status'] = 0;
                    $response['message'] = 'given user id is not exsit';
                    $response['data'] = $result;
                }else{
                    $response['status'] = 1;
                    $response['message'] = '';
                    $response['data'] = $result;
                }
			}else{
                $response['status'] = 0;
			    $response['message'] = 'given user id is not owner of group';
			    $response['data'] = $result;
            }
			
            
            
            return $response;

        }catch(\Exception $e){
            print_r($e);
            Log::debug($e);
            throw new Exception($e);
        }
    }

    /**
     * 
     */
    public function removeAdmin($input)
    {
        Log::debug("Started ... addAdmin chat Service : ".json_encode($input));
        try{
            $codes = new Codes;

            if(!isset($input['group_id']) || empty($input['group_id'])){
				$response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'group id is required',
                    'data'=>null
                ];

                return $response;
            }

            if(!isset($input['user_id']) || empty($input['user_id'])){
				$response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'user id is required',
                    'data'=>null
                ];

                return $response;
            }

            if(!isset($input['member_id']) || empty($input['member_id'])){
				$response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'member id is required',
                    'data'=>null
                ];

                return $response;
            }

            $group = new ConversationGroup($input['group_id']);
            
            $result = $group->remove_admin($input['member_id'] );

			if($result){
				$response['status'] = 1;
			    $response['message'] = '';
			    $response['data'] = $result;
			}else{
                $response['status'] = 0;
			    $response['message'] = 'given user id is not owner of group';
			    $response['data'] = $result;
            }
			
            
            
            return $response;

        }catch(\Exception $e){
            print_r($e);
            Log::debug($e);
            throw new Exception($e);
        }
    }

    /**
     * 
     */
    public function messageSeen($input)
    {
        Log::debug("Started ... addAdmin chat Service : ".json_encode($input));
        try{
            $codes = new Codes;

            if(!isset($input['conversation_id']) || empty($input['conversation_id'])){
				$response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'conversation id is required',
                    'data'=>null
                ];

                return $response;
            }

            if(!isset($input['user_id']) || empty($input['user_id']) || !is_numeric($input['user_id'])){
				$response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'user id is required',
                    'data'=>null
                ];

                return $response;
            }

            $user = new UserModel( $input['user_id'] );

            $conversation = $user->mark_conversation_as_seen($input['conversation_id'], $input['user_id'], $input['message_id'] );
			
			if($conversation){

				$response['status'] = 1;
			    $response['message'] = '';
			    $response['data'] = $conversation;
			}
            
            
            return $response;

        }catch(\Exception $e){
            //print_r($e);
            Log::debug($e);
            throw new Exception($e);
        }
    }

    /**
     * 
     */
    public function createMessage($input)
    {
        Log::debug("Started ... createMessage chat Service : ".json_encode($input));
        try{
            $codes = new Codes;
            if(!isset($input['user_id']) || empty($input['user_id'])){
				$response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'user id is required',
                    'data'=>null
                ];

                return $response;
            }

            $user = new UserModel($input['user_id']);
			
			/* if message not set */
			if(!isset($input['message']) || empty($input['message'])){
				$response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'message is required',
                    'data'=>null
                ];

                return $response;
            }
			
            /* if both (conversation_id & recipients) not set */
            if(!isset($input['conversation_id']) || empty($input['conversation_id'])){
				$response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'conversation id is required',
                    'data'=>null
                ];

                return $response;
            }

            // if(isset($input['conversation_id']) || !is_numeric($input['conversation_id'])){
			// 	$response = [
            //         'status'=>$codes->RC_ERROR,
            //         'message'=>'conversation id must be numeric',
            //         'data'=>null
            //     ];

            //     return $response;
            // }

            if(!isset($input['recipients']) || !is_array($input['recipients'])){
				$response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'recipients is required and must be in array format',
                    'data'=>null
                ];

                return $response;
            }

			
			/* if recipients not array */
			if(isset($input['recipients'])) {
				
				/* recipients must contain numeric values only */
				$input['recipients'] = array_filter($input['recipients'], 'is_numeric');
				/* check blocking */
				foreach($input['recipients'] as $recipient) {
					if($user->blocked($recipient)) {
                        $response = [
                            'status'=>$codes->RC_ERROR,
                            'message'=>'recipient  is blocked',
                            'data'=>null
                        ];
        
                        return $response;
					}
				}
            }
             
            $photo = '';
            $dimensions = array();
			if(isset($_FILES['photo']) && !empty($_FILES["photo"]["name"])) {
				$max_allowed_size = $codes->MAX_PHOTO_SIZE * 1024;
				/* check & create uploads dir */
				$folder = 'photos';
				$depth = './../../';
				if(!file_exists($depth.$codes->UPLOADS_DIRECTORY.'/'.$folder)) {
					@mkdir($depth.$codes->UPLOADS_DIRECTORY.'/'.$folder, 0777, true);
				}
				if(!file_exists($depth.$codes->UPLOADS_DIRECTORY.'/'.$folder.'/' . date('Y'))) {
					@mkdir($depth.$codes->UPLOADS_DIRECTORY.'/'.$folder.'/' . date('Y'), 0777, true);
				}
				if(!file_exists($codes->UPLOADS_DIRECTORY.'/'.$folder.'/' . date('Y') . '/' . date('m'))) {
					@mkdir($depth.$codes->UPLOADS_DIRECTORY.'/'.$folder.'/' . date('Y') . '/' . date('m'), 0777, true);
				}
				// valid inputs
				if(!isset($_FILES["photo"]) || $_FILES["photo"]["error"] != UPLOAD_ERR_OK) {
                    $response = [
                        'status'=>$codes->RC_ERROR,
                        'message'=>"Something wrong with upload! Is 'upload_max_filesize' set correctly?",
                        'data'=>null
                    ];
    
                    return $response;	
					//throw new Exception("Something wrong with upload! Is 'upload_max_filesize' set correctly?");
				}

				// check file size
				if($_FILES["photo"]["size"] > $max_allowed_size) {
                    $response = [
                        'status'=>$codes->RC_ERROR,
                        'message'=>"The file size is so big",
                        'data'=>null
                    ];
    
                    return $response;	
					//throw new Exception("The file size is so big");
				}
				
				/* prepare new file name */
				$directory = $folder.'/'. date('Y') . '/' . date('m') . '/';
				$prefix = $codes->UPLOADES_PREFIX.'_'.md5(time()*rand(1, 9999));
                $image = new Image($_FILES["photo"]["tmp_name"]);
                $get_dimensions = getimagesize($_FILES['photo']['tmp_name']);
				$dimensions = array(
					'width' => $get_dimensions[0],
					'height' => $get_dimensions[1],
				);
				$image_tmp_name = $directory.$prefix.'_tmp'.$image->_img_ext;
				$image_new_name = $directory.$prefix.$image->_img_ext;
				$path_tmp = $depth.$codes->UPLOADS_DIRECTORY.'/'.$image_tmp_name;
				$path_new = $depth.$codes->UPLOADS_DIRECTORY.'/'.$image_new_name;

				/* check if the file uploaded successfully */
				if(!@move_uploaded_file($_FILES['photo']['tmp_name'], $path_tmp)) {
                    $response = [
                        'status'=>$codes->RC_ERROR,
                        'message'=>"Sorry, can not upload the photo",
                        'data'=>null
                    ];
    
                    return $response;	
					//throw new Exception("Sorry, can not upload the photo");
				}
				
				/* save the new image */
				$resize =  true;
				$image->save($path_new, $path_tmp, $resize);
               
				/* delete the tmp image */
				unlink($path_tmp);

				/* upload to amazon s3 */
				if($codes->S3_ENABLED) {
					aws_s3_upload($path_new, $image_new_name);
				}
				
                /* return */
				$photo = $image_new_name;
			}
            $temp_msg_id = isset($input['temp_msg_id']) && !empty( $input['temp_msg_id'] ) ? $input['temp_msg_id'] : 0;
            
			$conversation = $user->post_conversation_message($input['message'], $photo, $input['conversation_id'], @$input['recipients'], $dimensions,$temp_msg_id);

			$conversation["temp_msg_id"] = $temp_msg_id;

			if($conversation){
                if($conversation == 'not author'){
                    $response['status'] = 0;
                    $response['message'] = 'You are not authorized to do this';
                    $response['data'] = null;
                }else{
                    $response['status'] = 1;
                    $response['message'] = '';
                    $response['data'] = $conversation;
                }
			}
            
            return $response;

        }catch(\Exception $e){
            print_r($e);
            Log::debug($e);
            throw new Exception($e);
        }
    }

    /**
     * 
     */
    public function createMultimessage($input)
    {
        Log::debug("Started ... createMultimessage chat Service : ".json_encode($input));
        try{
            $codes = new Codes;
            if(!isset($input['user_id']) || empty($input['user_id'])){
				$response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'user id is required',
                    'data'=>null
                ];

                return $response;
            }

            $user = new UserModel($input['user_id']);
			
			/* if message not set */
			if(!isset($input['message_id']) || !is_array($input['message_id'])){
				$response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'message is required and must be an array',
                    'data'=>null
                ];

                return $response;
            }
			
            /* if both (conversation_id & recipients) not set */
            if(!isset($input['conversation_id']) || empty($input['conversation_id'])){
				$response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'conversation id is required',
                    'data'=>null
                ];

                return $response;
            }

            // if(isset($input['conversation_id']) || !is_numeric($input['conversation_id'])){
			// 	$response = [
            //         'status'=>$codes->RC_ERROR,
            //         'message'=>'conversation id must be numeric',
            //         'data'=>null
            //     ];

            //     return $response;
            // }

            if(!isset($input['recipients']) || !is_array($input['recipients'])){
				$response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'recipients is required and must be in array format',
                    'data'=>null
                ];

                return $response;
            }

			
			/* if recipients not array */
			if(isset($input['recipients'])) {
				
				/* recipients must contain numeric values only */
				$input['recipients'] = array_filter($input['recipients'], 'is_numeric');
				/* check blocking */
				foreach($input['recipients'] as $recipient) {
					if($user->blocked($recipient)) {
                        $response = [
                            'status'=>$codes->RC_ERROR,
                            'message'=>'recipient  is blocked',
                            'data'=>null
                        ];
        
                        return $response;
					}
				}
            }
             
            
            $conversation=array();
            $temp_msg_id = isset($input['temp_msg_id']) && !empty( $input['temp_msg_id'] ) ? $input['temp_msg_id'] : 0;
            $conn = ConnectionManager::get('default');
            foreach($input['message_id'] as $mk=>$msg)
            {
                $msgid = $conn->execute(sprintf("SELECT * FROM `conversations_messages` where message_id = '%s'", $msg));
                $count=$msgid->count();
                if($count>0)
                {
                $msgdetail = $msgid->fetch("assoc");
               
                $mess=$msgdetail['message'];
                $photo=$msgdetail['image'];
                $dimensions['all']=$msgdetail['img_dimensions'];
                if(!empty($input['recipients']))
                {
                    foreach($input['recipients'] as $rk=>$rval)
                    {
                        $conversationid=$user->create_get_conversation($rval);
                        $conversation[] = $user->post_conversation_message($mess, $photo, $conversationid, $rval, $dimensions,$temp_msg_id);
                    }
                }
               
                if(!empty($input['conversation_id']))
                {
                    foreach($input['conversation_id'] as $ck=>$cval)
                    {
                        $conversation[] = $user->post_conversation_message($mess, $photo, $cval, $input['recipients'], $dimensions,$temp_msg_id);    
                    }
                }
                
                }
            }
           // print_r($conversation);
			$temp_msg_id = isset($input['temp_msg_id']) && !empty( $input['temp_msg_id'] ) ? $input['temp_msg_id'] : 0;
			//$conversation["temp_msg_id"] = $temp_msg_id;

			if(count($conversation) > 0){
				$response['status'] = 1;
			    $response['message'] = 'Message sent successfully';
			    $response['data'] = $conversation;
            }
            
            return $response;

        }catch(\Exception $e){
            print_r($e);
            Log::debug($e);
            throw new Exception($e);
        }
    }
}