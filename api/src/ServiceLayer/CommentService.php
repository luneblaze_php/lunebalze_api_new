<?php

namespace App\ServiceLayer;

use App\DaoLayer\CampusDetailDao;
use App\Utils\Codes;
use App\Utils\DateUtils;
use App\Utils\DataValidation;
use App\Model\UserModel;
use Cake\Core\Exception\Exception;
use App\DaoLayer\InterestMstDao;
use App\DaoLayer\OrganizationDao;
use App\DaoLayer\OrganizationTypeDao;
use App\DaoLayer\PostsDao;
use App\DaoLayer\PostsCommentsDao;
use App\DaoLayer\PostsLikesDao;
use App\DaoLayer\PostsSavedDao;
use App\DaoLayer\ScheduleCampusDao;
use App\DaoLayer\UserInterestsDao;
use App\DaoLayer\UserOrganizerDao;
use App\DaoLayer\UsersDao;
use App\DaoLayer\UserBlocksDao;
use App\DaoLayer\NotificationsDao;
use App\DaoLayer\ArticlesDao;
use App\DaoLayer\PointsDao;
use App\Utils\ImageUtils;
use App\Model\Image;
use Cake\Datasource\ConnectionManager;
use Cake\Log\Log;

class CommentService
{
    public function _apiError($code=404, $message=''){
        $response = [
            'status'=>$code,
            'message'=> ('' !== $message ? $message : 'Invalid data'),
            'data'=>null
        ];

        return $response; 
    }

    public function getReactions($input)
    {
        Log::debug("Started ... getInterestSiblings Service : ");
        try{
            $conn = ConnectionManager::get('default');
            $codes = new Codes;
            $dataValidation = new DataValidation;
            //extract($input);


            if(!isset($input['reaction_id'])){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'reaction_id  is required',
                    'data'=>null
                ];
                return $response;
            }
            if($dataValidation->isEmpty($input['reaction_id'])){
                $type = 0;
                $reaction = 0;
            }else{
                $type = 1;
                $reaction = $input['reaction_id'];
            }
            
            //extract($input);
            $ad = new NotificationsDao;
            $data = $ad->getReactions($type , $reaction);
            
            $response['status'] = $data['status'];
            $response['message'] = $data['message'];
            $response['data'] = $data['data'];
            

            return $response;            
        }catch(\Exception $e){
            print_r($e);
            Log::debug($e);
            throw new Exception($e);
        }
    }
    public function getPoll($input)
    {
        Log::debug("Started ... getInterestSiblings Service : ");
        try{
            $conn = ConnectionManager::get('default');
            $codes = new Codes;
            $dataValidation = new DataValidation;
            //extract($input);


            if(!isset($input['user_id']) || $dataValidation->isEmpty($input['user_id'])){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'user_id  is required',
                    'data'=>null
                ];
                return $response;
            }
            if(!isset($input['poll_id']) || $dataValidation->isEmpty($input['poll_id'])){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'poll_id  is required',
                    'data'=>null
                ];
                return $response;
            }
            
            
            extract($input);
            $ad = new PostsDao;
            $data = $ad->getPollData($poll_id , $user_id);
            if($data != []){
                $response['status'] = $codes->RC_SUCCESS;
                $response['message'] = $codes->RM_SUCCESS;
                $response['data'] = $data;
            }else{
                $response['status'] = $codes->RC_ERROR;
                $response['message'] = "Poll Not Found";
                $response['data'] = null;
            }
            

            return $response;            
        }catch(\Exception $e){
            print_r($e);
            Log::debug($e);
            throw new Exception($e);
        }
    }

    public function deleteNotification($input)
    {
        Log::debug("Started ... getInterestSiblings Service : ");
        try{
            $conn = ConnectionManager::get('default');
            $codes = new Codes;
            $dataValidation = new DataValidation;
            //extract($input);

            if(!isset($input['notification_id']) || $dataValidation->isEmpty($input['notification_id'])){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'notification_id  is required',
                    'data'=>null
                ];
                return $response;
            }
            
            extract($input);
            $ad = new NotificationsDao;
            $data = $ad->deleteNotification($notification_id);
            if(isset($data['status'])){
                if($data['status'] == 1){
                    $response['status'] = 1;
                    $response['message'] = $data['message'];
                    $response['data'] = null;
                }else{
                    $response['status'] = 0;
                    $response['message'] = $data['message'];
                    $response['data'] = null;
                }
            }else{
                $response['status'] = 1;
                $response['message'] = 'notification status';
                $response['data'] = $data;
            }

            return $response;            
        }catch(\Exception $e){
            print_r($e);
            Log::debug($e);
            throw new Exception($e);
        }
    }

    public function getArticle($input)
    {
        Log::debug("Started ... getInterestSiblings Service : ");
        try{
            $conn = ConnectionManager::get('default');
            $codes = new Codes;
            $dataValidation = new DataValidation;
            //extract($input);

            if(!isset($input['article_id']) || $dataValidation->isEmpty($input['article_id'])){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'creator_id  is required',
                    'data'=>null
                ];
                return $response;
            }
            if(!isset($input['user_id']) || $dataValidation->isEmpty($input['user_id'])){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'user_type  is required',
                    'data'=>null
                ];
                return $response;
            }
            
            extract($input);
            $ad = new ArticlesDao;
            $data = $ad->getArticlesByArticlesId($article_id , $user_id);
            $response['status'] = 1;
            $response['message'] = 'Article data';
            $response['data'] = $data;

            return $response;            
        }catch(\Exception $e){
            print_r($e);
            Log::debug($e);
            throw new Exception($e);
        }
    }

    public function editArticle($input)
    {
        Log::debug("Started ... getInterestSiblings Service : ");
        try{
            $conn = ConnectionManager::get('default');
            $codes = new Codes;
            $dataValidation = new DataValidation;
            //extract($input);

            if(!isset($input['article_id']) || $dataValidation->isEmpty($input['article_id'])){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'article_id  is required',
                    'data'=>null
                ];
                return $response;
            }
            if(!isset($input['creator_id']) || $dataValidation->isEmpty($input['creator_id'])){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'creator_id  is required',
                    'data'=>null
                ];
                return $response;
            }
            
            extract($input);
            $ad = new ArticlesDao;
            $data = $ad->editArticle($input , $input['article_id'] , $input['creator_id']);
            $response['status'] = 1;
            $response['message'] = 'Article data';
            $response['data'] = $data;

            return $response;            
        }catch(\Exception $e){
            print_r($e);
            Log::debug($e);
            throw new Exception($e);
        }
    }







    public function createArticle($input)
    {
        Log::debug("Started ... getInterestSiblings Service : ");
        try{
            $conn = ConnectionManager::get('default');
            $codes = new Codes;
            $dataValidation = new DataValidation;
            //extract($input);

            if(!isset($input['creator_id']) || $dataValidation->isEmpty($input['creator_id'])){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'creator_id  is required',
                    'data'=>null
                ];
                return $response;
            }
            if(!isset($input['user_type']) || $dataValidation->isEmpty($input['user_type'])){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'user_type  is required',
                    'data'=>null
                ];
                return $response;
            }
            if(!isset($input['title']) || $dataValidation->isEmpty($input['title'])){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'title  is required',
                    'data'=>null
                ];
                return $response;
            }
            if(!isset($input['description']) || $dataValidation->isEmpty($input['description'])){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'description  is required',
                    'data'=>null
                ];
                return $response;
            }
            if(!isset($input['creator_id']) || $dataValidation->isEmpty($input['creator_id'])){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'creator_id  is required',
                    'data'=>null
                ];
                return $response;
            }

            $ad = new ArticlesDao;
            $data = $ad->createArticle($input);
            $response['status'] = 1;
            $response['message'] = 'article creation Status';
            $response['data'] = $data;

            return $response;            
        }catch(\Exception $e){
            print_r($e);
            Log::debug($e);
            throw new Exception($e);
        }
    }

    public function get_comment($jsonInput){

        $codes = new Codes;
        $dataValidation = new DataValidation;
        $organizationDao = new OrganizationDao;
        $imageUtils = new ImageUtils;

        Log::debug("Started ... Comment Service : ".json_encode($inputJson));
        try{

            if(!isset($inputJson['user_id'])){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'User Id is not present',
                    'data'=>null
                ];

                return $response;            
            } else {

                $user = UsersDao::updateStatusByUserId($jsonInput['user_id']);

                $system = UserDao::getSystem();

                // check user activated
                if($system['activation_enabled'] && !$user['user_activated']) {
                    throw new Exception("Before you can interact with other users, you need to confirm your email address");
                }
                // valid inputs
                $valid['handle'] = array('post', 'photo');
                if(isset($jsonInput['handle']) && !in_array($jsonInput['handle'], $valid['handle'])) {
                    $response = [
                        'status'=>$codes->RC_ERROR,
                        'message'=>'Invalid handle',
                        'data'=>null
                    ];

                    return $response;   
                }
                /* if id is set & not numeric */
                if(!isset($jsonInput['id']) || !is_numeric($jsonInput['id'])) {
                    $response = [
                        'status'=>$codes->RC_ERROR,
                        'message'=>'Invalid id',
                        'data'=>null
                    ];

                    return $response;
                }
                /* if message not set */
                if(!isset($jsonInput['message'])) {
                    $response = [
                        'status'=>$codes->RC_ERROR,
                        'message'=>'Invalid message',
                        'data'=>null
                    ];

                    return $response;
                }
                
                if(isset($_FILES['photo'])) {
                    $max_allowed_size =$codes->MAX_FILE_SIZE * 1024;
                    /* check & create uploads dir */
                    $folder = 'photos';
                    $depth = '../../../';
                    if(!file_exists($depth.$codes->UPLOADS_DIRECTORY.'/'.$folder)) {
                        @mkdir($depth.$codes->UPLOADS_DIRECTORY.'/'.$folder, 0777, true);
                    }
                    if(!file_exists($depth.$codes->UPLOADS_DIRECTORY.'/'.$folder.'/' . date('Y'))) {
                        @mkdir($depth.$codes->UPLOADS_DIRECTORY.'/'.$folder.'/' . date('Y'), 0777, true);
                    }
                    if(!file_exists($codes->UPLOADS_DIRECTORY.'/'.$folder.'/' . date('Y') . '/' . date('m'))) {
                        @mkdir($depth.$codes->UPLOADS_DIRECTORY.'/'.$folder.'/' . date('Y') . '/' . date('m'), 0777, true);
                    }
                    // valid inputs
                    if(!isset($_FILES["photo"]) || $_FILES["photo"]["error"] != UPLOAD_ERR_OK) {
                        //throw new Exception("Something wrong with upload! Is 'upload_max_filesize' set correctly?");

                        $response = [
                            'status'=>$codes->RC_ERROR,
                            'message'=>"Something wrong with upload! Is 'upload_max_filesize' set correctly?",
                            'data'=>null
                        ];

                        return $response;
                    }

                    // check file size
                    if($_FILES["photo"]["size"] > $max_allowed_size) {
                        $response = [
                            'status'=>$codes->RC_ERROR,
                            'message'=>"The file size is so big",
                            'data'=>null
                        ];

                        return $response;
                    }
                    
                    /* prepare new file name */
                    $directory = $folder.'/'. date('Y') . '/' . date('m') . '/';
                    $prefix = $codes->UPLOADES_PREFIX.'_'.md5(time()*rand(1, 9999));
                    $image = new Image($_FILES["photo"]["tmp_name"]);
                    $image_tmp_name = $directory.$prefix.'_tmp'.$image->_img_ext;
                    $image_new_name = $directory.$prefix.$image->_img_ext;
                    $path_tmp = $depth.$codes->UPLOADS_DIRECTORY.'/'.$image_tmp_name;
                    $path_new = $depth.$codes->UPLOADS_DIRECTORY.'/'.$image_new_name;

                    /* check if the file uploaded successfully */
                    if(!@move_uploaded_file($_FILES['photo']['tmp_name'], $path_tmp)) {
                        $response = [
                            'status'=>$codes->RC_ERROR,
                            'message'=>"Sorry, can not upload the photo",
                            'data'=>null
                        ];
                        return $response;
                    }
                    
                    /* save the new image */
                    $resize =  true;
                    $image->save($path_new, $path_tmp, $resize);
                   
                    /* delete the tmp image */
                    unlink($path_tmp);

                    /* upload to amazon s3 */
                    if($codes->S3_ENABLED) {
                        //aws_s3_upload($path_new, $image_new_name);
                    }
                    
                    /* return */
                    $photo = $image_new_name;
                }
                
                $comment_array = array();
                $comment_array['handle'] = isset($jsonInput['handle'])?$jsonInput['handle']:'';
                $comment_array['id'] = $jsonInput['id'];
                if(isset($jsonInput['message'])){
                    $comment_array['message'] = $jsonInput['message'];
                }

                if( isset($photo) ){
                    $comment_array['photo'] = $photo;
                }

                $comment = self::comment($user, $system, $jsonInput['user_id'], $comment_array);
                
                return $comment;
            }
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }


    /**
     * blocked
     *
     * @param integer $user_id
     * @return boolean
     */
    public function blocked($user_id=0, $block_id=0)
    {
        /* check if there is any blocking between the viewer & the target */
        if ($block_id) {
            $check = UserBlocksDao::getUsersBlocksByUserIdAndBlockId($user_id, $block_id);
            return $check;
        }
        return false;
    }
    
    
    /**
     * comment
     *
     * @param string $handle
     * @param integer $node_id
     * @param string $message
     * @param string $photo
     * @return array
     */
    public function comment($user=[],$system=[], $user_id=false, array $args = array())
    {
        $codes = new Codes;
        $serverUtils = new ServerUtils;
        $postsDao = new PostsDao;

        $systemUpload = $serverUtils->getSystemUpload();

        $comment = array();

        /* default */
        $comment['node_id'] = $args['id'];
        $comment['node_type'] = $args['handle'];
        $comment['text'] = isset($args['message']) ? $args['message'] : '';
        $comment['image'] = isset($args['photo']) ? $args['photo'] : '';
        $comment['time'] = $date;
        $comment['likes'] = 0;

        $comment['user_id'] = $user['user_id'];
        $comment['user_type'] = "user";
        $comment['author_picture'] = $user['user_picture'];
        $comment['author_url'] = $system['system_url'] . '/' . $user['user_name'];
        $comment['author_name'] = $user['user_fullname'];
        $comment['author_verified'] = $user['user_verified'];

        /* check the handle */
        if ($args['handle'] == "post") {
            /* (check|get) post */
            $post = $postsDao->getPostDetailByPostIdPostType($args['id']);
            if ( empty($post) || !$post) {
                self::_apiError(403);
            }
        } else {
            /* (check|get) photo 
            $photo = self::get_photo($args['id']);
            if (!$photo) {
                self::_apiError(403);
            }
            $post = $photo['post'];*/
        }

        /* check blocking */
        if (self::blocked($post['author_id'], $user['user_id'])) {
            self::_apiError(403);
        }

        /* check if the viewer is page admin of the target post */
        if ($post['is_page_admin']) {
            $comment['user_id'] = $post['page_id'];
            $comment['user_type'] = "page";
            #$comment['author_picture'] = self::get_picture($post['page_picture'], "page");
            $comment['author_url'] = $system['system_url'] . '/pages/' . $post['page_name'];
            $comment['author_name'] = $post['page_title'];
            $comment['author_verified'] = $post['page_verified'];
        }

        /* insert the comment */
        $comment['comment_id'] = PostsCommentsDao::insertPostComment($comment);

        
        /* insert /update in user_activity
         */
        UserActivityDao::insertUserActivity();

        /* insert /update in user_activity
         */
        
        /* update (post|photo) comments counter */
        if ($args['handle'] == "post") {
            $post_data =  PostDao::getPostDetailByPostId($comment['node_id']);
            if ( !empty($post_data['comment_ids']) ) {
                $comment_ids = explode(',', $post_data['comment_ids']); 
            } else {
                $comment_ids = array();
            }

            array_push($comment_ids, $comment['comment_id']);
            
            $comment_ids = implode(',', $comment_ids);

            PostDao::updatePostCommentCounter($comment_ids, $comment['node_id']);

        } else {

            PostDao::updatePostPhotoCommentCounter($comment['node_id']);
        }

        /* post notification */
        /* notification merging : Arnab mohanty*/
        //self::post_notification($post['user_id'], 'comment', $comment['node_type'], $comment['node_id'].'/'.$comment['comment_id'] );
        
        if($post['author_id'] != $user['user_id']){
            $node_url=$comment['node_id'].'/'.$comment['comment_id'];
                
                $data = NotificationsDao::getNotificationByToUserIdAndActionAnsNodeTypeAndUniqueId($post['author_id'], 'comment', $comment['node_type'], $comment['node_id']);
                
                if ($data){
                    
                    $total = $data['total']+1;
                    $total_user_ids = $data['total_user_ids'];
                    $new_total_user_ids = $total_user_ids.','.$user['user_id'];
                    $total_user_ids_array = explode(',',$total_user_ids);
                    if (in_array($user['user_id'], $total_user_ids_array)) 
                       NotificationsDao::updateNotification($user['user_id'], $comment['comment_id'], $node_url, $data['notification_id']);
                    elseif($data['to_user_id'] != $user['user_id'])
                        NotificationsDao::updateNotificationTotal($new_total_user_ids, $total, $user['user_id'], $comment['comment_id'], $node_url, $data['notification_id']);
                    if($data['seen']==1){
                        UsersDao::updateLiveNotificationCounter($post['author_id']);                        
                    }
                }
                
                #self::post_notification($post['author_id'],'comment',$comment['node_type'],$node_url,$comment['comment_id'],'',$comment['node_id']);
                
            }

        /* post mention notifications if any */
        #self::post_mentions($comment['text'], $comment['node_id'], $comment['node_type'], $comment['comment_id']);

        /* parse text
        $comment['text_plain'] = $comment['text'];
        $comment['text'] = parse_api($comment['text_plain']);*/

        /* check if viewer can manage comment [Edit|Delete] 
        $comment['edit_comment'] = true;
        $comment['delete_comment'] = true;*/
        
        /* Post Merging by Arnab Mohanty 6th dec
        if($post['author_id'] != self::$_data['user_id']){
            $check_post = self::$conn->execute(sprintf("SELECT * FROM `posts` WHERE `post_type` = 'post_comment' AND origin_id = %s", self::secure($comment['node_id'], 'int'))) or self::_apiError('SQL_apiError_THROWEN');
            if ($check_post->count() > 0){
                $data = $check_post->fetch('assoc');
                $total = $data['total']+1;
                $total_user_ids = $data['total_user_ids'];
                $new_total_user_ids = $total_user_ids.','.self::$_data['user_id'];
                $total_user_ids_array = explode(',',$total_user_ids);
                if (in_array(self::$_data['user_id'], $total_user_ids_array)) 
                   self::$conn->execute(sprintf("UPDATE `posts` SET time = %s, text=%s WHERE post_id = %s ", self::$date, self::secure($comment['text']), self::secure($data['post_id'], 'int'))) or self::_apiError('SQL_apiError_THROWEN');
                elseif($data['user_id'] != self::$_data['user_id'])
                    self::$conn->execute(sprintf("UPDATE `posts` SET total_user_ids = %s, text=%s, total = %s, time = %s, user_id = %s WHERE post_id = %s ",self::$new_total_user_ids, self::secure($comment['text']), self::$total, self::$date, self::secure(self::$_data['user_id'], 'int'), self::secure($data['post_id'], 'int'))) or self::_apiError('SQL_apiError_THROWEN');
                
            }
            else{
                $post_relevancy = self::$conn->execute("SELECT * FROM `post_relevancy` where id = '1'") or self::_apiError('SQL_apiError_THROWEN');
                $post_relevancy_row = $post_relevancy->fetch('assoc');
                self::$conn->execute(sprintf("INSERT INTO posts (user_id, text, total_user_ids, origin_id, user_type, post_type, time, privacy, post_type_value) VALUES (%s, %s, %s, %s, 'user', 'post_comment', %s, 'public', %s)", self::secure(self::$_data['user_id'], 'int'), self::secure($comment['text']), self::secure(self::$_data['user_id']), self::secure($comment['node_id'], 'int'), self::secure($date), self::secure($post_relevancy_row['profile_content']))) or self::_apiError('SQL_apiError_THROWEN');
            }
        }*/
        
        /* Need to dicuss not sure 
        $node_id=$comment['node_id'];
        $handle=$args['handle'];
        $check_post = self::$conn->execute(sprintf("SELECT * FROM `posts` WHERE (`post_type` = '' OR `post_type` = 'post_like' OR `post_type` = 'post_comment' OR `post_type`='poll' OR `post_type`='photos') AND post_id = %s", self::secure($node_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        if ($check_post->count() > 0){
            $data = $check_post->fetch('assoc');
            $total_user_ids = $data['total_user_ids'];
            $new_total_user_ids = $total_user_ids.','.self::$_data['user_id'];
            $total_user_ids_array = explode(',',$total_user_ids);
            $total = $data['total']+1;
            if (in_array(self::$_data['user_id'], $total_user_ids_array))
            {
                self::$conn->execute(sprintf("UPDATE `posts` SET post_type = 'post_comment',time = %s WHERE post_id = %s ",self::$date, self::secure($data['post_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
            }
            else
            {
               self::$conn->execute(sprintf("UPDATE `posts` SET post_type = 'post_comment', total_user_ids = %s, total = %s, time = %s WHERE post_id = %s ", self::$new_total_user_ids ,self::secure($total), self::$date, self::secure($data['post_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
            }
            
        }
        $check_post = self::$conn->execute(sprintf("SELECT * FROM `posts` WHERE (`post_type` = 'shared' OR `post_type` = 'shared_like' OR `post_type` = 'shared_comment' OR `post_type`='article_shared' OR `post_type`='question_shared' OR `post_type`='session_shared' OR `post_type`='answer_shared') AND post_id = %s", self::secure($node_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        if ($check_post->count() > 0){
            $data = $check_post->fetch('assoc');
            $total_user_ids = $data['total_user_ids'];
            $total = $data['total']+1;
            $new_total_user_ids = $total_user_ids.','.self::$_data['user_id'];  
            $total_user_ids_array = explode(',',$total_user_ids);
            $total = count(explode(',',$data['comment_ids']))-1;
            if (in_array(self::$_data['user_id'], $total_user_ids_array))
            {
                self::$conn->execute(sprintf("UPDATE `posts` SET post_type = 'post_comment',time = %s WHERE post_id = %s ",self::$date, self::secure($data['post_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
            }
            else
            {
            self::$conn->execute(sprintf("UPDATE `posts` SET post_type = 'post_comment', total_user_ids = %s, total = %s, time = %s WHERE post_id = %s ", self::$new_total_user_ids ,self::secure($total), self::$date, self::secure($data['post_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
            }
            
        }*/
        
        /* return */
        return $comment;
    }

      /**
     * 
     */
    public function readNotification($input)
    {
        Log::debug("Started ... readNotification Service : ");
        try{
            $conn = ConnectionManager::get('default');
            $codes = new Codes;
            extract($input);

            if(!isset($input['user_id']) || empty($input['user_id'])){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'user id is required',
                    'data'=>null
                ];

                return $response;
            }

            if(!isset($input['notification_id']) || empty($input['notification_id'])){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'notification_id is required',
                    'data'=>null
                ];

                return $response;
            }

            $user = new UserModel($input['user_id']);

            $sql=sprintf("UPDATE notifications SET seen = '1' WHERE notification_id = %s AND to_user_id = %s", $notification_id, $user->_data['user_id']);

            Log::debug("SQL : ".$sql);

            $conn->execute($sql);
            
            $response['status'] = 1;
            $response['message'] = 'Notification Read';
            $response['data'] = [];

            return $response;            
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }

      /**
     * 
     */
    public function notificationSeen($input)
    {
        Log::debug("Started ... notificationSeen Service : ");
        try{
            $conn = ConnectionManager::get('default');
            $codes = new Codes;
            extract($input);

            if(!isset($input['user_id']) || empty($input['user_id'])){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'user id is required',
                    'data'=>null
                ];

                return $response;
            }

         

            $user = new UserModel($input['user_id']);

            $conversation = $user->mark_notification_as_seen($input['user_id']);
			
			if($conversation){
				$response['status'] = 1;
			    $response['message'] = 'Notification Seen Successfully';
			}

            return $response;            
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }

       /**
     * 
     */
    public function getSubInterestList($input)
    {
        Log::debug("Started ... getSubInterestList Service : ");
        try{
            $conn = ConnectionManager::get('default');
            $codes = new Codes;
            extract($input);

            if(!isset($input['interest_id']) || empty($input['interest_id'])){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'inser id is required',
                    'data'=>null
                ];

                return $response;
            }

            if(isset($user_id) && !empty($user_id)){
				$user = new UserModel($user_id);
			} else {
				$user = new UserModel();
			}
			$data = $user->get_sub_interest_list($interest_id);
			$response['status'] = 1;
			$response['message'] = '';
			$response['data'] = $data;

            return $response;            
        }catch(\Exception $e){
            print_r($e);
            Log::debug($e);
            throw new Exception($e);
        }
    }
    private function getImgFullPath($img = '', $newPath = 0){
        $codes = new Codes;
        return !empty($img) ?$codes->SYSTEM_URL.'/'.($newPath?'api/':'').$codes->UPLOADS_DIRECTORY.'/'.$img:'';
    }

       /**
     * 
     */
    public function getAllinterest($input)
    {
        Log::debug("Started ... getAllinterest Service : ");
        try{
            $conn = ConnectionManager::get('default');
            $codes = new Codes;
            extract($input);

            $rows = [];
            $user = new UserModel();
            // get data
            $get_rows = $conn->execute("SELECT * FROM `interest_mst` WHERE `status` = '1' and parent_id='0'");
            if ($get_rows->count() > 0) {
                $i=0;
                while ($row = $get_rows->fetch("assoc")) {
                    $rows[$i]['interest_id'] = $row['interest_id'];
                    $rows[$i]['text'] = $row['text'];
                    $rows[$i]['parent_id'] = $row['parent_id'];
                    $rows[$i]['image'] = $this->getImgFullPath($row['image'],1);
                    //$rows[$i]['description'] = $row['description'];
                    $rows[$i]['status'] = $row['status'];
                    //$rows[$i]['added_on'] = $row['added_on'];
                // $rows[$i]['modified_on'] = $row['modified_on'];
                    $get_row = $conn->execute(sprintf("SELECT * FROM `interest_mst` WHERE `status` = '1' and parent_id='%s'", $row['interest_id']));
                    if($get_row->count()==0)
                    {
                        $empty=array();
                        $rows[$i]['child']=$empty;
                    }
                    else
                    {
                        $rows[$i]['child']=$user->childinterest($row['interest_id']);
                    }
                    $i++;
                }
            }
            
            $response['status'] = 1;
            $response['message'] = 'Interest List';
            $response['data'] = $rows;


            return $response;            
        }catch(\Exception $e){
            print_r($e);
            Log::debug($e);
            throw new Exception($e);
        }
    }

      /**
     * 
     */
    public function getInterestSiblings($input)
    {
        Log::debug("Started ... getInterestSiblings Service : ");
        try{
            $conn = ConnectionManager::get('default');
            $codes = new Codes;
            extract($input);

            if(!isset($input['interest_id']) || empty($input['interest_id'])){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'inser id is required',
                    'data'=>null
                ];

                return $response;
            }

            if(isset($user_id) && !empty($user_id)){
				$user = new UserModel($user_id);
			} else {
				$user = new UserModel();
			}
			$data = $user->get_interest_list($interest_id);
			$response['status'] = 1;
			$response['message'] = '';
			$response['data'] = $data;

            return $response;            
        }catch(\Exception $e){
            print_r($e);
            Log::debug($e);
            throw new Exception($e);
        }
    }

      /**
     * 
     */
    public function loggedInDevices($input)
    {
        Log::debug("Started ... loggedInDevices Service : ");
        try{
            $conn = ConnectionManager::get('default');
            $codes = new Codes;
            extract($input);

            if(!isset($input['email']) || empty($input['email'])){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'Plese enter email',
                    'data'=>null
                ];

                return $response;
            }

            if(isset($user_id) && !empty($user_id)){
				$user = new UserModel($user_id);
			} else {
				$user = new UserModel();
			}

			$data = $user->logged_in_devices($email);

			$response['status'] = 1;
			$response['message'] = '';
			$response['devices'] = $data;

            return $response;            
        }catch(\Exception $e){
            print_r($e);
            Log::debug($e);
            throw new Exception($e);
        }
    }

      /**
     * 
     */
    public function getUserByPhone($input)
    {
        Log::debug("Started ... getUserByPhone Service : ");
        try{
            $conn = ConnectionManager::get('default');
            $codes = new Codes;
            extract($input);

            if(!isset($input['user_id']) || empty($input['user_id'])){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'Viewer user_id is required',
                    'data'=>null
                ];

                return $response;
            }

            if(!isset($input['phone_no']) || empty($input['phone_no'])){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'Phone number is required',
                    'data'=>null
                ];

                return $response;
            }

            if ( !is_array($input['phone_no']) ) {
                $input['phone_no'] = (array) $input['phone_no'];
            }
    
            $phone_numbers = implode(", ", $input['phone_no']);
    

            $userObj = new UserModel($input['user_id']);
		
		$users = array();
		/* get users */
		$get_users = $conn->execute(sprintf("SELECT users.user_id, users.user_name, users.user_fullname, users.user_gender, users.user_picture, users.user_work, users.user_work_title, users.user_work_place, users_extra_info.data as user_phone FROM users JOIN users_extra_info ON users.user_id = users_extra_info.user_id WHERE status = 1 AND data IN (%s)", $phone_numbers));
		
		if ($get_users->count() > 0) {
			while ($user = $get_users->fetch("assoc")) {
				$user['user_picture'] = $userObj->get_picture($user['user_picture'], $user['user_gender']);
				$user['connection'] = $userObj->connection($user['user_id'], true);				
				$user['mutual_friends_count'] = count($userObj->get_friends($user['user_id']));
				// $user['friends'] = $userObj->get_friends($user['user_id']);
				$users[] = $user;
			}
		}
		
		if($users){
			$response['status'] = 1;
			$response['message'] = 'Result';
			$response['data'] = $users;
		} else {
			$response['status'] = 0;
			$response['message'] = 'Nothing Found';
			$response['data'] = array();
		}

            return $response;            
        }catch(\Exception $e){
            print_r($e);
            Log::debug($e);
            throw new Exception($e);
        }
    }



      /**
     * 
     */
    public function comments($input)
    {
        Log::debug("Started ... comments Service : ");
        try{
            $conn = ConnectionManager::get('default');
            $codes = new Codes;
            extract($input);

            if(!isset($input['id']) || empty($input['id'])|| !is_numeric($input['id'])){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'Id Required. Invalid request.',
                    'data'=>null
                ];

                return $response;
            }

            $valid['handle'] = array('post', 'photo');
            if(!isset($input['handle']) || !in_array($input['handle'], $valid['handle'])){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'Valid handle required',
                    'data'=>null
                ];

                return $response;
            }

            if(!isset($input['message'])){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'message required',
                    'data'=>null
                ];

                return $response;
            }

            if(isset($user_id) && !empty($user_id)){
				$user = new UserModel($user_id);
			} else {
				$user = new UserModel();
			}

            if($codes->ACTIVATION_ENABLED && !$user->_data['user_activated']){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'Before you can interact with other users, you need to confirm your email address',
                    'data'=>null
                ];

                return $response;
            }

           

			if(isset($_FILES['photo'])) {
				$max_allowed_size = $codes->MAX_FILE_SIZE * 1024;
				/* check & create uploads dir */
				$folder = 'photos';
				$depth = '../../../';
				if(!file_exists($depth.$codes->UPLOADS_DIRECTORY.'/'.$folder)) {
					@mkdir($depth.$codes->UPLOADS_DIRECTORY.'/'.$folder, 0777, true);
				}
				if(!file_exists($depth.$codes->UPLOADS_DIRECTORY.'/'.$folder.'/' . date('Y'))) {
					@mkdir($depth.$codes->UPLOADS_DIRECTORY.'/'.$folder.'/' . date('Y'), 0777, true);
				}
				if(!file_exists($codes->UPLOADS_DIRECTORY.'/'.$folder.'/' . date('Y') . '/' . date('m'))) {
					@mkdir($depth.$codes->UPLOADS_DIRECTORY.'/'.$folder.'/' . date('Y') . '/' . date('m'), 0777, true);
				}
				// valid inputs
				if(!isset($_FILES["photo"]) || $_FILES["photo"]["error"] != UPLOAD_ERR_OK) {
                    $response = [
                        'status'=>$codes->RC_ERROR,
                        'message'=>"Something wrong with upload! Is 'upload_max_filesize' set correctly?",
                        'data'=>null
                    ];
    
                    return $response;
				}

				// check file size
				if($_FILES["photo"]["size"] > $max_allowed_size) {
                    $response = [
                        'status'=>$codes->RC_ERROR,
                        'message'=>"The file size is so big",
                        'data'=>null
                    ];
    
                    return $response;
				}
				
				/* prepare new file name */
				$directory = $folder.'/'. date('Y') . '/' . date('m') . '/';
				$prefix = $codes->UPLOADES_PREFIX.'_'.md5(time()*rand(1, 9999));
				$image = new Image($_FILES["photo"]["tmp_name"]);
				$image_tmp_name = $directory.$prefix.'_tmp'.$image->_img_ext;
				$image_new_name = $directory.$prefix.$image->_img_ext;
				$path_tmp = $depth.$codes->UPLOADS_DIRECTORY.'/'.$image_tmp_name;
				$path_new = $depth.$codes->UPLOADS_DIRECTORY.'/'.$image_new_name;

				/* check if the file uploaded successfully */
				if(!@move_uploaded_file($_FILES['photo']['tmp_name'], $path_tmp)) {
                    $response = [
                        'status'=>$codes->RC_ERROR,
                        'message'=>"Sorry, can not upload the photo",
                        'data'=>null
                    ];
    
                    return $response;
				}
				
				/* save the new image */
				$resize =  true;
				$image->save($path_new, $path_tmp, $resize);
               
				/* delete the tmp image */
				unlink($path_tmp);

				/* upload to amazon s3 */
				if($codes->S3_ENABLED) {
					aws_s3_upload($path_new, $image_new_name);
				}
				
                /* return */
				$photo = $image_new_name;
			}
			
			$comment_array = array();
			$comment_array['handle'] = $input['handle'];
			$comment_array['id'] = $input['id'];
			if(isset($input['message'])){
				$comment_array['message'] = $input['message'];
			}


			if( isset($photo) ){
				$comment_array['photo'] = $photo;
			}
			
			$comment = $user->comment($comment_array);
			
			if($comment){
				$response['status'] = 1;
			    $response['message'] = '';
			    $response['data'] = $comment;
			}

            return $response;            
        }catch(\Exception $e){
            print_r($e);
            Log::debug($e);
            throw new Exception($e);
        }
    }


      /**
     * 
     */
    public function notifications($input)
    {
        Log::debug("Started ... notifications Service : ");
        try{
            $conn = ConnectionManager::get('default');
            $codes = new Codes;
            extract($input);

            if(!isset($input['user_id']) || empty($input['user_id'])){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'user id is required',
                    'data'=>null
                ];

                return $response;
            }

            if(!isset($input['offset']) || $input['offset'] == ""){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'offset is required',
                    'data'=>null
                ];

                return $response;
            }

            if(!isset($input['last_notification_id']) || $input['last_notification_id'] == ""){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'Last notification id is required',
                    'data'=>null
                ];

                return $response;
            }

         

            $user = new UserModel($input['user_id']);

            $data = $user->get_notifications($input['offset'], $last_notification_id);
            $response['status'] = 1;
            $response['message'] = 'notifications';
            $response['data'] = $data;

            return $response;            
        }catch(\Exception $e){
            print_r($e);
            Log::debug($e);
            throw new Exception($e);
        }
    }



    public function share($input)
    {
        Log::debug("Started ... notifications Service : ");
        try{
            $conn = ConnectionManager::get('default');
            $codes = new Codes;
            extract($input);

            if(!isset($input['user_id']) || empty($input['user_id'])){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'user id is required',
                    'data'=>null
                ];

                return $response;
            }

            if(!isset($input['type']) || $input['type'] == ""){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'type is required',
                    'data'=>null
                ];

                return $response;
            }

            if(!isset($input['content_id']) || $input['content_id'] == ""){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'content_id id is required',
                    'data'=>null
                ];

                return $response;
            }

         

            $user = new PostsDao;

            $data = $user->share($input['type'], $input['content_id'], $input['user_id']);
            //return $data;
            $response['status'] = $data['status'];
            $response['message'] = $data['msg'];
            $response['data'] = null;

            return $response;            
        }catch(\Exception $e){
            print_r($e);
            Log::debug($e);
            throw new Exception($e);
        }
    }

       /**
     * 
     */
    public function connect($input)
    {
        Log::debug("Started ... connect Service : ");
        try{
            $conn = ConnectionManager::get('default');
            $codes = new Codes;
            extract($input);

            // valid inputs
            $valid = array('block', 'unblock', 'friend-accept', 'friend-decline', 'friend-add', 'friend-cancel', 'friend-remove', 'follow', 'unfollow', 'like', 'unlike', 'attend','unattend','session_join','session_unjoin', 'session_thanks','interest_follow','interest_unfollow','follow_session_interest','question_notify','remove_question_notify','boost', 'unboost', 'join', 'leave','session_block','session_unblock', 'delete_session','notify_me_venue','remove_notify_me_venue', 'make_primary_phone', 'make_primary_email','payment','organizer_follow','organizer_unfollow');
            
            if(isset($input['connect_type']) && !in_array($input['connect_type'], $valid)) {
                $response['status'] = 0;
                $response['message'] = "invalid parameters.";
                $response['data'] = null;

                return $response;
            }

            if(!isset($input['user_id']) || empty($input['user_id'])){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'user id is required',
                    'data'=>null
                ];

                return $response;
            }

            if (isset($input['connect_type']) && $input['connect_type'] == 'friend-add' && !is_array($input['id']) ) {
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'id must be an array',
                    'data'=>null
                ];

                return $response;
			}

            $user = new UserModel($input['user_id']);

            if(isset($input['connect_type']) && $input['connect_type'] != 'make_primary_email'){
                if (!isset($input['id'])){
                    $response = [
                        'status'=>$codes->RC_ERROR,
                        'message'=>'id required',
                        'data'=>null
                    ];
    
                    return $response;
                }
				$user->connect($input['connect_type'], $input['id']);
				$response['status'] = 1;
				$response['message'] = 'success';
				$response['data'] = array('user_id'=> $input['user_id']);

                return $response;    
			} else {

                if (!isset($input['user_email']) || (isset($input['user_email']) && $input['user_email'] == '')) {
                    $response = [
                        'status'=>$codes->RC_ERROR,
                        'message'=>'user_email must be required in array format',
                        'data'=>null
                    ];
    
                    return $response;
				}
				
                $user->connect($input['connect_type'], $input['user_email']);

                $response['status'] = 1;
                $response['message'] = 'success';
                $response['data'] = array('user_id'=>$input['user_id']);

                return $response;
            }

        }catch(\Exception $e){
           // Log::debug($e);
            throw new Exception($e);
        }
    }

 /**
     * 
     */
    public function sendInterestRequest($input)
    {
        Log::debug("Started ... sendInterestRequest Service : ");
        try{
            $conn = ConnectionManager::get('default');
            $codes = new Codes;
            extract($input);

            if(!isset($input['user_id']) || empty($input['user_id'])){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'user id is required',
                    'data'=>null
                ];

                return $response;
            }

            $user = new UserModel($input['user_id']);

            if($codes->ACTIVATION_ENABLED || !$user->_data['user_activated']){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'Before you can interact with other users, you need to confirm your email address',
                    'data'=>null
                ];

                return $response;
            }

            if(!isset($input['interest_name']) || empty($input['interest_name'])){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'interest name is required',
                    'data'=>null
                ];

                return $response;
            }
            if(!isset($input['parent_interest_name']) || empty($input['parent_interest_name'])){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'parent interest name is require',
                    'data'=>null
                ];

                return $response;
            }
            
            if(!isset($input['remarks'])){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'remarks is required',
                    'data'=>null
                ];

                return $response;
            }

            
            $dateUtils = new DateUtils;
            $date = $dateUtils->getCurrentDate();

            $sql = sprintf("INSERT INTO `interest_request_mst` (created_by, interest, parent_interest, remarks, added_on, modified_on) VALUES ('%s', '%s', '%s', '%s', '%s', '%s')", $user->_data['user_id'], $input['interest_name'], $input['parent_interest_name'], $input['remarks'], $date, $date);

            Log::debug("SQL : ".$sql);

            $conn->execute($sql);
            
            $response['status'] = 1;
            $response['message'] = 'Interest Request Submitted Successfully';
            $response['data'] = array();

            return $response;            
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }    
    

      /**
     * 
     */
    public function updateToken($input)
    {
        Log::debug("Started ... updateToken Service : ");
        try{
            $conn = ConnectionManager::get('default');
            $codes = new Codes;
            extract($input);

            if(!isset($input['user_id']) || empty($input['user_id'])){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'user id is required',
                    'data'=>null
                ];

                return $response;
            }

          
            if(!isset($input['device_id']) || empty($input['device_id'])){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'Device id is required',
                    'data'=>null
                ];

                return $response;
            }

            if(!isset($input['device_type']) || !in_array($input['device_type'],array('A','I'))){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'Please enter a valid device type',
                    'data'=>null
                ];

                return $response;
            }

            if(!isset($input['device_token']) || empty($input['device_token'])){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'Device Token is required',
                    'data'=>null
                ];

                return $response;
            }

            $user = new UserModel($input['user_id']);

            $data = $user->update_token($input);
            $response['status'] = 1;
            $response['message'] = 'Device token updated successfully.';
            $response['data'] = array();

            return $response;            
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }

    /**
     * 
     */
    public function getArticleComments($input)
    {
        Log::debug("Started ... getArticleComments Service : ");
        try{
            $conn = ConnectionManager::get('default');
            $codes = new Codes;
            extract($input);

            if(!isset($input['user_id']) || empty($input['user_id'])){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'user id is required',
                    'data'=>null
                ];

                return $response;
            }

          
            if(!isset($input['article_id']) || !is_numeric($input['article_id'])){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'Article id is required',
                    'data'=>null
                ];

                return $response;
            }

            if(!isset($input['comment_id']) || !is_numeric($input['comment_id'])){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'comment id is required',
                    'data'=>null
                ];

                return $response;
            }


            if(!isset($input['target']) || !is_numeric($input['target']) || empty($input['target'])){
                $target = 0;
            }
            $od = 'desc';
            if(!isset($input['order']) || empty($input['order'])){
                $od = 'desc';
            }else{
                if($input['order'] == 'ASC'){
                    $od= 'asc';
                }else{
                    $od  = 'desc';
                }
            }





            //$user = new UserModel($input['user_id']);
            $ad = new ArticlesDao;
            //$target=(isset($input['target']))?$input['target']:0;
            if(isset($input['type']) and $input['type'] == "reply"){
                if(!isset($input['reply_id']) || !is_numeric($input['reply_id'])){
                    $response = [
                        'status'=>$codes->RC_ERROR,
                        'message'=>'comment id is required',
                        'data'=>null
                    ];

                    return $response;
                }
                $article_data = $ad->getArticleDiscussionsReplies($input['user_id'], $input['article_id'], $input['comment_id'],$target , $input["reply_id"] , $od);
            }else
                $article_data = $ad->getArticleDiscussions($input['user_id'], $input['article_id'], $input['comment_id'],$target , $od);
            
            $response['status'] = 1;
            $response['message'] = 'Articles Comment';
            $response['data'] = $article_data;
            

            return $response;            
        }catch(\Exception $e){
            print_r($e);
            Log::debug($e);
            throw new Exception($e);
        }
    }

    /**
     * 
     */
    public function getArticleCommentReply($input)
    {
        Log::debug("Started ... getArticleCommentReply Service : ");
        try{
            $conn = ConnectionManager::get('default');
            $codes = new Codes;
            extract($input);

           
          
            if(!isset($input['article_discussion_id']) || empty($input['article_discussion_id'])){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'Article discussion id is required',
                    'data'=>null
                ];

                return $response;
			}

            if(!isset($input['comment_id']) || !is_numeric($input['comment_id'])){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'comment id is required',
                    'data'=>null
                ];

                return $response;
			}

			if(!isset($input['order']) || empty($input['order'])){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'order field is required',
                    'data'=>null
                ];

                return $response;
			}

            $valid_orders = array("next", "prev");
			if ( !isset($input['order']) || !in_array( $input['order'], $valid_orders) ) {
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'Invalid value of the field: order',
                    'data'=>null
                ];

                return $response;
			}



			$user = new UserModel($input['user_id']);

            $target=(isset($input['target']))?$input['target']:0;
		   	$article_data = $user->get_article_discussions_reply_next_prev($input['article_discussion_id'], $input['comment_id'], $input['order'],$target);
		  	
			$response['status'] = 1;
			$response['message'] = 'Articles Comment Reply';
			$response['data'] = $article_data;

            return $response;            
        }catch(\Exception $e){
            print_r($e);
            Log::debug($e);
            throw new Exception($e);
        }
    }

     /**
     * 
     */
    /*public function allLikes($input)
    {
        Log::debug("Started ... allLikes Service : ");
        try{
            $conn = ConnectionManager::get('default');
            $codes = new Codes;
            extract($input);

           
          
            if(!isset($input['user_id']) || empty($input['user_id'])){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'user_id id is required',
                    'data'=>null
                ];

                return $response;
			}

            if(!isset($input['id']) || !is_numeric($input['id'])){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'id is required',
                    'data'=>null
                ];

                return $response;
			}

			if(!isset($input['reaction']) || empty($input['reaction'])){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'Reaction type is required',
                    'data'=>null
                ];

                return $response;
			}

            if(!isset($input['offset']) || $input['offset'] == ""){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'Offset is required',
                    'data'=>null
                ];

                return $response;
			}

            
			$user = new UserModel($input['user_id']);

            $id = $input['id'];
            $offset = $input['offset'];
            
            $return = array();
			
			$return = $user->alllikes($input['reaction'],$id,$offset);
			
            
            $response['status'] = 1;
			$response['message'] = 'Done';
			$response['data'] = $return;

            return $response;            
        }catch(\Exception $e){
            //print_r($e);
            Log::debug($e);
            throw new Exception($e);
        }
    }*/

    public function allLikes($input)
    {
        Log::debug("Started ... allLikes Service : ");
        try{
            $conn = ConnectionManager::get('default');
            $codes = new Codes;
            extract($input);

           
          
            if(!isset($input['userid']) || empty($input['userid'])){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'userid id is required',
                    'data'=>null
                ];

                return $response;
            }

            if(!isset($input['type']) || $input['type'] == ''){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'type is required',
                    'data'=>null
                ];

                return $response;
            }

            if(!isset($input['reaction_type']) || $input['reaction_type'] == ''){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'reaction_type is required',
                    'data'=>null
                ];

                return $response;
            }

            if(!isset($input['comment_id']) || empty($input['comment_id'])){
                $input['comment_id'] = 0;
            }

            if(!isset($input['lastid']) || empty($input['lastid'])){
                $input['lastid'] = 0;
            }

            if(!isset($input['content_id']) || $input['content_id'] == ""){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'content_id required',
                    'data'=>null
                ];

                return $response;
            }

            
            $pd = new PostsDao;
            extract($input);
            $return = $pd->alllikes($type , $userid , $reaction_type , $content_id , $comment_id , $lastid);
            
            $response['status'] = 1;
            $response['message'] = 'Done';
            $response['data'] = $return;

            return $response;            
        }catch(\Exception $e){
            //print_r($e);
            Log::debug($e);
            throw new Exception($e);
        }
    }

    public function search($input)
    {
        Log::debug("Started ... search Service : ");
        try{
            $conn = ConnectionManager::get('default');
            $codes = new Codes;
            extract($input);

           
          
            if(!isset($input['query']) || empty($input['query'])){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'query is required',
                    'data'=>null
                ];

                return $response;
			}

            $search_type_allowed = ['all','posts','users','articles','venues','sessions','qa','interests','poll','organization','college'];
            if(!isset($input['search_type']) || !in_array($input['search_type'], $search_type_allowed)){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'please select a valid search type..',
                    'data'=>null
                ];

                return $response;
			}

            
			if(isset($user_id) && !empty($user_id)) {
                $user = new UserModel($user_id);
            } else {
                $user = new UserModel();
            }

            if($search_type=='sessions' && !isset($last_session_id))
            {
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=> 'last_session_id parameter is required.',
                    'data'=>null
                ];

                return $response;
            }
            else
            {
                $last_session_id=0;
            }

            $results = $user->search($query,$search_type,$offset,$last_session_id);
			//pr($posts);die;
			
			if($results) {
				/* assign variables */
				$response['status'] = 1;
			    $response['message'] = '';
			    $response['data'] = $results;
			} else{
				$response['status'] = 0;
			    $response['message'] = 'No Posts found';
			    $response['data'] = array();
			}

            return $response;            
        }catch(\Exception $e){
            print_r($e);
            Log::debug($e);
            throw new Exception($e);
        }
    }

    public function myActivity($input)
    {
        Log::debug("Started ... myActivity Service : ");
        try{
            $conn = ConnectionManager::get('default');
            $codes = new Codes;
            extract($input);

           
          
            if(!isset($input['user_id']) || empty($input['user_id'])){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'user_id id is required',
                    'data'=>null
                ];

                return $response;
			}

            if(!isset($input['target_user_id']) || empty($input['target_user_id'])){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'Please fill target_user_id.',
                    'data'=>null
                ];

                return $response;
			}

            
			$user = new UserModel($input['user_id']);

            $posts = $user->myactivity(array('offset' => $input['offset'],'id'=>@$input['id'],'target_user_id'=>$input['target_user_id'],'last_post_time'=>$input['last_post_time']));
			//pr($posts);die;
			
			if($posts) {
				/* assign variables */
				$response['status'] = 1;
			    $response['message'] = '';
			    $response['data'] = $posts;
			} else{
				$response['status'] = 0;
			    $response['message'] = 'No Posts found';
			    $response['data'] = array();
			}

            return $response;            
        }catch(\Exception $e){
            print_r($e);
            Log::debug($e);
            throw new Exception($e);
        }
    }


    public function getPosts($input)
    {
        Log::debug("Started ... getPosts Service : ");
        try{
            $conn = ConnectionManager::get('default');
            $codes = new Codes;
            extract($input);

           
          
            if(!isset($input['user_id']) || empty($input['user_id'])){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'user_id id is required',
                    'data'=>null
                ];

                return $response;
			}

            $codes = new Codes;
        $conn = ConnectionManager::get('default');
            
			$user = new UserModel($input['user_id']);

            if(!isset($input['id'])){
                $input['id'] = "";
            }

            if(!isset($input['offset'])){
                $input['offset'] = "";
            }

            $posts = $user->get_posts( array('get' => $input['get'], 'filter' => $input['filter'], 'offset' => $input['offset'],'id'=> $input['id'],'last_score'=>$input['last_score']));
			
            if(isset($posts['status'])){
                return $posts['status'];
            }
            //pr($posts);die;
			$post_relevancy = $conn->execute(sprintf("SELECT * FROM `post_relevancy` where id = '1'"));
			$post_relevancy_row = $post_relevancy->fetch("assoc");
			//pr($post_relevancy_row);die;
			$valid_post_types = ['article','session','question','session_attend','article_shared','session_shared','question_shared'];
				
			$attack = array();
			foreach ($posts as $key => $row) {
				if($row['score'] == ''){
					if(in_array($row['post_type'],$valid_post_types))
						$post_type_relevancy = $post_relevancy_row[$row['post_type']];
					else
						$post_type_relevancy = $post_relevancy_row['profile_content'];
					$minutes = ( strtotime($date) - strtotime($row['time']) ) / 60;
					$row['score'] = ceil($minutes*$post_type_relevancy);
				}	
				$attack[$key]  = $row['score'];
			}
			//pr($post_relevancy_row);die;
			$post_polls = $conn->execute(sprintf("SELECT *,sum(sp.votes) total_votes FROM users_sessions_polls_options usp,sessions_polls sp where usp.user_id= '%s' AND usp.session_poll_id = sp.sessions_poll_id group by usp.user_id", $input['user_id']));
			$i1 = 0;
			while($post_poll = $post_polls->fetch("assoc")){
				$posts[$i1]['total_votes'] = $post_poll['total_votes'];
				$i1++;
			}
			
			if($posts) {
				/* assign variables */
				$response['status'] = 1;
			    $response['message'] = '';
			    $response['data'] = $posts;
			} else{
				$response['status'] = 0;
			    $response['message'] = 'No Posts found';
			    $response['data'] = array();
			}

            return $response;            
        }catch(\Exception $e){
            print_r($e);
            Log::debug($e);
            throw new Exception($e);
        }
    }

     /**
     * 
     */
    public function articleComment($input)
    {
        Log::debug("Started ... articleComment Service : ");
        try{
            $conn = ConnectionManager::get('default');
            $codes = new Codes;
            extract($input);

            if(!isset($input['user_id']) || empty($input['user_id'])){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'user id is required',
                    'data'=>null
                ];

                return $response;
            }

          
            if(!isset($input['article_id']) || !is_numeric($input['article_id'])){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'Article id is required',
                    'data'=>null
                ];

                return $response;
			}
			if(!isset($input['message']) || empty($input['message'])){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'message is required',
                    'data'=>null
                ];

                return $response;
			}
            if(!isset($input['parent_id']) || empty($input['parent_id'])){
                $input['parent_id'] = '0';
            }
            if(!isset($input['temp_id']) || empty($input['temp_id'])){
                $tempid = '0';
            }else{$tempid = $input['temp_id'];}
			$user = new UserModel($input['user_id']);

            $dateUtils = new DateUtils;
            $date = $dateUtils->getCurrentDate();
			
			// check user activated
			if($codes->ACTIVATION_ENABLED && !$user->_data['user_activated']) {
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'Before you can interact with other users, you need to confirm your email address',
                    'data'=>null
                ];

                return $response;
			}
			$data['id'] = $input['article_id'];
			$data['discussion_post'] = $input['message'];
            $data['parent_id'] = $input['parent_id'];
			
			$article = $conn->execute(sprintf("SELECT * FROM `articles` WHERE articles_id = %s", $input['article_id']));
			if($article->count() == 0){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'Invalid Article id',
                    'data'=>null
                ];

                return $response;
			}


			$article_data = $article->fetch('assoc');
			$data['created_by'] = $article_data['created_by'];
            $comments=$input;
			$comments['comment_id'] = $user->connect('add_article_discussion',$data);
            $comments['time']=$date;
			/* $comment_array = array();
			$comment_array['id'] = $input['article_id'];
			$comment_array['message'] = $input['message'];
						
			$comment =  $conn->execute(sprintf("INSERT INTO `articles_discussion` (articles_id, post,user_id,parent_comment_id,status,added_on,modified_on) VALUES (%s, %s, %s, 0, 1, %s, %s)", secure($input['article_id'], 'int'), secure($input['message']), secure($input['user_id'], 'int'), secure($date, 'datetime'), secure($date, 'datetime'))) or _apiError(SQL_ERROR_THROWEN);
			
			$comments['comment_id'] = $db->insert_id; */

			/* $to_user_id = $input['user_id'];
			$message = "New Comment";
			$type = 'comment';
			$data = array(
				'comment_id' => $comments['comment_id'],
				'post_id' => $input['article_id'],
			);

			_sendPushNotificationMessage($to_user_id, $message, $type, $data); */
			$comments['temp_id'] = $tempid;
			$response['status'] = 1;
			$response['message'] = 'Added';
			$response['data'] = $comments;

            return $response;            
        }catch(\Exception $e){
            print_r($e);
            Log::debug($e);
            throw new Exception($e);
        }
    }

    public function getArticleOld($input)
    {
        Log::debug("Started ... getArticle Service : ");
        try{
            $conn = ConnectionManager::get('default');
            $codes = new Codes;
            extract($input);

            if(!isset($input['user_id']) || empty($input['user_id'])){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'user id is required',
                    'data'=>null
                ];

                return $response;
            }

          
            if(!isset($input['article_id']) || !is_numeric($input['article_id'])){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'Article id is required',
                    'data'=>null
                ];

                return $response;
			}
			
			$user = new UserModel($input['user_id']);

            $dateUtils = new DateUtils;
            $date = $dateUtils->getCurrentDate();
			

            $get_article = $conn->execute(sprintf("SELECT * FROM `articles` WHERE `articles_id` = %s AND `status` = '1' ", $input['article_id']));
            if ($get_article->count() == 0) {
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'Article not found',
                    'data'=>null
                ];

                return $response;
            }

            $article_data = $get_article->fetch("assoc");
            $article_data['title'] = $user->parse_api(strip_tags($article_data['title']));
            $article_data['description'] = $user->parse_api($article_data['description']);
			//print_r($article_data);
			$article_data['edit'] = false;
			if($article_data['created_by'] == $input['user_id']){
				$article_data['edit'] = true;
			}

            $article_data['created_by'] = $user->get_user_by_id($article_data['created_by']);
            $article_data['created_by'] = $article_data['created_by'][0];

			if ( !empty($article_data['img_dimensions']) ) {
				$article_data['img_dimensions'] = $article_data['img_dimensions'];
				list($height,$width) = explode("X",$article_data['img_dimensions'],2);
				$article_data['img_width'] = $width;
				$article_data['img_height'] = $height;
			} else {
				$article_data['img_width'] = '';
				$article_data['img_height'] = '';
			}
            
            /* get page picture */
            $article_data['page_picture_default'] = ($article_data['cover_photo']) ? false : true;
            $article_data['page_picture'] = UserModel::get_picture($article_data['cover_photo'], 'page');

            $article_data['cover_photo'] = $codes->SYSTEM_URL .'/content/uploads/'.$article_data['cover_photo'];

            /* get posts */
            $posts = $user->get_posts(array('get' => 'posts_page', 'id' => $article_data['articles_id']));
    

            /* GET COUNT OF ALL */
            $article_data['discussions_cnt'] = $user->get_article_discussions_cnt($article_data['articles_id']);
			
			$get_post = $conn->execute(sprintf("select post_id FROM posts_articles WHERE article_id = %s", $article_data['articles_id']));

			$post = $get_post->fetch("assoc");
			/* $post_data = $conn->execute(sprintf("select likes,shares FROM posts WHERE post_id = %s", secure($post['post_id'], 'int'))) or _apiError(SQL_ERROR_THROWEN);
			$post_likes_shares = $post_data->fetch("assoc");
			$article_data['likes'] = $post_likes_shares['likes']; */
			
			$article_data['i_like'] = false;
			$check_liked = $conn->execute(sprintf("select * FROM articles_likes WHERE articles_id = %s AND user_id = %s", $article_data['articles_id'], $user->_data['user_id']));
			if ($check_liked->count() > 0) {
				$article_data['i_like'] = true;
			}
			
			$article_likes = $conn->execute( sprintf( "select * FROM articles_likes WHERE articles_id = %s", $article_data['articles_id']));
			
			$article_data['likes'] = $article_likes->count();
			
			$article_data['liked_users'] = [];
			if($article_likes->count()){
				while($likers = $article_likes->fetch("assoc")){
					if(!$user->blocked($likers['user_id'])){
						$article_data['liked_users'][] = $user->get_user_by_id($likers['user_id'])[0];
					}
				}
			}
			$article_data['likes'] = count($article_data['liked_users']);
			
			//$article_data['shares'] = $post_likes_shares['shares'];
			
			/* $get_user_like = $conn->execute(sprintf('SELECT * FROM posts_likes WHERE post_id = %s AND user_id = %s', secure($post['post_id'], 'int'), secure($input['user_id'], 'int')));
			$article_data['i_like'] = false;
			if($get_user_likecount()){
				$article_data['i_like'] = true;
			} */
			
			$post_save_data = $conn->execute(sprintf("SELECT count(*) AS cnt FROM `posts_saved` WHERE post_id = %s AND user_id = %s", $post['post_id'], $input['user_id']));
            $post_save = $post_save_data->fetch("assoc");
            $article_data['i_save'] =  $post_save['cnt'];
            
			$get_posts = $conn->execute(sprintf("SELECT posts.*, users.user_name, users.user_fullname, users.user_gender, users.user_picture, users.user_picture_id, users.user_cover_id, users.user_verified, users.user_subscribed, users.user_pinned_post, pages.*, groups.group_name, groups.group_picture_id, groups.group_cover_id, groups.group_title, groups.group_admin, groups.group_pinned_post FROM posts LEFT JOIN users ON posts.user_id = users.user_id AND posts.user_type = 'user' LEFT JOIN pages ON posts.user_id = pages.page_id AND posts.user_type = 'page' LEFT JOIN groups ON posts.in_group = '1' AND posts.group_id = groups.group_id WHERE NOT (users.user_name <=> NULL AND pages.page_name <=> NULL) AND posts.post_id = %s", $post['post_id']));
			$postss = $get_posts->fetch("assoc");
			if ($postss['user_type'] == "user") {
				 $article_data['i_pin '] = $postss['pinned'] = ((!$postss['in_group'] && $postss['post_id'] == $postss['user_pinned_post']) || ($postss['in_group'] && $post['post_id'] == $postss['group_pinned_post'])) ? true : false;
			} else {
				$article_data['i_pin '] =  $postss['pinned'] = ($postss['post_id'] == $postss['page_pinned_post']) ? true : false;
			}
			
            $i = 0;
            $articleInterestArr = array();
            $get_article_interest = $conn->execute(sprintf("SELECT *, ( SELECT text FROM `interest_mst` WHERE interest_id = si.interest ) AS article_name FROM `articles_interest` AS si WHERE si.`articles_id` = %s AND `joined`!='1'", $article_data['articles_id']));
            while ($article_interest = $get_article_interest->fetch("assoc")) {
                $articleInterestArr[$i]['interest_id'] = $article_interest['interest'];
                $articleInterestArr[$i]['article_name'] = $article_interest['article_name'];

                $i++;
            }
		   $article_data['intrests'] = $articleInterestArr;
		  if($article_data){
				$response['status'] = 1;
			    $response['message'] = '';
			    $response['data'] = $article_data;
			}
			
            return $response;            
        }catch(\Exception $e){
            print_r($e);
            Log::debug($e);
            throw new Exception($e);
        }
    }

    /**
     * 
     */
    public function articleCommentReply($input)
    {
        Log::debug("Started ... articleComment Service : ");
        try{
            $conn = ConnectionManager::get('default');
            $codes = new Codes;
            extract($input);

            if(!isset($input['user_id']) || empty($input['user_id'])){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'user id is required',
                    'data'=>null
                ];

                return $response;
            }

          
            if(!isset($input['article_id']) || !is_numeric($input['article_id'])){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'Article id is required',
                    'data'=>null
                ];

                return $response;
			}
			if(!isset($input['message']) || empty($input['message'])){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'message is required',
                    'data'=>null
                ];

                return $response;
			}
			$user = new UserModel($input['user_id']);

            $dateUtils = new DateUtils;
            $date = $dateUtils->getCurrentDate();
			
			// check user activated
			if($codes->ACTIVATION_ENABLED && !$user->_data['user_activated']) {
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'Before you can interact with other users, you need to confirm your email address',
                    'data'=>null
                ];

                return $response;
			}
			$data['id'] = $input['article_id'];
			$data['discussion_post'] = $input['message'];
            $data['article_comment_id'] = $input['article_comment_id'];
			
			$article = $conn->execute(sprintf("SELECT * FROM `articles` WHERE articles_id = %s", $input['article_id']));
			if($article->count() == 0){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'Invalid Article id',
                    'data'=>null
                ];

                return $response;
			}
			$article_data = $article->fetch('assoc');
			$data['created_by'] = $article_data['created_by'];
            $comments=$input;
			$comments['comment_id'] = $user->connect('add_article_comment_discussion',$data);
            $comments['time']=$date;
			/* $comment_array = array();
			$comment_array['id'] = $input['article_id'];
			$comment_array['message'] = $input['message'];
						
			$comment =  $conn->execute(sprintf("INSERT INTO `articles_discussion` (articles_id, post,user_id,parent_comment_id,status,added_on,modified_on) VALUES (%s, %s, %s, 0, 1, %s, %s)", secure($input['article_id'], 'int'), secure($input['message']), secure($input['user_id'], 'int'), secure($date, 'datetime'), secure($date, 'datetime'))) or _apiError(SQL_ERROR_THROWEN);
			
			$comments['comment_id'] = $db->insert_id; */

			/* $to_user_id = $input['user_id'];
			$message = "New Comment";
			$type = 'comment';
			$data = array(
				'comment_id' => $comments['comment_id'],
				'post_id' => $input['article_id'],
			);

			_sendPushNotificationMessage($to_user_id, $message, $type, $data); */
			
			$response['status'] = 1;
			$response['message'] = 'Added';
			$response['data'] = $comments;

            return $response;            
        }catch(\Exception $e){
            print_r($e);
            Log::debug($e);
            throw new Exception($e);
        }
    }

     /**
     * 
     */
    public function articleReaction($input)
    {
        Log::debug("Started ... articleReaction Service : ");
        try{
            $conn = ConnectionManager::get('default');
            $codes = new Codes;
            extract($input);

            if(!isset($input['user_id']) || empty($input['user_id'])){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'user id is required',
                    'data'=>null
                ];

                return $response;
            }

          
            if(!isset($input['article_id']) || !is_numeric($input['article_id'])){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'Article id is required',
                    'data'=>null
                ];

                return $response;
			}
			if(!isset($input['reaction']) || empty($input['reaction'])){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'reaction is required',
                    'data'=>null
                ];

                return $response;
			}
			$user = new UserModel($input['user_id']);
			
			$get_post = $conn->execute(sprintf("select post_id FROM posts_articles WHERE article_id = %s", $input['article_id']));
			$post = $get_post->fetch('assoc');
			$id = $post['post_id'];
			
			$return = array();

			switch ($input['reaction']) {

				case 'share':
					$return = $user->share($id);
					break;

				case 'like_article':
					$return = $user->like_article($input['article_id']);
					break;

				case 'unlike_article':
					$return =  $user->unlike_article($input['article_id']);
					break;
					
				case 'save_article':
					$return =  $user->save_post($id);
					break;	
                
				case 'unsave_article':
					$return =  $user->unsave_post($id);
					break;
					
				case 'boost_article':
					$return =  $user->boost_post($id);
					break;

				case 'unboost_article':
					$return =  $user->unboost_post($id);
					break;

                case 'pin_article':
					$return =  $user->pin_post($id);
					break;

				case 'unpin_article':
					$return =  $user->unpin_post($id);
					break;	

                case 'delete_article':
					$return = $user->delete_article($input['article_id']);
					break;
				default:
                        $response = [
                            'status'=>$codes->RC_ERROR,
                            'message'=>'reaction type is required',
                            'data'=>null
                        ];

                        return $response;
					break;
			}
			
			
			
				$response['status'] = 1;
			    $response['message'] = 'Done';
			    $response['data'] = array();

            return $response;            
        }catch(\Exception $e){
            print_r($e);
            Log::debug($e);
            throw new Exception($e);
        }
    }

     /**
     * 
     */
    public function editType($input)
    {
        Log::debug("Started ... editType Service : ");
        try{
            $conn = ConnectionManager::get('default');
            $codes = new Codes;
            extract($input);

            if(!isset($input['user_id']) || empty($input['user_id'])){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'user id is required',
                    'data'=>null
                ];

                return $response;
            }

            $user = new UserModel($input['user_id']);
			$return = array();

			switch ($input['type']) {

				case 'post':
					$return = $user->edit_post($input['id'], $input['message']);
                    if(isset($return['error'])){
                        return $return;
                    }
					break;
                case 'posttitle':
					$return = $user->edit_posttitle($input['id'], $input['message']);
                    if(isset($return['error'])){
                        return $return;
                    }
					break;

				case 'article':
					if(!isset($input['id']) || empty($input['id'])){
                        $response = [
                            'status'=>$codes->RC_ERROR,
                            'message'=>'Article id is required',
                            'data'=>null
                        ];
        
                        return $response;
					}
					
					if(!isset($input['message']) || empty($input['message'])){
                        $response = [
                            'status'=>$codes->RC_ERROR,
                            'message'=>'Article Title is required',
                            'data'=>null
                        ];
        
                        return $response;
					}
					
					$get_articles = $conn->execute(sprintf("select * FROM articles WHERE articles_id = '%s'", $input['id']));
					if($get_articles->count() == 0){
                        $response = [
                            'status'=>$codes->RC_ERROR,
                            'message'=>'Invalid Article',
                            'data'=>null
                        ];
        
                        return $response;
					}
					$articles = $get_articles->fetch('assoc');
					
					if(!isset($input['description']) || empty($input['description'])){
                        $response = [
                            'status'=>$codes->RC_ERROR,
                            'message'=>'Description is required',
                            'data'=>null
                        ];
        
                        return $response;
					}
					
					$max_allowed_size =$codes->MAX_FILE_SIZE * 1024;
					
					/* check & create uploads dir */
					$folder = 'photos';
					$depth = '../../../';
					if(!file_exists($depth.$codes->UPLOADS_DIRECTORY.'/'.$folder)) {
						@mkdir($depth.$codes->UPLOADS_DIRECTORY.'/'.$folder, 0777, true);
					}
					if(!file_exists($depth.$codes->UPLOADS_DIRECTORY.'/'.$folder.'/' . date('Y'))) {
						@mkdir($depth.$codes->UPLOADS_DIRECTORY.'/'.$folder.'/' . date('Y'), 0777, true);
					}
					if(!file_exists($codes->UPLOADS_DIRECTORY.'/'.$folder.'/' . date('Y') . '/' . date('m'))) {
						@mkdir($depth.$codes->UPLOADS_DIRECTORY.'/'.$folder.'/' . date('Y') . '/' . date('m'), 0777, true);
					}
					
					$photo = $articles['cover_photo'];

					if ( isset($_FILES) && isset($_FILES["photo"]) ) {
						// check file size
						if($_FILES["photo"]["size"] > $max_allowed_size) {
							$response = [
                                'status'=>$codes->RC_ERROR,
                                'message'=>'The file size is so big',
                                'data'=>null
                            ];
            
                            return $response;
						}
						/* prepare new file name */
						$directory = $folder.'/'. date('Y') . '/' . date('m') . '/';
						$prefix = $codes->UPLOADES_PREFIX.'_'.md5(time()*rand(1, 9999));
						$image = new Image($_FILES["photo"]["tmp_name"]);
						$image_tmp_name = $directory.$prefix.'_tmp'.$image->_img_ext;
						$image_new_name = $directory.$prefix.$image->_img_ext;
						$path_tmp = $depth.$codes->UPLOADS_DIRECTORY.'/'.$image_tmp_name;
						$path_new = $depth.$codes->UPLOADS_DIRECTORY.'/'.$image_new_name;

						/* check if the file uploaded successfully */
						if(!@move_uploaded_file($_FILES['photo']['tmp_name'], $path_tmp)) {
                            $response = [
                                'status'=>$codes->RC_ERROR,
                                'message'=>'Sorry, can not upload the photo',
                                'data'=>null
                            ];
            
                            return $response;
						}
						
						/* save the new image */
						$resize =  true;
						$image->save($path_new, $path_tmp, $resize);
					   
						/* delete the tmp image */
						unlink($path_tmp);

						/* upload to amazon s3 */
						if($codes->S3_ENABLED) {
							aws_s3_upload($path_new, $image_new_name);
						}
						
						/* return */
						$photo = $image_new_name;
						
					}
					
					$conn->execute(sprintf("UPDATE articles SET title = '%s',cover_photo = '%s',description = '%s' WHERE articles_id = '%s'", $input['message'],$photo,$description,$input['id']));
					
					$get_post = $conn->execute(sprintf("select post_id FROM posts_articles WHERE article_id = %s", $input['id']));
					if($get_post->count() > 0){
						$post = $get_post->fetch('assoc');
						
						$conn->execute(sprintf("UPDATE posts SET text = '%s' WHERE post_id = %s", $input['message'], $post['post_id']));
						
						$conn->execute(sprintf("UPDATE posts_articles SET source = '%s' WHERE article_id = %s", $photo, $input['id']));
					}
					
					$return = 1;
					break;
					
				case 'comment':

				        $max_allowed_size =$codes->MAX_FILE_SIZE * 1024;
						/* check & create uploads dir */
						$folder = 'photos';
						$depth = '../../../';
						if(!file_exists($depth.$codes->UPLOADS_DIRECTORY.'/'.$folder)) {
							@mkdir($depth.$codes->UPLOADS_DIRECTORY.'/'.$folder, 0777, true);
						}
						if(!file_exists($depth.$codes->UPLOADS_DIRECTORY.'/'.$folder.'/' . date('Y'))) {
							@mkdir($depth.$codes->UPLOADS_DIRECTORY.'/'.$folder.'/' . date('Y'), 0777, true);
						}
						if(!file_exists($codes->UPLOADS_DIRECTORY.'/'.$folder.'/' . date('Y') . '/' . date('m'))) {
							@mkdir($depth.$codes->UPLOADS_DIRECTORY.'/'.$folder.'/' . date('Y') . '/' . date('m'), 0777, true);
						}
						
						$photo = null;

						if ( isset($_FILES) && isset($_FILES["photo"]) ) {

							// valid inputs
							// if(!isset($_FILES["photo"]) || $_FILES["photo"]["error"] != UPLOAD_ERR_OK) {
							// 	throw new Exception("Something wrong with upload! Is 'upload_max_filesize' set correctly?");
							// }

							if( $_FILES["photo"]["error"] != UPLOAD_ERR_OK) {
                                $response = [
                                    'status'=>$codes->RC_ERROR,
                                    'message'=>'Something wrong with upload! Is "upload_max_filesize" set correctly?',
                                    'data'=>null
                                ];
                
                                return $response;
							}

							// check file size
							if($_FILES["photo"]["size"] > $max_allowed_size) {
								$response = [
                                    'status'=>$codes->RC_ERROR,
                                    'message'=>'The file size is so big',
                                    'data'=>null
                                ];
                
                                return $response;
							}
							
							/* prepare new file name */
							$directory = $folder.'/'. date('Y') . '/' . date('m') . '/';
							$prefix = $codes->UPLOADES_PREFIX.'_'.md5(time()*rand(1, 9999));
							$image = new Image($_FILES["photo"]["tmp_name"]);
							$image_tmp_name = $directory.$prefix.'_tmp'.$image->_img_ext;
							$image_new_name = $directory.$prefix.$image->_img_ext;
							$path_tmp = $depth.$codes->UPLOADS_DIRECTORY.'/'.$image_tmp_name;
							$path_new = $depth.$codes->UPLOADS_DIRECTORY.'/'.$image_new_name;

							/* check if the file uploaded successfully */
							if(!@move_uploaded_file($_FILES['photo']['tmp_name'], $path_tmp)) {
                                $response = [
                                    'status'=>$codes->RC_ERROR,
                                    'message'=>'Sorry, can not upload the photo',
                                    'data'=>null
                                ];
                
                                return $response;
							}
							
							/* save the new image */
							$resize =  true;
							$image->save($path_new, $path_tmp, $resize);
		                   
							/* delete the tmp image */
							unlink($path_tmp);

							/* upload to amazon s3 */
							if($codes->S3_ENABLED) {
								aws_s3_upload($path_new, $image_new_name);
							}
							
		                    /* return */
							$photo = $image_new_name;
							
						}

					$return = $user->edit_comment($input['id'], $input['message'], $photo);
					break;
                
                case 'poll':
					$return = $user->edit_poll($input['user_id'],$input['id'], $input['message'],$input['options']);
					break;

			}
			
			
			if($return){

                if(isset($return['error'])){
                    $response['status'] = 0;
			    $response['message'] = $return['error'];
			    $response['data'] = array();

                }else{
                    $response['status'] = 1;
                    $response['message'] = 'Done';
                    if($input['type']=='post')
                    {
                        $response['data'] = $return;
                    }
                    else
                    {
                        $response['data'] = array();
                    }
                }
			} else{
				$response['status'] = 0;
			    $response['message'] = 'No result found';
			    $response['data'] = array();
			}

          
            return $response;            
        }catch(\Exception $e){
            print_r($e);
            Log::debug($e);
            throw new Exception($e);
        }
    }

    public function postCommentReply($input)
    {
        Log::debug("Started ... postCommentReply Service : ");
        try{
            $conn = ConnectionManager::get('default');
            $codes = new Codes;
            extract($input);

            if (!isset($input['action']) || empty($input['action'])) {
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'Action is required',
                    'data'=>null
                ];

                return $response;
            }
            switch ($input['action']) {
                case 'view':
                    if (!isset($input['parent_comment_id']) || empty($input['parent_comment_id'])) {
                        $response = [
                            'status'=>$codes->RC_ERROR,
                            'message'=>'Parent Comment Id is required',
                            'data'=>null
                        ];
        
                        return $response;
                    }
                    if(!isset($input['user_id']) || !is_numeric($input['user_id'])){
                        $response = [
                            'status'=>$codes->RC_ERROR,
                            'message'=>'User id is required',
                            'data'=>null
                        ];
        
                        return $response;
                    }
                    /* if order not set */
                    if(!isset($input['order'])) {
                        $response = [
                            'status'=>$codes->RC_ERROR,
                            'message'=>'order field is required',
                            'data'=>null
                        ];
        
                        return $response;
                    }
                    $valid_orders = array("next", "prev");
                    if ( !isset($input['order']) || !in_array( $input['order'], $valid_orders) ) {
                        $response = [
                            'status'=>$codes->RC_ERROR,
                            'message'=>'Invalid value of the field: order',
                            'data'=>null
                        ];
        
                        return $response;
                    }
                    if ( !isset($input['reply_id']) ) {
                        $response = [
                            'status'=>$codes->RC_ERROR,
                            'message'=>'Reply id is required',
                            'data'=>null
                        ];
        
                        return $response;
                    }

                    $cur_query = "SELECT * FROM posts_comments WHERE node_id = '%s' ";
                    $get_replays = $conn->execute(sprintf($cur_query, $input['parent_comment_id']));
                    if($get_replays->count()==0){
                        $response['status'] = 1;
                        $response['message'] = 'No Records Found';
                        $response['data'] = '';
                    }
                    else{
                        $user = new UserModel($input['user_id']);
                        $target=(isset($input['target']))?$input['target']:0;
                        $replies = $user->get_post_comments_replies_next_prev($input['parent_comment_id'],$input['order'],$input['reply_id'],$target);
                        
                        $response['status'] = 1;
                        $response['message'] = '';
                        $response['data'] = $replies;
                    }
                    break;
                case 'delete':
                    if (!isset($input['comment_id']) || empty($input['comment_id'])) {
                        $response = [
                            'status'=>$codes->RC_ERROR,
                            'message'=>'Comment Id is required.',
                            'data'=>null
                        ];
        
                        return $response;
                    }
                    $qpostc=$conn->execute(sprintf("select * from posts_comments where comment_id =  '%s'", $input['comment_id']));
                    $rpostc=$qpostc->fetch("assoc");
                    $post_id = $rpostc['node_id'];
                    $delete = $conn->execute(sprintf("DELETE FROM posts_comments WHERE comment_id =  '%s'", $input['comment_id']));
                    if($delete){
                        $qpost=$conn->execute(sprintf("select * from posts where post_id =  %s", $post_id));
                        $rpost=$qpost->fetch("assoc");
                        $totalcomment=$rpost['comments']-1;
                        $commentid=str_replace($input['comment_id'].',','',$rpost['comment_ids']);
                        $commentid=str_replace($input['comment_id'],'',$commentid);
                        $commentid=trim($commentid);
                        $conn->execute(sprintf("update posts set comments='%s',comment_ids='%s' where post_id =  '%s'", $totalcomment, $commentid, $post_id));
                        
                        $response['status'] = 1;
                        $response['message'] = "Post Comment Deleted Successfully.";
                    }
                    else{
                        $response['status'] = 1;
                        $response['message'] = "Post Comment Not Deleted.";
                    }
                    break;
                case 'edit':
                    if (!isset($input['comment_id']) || empty($input['comment_id'])) {
                        $response = [
                            'status'=>$codes->RC_ERROR,
                            'message'=>'Comment Id is required.',
                            'data'=>null
                        ];
        
                        return $response;
                    }
                    if (!isset($input['text']) || empty($input['text'])) {
                        $response = [
                            'status'=>$codes->RC_ERROR,
                            'message'=>'Text is required.',
                            'data'=>null
                        ];
        
                        return $response;
                    }
                    $update = $conn->execute(sprintf("UPDATE `posts_comments` SET `text` = '%s' WHERE `comment_id` =  '%s'", $input['text'], $input['comment_id']));
                    if($update){
                        $response['status'] = 1;
                        $response['message'] = "Post Comment Updated Successfully.";
                        $response['data']=$input['text'];
                    }
                    else{
                        $response['status'] = 1;
                        $response['message'] = "Post Comment Not Updated.";
                    }
                    break;
                case 'add':
                    extract($input);
                    global $date;
                    $dateUtils = new DateUtils;
            $date = $dateUtils->getCurrentDate();
                    if (!isset($text) || empty($text)) {
                        $response = [
                            'status'=>$codes->RC_ERROR,
                            'message'=>'Text is required.',
                            'data'=>null
                        ];
        
                        return $response;
                    }
                    if (!isset($node_type) || empty($node_type)) {
                        $response = [
                            'status'=>$codes->RC_ERROR,
                            'message'=>'Node Type is required.',
                            'data'=>null
                        ];
        
                        return $response;
                    }
                    if (!isset($user_id) || empty($user_id)) {
                        $response = [
                            'status'=>$codes->RC_ERROR,
                            'message'=>'user id is required.',
                            'data'=>null
                        ];
        
                        return $response;
                    }
                    if (!isset($user_type) || empty($user_type)) {
                        $response = [
                            'status'=>$codes->RC_ERROR,
                            'message'=>'User Type is required.',
                            'data'=>null
                        ];
        
                        return $response;
                    }
    
                    $parent_id = 0;
                    if(!empty($input['parent_id'])){
                        $parent_id = $input['parent_id'];
                    }
    
                    $user = new UserModel($input['user_id']);
    
                    $result = $user->add_post_comment_reply( $parent_id, $node_type, $user_type, $text);

                    if(isset($result) && isset($result['status']) && $result['status'] == 'invalid'){
                        return $result;
                    }

                    $input['id']=$result;
                    $input['time']=$date;
                    if($result){
                        $response['status'] = 1;
                        $response['message'] = "Post Comment Added Successfully.";
                        $response['data']=$input;
                    }
                    else{
                        $response['status'] = 1;
                        $response['message'] = "Post Comment Not Added.";
                    }
                    break;
                default:
                    $response['status'] = 0;
                    $response['message'] = "Invalid action";
                    $response['data'] = null;
                    break;
            }

            return $response;            
        }catch(\Exception $e){
            
            print_r($e);
            Log::debug($e);
            throw new Exception($e);
        }
    }

    public function postCommentDelete($input)
    {
        Log::debug("Started ... postCommentDelete Service : ");
        try{
            $conn = ConnectionManager::get('default');
            $codes = new Codes;
            extract($input);

            $dateUtils = new DateUtils;
            $date = $dateUtils->getCurrentDate();

            if(!isset($comment_id)){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'Comment id is required.',
                    'data'=>null
                ];

                return $response;
            }
            $post=$conn->execute(sprintf("select * from posts_comments where comment_id =  %s", $comment_id));
            $getposts=$post->fetch("assoc");
            $delete = $conn->execute(sprintf("DELETE FROM posts_comments WHERE comment_id =  %s", $comment_id));
            //$delete = 1;
            if($delete){
                $posts=$conn->execute(sprintf("select * from posts where post_id =  %s", $getposts['node_id']));
                $detail=$posts->fetch("assoc");
                $comment_ids = explode(",",$detail['comment_ids']);
                if (($key = array_search($comment_id, $comment_ids)) !== false) {
                    unset($comment_ids[$key]);
                }
                $newcomment = implode(",",$comment_ids);
                
                $newcount=$detail['comments']-1;
                
                $total_user_ids = explode(",",$detail['total_user_ids']);
                if (($key = array_search($user_id, $total_user_ids)) !== false) {
                    unset($total_user_ids[$key]);
                }
                $new_total_user_ids = implode(",",$total_user_ids);
                
                $conn->execute(sprintf("update posts set comment_ids='%s',comments='%s',total_user_ids='%s'  where post_id =  %s", $newcomment, $newcount, $new_total_user_ids, $getposts['node_id']));
                
                
                $node_url = $getposts['node_id'].'/'.$comment_id;
                
                $check1 = $conn->execute(sprintf("SELECT * FROM `notifications` WHERE `node_type` = 'post' AND `action` = 'comment' AND unique_id = '%s'", $getposts['node_id'])) ;
                
                    $data = $check1->fetch("assoc");
                    if(count(explode(",",$data['total_user_ids']))>1)
                    {
                        $total = $data['total']-1;
                        $from_userid = end($total_user_ids);
                        $new_node = $getposts['node_id'].'/'.end($comment_ids);
                        $new_reply_id = end($comment_ids);
                        
                        $conn->execute(sprintf("UPDATE notifications SET total_user_ids = '%s', total = '%s', from_user_id = '%s', time = '%s', seen = '0',node_url = '%s', notify_id = '%s' WHERE notification_id = '%s'",$new_total_user_ids ,$total, $from_userid, $date, $new_node, $new_reply_id, $data['notification_id']));
                    }
                    else
                    {
                        $conn->execute(sprintf("DELETE FROM notifications WHERE notification_id = %s", $data['notification_id']));
                    }
            /* update notifications counter -1 */
            $conn->execute(sprintf("UPDATE users SET user_live_notifications_counter = IF(user_live_notifications_counter=0,0,user_live_notifications_counter-1) WHERE user_id = %s", $user_id));
                
                $response['status'] = 1;
                $response['message'] = "Post Comment Deleted Successfully.";
            }

            return $response;            
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }

    public function getPostComments($input)
    {
        Log::debug("Started ... suggestInterestEdit Service : ");
        try{
            $conn = ConnectionManager::get('default');
            $codes = new Codes;
            extract($input);

            if(!isset($input['user_id']) || empty($input['user_id'])){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'User id is required.',
                    'data'=>null
                ];

                return $response;
            }
            if(!isset($input['post_id']) || !is_numeric($input['post_id'])){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'Post id is required.',
                    'data'=>null
                ];

                return $response;
            }
            /* if sessions_qa_id not set */
            if(!isset($input['comment_id'])) {
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'Comment id is required.',
                    'data'=>null
                ];

                return $response;
            }
            
            /* if order not valid */
            $valid_orders = array("next", "prev");
            if (!isset($input['order']) || !in_array( $input['order'], $valid_orders) ) {
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'Invalid value of the field: order',
                    'data'=>null
                ];

                return $response;
            }

            $order = $input['order'];
            $target=(isset($input['target']))?$input['target']:0;
            
            $user = new UserModel($input['user_id']);
            $comments = $user->get_comments_next_prev($input['post_id'],0,true,true,array(),$input['comment_id'],$order,$target);
            
            if($comments){
                if($comments['post']['post_id']=='')
                {
                    $response['status'] = 2;
                    $response['message'] = 'Post is deleted';
                    $response['data'] = $comments;
                }
                else
                {
                    $response['status'] = 1;
                    $response['message'] = '';
                    $response['data'] = $comments;
                }
            } else{
                $response['status'] = 0;
                $response['message'] = 'No comment found';
                $response['data'] = null;
            }

            return $response;            
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }

    public function suggestInterestEdit($input)
    {
        Log::debug("Started ... suggestInterestEdit Service : ");
        try{
            $conn = ConnectionManager::get('default');
            $codes = new Codes;
            extract($input);

            if(!isset($input['user_id']) || empty($input['user_id'])){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'user id is required',
                    'data'=>null
                ];

                return $response;
            }

          
            if(!isset($input['interest_id']) || empty($input['interest_id'])){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'interest id is required',
                    'data'=>null
                ];

                return $response;
            }

           
            $user = new UserModel($input['user_id']);
            $dateUtils = new DateUtils;
            $date = $dateUtils->getCurrentDate();

            $sql = sprintf("insert into suggest_interest_edit set interest_id = '%s',user_id='%s',remark='%s',added_on='%s'", $input['interest_id'], $input['user_id'], $input['remark'], $date);

            Log::debug("SQL : ".$sql);

            $conn->execute($sql);

            $sql="SELECT LAST_INSERT_ID()";
            $stmt = $conn->execute($sql);
            $res = $stmt->fetch();

            $did = $res[0];
        
            $response['status'] = 1;
            $response['message'] = "Success";
            $response['data'] = array('id'=>$did);

            return $response;            
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }

    public function question($input)
    {
        Log::debug("Started ... suggestInterestEdit Service : ");
        try{
            $conn = ConnectionManager::get('default');
            $codes = new Codes;
            extract($input);

            
            if(!isset($input['user_id']) || empty($input['user_id'])){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'user id is required',
                    'data'=>null
                ];

                return $response;
            }
            $user = new UserModel($input['user_id']);
            
            if(!isset($input['question_id']) || empty($input['question_id'])){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'question id is required',
                    'data'=>null
                ];

                return $response;
            }

            if(!isset($input['type']) || empty($input['type'])){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'type is required',
                    'data'=>null
                ];

                return $response;
            }

            if(!isset($input['sortby']) || empty($input['sortby'])){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'sortby is required',
                    'data'=>null
                ];

                return $response;
            }

             $target=(isset($input['target']))?$input['target']:0;
             
            switch($input['type']) {
                case "question" :
                    $get_discussion =  $conn->execute(sprintf("SELECT * FROM `sessions_qa` WHERE sessions_qa_id = '%s' AND `status` != '0' AND qa_type = 1", $input['question_id']));
                    if($get_discussion->count() == 0){
                        $response = [
                            'status'=>$codes->RC_ERROR,
                            'message'=>'Invalid Question Id',
                            'data'=>null
                        ];
        
                        return $response;
                    }
                    $qa = $get_discussion->fetch("assoc");
                    unset($qa['likes']);
                    $sessions_id = $qa['sessions_id'];
                        
                        $get_mylike_q =  $conn->execute(sprintf("SELECT * FROM `sessions_qa_likes` WHERE sessions_qa_id = '%s' AND `user_id` = %s ", $qa['sessions_qa_id'], $user->_data['user_id']));
                        $qa['i_like'] = false;
                        if ($get_mylike_q->count() > 0) {
                            $qa['i_like'] = true;
                        }
                        
                        $get_myfollow =  $conn->execute(sprintf("SELECT * FROM `sessions_qa_follows` WHERE sessions_qa_id = '%s' AND `user_id` = %s ", $qa['sessions_qa_id'], $user->_data['user_id']));
                        $qa['i_follow'] = false;
                        if ($get_myfollow->count() > 0) {
                            $qa['i_follow'] = true;
                        }
    
                        $get_total_likes_q =  $conn->execute(sprintf("SELECT count(*) AS total_likes FROM `sessions_qa_likes` WHERE sessions_qa_id = %s ", $qa['sessions_qa_id']));
    
                        $qa['total_likes'] = 0;
                        $tl_q = $get_total_likes_q->fetch("assoc");
                        if (intval($tl_q['total_likes']) > 0) {
                            $qa['total_likes'] = $tl_q['total_likes'];
                        }
                        $total_likes_q =  $conn->execute(sprintf("SELECT * FROM `sessions_qa_likes` WHERE sessions_qa_id = '%s'", $qa['sessions_qa_id']));
                        $likes = [];
    
                        if($total_likes_q->count() > 0){
    
                            while($user_likes = $total_likes_q->fetch("assoc")){
                                if(!$user->blocked($user_likes['user_id'])){
                                    $user_data = $user->get_user_by_id($user_likes['user_id'])[0];
                                    $user_data['is_friend'] = in_array($user_likes['user_id'][0], $user->_data['friends_ids']) ? 1 : 0;
                                    $user_data['is_following'] = in_array($user_likes['user_id'][0], $user->_data['followings_ids']) ? 1 : 0;
                                    $likes[] = $user_data;
                                }
                            }
                        }
                        if(is_array($likes)){
                            $qa['total_likes'] = count($likes);
                        }
                        $qa['total_ans'] = 0;
                        $qa['answers'] = array();
                        
                        /* if sessions_qa_id not set */
                        if(!isset($input['sessions_qa_id'])) {
                            $response = [
                                'status'=>$codes->RC_ERROR,
                                'message'=>'sessions_qa_id field is required',
                                'data'=>null
                            ];
            
                            return $response;
                        }
                        
                        
                        $order = $input['order'];
                        $sessions_qa_id = $input['sessions_qa_id'];
                    $get_subQA_prev_total=array();
                    $coment_id=$sessions_qa_id;
                    if($target==1 && $order == 'prev' && $sessions_qa_id!=0){$sessions_qa_id=0;}
                    if($input['sortby']=='date')
                    {
                        /* if order not valid */
                        $valid_orders = array("next", "prev");
                        if (!isset($input['order']) || !in_array( $input['order'], $valid_orders) ) {
                            $response = [
                                'status'=>$codes->RC_ERROR,
                                'message'=>'Invalid value of the field: order',
                                'data'=>null
                            ];
            
                            return $response;
                        }
                        $operator = $order == "next" ? ">=" : "<=";
                        if($sessions_qa_id == 0 &&  $order == 'prev') {
                            if($target==1 && $coment_id!=0){
                                $get_subQA =  $conn->execute(sprintf("select * from (select * from (SELECT sessions_qa.*,reports.report_id FROM `sessions_qa` LEFT JOIN reports on (reports.node_id = sessions_qa.sessions_qa_id AND reports.node_type='answer' AND reports.user_id = '%s') WHERE sessions_qa.sessions_qa_id = '%s')comm UNION select * from (SELECT sessions_qa.*,reports.report_id FROM `sessions_qa` LEFT JOIN reports on (reports.node_id = sessions_qa.sessions_qa_id AND reports.node_type='answer' AND reports.user_id = '%s') WHERE sessions_qa.sessions_id = '%s' AND sessions_qa.`status` != '0' AND sessions_qa.qa_type = 2 AND sessions_qa.parent_question_id='%s' ORDER BY sessions_qa.added_on DESC LIMIT 15) comm) comment limit 15", $user->_data['user_id'], $coment_id, $user->_data['user_id'], $sessions_id, $qa['sessions_qa_id']));
                            }
                            else
                            {
                                $get_subQA =  $conn->execute(sprintf("SELECT sessions_qa.*,reports.report_id FROM `sessions_qa` LEFT JOIN reports on (reports.node_id = sessions_qa.sessions_qa_id AND reports.node_type='answer' AND reports.user_id = '%s') WHERE sessions_qa.sessions_id = '%s' AND sessions_qa.`status` != '0' AND sessions_qa.qa_type = 2 AND sessions_qa.parent_question_id='%s' ORDER BY sessions_qa.added_on DESC LIMIT 15", $user->_data['user_id'], $sessions_id, $qa['sessions_qa_id']));
                            }
                            $get_subQA_total =  $conn->execute(sprintf("SELECT sessions_qa.*,reports.report_id FROM `sessions_qa` LEFT JOIN reports on (reports.node_id = sessions_qa.sessions_qa_id AND reports.node_type='answer' AND reports.user_id = '%s') WHERE sessions_qa.sessions_id = '%s' AND sessions_qa.`status` != '0' AND sessions_qa.qa_type = 2 AND sessions_qa.parent_question_id='%s' ORDER BY sessions_qa.added_on DESC", $user->_data['user_id'], $sessions_id, $qa['sessions_qa_id']));
                        } else {
                            if($order == 'next'){
                                $sessions_qa_id .= " ORDER BY sessions_qa.sessions_qa_id ASC";
                            } else {
                                $sessions_qa_id .= " ORDER BY sessions_qa.sessions_qa_id DESC";
                            }
                            $get_subQA =  $conn->execute(sprintf("SELECT sessions_qa.*,reports.report_id FROM `sessions_qa` LEFT JOIN reports on (reports.node_id = sessions_qa.sessions_qa_id AND reports.node_type='answer' AND reports.user_id = '%s') WHERE sessions_qa.sessions_id = %s AND sessions_qa.`status` != '0' AND sessions_qa.qa_type = 2 AND sessions_qa.parent_question_id=%s AND sessions_qa.sessions_qa_id %s %s LIMIT 15", $user->_data['user_id'], $sessions_id, $qa['sessions_qa_id'], $operator, $sessions_qa_id));
                            
                            $get_subQA_total =  $conn->execute(sprintf("SELECT sessions_qa.*,reports.report_id FROM `sessions_qa` LEFT JOIN reports on (reports.node_id = sessions_qa.sessions_qa_id AND reports.node_type='answer' AND reports.user_id = '%s') WHERE sessions_qa.sessions_id = %s AND sessions_qa.`status` != '0' AND sessions_qa.qa_type = 2 AND sessions_qa.parent_question_id=%s AND sessions_qa.sessions_qa_id %s %s", $user->_data['user_id'], $sessions_id, $qa['sessions_qa_id'], $operator, $sessions_qa_id));
                            
                            $get_subQA_prev_total =  $conn->execute(sprintf("SELECT sessions_qa.*,reports.report_id FROM `sessions_qa` LEFT JOIN reports on (reports.node_id = sessions_qa.sessions_qa_id AND reports.node_type='answer' AND reports.user_id = '%s') WHERE sessions_qa.sessions_id = %s AND sessions_qa.`status` != '0' AND sessions_qa.qa_type = 2 AND sessions_qa.parent_question_id=%s AND sessions_qa.sessions_qa_id > %s", $user->_data['user_id'], $sessions_id, $qa['sessions_qa_id'], $sessions_qa_id));
                        }
                    }
                    else
                    {
                        /* if order not valid */
                        $valid_orders = array("next", "prev");
                        if (!isset($input['order']) || !in_array( $input['order'], $valid_orders) ) {
                            $response = [
                                'status'=>$codes->RC_ERROR,
                                'message'=>"Invalid value of the field: order",
                                'data'=>null
                            ];
            
                            return $response;
                        }
                        $operator = $order == "next" ? ">=" : "<=";
                        if($sessions_qa_id == 0 &&  $order == 'prev') {
                            if($target==1 && $coment_id!=0){
                            $get_subQA =  $conn->execute(sprintf("select * from (select * from (SELECT sq.*,reports.report_id,(select count(*) as totallike from `sessions_qa_likes` where `sessions_qa_id`=sq.sessions_qa_id)totallike FROM `sessions_qa` sq LEFT JOIN reports on (reports.node_id = sq.sessions_qa_id AND reports.node_type='answer' AND reports.user_id = '%s') WHERE sq.sessions_qa_id = '%s')comm UNION select * from ( SELECT sq.*,reports.report_id,(select count(*) as totallike from `sessions_qa_likes` where `sessions_qa_id`=sq.sessions_qa_id)totallike FROM `sessions_qa` sq LEFT JOIN reports on (reports.node_id = sq.sessions_qa_id AND reports.node_type='answer' AND reports.user_id = '%s') WHERE sq.sessions_id = '%s' AND sq.`status` != '0' AND sq.qa_type = 2 AND sq.parent_question_id='%s' ORDER BY totallike DESC,added_on DESC LIMIT 15)comm ) comment limit 15", $user->_data['user_id'], $coment_id, $user->_data['user_id'], $sessions_id, $qa['sessions_qa_id']));
                            }
                            else
                            {
                            $get_subQA =  $conn->execute(sprintf("SELECT sq.*,reports.report_id,(select count(*) as totallike from `sessions_qa_likes` where `sessions_qa_id`=sq.sessions_qa_id)totallike FROM `sessions_qa` sq LEFT JOIN reports on (reports.node_id = sq.sessions_qa_id AND reports.node_type='answer' AND reports.user_id = '%s') WHERE sq.sessions_id = %s AND sq.`status` != '0' AND sq.qa_type = 2 AND sq.parent_question_id=%s ORDER BY totallike DESC,added_on DESC LIMIT 15", $user->_data['user_id'], $sessions_id, $qa['sessions_qa_id']));
                            }
                            $get_subQA_total =  $conn->execute(sprintf("SELECT sq.*,reports.report_id,(select count(*) as totallike from `sessions_qa_likes` where `sessions_qa_id`=sq.sessions_qa_id)totallike FROM `sessions_qa` sq LEFT JOIN reports on (reports.node_id = sq.sessions_qa_id AND reports.node_type='answer' AND reports.user_id = '%s') WHERE sq.sessions_id = %s AND sq.`status` != '0' AND sq.qa_type = 2 AND sq.parent_question_id=%s ORDER BY totallike DESC,added_on DESC", $user->_data['user_id'], $sessions_id, $qa['sessions_qa_id']));
                        }
                        else {
                            if($order == 'next'){
                                $sessions_qa_id .= " ORDER BY totallike ASC,added_on DESC";
                            } else {
                                $sessions_qa_id .= " ORDER BY totallike DESC,added_on DESC";
                            }
                            $get_subQA =  $conn->execute(sprintf("SELECT sq.*,reports.report_id,(select count(*) as totallike from `sessions_qa_likes` where `sessions_qa_id`=sq.sessions_qa_id)totallike FROM `sessions_qa` sq LEFT JOIN reports on (reports.node_id = sessions_qa.sessions_qa_id AND reports.node_type='answer' AND reports.user_id = '%s') WHERE sq.sessions_id = %s AND sq.`status` != '0' AND sq.qa_type = 2 AND sq.parent_question_id=%s AND sq.sessions_qa_id %s %s LIMIT 15", $user->_data['user_id'], $sessions_id, $qa['sessions_qa_id'], $operator, $sessions_qa_id));
                            
                            $get_subQA_total =  $conn->execute(sprintf("SELECT sq.*,reports.report_id,(select count(*) as totallike from `sessions_qa_likes` where `sessions_qa_id`=sq.sessions_qa_id)totallike FROM `sessions_qa` sq LEFT JOIN reports on (reports.node_id = sessions_qa.sessions_qa_id AND reports.node_type='answer' AND reports.user_id = '%s') WHERE sq.sessions_id = %s AND sq.`status` != '0' AND sq.qa_type = 2 AND sq.parent_question_id=%s AND sq.sessions_qa_id %s %s", $user->_data['user_id'], $sessions_id, $qa['sessions_qa_id'], $operator, $sessions_qa_id));
                            
                             $get_subQA_prev_total =  $conn->execute(sprintf("SELECT sq.*,reports.report_id,(select count(*) as totallike from `sessions_qa_likes` where `sessions_qa_id`=sq.sessions_qa_id)totallike FROM `sessions_qa` sq LEFT JOIN reports on (reports.node_id = sessions_qa.sessions_qa_id AND reports.node_type='answer' AND reports.user_id = '%s') WHERE sq.sessions_id = %s AND sq.`status` != '0' AND sq.qa_type = 2 AND sq.parent_question_id=%s AND sq.sessions_qa_id > %s", $user->_data['user_id'], $sessions_id, $qa['sessions_qa_id'], $operator, $sessions_qa_id));
                        }
                        
                    }
                    $get_total_subQA =  $conn->execute(sprintf("SELECT * FROM `sessions_qa` WHERE sessions_id = %s AND `status` != '0' AND qa_type = 2 AND parent_question_id=%s ORDER BY added_on ASC", $sessions_id, $qa['sessions_qa_id']));
                        $qa['total_ans'] = $get_total_subQA->count();
                    $more_present=0;
                    if($get_total_subQA->count()>15)
                    {
                        $more_present=1;
                    }
                    $loadmore=0;
                    if($get_subQA_prev_total && $get_subQA_prev_total->count() > 0)
                    {
                        $loadmore=1;
                    }
                    $qa['more_present']=$more_present;
                    $qa['loadmore']=$loadmore;
                        
                        if ($get_subQA->count() > 0) {
                            while ($subqa = $get_subQA->fetch("assoc")) {
                                if(!$user->blocked($subqa['user_id'])){
                                    $get_mylike =  $conn->execute(sprintf("SELECT * FROM `sessions_qa_likes` WHERE sessions_qa_id = %s AND `user_id` = %s ", $subqa['sessions_qa_id'], $user->_data['user_id']));
                                    $subqa['i_like'] = false;
                                    if ($get_mylike->count() > 0) {
                                        $subqa['i_like'] = true;
                                    }
                                    $subqa['i_reported']=($subqa['report_id'])?1:0;
                                    $get_myfollow =  $conn->execute(sprintf("SELECT * FROM `sessions_qa_follows` WHERE sessions_qa_id = %s AND `user_id` = %s ", $subqa['sessions_qa_id'], $user->_data['user_id']));
                                    $subqa['i_follow'] = false;
                                    if ($get_myfollow->count() > 0) {
                                        $subqa['i_follow'] = true;
                                    }
                                    
                                    $get_total_likes =  $conn->execute(sprintf("SELECT count(*) AS total_likes FROM `sessions_qa_likes` WHERE sessions_qa_id = %s ", $subqa['sessions_qa_id']));
    
                                    $subqa['total_likes'] = 0;
                                    $tl = $get_total_likes->fetch("assoc");
                                    if (intval($tl['total_likes']) > 0) {
                                        $subqa['total_likes'] = $tl['total_likes'];
                                    }
                                    $total_likes_sq =  $conn->execute(sprintf("SELECT * FROM `sessions_qa_likes` WHERE sessions_qa_id = %s ", $subqa['sessions_qa_id']));
                                    $likes = [];
                                    if($total_likes_sq->count() > 0){
                                        while($user_likessb = $total_likes_sq->fetch("assoc")){
                                            if(!$user->blocked($user_likessb['user_id'])){
                                                $user_data = $user->get_user_by_id($user_likessb['user_id'])[0];
                                                $user_data['is_friend'] = in_array($user_likes['user_id'][0], $user->_data['friends_ids']) ? 1 : 0;
                                                $user_data['is_following'] = in_array($user_likes['user_id'][0], $user->_data['followings_ids']) ? 1 : 0;
    
                                                $likes = $user_data;
                                            }
                                        }
                                    }
                                    $subqa['total_likes'] = count($likes);
                                    $subqa['user_data'] = $user->get_user_by_id($subqa['user_id']);
                                    $subqa['user_data'] = $subqa['user_data'][0];
    
                                    /* GET REPLY OF ANSWERS :: START */
                                    $replys = array();
                                    $get_replyQA =  $conn->execute(sprintf("SELECT * FROM `sessions_qa` WHERE sessions_id = %s AND `status` != '0' AND qa_type = 3 AND parent_question_id=%s ORDER BY added_on ASC", $sessions_id, $subqa['sessions_qa_id']));
                                    $subqa['total_replies'] = $get_replyQA->count();
                                    if ($get_replyQA->count() > 0) {
                                        while ($replyqa = $get_replyQA->fetch("assoc")) {
                                            if(!$user->blocked($replyqa['user_id'])){
                                                /* $get_mylike =  $conn->execute(sprintf("SELECT * FROM `sessions_qa_likes` WHERE sessions_qa_id = %s AND `user_id` = %s ", secure($replyqa['sessions_qa_id'], 'int'), $user->_data['user_id']));
                                                  $replyqa['i_like'] = false;
                                                  if ($get_mylike->count() > 0) {
                                                  $replyqa['i_like'] = true;
                                                  }
                                                  $get_total_likes =  $conn->execute(sprintf("SELECT count(*) AS total_likes FROM `sessions_qa_likes` WHERE sessions_qa_id = %s ", secure($replyqa['sessions_qa_id'], 'int')));
    
                                                  $replyqa['total_likes'] = 0;
                                                  $tl = $get_total_likes->fetch("assoc");
                                                  if (intval($tl['total_likes']) > 0) {
                                                  $replyqa['total_likes'] = $tl['total_likes'];
                                                  } */
                                                $replyqa['user_data'] = $user->get_user_by_id($replyqa['user_id']);
                                                $replyqa['user_data'] = $replyqa['user_data'][0];
                                                $replys[] = $replyqa;
                                            }
                                        }
                                    }
                                    //$subqa['replys'] = $replys;
                                    if(isset($subqa['replys'])){
                                        $subqa['total_replies'] = count($subqa['replys']);
                                    }
                                    /* GET REPLY OF ANSWERS :: END */
    
                                    $qa['answers'][] = $subqa;
                                }
                                // $qa['total_ans'] = count($qa['answers']);
                            }
                        }
                        if( ($sessions_qa_id == 0 &&  $order == 'prev') || $order == "prev") {
                            if($target==1 && $coment_id!=0)
                            {
                            $ckey = array_search($coment_id, array_column($qa['answers'], 'sessions_qa_id'));
                            $new_value = $qa['answers'][$ckey];
                            unset($qa['answers'][$ckey]);
                                array_unshift($qa['answers'], $new_value);
                            }
                            $qa['answers'] = $qa['answers'];
                        }
                        $more_present=0;
                        if($get_subQA_total->count()>15)
                        {
                            $more_present=1;
                        }
                        $qa['more_present'] =$more_present;
                        $qa['user_data'] = $user->get_user_by_id($qa['user_id']);
                        $qa['user_data'] = $qa['user_data'][0];
                    
                    
                        
                    $get_session =  $conn->execute(sprintf("SELECT sessions_id,title FROM `sessions` WHERE `sessions_id` = %s AND `status` IN ('2', '3', '4') ", $qa['sessions_id']));
                    
                    if($get_session->count()){
                        $session_data = $get_session->fetch("assoc");
                    } else {
                        $session_data['sessions_id'] = '';
                        $session_data['title'] = '';
                    }
                    
                    /* 
                    $session_data['created_by'] = $user->get_user_by_id($session_data['created_by']);
                    $session_data['created_by'] = $session_data['created_by'][0];
                    get page picture
                    $session_data['page_picture_default'] = ($session_data['cover_photo']) ? false : true;
                    $session_data['page_picture'] = UserModel::get_picture($session_data['cover_photo'], 'page');
                    
                    $session_data['cover_photo'] = $system['system_url'].'/content/uploads/'.$session_data['cover_photo'];
                    $event_photos = explode(',', $session_data['event_photos']);
                    $event_photos = array_filter($event_photos);
                    $session_data['event_photos'] =array(); 
                    if(!empty($event_photos)) {
                        foreach ($event_photos as $photo_key => $photo_value) {
                            $session_data['event_photos'][]['photo'] =  $system['system_url'].'/content/uploads/'.$photo_value;
                        }
                    }
                    */
                    $data['session_data'] = $session_data; 
                    
                    $question_follow =  $conn->execute(sprintf("SELECT * FROM `sessions_qa_follows` WHERE user_id = %s and sessions_qa_id = %s  ", $user->_data['user_id'], $qa['sessions_qa_id']));
                    if ($question_follow->count() > 0) { 
                        $qa['user_follow'] = 1;
                    } else {
                        $qa['user_follow'] = 0;
                    }
                    
                    /* $i = 0;
                    $sessionInterestArr = array();
                    $get_seession_interest =  $conn->execute(sprintf("SELECT *, ( SELECT text FROM `interest_mst` WHERE interest_id = si.interest ) AS session_name FROM `sessions_interest` AS si WHERE si.`sessions_id` = %s AND `joined` !='1'", secure($qa['sessions_id'], 'int')));
                    while ($session_interest = $get_seession_interest->fetch("assoc")) {
                        $sessionInterestArr[$i]['interest_id'] = $session_interest['interest'];
                        $sessionInterestArr[$i]['session_name'] = $session_interest['session_name'];
    
                        $i++;
                    }
                    $data['sessionInterestArr'] = $sessionInterestArr; */
                    $data['qas'] = $qa;
                    
                    
                    $response['status'] = 1;
                    $response['message'] = "Question Details";
                    $response['data'] = $data;
                break;
                
                default:
                    _apiError('404');
            }
            return $response;            
        }catch(\Exception $e){
            print_r($e);
            Log::debug($e);
            throw new Exception($e);
        }
    }

      /**
     * 
     */
    public function invite($input)
    {
        Log::debug("Started ... invite Service : ");
        try{
            $conn = ConnectionManager::get('default');
            $codes = new Codes;
            extract($input);

            $valid = array('invite_session', 'invite_interest', 'session_conduct_request', 'invite_venue', 'invite_question');

            if(!isset($input['user_id']) || empty($input['user_id'])){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'user id is required',
                    'data'=>null
                ];

                return $response;
            }

            if(!in_array($input['type'], $valid)){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'Please enter valid type',
                    'data'=>null
                ];

                return $response;
            }

            if(!isset($input['friend_id']) || empty($input['friend_id'])){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'friend_id is required',
                    'data'=>null
                ];

                return $response;
            }

            if(!isset($input['id']) || empty($input['id'])){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'id parameter is required',
                    'data'=>null
                ];

                return $response;
            }

            $user = new UserModel($input['user_id']);

            $user->send_invites($input['type'], $input['friend_id'],$input['id']);
			$response['status'] = 1;
			$response['message'] = 'success';
			$response['data'] = array('user_id'=>$input['user_id']);

            return $response;            
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }

    public function invitations($input)
    {
        Log::debug("Started ... invitations Service : ");
        try{
            $conn = ConnectionManager::get('default');
            $codes = new Codes;
            extract($input);

          

            if(!isset($input['user_id']) || empty($input['user_id'])){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'user id is required',
                    'data'=>null
                ];

                return $response;
            }

            $valid = array('interest_invitation', 'venue_invitation', 'ans_invitation_user_list', 'session_conduct_user_list', 'invite_session_user_list');

            if(!isset($input['type']) || $input['type'] == "" || !in_array($input['type'], $valid)){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'valid type required',
                    'data'=>null
                ];

                return $response;
            }

           
            
            $user = new UserModel($input['user_id']);

            if ($input['type'] == 'interest_invitation') {

                if(!isset($input['offset']) || $input['offset'] == ""){
                    $response = [
                        'status'=>$codes->RC_ERROR,
                        'message'=>'offset is required',
                        'data'=>null
                    ];
    
                    return $response;
                }

                $id = $input['invitation_id'];
                $query_search = '';
                if(isset($input['query'])){
                    $query_search = trim($input['query']);
                }
               
                $offset = $input['offset'];
                $offset *= $system['max_results'];
                
                $friend_list = $user->_data['friends_ids'];
                
                $invites_ids = array();
                $check = $conn->execute(sprintf("SELECT * FROM interest_invites WHERE interest_id = '%s' AND user_id = %s", $id, $user->_data['user_id']));	
                if ($check->count() > 0) {
                        while ($invite = $check->fetch("assoc")) {
                            $invites_ids[] = $invite['invited_users'];
                        }

                }
                $users = array();  
                if($friend_list){
                    foreach($friend_list as $friend){
                        $query = $conn->execute(sprintf("SELECT * FROM user_interest WHERE user_id = %s AND interest = '%s'", $friend, $id));	
                        if(!$query->count() > 0){  
                            $user_data =  $conn->execute(sprintf("SELECT privacy_connection_request, user_id,user_name,user_verified,user_fullname,user_gender,user_picture,user_work,	user_work_title,user_work_place,	user_current_city FROM users WHERE privacy_invite_interest = 'friends' AND user_id = '%s' AND user_fullname LIKE '%s' ORDER BY user_fullname ASC LIMIT %s, %s", $friend, $query_search."%", $offset, $codes->MAX_RESULTS));
                                
                            while ($_user = $user_data->fetch("assoc")) {
                                $_user['invited_status'] = 0;
                                if(in_array($_user['user_id'],$invites_ids)){
                                    $_user['invited_status'] = 1;
                                }
                                $_user['user_picture'] = $user->get_picture($_user['user_picture'], $_user['user_gender']);
                                /* get the connection between the viewer & the target */
                                $_user['connection'] = $user->connection($_user['user_id']);
                                /* get mutual friends count */
                                $_user['mutual_friends_count'] = $user->get_mutual_friends_count($_user['user_id']);
                                $_user['no_of_followers'] = count($user->get_followers_ids($_user['user_id']));
                                $_user['number_of_mutual_interests'] = count($user->get_mutual_interests($_user['user_id']));

                                $users[] = $_user;
                            }
                        }		   
                    }
                }
                $result = $users;
            }


            if ($input['type'] == 'venue_invitation') {

                if(!isset($input['offset']) || $input['offset'] == ""){
                    $response = [
                        'status'=>$codes->RC_ERROR,
                        'message'=>'offset is required',
                        'data'=>null
                    ];
    
                    return $response;
                }

                $id = $input['invitation_id'];
                $query_search = '';
                if(isset($input['query'])){
                    $query_search = trim($input['query']);
                }
                
                $offset = $input['offset'];
                $offset *= $system['max_results'];
                $invites_ids = array();
                
                $check = $conn->execute(sprintf("SELECT * FROM venue_invites WHERE venue_id = '%s' AND user_id = %s",$id, $user->_data['user_id']));	 
                if ($check->count() > 0) {
                     while ($invite = $check->fetch("assoc")) {
                         $invites_ids[] = $invite['invited_users'];
                     }
                }
                
                $friend_list = $user->_data['friends_ids'];
                $users = array();
                if($friend_list){
                    foreach($friend_list as $friend){
                        $query = $conn->execute(sprintf('SELECT * FROM users_venue WHERE venue_id = "%s" AND user_id = %s',$id,$friend));	 
                          if(!$query->count() > 0){ 
                          
                             $user_data =  $conn->execute(sprintf("SELECT privacy_connection_request, user_id,user_name,user_verified,user_fullname,user_gender,user_picture,user_work,	user_work_title,	user_work_place,user_current_city FROM users WHERE privacy_invite_venue ='friends' AND user_id = %s AND user_fullname LIKE '%s' ORDER BY user_fullname ASC LIMIT %s, %s", $friend, $query_search."%",$offset, $codes->MAX_RESULTS));	
                             
                             
                             while ($_user = $user_data->fetch("assoc")) {
                                  $_user['invited_status'] = 0;
                                 if(in_array($_user['user_id'],$invites_ids)){
                                     $_user['invited_status'] = 1;
                                 }
                                $_user['user_picture'] = $user->get_picture($_user['user_picture'], $_user['user_gender']);
                                /* get the connection between the viewer & the target */
                                $_user['connection'] = $user->connection($_user['user_id']);
                                /* get mutual friends count */
                                $_user['mutual_friends_count'] = $user->get_mutual_friends_count($_user['user_id']);
                                $_user['no_of_followers'] = count($user->get_followers_ids($_user['user_id']));
                                $_user['number_of_mutual_venues'] = count($user->get_mutual_venues($_user['user_id']));
                                $users[] = $_user;
                             }
                          }	
                            
                         }		   
                    }
                $result = $users;                
            }

            if ($input['type'] == 'ans_invitation_user_list') {

                if(!isset($input['offset']) || $input['offset'] == ""){
                    $response = [
                        'status'=>$codes->RC_ERROR,
                        'message'=>'offset is required',
                        'data'=>null
                    ];
    
                    return $response;
                }

                if(!isset($input['sessions_qa_id']) || $input['sessions_qa_id'] == ""){
                    $response = [
                        'status'=>$codes->RC_ERROR,
                        'message'=>'sessions_qa_id is required',
                        'data'=>null
                    ];
    
                    return $response;
                }

                $query_search = '';
                if(isset($input['query'])){
                    $query_search = trim($input['query']);
                }
                
               
              
                $offset = $input['offset'];
                $offset *= $system['max_results'];
                
                $session_qa = $conn->execute(sprintf('SELECT * FROM sessions_qa WHERE sessions_qa_id = %s', $input['sessions_qa_id']));
                if($session_qa->count() == 0){
                    $response = [
                        'status'=>$codes->RC_ERROR,
                        'message'=>'Invalid Question id',
                        'data'=>null
                    ];
    
                    return $response;
                }
                $questionData = $session_qa->fetch("assoc");
                
                $get_int =$conn->execute(sprintf("SELECT * FROM sessions WHERE sessions_id = %s", $questionData['sessions_id']));
                $sessionData = $get_int->fetch("assoc");
                    
                    
                $get_answered_users =$conn->execute(sprintf('SELECT * FROM sessions_qa WHERE parent_question_id = %s', $input['sessions_qa_id']));
                $answered_user_ids = array();
                if($get_answered_users->count() > 0){
                    while ($questions = $get_answered_users->fetch("assoc")) {
                        $answered_user_ids[] = $questions['user_id'];
                    }
                }
                
                $invites_ids = array();
                $check =$conn->execute(sprintf("SELECT * FROM sessions_qa_invites WHERE sessions_qa_id = %s AND user_id = %s", $input['sessions_qa_id'], $user->_data['user_id']));
                if ($check->count() > 0) {
                    while ($invite = $check->fetch("assoc")) {
                        $invites_ids[] = $invite['invited_users'];
                    }
                }
                
                $users = array();
                
                $friend_list = $user->_data['friends_ids'];
                //pr($answered_user_ids);
                //pr($invites_ids);die;
                if($friend_list){
                    $user_data =$conn->execute(sprintf("SELECT privacy_connection_request, user_id,user_name,user_verified,user_fullname,user_firstname,user_lastname,user_gender,user_picture,user_work,user_work_title,user_work_place,user_current_city FROM users WHERE user_id IN (%s) AND (user_fullname LIKE '%s' OR user_firstname LIKE '%s') AND privacy_invite_answer!='me' AND NOT EXISTS(SELECT * FROM users_blocks WHERE (users_blocks.user_id ='".$user->_data['user_id']."' AND users_blocks.blocked_id = users.user_id) OR (users_blocks.blocked_id = '".$user->_data['user_id']."' AND users_blocks.user_id = users.user_id)) ORDER BY user_fullname ASC LIMIT %s, %s", implode(",",$friend_list), $query_search."%", $query_search."%", $offset, $codes->MAX_RESULTS));
                    
                    while ($_user = $user_data->fetch("assoc")) {
                        if(!$user->blockedBetweenTwo($questionData['user_id'],$_user['user_id'])){
                            if(!in_array($_user['user_id'],$answered_user_ids)){
                                $get_follow =$conn->execute(sprintf("SELECT * FROM `sessions_qa_follows` WHERE sessions_qa_id = %s AND `user_id` = %s ", $input['sessions_qa_id'],$_user['user_id']));
                                
                                if (!$get_follow->count() > 0) {
                                    if(in_array($_user['user_id'],$invites_ids)){
                                        $_user['invited_status'] = 1;
                                    } else {
                                        $_user['invited_status'] = 0;
                                    }
                                    $_user['user_picture'] = $user->get_picture($_user['user_picture'], $_user['user_gender']);
                                    /* get the connection between the viewer & the target */
                                    $_user['connection'] = $user->connection($_user['user_id']);
                                    /* get mutual friends count */
                                    $_user['mutual_friends_count'] = $user->get_mutual_friends_count($_user['user_id']);
                                    $_user['no_of_followers'] = count($user->get_followers_ids($_user['user_id']));
                                    $_user['number_of_answers'] = $user->get_user_answers_count( $_user['user_id'] );
                                    $users[] = $_user;
                                }
                            }
                        }
                    }		   
                }
                
                $result = $users;
                $message = "user_list";
            }
            
            if ($input['type'] == 'session_conduct_user_list') {
                if(!isset($input['session_id'])  || empty($input['session_id']) ){
                    throw new Exception('session id is required');
                }
                
                if(!isset($input['offset'])  || is_empty($input['offset']) ){
                    throw new Exception('offset is required');
                }
                $query_search = '';
                if(isset($input['query'])){
                    $query_search = trim($input['query']);
                }
                
                /* initialize arguments */
                $session_id = $input['session_id'];
                $offset = $input['offset'];
                /* initialize vars */
                $users = array();
                $offset *= $system['max_results'];
                $friend_list = $user->_data['friends_ids'];
                
                if($friend_list){
                    
                    $invites_ids = array();
                    $check =$conn->execute(sprintf("SELECT * FROM sessions_conduct_request WHERE sessions_id = %s AND user_id = %s", secure($session_id, 'int'), $user->_data['user_id']));
                    if ($check->count() > 0) {
                        while ($invite = $check->fetch("assoc")) {
                            $invites_ids[] = $invite['invited_users'];
                        }
                    }
                    
                    $get_users =$conn->execute(sprintf("SELECT * FROM `sessions_presentor` WHERE `sessions_id` = %s ", secure($input['session_id'])));
                    $userArr = array();
                    while ($users_data = $get_users->fetch("assoc")) {
                        $userArr[] = $users_data['user_id'];
                    }
                    $get_attendies =$conn->execute(sprintf("SELECT * FROM `sessions_attends` WHERE `sessions_id` = %s ", secure($input['session_id'])));
                    while ($attendies_data = $get_attendies->fetch("assoc")) {
                        $userArr[] = $attendies_data['user_id'];
                    }
                    $get_int =$conn->execute(sprintf("SELECT * FROM sessions WHERE sessions_id = %s", $input['session_id']));
                    $qa_data = $get_int->fetch("assoc");
                    $userArr[] = $qa_data['created_by'];
                    
                    $sqlQuery = sprintf("SELECT user_id,user_name,user_verified,user_fullname,user_firstname,user_lastname,user_gender,user_picture, privacy_invite_conduct_session,privacy_connection_request,user_work,user_work_title,user_work_place,user_current_city FROM users WHERE user_id IN (%s)", implode(",",$friend_list));
                    
                    if(!empty($query_search)){
                        $sqlQuery .= "AND (user_fullname LIKE '$query_search%' OR user_firstname LIKE '$query_search%')";
                    }
                    
                    $sqlQuery .= " AND NOT EXISTS(SELECT * FROM users_blocks WHERE (users_blocks.user_id ='".$user->_data['user_id']."' AND users_blocks.blocked_id = users.user_id) OR (users_blocks.blocked_id = '".$user->_data['user_id']."' AND users_blocks.user_id = users.user_id))";
                    
                    $sqlQuery .= sprintf(" ORDER BY user_fullname ASC LIMIT %s, %s",$offset, $codes->MAX_RESULTS);
                    
                    //echo $sqlQuery;die;
                    $get_users =$conn->execute($sqlQuery);
                    
                    if ($get_users->count() > 0) {
                        while ($_user = $get_users->fetch("assoc")) {
                            if(!$user->blockedBetweenTwo($qa_data['created_by'],$_user['user_id']) && !$user->blockedBetweenTwo($qa_data['presentors'],$_user['user_id'])){
                                if(!in_array($_user['user_id'],$userArr) && $_user['privacy_invite_conduct_session'] == 'friends'){
                                    $_user['invited_status'] = 0;
                                    if(in_array($_user['user_id'],$invites_ids)){
                                        $_user['invited_status'] = 1;
                                    }
                                    $_user['user_picture'] = $user->get_picture($_user['user_picture'], $_user['user_gender']);
                                    /* get the connection between the viewer & the target */
                                    $_user['connection'] = $user->connection($_user['user_id']);
                                    /* get mutual friends count */
                                    $_user['mutual_friends_count'] = $user->get_mutual_friends_count($_user['user_id']);
                                    $_user['no_of_followers'] = count($user->get_followers_ids($_user['user_id']));

                                    $_user['number_of_sessions_conducted'] = count( $user->get_user_sessions_conducted($_user['user_id']) );

                                    $users[] = $_user;
                                }
                            }
                        }
                    }
                }

                $result = $users;
            }
            
            if ($input['type'] == 'invite_session_user_list') {
               
                if(!isset($input['session_id'])  || empty($input['session_id']) ){
                    throw new Exception('session id is required');
                }
                
                if(!isset($input['offset'])  || is_empty($input['offset']) ){
                    throw new Exception('offset is required');
                }
                
                $query = '';
                if(isset($input['query'])){
                    $query = trim($input['query']);
                }
                
                /* initialize arguments */
                $session_id = $input['session_id'];
                $offset = $input['offset'];
                /* initialize vars */
                $users = array();
                $offset *= $system['max_results'];
                $friend_list = $user->_data['friends_ids'];
                
                //pr($friend_list);
                if($friend_list){
                    
                    $invites_ids = array();
                    $check =$conn->execute(sprintf("SELECT * FROM sessions_invites WHERE session_id = %s AND user_id = %s", secure($session_id, 'int'), $user->_data['user_id']));
                    if ($check->count() > 0) {
                         while ($invite = $check->fetch("assoc")) {
                             $invites_ids[] = $invite['invited_users'];
                         }
                    }
                    
                    $get_users =$conn->execute(sprintf("SELECT * FROM `sessions_presentor` WHERE `sessions_id` = %s ", secure($input['session_id'])));
                    $userArr = array();
                    while ($users_data = $get_users->fetch("assoc")) {
                        $userArr[] = $users_data['user_id'];
                    }

                    //pr($userArr);
                    $get_attendies =$conn->execute(sprintf("SELECT * FROM `sessions_attends` WHERE `sessions_id` = %s AND status='1'", secure($input['session_id'])));
                    while ($attendies_data = $get_attendies->fetch("assoc")) {
                        $userArr[] = $attendies_data['user_id'];
                    }

                    //pr($userArr);
                    $get_int =$conn->execute(sprintf("SELECT * FROM sessions WHERE sessions_id = %s", $input['session_id']));
                    $qa_data = $get_int->fetch("assoc");
                    $userArr[] = $qa_data['created_by'];
                    //pr($userArr);
                    
                    foreach($friend_list as $friend_id){
                        $get_users =$conn->execute(sprintf("SELECT user_id,user_name,user_verified,user_fullname,user_firstname,user_lastname,user_gender,user_picture, privacy_invite_session,user_work,user_work_title,user_work_place,user_current_city FROM users WHERE user_id = %s AND (user_fullname LIKE '%s' OR user_firstname LIKE '%s') AND NOT EXISTS(SELECT * FROM users_blocks WHERE (users_blocks.user_id ='".$user->_data['user_id']."' AND users_blocks.blocked_id = users.user_id) OR (users_blocks.blocked_id = '".$user->_data['user_id']."' AND users_blocks.user_id = users.user_id)) ORDER BY user_fullname ASC LIMIT %s, %s", secure($friend_id, 'int'), secure($query."%"), secure($query."%"), $offset, $codes->MAX_RESULTS));
                        
                        if ($get_users->count() > 0) {
                            while ($_user = $get_users->fetch("assoc")) {
                                if(!$user->blockedBetweenTwo($qa_data['created_by'],$_user['user_id']) && !$user->blockedBetweenTwo($qa_data['presentors'],$_user['user_id'])){
                                    if(!in_array($_user['user_id'],$userArr) && $_user['privacy_invite_session'] == 'friends'){
                                        $_user['invited_status'] = 0;
                                        if(in_array($_user['user_id'],$invites_ids)){
                                            $_user['invited_status'] = 1;
                                        }
                                        $_user['user_picture'] = $user->get_picture($_user['user_picture'], $_user['user_gender']);
                                        /* get the connection between the viewer & the target */
                                        $_user['connection'] = $user->connection($_user['user_id']);
                                        /* get mutual friends count */
                                        $_user['mutual_friends_count'] = $user->get_mutual_friends_count($_user['user_id']);
                                        $_user['no_of_followers'] = count($user->get_followers_ids($_user['user_id']));
                                        $_user['no_of_session_attended'] = count( $user->get_attended_sessions($_user['user_id']) );
                                        $users[] = $_user;
                                    }
                                }
                            }
                        }
                    }
                }
                $result = $users;
            }

            return $response;            
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }

    public function getRewardsBadges($input)
    {
        Log::debug("Started ... getRewardsBadges Service : ");
        try{
            $conn = ConnectionManager::get('default');
            $codes = new Codes;
            $pod = new PointsDao;
            $dataValidation = new DataValidation;
            //extract($input);


            if(!isset($input['user_id']) || $dataValidation->isEmpty($input['user_id'])){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'user_id  is required',
                    'data'=>null
                ];
                return $response;
            }
            if(!isset($input['case']) || $dataValidation->isEmpty($input['case'])){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'case  is required',
                    'data'=>null
                ];
                return $response;
            }
            extract($input);
            switch ($case) {
                case '1':
                    $data = $pod->getPointsAndBadgesByUserid($user_id);
                    break;
                case '2':
                    $data = $pod->getRandomRewardByUserId($user_id);
                    break;
                case '3':
                    $data = $pod->getBadges();
                    break;
                case '4':
                    $data = $pod->getRewards();
                    break;
                
                default:
                    $response = [
                        'status'=>$codes->RC_ERROR,
                        'message'=>'case  is wrong',
                        'data'=>null
                    ];
                    return $response;
                    break;
            }
            
            
            
            $response['status'] = $codes->RC_SUCCESS;
            $response['message'] = $codes->RM_SUCCESS;
            $response['data'] = $data;
            

            return $response;            
        }catch(\Exception $e){
            print_r($e);
            Log::debug($e);
            throw new Exception($e);
        }
    }

    public function addUseReward($input)
    {
        Log::debug("Started ... addUseReward Service : ");
        try{
            $conn = ConnectionManager::get('default');
            $codes = new Codes;
            $pod = new PointsDao;
            $dataValidation = new DataValidation;
            //extract($input);


            if(!isset($input['user_id']) || $dataValidation->isEmpty($input['user_id'])){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'user_id  is required',
                    'data'=>null
                ];
                return $response;
            }
            if(!isset($input['reward_name']) || $dataValidation->isEmpty($input['reward_name'])){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'reward_name  is required',
                    'data'=>null
                ];
                return $response;
            }
            if(!isset($input['give']) || $dataValidation->isEmpty($input['give'])){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'give  is required',
                    'data'=>null
                ];
                return $response;
            }
            extract($input);
            switch ($give) {
                case '1':
                    $data = $pod->addPoints($reward_name, $user_id, 1);
                    break;
                case '-1':
                    $data = $pod->addPoints($reward_name, $user_id , -4);
                    break;
                
                default:
                    $response = [
                        'status'=>$codes->RC_ERROR,
                        'message'=>'give  is wrong',
                        'data'=>null
                    ];
                    return $response;
                    break;
            }
            
            
            
            $response['status'] = $data['status'];
            $response['message'] = $data['msg'];
            $response['data'] = null;
            

            return $response;            
        }catch(\Exception $e){
            print_r($e);
            Log::debug($e);
            throw new Exception($e);
        }
    }



    public function newsFeed($input)
    {
        Log::debug("Started ... getInterestSiblings Service : ");
        try{
            $conn = ConnectionManager::get('default');
            $codes = new Codes;
            $dataValidation = new DataValidation;
            //extract($input);


            if(!isset($input['user_id']) || $dataValidation->isEmpty($input['user_id'])){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'user_id  is required',
                    'data'=>null
                ];
                return $response;
            }
            if(!isset($input['previous_seen']) || $dataValidation->isEmpty($input['previous_seen'])){
                $input['previous_seen'] = '';
            }
            if(!isset($input['view_check']) || $dataValidation->isEmpty($input['view_check'])){
                $input['view_check'] = 1;
            }
            if(!isset($input['pagesize']) || $dataValidation->isEmpty($input['pagesize'])){
                $input['pagesize'] = $codes->MAX_RESULTS_NF;
            }
            
            //return $input['previous_seen'];
            //extract($input);
            $ad = new PostsDao;
            $data = $ad->newsFeed($input['user_id'] , $input['previous_seen'] , $input['view_check'] , $input['pagesize'] );
            
            $response['status'] = $codes->RC_SUCCESS;
            $response['message'] = $codes->RM_SUCCESS;
            $response['data'] = $data;
            

            return $response;            
        }catch(\Exception $e){
            print_r($e);
            Log::debug($e);
            throw new Exception($e);
        }
    }


}
