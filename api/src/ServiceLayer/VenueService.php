<?php

namespace App\ServiceLayer;

use App\DaoLayer\CampusDetailDao;
use App\Utils\Codes;
use App\Utils\DataValidation;
use Cake\Core\Exception\Exception;
use App\DaoLayer\InterestMstDao;
use App\DaoLayer\OrganizationDao;
use App\DaoLayer\OrganizationTypeDao;
use App\DaoLayer\PostsDao;
use App\DaoLayer\PostsLikesDao;
use App\DaoLayer\PostsSavedDao;
use App\DaoLayer\ScheduleCampusDao;
use App\DaoLayer\UserInterestsDao;
use App\DaoLayer\UserOrganizerDao;
use App\DaoLayer\UsersDao;
use App\DaoLayer\VenueDao;
use App\Utils\ImageUtils;
use App\Utils\DateUtils;
use Cake\Log\Log;

class VenueService
{
    


    public function addVenue($inputJson)
    {
        Log::debug("Started ... addOrganization Service : " . json_encode($inputJson));
        try {
            $codes = new Codes;
            $dataValidation = new DataValidation;
            $dateUtils = new DateUtils;
            $venueDao = new VenueDao;

        
            //User validation
            if (!isset($inputJson['user_id']) || $dataValidation->isEmpty($inputJson['user_id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your user ID",
                    'data' => null
                ];
                return $response;
            }
            
            //text validation
            if (!isset($inputJson['venue_name']) || $dataValidation->isEmpty($inputJson['venue_name'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "venue_name required",
                    'data' => null
                ];
                return $response;
            }
            /*
            if (!isset($inputJson['venue_type']) || $dataValidation->isEmpty($inputJson['venue_type'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "venue_type required",
                    'data' => null
                ];
                return $response;
            }
            */
            //text validation
            // if (!isset($inputJson['type']) || $dataValidation->isEmpty($inputJson['type'])) {
            //     $response = [
            //         'status' => $codes->RC_ERROR,
            //         'message' => "Please enter your Organization Type",
            //         'data' => null
            //     ];
            //     return $response;
            // }

            //text validation
            /*
            if (!isset($inputJson['cityid']) || $dataValidation->isEmpty($inputJson['cityid'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter legal name",
                    'data' => null
                ];
                return $response;
            }
            */
            //text validation
            if (!isset($inputJson['venue_email']) || $dataValidation->isEmpty($inputJson['venue_email'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Contact mail required",
                    'data' => null
                ];
                return $response;
            }

            //text validation
            if (!isset($inputJson['venue_contact']) || $dataValidation->isEmpty($inputJson['venue_contact'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Contact no required",
                    'data' => null
                ];
                return $response;
            }

            if (!isset($inputJson['location']) || $dataValidation->isEmpty($inputJson['location'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Address required",
                    'data' => null
                ];
                return $response;
            }

          
            
            Log::debug("Saving the venue : ");
            $qryResponse = $venueDao->addVenue($inputJson);
            Log::debug("Saved the venue ");

          
            if(isset($qryResponse['status'])){
                if($qryResponse['status'] == 0){
                    //Send the success response now
                    $response = [
                        'status' => 0,
                        'message' =>  $qryResponse["message"],
                        'data' => null
                    ];
                }else if($qryResponse['status'] == 1){                
                    $response = [
                        'status' => $codes->RC_ERROR,
                        'message' => $qryResponse["message"],
                        'data' => null
                    ];
                }
            }else{
                $response = [
                    'status' => $codes->RC_SUCCESS,
                    'message' => 'venue Created Succefully',
                    'data' => $qryResponse
                ];
            }

            return $response;

        } catch (\Exception $e) {
            Log::debug($e);
            throw new Exception($e);
        }
    }


    public function venueOtpVerify($inputJson)
    {
        Log::debug("Started ... addOrganization Service : " . json_encode($inputJson));
        try {
            $codes = new Codes;
            $dataValidation = new DataValidation;
            $dateUtils = new DateUtils;
            $venueDao = new VenueDao;

        
            //User validation
            if (!isset($inputJson['user_id']) || $dataValidation->isEmpty($inputJson['user_id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your user ID",
                    'data' => null
                ];
                return $response;
            }
            //text validation
            if (!isset($inputJson['venue_id']) || $dataValidation->isEmpty($inputJson['venue_id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "venue_name required",
                    'data' => null
                ];
                return $response;
            }
            if (!isset($inputJson['otp']) || $dataValidation->isEmpty($inputJson['otp'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "venue_name required",
                    'data' => null
                ];
                return $response;
            }
            
          
            
            Log::debug("Saving the venue : ");
            $qryResponse = $venueDao->venueOtpVerify($inputJson['venue_id'] ,$inputJson['user_id'], $inputJson['otp']);
            Log::debug("Saved the venue ");

          
            if(isset($qryResponse['status'])){
                if($qryResponse['status'] == 0){
                    //Send the success response now
                    $response = [
                        'status' => $codes->RC_ERROR,
                        'message' =>  $qryResponse["message"],
                        'data' => null
                    ];
                }else if($qryResponse['status'] == 1){                
                    $response = [
                        'status' => $codes->RC_SUCCESS,
                        'message' => $qryResponse["message"],
                        'data' => null
                    ];
                }
            }else{
                $response = [
                    'status' => $codes->RC_SUCCESS,
                    'message' => 'verification status',
                    'data' => $qryResponse
                ];
            }

            return $response;

        } catch (\Exception $e) {
            Log::debug($e);
            throw new Exception($e);
        }
    }



    public function editVenue($inputJson)
    {
        Log::debug("Started ... addOrganization Service : " . json_encode($inputJson));
        try {
            $codes = new Codes;
            $dataValidation = new DataValidation;
            $dateUtils = new DateUtils;
            $venueDao = new VenueDao;

        
            //User validation
            if (!isset($inputJson['user_id']) || $dataValidation->isEmpty($inputJson['user_id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your user ID",
                    'data' => null
                ];
                return $response;
            }
            //text validation
            if (!isset($inputJson['venue_id']) || $dataValidation->isEmpty($inputJson['venue_id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "venue_name required",
                    'data' => null
                ];
                return $response;
            }
            
            
          
            
            Log::debug("Saving the venue : ");
            $qryResponse = $venueDao->editVenue($inputJson);
            Log::debug("Saved the venue ");

            if(isset($qryResponse['status'])){
                if($qryResponse['status'] == 0){
                    //Send the success response now
                    $response = [
                                    'status' => $codes->RC_ERROR,
                                    'message' =>  $qryResponse["message"],
                                    'data' => null
                                ];
                            }else if($qryResponse['status'] == 1){                
                                $response = [
                                    'status' => $codes->RC_SUCCESS,
                                    'message' => $qryResponse["message"],
                                    'data' => null
                                ];
                            }
                        }else{
                            $response = [
                                'status' => $codes->RC_SUCCESS,
                                'message' => 'verification status',
                                'data' => $qryResponse
                            ];
                        }

            return $response;

        } catch (\Exception $e) {
            Log::debug($e);
            throw new Exception($e);
        }
    }

    public function deleteVenueCover($inputJson)
    {
        Log::debug("Started ... deleteVenueCover Service : " . json_encode($inputJson));
        try {
            $codes = new Codes;
            $dataValidation = new DataValidation;
            $dateUtils = new DateUtils;
            $venueDao = new VenueDao;

        
            //User validation
            if (!isset($inputJson['photo']) || $dataValidation->isEmpty($inputJson['photo'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your QUERY PHOTO",
                    'data' => null
                ];
                return $response;
            }
            //text validation
            if (!isset($inputJson['user_id']) || $dataValidation->isEmpty($inputJson['user_id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "user_id required",
                    'data' => null
                ];
                return $response;
            }
            if (!isset($inputJson['venue_id']) || $dataValidation->isEmpty($inputJson['venue_id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "venue_id required",
                    'data' => null
                ];
                return $response;
            }
            
            
          
            
            Log::debug("Saving the venue : ");
            $qryResponse = $venueDao->deleteVenueCover($inputJson['photo'],$inputJson['venue_id'], $inputJson['user_id']);
            Log::debug("Saved the venue ");
            if(isset($qryResponse['status'])){
                if($qryResponse['status'] == 0){
                    //Send the success response now
                    $response = [
                                    'status' => $codes->RC_SUCCESS,
                                    'message' =>  $qryResponse["message"],
                                    'data' => null
                                ];
                            }else if($qryResponse['status'] == 1){                
                                $response = [
                                    'status' => $codes->RC_SUCCESS,
                                    'message' => $qryResponse["message"],
                                    'data' => null
                                ];
                            }
                        }else{
                            $response = [
                                'status' => $codes->RC_SUCCESS,
                                'message' => 'verification status',
                                'data' => $qryResponse
                            ];
                        }

            return $response;

        } catch (\Exception $e) {
            Log::debug($e);
            throw new Exception($e);
        }
    }

    public function addVenueCover($inputJson)
    {
        Log::debug("Started ... addVenueCover Service : " . json_encode($inputJson));
        try {
            $codes = new Codes;
            $dataValidation = new DataValidation;
            $dateUtils = new DateUtils;
            $venueDao = new VenueDao;

        
            //User validation
            if (!isset($_FILES['logo'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your PHOTO",
                    'data' => null
                ];
                return $response;
            }
            //text validation
            if (!isset($inputJson['user_id']) || $dataValidation->isEmpty($inputJson['user_id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "USER_ID required",
                    'data' => null
                ];
                return $response;
            }
            if (!isset($inputJson['venue_id']) || $dataValidation->isEmpty($inputJson['venue_id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "venue_ID required",
                    'data' => null
                ];
                return $response;
            }
            
            
          
            
            Log::debug("Saving the venue : ");
            $qryResponse = $venueDao->addVenueCover($inputJson,$inputJson['venue_id'], $inputJson['user_id']);
            Log::debug("Saved the venue ");
            if(isset($qryResponse['status'])){
                if($qryResponse['status'] == 0){
                    //Send the success response now
                    $response = [
                                    'status' => $codes->RC_SUCCESS,
                                    'message' =>  $qryResponse["message"],
                                    'data' => null
                                ];
                            }else if($qryResponse['status'] == 1){                
                                $response = [
                                    'status' => $codes->RC_SUCCESS,
                                    'message' => $qryResponse["message"],
                                    'data' => null
                                ];
                            }
                        }else{
                            $response = [
                                'status' => $codes->RC_SUCCESS,
                                'message' => 'verification status',
                                'data' => $qryResponse
                            ];
                        }

            return $response;

        } catch (\Exception $e) {
            Log::debug($e);
            throw new Exception($e);
        }
    }


    public function getVenue($inputJson)
    {
        Log::debug("Started ... getVenue Service : " . json_encode($inputJson));
        try {
            $codes = new Codes;
            $dataValidation = new DataValidation;
            $dateUtils = new DateUtils;
            $venueDao = new VenueDao;

        
            //User validation
            if (!isset($inputJson['user_id']) || $dataValidation->isEmpty($inputJson['user_id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your user ID",
                    'data' => null
                ];
                return $response;
            }
            //text validation
            if (!isset($inputJson['venue_id']) || $dataValidation->isEmpty($inputJson['venue_id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "venue_id required",
                    'data' => null
                ];
                return $response;
            }
            
            
          
            
            Log::debug("Saving the venue : ");
            $qryResponse = $venueDao->getVenue($inputJson['venue_id'], $inputJson['user_id']);
            Log::debug("Saved the venue ");

          
            
                $response = [
                    'status' => $codes->RC_SUCCESS,
                    'message' => 'get venue',
                    'data' => $qryResponse
                ];
            

            return $response;

        } catch (\Exception $e) {
            Log::debug($e);
            throw new Exception($e);
        }
    }


    public function getCampusDriveList($inputJson)
    {
           Log::debug("Started ... getCampusDriveList Service : ".json_encode($inputJson));
            try{
                $venueDao = new VenueDao;
                $codes = new Codes;
                $dataValidation = new DataValidation;

                //Validation to be done here ---
                if(!isset($inputJson['venue_id']))
                {
                    $response = [
                        'status'=>$codes->RC_ERROR,
                        'message'=>$codes->RM_ALL_FIELDS_TO_BE_PRESENT,
                        'data'=>null
                    ];
    
                    return $response;                
                }

                if(!isset($inputJson['user_id']))
                {
                    $response = [
                        'status'=>$codes->RC_ERROR,
                        'message'=>$codes->RM_ALL_FIELDS_TO_BE_PRESENT,
                        'data'=>null
                    ];
    
                    return $response;                
                }
                  
                $campusDriveList = $venueDao->getCampusDetailByOrganizationId($inputJson['user_id'], $inputJson['venue_id'], $inputJson['offset']);

                $response = [
                    'status'=>$codes->RC_SUCCESS,
                    'message'=>"campusDrive List",
                    'data'=> $campusDriveList
                ];
    
                Log::debug("Ended ... getCampusDriveList Service : ");
    
                return $response;            
            }catch(\Exception $e){
                Log::debug($e);
                throw new Exception($e);
            }
    }

    

    public function getVenuePosts($inputJson)
    {
        Log::debug("Started ... getVenuePosts Service : ".json_encode($inputJson));
        try{

            $codes = new Codes;
            $dataValidation = new DataValidation;
            $organizationDao = new VenueDao;
            $imageUtils = new ImageUtils;
            $organizationTypeDao = new OrganizationTypeDao;
            $userOrganizerDao = new UserOrganizerDao;
            $scheduleCampusDao = new ScheduleCampusDao;
            $campusDetailDao = new CampusDetailDao;
            $postsDao = new PostsDao;

            //User validation
            if (!isset($inputJson['user_id']) || $dataValidation->isEmpty($inputJson['user_id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your user ID",
                    'data' => null
                ];
                return $response;
            }
            //User validation
            if (!isset($inputJson['venue_id']) || $dataValidation->isEmpty($inputJson['venue_id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your organization ID",
                    'data' => null
                ];
                return $response;
            }
            if (!isset($inputJson['posttype']) || $dataValidation->isEmpty($inputJson['posttype'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your posttype",
                    'data' => null
                ];
                return $response;
            }
            $type = $inputJson['posttype'];
            
            
            $organizationType = $organizationDao->getVenuePosts($inputJson, $type);
            
            $response = [
                'status' => $codes->RC_SUCCESS,
                'message' => "getVenuePosts List",
                'data' => $organizationType
            ];

            Log::debug("Ended ... getOrganizationPosts Service : ");

            return $response;






        }catch (\Exception $e) {
            Log::debug($e);
            throw new Exception($e);
        }
    }


    public function collegeStudentData($inputJson)
    {
           Log::debug("Started ... collegeStudentData Service : ".json_encode($inputJson));
            try{
                $venueDao = new VenueDao;
                $codes = new Codes;
                $dataValidation = new DataValidation;

                //Validation to be done here ---
                if(!isset($inputJson['venue_id']))
                {
                    $response = [
                        'status'=>$codes->RC_ERROR,
                        'message'=>$codes->RM_ALL_FIELDS_TO_BE_PRESENT,
                        'data'=>null
                    ];
    
                    return $response;                
                }

                if(!isset($inputJson['user_id']))
                {
                    $response = [
                        'status'=>$codes->RC_ERROR,
                        'message'=>$codes->RM_ALL_FIELDS_TO_BE_PRESENT,
                        'data'=>null
                    ];
    
                    return $response;                
                }
                if(!isset($inputJson['assessment_type']) || $dataValidation->isEmpty($inputJson['assessment_type']))
                {
                    $inputJson['assessment_type'] = 0;                
                }
                if(!isset($inputJson['year']) || $dataValidation->isEmpty($inputJson['year']))
                {
                    $inputJson['year'] = 0;                
                }
                  
                $campusDriveList = $venueDao->collegeStudentData($inputJson['user_id'], $inputJson['venue_id'] , $inputJson['assessment_type'] , $inputJson['year']);

                $response = [
                    'status'=>$codes->RC_SUCCESS,
                    'message'=>"venue data",
                    'data'=> $campusDriveList
                ];
    
                Log::debug("Ended ... collegeStudentData Service : ");
    
                return $response;            
            }catch(\Exception $e){
                Log::debug($e);
                throw new Exception($e);
            }
    }

    public function collegeStudentList($inputJson)
    {
           Log::debug("Started ... collegeStudentList Service : ".json_encode($inputJson));
            try{
                $venueDao = new VenueDao;
                $codes = new Codes;
                $dataValidation = new DataValidation;

                //Validation to be done here ---
                if(!isset($inputJson['venue_id']))
                {
                    $response = [
                        'status'=>$codes->RC_ERROR,
                        'message'=>$codes->RM_ALL_FIELDS_TO_BE_PRESENT,
                        'data'=>null
                    ];
    
                    return $response;                
                }

                if(!isset($inputJson['user_id']))
                {
                    $response = [
                        'status'=>$codes->RC_ERROR,
                        'message'=>$codes->RM_ALL_FIELDS_TO_BE_PRESENT,
                        'data'=>null
                    ];
    
                    return $response;                
                }
                if(!isset($inputJson['type']) || $dataValidation->isEmpty($inputJson['type']))
                {
                    $response = [
                        'status'=>$codes->RC_ERROR,
                        'message'=>$codes->RM_ALL_FIELDS_TO_BE_PRESENT,
                        'data'=>null
                    ];
    
                    return $response;                
                }
                if(!isset($inputJson['assessment_type']) || $dataValidation->isEmpty($inputJson['assessment_type']))
                {
                    $inputJson['assessment_type'] = 0;                
                }
                if(!isset($inputJson['year']) || $dataValidation->isEmpty($inputJson['year']))
                {
                    $inputJson['year'] = 0;                
                }
                  
                $campusDriveList = $venueDao->collegeStudentList($inputJson['user_id'], $inputJson['venue_id'] , $inputJson['assessment_type'] , $inputJson['year'] , $inputJson['type'] , isset($inputJson['offset'])?$inputJson['offset']:0 );

                $response = [
                    'status'=>$codes->RC_SUCCESS,
                    'message'=>"venue data",
                    'data'=> $campusDriveList
                ];
    
                Log::debug("Ended ... collegeStudentList Service : ");
    
                return $response;            
            }catch(\Exception $e){
                Log::debug($e);
                throw new Exception($e);
            }
    }

    

}