<?php

namespace App\ServiceLayer;

use App\Utils\Codes;
use App\Utils\DataValidation;
use Cake\Core\Exception\Exception;
use App\DaoLayer\UsersDao;
use App\DaoLayer\UserSessionDao;
use App\Utils\DateUtils;
use App\Utils\ImageUtils;
use Cake\Log\Log;
use App\DaoLayer\QuickBitsDao;
use App\DaoLayer\PostsDao;

class QuickBitsService
{

    private function uploadMultiImage($UPLOADS_DIRECTORY){


        $folder = 'photos';
        $depth = '../';
        if(!file_exists($depth.$UPLOADS_DIRECTORY.'/'.$folder)) {
            @mkdir($depth.$UPLOADS_DIRECTORY.'/'.$folder, 0777, true);
        }
        if(!file_exists($depth.$UPLOADS_DIRECTORY.'/'.$folder.'/' . date('Y'))) {
            @mkdir($depth.$UPLOADS_DIRECTORY.'/'.$folder.'/' . date('Y'), 0777, true);
        }
        if(!file_exists($UPLOADS_DIRECTORY.'/'.$folder.'/' . date('Y') . '/' . date('m'))) {
            @mkdir($depth.$UPLOADS_DIRECTORY.'/'.$folder.'/' . date('Y') . '/' . date('m'), 0777, true);
        }

        $files = array();
        if(isset($_FILES['picture']) && !empty($_FILES['picture'])){
            foreach($_FILES['picture'] as $key => $val) {
                for($i=0; $i < count($val); $i++) { //if we have to skip a card makes an array with key as i and value as image or not
                    $files[$i][$key] = @$val[$i];
                }
            }
        }
      
        
        $files_num = count($files);
        foreach ($files as $key=>$file) {            
            $dimensions =  '';
            $image_new_name = '';
            if(isset($file["tmp_name"]) && !empty($file["tmp_name"])){
                // valid inputs
                if(!isset($file) || $file["error"] != UPLOAD_ERR_OK) {
                    if($files_num > 1) {
                        continue;
                    } else {
                        throw new Exception("Something wrong with upload! Is 'upload_max_filesize' set correctly?");
                    }
                }

                /* prepare new file name */
                $directory = $folder.'/'. date('Y') . '/' . date('m') . '/';
                $prefix = md5(time()*rand(1, 9999));

                $imgArr = explode('.', $file["name"]);
                $extension = end($imgArr);
                $get_dimensions = getimagesize($file["tmp_name"]);
                $dimensions =  $get_dimensions[1].'X'.$get_dimensions[0];
                $image_new_name = $directory.$prefix.'.'.$extension;
                $path_new = $depth.$UPLOADS_DIRECTORY.'/'.$image_new_name;

                /* check if the file uploaded successfully */
                if(!@move_uploaded_file($file['tmp_name'], $path_new)) {
                    if($files_num > 1) {
                        continue;
                    } else {
                        throw new Exception("Sorry, can not upload the file");
                    }
                } 

            }
            /* return */
            $return_files[$key]['image'] = @$image_new_name;
            $return_files[$key]['dimensions'] = @$dimensions;
        }
        
        return @$return_files;
    }


    public function createQuickBit($inputJson)
    {
        Log::debug("Started ... createQuickBit Service : " . json_encode($inputJson));
        try {
            $codes = new Codes;
            $dataValidation = new DataValidation;
            $userSessionDao = new UserSessionDao;
            $usersDao = new UsersDao;
            $imageUtils = new ImageUtils;
            $dateUtils = new DateUtils;
            $quickBitsDao = new QuickBitsDao;
            $postsDao = new PostsDao;

            //Validations to be done
            //Title validation
            if (!isset($inputJson['title']) || $dataValidation->isEmpty($inputJson['title'])) {
                $inputJson['title'] = '';
            }

            //User type validation
            if (!isset($inputJson['user_type']) || $dataValidation->isEmpty($inputJson['user_type'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your user type",
                    'data' => null
                ];
                return $response;
            }

            //creator validation
            if (!isset($inputJson['creator_id']) || $dataValidation->isEmpty($inputJson['creator_id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter creator",
                    'data' => null
                ];
                return $response;
            }
            //Questions validation
            if (!isset($inputJson['card_text']) || !is_array($inputJson['card_text']) || count($inputJson['card_text']) < 1) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter card text",
                    'data' => null
                ];
                return $response;
            }
            //color code validation
            if (!isset($inputJson['color_code']) || !is_array($inputJson['color_code']) || count($inputJson['color_code']) < 1) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter color code",
                    'data' => null
                ];
                return $response;
            }
            //interests validation
            if (!isset($inputJson['interests']) || $dataValidation->isEmpty($inputJson['interests'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter interests",
                    'data' => null
                ];
                return $response;
            }
           
            //uploading image
            $card_pictures = $this->uploadMultiImage($codes->UPLOADS_DIRECTORY);
            Log::debug("Saving the quickBit : ");
            $quickBitId = $quickBitsDao->saveQuickBits($inputJson, $card_pictures, $dateUtils->getCurrentDate());
            Log::debug("Saved the quickBit ");
            // if($quickBitId){
            //     $postsDao->saveQuickBitPosts($quickBitId, $inputJson['creator_id'], $inputJson['title'], $dateUtils->getCurrentDate());
            // }


            Log::debug('quickBit : ' . json_encode($quickBitId));

            //Send the success response now
            $response = [
                'status' => $codes->RC_SUCCESS,
                'message' => $codes->RM_SUCCESS,
                'data' => $quickBitsDao->getQuickBitByUserAndId($quickBitId, $inputJson['creator_id'], $dateUtils->getCurrentDate())
            ];
            return $response;

            Log::debug("Ended ... quickBit Service : ");
        } catch (\Exception $e) {
            Log::debug($e);
            throw new Exception($e);
        }
    }
    public function updateQuickBit($inputJson)
    {
        Log::debug("Started ... updateQuickBit Service : " . json_encode($inputJson));
        try {
            $codes = new Codes;
            $dataValidation = new DataValidation;
            $userSessionDao = new UserSessionDao;
            $usersDao = new UsersDao;
            $imageUtils = new ImageUtils;
            $dateUtils = new DateUtils;
            $quickBitsDao = new QuickBitsDao;

            //Validations to be done
            //Title validation
            if (!isset($inputJson['title']) || $dataValidation->isEmpty($inputJson['title'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your title",
                    'data' => null
                ];
                return $response;
            }

            //User type validation
            if (!isset($inputJson['user_type']) || $dataValidation->isEmpty($inputJson['user_type'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your user type",
                    'data' => null
                ];
                return $response;
            }

            //creator validation
            if (!isset($inputJson['creator_id']) || $dataValidation->isEmpty($inputJson['creator_id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter creator",
                    'data' => null
                ];
                return $response;
            }
            //Questions validation
            if (!isset($inputJson['card_text']) || !is_array($inputJson['card_text']) || count($inputJson['card_text']) < 1) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter card text",
                    'data' => null
                ];
                return $response;
            }
            //color code validation
            if (!isset($inputJson['color_code']) || !is_array($inputJson['color_code']) || count($inputJson['color_code']) < 1) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter color code",
                    'data' => null
                ];
                return $response;
            }
            //interests validation
            if (!isset($inputJson['interests']) || $dataValidation->isEmpty($inputJson['interests'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter interests",
                    'data' => null
                ];
                return $response;
            }
            //quickbit id validation
            if (!isset($inputJson['quickbit_id']) || $dataValidation->isEmpty($inputJson['quickbit_id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter quickbit id",
                    'data' => null
                ];
                return $response;
            }
           
            //uploading image
            $card_pictures = $this->uploadMultiImage($codes->UPLOADS_DIRECTORY);
            Log::debug("Saving the quickBit : ");
            $quickBitId = $quickBitsDao->updateQuickBits($inputJson, $card_pictures, $dateUtils->getCurrentDate());
            Log::debug("Saved the quickBit ");

            Log::debug('quickBit : ' . json_encode($quickBitId));

            //Send the success response now
            $response = [
                'status' => $codes->RC_SUCCESS,
                'message' => $codes->RM_SUCCESS,
                'data' => $quickBitsDao->getQuickBitByUserAndId($quickBitId, $inputJson['creator_id'], $dateUtils->getCurrentDate())
            ];
            return $response;

            Log::debug("Ended ... quickBit Service : ");
        } catch (\Exception $e) {
            Log::debug($e);
            throw new Exception($e);
        }
    }

    public function updateQuickBit0($inputJson)
    {
        Log::debug("Started ... updateQuickBit Service : " . json_encode($inputJson));
        try {
            $codes = new Codes;
            $dataValidation = new DataValidation;
            $userSessionDao = new UserSessionDao;
            $usersDao = new UsersDao;
            $imageUtils = new ImageUtils;
            $dateUtils = new DateUtils;
            $quickBitsDao = new QuickBitsDao;
            $quickBitsQuestionsDao = new QuickBitzesQuestionsDao;
            $quickBitsQuestionsOptionsDao = new QuickBitzesQuestionsOptionsDao;
            $postsDao = new PostsDao;

            //Validations to be done
            //Id validation
            if (!isset($inputJson['quickbit_id']) || $dataValidation->isEmpty($inputJson['quickbit_id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your quickBit ID",
                    'data' => null
                ];
                return $response;
            }
            //Title validation
            if (!isset($inputJson['title']) || $dataValidation->isEmpty($inputJson['title'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your title",
                    'data' => null
                ];
                return $response;
            }
            //Description validation
            if (!isset($inputJson['description']) || $dataValidation->isEmpty($inputJson['description'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your description",
                    'data' => null
                ];
                return $response;
            }
            //Picture validation
            // if (!isset($inputJson['picture']) || $dataValidation->isEmpty($inputJson['picture'])) {
            //     $response = [
            //         'status' => $codes->RC_ERROR,
            //         'message' => "Please enter your picture",
            //         'data' => null
            //     ];
            //     return $response;
            // }
            //User type validation
            if (!isset($inputJson['user_type']) || $dataValidation->isEmpty($inputJson['user_type'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your user type",
                    'data' => null
                ];
                return $response;
            }
            //QuickBit start time validation
            if (!isset($inputJson['start_time']) || $dataValidation->isEmpty($inputJson['start_time'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your user type",
                    'data' => null
                ];
                return $response;
            }
            //QuickBit live duration validation
            if (!isset($inputJson['live_duration']) || $dataValidation->isEmpty($inputJson['live_duration'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter live duration",
                    'data' => null
                ];
                return $response;
            }
            //creator validation
            if (!isset($inputJson['creator_id']) || $dataValidation->isEmpty($inputJson['creator_id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter creator",
                    'data' => null
                ];
                return $response;
            }
           

            //uploading images
            if(isset($_FILES["quickBit_picture"]['name']) && !empty($_FILES["quickBit_picture"]['name'])) {
                $quickBit_picture = $this->uploadImage($codes->UPLOADS_DIRECTORY);
            }

            Log::debug("Saving the quickBit : ");
            $qryStatus = $quickBitsDao->updateQuickBits($inputJson, @$quickBit_picture['image'], @$quickBit_picture['dimensions']);
            Log::debug("Saved the quickBit ");
            if($qryStatus){
                $quickBitsQuestionsDao->deleteQuestionsByQuickBitId($inputJson['quickbit_id']);
                //uploading image
                $question_pictures = $this->uploadMultiImage($codes->UPLOADS_DIRECTORY);
                foreach ($inputJson['question'] as $i => $q) {
                    // checking answers options 
                    $answers = [];
                    if (isset($inputJson['answers'][$i]) && !empty($inputJson['answers'][$i])) {
                        $aswArr = explode('#*#', $inputJson['answers'][$i]);
                        foreach ($aswArr as $aswkey => $ans)
                            array_push($answers, ["option_text"=>$ans, "is_correct"=>((isset($inputJson['correct'][$i]) && $aswkey == ($inputJson['correct'][$i]-1))?1:0)]);
                    }
                    // seting array of answers in current format
                    $question = [
                        "question"=>$q,
                        "picture"=>'',
                        "time_alloted"=> @$inputJson['time_alloted'][$i],
                        "options"=> $answers,
                        "image_file_name"=> @$question_pictures[$i]['image'],
                        "image_file_dimensions"=> @$question_pictures[$i]['dimensions'],
                    ];
                    $quickBitQuestionsId = $quickBitsQuestionsDao->saveQuickBitzesQuestions($question, $inputJson['quickbit_id']);

                    if($quickBitQuestionsId && $question['options'] && count($question['options']) > 0){
                        foreach ($question['options'] as $key => $optionRow) {
                            $quickBitsQuestionsOptionsDao->saveQuickBitzesQuestionsOptions($optionRow, $inputJson['quickbit_id'], $quickBitQuestionsId);
                        }
                    }
                }

                // updating post
                $postsDao->updatePostsTitleByQuickBitId($inputJson['quickbit_id'], $inputJson['title'], $dateUtils->getCurrentDate());
            }


            Log::debug('quickBit : ' . json_encode($inputJson['quickbit_id']));

            //Send the success response now
            $response = [
                'status' => $codes->RC_SUCCESS,
                'message' => $codes->RM_SUCCESS,
                'data' => $quickBitsDao->getQwizByUserAndId($inputJson['quickbit_id'], $inputJson['creator_id'], $dateUtils->getCurrentDate())
            ];
            return $response;

            Log::debug("Ended ... quickBit Service : ");
        } catch (\Exception $e) {
            Log::debug($e);
            throw new Exception($e);
        }
    }
    public function deleteQuickBit($inputJson)
    {
        Log::debug("Started ... deleteQuickBit Service : " . json_encode($inputJson));
        try {
            $codes = new Codes;
            $dataValidation = new DataValidation;
            $userSessionDao = new UserSessionDao;
            $usersDao = new UsersDao;
            $dateUtils = new DateUtils;
            $quickBitsDao = new QuickBitsDao;
            $postsDao = new PostsDao;

            //Validations to be done
            //Id validation
            if (!isset($inputJson['quickbit_id']) || $dataValidation->isEmpty($inputJson['quickbit_id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your quickBit ID",
                    'data' => null
                ];
                return $response;
            }
           
            //creator validation
            if (!isset($inputJson['creator_id']) || $dataValidation->isEmpty($inputJson['creator_id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter creator",
                    'data' => null
                ];
                return $response;
            }

            Log::debug("Deleting the quickBit : ");
            $qryStatus = $quickBitsDao->deleteQuickBits($inputJson['quickbit_id'], $inputJson['creator_id']);
            Log::debug("Deleted the quickBit ");

            Log::debug('quickBit : ' . json_encode($inputJson['quickbit_id']));

            //Send the success response now
            $response = [
                'status' => $codes->RC_SUCCESS,
                'message' => $codes->RM_SUCCESS,
                'data' => ['deleted'=>true]
            ];
            return $response;

            Log::debug("Ended ... quickBit Service : ");
        } catch (\Exception $e) {
            Log::debug($e);
            throw new Exception($e);
        }
    }

    public function getMyQuickBit($inputJson)
    {
        Log::debug("Started ... getMyQuickBit Service : " . json_encode($inputJson));
        try {
            $codes = new Codes;
            $dataValidation = new DataValidation;
            $quickBitsDao = new QuickBitsDao;
            $dateUtils = new DateUtils;

            //Validations to be done
            //Id validation
            if (!isset($inputJson['quickbit_id']) || $dataValidation->isEmpty($inputJson['quickbit_id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your quickBit ID",
                    'data' => null
                ];
                return $response;
            }
           
            //creator validation
            if (!isset($inputJson['user_id']) || $dataValidation->isEmpty($inputJson['user_id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter user ID",
                    'data' => null
                ];
                return $response;
            }
            Log::debug("getting the quickBit : ");
            $quickBit = $quickBitsDao->getQuickBitByUserAndId($inputJson['quickbit_id'], $inputJson['user_id'], $dateUtils->getCurrentDate());
            Log::debug("Got the quickBit ");

            //Send the success response now
            $response = [
                'status' => $codes->RC_SUCCESS,
                'message' => $codes->RM_SUCCESS,
                'data' => ['quickBit'=>empty($quickBit)?(object)[]:$quickBit]
            ];
            return $response;

            Log::debug("Ended ... quickBit Service : ");
        } catch (\Exception $e) {
            Log::debug($e);
            throw new Exception($e);
        }
    }
    public function getQuickBitsList($inputJson)
    {
        Log::debug("Started ... getQuickBitsList Service : " . json_encode($inputJson));
        try {
            $codes = new Codes;
            $quickBitsDao = new QuickBitsDao;
            $dateUtils = new DateUtils;
            $dataValidation = new DataValidation;

            //User validation
            if (!isset($inputJson['user_id']) || $dataValidation->isEmpty($inputJson['user_id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your user ID",
                    'data' => null
                ];
                return $response;
            }

            Log::debug("getting the quickBits : ");
            $quickBitsList = $quickBitsDao->getQuickBitsList($inputJson, $dateUtils->getCurrentDate());
            Log::debug("Got the quickBit ");

            //Send the success response now
            $response = [
                'status' => $codes->RC_SUCCESS,
                'message' => $codes->RM_SUCCESS,
                'data' => ['quickBitsList'=>$quickBitsList?$quickBitsList:[]]
            ];
            return $response;

            Log::debug("Ended ... quickBit Service : ");
        } catch (\Exception $e) {
            Log::debug($e);
            throw new Exception($e);
        }
    }



}
