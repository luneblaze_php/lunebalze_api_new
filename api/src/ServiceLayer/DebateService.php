<?php

namespace App\ServiceLayer;

use App\Utils\Codes;
use App\Utils\DataValidation;
use Cake\Core\Exception\Exception;
use App\DaoLayer\UsersDao;
use App\Utils\DateUtils;
use Cake\Log\Log;
use App\DaoLayer\DebatesDao;
use App\DaoLayer\DebatesQuestionsDao;
use App\DaoLayer\DebatesQuestionsOptionsDao;
use App\DaoLayer\PostsDao;

class DebateService
{

    private function uploadImage($UPLOADS_DIRECTORY, $inputJson){
        $folder = 'photos';
        $depth = '../';
        if(!file_exists($depth.$UPLOADS_DIRECTORY.'/'.$folder)) {
            @mkdir($depth.$UPLOADS_DIRECTORY.'/'.$folder, 0777, true);
        }
        if(!file_exists($depth.$UPLOADS_DIRECTORY.'/'.$folder.'/' . date('Y'))) {
            @mkdir($depth.$UPLOADS_DIRECTORY.'/'.$folder.'/' . date('Y'), 0777, true);
        }
        if(!file_exists($UPLOADS_DIRECTORY.'/'.$folder.'/' . date('Y') . '/' . date('m'))) {
            @mkdir($depth.$UPLOADS_DIRECTORY.'/'.$folder.'/' . date('Y') . '/' . date('m'), 0777, true);
        }

        // valid inputs
        if(!isset($_FILES["debate_picture"]) || $_FILES["debate_picture"]["error"] != UPLOAD_ERR_OK) {
            throw new Exception("Something wrong with upload! Is 'upload_max_filesize' set correctly?");
        }
        $directory = $folder.'/'. date('Y') . '/' . date('m') . '/';
        $prefix = md5(time()*rand(1, 9999));
        $imgArr = explode('.', $_FILES['debate_picture']['name']);
        $extension = end($imgArr);
        $get_dimensions = getimagesize($_FILES['debate_picture']['tmp_name']);
        $dimensions = $get_dimensions[1].'X'.$get_dimensions[0];
            
        $image_new_name = $directory.$prefix.'.'.$extension;
        $path_new = $depth.$UPLOADS_DIRECTORY.'/'.$image_new_name;

        /* check if the file uploaded successfully */
        if(!@move_uploaded_file($_FILES['debate_picture']['tmp_name'], $path_new)) {
            throw new Exception("Sorry, can not upload the photos");
        }
                
        /* return */
        $return_files['image'] = $image_new_name;
        $return_files['dimensions'] = $dimensions;
        return $return_files;
    }


    public function createDebate($inputJson)
    {
        Log::debug("Started ... createDebate Service : " . json_encode($inputJson));
        try {
            $codes = new Codes;
            $dataValidation = new DataValidation;
            $usersDao = new UsersDao;
            $dateUtils = new DateUtils;
            $debatesDao = new DebatesDao;
            $postsDao = new PostsDao;

            //Validations to be done
            //Title validation
            if (!isset($inputJson['title']) || $dataValidation->isEmpty($inputJson['title'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your title",
                    'data' => null
                ];
                return $response;
            }
            //Description validation
            if (!isset($inputJson['description']) || $dataValidation->isEmpty($inputJson['description'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your description",
                    'data' => null
                ];
                return $response;
            }
            //User type validation
            if (!isset($inputJson['user_type']) || $dataValidation->isEmpty($inputJson['user_type'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your user type",
                    'data' => null
                ];
                return $response;
            }
            //Debate start time validation
            if (!isset($inputJson['start_time']) || $dataValidation->isEmpty($inputJson['start_time'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your user type",
                    'data' => null
                ];
                return $response;
            }
            //Debate live duration validation
            if (!isset($inputJson['live_duration']) || $dataValidation->isEmpty($inputJson['live_duration'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter live duration",
                    'data' => null
                ];
                return $response;
            }
            //creator validation
            if (!isset($inputJson['creator_id']) || $dataValidation->isEmpty($inputJson['creator_id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter creator",
                    'data' => null
                ];
                return $response;
            }
            if (!isset($inputJson['interests']) || $dataValidation->isEmpty($inputJson['interests'])) {
                $inputJson['interests'] = '';
            }
           
            //uploading images
            if(!isset($_FILES['debate_picture']) or $_FILES['debate_picture'] == null or $_FILES["debate_picture"]["error"] != UPLOAD_ERR_OK){
                $debate_picture['image'] = '';$debate_picture['dimensions'] = '';
            }else{
                $debate_picture = $this->uploadImage($codes->UPLOADS_DIRECTORY, $inputJson);
            }

            Log::debug("Saving the debate : ");
            $debateId = $debatesDao->saveDebates($inputJson, $debate_picture['image'], $debate_picture['dimensions']);
            Log::debug("Saved the debate ");
            //return $debateId;

            Log::debug('debate : ' . json_encode($debateId));

            //Send the success response now
            $response = [
                'status' => $codes->RC_SUCCESS,
                'message' => $codes->RM_SUCCESS,
                'data' => $debatesDao->getDebateByUserAndId($debateId, $inputJson['creator_id'], $dateUtils->getCurrentDate())
            ];
            return $response;

            Log::debug("Ended ... debate Service : ");
        } catch (\Exception $e) {
            Log::debug($e);
            throw new Exception($e);
        }
    }

    public function updateDebate($inputJson)
    {
        Log::debug("Started ... updateDebate Service : " . json_encode($inputJson));
        try {
            $codes = new Codes;
            $dataValidation = new DataValidation;
            $usersDao = new UsersDao;
            $dateUtils = new DateUtils;
            $debatesDao = new DebatesDao;
            $postsDao = new PostsDao;

            //Validations to be done
            //Id validation
            if (!isset($inputJson['debate_id']) || $dataValidation->isEmpty($inputJson['debate_id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your debate ID",
                    'data' => null
                ];
                return $response;
            }
            //Title validation
            if (!isset($inputJson['title']) || $dataValidation->isEmpty($inputJson['title'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your title",
                    'data' => null
                ];
                return $response;
            }
            //Description validation
            if (!isset($inputJson['description']) || $dataValidation->isEmpty($inputJson['description'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your description",
                    'data' => null
                ];
                return $response;
            }
            //User type validation
            if (!isset($inputJson['user_type']) || $dataValidation->isEmpty($inputJson['user_type'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your user type",
                    'data' => null
                ];
                return $response;
            }
            //Debate start time validation
            if (!isset($inputJson['start_time']) || $dataValidation->isEmpty($inputJson['start_time'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your user type",
                    'data' => null
                ];
                return $response;
            }
            //Debate live duration validation
            if (!isset($inputJson['live_duration']) || $dataValidation->isEmpty($inputJson['live_duration'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter live duration",
                    'data' => null
                ];
                return $response;
            }
            //creator validation
            if (!isset($inputJson['creator_id']) || $dataValidation->isEmpty($inputJson['creator_id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter creator",
                    'data' => null
                ];
                return $response;
            }

            //uploading images
            if(isset($_FILES["debate_picture"]['name']) && !empty($_FILES["debate_picture"]['name'])) {
                $debate_picture = $this->uploadImage($codes->UPLOADS_DIRECTORY);
            }
           
            Log::debug("Saving the debate : ");
            $qryStatus = $debatesDao->updateDebates($inputJson, @$debate_picture['image'], @$debate_picture['dimensions']);
            Log::debug("Saved the debate ");
            if($qryStatus){
                // updating post
                $postsDao->updatePostsTitleByDebateId($inputJson['debate_id'], $inputJson['title'], $dateUtils->getCurrentDate());
            }


            Log::debug('debate : ' . json_encode($inputJson['debate_id']));

            //Send the success response now
            $response = [
                'status' => $codes->RC_SUCCESS,
                'message' => $codes->RM_SUCCESS,
                'data' => $debatesDao->getDebateByUserAndId($inputJson['debate_id'], $inputJson['creator_id'], $dateUtils->getCurrentDate())
            ];
            return $response;

            Log::debug("Ended ... debate Service : ");
        } catch (\Exception $e) {
            Log::debug($e);
            throw new Exception($e);
        }
    }
    public function deleteDebate($inputJson)
    {
        Log::debug("Started ... deleteDebate Service : " . json_encode($inputJson));
        try {
            $codes = new Codes;
            $dataValidation = new DataValidation;
            $usersDao = new UsersDao;
            $dateUtils = new DateUtils;
            $debatesDao = new DebatesDao;
            $postsDao = new PostsDao;

            //Validations to be done
            //Id validation
            if (!isset($inputJson['debate_id']) || $dataValidation->isEmpty($inputJson['debate_id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your debate ID",
                    'data' => null
                ];
                return $response;
            }
           
            //creator validation
            if (!isset($inputJson['creator_id']) || $dataValidation->isEmpty($inputJson['creator_id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter creator",
                    'data' => null
                ];
                return $response;
            }

            Log::debug("Deleting the debate : ");
            $qryStatus = $debatesDao->deleteDebates($inputJson['debate_id'], $inputJson['creator_id']);
            Log::debug("Deleted the debate ");
            if($qryStatus){
                // deleting post
                $postsDao->deletePostsByDebateId($inputJson['debate_id']);
            }


            Log::debug('debate : ' . json_encode($inputJson['debate_id']));

            //Send the success response now
            $response = [
                'status' => $codes->RC_SUCCESS,
                'message' => $codes->RM_SUCCESS,
                'data' => ['deleted'=>true]
            ];
            return $response;

            Log::debug("Ended ... debate Service : ");
        } catch (\Exception $e) {
            Log::debug($e);
            throw new Exception($e);
        }
    }

    public function getMyDebate($inputJson)
    {
        Log::debug("Started ... getMyDebate Service : " . json_encode($inputJson));
        try {
            $codes = new Codes;
            $dataValidation = new DataValidation;
            $debatesDao = new DebatesDao;
            $dateUtils = new DateUtils;

            //Validations to be done
            //Id validation
            if (!isset($inputJson['debate_id']) || $dataValidation->isEmpty($inputJson['debate_id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your debate ID",
                    'data' => null
                ];
                return $response;
            }
           
            //creator validation
            if (!isset($inputJson['user_id']) || $dataValidation->isEmpty($inputJson['user_id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter user ID",
                    'data' => null
                ];
                return $response;
            }
            Log::debug("getting the debate : ");
            $debate = $debatesDao->getDebateByUserAndId($inputJson['debate_id'], $inputJson['user_id'], $dateUtils->getCurrentDate());
            Log::debug("Got the debate ");

            //Send the success response now
            $response = [
                'status' => $codes->RC_SUCCESS,
                'message' => $codes->RM_SUCCESS,
                'data' => empty($debate)?(object)[]:$debate
            ];
            return $response;

            Log::debug("Ended ... debate Service : ");
        } catch (\Exception $e) {
            Log::debug($e);
            throw new Exception($e);
        }
    }
    public function getDebatesList($inputJson)
    {
        Log::debug("Started ... getDebatesList Service : " . json_encode($inputJson));
        try {
            $codes = new Codes;
            $debatesDao = new DebatesDao;
            $dateUtils = new DateUtils;
            $dataValidation = new DataValidation;

            //User validation
            if (!isset($inputJson['user_id']) || $dataValidation->isEmpty($inputJson['user_id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your user ID",
                    'data' => null
                ];
                return $response;
            }

            Log::debug("getting the debates : ");
            $debatesList = $debatesDao->getDebatesList($inputJson, $dateUtils->getCurrentDate());
            Log::debug("Got the debate ");

            //Send the success response now
            $response = [
                'status' => $codes->RC_SUCCESS,
                'message' => $codes->RM_SUCCESS,
                'data' => $debatesList?$debatesList:[]
            ];
            return $response;

            Log::debug("Ended ... debate Service : ");
        } catch (\Exception $e) {
            Log::debug($e);
            throw new Exception($e);
        }
    }

    public function updateDebateParticipation($inputJson)
    {
        Log::debug("Started ... updateDebateParticipation Service : " . json_encode($inputJson));
        try {
            $codes = new Codes;
            $dataValidation = new DataValidation;
            $dateUtils = new DateUtils;
            $debatesDao = new DebatesDao;

            //Validations to be done
            //Id validation
            if (!isset($inputJson['debate_id']) || $dataValidation->isEmpty($inputJson['debate_id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your debate ID",
                    'data' => null
                ];
                return $response;
            }
            //User validation
            if (!isset($inputJson['user_id']) || $dataValidation->isEmpty($inputJson['user_id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your user ID",
                    'data' => null
                ];
                return $response;
            }
            //status validation
            if (!isset($inputJson['status']) || $dataValidation->isEmpty($inputJson['status'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your status",
                    'data' => null
                ];
                return $response;
            }
           
            Log::debug("Saving the Participation : ");
            $qryResponse = $debatesDao->addRemoveDebateParticipation($inputJson, $dateUtils->getCurrentDate());
            Log::debug("Saved the Participation ");

            Log::debug('debate : ' . json_encode($inputJson['debate_id']));

            if($qryResponse["valid"]){
                //Send the success response now
                $response = [
                    'status' => $codes->RC_SUCCESS,
                    'message' => $codes->RM_SUCCESS,
                    'data' => [(($inputJson['status'] == 1)?'participated':'deleted')=>true]
                ];
            }else{                
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => $qryResponse["msg"],
                    'data' => null
                ];
            }
            return $response;

            Log::debug("Ended ... debate Service : ");
        } catch (\Exception $e) {
            Log::debug($e);
            throw new Exception($e);
        }
    }


    public function addDebateParticipantsProsAndCons($inputJson)
    {
        Log::debug("Started ... addDebateParticipantsProsAndCons Service : " . json_encode($inputJson));
        try {
            $codes = new Codes;
            $dataValidation = new DataValidation;
            $dateUtils = new DateUtils;
            $debatesDao = new DebatesDao;

            //Validations to be done
            //Id validation
            if (!isset($inputJson['debate_id']) || $dataValidation->isEmpty($inputJson['debate_id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your debate ID",
                    'data' => null
                ];
                return $response;
            }
            //User validation
            if (!isset($inputJson['user_id']) || $dataValidation->isEmpty($inputJson['user_id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your user ID",
                    'data' => null
                ];
                return $response;
            }
            //text validation
            if (!isset($inputJson['debate_text']) || $dataValidation->isEmpty($inputJson['debate_text'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your pros/cons",
                    'data' => null
                ];
                return $response;
            }
            //is_pro validation
            if (!isset($inputJson['is_pro'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your pros/cons type",
                    'data' => null
                ];
                return $response;
            }
            
            Log::debug("Saving the Participation ProsAndCons : ");
            $qryResponse = $debatesDao->sendDebateParticipationProsAndCons($inputJson, $dateUtils->getCurrentDate());
            Log::debug("Saved the Participation ProsAndCons ");
            //return $qryResponse;
            Log::debug('debate : ' . json_encode($inputJson['debate_id']));

            if($qryResponse["valid"]){
                //Send the success response now
                $response = [
                    'status' => $codes->RC_SUCCESS,
                    'message' => $codes->RM_SUCCESS,
                    'data' => $qryResponse["data"]
                ];
            }else{                
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => $qryResponse["msg"],
                    'data' => null
                ];
            }
            return $response;

            Log::debug("Ended ... debate Service : ");
        } catch (\Exception $e) {
            Log::debug($e);
            throw new Exception($e);
        }
    }

    public function updateDebateParticipantsProsAndCons($inputJson)
    {
        Log::debug("Started ... updateDebateParticipantsProsAndCons Service : " . json_encode($inputJson));
        try {
            $codes = new Codes;
            $dataValidation = new DataValidation;
            $dateUtils = new DateUtils;
            $debatesDao = new DebatesDao;

            //Validations to be done
            //Id validation
            if (!isset($inputJson['discussion_id']) || $dataValidation->isEmpty($inputJson['discussion_id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your discussion ID",
                    'data' => null
                ];
                return $response;
            }
            //User validation
            if (!isset($inputJson['user_id']) || $dataValidation->isEmpty($inputJson['user_id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your user ID",
                    'data' => null
                ];
                return $response;
            }
            //text validation
            if (!isset($inputJson['debate_text']) || $dataValidation->isEmpty($inputJson['debate_text'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your pros/cons",
                    'data' => null
                ];
                return $response;
            }
            //is_pro validation
            if (!isset($inputJson['is_pro'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your pros/cons type",
                    'data' => null
                ];
                return $response;
            }
            
            Log::debug("Saving the Participation ProsAndCons : ");
            $qryResponse = $debatesDao->updateDebateParticipantsProsAndCons($inputJson, $dateUtils->getCurrentDate());
            Log::debug("Saved the Participation ProsAndCons ");

            // Log::debug('debate : ' . json_encode($inputJson['debate_id']));

            if($qryResponse["valid"]){
                //Send the success response now
                $response = [
                    'status' => $codes->RC_SUCCESS,
                    'message' => $codes->RM_SUCCESS,
                    'data' => $qryResponse["data"]
                ];
            }else{                
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => $qryResponse["msg"],
                    'data' => null
                ];
            }
            return $response;

            Log::debug("Ended ... debate Service : ");
        } catch (\Exception $e) {
            Log::debug($e);
            throw new Exception($e);
        }
    }

    public function deleteDebateParticipantsProsAndCons($inputJson)
    {
        Log::debug("Started ... deleteDebateParticipantsProsAndCons Service : " . json_encode($inputJson));
        try {
            $codes = new Codes;
            $dataValidation = new DataValidation;
            $dateUtils = new DateUtils;
            $debatesDao = new DebatesDao;

            //Validations to be done
            //Id validation
            if (!isset($inputJson['discussion_id']) || $dataValidation->isEmpty($inputJson['discussion_id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your discussion ID",
                    'data' => null
                ];
                return $response;
            }
            //User validation
            if (!isset($inputJson['user_id']) || $dataValidation->isEmpty($inputJson['user_id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your user ID",
                    'data' => null
                ];
                return $response;
            }
            
            Log::debug("Saving the Participation ProsAndCons : ");
            $qryResponse = $debatesDao->deleteDebateParticipantsProsAndCons($inputJson, $dateUtils->getCurrentDate());
            Log::debug("Saved the Participation ProsAndCons ");

            // Log::debug('debate : ' . json_encode($inputJson['debate_id']));

            if($qryResponse["valid"]){
                //Send the success response now
                $response = [
                    'status' => $codes->RC_SUCCESS,
                    'message' => $codes->RM_SUCCESS,
                    'data' => $qryResponse["data"]
                ];
            }else{                
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => $qryResponse["msg"],
                    'data' => null
                ];
            }
            return $response;

            Log::debug("Ended ... debate Service : ");
        } catch (\Exception $e) {
            Log::debug($e);
            throw new Exception($e);
        }
    }

    public function calculateDebateScore($inputJson)
    {
        Log::debug("Started ... calculateDebateScore Service : " . json_encode($inputJson));
        try {
            $codes = new Codes;
            $dataValidation = new DataValidation;
            $dateUtils = new DateUtils;
            $debatesDao = new DebatesDao;
            $dateUtils = new DateUtils;

            //Validations to be done
            //Id validation
            if (!isset($inputJson['debate_id']) || $dataValidation->isEmpty($inputJson['debate_id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your debate ID",
                    'data' => null
                ];
                return $response;
            }
            //User validation
            if (!isset($inputJson['user_id']) || $dataValidation->isEmpty($inputJson['user_id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your user ID",
                    'data' => null
                ];
                return $response;
            }
            
            Log::debug("Saving the Participation ProsAndCons : ");
            $score = $debatesDao->getDebateScoreCalculation($inputJson, $dateUtils->getCurrentDate());
            Log::debug("Saved the Participation ProsAndCons ");

            Log::debug('debate : ' . json_encode($inputJson['debate_id']));

            //Send the success response now
            $response = [
                'status' => $codes->RC_SUCCESS,
                'message' => $codes->RM_SUCCESS,
                'data' => ['score'=>$score, 'prosAndCons'=>$score]
            ];
            return $response;

            Log::debug("Ended ... calculateDebateScore Service: ");
        } catch (\Exception $e) {
            Log::debug($e);
            throw new Exception($e);
        }
    }
    public function inviteUserForDebate($inputJson)
    {
        Log::debug("Started ... inviteUserForDebate Service : " . json_encode($inputJson));
        try {
            $codes = new Codes;
            $dataValidation = new DataValidation;
            $dateUtils = new DateUtils;
            $debatesDao = new DebatesDao;
            $dateUtils = new DateUtils;

            //Validations to be done
            //Id validation
            if (!isset($inputJson['debate_id']) || $dataValidation->isEmpty($inputJson['debate_id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your debate ID",
                    'data' => null
                ];
                return $response;
            }
            //User validation
            if (!isset($inputJson['user_id']) || $dataValidation->isEmpty($inputJson['user_id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your user ID",
                    'data' => null
                ];
                return $response;
            }
            //Sender User validation
            if (!isset($inputJson['sender_user_id']) || $dataValidation->isEmpty($inputJson['sender_user_id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your sender user ID",
                    'data' => null
                ];
                return $response;
            }
            
            Log::debug("Saving the invitation : ");
            $qryResponse = $debatesDao->sendDebateInvitaion($inputJson, $dateUtils->getCurrentDate());
            Log::debug("Saved the invitation ");

            Log::debug('debate : ' . json_encode($inputJson['debate_id']));

            if($qryResponse["valid"]){
                //Send the success response now
                $response = [
                    'status' => $codes->RC_SUCCESS,
                    'message' => $codes->RM_SUCCESS,
                    'data' => null
                ];
            }else{                
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => $qryResponse["msg"],
                    'data' => null
                ];
            }
            return $response;

            Log::debug("Ended ... inviteUserForDebate Service: ");
        } catch (\Exception $e) {
            Log::debug($e);
            throw new Exception($e);
        }
    }
    public function getInvitedUserListForDebate($inputJson)
    {
        Log::debug("Started ... getInvitedUserListForDebate Service : " . json_encode($inputJson));
        try {
            $codes = new Codes;
            $dataValidation = new DataValidation;
            $debatesDao = new DebatesDao;

            //Validations to be done
            //Id validation
            if (!isset($inputJson['debate_id']) || $dataValidation->isEmpty($inputJson['debate_id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your debate ID",
                    'data' => null
                ];
                return $response;
            }
            //User validation
            if (!isset($inputJson['user_id']) || $dataValidation->isEmpty($inputJson['user_id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your user ID",
                    'data' => null
                ];
                return $response;
            }
            
            Log::debug("Searching the invitations : ");
            $list = $debatesDao->getDebateInvitedUserList($inputJson);
            Log::debug("Sending the invitations ");

            Log::debug('debate : ' . json_encode($inputJson['debate_id']));

            //Send the success response now
            $response = [
                'status' => $codes->RC_SUCCESS,
                'message' => $codes->RM_SUCCESS,
                'data' => ($list)?$list:[]
            ];
            return $response;

            Log::debug("Ended ... getInvitedUserListForDebate Service: ");
        } catch (\Exception $e) {
            Log::debug($e);
            throw new Exception($e);
        }
    }


    public function updateDebateProsAndConsFollowers($inputJson)
    {
        Log::debug("Started ... updateDebateProsAndConsFollowers Service : " . json_encode($inputJson));
        try {
            $codes = new Codes;
            $dataValidation = new DataValidation;
            $dateUtils = new DateUtils;
            $debatesDao = new DebatesDao;

            //Validations to be done
            //Id validation
            // if (!isset($inputJson['debate_id']) || $dataValidation->isEmpty($inputJson['debate_id'])) {
            //     $response = [
            //         'status' => $codes->RC_ERROR,
            //         'message' => "Please enter your debate ID",
            //         'data' => null
            //     ];
            //     return $response;
            // }
            //User validation
            if (!isset($inputJson['user_id']) || $dataValidation->isEmpty($inputJson['user_id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your user ID",
                    'data' => null
                ];
                return $response;
            }
            //discussion validation
            if (!isset($inputJson['discussion_id']) || $dataValidation->isEmpty($inputJson['discussion_id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your discussion ID",
                    'data' => null
                ];
                return $response;
            }
            //status validation
            if (!isset($inputJson['status']) || $dataValidation->isEmpty($inputJson['status'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your status",
                    'data' => null
                ];
                return $response;
            }
           
            Log::debug("Saving the Participation Followers: ");
            $qryResponse = $debatesDao->addRemoveDebateProsAndConsFollowers($inputJson, $dateUtils->getCurrentDate());
            Log::debug("Saved the Participation Followers");

            // Log::debug('debate : ' . json_encode($inputJson['debate_id']));

            if($qryResponse["valid"]){
                //Send the success response now
                $response = [
                    'status' => $codes->RC_SUCCESS,
                    'message' => $codes->RM_SUCCESS,
                    'data' => [(($inputJson['status'] == 1)?'followed':'deleted')=>true]
                ];
            }else{                
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => $qryResponse["msg"],
                    'data' => null
                ];
            }
            return $response;

            Log::debug("Ended ... debate Service : ");
        } catch (\Exception $e) {
            Log::debug($e);
            throw new Exception($e);
        }
    }

    public function addDebateProsAndConsRating($inputJson)
    {
        Log::debug("Started ... addDebateProsAndConsRating Service : " . json_encode($inputJson));
        try {
            $codes = new Codes;
            $dataValidation = new DataValidation;
            $dateUtils = new DateUtils;
            $debatesDao = new DebatesDao;

            //User validation
            if (!isset($inputJson['user_id']) || $dataValidation->isEmpty($inputJson['user_id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your user ID",
                    'data' => null
                ];
                return $response;
            }
            //discussion validation
            if (!isset($inputJson['discussion_id']) || $dataValidation->isEmpty($inputJson['discussion_id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your discussion ID",
                    'data' => null
                ];
                return $response;
            }
            //rating validation
            if (!isset($inputJson['rating']) || $dataValidation->isEmpty($inputJson['rating'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your rating",
                    'data' => null
                ];
                return $response;
            }
           
            Log::debug("Saving the Participation Rating: ");
            $qryResponse = $debatesDao->addDebateProsAndConsRating($inputJson, $dateUtils->getCurrentDate());
            Log::debug("Saved the Participation Rating");

            if($qryResponse["valid"]){
                //Send the success response now
                $response = [
                    'status' => $codes->RC_SUCCESS,
                    'message' => $codes->RM_SUCCESS,
                    'data' => null
                ];
            }else{                
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => $qryResponse["msg"],
                    'data' => null
                ];
            }
            return $response;

            Log::debug("Ended ... debate Service : ");
        } catch (\Exception $e) {
            Log::debug($e);
            throw new Exception($e);
        }
    }


    public function getDebateProsAndConsList($inputJson)
    {
        Log::debug("Started ... getDebateProsAndConsList Service : " . json_encode($inputJson));
        try {
            $codes = new Codes;
            $debatesDao = new DebatesDao;
            $dateUtils = new DateUtils;
            $dataValidation = new DataValidation;

            //User validation
            if (!isset($inputJson['debate_id']) || $dataValidation->isEmpty($inputJson['debate_id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your debate ID",
                    'data' => null
                ];
                return $response;
            }
            //User validation
            if (!isset($inputJson['user_id']) || $dataValidation->isEmpty($inputJson['user_id'])) {
                $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "Please enter your user ID",
                    'data' => null
                ];
                return $response;
            }
            Log::debug("getting the debates ProsAndCons : ");
            $debatesList = $debatesDao->getDebateProsAndCons($inputJson, $dateUtils->getCurrentDate());
            Log::debug("Got the debate ProsAndCons");

            //Send the success response now
            $response = [
                'status' => $codes->RC_SUCCESS,
                'message' => $codes->RM_SUCCESS,
                'data' => $debatesList?$debatesList:[]
            ];
            return $response;

            Log::debug("Ended ... debate Service : ");
        } catch (\Exception $e) {
            Log::debug($e);
            throw new Exception($e);
        }
    }

}
