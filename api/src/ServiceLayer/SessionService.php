<?php

namespace App\ServiceLayer;

use App\DaoLayer\CampusDetailDao;
use App\Utils\Codes;
use App\Utils\DataValidation;
use Cake\Core\Exception\Exception;
use App\Model\Image;
use App\Model\UserModel;
use App\Model\ConversationGroup;
use App\DaoLayer\InterestMstDao;
use App\DaoLayer\OrganizationDao;
use App\DaoLayer\OrganizationTypeDao;
use App\DaoLayer\PostsDao;
use App\DaoLayer\PostsLikesDao;
use App\DaoLayer\PostsSavedDao;
use App\DaoLayer\ScheduleCampusDao;
use App\DaoLayer\UserInterestsDao;
use App\DaoLayer\UserOrganizerDao;
use App\DaoLayer\UsersDao;
use App\DaoLayer\VenueDao;
use App\Utils\DateUtils;
use App\Utils\ImageUtils;
use Cake\Log\Log;
use Cake\Datasource\ConnectionManager;

class SessionService
{
    /**
     * 
     */
    public function getVenueList($input)
    {
        Log::debug("Started ... getVenueList : ".json_encode($input));
        try{
            $codes = new Codes;
            if(
                !isset($input['latitude']) || empty($input['latitude'])
                || !isset($input['longitude']) || empty($input['longitude'])
                || !isset($input['offset'])
            ) {
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'Please provide latitude , longitude and offset',
                    'data'=>null
                ];

                return $response;
            }

            if(!isset($input['text'])) {
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'Please provide text parameter',
                    'data'=>null
                ];

                return $response;
            }

            $text = trim($input['text']);
            $user = new UserModel();
          
            $res = $user->get_venue($input['latitude'], $input['longitude'], $input['offset'], $text);
            $response = array();
            if($res){
                $response['status'] = 1;
                $response['message'] = 'Venue List.';
                $response['data'] = $res;
            }else{
                $response['status'] = 0;
                $response['message'] = 'No Venue List found';
                $response['data'] = $res;
            }

            return $response; 

        }catch(\Exception $e){
            print_r($e);
            Log::debug($e);
            throw new Exception($e);
        }
    }

    public function setSessionStatus($input)
    {
        Log::debug("Started ... getVenueList : ".json_encode($input));

        try{
            $conn = ConnectionManager::get('default');
            $codes = new Codes;
            extract($input);
            if(!isset($user_id) || empty($user_id)){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'You must provide user id',
                    'data'=>null
                ];

                return $response;
            }
            
            if(!isset($session_id) || empty($session_id)){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'You must provide session id',
                    'data'=>null
                ];

                return $response;
            }
            
            if(!isset($is_live) || $is_live==''){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'You must provide live status.',
                    'data'=>null
                ];

                return $response;
            }
            
            $user = new UserModel($user_id);
            $get_session =  $conn->execute(sprintf("SELECT * FROM `sessions` WHERE `sessions_id` = %s AND `status`='3'", $input['session_id']));
                    if ($get_session->count() == 0) {
                        $response = [
                            'status'=>$codes->RC_ERROR,
                            'message'=>'Your session is not approved yet. Please contact to administrator.',
                            'data'=>null
                        ];
        
                        return $response;
                    }
            $session_data = $get_session->fetch("assoc");
            if($is_live==1 && $session_data['finalized_data']==0)
            {
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'Your session is not finalized yet. Please contact to administrator.',
                    'data'=>null
                ];

                return $response;
            }
            $msg='';
            if($is_live==1)
            {
                 $conn->execute(sprintf("UPDATE sessions SET is_session_live =$is_live WHERE sessions_id = %s", $session_data['sessions_id']));
                $msg='Session started successfully';
            }
            else if($is_live==0)
            {
                 $conn->execute(sprintf("UPDATE sessions SET is_session_live =$is_live,status=4 WHERE sessions_id = %s", $session_data['sessions_id']));
                
                $attendees =  $conn->execute(sprintf("SELECT * FROM sessions_attends WHERE sessions_id = %s", $session_data['sessions_id']));
                while($result=$attendees->fetch("assoc"))
                {
                    $user->post_notification($result['user_id'], 'session_ended', 'session',$session_data['sessions_id']);
                }
                $msg='Session organized successfuly';
            }		
            
            $response['status'] = 1;
            $response['message'] = $msg;
           

            return $response; 

        }catch(\Exception $e){
           // print_r($e);
            Log::debug($e);
            throw new Exception($e);
        }
    }

    public function suggestPollVenue($input)
    {
        Log::debug("Started ... suggestPollVenue : ".json_encode($input));
        try{
            $conn = ConnectionManager::get('default');
            $codes = new Codes;
            extract($input);
            
    		if(!isset($user_id) || empty($user_id)){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'You must provide user id',
                    'data'=>null
                ];

                return $response;
            }
            $user = new UserModel($user_id);
            
            if(!isset($session_id) || empty($session_id)){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'You must provide session id',
                    'data'=>null
                ];

                return $response;
            }
            
            $get_session = $conn->execute(sprintf("SELECT * FROM `sessions` WHERE `sessions_id` = %s", $session_id));
            if ($get_session->count() == 0) {
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'Please send a valid session id.',
                    'data'=>null
                ];

                return $response;
            }
            
            $q = '';
            if(isset($search)){
                $q  = trim($search);
            }
            $session_polls = $conn->execute(sprintf("SELECT * from sessions_polls where session_id = %s", $session_id));
            $selectedVenues ='';
            if ($session_polls->count() == 0) {
                $selectedVenues ='';
            } else {
                $session_polls = $session_polls->fetch("assoc");
                $sessions_poll_id = $session_polls['sessions_poll_id'];
                $sessions_polls_options = $conn->execute(sprintf("SELECT option_id, venue, event_date FROM sessions_polls_options WHERE session_poll_id = %s GROUP BY venue", $sessions_poll_id));
                
                if($sessions_polls_options->count() > 0){
                    while($row =$sessions_polls_options->fetch("assoc") ){
                        $selectedVenues .= "'".trim($row['venue'])."',";
                    }
                    $selectedVenues = rtrim($selectedVenues,',');
                }
                
            }
            if(!empty($q)){
    
                $get_file = $conn->execute(sprintf("SELECT venue_id,venue_name,venue_type,latitude,longitude,cover_photo FROM venue WHERE status = '1' AND venue_name NOT IN (%s) AND LOWER(venue_name) LIKE %s ORDER BY venue_name ASC",$selectedVenues, "%".$q.'%'));
            } else {
                $get_file = $conn->execute( sprintf("SELECT venue_id,venue_name,venue_type,latitude,longitude,cover_photo FROM venue WHERE status = '1' AND venue_name NOT IN (%s) ORDER BY venue_name ASC",$selectedVenues));
            }
            
            if ($get_file->count() > 0) {
                if ($get_file->count() > 0) {
                    while ($v = $get_file->fetch("assoc")) {
                        $v['cover_photo']=$codes->UPLOADS_DIRECTORY . '/' .$v['cover_photo'];
                        
                        /*all photo*/
                        $allimage = $conn->execute(sprintf("SELECT * FROM `venue_image` WHERE venue_id = %s", $v['venue_id']));
                        $allphoto=[];
                            if(!empty($allimage )){		
                                foreach($allimage as $sk=>$slot){
                                    $imgar=$codes->UPLOADS_DIRECTORY . '/' .$slot['photo'];
                                    $allphoto[$sk]['photo']=$imgar;
                                    $allphoto[$sk]['photoid']=$slot['id'];
                                }
                            }
                            $v['allphoto']=$allphoto;
                        /*all photo*/
                        
                        /* Time slot*/
                        $days = [
                            'Sunday',
                            'Monday',
                            'Tuesday',
                            'Wednesday',
                            'Thursday',
                            'Friday',
                            'Saturday'
                        ];
                        $dayar=[];
                        foreach($days as $kd=>$dval)
                        {
                            $slotar['slots']=[];
                            $slot_all = $conn->execute(sprintf("SELECT * FROM `venue_slots` WHERE venue_id = %s and day='%s'  ", $v['venue_id'], $dval)); 
                            if(!empty($slot_all )){		
                                foreach($slot_all as $sk=>$slot){
                                    $slotar['slots'][$sk]['start']=$slot['starttime'];
                                    $slotar['slots'][$sk]['end']=$slot['endtime'];
                                    $slotar['slots'][$sk]['slotid']=$slot['id'];
                                    //$slotar['slots'][$sk]['isbook']=($qvchk->count()==0)?0:1;
                                }
                            }
                            $dayar[]=$slotar;
                        }
                        $v['days']=$dayar;
                        $data[] = $v;
                    }
                }
            }
            
            $response['status'] = 1;
            $response['message'] = 'Venue List';
            $response['data'] = $data;

            return $response;

        }catch(\Exception $e){
           // print_r($e);
            Log::debug($e);
            throw new Exception($e);
        }
    }

    function distance($lat1, $lon1, $lat2, $lon2, $unit='K') {
        $codes = new Codes;
		$theta = $lon1 - $lon2;
		$dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
		$dist = acos($dist);
		$dist = rad2deg($dist);
		$miles = $dist * 60 * 1.1515;
		$unit = strtoupper($unit);
	
		if ($unit == "K") {
			$dist = round($miles * 1.609344,2);
			$distance_in_meter =  round($dist * 1000);
			return array('distance' => $dist.' Km','distance_in_meter'=>$distance_in_meter);
		} else if ($unit == "N") {
			return ($miles * 0.8684);
		} else {
			return $miles;
		}
	}

    public function allSessionsV1($input)
    {
        Log::debug("Started ... allSessionsV1 : ".json_encode($input));
        try{
            $conn = ConnectionManager::get('default');
            $codes = new Codes;
            extract($input);
            
            if(!isset($user_id) || empty($user_id)){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'You must provide user id',
                    'data'=>null
                ];

                return $response;
            }
            
            if(!isset($session_type) || empty($session_type) || !in_array($session_type,['upcoming','assigned','organized'])){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'You must provide a valid session_type (upcoming,assigned,organized).',
                    'data'=>null
                ];

                return $response;
            }

            if(!isset($sort_by)){
                // if(!in_array($sort_by,array('distance'))){
                //     $response = [
                //         'status'=>$codes->RC_ERROR,
                //         'message'=>'Please enter a valid sort by.',
                //         'data'=>null
                //     ];
    
                //     return $response;
                // }
            } else {
                //$sort_by = 'distance';
                //$order_by = 'ASC';
            }
            
            if(isset($sort_by) && $sort_by == 'distance'){
                if(!isset($order_by) || !in_array($order_by,array('ASC','DESC'))){
                    $response = [
                        'status'=>$codes->RC_ERROR,
                        'message'=>'Please enter a valid order by',
                        'data'=>null
                    ];
    
                    return $response;
                }
                if($order_by == 'ASC'){
                    $sort = SORT_ASC;
                }
                if($order_by == 'DESC'){
                    $sort = SORT_DESC;
                }
            }
            $user = new UserModel($user_id);
            $conn->execute(sprintf("update users set `user_latitude`='%s',`user_longitude`='%s' where user_id=%s", $lat, $long, $user_id));
            // check user activated
            if ($codes->ACTIVATION_ENABLED && !$user->_data['user_activated']) {
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'Before you can accept any session, you need to confirm your email address',
                    'data'=>null
                ];

                return $response;
            }
            
            // initialize the return array
            $return = array();
    
            
            /* Get User Venue */ 
            $userVenueArray = [] ; 
            $get_user_venue = $conn->execute(sprintf("SELECT * FROM `users_venue` WHERE user_id = %s", $user->_data['user_id']));
            if ($get_user_venue->count() > 0) {
                while($venue_data = $get_user_venue->fetch("assoc")){
                    $userVenueArray[] = $venue_data['venue_id'];
                }
            }
    
            $sessions = [];
            if(isset($session_roles)){
                if($session_roles == 'Initiated'){
                    $query_venue = "SELECT sessions.* FROM sessions WHERE sessions.created_by = '%s'";
                
                } elseif($session_roles == 'Conducted' || $session_roles == 'Conducting'){
                    //$query_venue=sprintf('SELECT sessions.* FROM sessions 
                    //INNER JOIN sessions_attends ON (sessions_attends.sessions_id = sessions.sessions_id)
                    //WHERE sessions_attends.user_id = %s AND sessions_attends.status =1
                    //AND sessions.sessions_id not in (SELECT sessions_id FROM sessions_attends WHERE sessions_attends.user_id = %s AND sessions_attends.status =1)',$user->_data['user_id'],$user->_data['user_id']);
                    
                    $query_venue = 'SELECT sessions.* FROM sessions  WHERE (`presentors`=%s)';
                    
                } elseif($session_roles == 'Attending' || $session_roles == 'Attended'){

                    $query_venue = 'SELECT sessions.* FROM sessions 
                    INNER JOIN sessions_attends ON (sessions_attends.sessions_id = sessions.sessions_id)
                    WHERE sessions_attends.user_id = %s AND sessions_attends.status =1';
                
                } elseif($session_roles == 'Joined'){

                    $query_venue = 'SELECT sessions.* FROM sessions 
                    INNER JOIN sessions_joins ON (sessions_joins.sessions_id = sessions.sessions_id)
                    WHERE sessions_joins.user_id = %s
                    AND sessions.`status` = 4';
                    
                } else {
                    $query_venue = "SELECT sessions.* FROM sessions WHERE 1=1";
                    $noUser = 0;
                }
                
            } else {
                $query_venue = "SELECT sessions.* FROM sessions WHERE 1=1";
                $noUser = 0;
            }
            
            if(isset($interest_search) && !empty($interest_search) && is_array($interest_search)){
                $interest_search_a = implode(',',$interest_search);
                $query_venue .= " AND (SELECT count(*) FROM sessions_interest WHERE sessions_interest.sessions_id= sessions.sessions_id AND sessions_interest.interest IN(%s)) > 0";
            }
            
            if(isset($my_interest) && $my_interest == '1'){
                $interestArr = array();
                $get_my_interest = $conn->execute(sprintf("SELECT * FROM `user_interest` WHERE `user_id` = %s", $user->_data['user_id']));
                while ($interest = $get_my_interest->fetch("assoc")) {
                    $interestArr[] = $interest['interest'];
                }
               // print_r($interestArr);
                if(!empty($interestArr)){
                    $interestArr_search = implode(',',$interestArr);
                    $query_venue .= " AND (SELECT count(*) FROM sessions_interest WHERE sessions_interest.sessions_id= sessions.sessions_id AND sessions_interest.interest IN(%s)) > 0";
                }
            }

            if($session_type=='upcoming'){
                $query_venue .= ' AND sessions.status=2';
                $orderby='sessions.verification_time DESC';
                $condtn="sessions.verification_time < '%s'";
            } elseif ($session_type=='assigned'){
                $query_venue .= ' AND sessions.status=3';
                $orderby='sessions.assigning_time DESC';
                $condtn="sessions.assigning_time < '%s'";
            } else {
                $query_venue .= ' AND sessions.status=4';
                $orderby='sessions.conclusion_time DESC';
                $condtn="sessions.conclusion_time < '%s'";
            }
            
            
            $offset = 0;
            $limit = 10;
            if(!empty($input['limit'])){
                $limit = $input['limit'];
            }
            if(!empty($input['offset'])){
                $offset = $input['offset'] * $limit;
            }
            //$query_venue .= " ORDER BY $orderby";
            if($last_session_time==''){
                $query_venue .= " ORDER BY %s";
            
                if(isset($interest_search_a)){
                    if(isset($session_roles)){
                        $get_sessions =  $conn->execute(sprintf($query_venue, $user->_data['user_id'], $interest_search_a,   $orderby));
                    }else{
                        $get_sessions =  $conn->execute(sprintf($query_venue, $interest_search_a,   $orderby));
                    }
                }else if(isset($interestArr_search)){
                //    echo $orderby;
                //    echo $interestArr_search;
                //    echo $query_venue;exit;
                    if(isset($session_roles)){
                        $get_sessions =  $conn->execute(sprintf($query_venue, $user->_data['user_id'], $interestArr_search,   $orderby));
                    }else{
                        $get_sessions =  $conn->execute(sprintf($query_venue, $interestArr_search,   $orderby));
                    }
                }else{
                   // $query_venue .= " ORDER BY %s";
                    if(isset($session_roles)){
                        $get_sessions =  $conn->execute(sprintf($query_venue, $user->_data['user_id'],   $orderby));
                    }else{
                        
                        //echo $orderby;
                        $get_sessions =  $conn->execute(sprintf($query_venue, $orderby));
                    }
                }
            }
            else
            {
                $query_venue .= " AND '%s' ORDER BY %s";
                if(isset($interest_search_a)){
                    if(isset($session_roles)){
                        $get_sessions =  $conn->execute(sprintf($query_venue, $user->_data['user_id'], $interest_search_a, $last_session_time,  $condtn, $orderby));
                    }else{
                        $get_sessions =  $conn->execute(sprintf($query_venue, $interest_search_a, $last_session_time,  $condtn, $orderby));
                    }
                }else if(isset($interestArr_search)){
                    if(isset($session_roles)){
                        $get_sessions =  $conn->execute(sprintf($query_venue, $user->_data['user_id'], $interestArr_search, $last_session_time,  $condtn, $orderby));
                    }else{
                        $get_sessions =  $conn->execute(sprintf($query_venue, $interestArr_search, $last_session_time,  $condtn, $orderby));
                    }
                }else{
                    if(isset($session_roles)){
                        $get_sessions =  $conn->execute(sprintf($query_venue, $user->_data['user_id'],  $last_session_time,  $condtn, $orderby));
                    }else{
                        $get_sessions =  $conn->execute(sprintf($query_venue,  $last_session_time,  $condtn, $orderby));
                    }
                }
            }

         
           
            
            if($get_sessions->count() > 0 ){
                $sessions['assigned'] = [];
                $sessions['organized'] = [];
                $sessions['verified'] = [];
                //$sessions['initiation'] = [];
                //$sessions['deleted'] = [];
                //$sessions['attending'] = [];
                while ($row = $get_sessions->fetch("assoc")) {
                    if(!empty($row['presentors'])){
                        $sharerr = explode(',', $row['presentors']);
                        $sharerr = array_filter($sharerr);
                        $sharer_blocked = false;
                        if(!empty($sharerr)){
                            foreach($sharerr as $sharerr_id){
                                if($user->blocked($sharerr_id)){
                                    $sharer_blocked= true;
                                }
                            }
                        }
                        if($sharer_blocked){
                            continue;
                        }
                    }
                    
                    $row['venue'] = rtrim($row['venue'],',');
                    $row['venues']=[];
                    $distanceArray=[];
                    if(!empty($row['venue'])){
                        $get_venues = $conn->execute(sprintf("SELECT venue_id,venue_name,venue_type,description,location,total_rating,latitude,longitude, website_link, teams FROM venue WHERE venue_id IN ('%s') AND status='1'",$row['venue']));
                        
                        
                        if ($get_venues->count() > 0) {
                            while ($venuerow = $get_venues->fetch("assoc")) {
                                $teams = json_decode(html_entity_decode($venuerow['teams']), TRUE);
                                $team_data = [] ;
                                if(!empty($teams )){
                                    foreach($teams as $key=>$value){
                                        $temp_data['name'] = $key;
                                        $temp_data['role'] = $value;
                                        $team_data[] = $temp_data;
                                    }
                                }
                                $venuerow['teams'] = $team_data;
                                $distance = $this->distance($lat, $long, (float) $venuerow['latitude'], (float) $venuerow['longitude']);
                                $venuerow['distance_km'] = $distance['distance'];
                                $venuerow['distance_m'] = $distance['distance_in_meter'];
                                $distanceArray[] = $venuerow['distance_m'];
                                
                                $row['venues'][] = $venuerow;
                            }
                        }
                    }
                    if(!empty($distanceArray)){
                        $row['min_distance_m'] = min($distanceArray);
                        $row['min_distance_km'] = round($row['min_distance_m']/1000,2)." km";
                    } else {
                        $row['min_distance_m'] = 0;
                        $row['min_distance_km'] = "0 km";
                    }
                    
                    //unset($row['venue']);
                    
                    if(isset($location_based)){
                        if($location_based == 'Within_10KM'){
                            if($row['min_distance_m'] > 10000){
                                continue;
                            }
                            
                        } elseif($location_based == '10KM_From_lat_long_provided'){
                            if($row['min_distance_m'] > 10000){
                                continue;
                            }
                        }
                    }
                    
                    if(!empty($row['event_date'])){
                        $event_dates = explode(',', $row['event_date']);
                        $event_dates = array_filter($event_dates);
                        $row['event_date'] = array();
                        if(!empty($event_dates)){
                            foreach($event_dates as $event_date){
                                $row['event_date'][]['value'] = $event_date;
                            }
                        }
                    }else{
                        $row['event_date'] = [];
                    }
                    
                    
                    $sessions_poll_id = 0; 
                    $session_polls = $conn->execute(sprintf("SELECT * FROM sessions_polls WHERE session_id = %s LIMIT 1", $row['sessions_id']));
                    
                    if($session_polls->count()){
                        $session_polls = $session_polls->fetch("assoc");
                        $sessions_poll_id = $session_polls['sessions_poll_id'];
                        
                    }
                    
                    
                    $poll_options = [];
                    if($sessions_poll_id){
                        $sessions_polls_venues = $conn->execute(sprintf("SELECT option_id, venue, event_date FROM sessions_polls_options WHERE session_poll_id = %s", $sessions_poll_id));
                        if($sessions_polls_venues->count() > 0){
                            while($poll_venues = $sessions_polls_venues->fetch("assoc")){
                                $poll_venues['venue']= trim($poll_venues['venue']);
                                $get_venues = $conn->execute(sprintf("SELECT venue_id FROM venue WHERE venue_name ='%s'", $poll_venues['venue']));
                                $ven = $get_venues->fetch("assoc");
                                $poll_venues['venue_id']= @$ven['venue_id'];
                                $poll_options[] = $poll_venues;
                            }
                        }
                        
                    }
                    
                    if(isset($time_based)){
                        if($time_based == 'Within_10_days'){
                            $date_10_days_after = date('Y-m-d',strtotime("+10 days"));
                            if($row['status'] == 4 || $row['finalized_data'] == 1){
                                if(!empty($row['event_date'])){
                                    $date_found_time = false;
                                    foreach($row['event_date'] as $eDate){
                                        if($eDate['value'] >= $start_date && $eDate['value'] <= $end_date){
                                            $date_found = true;
                                        }
                                    }
                                    if($date_found_time === false){
                                        continue;
                                    }
                                }
                            } else {
                                if(!empty($poll_options)){
                                    $date_found_time = false;
                                    foreach($poll_options as $eDate){
                                        if($eDate['event_date'] <= $date_10_days_after && $eDate['event_date']>= date('Y-m-d')){
                                            $date_found_time = true;
                                        }
                                    }
                                    if($date_found_time === false){
                                        continue;
                                    }
                                }
    
                            }
    
                        }
                    }
                    
                    if(isset($start_date) && !empty($start_date) && isset($end_date) && !empty($end_date)){
                        if($row['status'] == 4 || $row['finalized_data'] == 1){
                            if(!empty($row['event_date'])){
                                $date_found = false;
                                foreach($row['event_date'] as $eDate){
                                    if($eDate['value'] >= $start_date && $eDate['value'] <= $end_date){
                                        $date_found = true;
                                    }
                                }
                                if($date_found === false){
                                    continue;
                                }
                            }
                        } else {
                            if(!empty($poll_options)){
                                $date_found = false;
                                foreach($poll_options as $eDate){
                                    if($eDate['event_date'] >= $start_date && $eDate['event_date'] <= $end_date){
                                        $date_found = true;
                                    }
                                }
                                if($date_found === false){
                                    continue;
                                }
                            }
                        }
                    }
                    
                    if(isset($my_venue) && $my_venue == 1 && !empty($userVenueArray)){
                        if($row['status'] == 4 || $row['finalized_data'] == 1){
                            $venue_found = false;
                            foreach($userVenueArray as $venue_id_sear){
                                $key = array_search($venue_id_sear, array_column($row['venues'], 'venue_id'));
                                if($key === false){
                                    //$venue_found = true;
                                } else {
                                    $venue_found = true;
                                }
                            }
                            if($venue_found === false){
                                continue;
                            }
                        } else {
                            $venue_found = false;
                            foreach($userVenueArray as $venue_id_sear){
                                $key = array_search($venue_id_sear, array_column($poll_options, 'venue_id'));
                                if($key === false){
                                    //$venue_found = true;
                                } else {
                                    $venue_found = true;
                                }
                            }
                            if($venue_found === false){
                                continue;
                            }
                        }
                    }
                    
                    if(isset($venue_id_search) && !empty($venue_id_search) && is_array($venue_id_search)){
                        if($row['status'] == 4 || $row['finalized_data'] == 1){
                            $venue_found = false;
                            foreach($venue_id_search as $venue_id_sear){
                                $key = array_search($venue_id_sear, array_column($row['venues'], 'venue_id'));
                                if($key === false){
                                    //$venue_found = true;
                                } else {
                                    $venue_found = true;
                                }
                            }
                            if($venue_found === false){
                                continue;
                            }
                        } else {
                            $venue_found = false;
                            foreach($venue_id_search as $venue_id_sear){
                                $key = array_search($venue_id_sear, array_column($poll_options, 'venue_id'));
                                if($key === false){
                                    //$venue_found = true;
                                } else {
                                    $venue_found = true;
                                }
                            }
                            if($venue_found === false){
                                continue;
                            }
                        }
                        
                    }
    
                    
                    $topics = explode(',', $row['topics']);
                    $topics = array_filter($topics);
                    $row['topics'] = array();
                    if(!empty($topics)){
                        foreach($topics as $topic){
                            $row['topics'][]['value'] = $topic;
                        }
                    }
                    
                    $prerequisite = explode(',', $row['prerequisite']);
                    $prerequisite = array_filter($prerequisite);
                    $row['prerequisite'] = array();
                    if(!empty($prerequisite)){
                        foreach($prerequisite as $prerequis){
                            $row['prerequisite'][]['value'] = $prerequis;
                        }
                    }
                    
                    $event_times = explode(',', $row['event_time']);
                    $event_times = array_filter($event_times);
                    $row['event_time'] = array();
                    if(!empty($event_times)){
                        foreach($event_times as $event_time){
                            $row['event_time'][]['value'] = $event_time;
                        }
                    }
                    
                    $row['cover_photo'] = $codes->ROOT_URL.'/content/uploads/'.$row['cover_photo'];
                    $event_photos = explode(',', $row['event_photos']);
                    $event_photos = array_filter($event_photos);
                    $row['event_photos'] = array();
                    if(!empty($event_photos)){
                        foreach($event_photos as $event_photo){
                            $row['event_photos'][]['photo'] = $codes->ROOT_URL.'/content/uploads/'.$event_photo;
                        }
                    }
                    
                    $event_expired = FALSE;
                    $events_time = @explode(',',$row['event_time']);
                    $events_dates = @explode(',',$row['event_date']);			
                    if(!empty($event_dates)){
                        foreach ($event_dates as $ed) {
                            if(!empty($event_times)){
                                foreach ($event_times as $et) {
                                    $event_date = $ed. " " . $et;
                                    $endtime = strtotime("+" . $row['event_duration'] . " minutes", strtotime($event_date));		
                                    if (intval($endtime) > 0 && time() >= $endtime) {
                                        $event_expired = TRUE;
                                    }
                                }
                            }
                        }
                    }				
                    
                    $row['event_expired'] = $event_expired;
                    
                    
                    $get_blocked = $conn->execute(sprintf("SELECT * FROM `sessions_blocked` WHERE `user_id` = %s ", $row['created_by']));
                    $blockedArr['blocked_ids'] = array();
                    while ($blocked_data = $get_blocked->fetch("assoc")) {
                        $row['blocked_ids'][] = $blocked_data['blocked_id'];
                    }
                    $row['sharer'] = $row['presentors'];
    
                    $get_session_venue = $conn->execute(sprintf("SELECT * FROM `sessions_presentor` WHERE sessions_id = %s AND `status` = '2'", $row['sessions_id']));
                    if ($get_session_venue->count() > 0) {
                        $venue_data = $get_session_venue->fetch("assoc");
                        $row['session_venue'] = $venue_data['venue'];
                        $row['total_allowed_members'] = $venue_data['total_allowed_members'];
                    }
                    else {
                        $row['session_venue'] = '';
                        $row['total_allowed_members'] = $row['total_allowed_members'];
                        
                    }

                    $row['users_venue'] = 0;
                        
                    if(isset($venue_data['venue'])){
                        $users_venue_data = $conn->execute(sprintf("SELECT * FROM `users_venue` WHERE user_id = %s and venue_id = '%s'", $user->_data['user_id'], $venue_data['venue']));
                       if ($users_venue_data->count() > 0) { 
                            $row['users_venue'] = 1;
                        }
                    }
                    
                    $sharer_following = $conn->execute(sprintf("SELECT * from followings WHERE user_id = %s AND following_id IN('%s')", $user->_data['user_id'], $row['sharer']));
                    $row['sharer_following'] = 0;
                    if ($sharer_following->count() > 0) { 
                        $row['sharer_following'] = 1;
                    }
                    
                    $session_reported = $conn->execute(sprintf("SELECT * FROM reports WHERE user_id = %s AND node_id = %s AND node_type = 'session'", $user->_data['user_id'], $row['sessions_id']));
                    $row['session_reported'] = 0;
                    if ($session_reported->count() > 0) {
                        $row['session_reported'] = 1;
                    }
                    
                    $user_interest = TRUE;
                    $check_user_interest = $conn->execute(sprintf("SELECT * FROM sessions_interest WHERE sessions_id = %s AND status = 1", $row['sessions_id']));
                    if ($check_user_interest->count() > 0) {
                        while ($interest = $check_user_interest->fetch("assoc")) {
                            $check_int = $conn->execute(sprintf("SELECT * FROM user_interest WHERE user_id = %s AND interest = %s", $user->_data['user_id'], $interest['interest']));
                            if ($check_int->count() > 0) {
                                continue;
                            }
                            $user_interest = FALSE;
                        }
                    }
                    $row['user_interest']= $user_interest;
                    
                    $get_24hr_attend = $conn->execute(sprintf("SELECT * FROM `sessions_attends` WHERE added_on >= now() - INTERVAL 1 DAY AND `user_id` = %s", $user->_data['user_id']));
                    if ($get_24hr_attend->count() >= 2) {
                       $allow_attend = FALSE;			    
                    }
                    else{
                        $allow_attend = TRUE;
                    }
                    $row['allow_attend']= $allow_attend;
    
                    
                    $get_24hr_accept = $conn->execute(sprintf("SELECT * FROM `sessions_presentor` WHERE added_on >= now() - INTERVAL 1 DAY AND `user_id` = %s", $user->_data['user_id']));
                    if ($get_24hr_accept->count() >= 2) {
                       $allow_accept = FALSE;			    
                    }
                    else{
                        $allow_accept = TRUE;
                    }
                    
                    $row['allow_accept']= $allow_accept;
                    
                    /* get the connection */
                    $row['i_attend'] = false;
                    if (isset($user->_logged_in)) {
                        if($row['status']==4)
                        {
                        $get_likes = $conn->execute(sprintf("SELECT * FROM `sessions_attends` WHERE sessions_id = %s AND user_id = %s and status=1",$row['sessions_id'], $user->_data['user_id']));	
                        }
                        else
                        {
                        $get_likes = $conn->execute(sprintf("SELECT * FROM `sessions_attends` WHERE sessions_id = %s AND user_id = %s",$row['sessions_id'], $user->_data['user_id']));
                        }
                        if ($get_likes->count() > 0) {
                            $row['i_attend'] = true;
                        }
                    }
                    $row['i_join'] = false;
                    if (isset($user->_logged_in)) {
                        $get_likes = $conn->execute(sprintf("SELECT * FROM `sessions_joins` WHERE sessions_id = %s AND user_id = %s",$row['sessions_id'], $user->_data['user_id']));
                        if ($get_likes->count() > 0) {
                            $row['i_join'] = true;
                        }
                    }
                    /* get the connection */
                    $row['i_accept'] = false;
                    if (isset($user->_logged_in)) {
                        $get_likes = $conn->execute(sprintf("SELECT * FROM `sessions_presentor` WHERE sessions_id = %s AND user_id = %s",$row['sessions_id'], $user->_data['user_id']));
                        if ($get_likes->count() > 0) {
                            $row['i_accept'] = true;
                        }
                    }
    
                    /* get the thank you button */
                    $row['i_thanks'] = 0;
                    $row['noofthanks']=0;
                    if (isset($user->_logged_in)) {
                        $get_likes = $conn->execute(sprintf("SELECT * FROM `sessions_thanks` WHERE session_id = %s AND user_id = %s",$row['sessions_id'], $user->_data['user_id']));
                            if ($get_likes->count() > 0) {
                                $row['i_thanks'] = 1;
                            }
                            
                            $get_likes = $conn->execute(sprintf("SELECT * FROM `sessions_thanks` WHERE session_id = %s",$row['sessions_id']));
                            $row['noofthanks']=$get_likes->count();
                    }
    
                    /* get total attendes */
                    $row['total_attend'] = 0;
                    if (isset($user->_logged_in)) {
                        if($row['status']==4)
                        {
                        $get_likes = $conn->execute(sprintf("SELECT count(*) AS cnt FROM `sessions_attends` WHERE sessions_id = %s and status=1",$row['sessions_id']));
                        }
                        else
                        {
                        $get_likes = $conn->execute(sprintf("SELECT count(*) AS cnt FROM `sessions_attends` WHERE sessions_id = %s",$row['sessions_id']));
                        }
                        if ($get_likes->count() > 0) {
                            $cnt = $get_likes->fetch("assoc");
                            $row['total_attend'] = $cnt['cnt'];
                        }
                    }
                    
                    /* get total question */
                    $row['total_question_asked'] = 0;
                    if (isset($user->_logged_in)) {
                        $get_likes = $conn->execute(sprintf("SELECT count(*) AS cnt FROM `sessions_qa` WHERE sessions_id = %s and qa_type=1 and status=1 ",$row['sessions_id']));
                        if ($get_likes->count() > 0) {
                            $cnt = $get_likes->fetch("assoc");
                            $row['total_question_asked'] = $cnt['cnt'];
                        }
                    }
                            
                    /* Presentors*/
                    
                    $presentors = explode(',', $row['presentors']);
                    $presentors = array_filter($presentors);
                    $row['presentors'] = array();
                    //pr($presentors);die;
                    if(!empty($presentors)){
                        foreach($presentors as $presentor){
                            $tmpPresentor = array();
                            $userDetail = $user->get_user_by_id($presentor);
                            if(isset($userDetail[0])){
                                $tmpPresentor['user_id'] = $userDetail[0]['user_id'];
                                $tmpPresentor['user_name'] = $userDetail[0]['user_name'];
                                $tmpPresentor['user_fullname'] = $userDetail[0]['user_fullname'];
                            }
                            $tmpPresentor['add_edit_option'] = 0;
                            if (isset($userDetail[0]) && $userDetail[0]['user_id'] == $user->_data['user_id']) {
                                $tmpPresentor['add_edit_option'] = 1;
                            }
                            $row['presentors'][] = $tmpPresentor;
                        }
                    }
                    if ($row['sharer'] != '') {
                        $row['sharer'] = $user->get_user_by_ids($row['sharer']);
                    } else {
                        $row['sharer'] = [];
                    }
    
                    /* get total friends attendes */
                    $row['session_attend_friends'] = [];
                    //pr($row);die;
                    $my_friends = $conn->execute(sprintf("SELECT GROUP_CONCAT(user_two_id) AS my_friends FROM `friends` WHERE status = %s AND user_one_id = %s ", 1, $user->_data['user_id']));
                    $my_friends_ids = $my_friends->fetch("assoc");
    
                    if ($row['sessions_id'] != "" && $my_friends_ids['my_friends'] != "") {
                        $sessions_attends = $conn->execute(sprintf("SELECT * FROM `sessions_attends` WHERE sessions_id = %s AND user_id IN (%s) ",$row['sessions_id'], $my_friends_ids['my_friends']));
                        if ($sessions_attends->count() > 0) {
                            while ($session_attend_users = $sessions_attends->fetch("assoc")) {
    
                                $friends = $conn->execute(sprintf("SELECT * FROM `users` WHERE user_id = %s ", $session_attend_users['user_id'], $user->_data['user_id']));
                                $friends_data = $friends->fetch("assoc");
                                
                                if(isset($friends_data) && is_array($friends_data)){
                                    $session_attend_data['user_id'] = $friends_data['user_id'];
                                    $session_attend_data['user_picture'] =$user->get_picture($friends_data['user_picture'], $friends_data['user_gender']);
                                    $session_attend_data['user_fullname'] = $friends_data['user_fullname'];
                                    $row['session_attend_friends'][] = $session_attend_data;
                                }
                            }
                        }
                    }
    
                    /* get total friends attendes */
                    //$row['session_engaged_peoples'] = [];
                    $my_friends = $conn->execute(sprintf("SELECT GROUP_CONCAT(user_id) AS users FROM `sessions_comment` WHERE status = %s AND sessions_id = %s ", 1,$row['sessions_id']));
                    $user_ids = $my_friends->fetch("assoc");
    
                    $final_user_ids = "";
                    if ($user_ids['users'] != "") {
                        $final_user_ids = $user_ids['users'] . ",";
                    }
    
                    $sessions_attends = $conn->execute(sprintf("SELECT GROUP_CONCAT(user_id) AS users FROM `sessions_attends` WHERE sessions_id = %s ",$row['sessions_id']));
                    $sessions_user_ids = $sessions_attends->fetch("assoc");
    
                    if ($sessions_user_ids['users'] != "") {
                        $final_user_ids .= $sessions_user_ids['users'];
                    }
    
                    if ($final_user_ids != "") {
    
                        $final_user_ids = rtrim($final_user_ids, ",");
    
                        $friends = $conn->execute(sprintf("SELECT * FROM `users` WHERE user_id IN (%s) ", $final_user_ids));
    
                        while ($friends_data = $friends->fetch("assoc")) {
                            $session_engaged_data['user_id'] = $friends_data['user_id'];
                            $session_engaged_data['user_picture'] =$user->get_picture($friends_data['user_picture'], $friends_data['user_gender']);
                            $session_engaged_data['user_fullname'] = $friends_data['user_fullname'];
                            $session_engaged_data['user_work'] = $friends_data['user_work'];
                            $session_engaged_data['user_work_title'] = $friends_data['user_work_title'];
                            $session_engaged_data['user_work_place'] = $friends_data['user_work_place'];
                            //$row['session_engaged_peoples'][] = $session_engaged_data;
                        }
                    }
    
                    /* get People attendes */
                   // $row['session_attend_peoples'] = [];
                    $sessions_attends = $conn->execute(sprintf("SELECT * FROM `sessions_attends` WHERE sessions_id = %s ",$row['sessions_id']));
                    if ($sessions_attends->count() > 0) {
                        while ($session_attend_users = $sessions_attends->fetch("assoc")) {
    
                            $friends = $conn->execute(sprintf("SELECT * FROM `users` WHERE user_id = '%s'", $session_attend_users['user_id']));
                            $friends_data = $friends->fetch("assoc");
                            
                            if(isset($friends_data) && is_array($friends_data) > 0){
                                $session_attend_data['user_id'] = $friends_data['user_id'];
                                $session_attend_data['user_picture'] =$user->get_picture($friends_data['user_picture'], $friends_data['user_gender']);
                                $session_attend_data['user_fullname'] = $friends_data['user_fullname'];
                                $session_attend_data['user_work'] = $friends_data['user_work'];
                                $session_attend_data['user_work_title'] = $friends_data['user_work_title'];
                                $session_attend_data['user_work_place'] = $friends_data['user_work_place'];
                            }
                            //$row['session_attend_peoples'][] = $session_attend_data;
                        }
                    }
                    //pr($row);die;
                    $row['created_by'] = $user->get_user_by_id($row['created_by']);
                    if(isset($row['created_by'][0])){
                        $row['created_by'] = $row['created_by'][0];
                    }
                    
                    //no of verified discussion
                    //$row['verifieddiscussion']=[];
                        $disscussion=$conn->execute(sprintf("SELECT * FROM `sessions_discussion` WHERE sessions_id=%s and status=1 and parent_discussion_id=0",$row['sessions_id']));
                        //while ($rest = $disscussion->fetch("assoc")) {
                        //	$row['verifieddiscussion'][] =$rest;
                        //}
                    $row['noofverifieddiscussion']=$disscussion->count();
                    
                    if($row['status']==3){
                        if(count($sessions['assigned'])<10)
                        {
                        $sessions['assigned'][] = $row;
                        }
                        
                    } elseif($row['status']==4){
                        if(count($sessions['organized'])<10)
                        {
                        $qwer=$conn->execute(sprintf("select ROUND(total_rating/cnt,2) as total_rating from (select ROUND(sum(rating1+rating2+rating3+rating4+rating5)/5,2) as total_rating,count(*) as cnt from `venue_rating` WHERE session_id = %s) a",$row['sessions_id']));
                        $resrt = $qwer->fetch("assoc");
                    
                        $row['total_rating']=$resrt['total_rating'];
                        $sessions['organized'][] = $row;
                        }
                    } elseif($row['status']==1){
                        //$sessions['initiation'][] = $row;
                    } elseif($row['status']==2){
                        if(count($sessions['verified'])<10)
                        {
                        $sessions['verified'][] = $row;
                        }
                    } else {
                        //$sessions['deleted'][] = $row;
                    }
                    $users_attended_sessions = $conn->execute(sprintf("SELECT * FROM `sessions_attends` WHERE sessions_id = %s AND user_id = %s",$row['sessions_id'], $user->_data['user_id']));
    
                    if ($users_attended_sessions->count() > 0) {
                        //$sessions['attending'][] = $row;
                    }
                    
                }
                if(isset($sort_by) && $sort_by == 'distance'){
                    //$sessions['initiation'] = array_orderby($sessions['initiation'], 'min_distance_m', $sort);
                    $sessions['verified']   = $this->array_orderby($sessions['verified'], 'min_distance_m', $sort);
                    $sessions['assigned']   = $this->array_orderby($sessions['assigned'], 'min_distance_m', $sort);
                    $sessions['organized']  = $this->array_orderby($sessions['organized'], 'min_distance_m', $sort);
                }
                else
                {
                    //$sort='SORT_DESC';
                    //$sessions['verified']   = array_sort($sessions['verified'], 'verification_time', $sort);
                    //$sessions['assigned']   = array_sort($sessions['assigned'], 'assigning_time', $sort);
                    //$sessions['organized']  = array_sort($sessions['organized'], 'conclusion_time', $sort);
                }
                //$sessions['initiation'] = unique_multidim_array($sessions['initiation'],'sessions_id');
                //$sessions['verified'] = unique_multidim_array($sessions['verified'],'sessions_id');
                //$sessions['assigned'] = unique_multidim_array($sessions['assigned'],'sessions_id');
                //$sessions['organized'] = unique_multidim_array($sessions['organized'],'sessions_id');
                /* assign variables */
                $data['sessions'] = $sessions;
                if(empty($data['sessions'])){
                    $data['sessions'] = (object)[];
                }
    
                $response['status'] = 1;
                $response['message'] = 'All sessions.';
                $response['data'] = $data;
            } else {
                $sessions['assigned'] = [];
                $sessions['organized'] = [];
                $sessions['verified'] = [];
                $data['sessions'] = $sessions;
                $response['status'] = 2;
                $response['message'] = 'No more data.';
                $response['data'] = $data;
            }
            
            return $response;

        }catch(\Exception $e){
           //print_r($e);
            Log::debug($e);
            throw new Exception($e);
        }
    }

    function array_orderby()
    {
        $args = func_get_args();
        $data = array_shift($args);
        foreach ($args as $n => $field) {
            if (is_string($field)) {
                $tmp = array();
                foreach ($data as $key => $row)
                    $tmp[$key] = $row[$field];
                $args[$n] = $tmp;
                }
        }
        $args[] = &$data;
        call_user_func_array('array_multisort', $args);
        return array_pop($args);
    }


    public function markUserAttendStatus($input)
    {
        Log::debug("Started ... markUserAttendStatus : ".json_encode($input));

        extract($input);
        global $db,$system;
        $data = array();
        try {

             $conn = ConnectionManager::get('default');
            $codes = new Codes;
            extract($input);
            if(!isset($user_id) || empty($user_id)){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'You must provide user id',
                    'data'=>null
                ];

                return $response;
            }
            
            if(!isset($session_id) || empty($session_id)){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'You must provide session id',
                    'data'=>null
                ];

                return $response;
            }

            if(!isset($i_attend)){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'You must provide attend status',
                    'data'=>null
                ];

                return $response;
            }
            
            $response = [];
            
            $user = new UserModel($user_id);
            $get_session = $conn->execute(sprintf("SELECT * FROM `sessions` WHERE `sessions_id` = %s AND `status` IN ('2', '3', '4')", $input['session_id']));
                    if ($get_session->count() == 0) {
                        $response = [
                            'status'=>$codes->RC_ERROR,
                            'message'=>'Your session is not approved yet. Please contact to administrator.',
                            'data'=>null
                        ];
        
                        return $response;
                    }
            $session_data = $get_session->fetch("assoc");
            $userid=$user->_data['user_id'];
            $qwe=$conn->execute(sprintf("select * from `sessions_attends` where sessions_id ='%s'  and user_id = '%s'", $session_data['sessions_id'], $userid));
            if($qwe->count() == 0)
            {
                if($session_data['status']==4)
                {
                    $get_likes = $conn->execute(sprintf("insert into `sessions_attends` set sessions_id = '%s', user_id = '%s', status='%s'", $session_data['sessions_id'], $user->_data['user_id'], $i_attend));
                }
                else
                {
                    $get_likes = $conn->execute(sprintf("insert into `sessions_attends` set sessions_id = %s,user_id = '%s'", $session_data['sessions_id'], $user->_data['user_id']));	
                }
               
            }
            else
            {
                
                $get_likes = $conn->execute(sprintf("update `sessions_attends` set status='%s' where sessions_id = '%s' AND user_id = '%s'", $i_attend, $session_data['sessions_id'], $user->_data['user_id'])) ;
                $sessions_poll = $conn->execute(sprintf("SELECT sessions_poll_id FROM `sessions_polls` WHERE session_id = '%s'", $session_data['sessions_id']))  ;
                while($polloption=$sessions_poll->fetch("assoc"))
                {
                    $node_url=$session_data['sessions_id'].'/'.$polloption['sessions_poll_id'];
                    $user->delete_notification($session_data['created_by'], 'vote', 'session', $node_url);
                }
               
    
                $conn->execute(sprintf("DELETE FROM `users_sessions_polls_options` WHERE `session_poll_id` IN (SELECT `sessions_poll_id` from `sessions_polls` where `session_id`='%s') and `user_id`='%s'", $session_data['sessions_id'], $user->_data['user_id'])) ;
            
                $action='session_attend';$node_type='session';
                $node_url=$session_data['sessions_id'];
                $user->delete_notification($session_data['created_by'], $action, $node_type, $node_url);
               
            //$this->delete_notification($session_data['created_by'], 'vote', $node_type, $node_url);
                    
                    /* check if the viewer already liked this page */
                    $check = $conn->execute(sprintf("SELECT * FROM sessions_attends WHERE user_id = %s AND sessions_id = %s", $user->_data['user_id'], $session_data['sessions_id'])) ;
                    /* if no -> return */
                    if ($check->count() == 0){
                        $response['status'] = 1;
                        $response['message'] = 'Marked Successfully';
                       
                        return $response;
                    }
                        //return;
                    /* like this page */
                    $conn->execute(sprintf("DELETE FROM sessions_attends WHERE user_id = %s AND sessions_id = %s", $user->_data['user_id'], $session_data['sessions_id'])) ;
                    /* update likes counter -1 */
                    $conn->execute(sprintf("UPDATE sessions SET total_attends = IF(total_attends=0,0,total_attends-1) WHERE sessions_id = %s", $session_data['sessions_id'])) ;
    
                    /*AUTO UNJOIN*/
                    /* check if the viewer already liked this page */
                    $check = $conn->execute(sprintf("SELECT * FROM sessions_joins WHERE user_id = %s AND sessions_id = %s", $user->_data['user_id'], $session_data['sessions_id'])) ;
                    /* if no -> return */
                    if ($check->count() == 0){
                        $response['status'] = 1;
                        $response['message'] = 'Marked Successfully';
                       
                        return $response;
                    }
                    /* like this page */
                    $conn->execute(sprintf("DELETE FROM sessions_joins WHERE user_id = %s AND sessions_id = %s", $user->_data['user_id'], $session_data['sessions_id'])) ;
                    
                    /*  session_attend_notify*/
                    $checkn = $conn->execute(sprintf("SELECT * FROM sessions_attend_notify WHERE user_id = %s AND sessions_id = %s", $user->_data['user_id'], $session_data['sessions_id'])) ;
                    if ($check->count() == 0){
                        $response['status'] = 1;
                        $response['message'] = 'Marked Successfully';
                       
                        return $response;
                    }
                    $conn->execute(sprintf("DELETE FROM sessions_attend_notify WHERE user_id = %s AND sessions_id = %s", $user->_data['user_id'], $session_data['sessions_id'])) ;
                    /*  session_attend_notify*/
                    
                    /* update likes counter -1 */
                    $conn->execute(sprintf("UPDATE sessions SET total_joins = IF(total_joins=0,0,total_joins-1) WHERE sessions_id = %s", $session_data['sessions_id'])) ;
                    
            }
            $response['status'] = 1;
            $response['message'] = 'Marked Successfully';
           
            return $response;
            
        }catch(\Exception $e){
            print_r($e);
            Log::debug($e);
            throw new Exception($e);
        }
    }

    public function deleteSessionComment($input)
    {
        Log::debug("Started ... deleteSessionComment : ".json_encode($input));

        extract($input);
        global $db,$system;
        $data = array();
        try {

            $conn = ConnectionManager::get('default');
            $codes = new Codes;
            extract($input);
            if(!isset($user_id) || empty($user_id)){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'You must provide user id',
                    'data'=>null
                ];

                return $response;
            }
            
            if(!isset($sessions_comment_id) || empty($sessions_comment_id)){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'sessions_comment_id is required',
                    'data'=>null
                ];

                return $response;
            }

            $user = new UserModel($input['user_id']);
            $check = $conn->execute(sprintf("SELECT sessions_comment.*,sessions.created_by,sessions.presentors FROM `sessions_comment` INNER JOIN sessions ON (sessions.sessions_id = sessions_comment.sessions_id) WHERE sessions_comment.sessions_comment_id = %s",$input['sessions_comment_id']));
            if($check->count() == 0){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'Invalid Comment or session',
                    'data'=>null
                ];

                return $response;
            }
            $comment = $check->fetch("assoc");
            
            // parent_comment_id = 0 than its comment 
            if($comment['parent_comment_id'] == 0){
                if(!in_array($user->_data['user_id'],[$comment['user_id'],$comment['created_by'],$comment['presentors']])){
                    $response = [
                        'status'=>$codes->RC_ERROR,
                        'message'=>'You are not authorized to do this',
                        'data'=>null
                    ];
    
                    return $response;
                }
                $conn->execute(sprintf("DELETE FROM `sessions_comment` WHERE sessions_comment_id=%s",$input['sessions_comment_id']));
                $conn->execute(sprintf("DELETE FROM `sessions_comment_likes` WHERE sessions_comment_id=%s",$input['sessions_comment_id']));
                
                $conn->execute(sprintf("DELETE FROM `sessions_comment` WHERE parent_comment_id=%s",$input['sessions_comment_id']));
                
            } else {
                $parent = $conn->execute(sprintf("SELECT sessions_comment.* FROM `sessions_comment` WHERE sessions_comment.sessions_comment_id = %s", $comment['parent_comment_id']));
                if($parent->count() > 0){
                    $parentComment = $parent->fetch("assoc");
                    if(!in_array($user->_data['user_id'],[$parentComment['user_id'],$comment['user_id'],$comment['created_by'],$comment['presentors']])){
                        $response = [
                            'status'=>$codes->RC_ERROR,
                            'message'=>'You are not authorized to do this',
                            'data'=>null
                        ];
        
                        return $response;
                    }
                    $conn->execute(sprintf("DELETE FROM `sessions_comment` WHERE sessions_comment_id=%s",$input['sessions_comment_id']));
                    $conn->execute(sprintf("DELETE FROM `sessions_comment_likes` WHERE sessions_comment_id=%s",$input['sessions_comment_id']));
                }
            }
            
            $response['status'] = 1;
            $response['message'] = '';
            $response['data'] = true;

            return $response;
        }catch(\Exception $e){
            print_r($e);
            Log::debug($e);
            throw new Exception($e);
        }
    }

    public function editSessionComment($input)
    {
        Log::debug("Started ... editSessionComment : ".json_encode($input));

        extract($input);
        global $db,$system;
        $data = array();
        try {

            $conn = ConnectionManager::get('default');
            $codes = new Codes;
            extract($input);
            if(!isset($user_id) || empty($user_id)){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'You must provide user id',
                    'data'=>null
                ];

                return $response;
            }
            
            if(!isset($sessions_comment_id) || empty($sessions_comment_id)){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'sessions_comment_id is required',
                    'data'=>null
                ];

                return $response;
            }

            if(!isset($input['post']) || empty($input['post'])){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'post is required',
                    'data'=>null
                ];

                return $response;
            }
            
            $user = new UserModel($input['user_id']);
            $check = $conn->execute(sprintf("SELECT * FROM `sessions_comment` WHERE sessions_comment_id = %s",$input['sessions_comment_id']));
            if($check->count() == 0){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'Invalid sessions_comment_id',
                    'data'=>null
                ];

                return $response;
            }

            $comment = $check->fetch("assoc");
            if($comment['user_id'] != $user->_data['user_id']){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'You are not authorized to do this',
                    'data'=>null
                ];

                return $response;
            }
            
            $conn->execute(sprintf("UPDATE sessions_comment SET post = '%s' WHERE sessions_comment_id = %s", $input['post'],$input['sessions_comment_id']));
            
            $response['status'] = 1;
            $response['message'] = '';
            $response['data'] = true;

            return $response;
        }catch(\Exception $e){
            print_r($e);
            Log::debug($e);
            throw new Exception($e);
        }
    }

    public function uploadVideo($input)
    {
        Log::debug("Started ... uploadVideo : ".json_encode($input));

        extract($input);
        global $db,$system;
        $data = array();
        try {
            $conn = ConnectionManager::get('default');
            $codes = new Codes;
            extract($input);

            if(!isset($user_id)){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'You must provide user id',
                    'data'=>null
                ];

                return $response;
            }

            if(!isset($type)){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'You must provide type',
                    'data'=>null
                ];

                return $response;
            }

            if (!isset($input['session_id']) || !is_numeric($input['session_id'])) {
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'session id is required',
                    'data'=>null
                ];

                return $response;
            }
                    
            $get_session = $conn->execute(sprintf("SELECT * FROM `sessions` WHERE `sessions_id` = %s",$input['session_id']));
            
            if ($get_session->count() == 0) {
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'Wrong session id',
                    'data'=>null
                ];

                return $response;
            }
            $sess = $get_session->fetch("assoc");

            if($type=='video')
            {
                if(!isset($_FILES['video'])){
                    $response = [
                        'status'=>$codes->RC_ERROR,
                        'message'=>'You must upload a video for your session',
                        'data'=>null
                    ];
    
                    return $response;
                }
                
                $file=$_FILES;
                // check & create uploads dir
                $folder = 'videos';
                $depth = '../../';
                if(!file_exists($depth.$codes->UPLOADS_DIRECTORY.'/'.$folder)) {
                    @mkdir($depth.$codes->UPLOADS_DIRECTORY.'/'.$folder, 0777, true);
                }
                if(!file_exists($depth.$codes->UPLOADS_DIRECTORY.'/'.$folder.'/' . date('Y'))) {
                    @mkdir($depth.$codes->UPLOADS_DIRECTORY.'/'.$folder.'/' . date('Y'), 0777, true);
                }
                if(!file_exists($codes->UPLOADS_DIRECTORY.'/'.$folder.'/' . date('Y') . '/' . date('m'))) {
                    @mkdir($depth.$codes->UPLOADS_DIRECTORY.'/'.$folder.'/' . date('Y') . '/' . date('m'), 0777, true);
                }
                $max_allowed_size = $codes->MAX_VIDEO_SIZE * 1024*50;
                
                // valid inputs
                if(!isset($file["video"]) || $file["video"]["error"] != UPLOAD_ERR_OK) {
                    $response = [
                        'status'=>$codes->RC_ERROR,
                        'message'=> "Something wrong with upload! Is 'upload_max_filesize' set correctly?",
                        'data' => null
                    ];
    
                    return $response;
                }
                // check file size
                if($file["video"]["size"] > $max_allowed_size) {
                    $response = [
                        'status'=>$codes->RC_ERROR,
                        'message'=>'The file size is so big',
                        'data'=>null
                    ];
    
                    return $response;
                }
                
                
                
                //prepare new file name 
                $directory = $folder.'/'. date('Y') . '/' . date('m') . '/';
                $prefix = $codes->UPLOADES_PREFIX.'_'.md5(time()*rand(1, 9999));
                $image = explode(".",$file["video"]["name"]);
                $image_tmp_name = $directory.$prefix.'_tmp.'.end($image);
                $image_new_name = $directory.$prefix.'.'.end($image);
                $path_tmp = $depth.$codes->UPLOADS_DIRECTORY.'/'.$image_tmp_name;
                $path_new = $depth.$codes->UPLOADS_DIRECTORY.'/'.$image_new_name;
                //check if the file uploaded successfully
                if(!@move_uploaded_file($file['video']['tmp_name'], $path_new)) {
                    $response = [
                        'status'=>$codes->RC_ERROR,
                        'message'=>'Sorry, can not upload the file',
                        'data'=>null
                    ];
    
                    return $response;
                }
                $videofile=$image_new_name;
                $newvideofile=$sess['video'].','.$videofile;
                $conn->execute(sprintf("UPDATE sessions SET video = '%s'  WHERE sessions_id = %s", $newvideofile, $input['session_id']));
                $response['status'] = 1;
                $response['message'] = "Video upload successfully";
            }
            else if($type=='file')
            {
                if(isset($_FILES['file'])) {
                        if(!$codes->FILE_ENABLED) {
                            $response = [
                                'status'=>$codes->RC_ERROR,
                                'message'=>'This feature has been disabled',
                                'data'=>null
                            ];
            
                            return $response;
                        }
    
                        // valid inputs
                        if(!isset($_FILES["file"]) || $_FILES["file"]["error"] != UPLOAD_ERR_OK) {
                            $response = [
                                'status'=>$codes->RC_ERROR,
                                'message'=>"Something wrong with upload! Is 'upload_max_filesize' set correctly?",
                                'data'=>null
                            ];
            
                            return $response;
                        }
    
                        // check file size
                        $max_allowed_size = $codes->MAX_FILE_SIZE * 1024;
                        if($_FILES["file"]["size"] > $max_allowed_size) {
                            $response = [
                                'status'=>$codes->RC_ERROR,
                                'message'=>"The file size is so big",
                                'data'=>null
                            ];
            
                            return $response;
                        }
                        
                        
                        // check file extesnion
                        $extension = $this->get_extension($_FILES['file']['name']);
                        if(!$this->valid_extension($extension, $codes->FILE_EXTENSIONS)) {
                            $response = [
                                'status'=>$codes->RC_ERROR,
                                'message'=>"The file type is not valid or not supported",
                                'data'=>null
                            ];
            
                            return $response;
                        }
    
                        /* check & create uploads dir */
                        $folder = 'files';
                        $depth = '../../';
                        if(!file_exists($depth.$codes->UPLOADS_DIRECTORY.'/'.$folder)) {
                            @mkdir($depth.$codes->UPLOADS_DIRECTORY.'/'.$folder, 0777, true);
                        }
                        if(!file_exists($depth.$codes->UPLOADS_DIRECTORY.'/'.$folder.'/' . date('Y'))) {
                            @mkdir($depth.$codes->UPLOADS_DIRECTORY.'/'.$folder.'/' . date('Y'), 0777, true);
                        }
                        if(!file_exists($codes->UPLOADS_DIRECTORY.'/'.$folder.'/' . date('Y') . '/' . date('m'))) {
                            @mkdir($depth.$codes->UPLOADS_DIRECTORY.'/'.$folder.'/' . date('Y') . '/' . date('m'), 0777, true);
                        }
    
                        /* prepare new file name */
                        $directory = $folder.'/'. date('Y') . '/' . date('m') . '/';
                        $prefix =$codes->UPLOADES_PREFIX.'_'.md5(time()*rand(1, 9999));
                        $file_name = $directory.$prefix.'.'.$extension;
                        $path = $depth.$codes->UPLOADS_DIRECTORY.'/'.$file_name;
                                            
                        /* check if the file uploaded successfully */
                        if(!@move_uploaded_file($_FILES['file']['tmp_name'], $path)) {
                            $response = [
                                'status'=>$codes->RC_ERROR,
                                'message'=>"Sorry, can not upload the file",
                                'data'=>null
                            ];
            
                            return $response;
                        }
                        
                        /* upload to amazon s3 */
                        if($codes->S3_ENABLED) {
                            $this->aws_s3_upload($path, $file_name);
                        }
                        $newfile=$sess['file'].','.$file_name;
                        $conn->execute(sprintf("UPDATE sessions SET file = '%s'  WHERE sessions_id = %s", $newfile, $input['session_id']));
                        
                        $response['status'] = 1;
                        $response['message'] = "File upload successfully";
                    }
                    else
                    {
                        $response['status'] = 0;
                        $response['message'] = "Upload a file";
                    }

                return $response;
    
            }
            else
            {
                if(!isset($_FILES['photo']) || empty($_FILES['photo']) || !is_array($_FILES['photo'])){
                    $response = [
                        'status'=>$codes->RC_ERROR,
                        'message'=>"You must provide image.",
                        'data'=>null
                    ];
    
                    return $response;
                }
                
                
                $photo = '';
                $dimensions = array();
                $user_id=$input['user_id'];
                $max_allowed_size = $codes->MAX_PHOTO_SIZE * 1024;
                /* check & create uploads dir */
                $folder = 'photos';
                $depth = '../../';
                if(!file_exists($depth.$codes->UPLOADS_DIRECTORY.'/'.$folder)) {
                    @mkdir($depth.$codes->UPLOADS_DIRECTORY.'/'.$folder, 0777, true);
                }
                if(!file_exists($depth.$codes->UPLOADS_DIRECTORY.'/'.$folder.'/' . date('Y'))) {
                    @mkdir($depth.$codes->UPLOADS_DIRECTORY.'/'.$folder.'/' . date('Y'), 0777, true);
                }
                if(!file_exists($codes->UPLOADS_DIRECTORY.'/'.$folder.'/' . date('Y') . '/' . date('m'))) {
                    @mkdir($depth.$codes->UPLOADS_DIRECTORY.'/'.$folder.'/' . date('Y') . '/' . date('m'), 0777, true);
                }
                
                foreach($_FILES["photo"]['tmp_name'] as $key=>$image)
                {
                // valid inputs
                    if(!isset($_FILES["photo"]["error"][$key]) || $_FILES["photo"]["error"][$key] != UPLOAD_ERR_OK) {
                        $response = [
                            'status'=>$codes->RC_ERROR,
                            'message'=>"Something wrong with upload! Is 'upload_max_filesize' set correctly?",
                            'data'=>null
                        ];
        
                        return $response;
                    }
    
                    // check file size
                    if($_FILES["photo"]["size"][$key] > $max_allowed_size) {
                        $response = [
                            'status'=>$codes->RC_ERROR,
                            'message'=>"The file size is so big",
                            'data'=>null
                        ];
        
                        return $response;
                    }
                    
                    /* prepare new file name */
                    $directory = $folder.'/'. date('Y') . '/' . date('m') . '/';
                    $prefix =$codes->UPLOADES_PREFIX.'_'.md5(time()*rand(1, 9999));
                    $image = new Image($_FILES["photo"]["tmp_name"][$key]);
                    $get_dimensions = getimagesize($_FILES['photo']['tmp_name'][$key]);
                    $dimensions = array(
                        'width' => $get_dimensions[0],
                        'height' => $get_dimensions[1],
                    );
                    $image_tmp_name = $directory.$prefix.'_tmp'.$image->_img_ext;
                    $image_new_name = $directory.$prefix.$image->_img_ext;
                    $path_tmp = $depth.$codes->UPLOADS_DIRECTORY.'/'.$image_tmp_name;
                    $path_new = $depth.$codes->UPLOADS_DIRECTORY.'/'.$image_new_name;
    
                    /* check if the file uploaded successfully */
                    if(!@move_uploaded_file($_FILES['photo']['tmp_name'][$key], $path_tmp)) {
                        $response = [
                            'status'=>$codes->RC_ERROR,
                            'message'=>"Sorry, can not upload the photo",
                            'data'=>null
                        ];
        
                        return $response;
                    }
                    
                    /* save the new image */
                    $resize =  true;
                    $image->save($path_new, $path_tmp, $resize);
                   
                    /* delete the tmp image */
                    unlink($path_tmp);
    
                    /* upload to amazon s3 */
                    if($codes->S3_ENABLED) {
                        $this->aws_s3_upload($path_new, $image_new_name);
                    }
                    
                    /* return */
                    $photo.= $image_new_name.',';
                    $dimension=$dimensions['width'].'px * '.$dimensions['height'].'px';
                }
                if($photo!='')
                {
                $newphoto=$sess['event_photos'].','.$photo;
                $conn->execute(sprintf("UPDATE sessions SET event_photos = '%s'  WHERE sessions_id = %s", $newphoto, $input['session_id']));
                    $response['status'] = 1;
                    $response['message'] = "Photo upload successfully";
                }
                else
                {
                    $response['status'] = 0;
                        $response['message'] = "Photo can not be upload";	
                }
            }

            return $response;
        }catch(\Exception $e){
            print_r($e);
            Log::debug($e);
            throw new Exception($e);
        }
    }

    function valid_extension($extension, $allowed_extensions) {
        $extensions = explode(',', $allowed_extensions);
        foreach ($extensions as $key => $value) {
            $extensions[$key] = strtolower(trim($value));
        }
        if(is_array($extensions) && in_array($extension, $extensions)) {
            return true;
        }
        return false;
    }
    

    function get_extension($path) {
        return strtolower(pathinfo($path, PATHINFO_EXTENSION));
    }

    function aws_s3_upload($path, $file_name) {
        $codes = new Codes;
        require_once(ABSPATH.'includes/libs/AWS/aws-autoloader.php');
        $s3Client = Aws\S3\S3Client::factory(array(
            'version'    => 'latest',
            'region'      => $system['s3_region'],
            'credentials' => array(
                'key'    => $system['s3_key'],
                'secret' => $system['s3_secret'],
            )
        ));
        $Key = 'uploads/'.$file_name;
        $s3Client->putObject([
            'Bucket' => $codes->S3_BUCKET,
            'Key'    => $Key,
            'Body'   => fopen($path, 'r+'),
            'ACL'    => 'public-read',
        ]);
        /* remove local file */
        gc_collect_cycles();
        if($s3Client->doesObjectExist($codes->S3_BUCKET, $Key)) {
            unlink($path);
        }
    }


    public function sessionPhotodelete($input)
    {
        Log::debug("Started ... sessionPhotodelete : ".json_encode($input));

        extract($input);
        global $db,$system;
        $data = array();
        try {

            $conn = ConnectionManager::get('default');
            $codes = new Codes;
            extract($input);

            if(!isset($user_id)){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'You must provide user id',
                    'data'=>null
                ];

                return $response;
            }
            $user = new UserModel($user_id);
            
            if(!isset($session_id)){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'You must provide session id',
                    'data'=>null
                ];

                return $response;
            }

            if(!isset($photo_name) || empty($photo_name)){
                 $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'You must provide photo name',
                    'data'=>null
                ];

                return $response;
            }
            
            $get_session =  $conn->execute(sprintf("SELECT * FROM `sessions` WHERE `sessions_id` = %s", $session_id));
            if ($get_session->count() == 0) {
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'Your session is not approved yet. Please contact to administrator.',
                    'data'=>null
                ];

                return $response;
            }

            $session_data = $get_session->fetch("assoc");
            $eventphoto=$session_data['event_photos'];
            $neweventphoto=str_replace($photo_name.',','',$eventphoto);
            $neweventphoto=str_replace($photo_name,'',$neweventphoto);
                    
            $conn->execute(sprintf("update `sessions` set event_photos='%s' where sessions_id=%s", $neweventphoto, $session_id));
            $response['status'] = 1;
            $response['message']="Photo deleted successfully";


            return $response;
        }catch(\Exception $e){
            print_r($e);
            Log::debug($e);
            throw new Exception($e);
        }
    }

    public function suggestNewVenues($input)
    {
        Log::debug("Started ... suggestNewVenues : ".json_encode($input));

        extract($input);
        global $db,$system;
        $data = array();
        try {

            $conn = ConnectionManager::get('default');
            $codes = new Codes;
            extract($input);
            if(!isset($user_id) || empty($user_id)){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'You must provide user id',
                    'data'=>null
                ];

                return $response;
            }
            $user = new UserModel($user_id);
            
            if(!isset($session_id) || empty($session_id)){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'You must provide session id',
                    'data'=>null
                ];

                return $response;
            }
            
            if(!isset($venue_id) || empty($venue_id)){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'You must provide venue id',
                    'data'=>null
                ];

                return $response;
            }
            
            if(!isset($dates) || empty($dates) || !is_array($dates)){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'You must provide dates',
                    'data'=>null
                ];

                return $response;
            }
            
            $get_session = $conn->execute(sprintf("SELECT * FROM `sessions` WHERE `sessions_id` = %s", $session_id));
            if ($get_session->count() == 0) {
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'Please send a valid session id',
                    'data'=>null
                ];

                return $response;
            }
            
            $session_polls = $conn->execute(sprintf("SELECT * from sessions_polls where session_id = %s", $session_id));
    
            if ($session_polls->count() == 0) {
                $conn->execute(sprintf("INSERT INTO sessions_polls (session_id) VALUES (%s)", $session_id));
                $sql="SELECT LAST_INSERT_ID()";
                $stmt = $conn->execute($sql);
                $res = $stmt->fetch("assoc");
                
                $sessions_poll_id = $res[0];
            } else {
                $session_polls = $session_polls->fetch("assoc");
                $sessions_poll_id = $session_polls['sessions_poll_id'];
            }
            
            $user->add_session_poll_venues($session_id,$sessions_poll_id,$venue_id,$dates);
            
            $response['status'] = 1;
            $response['message'] = 'Venue suggested successfully';
            $response['data'] = [];


            return $response;
        }catch(\Exception $e){
            print_r($e);
            Log::debug($e);
            throw new Exception($e);
        }
    }

    public function suggestNewDates($input)
    {
        Log::debug("Started ... suggestNewDates : ".json_encode($input));

        extract($input);
        global $db,$system;
        $data = array();
        try {

            $conn = ConnectionManager::get('default');
            $codes = new Codes;
            extract($input);
            if(!isset($user_id) || empty($user_id)){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'You must provide user id',
                    'data'=>null
                ];

                return $response;
            }
            $user = new UserModel($user_id);
            
            if(!isset($session_id) || empty($session_id)){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'You must provide session id',
                    'data'=>null
                ];

                return $response;
            }
            
            if(!isset($venue) || empty($venue)){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'You must provide venue',
                    'data'=>null
                ];

                return $response;
            }
            
            if(!isset($dates) || empty($dates) || !is_array($dates)){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'You must provide dates',
                    'data'=>null
                ];

                return $response;
            }
            
            $get_session = $conn->execute(sprintf("SELECT * FROM `sessions` WHERE `sessions_id` = %s", $session_id));
            if ($get_session->count() == 0) {
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'Please send a valid session id',
                    'data'=>null
                ];

                return $response;
            }
            
            $session_polls = $conn->execute(sprintf("SELECT * from sessions_polls where session_id = %s", $session_id));
    
            if ($session_polls->count() == 0) {
                $conn->execute(sprintf("INSERT INTO sessions_polls (session_id) VALUES (%s)", $session_id));
                $sql="SELECT LAST_INSERT_ID()";
                $stmt = $conn->execute($sql);
                $res = $stmt->fetch("assoc");
                
                $sessions_poll_id = $res[0];
            } else {
                $session_polls = $session_polls->fetch("assoc");
                $sessions_poll_id = $session_polls['sessions_poll_id'];
            }

            $poll_options_event_dates = $conn->execute(sprintf("SELECT distinct event_date FROM sessions_polls_options WHERE session_poll_id = %s and venue='%s'", $sessions_poll_id, $venue));
            $date_inserted = $poll_options_event_dates->count();
            while ($date = $poll_options_event_dates->fetch("assoc")) {
                if(in_array($date['event_date'], $dates)){
                    $response = [
                        'status'=>$codes->RC_ERROR,
                        'message'=>'Date is already added',
                        'data'=>null
                    ];
    
                    return $response;
                }
            }
            
            $user->add_session_poll_dates($session_id, $sessions_poll_id, $dates, $venue);
            
            $response['status'] = 1;
            $response['message'] = 'Date added successfully';
            $response['data'] = [];


            return $response;
        }catch(\Exception $e){
            print_r($e);
            Log::debug($e);
            throw new Exception($e);
        }
    }

    public function poll($input)
    {
        Log::debug("Started ... poll : ".json_encode($input));

        extract($input);
        global $db,$system;
        $dateUtils = new DateUtils;
        $date = $dateUtils->getCurrentDate();
        $data = array();
        try {
            $conn = ConnectionManager::get('default');
            $codes = new Codes;
            extract($input);

             if(!isset($user_id) || empty($user_id)){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'You must provide user id',
                    'data'=>null
                ];

                return $response;
            }
            $user = new UserModel($user_id);
            
            if(!isset($session_id) || empty($session_id)){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'You must provide session id',
                    'data'=>null
                ];

                return $response;
            }
            
            if(!isset($option_ids) || empty($option_ids) || !is_array($option_ids)){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'You must provide options ids.',
                    'data'=>null
                ];

                return $response;
            }

            $get_session = $conn->execute(sprintf("SELECT * FROM `sessions` WHERE `sessions_id` = %s", $session_id));
            if ($get_session->count() == 0) {
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'Please send a valid session id.',
                    'data'=>null
                ];

                return $response;
            }
            $session_data = $get_session->fetch("assoc");
            //print_r($session_data);exit;
            
            $get_max_attendies = $conn->execute(sprintf("SELECT * FROM `sessions_presentor` WHERE sessions_id = %s AND `status` = '2'", $session_id));
            if ($get_max_attendies->count() > 0) {
                $max_attendies_data = $get_max_attendies->fetch("assoc");
                $max_attendies = $max_attendies_data['total_allowed_members'];
            } else {
                $max_attendies = $session_data['total_allowed_members'];
            }
            
            /* arnab   get attendee list
             */
            $get_attendies = $conn->execute(sprintf("SELECT * FROM `sessions_attends` WHERE sessions_id = %s", $session_id));
            $attendesuser=array();
            if ($get_attendies->count() > 0) {
                while ($con = $get_attendies->fetch("assoc")) {
                    $attendesuser[]=$con['user_id'];
                }
            }
            $check = $conn->execute(sprintf("SELECT * FROM sessions_attend_notify WHERE sessions_id = %s AND user_id = %s", $session_id, $user_id));
            
            $attendfn=0;
            $i_wait=false;
            if(in_array($user_id,$attendesuser))
            {
                $poll_status = 1;
            }
            elseif($check->count()>0)
            {
                $poll_status = 0;
                $attendfn=1;
                $i_wait=true;
            }
            else
            {
                if($session_data['total_attends'] < $max_attendies){
                    $poll_status = 1;
                } else {
                    $poll_status = 0;
                }
            }
    
            $session_polls = $conn->execute(sprintf("SELECT * from sessions_polls where session_id = %s", $session_id));
    
            if ($session_polls->count() == 0) {
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=> 'Session is not verified yet. Please contact to administrator.',
                    'data'=>null
                ];

                return $response;
            } else {
                $session_polls = $session_polls->fetch("assoc");
                $sessions_poll_id = $session_polls['sessions_poll_id'];
            }
            
            if(count($option_ids) > 0 ){ 
                //$conn->execute(sprintf("DELETE FROM users_sessions_polls_options WHERE user_id = %s AND session_poll_id = %s", $user->_data['user_id'], $sessions_poll_id));		
                foreach($option_ids as $option_id){
                    $conn->execute(sprintf("INSERT INTO users_sessions_polls_options (user_id, session_poll_id, option_id, status) VALUES (%s, %s, %s, '%s')", $user->_data['user_id'], $sessions_poll_id,  $option_id, $poll_status));
                }
            }
            
            $alreadyNotiSent = array();
            $get_poll = $conn->execute(sprintf("SELECT * FROM users_sessions_polls_options where session_poll_id = %s", $sessions_poll_id));
            $poll = $get_poll->rowCount();
            
            $conn->execute(sprintf("UPDATE sessions_polls SET votes = '%s' WHERE sessions_poll_id = %s", $poll ,$sessions_poll_id));
    
            $sessions_presentor = $conn->execute(sprintf("SELECT user_id FROM `sessions_presentor` WHERE sessions_id = %s AND `status` = '2'", $session_id));
            $sharer = $sessions_presentor->fetch("assoc");
    
            
            /* Arnab Mohanty : Notification*/
            $node_url=$session_data['sessions_id'].'/'.$sessions_poll_id;
            
            $user->delete_notification($session_data['created_by'], 'vote', 'session', $node_url);
            $user->delete_notification($sharer['user_id'], 'vote', 'session', $node_url);
            
            $notify_id=$sessions_poll_id;
            if($session_data['created_by']!=$user_id)
            {
            $check = $conn->execute(sprintf("SELECT * FROM `notifications` WHERE `node_type` = 'session' AND `action` = 'vote' AND to_user_id = %s AND `node_url` = '%s'", $session_data['created_by'], $node_url));
                if ($check->count() > 0){
                    $data = $check->fetch("assoc");
                    $total = $data['total']+1;
                    $total_user_ids = $data['total_user_ids'];
                    $new_total_user_ids = $total_user_ids.','.$user_id;
                    $total_user_ids_array = explode(',',$total_user_ids);
                    if (in_array($user_id, $total_user_ids_array)) 
                       $conn->execute(sprintf("UPDATE notifications SET from_user_id = %s,time = '%s', seen = '0' WHERE notification_id = %s", $user_id, $date, $data['notification_id']));
                    elseif($data['from_user_id'] != $user_id)
                        $conn->execute(sprintf("UPDATE notifications SET total_user_ids = '%s', total = %s, from_user_id = %s, time = %s, seen = '0' WHERE notification_id = %s ", $new_total_user_ids, $total, $user_id, $date, $data['notification_id']));
                    
                    $conn->execute(sprintf("UPDATE users SET user_live_notifications_counter = user_live_notifications_counter + 1 WHERE user_id = %s", $session_data['created_by']));							
                }
    
            $user->post_notification($session_data['created_by'], 'vote', 'session', $node_url, $notify_id,'');
            $alreadyNotiSent[] = $session_data['created_by'];
            }	
            //$user->post_notification($session_data['created_by'], 'vote', 'session', $session_data['sessions_id']);
            if (isset($sharer['user_id']) && !in_array($sharer['user_id'], $alreadyNotiSent)) {
            if($sharer['user_id']!=$user_id)
            {
            $check = $conn->execute(sprintf("SELECT * FROM `notifications` WHERE `node_type` = 'session' AND `action` = 'vote_sharer' AND to_user_id = %s AND `node_url` = '%s'", $sharer['user_id'], $node_url));
                if ($check->count() > 0){
                    $data = $check->fetch("assoc");
                    $total = $data['total']+1;
                    $total_user_ids = $data['total_user_ids'];
                    $new_total_user_ids = $total_user_ids.','.$user_id;
                    $total_user_ids_array = explode(',',$total_user_ids);
                    if (in_array($user_id, $total_user_ids_array)) 
                       $conn->execute(sprintf("UPDATE notifications SET from_user_id = %s,time = %s, seen = '0' WHERE notification_id = %s ", $user_id, $date, $data['notification_id']));
                    elseif($data['from_user_id'] != $user_id)
                        $conn->execute(sprintf("UPDATE notifications SET total_user_ids = %s, total = '%s', from_user_id = %s, time = '%s', seen = '0' WHERE notification_id = %s ", $new_total_user_ids, $total, $user_id, $date, $data['notification_id']));
                    
                    $conn->execute(sprintf("UPDATE users SET user_live_notifications_counter = user_live_notifications_counter + 1 WHERE user_id = %s", $sharer['user_id']));							
                }
    
            $user->post_notification($sharer['user_id'], 'vote_sharer', 'session', $node_url, $notify_id,'');
            }
            
                //$user->post_notification($sharer['user_id'], 'vote', 'session', $session_data['sessions_id']);
            }
            if($poll_status == 1){
                $message = "Poll submitted successfully.";
                $user->connect('attend', $session_id);
                
                if($user->_data['user_id'] != $session_data['created_by']){
                    $check_post = $conn->execute(sprintf("SELECT * FROM `posts` WHERE (`post_type` = 'session_attend' OR `post_type` = 'session') AND origin_id = %s", $session_data['sessions_id']));
                    if ($check_post->count() > 0){
                        $data = $check_post->fetch("assoc");
                        $total = $data['total']+1;
                        $total_user_ids = $data['total_user_ids'];
                        $new_total_user_ids = $total_user_ids.','.$user->_data['user_id'];
                        $total_user_ids_array = explode(',',$total_user_ids);
                        if (in_array($user->_data['user_id'], $total_user_ids_array)){ 
                           $conn->execute(sprintf("UPDATE `posts` SET time = '%s' WHERE post_id = %s ", $date, $data['post_id']));
                        } elseif($data['user_id'] != $user->_data['user_id']){
                            $conn->execute(sprintf("UPDATE `posts` SET total_user_ids = '%s', total = '%s', time = '%s', user_id = %s,post_type='session_attend' WHERE post_id = %s ", $new_total_user_ids, $total, $date, $user->_data['user_id'], $data['post_id']));
                        }
                        $postid=$data['post_id'];
                        
                    } else{
                        $post_relevancy = $conn->execute("SELECT * FROM `post_relevancy` where id = '1'");
                        $post_relevancy_row = $post_relevancy->fetch("assoc");
                        $conn->execute(sprintf("INSERT INTO posts (user_id, total_user_ids, origin_id, user_type, post_type, time, privacy, post_type_value) VALUES (%s, %s, %s, 'user', 'session_attend', '%s', 'public', '%s')", $user->_data['user_id'], $user->_data['user_id'], $session_data['sessions_id'], $date, $post_relevancy_row['session_attend']));
                        $post_id = $db->insert_id;
                        $conn->execute(sprintf("INSERT INTO posts_sessions (post_id, sessions_id, source) VALUES (%s, %s, %s)", $post_id, $session_data['sessions_id'], $session_data['cover_photo']));
                        $postid=$post_id;
                    }
                    /* insert /update in user_activity*/
                   $check=$conn->execute(sprintf("select * from user_activity where post_id=%s and user_id=%s", $postid, $user->_data['user_id']));
                   if($check->count() == 0){
                   $conn->execute(sprintf("insert into user_activity set post_id=%s,post_type='session_attend',user_id=%s,action='session_attend',time='%s'",$postid,$user->_data['user_id'],$date));
                   }
                   else
                   {
                   $conn->execute(sprintf("update user_activity set action='session_attend',time='%s',comment_id=0 where post_id=%s and user_id=%s",$date,$postid,$user->_data['user_id']));
                   }
                   /* insert /update in user_activity
                    */
            
                }
            } else{
                $check = $conn->execute(sprintf("SELECT * FROM sessions_attend_notify WHERE sessions_id = %s AND user_id = %s", $session_data['sessions_id'], $user->_data['user_id']));
                if ($check->count() == 0){
                    $user->connect('attend-notify', $session_id);
                }
                if($attendfn==0)
                {
                 $message = "Maximum attendies limit exceeded. your votes have been recorded. we will notify you if get the chance!";
                }
                else
                {
                    $message="Already Notified";
                }
            }
            $response['status'] = 1;
            $response['message'] = $message;
            $response['i_wait']=$i_wait;
            $response['data'] = [];

            return $response;
        }catch(\Exception $e){
            print_r($e);
            Log::debug($e);
            throw new Exception($e);
        }
    }

    public function deletePoll($input)
    {
        Log::debug("Started ... deletePoll : ".json_encode($input));

        extract($input);
        global $db,$system;
        $data = array();
        try {

            $conn = ConnectionManager::get('default');
            $codes = new Codes;
            extract($input);
            if(!isset($user_id) || empty($user_id)){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'You must provide user id',
                    'data'=>null
                ];

                return $response;
            }
            $user = new UserModel($user_id);
            
            if(!isset($session_id) || empty($session_id)){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'You must provide session id',
                    'data'=>null
                ];

                return $response;
            }

            if(!isset($option_id) || empty($option_id)){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'You must provide options id',
                    'data'=>null
                ];

                return $response;
            }          
          
            $get_session =  $conn->execute(sprintf("SELECT * FROM `sessions` WHERE `sessions_id` = %s", $session_id));
            if ($get_session->count() == 0) {
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'Please send a valid session id',
                    'data'=>null
                ];

                return $response;
            }
            
            $session_polls =  $conn->execute(sprintf("SELECT * from sessions_polls where session_id = %s", $session_id));
    
            if ($session_polls->count() == 0) {
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'Session is not verified yet. Please contact to administrator',
                    'data'=>null
                ];

                return $response;
            } else {
                $session_polls = $session_polls->fetch("assoc");
                $sessions_poll_id = $session_polls['sessions_poll_id'];
            }
            
             $conn->execute(sprintf("DELETE FROM users_sessions_polls_options WHERE user_id = %s AND session_poll_id = %s AND option_id = %s", $user->_data['user_id'], $sessions_poll_id, $option_id)) ;
            
            
            $message = "Poll unvoted.";
            
            $response['status'] = 1;
            $response['message'] = $message;
            $response['data'] = [];

            return $response;
        }catch(\Exception $e){
            print_r($e);
            Log::debug($e);
            throw new Exception($e);
        }
    }

    public function getPollVotingUsers($input)
    {
        Log::debug("Started ... getPollVotingUsers : ".json_encode($input));

        extract($input);
        global $db,$system;
        $data = array();
        try {

            $conn = ConnectionManager::get('default');
            $codes = new Codes;
            extract($input);
            if(!isset($user_id) || empty($user_id)){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'You must provide user id',
                    'data'=>null
                ];

                return $response;
            }
            $user = new UserModel($user_id);
            
            if(!isset($option_id) || empty($option_id)){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'You must provide options id',
                    'data'=>null
                ];

                return $response;
            }
            
            if(!isset($offset) || !is_numeric($offset)){
                $off = 0;
            } else {
                $off = $offset;
            }
            
            $data = $user->who_votes_session($option_id,$off);
            
            
            $response['status'] = 1;
            $response['message'] = 'User voted list.';
            $response['data'] = $data;

            return $response;
        }catch(\Exception $e){
            print_r($e);
            Log::debug($e);
            throw new Exception($e);
        }
    }

    public function edit($input)
    {
        Log::debug("Started ... edit : ".json_encode($input));

        extract($input);
        global $db,$system;
        $data = array();
        try {

            $conn = ConnectionManager::get('default');
            $codes = new Codes;
            extract($input);
            if(!isset($user_id) || empty($user_id)){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'You must provide user id',
                    'data'=>null
                ];

                return $response;
            }
            $user = new UserModel($user_id);

            if(!isset($handle)){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'You must provide handle parameters',
                    'data'=>null
                ];

                return $response;
            }
            
            $user = new UserModel($user_id);
            
            // check user activated
            if ($codes->ACTIVATION_ENABLED && !$user->_data['user_activated']) {
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'Before you can interact with other users, you need to confirm your email address',
                    'data'=>null
                ];

                return $response;
            }
            // initialize the return array
            $return = array();
    
            switch ($input['handle']) {
    
                case 'session_accept':
    
                    // valid inputs
                    /* if id is set & not numeric */
                    if (!isset($input['session_id']) || !is_numeric($input['session_id'])) {
                        $response = [
                            'status'=>$codes->RC_ERROR,
                            'message'=>'session id is required',
                            'data'=>null
                        ];
        
                        return $response;
                    }
                    
                    $get_session =$conn->execute(sprintf("SELECT * FROM `sessions` WHERE `sessions_id` = %s AND `status` IN ('2', '3', '4') ", $input['session_id']));
                    
                    if ($get_session->count() == 0) {
                        $response = [
                            'status'=>$codes->RC_ERROR,
                            'message'=>'Your session is not approved yet. Please contact to administrator',
                            'data'=>null
                        ];
        
                        return $response;
                    }
                    $session_data = $get_session->fetch("assoc");
                    /* check product price */
                    if (!isset($input['ans1']) || empty($input['ans1'])) {
                        $response = [
                            'status'=>$codes->RC_ERROR,
                            'message'=>'Please add your Answer of Question 1',
                            'data'=>null
                        ];
        
                        return $response;
                    }
                    if (!isset($input['ans2']) || empty($input['ans2'])) {
                        $response = [
                            'status'=>$codes->RC_ERROR,
                            'message'=>'Please add your Answer of Question 2',
                            'data'=>null
                        ];
        
                        return $response;
                    }
                    if (!isset($input['total_allowed_members']) || empty($input['total_allowed_members'])) {
                        $response = [
                            'status'=>$codes->RC_ERROR,
                            'message'=>'Please add Total Allowed Members',
                            'data'=>null
                        ];
        
                        return $response;
                    }
                    $ques = $input['ans1'].','.$input['ans2'];
                    // edit product
                    $user->edit_session($input['session_id'],$session_data['title'] , $ques , $input['total_allowed_members']);
                    
                    $response['status'] = 1;
                    $response['message'] = 'Successfully accepted session';
                    $response['data'] = array('session_id'=>$input['session_id']);
                    
                    break;
                case 'session_edit':
    
                    // valid inputs
                    /* if id is set & not numeric */
                    if (!isset($input['session_id']) || !is_numeric($input['session_id'])) {
                        $response = [
                            'status'=>$codes->RC_ERROR,
                            'message'=>'session id is required',
                            'data'=>null
                        ];
        
                        return $response;
                    }
                    /* if product info not set */
                    if (!isset($input['event_date']) || !isset($input['event_time']) || !isset($input['venue_id'])) {
                        $response = [
                            'status'=>$codes->RC_ERROR,
                            'message'=>'Please send all parameters',
                            'data'=>null
                        ];
        
                        return $response;
                    }
                    /* check product price */
                    if (empty($input['event_date'])) {
                        $response = [
                            'status'=>$codes->RC_ERROR,
                            'message'=>'Please add your Event Date',
                            'data'=>null
                        ];
        
                        return $response;
                    }
                    if (empty($input['event_time'])) {
                        $response = [
                            'status'=>$codes->RC_ERROR,
                            'message'=>'Please add your Event Time',
                            'data'=>null
                        ];
        
                        return $response;
                    }
                    if (empty($input['event_duration'])) {
                        $response = [
                            'status'=>$codes->RC_ERROR,
                            'message'=>'Please add your Event Duration',
                            'data'=>null
                        ];
        
                        return $response;
                    }
                    if (empty($input['venue_id'])) {
                        $response = [
                            'status'=>$codes->RC_ERROR,
                            'message'=>'Please add your Venue',
                            'data'=>null
                        ];
        
                        return $response;
                    }
                    
                    if(!isset($_FILES['event_photo'])){
                        // throw new Exception('Please select event photo.');
                    }
                
                    // edit product
                    $s = $user->edit_sessions_data($input['session_id'], $input['event_date'], $input['event_time'], $input['venue_id'], $input['event_duration'], $_FILES);
                    
                    if(is_array($s)){
                        return $s;
                    }
                    
                    /* return */
                    $response['status'] = 1;
                    $response['message'] = 'Successfully updated event date and time.';
                    $response['data'] = array('session_id'=>$input['session_id']);
                    
                    break;
                    
                case 'edit_topics':
                    $check =$conn->execute(sprintf("SELECT * FROM sessions WHERE sessions_id = %s", $input['session_id']));
                    $data = $check->fetch("assoc");
                    $topics_n = implode(',',@$input['topics']);
                    $topicsSave = $data['topics'].','.$topics_n;
                    $topicsSave = rtrim($topicsSave,",");
                    
                    $prerequisitNew = implode(',',@$input['prerequisite']);
                    $prerequisiteSave = $data['prerequisite'].','.$prerequisitNew;
                    $prerequisiteSave = rtrim($prerequisiteSave,",");
        
                   $conn->execute(sprintf("UPDATE sessions SET prerequisite = '%s',topics = '%s'  WHERE sessions_id = %s", $prerequisiteSave, $topicsSave, $input['session_id']));
    
                    $get_attendies =$conn->execute(sprintf("SELECT * FROM `sessions_attends` WHERE `sessions_id` = %s ", $input['session_id']));
                    $notification_sent = array();
                    while ($attendies_data = $get_attendies->fetch("assoc")) {
                        if($attendies_data['user_id'] != $user->_data['user_id'])
                        $user->post_notification($attendies_data['user_id'], 'added_topics', 'session', $input['session_id']);
                    }
                    
                    $response['status'] = 1;
                    $response['message'] = 'Successfully updated toipics.';
                    $response['data'] = array('session_id'=>$input['session_id']);
                
                break;	
                    
                    
    
                case 'session_photos_edit':
    
                    // valid inputs
                    /* if id is set & not numeric */
                    if (!isset($input['session_id']) || !is_numeric($input['session_id'])) {
                        $response = [
                            'status'=>$codes->RC_ERROR,
                            'message'=>'session id is required',
                            'data'=>null
                        ];
        
                        return $response;
                    }
    
                    $eventPhotos = '';
                    $eventPhotosEdit = '';
                    $edit_ids=[];
                    foreach ($input['event_photo'] AS $image_id => $value) {
                        if (isset($value) && $value != '') {
                            $eventPhotos .= $value . ',';
                        }
                    }
                    foreach ($input['edit_event_photo'] AS $image_id => $value) {
                        if (isset($value) && $value != '') {
                            $eventPhotosEdit .= $value . ',';
                            $edit_ids[]=$image_id;
                        }
                    }
    
                    $eventPhotos = rtrim($eventPhotos, ",");
                    $eventPhotosEdit = rtrim($eventPhotosEdit, ",");
                    // edit product
                    $s = $user->edit_sessions_photos($input['id'], $eventPhotos, $eventPhotosEdit,$edit_ids);
                    if(is_array($s)){
                        return $s;
                    }
                    /* return */
                    $return['callback'] = 'window.location = "' . $codes->SYSTEM_URL . '/session/' . $input['id'] . '/photos";';
                    break;
    
                default:
                    throw new Exception('Invalid handle type.');
                    break;
            }

            return $response;
        }catch(\Exception $e){
            print_r($e);
            Log::debug($e);
            throw new Exception($e);
        }
    }

    public function postCommentReply($input)
    {
        Log::debug("Started ... postCommentReply : ".json_encode($input));

        extract($input);
        global $db,$system;
        $data = array();
        try {

            $conn = ConnectionManager::get('default');
            $codes = new Codes;
            extract($input);
            if(!isset($user_id) || empty($user_id)){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'You must provide user id',
                    'data'=>null
                ];

                return $response;
            }
            $user = new UserModel($user_id);
            
            if(!isset($sessions_id) || empty($sessions_id)){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'You must provide sessions id',
                    'data'=>null
                ];

                return $response;
            }
            
            if(!isset($input['post']) || empty($input['post'])){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'post is required',
                    'data'=>null
                ];

                return $response;
            }

            if(!isset($input['parent_comment_id']) || empty($input['parent_comment_id'])){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'parent_comment_id is required',
                    'data'=>null
                ];

                return $response;
            }
            
            $dateUtils = new DateUtils;
            $date = $dateUtils->getCurrentDate();
            
            $replyData['sessions_id'] = $input['sessions_id'];
            $replyData['sc_post'] = $input['post'];
            $replyData['id'] = $input['parent_comment_id'];
            
            $input['sessions_comment_id'] = $user->add_session_comment_reply( $replyData );
            $input['time'] = $date;
            $response['status'] = 1;
            $response['message'] = '';
            $response['data'] = $input;

            return $response;
        }catch(\Exception $e){
           // print_r($e);
            Log::debug($e);
            throw new Exception($e);
        }
    }

    public function assignRoom($input)
    {
        Log::debug("Started ... assignRoom : ".json_encode($input));

        extract($input);
        global $db,$system;
        $data = array();
        try {

            $conn = ConnectionManager::get('default');
            $codes = new Codes;
            extract($input);
            
            if(!isset($session_id) || empty($session_id)){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'Invalid request',
                    'data'=>null
                ];

                return $response;
            }

            if(!isset($final_place) || empty($final_place)){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=> 'final place required',
                    'data'=>null
                ];

                return $response;
            }
            
            
            $return = $this->add_final_room( $input['session_id'], $input['final_place'] );			

			if($return){
				$response['status'] = 1;
			    $response['message'] = 'Final Room added to the session';
			    $response['data'] = [];
			}

            return $response;
        }catch(\Exception $e){
            print_r($e);
            Log::debug($e);
            throw new Exception($e);
        }
    }

    public function add_final_room( $session_id, $final_place ) {

        global $db;
        $conn = ConnectionManager::get('default');
        $codes = new Codes;
    
        $rs = $conn->execute(sprintf("UPDATE sessions SET final_place = '%s' WHERE sessions_id = '%s'", $final_place, $session_id));   
    
        $response = $rs->rowCount();
    
        if ( $response !== false ) {
            
            return $this->get_session_data( $session_id );
    
        } else {
    
            return false;
    
        }
    
    }

    public function get_session_data( $session_id ) {

        global $db;
        $conn = ConnectionManager::get('default');
        $codes = new Codes;
    
    
        $get_session = $conn->execute(sprintf("SELECT * FROM sessions WHERE sessions_id = %s", $session_id));
        
        $session_data = $get_session->fetch("assoc");
        $session_data['co_ordinators_data'] = array();
    
        if ( !empty($session_data['co_ordinators']) ) {
                
            $co_ordinators_ids = explode(',', $session_data['co_ordinators']);
    
            foreach ( $co_ordinators_ids as $id ) {
    
                $user = new UserModel($id);
    
                $cord = $conn->execute(sprintf("SELECT user_id,user_name,user_picture,user_fullname,user_gender,user_work_title,user_work_place FROM `users` WHERE user_id = %s ", $id));
                $cord_data = $cord->fetch("assoc");
                $cord_data['user_picture'] = $user->get_picture($cord_data['user_picture'], $cord_data['user_gender']);
                $cord_data['connection'] = $user->connection($cord_data['user_id']);
    
                array_push($session_data['co_ordinators_data'], $cord_data);
            }
        }
    
        return $session_data;
    
    }

    public function getAnswerReplies($input)
    {
        Log::debug("Started ... getAnswerReplies : ".json_encode($input));

        extract($input);
        global $db,$system;
        $data = array();
        try {

            $conn = ConnectionManager::get('default');
            $codes = new Codes;
            extract($input);

            if(!isset($user_id) || empty($user_id)){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'You must provide user id',
                    'data'=>null
                ];

                return $response;
            }
            $user = new UserModel($user_id);
            
            if(!isset($sessions_qa_id) || empty($sessions_qa_id)){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'sessions_qa_id is required',
                    'data'=>null
                ];

                return $response;
            }

            if(!isset($comment_id) || empty($comment_id)){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=> 'comment id required',
                    'data'=>null
                ];

                return $response;
            }
            
            /* if order not set */
            if(!isset($input['order'])) {
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=> 'corder field required',
                    'data'=>null
                ];

                return $response;
            }

            $valid_orders = array("next", "prev");
            if ( !isset($input['order']) || !in_array( $input['order'], $valid_orders) ) {
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=> 'Invalid value of the field: order',
                    'data'=>null
                ];

                return $response;
            }

            $target=(isset($input['target']))?$input['target']:0;
            $user = new UserModel($input['user_id']);
            $replies = $user->get_session_answer_replies_next_prev($input['sessions_qa_id'],$input['comment_id'],$input['order'],$target);
            
            $response['status'] = 1;
            $response['message'] = '';
            $response['data'] = $replies;

            return $response;
        }catch(\Exception $e){
            print_r($e);
            Log::debug($e);
            throw new Exception($e);
        }
    }

    public function editSessionAnswer($input)
    {
        Log::debug("Started ... editSessionAnswer : ".json_encode($input));

        extract($input);
        global $db,$system;
        $data = array();
        try {

            $conn = ConnectionManager::get('default');
            $codes = new Codes;
            extract($input);
            if(!isset($user_id) || empty($user_id)){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'You must provide user id',
                    'data'=>null
                ];

                return $response;
            }
            $user = new UserModel($user_id);
            
            if(!isset($sessions_qa_id) || empty($sessions_qa_id)){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'sessions_qa_id is required',
                    'data'=>null
                ];

                return $response;
            }
            
            if(!isset($input['post']) || empty($input['post'])){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'post is required',
                    'data'=>null
                ];

                return $response;
            }

            $check = $conn->execute(sprintf("SELECT * FROM `sessions_qa` WHERE sessions_qa_id = %s", $input['sessions_qa_id']));
            if($check->count() == 0){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'Invalid sessions_qa_id',
                    'data'=>null
                ];

                return $response;
            }

            $comment = $check->fetch("assoc");

            if($comment['user_id'] != $user->_data['user_id']){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'You are not authorized to do this',
                    'data'=>null
                ];

                return $response;
            }
            
            $conn->execute(sprintf("UPDATE sessions_qa SET post = '%s' WHERE sessions_qa_id = %s", $input['post'], $input['sessions_qa_id']));
            
            $response['status'] = 1;
            $response['message'] = '';
            $response['data'] = true;

            return $response;
        }catch(\Exception $e){
            print_r($e);
            Log::debug($e);
            throw new Exception($e);
        }
    }

    public function deleteSessionAnswer($input)
    {
        Log::debug("Started ... deleteSessionAnswer : ".json_encode($input));

        extract($input);
        global $db,$system;
        $data = array();
        try {

            $conn = ConnectionManager::get('default');
            $codes = new Codes;
            extract($input);
            if(!isset($user_id) || empty($user_id)){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'You must provide user id',
                    'data'=>null
                ];

                return $response;
            }
            $user = new UserModel($user_id);
            
            if(!isset($sessions_qa_id) || empty($sessions_qa_id)){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'sessions_qa_id is required',
                    'data'=>null
                ];

                return $response;
            }
            
            $check = $conn->execute(sprintf("SELECT sessions_qa.*,sessions.created_by,sessions.presentors FROM `sessions_qa` INNER JOIN sessions ON (sessions.sessions_id = sessions_qa.sessions_id) WHERE sessions_qa.sessions_qa_id = %s",$input['sessions_qa_id']));
            if($check->count() == 0){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'Invalid sessions_qa_id',
                    'data'=>null
                ];

                return $response;
            }
            $comment = $check->fetch("assoc");
    
            if($comment['qa_type'] == 3){
                $parentAnswer = $conn->execute(sprintf("SELECT sessions_qa.* FROM `sessions_qa` WHERE sessions_qa.sessions_qa_id = %s", $comment['parent_question_id']));
                if($parentAnswer->count() > 0){
                    $parentAnswerComment = $parentAnswer->fetch("assoc");
                    $parentQuestion = $conn->execute(sprintf("SELECT sessions_qa.* FROM `sessions_qa` WHERE sessions_qa.sessions_qa_id = %s", $parentAnswerComment['parent_question_id']));
                    if($parentQuestion->count() > 0){
                        $parentQuestionComment = $parentQuestion->fetch("assoc");
                        if(!in_array($user->_data['user_id'],[$parentAnswerComment['user_id'],$$parentQuestionComment['user_id'],$comment['user_id'],$comment['created_by'],$comment['presentors']])){
                            $response = [
                                'status'=>$codes->RC_ERROR,
                                'message'=>'You are not authorized to do this',
                                'data'=>null
                            ];
            
                            return $response;
                        }
                        $conn->execute(sprintf("DELETE FROM sessions_qa WHERE sessions_qa_id = %s",$input['sessions_qa_id']));
                    }
                }
            } elseif($comment['qa_type'] == 2){
                $parentQuestion = $conn->execute(sprintf("SELECT sessions_qa.* FROM `sessions_qa` WHERE sessions_qa.sessions_qa_id = %s",$comment['parent_question_id']));
                if($parentQuestion->count() > 0){
                    $parentQuestionComment = $parentQuestion->fetch("assoc");
                    if(!in_array($user->_data['user_id'],[$parentQuestionComment['user_id'],$comment['user_id'],$comment['created_by'],$comment['presentors']])){
                        $response = [
                            'status'=>$codes->RC_ERROR,
                            'message'=>'You are not authorized to do this',
                            'data'=>null
                        ];
        
                        return $response;
                    }
                    $conn->execute(sprintf("DELETE FROM sessions_qa WHERE qa_type = 3 AND parent_question_id = %s",$input['sessions_qa_id']));
                    $conn->execute(sprintf("DELETE FROM sessions_qa WHERE sessions_qa_id = %s",$input['sessions_qa_id']));
                    
                    $conn->execute(sprintf("DELETE FROM sessions_qa_follows WHERE sessions_qa_id = %s",$input['sessions_qa_id']));
                    $conn->execute(sprintf("DELETE FROM sessions_qa_invites WHERE sessions_qa_id = %s",$input['sessions_qa_id']));
                    $conn->execute(sprintf("DELETE FROM sessions_qa_likes WHERE sessions_qa_id = %s",$input['sessions_qa_id']));
                }
                
            } else {
                if(!in_array($user->_data['user_id'],[$comment['user_id'],$comment['created_by'],$comment['presentors']])){
                    $response = [
                        'status'=>$codes->RC_ERROR,
                        'message'=>'You are not authorized to do this',
                        'data'=>null
                    ];
    
                    return $response;
                }
                
                $conn->execute(sprintf("DELETE FROM sessions_qa WHERE sessions_qa_id = %s",$input['sessions_qa_id']));
                
                $conn->execute(sprintf("DELETE FROM sessions_qa_follows WHERE sessions_qa_id = %s",$input['sessions_qa_id']));
                $conn->execute(sprintf("DELETE FROM sessions_qa_invites WHERE sessions_qa_id = %s",$input['sessions_qa_id']));
                $conn->execute(sprintf("DELETE FROM sessions_qa_likes WHERE sessions_qa_id = %s",$input['sessions_qa_id']));
                
                $answer = $conn->execute(sprintf("SELECT * FROM `sessions_qa` WHERE parent_question_id = %s",$input['sessions_qa_id']));
                if($answer->count() > 0 ){
                    while($ans = $answer->fetch("assoc")){
                        $conn->execute(sprintf("DELETE FROM sessions_qa WHERE qa_type = 3 AND parent_question_id = %s",$ans['sessions_qa_id']));
                        
                        
                        $conn->execute(sprintf("DELETE FROM sessions_qa WHERE sessions_qa_id= %s",$ans['sessions_qa_id']));
                        
                        $conn->execute(sprintf("DELETE FROM sessions_qa_follows WHERE sessions_qa_id = %s",$ans['sessions_qa_id']));
                        $conn->execute(sprintf("DELETE FROM sessions_qa_invites WHERE sessions_qa_id = %s",$ans['sessions_qa_id']));
                        $conn->execute(sprintf("DELETE FROM sessions_qa_likes WHERE sessions_qa_id = %s",$ans['sessions_qa_id']));
                    }
                }
            }
            
            $response['status'] = 1;
            $response['message'] = '';
            $response['data'] = true;

            return $response;
        }catch(\Exception $e){
            print_r($e);
            Log::debug($e);
            throw new Exception($e);
        }
    }

    public function assignCoordinator($input)
    {
        Log::debug("Started ... assignCoordinators : ".json_encode($input));

        extract($input);
        global $db,$system;
        $data = array();
        try {

            $conn = ConnectionManager::get('default');
            $codes = new Codes;
            extract($input);
            /* if coordinators not set */
			if(!isset($input['coordinators'])) {
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'coordinators array is required',
                    'data'=>null
                ];

                return $response;
			};

			/* if coordinators not array */
			if ( !is_array($input['coordinators']) ) {
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'coordinators must be an array',
                    'data'=>null
                ];

                return $response;
			}
			
			$return = $this->assign_coordinators( $input['session_id'], $input['coordinators'] );

			if($return){
				$response['status'] = 1;
			    $response['message'] = 'Coordinators added to the session';
			    $response['data'] = [];
			}

            return $response;
        }catch(\Exception $e){
            print_r($e);
            Log::debug($e);
            throw new Exception($e);
        }
    }

    function assign_coordinators( $session_id, $coordinators ) {

        global $db;
    
        $conn = ConnectionManager::get('default');
        $codes = new Codes;
        $coordinator_ids = implode(',', $coordinators);
    
        $rx = $conn->execute(sprintf("UPDATE sessions SET co_ordinators = '%s' WHERE sessions_id = %s", $coordinator_ids, $session_id));   
    
        $response = $rx->rowCount();
    
        if ( $response !== false ) {
            
            return $this->get_session_data( $session_id );
    
        } else {
    
            return false;
    
        }
    
    }

    public function conduct($input)
    {
        Log::debug("Started ... conduct : ".json_encode($input));

        extract($input);
        global $db,$system;
        $data = array();
        try {

            $conn = ConnectionManager::get('default');
            $codes = new Codes;
            extract($input);
            $dateUtils = new DateUtils;
            $date = $dateUtils->getCurrentDate();
            if(!isset($user_id)){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'You must provide user id',
                    'data'=>null
                ];

                return $response;
            }
            //user_type validation
            /*
            if(!isset($user_type)){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'Please enter title.',
                    'data'=>null
                ];

                return $response;
            }
            if (empty($user_type)) {
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'You must enter a user_type for your session',
                    'data'=>null
                ];

                return $response;
            }
            */
            /* validate title */
            if(!isset($title)){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'Please enter title.',
                    'data'=>null
                ];

                return $response;
            }
            if (empty($title)) {
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'You must enter a title for your session',
                    'data'=>null
                ];

                return $response;
            }
            if (strlen($title) < 3) {
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'Session title must be at least 3 characters long. Please try another',
                    'data'=>null
                ];

                return $response;
            }
            
            if(!isset($topics) || !is_array($topics)){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'Please enter topics.',
                    'data'=>null
                ];

                return $response;
            }
            $topics = implode(',',array_filter($topics));
            if(empty($topics)){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'Please enter topics.',
                    'data'=>null
                ];

                return $response;
            }
            
            if(!isset($prerequisite) || !is_array($prerequisite)){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'Please enter prerequisite.',
                    'data'=>null
                ];

                return $response;
            }
            $prerequisite = implode(',',array_filter($prerequisite));
            if(empty($prerequisite)){
                 $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'Please enter prerequisite.',
                    'data'=>null
                ];

                return $response;
            }
            
            if(!isset($description) || empty($description)){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'Please enter description.',
                    'data'=>null
                ];

                return $response;
            }
            
            if(!isset($_FILES['cover_photo'])){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'You must upload a cover photo for your session',
                    'data'=>null
                ];

                return $response;
            }
            
            if(!isset($event_date) || !is_array($event_date)){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'Event date is required.',
                    'data'=>null
                ];

                return $response;
            }
            
            //$user_dates = count($event_date);
            //$unique_dates = count(array_unique($event_date));
            //if($user_dates != $unique_dates){
            //	throw new Exception('Same date added more than once.');
            //}
            //$event_dates = implode(',',array_filter($input['event_date']));
            //if(empty($event_dates)){
            //	throw new Exception('Event date is required.');
            //}
            //$event_dates = "";
            foreach($input['event_date'] as $e_date){
                if($e_date < $date){
                    $response = [
                        'status'=>$codes->RC_ERROR,
                        'message'=>'You can not add backdate.',
                        'data'=>null
                    ];
    
                    return $response;
                }
                //$event_dates .= date('Y-m-d',strtotime($e_date)).",";
            }
            //$event_dates = rtrim($event_dates,',');
            $event_dates=$input['event_date'];
            
            if(count($venues)<2)
            {
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'Send Atleast two venue.',
                    'data'=>null
                ];

                return $response;
            }
            
            if(!isset($interest_ids) || !is_array($interest_ids)){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'Please select interest(s).',
                    'data'=>null
                ];

                return $response;
            }
            
            if(!isset($venues) || !is_array($venues)){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'You must enter a location for your session.',
                    'data'=>null
                ];

                return $response;
            }
            
            //$venues = implode(',',array_filter($venues));
            //if(empty($venues)){
            //	throw new Exception('You must enter a location for your session.');
            //}
            
            // adding presentor entry
            if (!isset($ans1) || empty($input['ans1'])) {
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'Please add your Answer of Question 1.',
                    'data'=>null
                ];

                return $response;
            }
            if (!isset($user_type) || empty($input['user_type'])) {
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'Please add your user_type',
                    'data'=>null
                ];

                return $response;
            }
            if (!isset($community_id) || empty($input['community_id'])) {
                $input['comment_id'] =0;
                $community_id = 0;
            }
            if (!isset($ans2) || empty($input['ans2'])) {
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'Please add your Answer of Question 2.',
                    'data'=>null
                ];

                return $response;
            }
            if (!isset($people_allowed) || empty($input['people_allowed']) || $input['people_allowed'] < 0) {
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'Please enter number of people allowed.',
                    'data'=>null
                ];

                return $response;
            }
            
            $user = new UserModel($user_id);
            

            if(!isset($link)){
                $link = '';
            }

            // check user activated
            if ($system['activation_enabled'] && !$user->_data['user_activated']) {
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'Before you can accept any session, you need to confirm your email address',
                    'data'=>null
                ];

                return $response;
            }
            $eventPhotos = '';

            
            $sessionId = $user->session_create($topics, $prerequisite, $input['title'], $description,$_FILES ,$event_dates, $eventPhotos, $venues,5 , $user_type , $community_id , $link);

            if(is_array($sessionId)){
                return $sessionId;
            }
            
            //$sessionId = $user->session_create($input['title'], $input['description'], $_FILES, $input['location'],1);
            
            $user->create_session_interest($sessionId, $input['interest_ids']);
            
            $ques = $input['ans1'].','.$input['ans2'];
            $user->edit_session($sessionId, $input['title'],$ques,$input['people_allowed']);
            
            $response['status'] = 1;
            $response['message'] = 'Successfully created session';
            $response['data'] = $sessionId;

            return $response;
        } catch(\Exception $e){
            print_r($e);
            Log::debug($e);
            throw new Exception($e);
        }
    }

    public function create($input)
    {
        Log::debug("Started ... create : ".json_encode($input));

        extract($input);
        global $db,$system;
        $data = array();
        try {

            $conn = ConnectionManager::get('default');
            $codes = new Codes;
            extract($input);
            $dateUtils = new DateUtils;
            $date = $dateUtils->getCurrentDate();
            if(!isset($user_id)){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'You must provide user id',
                    'data'=>null
                ];

                return $response;
            }
            /* validate title */
            if(!isset($title)){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'Please enter title.',
                    'data'=>null
                ];

                return $response;
            }
            if (empty($title)) {
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'You must enter a title for your session',
                    'data'=>null
                ];

                return $response;
            }
            if (strlen($title) < 3) {
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'Session title must be at least 3 characters long. Please try another',
                    'data'=>null
                ];

                return $response;
            }
            
            if(!isset($topics) || !is_array($topics)){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'Please enter topics.',
                    'data'=>null
                ];

                return $response;
            }
            $topics = implode(',',array_filter($topics));
            if(empty($topics)){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'Please enter topics.',
                    'data'=>null
                ];

                return $response;
            }
            
            if(!isset($prerequisite) || !is_array($prerequisite)){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'Please enter prerequisite.',
                    'data'=>null
                ];

                return $response;
            }
            $prerequisite = implode(',',array_filter($prerequisite));
            if(empty($prerequisite)){
                 $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'Please enter prerequisite.',
                    'data'=>null
                ];

                return $response;
            }
            if (!isset($user_type) || empty($input['user_type'])) {
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'Please add your user_type',
                    'data'=>null
                ];

                return $response;
            }
            if (!isset($community_id) || empty($input['community_id'])) {
                $input['comment_id'] =0;
                $community_id = 0;
            }
            
            if(!isset($description) || empty($description)){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'Please enter description.',
                    'data'=>null
                ];

                return $response;
            }
            
            if(!isset($_FILES['cover_photo'])){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'You must upload a cover photo for your session',
                    'data'=>null
                ];

                return $response;
            }
            
            if(!isset($event_date) || !is_array($event_date)){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'Event date is required.',
                    'data'=>null
                ];

                return $response;
            }
            
            //$user_dates = count($event_date);
            //$unique_dates = count(array_unique($event_date));
            //if($user_dates != $unique_dates){
            //	throw new Exception('Same date added more than once.');
            //}
            //$event_dates = implode(',',array_filter($input['event_date']));
            //if(empty($event_dates)){
            //	throw new Exception('Event date is required.');
            //}
            //$event_dates = "";
            foreach($input['event_date'] as $e_date){
                if($e_date < $date){
                    $response = [
                        'status'=>$codes->RC_ERROR,
                        'message'=>'You can not add backdate.',
                        'data'=>null
                    ];
    
                    return $response;
                }
                //$event_dates .= date('Y-m-d',strtotime($e_date)).",";
            }
            //$event_dates = rtrim($event_dates,',');
            $event_dates=$input['event_date'];
            
            if(count($venues)<2)
            {
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'Send Atleast two venue.',
                    'data'=>null
                ];

                return $response;
            }
            
            if(!isset($interest_ids) || !is_array($interest_ids)){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'Please select interest(s).',
                    'data'=>null
                ];

                return $response;
            }
            
            if(!isset($venues) || !is_array($venues)){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'You must enter a location for your session.',
                    'data'=>null
                ];

                return $response;
            }
            
            //$venues = implode(',',array_filter($venues));
            //if(empty($venues)){
            //	throw new Exception('You must enter a location for your session.');
            //}
            
            // adding presentor entry
            // if (!isset($ans1) || empty($input['ans1'])) {
            //     $response = [
            //         'status'=>$codes->RC_ERROR,
            //         'message'=>'Please add your Answer of Question 1.',
            //         'data'=>null
            //     ];

            //     return $response;
            // }
            // if (!isset($ans2) || empty($input['ans2'])) {
            //     $response = [
            //         'status'=>$codes->RC_ERROR,
            //         'message'=>'Please add your Answer of Question 2.',
            //         'data'=>null
            //     ];

            //     return $response;
            // }
            // if (!isset($people_allowed) || empty($input['people_allowed']) || $input['people_allowed'] < 0) {
            //     $response = [
            //         'status'=>$codes->RC_ERROR,
            //         'message'=>'Please enter number of people allowed.',
            //         'data'=>null
            //     ];

            //     return $response;
            // }
            
            $user = new UserModel($user_id);
            
            // check user activated
            if ($system['activation_enabled'] && !$user->_data['user_activated']) {
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'Before you can accept any session, you need to confirm your email address',
                    'data'=>null
                ];

                return $response;
            }
            $eventPhotos = '';
            if(!isset($link) or $link == null){
                $link = '';
            }
            
            $sessionId = $user->session_create($topics, $prerequisite, $input['title'], $description,$_FILES ,$event_dates, $eventPhotos, $venues,1 ,$user_type , $community_id, $link);

            if(is_array($sessionId)){
                return $sessionId;
            }
            
            //$sessionId = $user->session_create($input['title'], $input['description'], $_FILES, $input['location'],1);
            
            $user->create_session_interest($sessionId, $input['interest_ids']);
            
            // $ques = $input['ans1'].','.$input['ans2'];
            // $user->edit_session($sessionId, $input['title'],$ques,$input['people_allowed']);
            
            $response['status'] = 1;
            $response['message'] = 'Successfully created session';
            $response['data'] = $sessionId;

            return $response;
        } catch(\Exception $e){
            print_r($e);
            Log::debug($e);
            throw new Exception($e);
        }
    }

    public function getMySession($input)
    {
        Log::debug("Started ... editSessionComment : ".json_encode($input));

        extract($input);
        global $db,$system;
        $data = array();
        try {

            $conn = ConnectionManager::get('default');
            $codes = new Codes;
            extract($input);
            //return $input;
            if(!isset($user_id) || empty($user_id)){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'You must provide user id',
                    'data'=>null
                ];

                return $response;
            }
            
            if(!isset($session_id) || empty($session_id)){
                $response = [
                    'status'=>$codes->RC_ERROR,
                    'message'=>'session_id is required',
                    'data'=>null
                ];

                return $response;
            }
            
            $user = new PostsDao;
            $args['userid'] = $user_id;

            $result = $user->getData($session_id , "sessions" , $args );
            
            $response['status'] = 1;
            $response['message'] = '';
            $response['data'] = $result;

            return $response;
        }catch(\Exception $e){
            print_r($e);
            Log::debug($e);
            throw new Exception($e);
        }
    }
}