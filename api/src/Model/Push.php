<?php 
 namespace App\Model;

class Push {
    //notification title
    private $title;
 
    //notification message 
    private $message;
 
    //notification image url 
    private $image;
	
	private $type;
 
    //initializing values in this constructor
    function __construct($title, $message, $image, $type) {
         $this->title = $title;
         $this->message = $message; 
         $this->image = $image; 
         $this->type = $type; 
    }
    
    //getting the push notification
    public function getPush() {
        $res = array();
        $res['data']['title'] = $this->title;
        $res['data']['message'] = $this->message;
        $res['data']['image'] = $this->image;
        $res['data']['type'] = $this->type;
        return $res;
    }
 
}

?>