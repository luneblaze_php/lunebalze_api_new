<?php

namespace App\Model;

use App\Model\UserModel;
use App\Utils\Codes;
use App\Utils\DateUtils;
use Cake\Core\Exception\Exception;
use Cake\Datasource\ConnectionManager;
use Cake\Log\Log;

use App\Model\Firebase;
use App\Model\Push;

/* Conversation Group
 * used for Apis 
 */


class ConversationGroup{
	
	public $status;
	public $message;
	public $data;

	public $_data = array();

	public function __construct($group_id = null) {
        $conn = ConnectionManager::get('default');
        global $db, $system;
        $this->status = 0;
		$this->message = '';
		$this->data = null;
		
		if($group_id){
            $query = $conn->execute(sprintf("SELECT * FROM conversation_groups WHERE group_id = %s ", $group_id));

            if ($query->count() > 0) {

                $this->_data = $query->fetch("assoc");

            } else{
				throw new Exception("Invalid Group.");
			}
		} else{
			$this->_data = array();
		}

    }

    /**
     * get_group_data
     *
     * @return array
     */
    public function get_group_data()
    {
        global $db, $system;
        $codes = new Codes;
        $conn = ConnectionManager::get('default');

        $response = $this->_data;
       //print_r($response);

        if ( !empty($response['group_photo']) ) {
            $response['group_photo'] = $codes->SYSTEM_URL . '/'. $codes->UPLOADS_DIRECTORY . '/' . $response['group_photo'];
        }

        $members = $conn->execute(sprintf("SELECT * FROM conversations_users WHERE  group_status !='1' AND conversation_id = %s", $response['conversation_id']));

        $response['no_of_members'] = $members->count();
        $response['members'] = array();

        while ( $member = $members->fetch("assoc") ) {

            //$user = new UserModel( $member['user_id'] );AND group_status= '0'

            $userData = $this->get_user_by_id($member['user_id']);
            
           
           
            /* if ( !empty($userData['user_picture']) ) {
                $img = $codes->UPLOADS_DIRECTORY . '/' . $user->_data['user_picture'];
            } else {
                $img = '';
            } */
            
            if($userData && $userData[0]){
                $userData = $userData[0];
                $member_entry = array(
                    "user_id" => $member['user_id'],
                    "user_name" => $userData['user_name'],
                    "user_full_name" => $userData['user_fullname'],
                    "user_email" => $userData['user_email'],
                    "user_picture" => $userData['user_picture'],
                    "role" => $member['user_type'],
                    "seen" => $member['seen'],
                    "deleted" => $member['deleted'],
                    "group_status" => $member['group_status'],

                );
                

                //print_r($member_entry);

                array_push($response['members'], $member_entry);
            }
        }

        return $response;        
    }

    /**
     * get_group_data_by_conversation_id
     *
     * @return array
     */
    public function get_group_data_by_conversation_id( $conversation_id )
    {
        global $db, $system;

        $response = array();

        $group = $conn->execute(sprintf("SELECT * FROM conversation_groups WHERE conversation_id = %s ", secure($conversation_id, 'int'))) or _apiError('SQL_ERROR_THROWEN');

        $group = $group->fetch("assoc");
        if ( !empty($group) ) {
            $group = new ConversationGroup( $group['group_id'] );
            $response = $group->get_group_data();
        }

        return $response;

    }

    /**
     * add_group_members
     *
     * @param array $recipients
     * @return boolean
     */
    public function add_group_members( $recipients = array(),$added_user_id=0 )
    {
        global $db,$date,$system;
        $conn = ConnectionManager::get('default');
        $codes = new Codes;

        $response = array();
		/* update the conversation with last message id 		
		$conn->execute(sprintf("UPDATE conversations SET last_message_id = %s WHERE conversation_id = %s", secure($message_id, 'int'), secure($conversation_id, 'int'))) or _apiError('SQL_ERROR_THROWEN');*/				
		/* update sender (me) with last message id 		
		$conn->execute(sprintf("UPDATE users SET user_live_messages_lastid = %s WHERE user_id = %s", secure($message_id, 'int'), secure($this->_data['user_id'], 'int'))) or _apiError('SQL_ERROR_THROWEN');				
		Create a System Message end */				
		
        $addedBy = $this->get_user_by_id($added_user_id);
        $addedBy = $addedBy[0];
        foreach ( $recipients as $user_id ) {				
			$check = $conn->execute(sprintf("SELECT * FROM conversations_users WHERE conversation_id = %s AND user_id = %s", $this->_data['conversation_id'], $user_id));			
			if($check->count()){
				$deletedUser = $check->fetch("assoc");
				if($deletedUser['group_status'] == '1'){
					$conn->execute(sprintf("UPDATE `conversations_users` SET `group_status`='0' WHERE conversation_id = %s AND user_id = %s", $this->_data['conversation_id'], $user_id));		
				} else {
					continue;	
				}	
			} else {
				$conn->execute(sprintf("INSERT INTO conversations_users (conversation_id, user_id ) VALUES (%s, %s)", $this->_data['conversation_id'], $user_id));
			}
        	
        	//array_push($response, $db->insert_id);

        	$data = array(
        		"group_name" => $this->_data['group_name'],
        		"group_id" => $this->_data['group_id'],
        		"conversation_id" => $this->_data['conversation_id'],
				"actor_id" => $added_user_id,
				"actee_id" => $user_id,
				"group_photo" => ($this->_data['group_photo'] ? $codes->UPLOADS_DIRECTORY . '/' . $this->_data['group_photo'] : ''),
        	);			
			
            $userData = $this->get_user_by_id($user_id);
            $userData = $userData[0];
        	/*$message = "xyz user was added to the group by abc" . $this->_data['group_name']; by abc*/        	
			$message = $userData['user_fullname']." was added to the group by ".$addedBy['user_fullname'];						
			/* Create a System Message start */						
			$conn->execute(sprintf("INSERT INTO conversations_messages (conversation_id, user_id, type, actor_id, actee_id, message, time) VALUES (%s, '0', 'system_message','%s', '%s', '%s', '%s')", $this->_data['conversation_id'], $added_user_id, $user_id, $message, $date));

            $sql="SELECT LAST_INSERT_ID()";
        
            $stmt = $conn->execute($sql);
            
            $res = $stmt->fetch();
			$message_id = $res[0];
					
			/* update the conversation with last message id */
			$conn->execute(sprintf("UPDATE conversations SET last_message_id = %s WHERE conversation_id = %s", $message_id, $this->_data['conversation_id']));
			
			/* update sender (me) with last message id 
			$conn->execute(sprintf("UPDATE users SET user_live_messages_lastid = %s WHERE user_id = %s", secure($message_id, 'int'), secure($this->_data['user_id'], 'int'))) or _apiError('SQL_ERROR_THROWEN');*/
			
        	$type = "group_member_add";			
			$this->sendPushToAllReceipents($message,$type, $data);			
        }
		return true;
        if ( !in_array(0, $response) ) {
        	return true;
        } else {
        	return false;
        }
        
    }

    /**
     * leave_group
     *
     * @param integer $user_id
     * @return boolean
     */
    public function leave_group( $user_id )
    {
        global $db,$date,$system;
        $codes = new Codes;
        $conn = ConnectionManager::get('default');

        $rs = $conn->execute(sprintf("UPDATE conversations_users SET group_status='1' WHERE conversation_id = %s AND user_id = %s", $this->_data['conversation_id'], $user_id));
        
        $response = $rs->rowCount();
		
		$data = array(
			"group_name" => $this->_data['group_name'],
			"group_id" => $this->_data['group_id'],
			"conversation_id" => $this->_data['conversation_id'],
			"actor_id" => $user_id,
			"actee_id" => 0,
			"group_photo" => ($this->_data['group_photo'] ? $codes->UPLOADS_DIRECTORY . '/' . $this->_data['group_photo'] : ''),
		);									
        $userData = $this->get_user_by_id($user_id);
        $userData = $userData[0];        	
		/*$message = "You are now the admin of " . $this->_data['group_name'];*/            
		$message = $userData['user_fullname']." has left";			
		
		/* Create a System Message start */						
		$conn->execute(sprintf("INSERT INTO conversations_messages (conversation_id, user_id,type,actor_id, message, time) VALUES (%s, '0','system_message', '%s', '%s','%s')", $this->_data['conversation_id'], $userData['user_id'], $message, $date));

            
        $sql="SELECT LAST_INSERT_ID()";
        $stmt = $conn->execute($sql);
        $res = $stmt->fetch();
        
        $message_id = $res[0];
		//$message_id = $db->insert_id;
					
		/* update the conversation with last message id */
		$conn->execute(sprintf("UPDATE conversations SET last_message_id = %s WHERE conversation_id = %s", $message_id, $this->_data['conversation_id']));
		
		/* update sender (me) with last message id 
		$conn->execute(sprintf("UPDATE users SET user_live_messages_lastid = %s WHERE user_id = %s", secure($message_id, 'int'), secure($this->_data['user_id'], 'int'))) or _apiError('SQL_ERROR_THROWEN');*/
        $sql="SELECT LAST_INSERT_ID()";
        $stmt = $conn->execute($sql);
        $res = $stmt->fetch();
        
        $message_id = $res[0];
		//$message_id = $db->insert_id;			
		$type = "group_member_left";			
		$this->sendPushToAllReceipents($message,$type, $data);			

		return true;
                
    }		
	public function sendPushToAllReceipents( $message,$type,$data){
		$group = new ConversationGroup( $data['group_id'] );
        $members = $group->get_group_data();
		//$members = $this->get_group_data();		
		if($members['no_of_members'] > 0){			
			foreach($members['members'] as $member){				
				$this->_sendPushNotificationMessage($member['user_id'], $message, $type, $data);			
			}		
		}	
	}

    /**
     * add_admin
     *
     * @param integer $user_id
     * @return boolean
     */
    public function add_admin( $user_id , $member_id)
    {
        global $db,$date,$system;
        $codes = new Codes;
        $conn = ConnectionManager::get('default');

		if($user_id == $this->_data['group_owner'] || $this->is_valid_group_admin($user_id )) {
			$rs = $conn->execute(sprintf("UPDATE conversations_users SET user_type = 'admin' WHERE conversation_id = %s AND user_id = %s", $this->_data['conversation_id'], $member_id));
			
			$response = $rs->rowCount();
			
			$data = array(
				"group_name" => $this->_data['group_name'],
				"group_id" => $this->_data['group_id'],
				"conversation_id" => $this->_data['conversation_id'],
				"actor_id" => $user_id,
				"actee_id" => $member_id,
				"group_photo" => ($this->_data['group_photo'] ? $codes->UPLOADS_DIRECTORY . '/' . $this->_data['group_photo'] : ''),
			);									
            $userData = $this->get_user_by_id($user_id);
            if(!isset($userData[0])){
                return 'user_not_exsit';
            }
            $userData = $userData[0];        	
            $memberData = $this->get_user_by_id($member_id);    
            if(!isset($memberData[0])){
                return 'user_not_exsit';
            }    	
            $memberData = $memberData[0];
			/*$message = "You are now the admin of " . $this->_data['group_name'];*/            
			$message = $memberData['user_fullname']." was made Admin by ".$userData['user_fullname'];			
			
			/* Create a System Message start */						
			$conn->execute(sprintf("INSERT INTO conversations_messages (conversation_id, user_id,type,actor_id, actee_id, message, time) VALUES (%s, '0','system_message', '%s','%s', '%s','%s')", $this->_data['conversation_id'], $user_id, $member_id, $message, $date));

            $sql="SELECT LAST_INSERT_ID()";
            $stmt = $conn->execute($sql);
            $res = $stmt->fetch();
            
            $message_id = $res[0];
						
			/* update the conversation with last message id */
			$conn->execute(sprintf("UPDATE conversations SET last_message_id = %s WHERE conversation_id = %s", $message_id, $this->_data['conversation_id']));
			
			/* update sender (me) with last message id 
			$conn->execute(sprintf("UPDATE users SET user_live_messages_lastid = %s WHERE user_id = %s", secure($message_id, 'int'), secure($this->_data['user_id'], 'int'))) or _apiError('SQL_ERROR_THROWEN');*/
						
			$type = "add_admin";			
			$this->sendPushToAllReceipents($message,$type, $data);	

			if ( $response !== false ) {
				return true;
			} else {
				return false;
			}
		} else {
           return false;
        }  
                
    }

    /**
     * remove_admin
     *
     * @param integer $user_id
     * @return boolean
     */
    public function remove_admin( $user_id )
    {
        global $db;
        $conn = ConnectionManager::get('default');
        $codes = new Codes;
		
		if($user_id == $this->_data['group_owner'] || $this->is_valid_group_admin($user_id )) {
			$rs = $conn->execute(sprintf("UPDATE conversations_users SET user_type = 'member' WHERE conversation_id = %s AND user_id = %s", $this->_data['conversation_id'], $user_id));
			
			$response = $rs->rowCount();

			if ( $response !== false ) {
				return true;
			} else {
				return false;
			}
        } else {
            return false;
        }        
    }


    /**
     * remove_members
     *
     * @param integer $user_id
     * @return boolean
     */
    public function remove_members( $user_id, $users )
    {
        global $db,$date,$system;
        $conn = ConnectionManager::get('default');
        $codes = new Codes;

        if ( $user_id == $this->_data['group_owner'] || $this->is_valid_group_admin( $user_id ) ) {
            
            $responses = array();
            
            $removedBy = $this->get_user_by_id($user_id);
            if(!isset($removedBy[0])){
                return "user_not_exsit";
            }
          
            $removedBy = $removedBy[0];
            foreach ($users as $value) {
                 $conn->execute(sprintf("UPDATE conversations_users SET group_status='1' WHERE conversation_id = %s AND user_id = %s", $this->_data['conversation_id'], $value));
               /* $conn->execute(sprintf("DELETE FROM conversations_users WHERE conversation_id = %s AND user_id = %s", secure($this->_data['conversation_id'], 'int'), secure($value, 'int'))) or _apiError('SQL_ERROR_THROWEN');*/

				$data = array(
					"group_name" => $this->_data['group_name'],
					"group_id" => $this->_data['group_id'],
					"conversation_id" => $this->_data['conversation_id'],
					"actor_id" => $user_id,
					"actee_id" => $value,
					"group_photo" => ($this->_data['group_photo'] ? $codes->UPLOADS_DIRECTORY . '/' . $this->_data['group_photo'] : ''),
				);
													
                $userData = $this->get_user_by_id($value);
  
                if(!isset($userData[0])){
                    return "user_not_exsit";
                }
                $userData = $userData[0];					
				/*$message = "You are now the admin of " . $this->_data['group_name'];*/					
				$message = $userData['user_fullname']." was removed by ".$removedBy['user_fullname'];					
				/* Create a System Message start */										
				$conn->execute(sprintf("INSERT INTO conversations_messages (conversation_id, user_id,type,actor_id,actee_id, message, time) VALUES (%s, '0','system_message','%s','%s', '%s', '%s')", $this->_data['conversation_id'], $user_id, $userData['user_id'], $message, $date));										
				
                $sql="SELECT LAST_INSERT_ID()";
                $stmt = $conn->execute($sql);
                $res = $stmt->fetch();
                
             
                $message_id = $res[0];
					
				/* update the conversation with last message id */
				$conn->execute(sprintf("UPDATE conversations SET last_message_id = %s WHERE conversation_id = %s", $message_id, $this->_data['conversation_id']));
				
				/* update sender (me) with last message id 
				$conn->execute(sprintf("UPDATE users SET user_live_messages_lastid = %s WHERE user_id = %s", secure($message_id, 'int'), secure($this->_data['user_id'], 'int'))) or _apiError('SQL_ERROR_THROWEN');*/										
				$type = "group_member_remove";					
				
				$this->sendPushToAllReceipents($message,$type, $data);

				//_sendPushNotificationMessage($value, $message, $type, $data);
            }
            return true;

        } else {
            return false;
        }     
    }

    function _sendPushNotificationMessage($to_user_id,$message,$type,$data=array()){
        global $db, $system;
        $codes = new Codes;
        $conn = ConnectionManager::get('default');
        
        // require_once(ABSPATH.'includes/Firebase.php');
        // require_once(ABSPATH.'includes/Push.php');
        
        /* Send Push Notification*/
        $devices = $conn->execute(sprintf("SELECT * FROM user_devices WHERE device_type='A' AND user_id = %s", $to_user_id));
        //pr($devices);die;
        if ($devices->num_rows > 0) {
            $device_tokens = array();
            while ($row = $devices->fetch("assoc")) {
                $device_tokens[] = $row['device_token'];
            }
            $title = "New notification from" . " " . $system['system_title'];
            $mPushNotification['data']['title'] = $title;
            $mPushNotification['data']['message'] = $message;
            $mPushNotification['data']['image'] = '';
            $mPushNotification['data']['type'] = $type;
    
            if ( isset($data['last_message_id']) && !empty($data['last_message_id']) ) {
                $mPushNotification['data']['last_message_id'] = $data['last_message_id'];
            }
            if ( isset($data['conversation_id']) && !empty($data['conversation_id']) ) {
                $mPushNotification['data']['conversation_id'] = $data['conversation_id'];
            }
            if ( isset($data['user_id']) && !empty($data['user_id']) ) {
                $mPushNotification['data']['user_id'] = $data['user_id'];
            }
            if ( isset($data['owner_user_id']) && !empty($data['owner_user_id']) ) {
                $mPushNotification['data']['owner_user_id'] = $data['owner_user_id'];
            }
            if ( isset($data['group_name']) && !empty($data['group_name']) ) {
                $mPushNotification['data']['group_name'] = $data['group_name'];
            }
            if ( isset($data['group_id']) && !empty($data['group_id']) ) {
                $mPushNotification['data']['group_id'] = $data['group_id'];
            }
            
            if(in_array($type, array('group_name_changed','group_photo_changed','group_member_left','group_member_add','group_member_remove','add_admin'))){
                $mPushNotification['data']['title'] = $title;
                $mPushNotification['data']['message'] = $message;
                $mPushNotification['data']['image'] = $data['group_photo'];
                $mPushNotification['data']['type'] = $type;
                $mPushNotification['data']['conversation_id'] = $data['conversation_id'];
                $mPushNotification['data']['group_id'] = $data['group_id'];
                $mPushNotification['data']['group_name'] = $data['group_name'];
                $mPushNotification['data']['actor_id'] = $data['actor_id'];
                $mPushNotification['data']['actee_id'] = $data['actee_id'];
            }
    
            //$push = new Push($title, $message,null,$type);
            
            //getting the push from push object
            //$mPushNotification = $push->getPush();
            //pr($mPushNotification);die;
    
            //creating firebase class object 
            $firebase = new Firebase(); 
            
            //sending push notification and displaying result 
            $firebase->send($device_tokens, $mPushNotification);
        }
    }

    function get_user_by_id($user_id)
    {
        global $db;
        $users = array();
        $conn = ConnectionManager::get('default');
        $codes = new Codes;
        /* make a search list */
        /* get users */
        $get_users = $conn->execute(sprintf("SELECT user_id, user_email,user_name, user_fullname, user_gender, user_picture, user_work, user_work_title, user_work_place FROM users WHERE user_id IN (%s)", $user_id));
        if ($get_users->count() > 0) {
            while ($user = $get_users->fetch("assoc")) {
                $user['user_picture'] = $this->get_picture($user['user_picture'], $user['user_gender']);
                $users[] = $user;
            }
        }
        return $users;
    }

    function get_picture($picture, $type)
    {
        global $system;
        $conn = ConnectionManager::get('default');
        $codes = new Codes;
        $system['theme'] = 'default';
        if ($picture == "") {
            switch ($type) {
                case 'male':
                    $picture = $codes->SYSTEM_URL . '/content/themes/' . $system['theme'] . '/images/blank_profile_male.jpg';
                    break;

                case 'female':
                    $picture = $codes->SYSTEM_URL . '/content/themes/' . $system['theme'] . '/images/blank_profile_female.jpg';
                    break;

                case 'page':
                    $picture = $codes->SYSTEM_URL . '/content/themes/' . $system['theme'] . '/images/blank_page.png';
                    break;

                case 'group':
                    $picture = $codes->SYSTEM_URL . '/content/themes/' . $system['theme'] . '/images/blank_group.png';
                    break;

                case 'game':
                    $picture = $codes->SYSTEM_URL . '/content/themes/' . $system['theme'] . '/images/blank_game.png';
                    break;

                case 'package':
                    $picture = $codes->SYSTEM_URL . '/content/themes/' . $system['theme'] . '/images/blank_package.png';
                    break;
            }
        } else {
            $picture = $codes->SYSTEM_URL .'/'. $codes->UPLOADS_DIRECTORY . '/' . $picture;
        }
        return $picture;
    }


    /**
     * update_conversation_group
     *
     * @param integer $group_id
     * @param integer $owner_id
     * @param string $update
     * @return boolean
     */
    public function update_conversation_group($member_id, $update = null)
    {
         global $db,$system;
         $conn = ConnectionManager::get('default');
         $codes = new Codes;
         $dateUtils = new DateUtils;
         $date = $dateUtils->getCurrentDate();

        $verify_member = $this->is_valid_group_member( $member_id );

        if ( $verify_member ) {

            $response = array();
           

            if ( array_key_exists("group_name", $update) ) {
                				
                $group_name = $update['group_name'];
                $photo      = $update['photo'] ? $update['photo'] : $this->_data['group_photo'];
               // print_r($update);exit;
                $rows = $conn->execute(sprintf("UPDATE conversation_groups SET group_name = '%s', group_photo = '%s' WHERE group_id = %s", $group_name, $photo, $this->_data['group_id']));   
                $response1 = $rows->rowCount();

                if ( $response1 !== false ) {	
                    									
					$userData = $this->get_user_by_id($member_id)[0];					
					/*$message = "You are now the admin of " . $this->_data['group_name'];*/					
					
					$message = $userData['user_fullname']." has changed the group name from ".$this->_data['group_name']." to $group_name ";					
					/* Create a System Message start */										
					$conn->execute(sprintf("INSERT INTO conversations_messages (conversation_id, user_id,type,actor_id, message, time) VALUES (%s, '0','system_message', '%s', '%s', '%s')", $this->_data['conversation_id'], $member_id, $message, $date));

                    $sql="SELECT LAST_INSERT_ID()";
                    $stmt = $conn->execute($sql);
                    $res = $stmt->fetch();
                    
                 
			        $message_id = $res[0];
					
					/* update the conversation with last message id */
					$conn->execute(sprintf("UPDATE conversations SET last_message_id = %s WHERE conversation_id = %s", $message_id, $this->_data['conversation_id']));
					
					/* update sender (me) with last message id 
					$conn->execute(sprintf("UPDATE users SET user_live_messages_lastid = %s WHERE user_id = %s", secure($message_id, 'int'), secure($this->_data['user_id'], 'int'))) or _apiError('SQL_ERROR_THROWEN');*/
					
                    $sql="SELECT LAST_INSERT_ID()";
                    $stmt = $conn->execute($sql);
                    $res = $stmt->fetch();
                    
                 
                    $message_id = $res[0];
                    									
					$type = "group_name_changed";					
					$data = array(                        
						"group_name" => $this->_data['group_name'],                        
						"group_id" => $this->_data['group_id'],                        
						"conversation_id" => $this->_data['conversation_id'],
						"actor_id" => $member_id,
						"actee_id" => 0,
						"group_photo" => ($photo ? $codes->UPLOADS_DIRECTORY . '/' . $photo : ''),						
					);					
					$this->sendPushToAllReceipents($message,$type, $data);
                    $response[] = true;
                }
            } 
            
            if ( array_key_exists("photo", $update) ) {
                
                $photo      = $update['photo'];
                $row = $conn->execute(sprintf("UPDATE conversation_groups SET group_photo = '%s' WHERE group_id = %s", $photo, $this->_data['group_id']));
                $response2 = $row->rowCount();

                if ( $response2 !== false ) {					
					$userData = $this->get_user_by_id($member_id)[0];					
					$message = $userData['user_fullname']." has changed the group photo";					
					/* Create a System Message start */										
					$conn->execute(sprintf("INSERT INTO conversations_messages (conversation_id, user_id,type,actor_id, message, time) VALUES (%s, '0','system_message', '%s', '%s', '%s')", $this->_data['conversation_id'], $member_id, $message, $date));
										
                    $sql="SELECT LAST_INSERT_ID()";
                    $stmt = $conn->execute($sql);
                    $res = $stmt->fetch();
                    
                 
			        $message_id = $res[0];
					
					/* update the conversation with last message id */
					$conn->execute(sprintf("UPDATE conversations SET last_message_id = %s WHERE conversation_id = %s", $message_id, $this->_data['conversation_id']));
					
					/* update sender (me) with last message id 
					$conn->execute(sprintf("UPDATE users SET user_live_messages_lastid = %s WHERE user_id = %s", secure($message_id, 'int'), secure($this->_data['user_id'], 'int'))) or _apiError('SQL_ERROR_THROWEN');*/										
					$type = "group_photo_changed";					
					$data = array(                       
						"group_name" => $this->_data['group_name'],                       
						"group_id" => $this->_data['group_id'],                       
						"conversation_id" => $this->_data['conversation_id'],
						"actor_id" => $member_id,
						"actee_id" => 0,
						"group_photo" => ($photo ? $codes->UPLOADS_DIRECTORY . '/' . $photo : ''),								
					);					
					$this->sendPushToAllReceipents($message,$type, $data);
                    $response[] = true;
                }
            }

            if ( in_array(0, $response) ) {
                return false;
            } else {
                return true;
            }

        } else {
            throw new Exception("You are not authorized to do this");
        }
    }

    /**
     * is_valid_group_member
     *
     * @param integer $member_id
     * @return boolean
     */
    private function is_valid_group_member( $member_id ) {

        global $db;
        $conn = ConnectionManager::get('default');
        $codes = new Codes;

        $get_conversation_id = $conn->execute(sprintf("SELECT conversation_id FROM conversation_groups WHERE group_id = %s", $this->_data['group_id']));

        $get_conversation_id = $get_conversation_id->fetch("assoc");

        if ( isset($get_conversation_id['conversation_id']) ) {
            
            $validate_member = $get_conversation_id = $conn->execute(sprintf("SELECT conversation_user_id FROM conversations_users WHERE conversation_id = %s AND user_id = %s", $get_conversation_id['conversation_id'], $member_id));

            if ( $validate_member->count() > 0 ) {
                return true;
            } else {
                return false;
            }

        } else {
            throw new Exception("Invalid Group");
        }

    }

    /**
     * is_valid_group_admin
     *
     * @param integer $member_id
     * @return boolean
     */
    private function is_valid_group_admin( $member_id ) {

        global $db;
        $conn = ConnectionManager::get('default');
        $codes = new Codes;
        
        $get_conversation_id = $conn->execute(sprintf("SELECT conversation_id FROM conversation_groups WHERE group_id = %s", $this->_data['group_id']));

        $get_conversation_id = $get_conversation_id->fetch("assoc");

        if ( isset($get_conversation_id['conversation_id']) ) {
            
            $validate_admin = $get_conversation_id = $conn->execute(sprintf("SELECT conversation_user_id FROM conversations_users WHERE conversation_id = %s AND user_id = %s AND user_type = 'admin'" , $get_conversation_id['conversation_id'], $member_id));

            if ( $validate_admin->count() > 0 ) {
                return true;
            } else {
                return false;
            }

        } else {
            throw new Exception("Invalid Group");
        }

    }
}

?>