<?php 
namespace App\Model;


class Firebase {
	
	private $firebase_api_key = 'AAAAxXlpSyU:APA91bHw05KuuStxOjPpm7zKW0rh-HwZqMTn_7c2vPZKOw_EK3TN4ABUtjShO9C7YAo-1zedUXKkhTwMRlRiFn75DO1diLDCyifAGxUzbqh_pg7qPbSafLoDME0k3wTxx8-nwmm8deaI';
 
    public function send($registration_ids, $message) {

        $fields = array(
            'registration_ids' => $registration_ids,
            'data' => $message,
        );

        return $this->sendPushNotification($fields);
    }
    
    /*
    * This function will make the actuall curl request to firebase server
    * and then the message is sent 
    */
    private function sendPushNotification($fields) {
        
        //importing the constant files
        //require_once 'Config.php';
        
        //firebase server url to send the curl request
        $url = 'https://fcm.googleapis.com/fcm/send';
 
        //building headers for the request
        $headers = array(
            'Authorization: key=' . $this->firebase_api_key,
            'Content-Type: application/json'
        );
 
        //Initializing curl to open a connection
        $ch = curl_init();

        //Setting the curl url
        curl_setopt($ch, CURLOPT_URL, $url);
        
        //setting the method as post
        curl_setopt($ch, CURLOPT_POST, true);
 
        //adding headers 
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
 
        //disabling ssl support
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        
        //adding the fields in json format 
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields) );

        //finally executing the curl request 
        $result = curl_exec($ch);

        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
 
        //Now close the connection
        curl_close($ch);
 
        //and return the result 
        return $result;
    }
}

?>