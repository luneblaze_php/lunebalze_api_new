<?php

namespace App\DaoLayer;

use App\Utils\Codes;
use Cake\Core\Exception\Exception;
use Cake\Datasource\ConnectionManager;
use Cake\Log\Log;

class SessionsThanksDao
{

	/**
	 * Get Sessions Thanks by User Id and Session Id
	 */
	public function getSessionsThanksByUserIdAndSessionId($userId, $sessionId)
	{
		Log::debug("Started ...getSessionsThanksByUserIdAndSessionId Dao : User Id : ".$userId.", Session Id : ".$sessionId);

		try{
			
			$conn = ConnectionManager::get('default');

			$sql=sprintf("SELECT * FROM sessions_thanks WHERE user_id = %s AND session_id = %s", $userId, $sessionId);
			
			Log::debug("SQL : ".$sql);

			$stmt = $conn->execute($sql);

			$result = $stmt->fetch("assoc");

			Log::debug("Ended ...getSessionsThanksByUserIdAndSessionId Dao");

			return $result;

		}catch(\Exception $e){
			Log::debug($e);
			throw new Exception($e);
		}
	}   

	/**
	 * Save Sessions Thanks
	 */
	public function saveSessionsThanks($userId, $sessionsId)
	{
		Log::debug("Started ...saveSessionsThanks Dao : User Id : ".$userId.", sessions Id : ".$sessionsId);

		try{
			
			$conn = ConnectionManager::get('default');

			$sql=sprintf("INSERT INTO sessions_thanks (user_id, session_id) VALUES (%s, %s)", $userId, $sessionsId);

			Log::debug("SQL : ".$sql);

			$conn->execute($sql);

			Log::debug("Ended ...saveSessionsThanks Dao");

		}catch(\Exception $e){
			Log::debug($e);
			throw new Exception($e);
		}
	}     

}