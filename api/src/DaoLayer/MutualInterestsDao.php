<?php 

//require_once('Hungarian.php');
namespace App\DaoLayer;

use App\Utils\Codes;
use Cake\Core\Exception\Exception;
use App\Utils\DateUtils;
use Cake\Datasource\ConnectionManager;
use Cake\Log\Log;
use App\DaoLayer\Hungarian;


class MutualInterestsDao {


    public $_data = array();


    public function __construct($user_id = null) {
        //Create the db connection here
        //global $db;

        //Please create the database connection here
        //global $db = new ConnectionManager::get('default'); 

        $this->data = null;
    }


    /**
     * @param $costMatrix - The cost matrix 
     * @return $result - The optimised result
     */
    private function hungarian($costMatrix)
    {
            $hungarian = new Hungarian($costMatrix);

            $result = $hungarian->solve(false);
        
            return $result;
    }

    /**
     * 
     * @param $userId1 - 1st user
     * @param $userId2 - 2nd user
     * @param $relative_constant - Relative Constant
     * @return $mutualInterestPercentage between two users
     */
    public function get_mutual_interests($userId1, $userId2, $relative_constant)
    {
        global $db;

        $userIdInterest1 = array();
        $userIdInterest2 = array();
        $interestArray = array();
        $db = ConnectionManager::get('default');
        $arr1 = array();
        $arr2 = array();
        //Fetch all the user interest of 1st user
        $userId1InterestQuery=$db->execute(sprintf("SELECT user_interest.interest FROM user_interest WHERE user_interest.user_id = %s AND user_interest.status = 1",$userId1));

        while ($int = $userId1InterestQuery->fetch('assoc')) {
            array_push($userIdInterest1,$int);
            array_push($arr1 , $int['interest']);
        }

        //rhythm check
        if($userIdInterest1 == []){return 0;}
        //Fetch all the user interest of 2nd user
        $userId2InterestQuery=$db->execute(sprintf("SELECT user_interest.interest FROM user_interest WHERE user_interest.user_id = %s AND user_interest.status = 1",$userId2));

        while ($int = $userId2InterestQuery->fetch("assoc")) {
            array_push($userIdInterest2,$int);
            array_push($arr2 , $int['interest']);
        }
        //rhythm check
        if($userIdInterest2 == []){return 0;}
        sort($arr1);
        sort($arr2);
        if($arr1 == $arr2){
            return 100;
        }
        //Fetch all the interest from the interest master table
        $userMastQuery = $db->execute(sprintf("SELECT interest_id, parent_id FROM interest_mst WHERE  interest_mst.status = 1"));
        while ($int = $userMastQuery->fetch('assoc')) {
            array_push($interestArray,$int);
        }
        //$interestArray = $map;
        //return $userId1;
        //$var = $this->get_mutual_percentage_between_two_interest(1,2,8,$map);
        //echo("<br>".$var."<br>");
        //$userIdInterest1 = explode(",", "5,8");
        //$userIdInterest2 = explode(",", "7,6");
        // echo json_encode($userIdInterest1,JSON_PRETTY_PRINT);
        // echo json_encode($userIdInterest2,JSON_PRETTY_PRINT);
        //$userIdInterest1 = array(["interest" => 1] , ["interest" => 2]);
        //$userIdInterest2 = array(["interest" => 1] , ["interest" => 2] , ["interest" => 3] , ["interest" => 4] ,["interest" => 5]);

        //Generate the cost matrix using the two arrays
        $costMatrix = $this->generate_cost_matrix($userIdInterest1,$userIdInterest2,$relative_constant,$interestArray);
        //return $costMatrix;
        //rhythm change    
        //while($costMatrix == null);
        if($costMatrix == null){return 0;}



        $result = $this->hungarian($costMatrix);
        //echo json_encode($result);
        $mutualInterestPercentage = 0.00;
        $i=0;
        foreach ($result as $key => $value) {
            $mutualInterestPercentage += (double) ($costMatrix[$i][$key] / 100) ;
            $i++;
        }
        $mutualInterestPercentage = $mutualInterestPercentage / count($result);
        
        return $mutualInterestPercentage * 100;

    }
    public function get_mutual_interests_csvs($userId1, $userId2, $relative_constant)
    {
        global $db;
        $userIdInterest1 = array();
        $userIdInterest2 = array();
        $interestArray = array();
        $db = ConnectionManager::get('default');
        $arr1 = array();
        $arr2 = array();
        //Fetch all the user interest of 1st user
        $temp = explode(",", $userId1);
        foreach ($temp as $t) {
            array_push($userIdInterest1 , ["interest" => $t]);
            array_push($arr1 , $t);
        }
        if($userIdInterest1 == []){return 0;}

        //Fetch all the user interest of 2nd user
        $temp = explode(",", $userId2);
        foreach ($temp as $t) {
            array_push($userIdInterest2 , ["interest" => $t]);
            array_push($arr2 , $t);
        }
        if($userIdInterest2 == []){return 0;}
        sort($arr1);
        sort($arr2);
        if($arr1 == $arr2){
            return 100;
        }
        //Fetch all the interest from the interest master table
        $userMastQuery = $db->execute(sprintf("SELECT interest_id, parent_id FROM interest_mst WHERE  interest_mst.status = 1"));
        while ($int = $userMastQuery->fetch('assoc')) {
            array_push($interestArray,$int);
        }
        

        //Generate the cost matrix using the two arrays
        $costMatrix = $this->generate_cost_matrix($userIdInterest1,$userIdInterest2,$relative_constant,$interestArray);

        //rhythm change    
        if($costMatrix == null){return 0;}



        $result = $this->hungarian($costMatrix);
        //echo json_encode($result);
        $mutualInterestPercentage = 0.00;
        $i=0;
        foreach ($result as $key => $value) {
            $mutualInterestPercentage += (double) ($costMatrix[$i][$key] / 100) ;
            $i++;
        }
        $mutualInterestPercentage = $mutualInterestPercentage / count($result);
        
        return $mutualInterestPercentage * 100;

    }
    public function get_mutual_interests_idncsv($userId1, $csv, $relative_constant)
    {
        global $db;
        $userIdInterest1 = array();
        $userIdInterest2 = array();
        $interestArray = array();
        $db = ConnectionManager::get('default');
        $arr1 = array();
        $arr2 = array();
        //Fetch all the user interest of 1st user
        $userId1InterestQuery=$db->execute(sprintf("SELECT user_interest.interest FROM user_interest WHERE user_interest.user_id = %s AND user_interest.status = 1",$userId1));

        while ($int = $userId1InterestQuery->fetch('assoc')) {
            array_push($userIdInterest1,$int);
            array_push($arr1 , $int['interest']);
        }
        if($userIdInterest1 == []){return 0;}
        //return $userIdInterest1;
        //Fetch all the user interest of 2nd user
        $temp = explode(",", $csv);
        foreach ($temp as $t) {
            array_push($userIdInterest2 , ["interest" => $t]);
            array_push($arr2 , $t);
        }
        //rhythm check
        if($userIdInterest2 == []){return 0;}
        //return $userIdInterest2;
        sort($arr1);
        sort($arr2);
        if($arr1 == $arr2){
            return 100;
        }



        //Fetch all the interest from the interest master table
        $userMastQuery = $db->execute(sprintf("SELECT interest_id, parent_id FROM interest_mst WHERE  interest_mst.status = 1"));
        while ($int = $userMastQuery->fetch('assoc')) {
            array_push($interestArray,$int);
        }
        

        //Generate the cost matrix using the two arrays
        $costMatrix = $this->generate_cost_matrix($userIdInterest1,$userIdInterest2,$relative_constant,$interestArray);
        if($costMatrix == null){return 0;}



        $result = $this->hungarian($costMatrix);
        $mutualInterestPercentage = 0.00;
        $i=0;
        foreach ($result as $key => $value) {
            $mutualInterestPercentage += (double) ($costMatrix[$i][$key] / 100) ;
            $i++;
        }
        $mutualInterestPercentage = $mutualInterestPercentage / count($result);
        
        return $mutualInterestPercentage * 100;

    }

    public $size1 = 0;
    public $size2 = 0;
    public $size = 0;
    /**
     * Generate the cost matrix
     * @param $userId1IntArr - Interest list of 1st user 
     * @param $userId2IntArr - Interest list of 2nd user 
     * @param $relative_constant - Relative constant
     * @return $cost matrix
     */
    private function generate_cost_matrix($userId1IntArr, $userId2IntArr,$relative_constant, $intArr)
    {
        //$size =  count($userId1IntArr) > count($userId2IntArr) ? count($userId1IntArr) : count($userId2IntArr);
        //$sizem =  count($userId1IntArr) < count($userId2IntArr) ? count($userId1IntArr) : count($userId2IntArr);
        $this->size1 = count($userId1IntArr);
        $this->size2 = count($userId2IntArr);
        //echo($this->size1."size of matrix<br>");
        //echo($this->size2."size of matrix<br>");
        //$this->size = $sizem;
        $push = ["interest" => 0];
        if($this->size1 < $this->size2){
            $temp = $userId1IntArr;
            $userId1IntArr = $userId2IntArr;
            $userId2IntArr = $temp;
        }
        $size =  count($userId1IntArr) > count($userId2IntArr) ? count($userId1IntArr) : count($userId2IntArr);
        $sizem =  count($userId1IntArr) < count($userId2IntArr) ? count($userId1IntArr) : count($userId2IntArr);
        $this->size1 = count($userId1IntArr);
        $this->size2 = count($userId2IntArr);
        $this->size = $sizem;
        /*$flag = 0;
        while( $size > count($userId2IntArr))
        {
            $flag = 2;
            array_push($userId2IntArr, $push);
        }
        while($size > count($userId1IntArr))
        {
            $flag = 1;
            array_push($userId1IntArr, $push);
        }*/


        $noOfRows = count($userId1IntArr);
        $noOfColumns = count($userId2IntArr);
        //echo($size."size of matrix<br>");
        if($noOfRows == 0 || $noOfColumns == 0)
            return null;

        $costMatrix = array();
        $costMatrix1  = array();

        for($i=0;$i<count($userId1IntArr);$i++)
        {
            for($j=0;$j<count($userId2IntArr);$j++)
            {
            
                $cost = $this->get_mutual_percentage_between_two_interest($userId1IntArr[$i]['interest'], 
                $userId2IntArr[$j]['interest'],$relative_constant,$intArr);
                //echo($userId1IntArr[$i]['interest'] ." - ".$userId2IntArr[$j]['interest']." : ".($cost * 100)."<br>");
                //$costMatrix[$i][$j] = (int) ($cost * 100) * -1;
                $costMatrix[$i][$j] = (int) ($cost * 100) ;
                $costMatrix1[$i][$j] = (int) ($cost * 100) ;
            }
        }

        // Remove extra rows/columns
        $finalCostMatrix = $costMatrix;

        /*$size =  count($userId1IntArr) > count($userId2IntArr) ? count($userId1IntArr) : count($userId2IntArr);
        $size1 = count($userId1IntArr);
        $size2 = count($userId2IntArr);
        while( $size > count($userId2IntArr))
        {
            array_push($userId2IntArr, 0);
        }
        while($size > count($userId1IntArr))
        {
            array_push($userId1IntArr, 0);
        }

        return $this->get_square_matrix1($finalCostMatrix,$size , $size);*/
        return $this->get_square_matrix($finalCostMatrix,$size);
    }


    /**
     * Get the cost for each of the cell of the cost matrix
     * @param $int1 - 1st Interest
     * @param $int2 - 2nd Interest
     * @param $relConstant - Relative constant
     * @param $intArr - Master Interest tree
     * @return $mutualInterest
     */
    private function get_mutual_percentage_between_two_interest($int1, $int2,$relConstant, $intArr)
    {   
        $dist = 0;

        $interestTrace1 = $this->get_interest_trace($int1,$intArr);
        $interestTrace2 = $this->get_interest_trace($int2,$intArr);


        $least_common_interest = $this->get_least_common_interest($interestTrace1,$interestTrace2);

        

        //$dist1 = $this->get_distance_to_interest($least_common_interest,$interestTrace1);
        //$dist2 = $this->get_distance_to_interest($least_common_interest,$interestTrace2);

        //echo($least_common_interest);echo(" dist <br>");
        $mutualInterest = pow((double) ($relConstant / 10.00),$least_common_interest) ; /* + (($dist1-$dist2) * ($relConstant / $dist1))*/

        return $mutualInterest;
    }


    /**
     * To get the least common interest
     * @param $interestTrace1 - Interest trace 1
     * @param $interestTrace2 - Interest trace 2
     * @return $common interest id
     */
    /*private function get_least_common_interest($interestTrace1, $interestTrace2)
    {
        for($i=0;$i<count($interestTrace1);$i++)
        {
            for($j=0;$j<count($interestTrace2);$j++)
            {
                if($interestTrace1[$i] == $interestTrace2[$j])
                {
                    echo("".$interestTrace1[$i]." \t : \t ".$interestTrace2[$j]."    intrest trace <br>");
                    return $interestTrace1[$i];
                    //echo("".$interestTrace1[$i]."<br>");
                }
                //echo("".$interestTrace1[$i]." \t : \t ".$interestTrace2[$j]."<br>");
            }
        }
    }*/
    private function get_least_common_interest($interestTrace1, $interestTrace2)
    {
        for($i=0;$i<count($interestTrace1);$i++)
        {
            for($j=0;$j<count($interestTrace2);$j++)
            {
                if($interestTrace1[$i] == $interestTrace2[$j])
                {
                    //echo("".$interestTrace1[$i]." \t : \t ".$interestTrace2[$j]."    intrest trace <br>");
                    return $i+$j;
                    //echo("".$interestTrace1[$i]."<br>");
                }
                //echo("".$interestTrace1[$i]." \t : \t ".$interestTrace2[$j]."<br>");
            }
        }
    }

    /**
     * To get the distance to the common interest
     * @param $interestTargetId - The target of the interest upto which the distance is to be calculated
     * @param $trace - The trace of the interest
     * @return $distance
     */
    private function get_distance_to_interest($interestTargetId, $trace)
    {
        $dist = 0;
        for($i=0;$i<count($trace);$i++)
        {
            if($trace[$i] == $interestTargetId)
                return $dist;
            $dist++;
        }
    }

    /**
     * Get the square matrix from the (0,0) position
     * @param $intArr - The array
     * @param $size - The size of the square
     * @return $square matrix
     */
    private function get_square_matrix($intArr, $size)
    {
        $finalArr = array();

        for($i=0;$i<$size;$i++)
        {
            for($j=0;$j<$size;$j++)
            {
                if($j == 0){
                    $max = $intArr[$i][$j];
                }elseif(isset($intArr[$i][$j]) && $intArr[$i][$j]  > $max){
                    $max = $intArr[$i][$j];
                }
                $finalArr[$i][$j] = isset($intArr[$i][$j])?$intArr[$i][$j] : $max;
                //echo("".$finalArr[$i][$j]." \t");
            }
            //echo("<br>");
        }

        return $finalArr;
    }
    private function get_square_matrix1($intArr, $size1 , $size2)
    {
        $finalArr = array();

        for($i=0;$i<$size1;$i++)
        {
            for($j=0;$j<$size2;$j++)
            {
                $finalArr[$i][$j] = $intArr[$i][$j];
                //echo("".$finalArr[$i][$j]." \t");
            }
            //echo("<br>");
        }

        return $finalArr;
    }

    /**
     * The trace of the interest
     * @param $intArr - The interest master
     * @param $interestId - The interest id whose trace is to be generated
     * @return $trace of the array
     */
    private function get_interest_trace($interestId, $intArr)
    {
        $trace = array();
        $intPointer = $interestId;
        while($intPointer > 0)
        {
            array_push($trace,$intPointer);
            $str = "#".$intPointer."";
            //$intPointer = $intArr[$str];
            $intPointer = $this->get_parent_interest_id($intPointer,$intArr);
        }
        array_push($trace , 0);
        return $trace;
    }

    /**
     * Get Parent interest id from the interest array tree
     * @param $intId - Interest Id whose Parent interest id is to be found
     * @param $intArr - The array tree of the interest master
     * @return $parentInterestId - The parent of the interest id/ null if the interest id is not present in the intArr array
     */
    private function get_parent_interest_id($intId,$intArr)
    {
        for($i=0;$i<count($intArr);$i++)
        {
            if($intArr[$i]['interest_id'] == $intId)
                return $intArr[$i]['parent_id'];
        }
        return 0;
    }



}