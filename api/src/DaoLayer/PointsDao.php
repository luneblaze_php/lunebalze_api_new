<?php

namespace App\DaoLayer;

use App\Utils\Codes;
use App\Utils\DateUtils;
use Cake\Core\Exception\Exception;
use Cake\Datasource\ConnectionManager;
use Cake\Log\Log;

class PointsDao
{
	/*@points = -4 when subtracting a reward
	@points = badge id in case of other badge add
	@points */
	public function addPoints($type  ,$userid , $points = 1 ){
		try {
			$conn = ConnectionManager::get('default');
			$ret['status'] = 0;
			$ret['msg'] = "Success";
			$dat = new DateUtils;
			$codes = new Codes;
			$inipts = $codes->INITIAL_PTS;
			$date = "".$dat->getCurrentDate();
			$dates = substr($date , 0 , 10);
			$sql = $conn->execute("SELECT * FROM `users_points` WHERE user_id = $userid");
			if($sql->count() == 0){
				$conn->execute("insert into `users_points` SET user_id = $userid , total = $inipts");
			}

			if($points < 0){//if negating points weekly left untouched
				$cond = "";
			}else{
				$cond = ", weekly = weekly + $points";
			}

			switch ($type) {
				case 'total':
					$sql = sprintf("UPDATE users_points set total = total + $points WHERE user_id = $userid");
					if(!$conn->execute($sql)){$ret['msg'] = "Something went wrong"; return $ret;}
					break;
				case 'quizzes':
					$sql = sprintf("UPDATE users_points set quizzes = quizzes + $points , total = total + $points $cond WHERE user_id = $userid");
					if(!$conn->execute($sql)){$ret['msg'] = "Something went wrong"; return $ret;}
					break;
				case 'debates':
					$sql = sprintf("UPDATE users_points set debates = debates + $points , total = total + $points $cond WHERE user_id = $userid");
					if(!$conn->execute($sql)){$ret['msg'] = "Something went wrong"; return $ret;}
					break;
				case 'articles':
					$sql = sprintf("UPDATE users_points set articles = articles + $points , total = total + $points $cond WHERE user_id = $userid");
					if(!$conn->execute($sql)){$ret['msg'] = "Something went wrong"; return $ret;}
					break;
				case 'quickbits':
					$sql = sprintf("UPDATE users_points set quickbits = quickbits + $points , total = total + $points $cond WHERE user_id = $userid");
					if(!$conn->execute($sql)){$ret['msg'] = "Something went wrong"; return $ret;}
					break;
				case 'polls':
					$sql = sprintf("UPDATE users_points set polls = polls + $points , total = total + $points $cond WHERE user_id = $userid");
					if(!$conn->execute($sql)){$ret['msg'] = "Something went wrong"; return $ret;}
					break;
				case 'sessions':
					$sql = sprintf("UPDATE users_points set sessions = sessions + $points , total = total + $points $cond WHERE user_id = $userid");
					if(!$conn->execute($sql)){$ret['msg'] = "Something went wrong"; return $ret;}
					break;
				case 'activities':
					$sql = sprintf("UPDATE users_points set activities = activities + $points , total = total + $points $cond WHERE user_id = $userid");
					if(!$conn->execute($sql)){$ret['msg'] = "Something went wrong"; return $ret;}
					break;
				case 'other_badge':
					$sql = $conn->execute(sprintf("SELECT other_badge from users_points WHERE user_id = $userid"));
					$ans = $sql->fetch('assoc');
					if($ans['other_badge'] == '' || $ans['other_badge'] == null){
						$fill = "$points";
					}else{
						$fill = $ans['other_badge'] . ",$points";
					}
					$sql = sprintf("UPDATE users_points set other_badge = $fill WHERE user_id = $userid");
					if(!$conn->execute($sql)){$ret['msg'] = "Something went wrong"; return $ret;}
					break;
				case 'moon':
					$sql = $conn->execute(sprintf("SELECT moon from users_points WHERE user_id = $userid"));
					$ans = $sql->fetch('assoc');
					if($points != -4 ){
						if($ans['moon'] == 3 ){
							$sql = sprintf("UPDATE users_points set moon = moon + 1 , moon_added = '$dates' WHERE user_id = $userid");
						}else{
							$sql = sprintf("UPDATE users_points set moon = moon + 1 WHERE user_id = $userid");
						}
					}else{
						if($ans['moon'] >= 4 ){
							$sql = sprintf("UPDATE users_points set moon = moon - 4 , moon_added = '0000-00-00' WHERE user_id = $userid");
						}else{
							$ret['msg'] = "Not enough points"; return $ret;
						}
					}
					if(!$conn->execute($sql)){$ret['msg'] = "Something went wrong"; return $ret;}
					break;
				case 'jupiter':
					$sql = $conn->execute(sprintf("SELECT jupiter from users_points WHERE user_id = $userid"));
					$ans = $sql->fetch('assoc');
					if($points != -4 ){
						if($ans['jupiter'] == 3 ){
							$sql = sprintf("UPDATE users_points set jupiter = jupiter + 1 , jupiter_added = '$dates' WHERE user_id = $userid");
						}else{
							$sql = sprintf("UPDATE users_points set jupiter = jupiter + 1 WHERE user_id = $userid");
						}
					}else{
						if($ans['jupiter'] >= 4 ){
							$sql = sprintf("UPDATE users_points set jupiter = jupiter - 4 , jupiter_added = '0000-00-00' WHERE user_id = $userid");
						}else{
							$ret['msg'] = "Not enough points"; return $ret;
						}
					}
					if(!$conn->execute($sql)){$ret['msg'] = "Something went wrong"; return $ret;}
					break;
				case 'saturn':
					$sql = $conn->execute(sprintf("SELECT saturn from users_points WHERE user_id = $userid"));
					$ans = $sql->fetch('assoc');
					if($points != -4 ){
						if($ans['saturn'] == 3 ){
							$sql = sprintf("UPDATE users_points set saturn = saturn + 1 , saturn_added = '$dates' WHERE user_id = $userid");
						}else{
							$sql = sprintf("UPDATE users_points set saturn = saturn + 1 WHERE user_id = $userid");
						}
					}else{
						if($ans['saturn'] >= 4 ){
							$sql = sprintf("UPDATE users_points set saturn = saturn - 4 , saturn_added = '0000-00-00' WHERE user_id = $userid");
						}else{
							$ret['msg'] = "Not enough points"; return $ret;
						}
					}
					if(!$conn->execute($sql)){$ret['msg'] = "Something went wrong"; return $ret;}
					break;
				case 'mars':
					$sql = $conn->execute(sprintf("SELECT mars from users_points WHERE user_id = $userid"));
					$ans = $sql->fetch('assoc');
					if($points != -4 ){
						if($ans['mars'] == 3 ){
							$sql = sprintf("UPDATE users_points set mars = mars + 1 , mars_added = '$dates' WHERE user_id = $userid");
						}else{
							$sql = sprintf("UPDATE users_points set mars = mars + 1 WHERE user_id = $userid");
						}
					}else{
						if($ans['mars'] >= 4 ){
							$sql = sprintf("UPDATE users_points set mars = mars - 4 , mars_added = '0000-00-00' WHERE user_id = $userid");
						}else{
							$ret['msg'] = "Not enough points"; return $ret;
						}
					}
					if(!$conn->execute($sql)){$ret['msg'] = "Something went wrong"; return $ret;}
					break;
				case 'pluto':
					$sql = $conn->execute(sprintf("SELECT pluto from users_points WHERE user_id = $userid"));
					$ans = $sql->fetch('assoc');
					if($points != -4 ){
						if($ans['pluto'] == 3 ){
							$sql = sprintf("UPDATE users_points set pluto = pluto + 1 , pluto_added = '$dates' WHERE user_id = $userid");
						}else{
							$sql = sprintf("UPDATE users_points set pluto = pluto + 1 WHERE user_id = $userid");
						}
					}else{
						if($ans['pluto'] >= 4 ){
							$sql = sprintf("UPDATE users_points set pluto = pluto - 4 , pluto_added = '0000-00-00' WHERE user_id = $userid");
						}else{
							$ret['msg'] = "Not enough points"; return $ret;
						}
					}
					if(!$conn->execute($sql)){$ret['msg'] = "Something went wrong"; return $ret;}
					break;
				default:
					$ret['msg'] = "invalid type"; return $ret;
					break;
			}
			$this->updatePointsBadge($userid);
			$ret['status'] = 1;
			return $ret;
		} catch (Exception $e) {
			Log::debug($e);
			throw new Exception($e);
		}
	}


	public function updatePointsBadge($userid){
		$conn = ConnectionManager::get('default');
		$sql = $conn->execute("select total from users_points where user_id = $userid");
		$res = $sql->fetch("assoc");
		$total = $res['total'];
		$sql = $conn->execute("SELECT * from badges where is_point_badge = 1 and upper_limit > $total order by upper_limit asc");
		if($res = $sql->fetch('assoc')){
			$badge = $res['id'];
		}else{
			$badge = 0;
		}
		$conn->execute("update users_points set points_badge = $badge where user_id = $userid");
		return 1;
	}

	public function checkAndDeduct($userId , $points){
        //$pod = new PointsDao;
        $codes = new Codes;
        $conn = ConnectionManager::get('default');
        $inipts = $codes->INITIAL_PTS;
        $sql = $conn->execute("SELECT * from users_points where user_id = $userId");
        if($res = $sql->fetch('assoc')){
            if($res['total'] >= $points){
                $this->addPoints('total',$userId,-$points);
                return 1;
            }else{
                return 0;
            }
        }else{
            $conn->execute("insert into `users_points` SET user_id = $userId , total = $inipts");
            $this->checkAndDeduct($userId , $points);
        }
        
    }


	public function getPointsAndBadgesByUserid($userid){
		try {
			$pd = new PostsDao;
			$conn = ConnectionManager::get('default');
			$sql = $conn->execute(sprintf("SELECT * from users_points WHERE user_id = $userid"));
			if($res = $sql->fetch('assoc')){
				$sqlm = $conn->execute(sprintf("select * from badges where id = %s", $res['points_badge']));
				$res['points_badge'] = $sqlm->fetch('assoc');
				if($res['other_badge'] != '' ){
					$temp = $res['other_badge'];
					$sqlm = $conn->execute(sprintf("select * from badges where FIND_IN_SET(id , '%s')", $temp));
					$res['other_badge'] = $sqlm->fetchAll('assoc');
				}else{$res['other_badge'] = [];}
				return $res;
			}
			return null;

		} catch (Exception $e) {
			Log::debug($e);
			throw new Exception($e);
		}
	}

	public function getBadges(){
		try {
			$pd = new PostsDao;
			$conn = ConnectionManager::get('default');
			$sql = $conn->execute(sprintf("SELECT * from badges WHERE 1"));
			if($res = $sql->fetch('assoc')){
				return $res;
			}
			return null;

		} catch (Exception $e) {
			Log::debug($e);
			throw new Exception($e);
		}
	}

	public function getRewards(){
		try {
			$pd = new PostsDao;
			$conn = ConnectionManager::get('default');
			$sql = $conn->execute(sprintf("SELECT * from rewards WHERE 1"));
			if($res = $sql->fetch('assoc')){
				return $res;
			}
			return null;

		} catch (Exception $e) {
			Log::debug($e);
			throw new Exception($e);
		}
	}

	public function getRandomRewardByUserId($userid){
		try {
			$pd = new PostsDao;
			$conn = ConnectionManager::get('default');
			$codes = new Codes;
			$arr = array();
			$sql = $conn->execute("SELECT * from rewards");
			while($res = $sql->fetch('assoc')){
				array_push($arr , $res['name']);
			}
			$sql = $conn->execute("SELECT * from users_points where user_id = $userid");
			$udata = $sql->fetch('assoc');
			$temp = array();
			foreach ($arr as $value) {
				if($udata[$value] == 7){
					array_push($temp , $value);
				}
			}
			$arr = array_diff($arr , $temp);
			$len = count($arr);
			$rand = rand(0,$len-1);
			$ans = $arr[$rand];
			$sql = $conn->execute("SELECT * from rewards WHERE name like '$ans'");
			if($res = $sql->fetch('assoc')){
				return $res;
			}
			return null;
		} catch (Exception $e) {
			Log::debug($e);
			throw new Exception($e);
		}
	}





}
