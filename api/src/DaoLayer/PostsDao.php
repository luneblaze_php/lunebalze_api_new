<?php

namespace App\DaoLayer;

use App\Utils\Codes;
use Cake\Core\Exception\Exception;
use App\Utils\DateUtils;
use Cake\Datasource\ConnectionManager;
use App\DaoLayer\MutualInterestsDao;
use App\DaoLayer\ArticlesDao;
use App\Model\UserModel;
use App\DaoLayer\QuickBitsDao;
use App\DaoLayer\DebatesDao;
use Cake\Log\Log;


class PostsDao
{

    /**
     * 
     */
    public function getPostDetailByPostIdPostType($postId,$postType)
    {
        Log::debug("Started ...getPostDetailByPostIdPostType Dao : Post Id : ".$postId.", Post Type : ".$postType);

        try{

            $conn = ConnectionManager::get('default');

            $sql = sprintf("SELECT posts.*, users.user_name,users.privacy_connection_request,users.user_work_title, 
            users.user_fullname, users.user_gender, users.user_picture, 
            users.user_picture_id, users.user_cover_id, users.user_verified, 
            users.user_subscribed, users.user_pinned_post, pages.*, 
            groups.group_name, groups.group_picture_id, 
            groups.group_cover_id, groups.group_title, 
            groups.group_admin, groups.group_pinned_post 
            FROM posts LEFT JOIN users ON posts.user_id = users.user_id 
            AND posts.user_type='%s' LEFT JOIN pages ON posts.user_id = pages.page_id 
            AND posts.user_type = 'page' LEFT JOIN groups ON posts.in_group = '1' 
            AND posts.group_id = groups.group_id WHERE NOT (users.user_name <=> NULL 
            AND pages.page_name <=> NULL) AND posts.post_id = '%s'",$postType,$postId);

            Log::debug("SQL : ".$sql);

            $stmt = $conn->execute($sql);

            $result = $stmt->fetch("assoc");

            Log::debug("Ended ...getPostDetailByPostIdPostType Dao");

            return $result;
            
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }

    }

    public function getPostDetailByPostId($postId)
    {
        Log::debug("Started ...getPostDetailByPostId Dao : Post Id : ".$postId);

        try{

            $conn = ConnectionManager::get('default');

            $sql = sprintf("SELECT comment_ids FROM posts WHERE post_id = %s", $postId);

            Log::debug("SQL : ".$sql);

            $stmt = $conn->execute($sql);

            $result = $stmt->fetch("assoc");

            Log::debug("Ended ...getPostDetailByPostId Dao");

            return $result;
            
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }

    }



    /**
     * 
     */
    public function getPostsByOrganizationIdUserTypePaymentFlag($organizationId, $userType, $paymentFlag, $offset)
    {
        Log::debug("Started ...getPostsByOrganizationIdUserTypePaymentFlag Dao : Organization Id : ".$organizationId.", User Type : ".$userType.", Payment Flag : ".$paymentFlag);

        try{

            $codes = new Codes;
            
            $conn = ConnectionManager::get('default');

            if(isset($paymentFlag))
                $sql=sprintf("SELECT * FROM `posts` WHERE `organisation_id`='%s' and user_type='%s' and payment=%s order by post_id desc limit %s,%s",$organizationId,$userType,$paymentFlag,$offset,$codes->MAX_RESULTS);          
            else 
                $sql=sprintf("SELECT * FROM `posts` WHERE `organisation_id`='%s' and user_type='%s' order by post_id desc limit %s,%s",$organizationId,$userType,$offset,$codes->MAX_RESULTS);          

            Log::debug("SQL : ".$sql);

            $stmt = $conn->execute($sql);

            $results = array();

            while($result = $stmt->fetch("assoc"))
                array_push($results,$result);

            Log::debug("Ended ...getPostsByOrganizationIdUserTypePaymentFlag Dao");

            return $results;
            
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }

    }


    
	/**
	 * Save Posts
	 */
	public function savePosts($userId, $articlesId, $date, $discussionPost, $article, $commentId)
	{
		Log::debug("Started ...savePosts Dao : User Id : ".$userId.", Total User Ids : ".$userId.", Origin Id : ".$articlesId.", User Type : user, Post Type : article_comment".", Time : ".$date. ", Privacy : public".", Text :".$discussionPost.", Post Type Value : ".$article.", Updated At : ".$date.", Comments : 1".", Comment Ids : ".$commentId.", Total : 1");

		try{
			
			$conn = ConnectionManager::get('default');

			$sql=sprintf("INSERT INTO posts (user_id, total_user_ids, origin_id, user_type, post_type, time, privacy, text, post_type_value,updated_at,comments,comment_ids,total) VALUES (%s, %s, %s, 'user', 'article_comment', %s, 'public', %s, %s,%s,1,%s,1)", $userId, $userId, $articlesId, $date, $discussionPost, $article,$date,$commentId);
			
			Log::debug("SQL : ".$sql);

			$conn->execute($sql);

			Log::debug("Ended ...savePosts Dao");

		}catch(\Exception $e){
			Log::debug($e);
			throw new Exception($e);
		}
	}
    
    /**
     * Save Posts from quiz
     */
    public function saveQuizPosts($quiz_id, $user_id, $post_title, $date)
    {
        Log::debug("Started ...savePosts Dao ");
        try{
            
            $conn = ConnectionManager::get('default');

            $sql=sprintf("INSERT INTO posts (user_id, post_title, quiz_id, post_type, privacy, text_tagged, origin_type, organisation_id, payment,total,updated_at, time) VALUES (%s, '%s', %s, 'quiz', 'public', '', '', 0, 0, 0, '%s', '%s')", $user_id, $post_title, $quiz_id, $date, $date);
            
            Log::debug("SQL : ".$sql);

            $conn->execute($sql);

            Log::debug("Ended ...savePosts Dao");

        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }

    public function updatePostCommentCounter($comment_ids, $post_id)
    {
        Log::debug("Started ...updatePostCommentCounter Dao");

        try{

            $conn = ConnectionManager::get('default');

            $conn->execute(sprintf("UPDATE posts SET comments = comments + 1, comment_ids = %s WHERE post_id = %s", $comment_ids, $post_id));

            Log::debug("Ended ...updatePostCommentCounter Dao");
            
            return 1;
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }

    public function updatePostPhotoCommentCounter($post_id)
    {
        Log::debug("Started ...updatePostPhotoCommentCounter Dao");

        try{

            $conn = ConnectionManager::get('default');

            $conn->execute(sprintf("UPDATE posts_photos SET comments = comments + 1 WHERE photo_id = %s", self::secure($post_id, 'int')));

            Log::debug("Ended ...updatePostPhotoCommentCounter Dao");
            
            return 1;
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }

    public function updatePostsTitleByQuizId($quiz_id, $post_title, $date)
    {
        Log::debug("Started ...updatePostsTitleByQuizId Dao");

        try{

            $conn = ConnectionManager::get('default');

            $sql=sprintf("UPDATE posts SET post_title = '%s', updated_at = '%s'
                WHERE quiz_id = '%s'", $post_title, $date, $quiz_id);

            Log::debug("SQL : ".$sql);

            $conn->execute($sql);
           
            Log::debug("Ended ...updatePostsTitleByQuizId Dao");
            
            return 1;
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }
    /**
     * Delete Quiz
     */
    public function deletePostsByQuizId($quiz_id)
    {
        Log::debug("Started ...deletePostsByQuizId Dao");

        try{

            $conn = ConnectionManager::get('default');

            //deleting questions
            $sql=sprintf("DELETE FROM `posts` WHERE quiz_id = '%s'", 
            $quiz_id);
            Log::debug("SQL : ".$sql);
            $conn->execute($sql);
           
            Log::debug("Ended ...deletePostsByQuizId Dao");
            
            return 1;
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }

        /**
     * Save Posts from debate
     */
    public function saveDebatePosts($debate_id, $user_id, $post_title, $date)
    {
        Log::debug("Started ...savePosts Dao ");
        try{
            
            $conn = ConnectionManager::get('default');

            $sql=sprintf("INSERT INTO posts (user_id, post_title, debate_id, post_type, privacy, text_tagged, origin_type, organisation_id, payment,total,updated_at, time) VALUES (%s, '%s', %s, 'debate', 'public', '', '', 0, 0, 0, '%s', '%s')", $user_id, $post_title, $debate_id, $date, $date);
            
            Log::debug("SQL : ".$sql);

            $conn->execute($sql);

            Log::debug("Ended ...savePosts Dao");

        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }
    public function updatePostsTitleByDebateId($debate_id, $post_title, $date)
    {
        Log::debug("Started ...updatePostsTitleByDebateId Dao");

        try{

            $conn = ConnectionManager::get('default');

            $sql=sprintf("UPDATE posts SET post_title = '%s', updated_at = '%s'
                WHERE debate_id = '%s'", $post_title, $date, $debate_id);

            Log::debug("SQL : ".$sql);

            $conn->execute($sql);
           
            Log::debug("Ended ...updatePostsTitleByDebateId Dao");
            
            return 1;
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }
    /**
     * Delete Debate
     */
    public function deletePostsByDebateId($debate_id)
    {
        Log::debug("Started ...deletePostsByDebateId Dao");

        try{

            $conn = ConnectionManager::get('default');

            //deleting questions
            $sql=sprintf("DELETE FROM `posts` WHERE debate_id = '%s'", 
            $debate_id);
            Log::debug("SQL : ".$sql);
            $conn->execute($sql);
           
            Log::debug("Ended ...deletePostsByDebateId Dao");
            
            return 1;
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }

    public function addNotify($toid, $fromid, $action, $node , $contentId , $commentId = 0)
    {
        Log::debug("Started ...updatePostsTitleByQuizId Dao");

        try{

            $conn = ConnectionManager::get('default');
            switch($action){
                case 'like':
                    $nodeurl = "".$contentId;
                    $sql = sprintf("INSERT INTO notifications set to_user_id = $toid , from_user_id = $fromid , action = '$action' , node_type = '$node' , node_url = '$nodeurl'");
                    $conn->execute($sql);
                    break;
                case 'added_comment':
                    $nodeurl = "".$contentId."/".$commentId;
                    $sql = sprintf("INSERT INTO notifications set to_user_id = $toid , from_user_id = $fromid , action = '$action' , node_type = '$node' , node_url = '$nodeurl'");
                    $conn->execute($sql);
                    break;
                case 'comment_reply':
                    $nodeurl = "".$contentId."/".$commentId;
                    $sql = sprintf("INSERT INTO notifications set to_user_id = $toid , from_user_id = $fromid , action = '$action' , node_type = '$node' , node_url = '$nodeurl'");
                    $conn->execute($sql);
                    break;
                case 'added_discussion_comment':
                    $nodeurl = "".$contentId."/".$commentId;
                    $sql = sprintf("INSERT INTO notifications set to_user_id = $toid , from_user_id = $fromid , action = '$action' , node_type = '$node' , node_url = '$nodeurl'");
                    $conn->execute($sql);
                    break;
                case 'share':
                    $nodeurl = "".$contentId;
                    $sql = sprintf("INSERT INTO notifications set to_user_id = $toid , from_user_id = $fromid , action = '$action' , node_type = '$node' , node_url = '$nodeurl'");
                    $conn->execute($sql);
                    break;
                case 'share':
                    $nodeurl = "".$contentId;
                    $sql = sprintf("INSERT INTO notifications set to_user_id = $toid , from_user_id = $fromid , action = '$action' , node_type = '$node' , node_url = '$nodeurl'");
                    $conn->execute($sql);
                    break;
                case 'upvoted_comment':
                    $nodeurl = "".$contentId."/".$commentId;
                    $sql = sprintf("INSERT INTO notifications set to_user_id = $toid , from_user_id = $fromid , action = '$action' , node_type = '$node' , node_url = '$nodeurl'");
                    $conn->execute($sql);
                    break;
                case 'downvoted_comment':
                    $nodeurl = "".$contentId."/".$commentId;
                    $sql = sprintf("INSERT INTO notifications set to_user_id = $toid , from_user_id = $fromid , action = '$action' , node_type = '$node' , node_url = '$nodeurl'");
                    $conn->execute($sql);
                    break;
                case 'invite':
                    $nodeurl = "".$contentId;
                    $sql = sprintf("INSERT INTO notifications set to_user_id = $toid , from_user_id = $fromid , action = '$action' , node_type = '$node' , node_url = '$nodeurl'");
                    $conn->execute($sql);
                    break;
                case 'added_procon_main':
                    $nodeurl = "".$contentId;
                    $sql = sprintf("INSERT INTO notifications set to_user_id = $toid , from_user_id = $fromid , action = '$action' , node_type = '$node' , node_url = '$nodeurl'");
                    $conn->execute($sql);
                    break;
                case 'added_procon_discussion':
                    $nodeurl = "".$contentId."/".$commentId;
                    $sql = sprintf("INSERT INTO notifications set to_user_id = $toid , from_user_id = $fromid , action = '$action' , node_type = '$node' , node_url = '$nodeurl'");
                    $conn->execute($sql);
                    break;

            }
            
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }

    public function share($type, $contentId, $userId)
    {
        Log::debug("Started ...deletePostsByDebateId Dao");

        try{
            $result = array("status" => 0 , "msg" => 'Success');
            $conn = ConnectionManager::get('default');
            $posttype = [
                'article' => 'article_id',
                'debate' => 'debate_id',
                'poll' => 'poll_id',
                'quickbit' => 'quickbit_id',
                'quiz' => 'quiz_id',
                'session' => 'session_id'
            ];
            $stmt = $conn->execute(sprintf("SELECT * from posts where `%s` = %s", $posttype[$type] , $contentId));
            if($res = $stmt->fetch('assoc')){
                $pid = $res['post_id'];
                $share_ids = $res['total_user_ids'] != null ? $res['total_user_ids'] : '';
            }else{$result['msg'] = "no post found";return $result;}
            $share_ids = explode(",", $share_ids);
            if(!in_array($userId, $share_ids)){
                array_push($share_ids, $userId);
            }else{
                $result['msg'] = "Already Shared";return $result;
            }
            array_unique($share_ids);
            $ids = implode(",", $share_ids);
            $ids = ltrim($ids, ",");
            //return $ids;
            if($conn->execute(sprintf("update posts set total_user_ids = '$ids' where post_id = $pid"))){
                $result['status'] = 1;return $result;
            }else{
                $result['msg'] = "Something went wrong";return $result;
            }
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }

    public function commentPoints($case , $contentId , $mul = 1 ){
        $pod = new PointsDao;
        $codes = new Codes;
        switch($case){
            case 'article_discussion':
                $k = $codes->COMMENT_K * $mul;
                $sql = $conn->execute("SELECT user_id from articles_discussion where articles_discussion_id = $contentId");
                $res = $sql->fetch('assoc');
                $pod->addPoints('activities', $res['user_id'] , $k);
                return $k;
                break;
            case 'article_discussion_get':
                $k = $codes->COMMENT_K;
                $sql = $conn->execute("SELECT * from articles_discussion_likes where articles_discussion_id = $contentId");
                $res = $sql->count();
                return $k * $res;
                break;
            case 'post_comment':
                $k = $codes->COMMENT_K * $mul;
                $sql = $conn->execute("SELECT user_id from posts_comments where comment_id = $contentId");
                $res = $sql->fetch('assoc');
                $pod->addPoints('activities', $res['user_id'] , $k);
                return $k;
                break;
            case 'session_comment':
                $k = $codes->COMMENT_K * $mul;
                $sql = $conn->execute("SELECT * from sessions_comment where sessions_comment_id = $contentId");
                $res = $sql->fetch('assoc');
                $pod->addPoints('activities', $res['user_id'] , $k);
                return $k;
                break;
            case 'session_discussion':
                $k = $codes->COMMENT_K * $mul;
                $sql = $conn->execute("SELECT * from sessions_discussion where sessions_discussion_id = $contentId");
                $res = $sql->fetch('assoc');
                $pod->addPoints('activities', $res['user_id'] , $k);
                return $k;
                break;
            case 'session_qa':
                $k = $codes->COMMENT_K * $mul;
                $sql = $conn->execute("SELECT * FROM `sessions_qa` where sessions_qa_id = $contentId");
                $res = $sql->fetch('assoc');
                $pod->addPoints('activities', $res['user_id'] , $k);
                return $k;
                break;
        }
    }

    public function react($userId , $type , $contentId , $reaction , $commentId)
    {
        Log::debug("Started ...react Dao : Post Id : ");

        try{

            $conn = ConnectionManager::get('default');
            //return array('status' => 0 , 'message' => 'here');
            $art = new ArticlesDao;
            $qb = new QuickBitsDao;
            $dd = new DebatesDao;

            $result = array("status" => 0 , "message" => "something went wrong");
            switch($type){
                case "article":
                    $s = sprintf("SELECT * from articles where articles_id = $contentId");
                    $st = $conn->execute($s);
                    if(!$e = $st->fetch("assoc")){$result['message'] = "article not found"; return $result;}
                    $created_by = $e['created_by'];
                    if($reaction >= 0){
                        $check = $conn->execute(sprintf("select * from articles_likes where articles_id = $contentId and user_id = $userId "));
                        if($e = $check->fetch('assoc'))
                            $sql = sprintf("UPDATE articles_likes set reaction_type = $reaction where  articles_id = $contentId and user_id = $userId");
                        else
                            $sql = sprintf("INSERT INTO articles_likes set articles_id = $contentId , user_id = $userId , reaction_type = $reaction");$art->articlePoints('by_content_like' , $userId , $contentId);
                        if(!$conn->execute($sql)){
                            return $result;
                        }

                    }elseif($reaction == -1 || $reaction == '-1'){
                        $sql = sprintf("DELETE from articles_likes where articles_id = $contentId and user_id = $userId");
                        if(!$conn->execute($sql)){
                            $art->articlePoints('by_content_unlike' , 0 , $contentId);
                            return $result;
                        }
                    }else{
                        $result['message'] = "invalid reaction";
                        return $result;
                    }
                    @$this->addNotify($created_by, $userId, 'like' , 'article' , $contentId );
                    break;
                case "article_discussion":
                    $s = sprintf("SELECT * from articles where articles_id = $contentId");
                    $st = $conn->execute($s);
                    if(!$e = $st->fetch("assoc")){$result['message'] = "article not found"; return $result;}

                    $s = sprintf("SELECT * from articles_discussion where articles_discussion_id = $commentId and articles_id = $contentId");
                    $st = $conn->execute($s);
                    if(!$e = $st->fetch("assoc")){$result['message'] = "article discussion not found"; return $result;}
                    if($reaction == 1 || $reaction == '1' || $reaction == -1 || $reaction == '-1'){
                        $check = $conn->execute(sprintf("SELECT * from articles_discussion_likes where articles_id = $contentId and user_id = $userId and articles_discussion_id = $commentId"));
                        if($e = $check->fetch('assoc'))
                            $sql = sprintf("UPDATE articles_discussion_likes set vote_type = %s where articles_id = $contentId and user_id = $userId and articles_discussion_id = $commentId" , $reaction);
                        else
                            $sql = sprintf("INSERT INTO articles_discussion_likes set articles_id = $contentId , user_id = $userId , vote_type = %s , articles_discussion_id = $commentId" , $reaction);$this->commentPoints('article_discussion' , $commentId , $reaction);
                        if(!$conn->execute($sql)){
                            return $result;
                        }
                        @$this->addNotify($e['user_id'], $userId, $reaction == 1 ? 'upvoted_comment' : 'downvoted_comment' ,'article', $contentId, $commentId );

                    }elseif($reaction == 0){
                        $sql = sprintf("DELETE from articles_discussion_likes where articles_id = $contentId and user_id = $userId and articles_discussion_id = $commentId");
                        if(!$conn->execute($sql)){
                            return $result;
                        }
                    }else{
                        $result['message'] = "invalid reaction";
                        return $result;
                    }
                    
                    break;
                case "quickbit":
                    $s = sprintf("SELECT * from quickbits where id = $contentId");
                    $st = $conn->execute($s);
                    if(!$e = $st->fetch("assoc")){$result['message'] = "quickbit not found"; return $result;}

                    $s = sprintf("SELECT * from posts where quickbit_id = $contentId");
                    $st = $conn->execute($s);
                    if(!$e = $st->fetch("assoc")){$result['message'] = "post not found"; return $result;}
                    $pid = $e['post_id'];
                    $created_by = $e['user_id'];
                    if($reaction >= 0){
                        $check = $conn->execute(sprintf("SELECT * from posts_likes where post_id = $pid and user_id = $userId "));
                        if($e = $check->fetch('assoc'))
                            $sql = sprintf("UPDATE posts_likes set reaction_type = $reaction where post_id = $pid and user_id = $userId");
                        else
                            $sql = sprintf("INSERT INTO posts_likes set post_id = $pid , user_id = $userId , reaction_type = $reaction");$qb->quickbitPoints('by_content_like',0,$contentId);
                        if(!$conn->execute($sql)){
                            return $result;
                        }
                        @$this->addNotify($created_by, $userId, 'like' , 'post' , $contentId );

                    }elseif($reaction == -1 || $reaction == '-1'){
                        $sql = sprintf("DELETE from posts_likes where post_id = $contentId and user_id = $userId");
                        $qb->quickbitPoints('by_content_unlike',0,$contentId);
                        if(!$conn->execute($sql)){
                            return $result;
                        }
                    }else{
                        $result['message'] = "invalid reaction";
                        return $result;
                    }

                    break;
                case "quickbit_comment":
                    $s = sprintf("SELECT * from quickbits where id = $contentId");
                    $st = $conn->execute($s);
                    if(!$e = $st->fetch("assoc")){$result['message'] = "quickbit not found"; return $result;}

                    $s = sprintf("SELECT * from posts where quickbit_id = $contentId");
                    $st = $conn->execute($s);
                    if(!$e = $st->fetch("assoc")){$result['message'] = "post not found"; return $result;}

                    $pid = $e['post_id'];
                    $s = sprintf("SELECT * from posts_comments where comment_id = $commentId ");
                    $st = $conn->execute($s);
                    if(!$e = $st->fetch("assoc")){$result['message'] = "quickbit comment not found"; return $result;}
                    $created_by = $e['user_id'];
                    $cid = $e['comment_id'];
                    if($reaction == 1 || $reaction == '1' || $reaction == -1 || $reaction == '-1'){
                        $check = $conn->execute(sprintf("SELECT * from posts_comments_likes where comment_id = $cid and user_id = $userId "));
                        if($e = $check->fetch('assoc'))
                            $sql = sprintf("UPDATE posts_comments_likes set vote_type = %s where comment_id = $cid and user_id = $userId" , $reaction);
                        else
                            $sql = sprintf("INSERT INTO posts_comments_likes set comment_id = $cid , user_id = $userId , vote_type = %s " , $reaction);$this->commentPoints('posts_comments' , $commentId , $reaction);
                        if(!$conn->execute($sql)){
                            return $result;
                        }
                        @$this->addNotify($created_by, $userId, $reaction == 1 ? 'upvoted_comment' : 'downvoted_comment' ,'post', $contentId, $commentId );
                    }elseif($reaction == 0){
                        $sql = sprintf("DELETE from posts_comments_likes where comment_id = $cid and user_id = $userId");
                        if(!$conn->execute($sql)){
                            return $result;
                        }
                    }else{
                        $result['message'] = "invalid reaction";
                        return $result;
                    }
                    break;
                case "quiz":
                    $s = sprintf("SELECT * from quizzes where id = $contentId");
                    $st = $conn->execute($s);
                    if(!$e = $st->fetch("assoc")){$result['message'] = "quiz not found"; return $result;}
                    $s = sprintf("SELECT * from posts where quiz_id = $contentId");
                    $st = $conn->execute($s);
                    if(!$e = $st->fetch("assoc")){$result['message'] = "post not found"; return $result;}
                    $pid = $e['post_id'];
                    $created_by = $e['user_id'];
                    if($reaction >= 0){
                        $check = $conn->execute(sprintf("SELECT * from posts_likes where post_id = $pid and user_id = $userId "));
                        if($e = $check->fetch('assoc'))
                            $sql = sprintf("UPDATE posts_likes set reaction_type = $reaction where post_id = $pid and user_id = $userId");
                        else
                            $sql = sprintf("INSERT INTO posts_likes set post_id = $pid , user_id = $userId , reaction_type = $reaction");
                        if(!$conn->execute($sql)){
                            return $result;
                        }
                        @$this->addNotify($created_by, $userId, 'like' , 'post' , $contentId );
                    }elseif($reaction == -1 || $reaction == '-1'){
                        $sql = sprintf("DELETE from posts_likes where post_id = $contentId and user_id = $userId");
                        if(!$conn->execute($sql)){
                            return $result;
                        }
                    }else{
                        $result['message'] = "invalid reaction";
                        return $result;
                    }
                    break;
                case "debate":
                    $s = sprintf("SELECT * from debates where id = $contentId");
                    $st = $conn->execute($s);
                    if(!$e = $st->fetch("assoc")){$result['message'] = "debates not found"; return $result;}

                    $s = sprintf("SELECT * from posts where debate_id = $contentId");
                    $st = $conn->execute($s);
                    if(!$e = $st->fetch("assoc")){$result['message'] = "posts not found"; return $result;}
                    $pid = $e['post_id'];
                    $created_by = $e['user_id'];
                    if($reaction >= 0){
                        $check = $conn->execute(sprintf("SELECT * from posts_likes where post_id = $pid and user_id = $userId "));
                        if($e = $check->fetch('assoc'))
                            $sql = sprintf("UPDATE posts_likes set reaction_type = $reaction where post_id = $pid and user_id = $userId");
                        else
                            $sql = sprintf("INSERT INTO posts_likes set post_id = $pid , user_id = $userId , reaction_type = $reaction");$dd->giveVotePoints( 'main_vote' ,$contentId);
                        if(!$conn->execute($sql)){
                            return $result;
                        }
                        @$this->addNotify($created_by, $userId, 'like' , 'post' , $contentId );
                    }elseif($reaction == -1 || $reaction == '-1'){
                        $sql = sprintf("DELETE from posts_likes where post_id = $contentId and user_id = $userId");
                        $dd->giveVotePoints( 'main_vote' ,$contentId ,-1);
                        if(!$conn->execute($sql)){
                            return $result;
                        }
                    }else{
                        $result['message'] = "invalid reaction";
                        return $result;
                    }
                    break;
                case "session":
                    $s = sprintf("SELECT * from sessions where sessions_id = $contentId");
                    $st = $conn->execute($s);
                    if(!$e = $st->fetch("assoc")){$result['message'] = "sessions not found"; return $result;}
                    $created_by = $e['created_by'];
                    if($reaction >= 0){
                        $check = $conn->execute(sprintf("SELECT * from session_likes where session_id = $contentId and user_id = $userId "));
                        if($e = $check->fetch('assoc'))
                            $sql = sprintf("UPDATE session_likes set reaction_type = $reaction where session_id = $contentId and user_id = $userId ");
                        else
                            $sql = sprintf("INSERT INTO session_likes set session_id = $contentId , user_id = $userId , reaction_type = $reaction");
                        if(!$conn->execute($sql)){
                            return $result;
                        }
                        @$this->addNotify($created_by, $userId, 'like' , 'session' , $contentId );
                    }elseif($reaction == -1 || $reaction == '-1'){
                        $sql = sprintf("DELETE from session_likes where session_id = $contentId and user_id = $userId");
                        if(!$conn->execute($sql)){
                            return $result;
                        }
                    }else{
                        $result['message'] = "invalid reaction";
                        return $result;
                    }
                    break;
                case "session_comment":
                    $s = sprintf("SELECT * from sessions where sessions_id = $contentId");
                    $st = $conn->execute($s);
                    if(!$e = $st->fetch("assoc")){$result['message'] = "sessions not found"; return $result;}

                    $s = sprintf("SELECT * from sessions_comment where sessions_id = $contentId and sessions_comment_id = $commentId");
                    $st = $conn->execute($s);
                    if(!$e = $st->fetch("assoc")){$result['message'] = "sessions not found"; return $result;}

                    if($reaction == 1 || $reaction == '1' || $reaction == -1 || $reaction == '-1'){
                        $check = $conn->execute(sprintf("SELECT * from sessions_comment_likes where sessions_id = $contentId and user_id = $userId and sessions_comment_id = $commentId"));
                        if($e = $check->fetch('assoc'))
                            $sql = sprintf("UPDATE sessions_comment_likes set vote_type = %s where sessions_id = $contentId and user_id = $userId and sessions_comment_id = $commentId" , $reaction);
                        else
                            $sql = sprintf("INSERT INTO sessions_comment_likes set sessions_id = $contentId , user_id = $userId , vote_type = %s , sessions_comment_id = $commentId" , $reaction);$this->commentPoints($type , $commentId , $reaction);
                        if(!$conn->execute($sql)){
                            return $result;
                        }

                    }elseif($reaction == 0){
                        $sql = sprintf("DELETE from sessions_comment_likes where sessions_id = $contentId and user_id = $userId and sessions_comment_id = $commentId");
                        if(!$conn->execute($sql)){
                            return $result;
                        }
                    }else{
                        $result['message'] = "invalid reaction";
                        return $result;
                    }
                    break;
                case "session_discussion":
                    $s = sprintf("SELECT * from sessions where sessions_id = $contentId");
                    $st = $conn->execute($s);
                    if(!$e = $st->fetch("assoc")){$result['message'] = "sessions not found"; return $result;}

                    $s = sprintf("SELECT * from sessions_discussion where sessions_id = $contentId and sessions_discussion_id = $commentId");
                    $st = $conn->execute($s);
                    if(!$e = $st->fetch("assoc")){$result['message'] = "sessions discussion not found"; return $result;}

                    if($reaction == 1 || $reaction == '1' || $reaction == -1 || $reaction == '-1'){
                        $check = $conn->execute(sprintf("SELECT * from sessions_discussion_likes where sessions_id = $contentId and user_id = $userId and sessions_discussion_id = $commentId"));
                        if($e = $check->fetch('assoc'))
                            $sql = sprintf("UPDATE sessions_discussion_likes set vote_type = %s where sessions_id = $contentId and user_id = $userId and sessions_discussion_id = $commentId" , $reaction);
                        else
                            $sql = sprintf("INSERT INTO sessions_discussion_likes set sessions_id = $contentId , user_id = $userId , vote_type = %s , sessions_discussion_id = $commentId" , $reaction);$this->commentPoints($type , $commentId , $reaction);
                        if(!$conn->execute($sql)){
                            return $result;
                        }

                    }elseif($reaction == 0){
                        $sql = sprintf("DELETE from sessions_discussion_likes where sessions_id = $contentId and user_id = $userId and sessions_discussion_id = $commentId");
                        if(!$conn->execute($sql)){
                            return $result;
                        }
                    }else{
                        $result['message'] = "invalid reaction";
                        return $result;
                    }
                    break;
                case "session_qa":
                    $s = sprintf("SELECT * from sessions where sessions_id = $contentId");
                    $st = $conn->execute($s);
                    if(!$e = $st->fetch("assoc")){$result['message'] = "sessions not found"; return $result;}

                    $s = sprintf("SELECT * from sessions_qa where sessions_id = $contentId and sessions_qa_id = $commentId");
                    $st = $conn->execute($s);
                    if(!$e = $st->fetch("assoc")){$result['message'] = "sessions qa not found"; return $result;}

                    if($reaction == 1 || $reaction == '1' || $reaction == -1 || $reaction == '-1'){
                        $check = $conn->execute(sprintf("SELECT * from sessions_qa_likes where user_id = $userId and sessions_qa_id = $commentId"));
                        if($e = $check->fetch('assoc'))
                            $sql = sprintf("UPDATE sessions_qa_likes set vote_type = %s where user_id = $userId and sessions_qa_id = $commentId" , $reaction);
                        else
                            $sql = sprintf("INSERT INTO sessions_qa_likes set user_id = $userId , vote_type = %s , sessions_qa_id = $commentId" , $reaction);$this->commentPoints($type , $commentId , $reaction);
                        if(!$conn->execute($sql)){
                            return $result;
                        }

                    }elseif($reaction == 0){
                        $sql = sprintf("DELETE from sessions_qa_likes where and user_id = $userId and sessions_qa_id = $commentId");
                        if(!$conn->execute($sql)){
                            return $result;
                        }
                    }else{
                        $result['message'] = "invalid reaction";
                        return $result;
                    }
                    break;
                case "poll":
                    $s = sprintf("SELECT * from polls where id = $contentId");
                    $st = $conn->execute($s);
                    if(!$e = $st->fetch("assoc")){$result['message'] = "poll not found"; return $result;}

                    $s = sprintf("SELECT * from polls_options where poll_id = $contentId and option_id = $reaction");
                    $st = $conn->execute($s);
                    if(!$e = $st->fetch("assoc")){$result['message'] = "poll option not found"; return $result;}

                    $s = sprintf("SELECT * from users_polls_options where poll_id = $contentId and user_id = $userId");
                    $st = $conn->execute($s);
                    if($e = $st->fetch("assoc")){$result['message'] = "already voted"; return $result;}

                    $sql = sprintf("INSERT into users_polls_options set user_id = $userId , poll_id = $contentId , option_id = $reaction");
                    if(!$conn->execute($sql)){$result['message'] = "something went wrong"; return $result;}
                    $this->pollPoints('by_content_like' , 0 , $contentId);
                    break;
                default:
                    return $result;
            }

            Log::debug("Ended ...getPostDetailByPostId Dao");
            $result['status'] =1;
            $result['message'] = 'Success';

            return $result;
            
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }

    }


    public function pollPoints($case , $userId , $pollsId = 0){
        $pod = new PointsDao;
        $conn = ConnectionManager::get('default');
        $codes = new Codes;
        switch ($case) {
            case 'const':
                $k = $codes->POLLS_K;
                $ret = $pod->checkAndDeduct($userId, $codes->DEDUCT_POLL);
                if($ret == 0){return $ret;}
                $pod->addPoints('polls', $userId , $k);
                return $k;
                break;
            case 'by_content':
                $k = $codes->POLLS_K;
                $l = $conn->execute(sprintf("SELECT * from `polls` where id = %s"  , $pollsId));
                $res = $l->fetch('assoc');
                $ret = $pod->checkAndDeduct($res['creator_id'], $codes->DEDUCT_POLL);
                if($ret == 0){return $ret;}
                $pod->addPoints('polls', $res['creator_id'] , $k);
                return $k;
                break;
            case 'like':
                $k = $codes->POLLS_C;
                $pod->addPoints('polls', $userId , $k);
                return $k;
                break;
            case 'unlike':
                $k = $codes->POLLS_C;
                $pod->addPoints('polls', $userId , -$k);
                return $k;
                break;
            case 'by_content_like':
                $k = $codes->POLLS_C;
                $l = $conn->execute(sprintf("SELECT * from `polls` where id = %s"  , $pollsId));
                $res = $l->fetch('assoc');
                $pod->addPoints('polls', $res['creator_id'] , $k);
                return $k;
                break;
            case 'by_content_unlike':
                $k = $codes->POLLS_C;
                $l = $conn->execute(sprintf("SELECT * from `polls` where id = %s"  , $pollsId));
                $res = $l->fetch('assoc');
                $pod->addPoints('polls', $res['creator_id'] , -$k);
                return $k;
                break;
            case 'remove':
                $k = $codes->POLLS_K;
                $c = $codes->POLLS_C;
                $l = $conn->execute(sprintf("SELECT * from users_polls_options where poll_id = %s "  , $pollsId));
                $n = $l->count();
                $total = $k + ($n * $c);
                $l = $conn->execute(sprintf("SELECT * from `polls` where id = %s"  , $pollsId));
                $res = $l->fetch('assoc');
                $pod->addPoints('polls', $res['creator_id'] , -$total);
                return -$total;
                break;
            case 'get':
                $k = $codes->POLLS_K;
                $c = $codes->POLLS_C;
                $l = $conn->execute(sprintf("SELECT * from users_polls_options where poll_id = %s"  , $pollsId));
                $n = $l->count();
                $total = $k + ($n * $c);
                return $total;
                break;
            default:
                return null;
                break;
        }
    }

    public function createPoll($args ,$date)
    {
        Log::debug("Started ...deletePostsByDebateId Dao");

        try{
            extract($args);
            $conn = ConnectionManager::get('default');
            $codes = new Codes;
            if($args['user_type'] == 'user' || $args['user_type'] == 'User' || $args['user_type'] == 'community' || $args['user_type'] == 'Community'){
                $sql = $conn->execute(sprintf("SELECT total FROM users_points where user_id = %s", $args['creator_id']));
                if($sql = $sql->fetch('assoc')){
                    if($sql['total'] >= $codes->DEDUCT_POLL){

                    }else{
                        throw new Exception("not enough points");
                    }
                }else{
                    $conn->execute("INSERT into users_points set user_id = $id , total = $codes->INITIAL_PTS");
                    return $this->createPoll($args, $date);
                }
            }
            $conn = ConnectionManager::get('default');
            $sql = sprintf("INSERT into polls set title = '%s' , user_type = '%s' , creator_id = %s , community_id = %s , interests = '%s'" ,$this->mysql_real_escape_string($args['title']) , $args['user_type'] , $args['creator_id'] , $args['community_id'], $args['interests'] );
            if(!$conn->execute($sql)){
                return ['status' => 0, "message" => "something went wrong"];
            }
            $sql = "SELECT LAST_INSERT_ID()";
            $stmt = $conn->execute($sql);
            
            $res = $stmt->fetch();
            $pollid = $res[0];
            //adding questions
            foreach ($option as $i ) {
                $sql = sprintf("INSERT INTO polls_options set poll_id = $pollid , text = '%s'", $this->mysql_real_escape_string($i));
                $conn->execute($sql);
            }
            $sql=sprintf("INSERT INTO posts ( user_id, text, community_id, post_type, privacy, text_tagged, origin_type, organisation_id, payment,total,updated_at, time , user_type , poll_id) VALUES ( '%s', '%s', '%s', 'poll', 'public', '', '', 0, 0, 0, '%s', '%s' , '%s' , $pollid)", $args['creator_id'], $this->mysql_real_escape_string($args['title']), $args['community_id'], $date, $date, $args['user_type']);
            Log::debug("SQL : ".$sql);
            $conn->execute($sql);

            foreach ($option as $i ) {
                $sql = sprintf("INSERT INTO posts_polls_options set poll_id = $pollid , text = '%s'", $this->mysql_real_escape_string($i));
                $conn->execute($sql);
            }
            if($user_type == 'user' || $user_type == 'User'){
                $ret = $this->pollPoints('const' , $args["creator_id"] , $pollid);
                if($ret == 0){throw new Exception('not enough points.');}
            }
            return $this->getPollData($pollid , $args['creator_id']);
            Log::debug("Ended ...deletePostsByDebateId Dao");
            
            return 1;
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }
    public function getPollData($pollid , $userId = 0){
        try{
            $conn = ConnectionManager::get("default");
            $sql = sprintf("SELECT * FROM posts where poll_id =  $pollid"); 
            $st = $conn->execute($sql);
            $poll = [];
            if($result = $st->fetch("assoc")){
                $poll = $result;
                $poll['title'] = $result['text'];
                $dat=$conn->execute(sprintf("select * from polls where id='%s'",$pollid));
                if($i = $dat->fetch("assoc")){

                }else{
                    return ["message"=> "something went wrong" , "status"=> 0];
                }
                $i['options'] = array();
                
                $sql=sprintf("select * from polls_options where poll_id='%s'",$pollid);          

                Log::debug("SQL : ".$sql);

                $s = $conn->execute($sql);
                //$i = 0;
                //$poll['options'] = array();
                $i['my_vote'] = 0;
                while($r = $s->fetch('assoc')){
                    $r['votes'] = 0;
                    $co = $conn->execute(sprintf("SELECT * from users_polls_options where poll_id = $pollid and option_id = %s", $r['option_id']));
                    while($e = $co->fetch("assoc")){
                        if($e['user_id'] == $userId){
                            $i["my_vote"] = $r['option_id'];
                        }
                        $r['votes']++;
                    }
                    array_push($i['options'], $r);

                }
                $pid = $poll['post_id'];
                $user = new UserModel($userId);
                $mf = $user->get_friends_ids($userId);
                $mf = implode(',', $mf);
                $i['comment_count'] = $conn->execute(sprintf("SELECT * FROM posts_comments WHERE node_id = $pid and node_type = 'user' "))->count();
                if($co = $conn->execute(sprintf("SELECT * FROM posts_comments WHERE node_id = $pid and node_type = 'user' and FIND_IN_SET(user_id , '$mf') order by comment_id desc"))->fetch('assoc')){
                    $friend = $this->userData($userId , $co['user_id'] , 'user' , 0)[0];
                    $co['user'] = $friend;
                    $i['friend_comment'] = $co;
                }else{
                    $i['friend_comment'] = null;
                }
                $i['like_count'] = $conn->execute(sprintf("SELECT * FROM posts_likes WHERE post_id = $pid "))->count();
                if($il = $conn->execute(sprintf("SELECT * FROM users_polls_options WHERE poll_id = $pollid and FIND_IN_SET(user_id , '$mf')"))->fetch('assoc')){
                    $friend = $this->userData($userId , $il['user_id'] , 'user' , 0)[0];
                    $il['user'] = $friend;
                    $i['friend_vote'] = $il;
                }else{
                    $i['friend_vote'] = null;
                }
                $dat = $this->userData($userId , $result['user_id'] , $result['user_type'] , $result['community_id']);
                //$poll['created_by'] = $dat[0];
                $i['created_by'] = (isset($dat[0])? $dat[0] : null);
                $i['community_data'] = (isset($dat[1])? $dat[1] : null);
                $i['points'] = $this->pollPoints('get' , 0 , $pollid);
                $poll['poll'] = $i;
                //array_push($res['poll'], $poll);
            }
            //$ans['polls'] = $results;
            return $poll;
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }

    public function distance($lat1, $lon1, $lat2, $lon2) { 
        $pi80 = M_PI / 180; 
        $lat1 *= $pi80; 
        $lon1 *= $pi80; 
        $lat2 *= $pi80; 
        $lon2 *= $pi80; 
        $r = 6372.797; // mean radius of Earth in km 
        $dlat = $lat2 - $lat1; 
        $dlon = $lon2 - $lon1; 
        $a = sin($dlat / 2) * sin($dlat / 2) + cos($lat1) * cos($lat2) * sin($dlon / 2) * sin($dlon / 2); 
        $c = 2 * atan2(sqrt($a), sqrt(1 - $a)); 
        $km = $r * $c; 
        //echo ' '.$km; 
        return $km; 
    }


    public function alllikes($type , $userId , $reaction , $contentId , $commentId , $lastid){
        try{
            //$offset *= $codes->MAX_RESULTS;
            $conn = ConnectionManager::get("default");
            $codes = new Codes;
            $results=array();
            $temp = explode(',', "article,article_discussion,article_discussion_replies,quickbit,quickbit_comment,quickbit_comment_reply,quiz,quiz_attempted,quiz_participants,debate,debate_participants,deabte_procon_rating,session,session_comment,session_comment_replies,session_discussion,session_discussion_replies,poll,questions,answers,replies,session_attendees_list,venue_followers,organization_followers");
            /*foreach ($temp as $key ) {
                $key = trim($key);
                $results[$key] = array();
            }*/
            if($lastid == 0){
                $cona = " order by id DESC limit 0 , $codes->MAX_RESULTS";
            }else{
                $cona = " and id < $lastid order by id DESC limit 0 , $codes->MAX_RESULTS";
            }
            
            switch ($type) {
                
                case 'article':
                    $alluser=$conn->execute(sprintf("select user_id FROM articles_likes WHERE articles_id = $contentId and reaction_type = $reaction $cona"));
                    if($alluser->count() > 0)
                    {
                        while($ralluser=$alluser->fetch("assoc"))
                        {
                            $dat = $this->userData($userId , $ralluser['user_id'] , 'user' , 0);
                            $push = isset($dat[0]) ? $dat[0] : null;
                            if($push == null){continue;}
                            array_push($results, $push);
                        }
                    }
                break;

                case 'article_discussion':
                    $alluser=$conn->execute(sprintf("select user_id FROM articles_discussion_likes WHERE articles_id = $contentId and vote_type = $reaction and articles_discussion_id = $commentId $cona"));
                    if($alluser->count() > 0)
                    {
                        while($ralluser=$alluser->fetch("assoc"))
                        {
                            $dat = $this->userData($userId , $ralluser['user_id'] , 'user' , 0);
                            $push = isset($dat[0]) ? $dat[0] : null;
                            if($push == null){continue;}
                            array_push($results, $push);
                        }
                    }
                break;

                case 'article_discussion_replies':
                    $alluser=$conn->execute(sprintf("select user_id FROM articles_discussion_likes WHERE articles_id = $contentId and vote_type = $reaction and articles_discussion_id = $commentId $cona"));
                    if($alluser->count() > 0)
                    {
                        while($ralluser=$alluser->fetch("assoc"))
                        {
                            $dat = $this->userData($userId , $ralluser['user_id'] , 'user' , 0);
                            $push = isset($dat[0]) ? $dat[0] : null;
                            if($push == null){continue;}
                            array_push($results, $push);
                        }
                    }
                break;

                case 'quickbit':
                    $sql = $conn->execute(sprintf("select post_id FROM posts where quickbit_id = $contentId"));
                    if($r = $sql->fetch("assoc")){
                        $contentId = $r['post_id'];
                    }else{break;}
                    $alluser=$conn->execute(sprintf("select user_id FROM posts_likes WHERE post_id = $contentId and reaction_type = $reaction $cona"));
                    if($alluser->count() > 0)
                    {
                        while($ralluser=$alluser->fetch("assoc"))
                        {
                            $dat = $this->userData($userId , $ralluser['user_id'] , 'user' , 0);
                            $push = isset($dat[0]) ? $dat[0] : null;
                            if($push == null){continue;}
                            array_push($results, $push);
                        }
                    }
                break;

                case 'quickbit_comment':
                    $sql = $conn->execute(sprintf("select post_id FROM posts where quickbit_id = $contentId"));
                    if($r = $sql->fetch("assoc")){
                        $contentId = $r['post_id'];
                    }else{break;}
                    $alluser=$conn->execute(sprintf("select user_id FROM posts_comments_likes WHERE comment_id = $commentId and vote_type = $reaction $cona"));
                    if($alluser->count() > 0)
                    {
                        while($ralluser=$alluser->fetch("assoc"))
                        {
                            $dat = $this->userData($userId , $ralluser['user_id'] , 'user' , 0);
                            $push = isset($dat[0]) ? $dat[0] : null;
                            if($push == null){continue;}
                            array_push($results, $push);
                        }
                    }
                break;

                case 'quickbit_comment_reply':
                    $sql = $conn->execute(sprintf("select post_id FROM posts where quickbit_id = $contentId"));
                    if($r = $sql->fetch("assoc")){
                        $contentId = $r['post_id'];
                    }else{break;}
                    $alluser=$conn->execute(sprintf("select user_id FROM posts_comments_likes WHERE comment_id = $commentId and vote_type = $reaction $cona"));
                    if($alluser->count() > 0)
                    {
                        while($ralluser=$alluser->fetch("assoc"))
                        {
                            $dat = $this->userData($userId , $ralluser['user_id'] , 'user' , 0);
                            $push = isset($dat[0]) ? $dat[0] : null;
                            if($push == null){continue;}
                            array_push($results, $push);
                        }
                    }
                break;

                case 'quiz':
                    $sql = $conn->execute(sprintf("select post_id FROM posts where quiz_id = $contentId"));
                    if($r = $sql->fetch("assoc")){
                        $contentId = $r['post_id'];
                    }else{break;}
                    $alluser=$conn->execute(sprintf("select user_id FROM posts_likes WHERE post_id = $contentId and reaction_type = $reaction $cona"));
                    if($alluser->count() > 0)
                    {
                        while($ralluser=$alluser->fetch("assoc"))
                        {
                            $dat = $this->userData($userId , $ralluser['user_id'] , 'user' , 0);
                            $push = isset($dat[0]) ? $dat[0] : null;
                            if($push == null){continue;}
                            array_push($results, $push);
                        }
                    }
                break;

                case 'quiz_attempted':
                    $alluser=$conn->execute(sprintf("select user_id FROM quizzes_participants WHERE quiz_id = $contentId and is_attained = 1 $cona"));
                    if($alluser->count() > 0)
                    {
                        while($ralluser=$alluser->fetch("assoc"))
                        {
                            $dat = $this->userData($userId , $ralluser['user_id'] , 'user' , 0);
                            $push = isset($dat[0]) ? $dat[0] : null;
                            if($push == null){continue;}
                            array_push($results, $push);
                        }
                    }
                break;

                case 'quiz_participants':
                    $alluser=$conn->execute(sprintf("select user_id FROM quizzes_participants WHERE quiz_id = $contentId  $cona"));
                    if($alluser->count() > 0)
                    {
                        while($ralluser=$alluser->fetch("assoc"))
                        {
                            $dat = $this->userData($userId , $ralluser['user_id'] , 'user' , 0);
                            $push = isset($dat[0]) ? $dat[0] : null;
                            if($push == null){continue;}
                            array_push($results, $push);
                        }
                    }
                break;

                case 'debate':
                    $sql = $conn->execute(sprintf("select post_id FROM posts where debate_id = $contentId"));
                    if($r = $sql->fetch("assoc")){
                        $contentId = $r['post_id'];
                    }else{break;}
                    $alluser=$conn->execute(sprintf("select user_id FROM posts_likes WHERE post_id = $contentId and reaction_type = $reaction $cona"));
                    if($alluser->count() > 0)
                    {
                        while($ralluser=$alluser->fetch("assoc"))
                        {
                            $dat = $this->userData($userId , $ralluser['user_id'] , 'user' , 0);
                            $push = isset($dat[0]) ? $dat[0] : null;
                            if($push == null){continue;}
                            array_push($results, $push);
                        }
                    }
                break;

                case 'debate_participants':
                    $alluser=$conn->execute(sprintf("select user_id FROM debates_participants WHERE debate_id = $contentId  $cona"));
                    if($alluser->count() > 0)
                    {
                        while($ralluser=$alluser->fetch("assoc"))
                        {
                            $dat = $this->userData($userId , $ralluser['user_id'] , 'user' , 0);
                            $push = isset($dat[0]) ? $dat[0] : null;
                            if($push == null){continue;}
                            array_push($results, $push);
                        }
                    }
                break;

                case 'debate_participants':
                    $alluser=$conn->execute(sprintf("select user_id FROM debates_participants WHERE debate_id = $contentId  $cona"));
                    if($alluser->count() > 0)
                    {
                        while($ralluser=$alluser->fetch("assoc"))
                        {
                            $dat = $this->userData($userId , $ralluser['user_id'] , 'user' , 0);
                            $push = isset($dat[0]) ? $dat[0] : null;
                            if($push == null){continue;}
                            array_push($results, $push);
                        }
                    }
                break;

                case 'deabte_procon_rating':
                    $alluser=$conn->execute(sprintf("select user_id FROM debates_discussions_rating WHERE debate_id = $contentId and discussion_id = $commentId $cona"));
                    if($alluser->count() > 0)
                    {
                        while($ralluser=$alluser->fetch("assoc"))
                        {
                            $dat = $this->userData($userId , $ralluser['user_id'] , 'user' , 0);
                            $push = isset($dat[0]) ? $dat[0] : null;
                            if($push == null){continue;}
                            array_push($results, $push);
                        }
                    }
                break;

                case 'session':
                    $alluser=$conn->execute(sprintf("select user_id FROM session_likes WHERE session_id = $contentId and reaction_type = $reaction $cona"));
                    if($alluser->count() > 0)
                    {
                        while($ralluser=$alluser->fetch("assoc"))
                        {
                            $dat = $this->userData($userId , $ralluser['user_id'] , 'user' , 0);
                            $push = isset($dat[0]) ? $dat[0] : null;
                            if($push == null){continue;}
                            array_push($results, $push);
                        }
                    }
                break;

                case 'session_comment':
                    $alluser=$conn->execute(sprintf("select user_id FROM sessions_comment_likes WHERE sessions_id = $contentId and vote_type = $reaction and sessions_comment_id = $commentId $cona"));
                    if($alluser->count() > 0)
                    {
                        while($ralluser=$alluser->fetch("assoc"))
                        {
                            $dat = $this->userData($userId , $ralluser['user_id'] , 'user' , 0);
                            $push = isset($dat[0]) ? $dat[0] : null;
                            if($push == null){continue;}
                            array_push($results, $push);
                        }
                    }
                break;

                case 'session_comment_replies':
                    $alluser=$conn->execute(sprintf("select user_id FROM sessions_comment_likes WHERE sessions_id = $contentId and vote_type = $reaction and sessions_comment_id = $commentId $cona"));
                    if($alluser->count() > 0)
                    {
                        while($ralluser=$alluser->fetch("assoc"))
                        {
                            $dat = $this->userData($userId , $ralluser['user_id'] , 'user' , 0);
                            $push = isset($dat[0]) ? $dat[0] : null;
                            if($push == null){continue;}
                            array_push($results, $push);
                        }
                    }
                break;

                case 'session_discussion':
                    $alluser=$conn->execute(sprintf("select user_id FROM sessions_discussion_likes WHERE sessions_id = $contentId and vote_type = $reaction and sessions_discussion_id = $commentId $cona"));
                    if($alluser->count() > 0)
                    {
                        while($ralluser=$alluser->fetch("assoc"))
                        {
                            $dat = $this->userData($userId , $ralluser['user_id'] , 'user' , 0);
                            $push = isset($dat[0]) ? $dat[0] : null;
                            if($push == null){continue;}
                            array_push($results, $push);
                        }
                    }
                break;

                case 'session_discussion_replies':
                    $alluser=$conn->execute(sprintf("select user_id FROM sessions_discussion_likes WHERE sessions_id = $contentId and vote_type = $reaction and sessions_discussion_id = $commentId $cona"));
                    if($alluser->count() > 0)
                    {
                        while($ralluser=$alluser->fetch("assoc"))
                        {
                            $dat = $this->userData($userId , $ralluser['user_id'] , 'user' , 0);
                            $push = isset($dat[0]) ? $dat[0] : null;
                            if($push == null){continue;}
                            array_push($results, $push);
                        }
                    }
                break;

                case 'poll':
                    $sql = $conn->execute(sprintf("select post_id FROM posts where poll_id = $contentId"));
                    if($r = $sql->fetch("assoc")){
                        $contentId = $r['post_id'];
                    }else{break;}
                    $alluser=$conn->execute(sprintf("select user_id FROM posts_likes WHERE post_id = $contentId and reaction_type = $reaction $cona"));
                    if($alluser->count() > 0)
                    {
                        while($ralluser=$alluser->fetch("assoc"))
                        {
                            $dat = $this->userData($userId , $ralluser['user_id'] , 'user' , 0);
                            $push = isset($dat[0]) ? $dat[0] : null;
                            if($push == null){continue;}
                            array_push($results, $push);
                        }
                    }
                break;

                case 'session_attendees_list':
                    $alluser=$conn->execute(sprintf("select user_id FROM sessions_attends WHERE session_id = $contentId and status = 1 $cona"));
                    if($alluser->count() > 0)
                    {
                        while($ralluser=$alluser->fetch("assoc"))
                        {
                            $dat = $this->userData($userId , $ralluser['user_id'] , 'user' , 0);
                            $push = isset($dat[0]) ? $dat[0] : null;
                            if($push == null){continue;}
                            array_push($results, $push);
                        }
                    }
                break;

                case 'venue_followers':
                    $alluser=$conn->execute(sprintf("select user_id FROM users_venue WHERE venue_id = $contentId $cona"));
                    if($alluser->count() > 0)
                    {
                        while($ralluser=$alluser->fetch("assoc"))
                        {
                            $dat = $this->userData($userId , $ralluser['user_id'] , 'user' , 0);
                            $push = isset($dat[0]) ? $dat[0] : null;
                            if($push == null){continue;}
                            array_push($results, $push);
                        }
                    }
                break;

                case 'organization_followers':
                    $alluser=$conn->execute(sprintf("select user_id FROM users_organization WHERE organization_id = $contentId $cona"));
                    if($alluser->count() > 0)
                    {
                        while($ralluser=$alluser->fetch("assoc"))
                        {
                            $dat = $this->userData($userId , $ralluser['user_id'] , 'user' , 0);
                            $push = isset($dat[0]) ? $dat[0] : null;
                            if($push == null){continue;}
                            array_push($results, $push);
                        }
                    }
                break;

                case 'questions':
                    $alluser=$conn->execute(sprintf("select user_id FROM sessions_qa_likes WHERE vote_type = $reaction and sessions_qa_id = $commentId $cona"));
                    if($alluser->count() > 0)
                    {
                        while($ralluser=$alluser->fetch("assoc"))
                        {
                            $dat = $this->userData($userId , $ralluser['user_id'] , 'user' , 0);
                            $push = isset($dat[0]) ? $dat[0] : null;
                            if($push == null){continue;}
                            array_push($results, $push);
                        }
                    }
                break;

                case 'answers':
                    $alluser=$conn->execute(sprintf("select user_id FROM sessions_qa_likes WHERE vote_type = $reaction and sessions_qa_id = $commentId $cona"));
                    if($alluser->count() > 0)
                    {
                        while($ralluser=$alluser->fetch("assoc"))
                        {
                            $dat = $this->userData($userId , $ralluser['user_id'] , 'user' , 0);
                            $push = isset($dat[0]) ? $dat[0] : null;
                            if($push == null){continue;}
                            array_push($results, $push);
                        }
                    }
                break;

                case 'replies':
                    $alluser=$conn->execute(sprintf("select user_id FROM sessions_qa_likes WHERE vote_type = $reaction and sessions_qa_id = $commentId $cona"));
                    if($alluser->count() > 0)
                    {
                        while($ralluser=$alluser->fetch("assoc"))
                        {
                            $dat = $this->userData($userId , $ralluser['user_id'] , 'user' , 0);
                            $push = isset($dat[0]) ? $dat[0] : null;
                            if($push == null){continue;}
                            array_push($results, $push);
                        }
                    }
                break;

                /*case 'voterlist':
                    $users='';
                    $get_voters = $conn->execute(sprintf("SELECT p.poll_id,u.user_id  FROM `posts_polls` p,`users_polls_options` u WHERE p.`poll_id` = u.poll_id and p.post_id=%s LIMIT %s, %s", $id, secure($offset, 'int', false), secure($codes->MAX_RESULTS, 'int', false)));
                    while ($voter = $get_voters->fetch("assoc")) {
                    if(!$this->blocked($voter['user_id'])){
                        $users.=$voter['user_id'].',';
                    }
                    }
                    $users=trim($users,',');
                    $userlist=$this->get_user_by_ids($users);
                break;
                case 'sessionjoinees':
                    $users='';
                    $my_friends = $conn->execute(sprintf("SELECT GROUP_CONCAT(user_id) AS users FROM `sessions_comment` WHERE status = %s AND sessions_id = %s", secure(1, 'int'), $id)) or _apiError('SQL_ERROR_THROWEN');
                        $user_ids = $my_friends->fetch("assoc");

                        $final_user_ids = "";
                        if ($user_ids['users'] != "") {
                            //$final_user_ids = $user_ids['users'] . ",";
                        }

                        $sessions_attends = $conn->execute(sprintf("SELECT GROUP_CONCAT(user_id) AS users FROM `sessions_joins` WHERE sessions_id = %s limit $offset,$codes->MAX_RESULTS", $id)) or _apiError('SQL_ERROR_THROWEN');
                        $sessions_user_ids = $sessions_attends->fetch("assoc");

                        if ($sessions_user_ids['users'] != "") {
                            $final_user_ids .= $sessions_user_ids['users'].',';
                        }

                    
                    $users=trim($final_user_ids,',');
                    if($users!='')
                    {
                    $userlist=$this->get_user_by_ids($users);
                    }
                    
                break;
                case 'friendattendees':
                    $users='';
                    $alluser=$conn->execute(sprintf("select user_id FROM sessions_attends WHERE sessions_id = %s limit $offset,$codes->MAX_RESULTS", secure($id)));
                    if($alluser->count() > 0)
                    {
                    while($ralluser=$alluser->fetch("assoc"))
                    {
                        $users.=$ralluser['user_id'].',';
                    }
                    $users=trim($users,',');
                    $userlist=$this->get_user_by_ids($users);
                    }
                break;
                case 'totalattendees':
                    $users='';
                    $alluser=$conn->execute(sprintf("select sessions_attends.user_id FROM sessions_attends INNER JOIN sessions ON (sessions_attends.sessions_id = sessions.sessions_id) WHERE sessions_attends.sessions_id = %s and (sessions.status=3 or sessions.status=4) and sessions_attends.status=1 limit $offset,$codes->MAX_RESULTS", secure($id)));
                    if($alluser->count() > 0)
                    {
                    while($ralluser=$alluser->fetch("assoc"))
                    {
                        $users.=$ralluser['user_id'].',';
                    }
                    $users=trim($users,',');
                    $userlist=$this->get_user_by_ids($users);
                    }
                break;
                case 'attendees':
                    $users='';
                    $alluser=$conn->execute(sprintf("select sessions_attends.user_id FROM sessions_attends INNER JOIN sessions ON (sessions_attends.sessions_id = sessions.sessions_id) WHERE sessions_attends.sessions_id = %s and (sessions.status=1 or sessions.status=2) limit $offset,$codes->MAX_RESULTS", secure($id)));
                    if($alluser->count() > 0)
                    {
                    while($ralluser=$alluser->fetch("assoc"))
                    {
                        $users.=$ralluser['user_id'].',';
                    }
                    $users=trim($users,',');
                    $userlist=$this->get_user_by_ids($users);
                    }
                break;
                case 'venue_attendees':
                    $query_attendees = $conn->execute(sprintf("SELECT sessions_attends.user_id FROM sessions INNER JOIN sessions_attends ON (sessions_attends.sessions_id = sessions.sessions_id AND sessions_attends.status=1) WHERE FIND_IN_SET(%s,sessions.venue) AND sessions.status=4 GROUP BY sessions_attends.user_id limit $offset,$codes->MAX_RESULTS", secure($id)));
                    if($query_attendees->count()){
                                while($attendes = $query_attendees->fetch("assoc")){
                                    if(!empty($this->get_user_by_id($attendes['user_id'])[0]) && $this->get_user_by_id($attendes['user_id'])[0]!='')
                                    {
                                    $userlist[] = $this->get_user_by_id($attendes['user_id'])[0];
                                    }
                                }
                            }
                break;
                case 'thanks':
                    $users='';
                    $alluser=$conn->execute(sprintf("select user_id FROM sessions_thanks WHERE session_id = %s limit $offset,$codes->MAX_RESULTS", secure($id)));
                    if($alluser->count() > 0)
                    {
                    while($ralluser=$alluser->fetch("assoc"))
                    {
                        $users.=$ralluser['user_id'].',';
                    }
                    $users=trim($users,',');
                    $userlist=$this->get_user_by_ids($users);
                    }
                break;
                case 'userfollowers':
                    $user_s=$this->get_followers_ids($this->_data['user_id']);
                    $users='';
                    foreach($user_s as $uk=>$uval)
                    {
                        $users.=$uval.',';
                    }
                    $users=trim($users,',');
                    $userlist=$this->get_user_by_ids($users);
                break;
                case 'interestfollowers':
                    $userlists = $this->who_interest_followed( array('interest_id' => $id,'offset'=>$offset) );
                    $userlist=$userlists['users'];
                break;
                case 'venuefollowers':
                    $users='';
                    $alluser=$conn->execute(sprintf("SELECT * FROM `users_venue` WHERE venue_id = %s limit $offset,$codes->MAX_RESULTS", secure($id)));
                    if($alluser->count() > 0)
                    {
                    while($ralluser=$alluser->fetch("assoc"))
                    {
                        $users.=$ralluser['user_id'].',';
                    }
                    $users=trim($users,',');
                    $userlist=$this->get_user_by_ids($users);
                    }
                break;
                case 'commentreply':
                    $users='';
                    $get_comment_likes = $conn->execute( sprintf( "SELECT * FROM `posts_comments_likes` WHERE comment_id = %s limit $offset,$codes->MAX_RESULTS", $id));
                    if($get_comment_likes->count()){
                        while($likers = $get_comment_likes->fetch("assoc")){
                            if(!$this->blocked($likers['user_id'])){
                                $users.=$likers['user_id'].',';
                            }
                        }
                        $users=trim($users,',');
                        $userlist=$this->get_user_by_ids($users);
                    }
                break;
                case 'sessioncommentreply':
                    $users='';
                    $reply_like_user_query = $conn->execute(sprintf("SELECT * FROM sessions_comment_likes WHERE sessions_comment_id = %s limit $offset,$codes->MAX_RESULTS" ,secure($id)));
            while($liked_user = $reply_like_user_query->fetch("assoc")){
                        if(!$this->blocked($liked_user['user_id'])){
                                $users.=$liked_user['user_id'].',';
                            }
                    }
                    $users=trim($users,',');
                    $userlist=$this->get_user_by_ids($users);
                break;
                case 'sessiondiscussionreply':
                    $users='';
                    $reply_like_user_query = $conn->execute(sprintf("SELECT * FROM sessions_discussion_likes WHERE sessions_discussion_id = %s limit $offset,$codes->MAX_RESULTS" ,secure($id)));
            while($liked_user = $reply_like_user_query->fetch("assoc")){
                        if(!$this->blocked($liked_user['user_id'])){
                                $users.=$liked_user['user_id'].',';
                            }
                    }
                    $users=trim($users,',');
                    $userlist=$this->get_user_by_ids($users);
                break;
                case 'articlecommentreply':
                    $users='';
                    $reply_like_user_query = $conn->execute(sprintf("SELECT * FROM articles_discussion_likes WHERE articles_discussion_id = %s limit $offset,$codes->MAX_RESULTS" ,secure($id)));
            while($liked_user = $reply_like_user_query->fetch("assoc")){
                        if(!$this->blocked($liked_user['user_id'])){
                                $users.=$liked_user['user_id'].',';
                            }
                    }
                    $users=trim($users,',');
                    $userlist=$this->get_user_by_ids($users);
                break;
                case 'answerreply':
                    $users='';
                    $reply_like_user_query = $conn->execute(sprintf("SELECT * FROM sessions_qa_likes WHERE sessions_qa_id = %s limit $offset,$codes->MAX_RESULTS" ,secure($id)));
            while($liked_user = $reply_like_user_query->fetch("assoc")){
                        if(!$this->blocked($liked_user['user_id'])){
                                $users.=$liked_user['user_id'].',';
                            }
                    }
                    $users=trim($users,',');
                    $userlist=$this->get_user_by_ids($users);
                break;
                case 'organisation_followers':
                $users='';
                    $reply_like_user_query = $conn->execute(sprintf("SELECT * FROM user_organizer WHERE organizer = %s limit $offset,$codes->MAX_RESULTS" ,secure($id)));
            while($liked_user = $reply_like_user_query->fetch("assoc")){
                        if(!$this->blocked($liked_user['user_id'])){
                                $users.=$liked_user['user_id'].',';
                            }
                    }
                    $users=trim($users,',');
                    if($users!=''){
                    $userlist=$this->get_user_by_ids($users);
                    }else{$userlist=[];}
                break;*/
                default:
                    throw new Exception('Reaction type is required');
                break;
                
            }
            return $results;
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
        
        
    }

    //used everywhere for getting user data 
    public function userData($uid , $cid , $user_type , $communityId){
        try{
            $conn = ConnectionManager::get('default');
            $mi = new MutualInterestsDao;
            $points = new PointsDao;
            $codes = new Codes;
            $arr = array();
            if($user_type == 'organisation' || $user_type == 'organization' || $user_type == 'Organisation' || $user_type == 'Organization'){
                $sql = $conn->execute(sprintf("SELECT * FROM `organization` where id = %s", $cid));
                if($res = $sql->fetch('assoc')){
                    //return ["message"=>'organisation not found'];
                }else{return ["message"=>'organisation not found'];}
                if($res['Logo'] != '' && $res['Logo'] != null){$res['Logo'] = $this->getImgFullPath($res['Logo'] ,1);}
                $res['logo'] = $res['Logo'];
                unset($res['Logo']);
                if($res['cover_photo'] != '' && $res['cover_photo'] != null){$res['cover_photo'] = $this->getImgFullPath($res['cover_photo'] ,1);}
                $res["i_follow"] = 0;
                
                $sql = $conn->execute(sprintf("SELECT * FROM `users_organization` where user_id = %s and organization_id = %s",$uid, $cid));
                if($re = $sql->fetch('assoc')){$res['i_follow'] = 1;}
                array_push($arr, $res);
            }elseif($user_type == 'venue' || $user_type == 'Venue' || $user_type == 'institution' || $user_type == 'Institution') {
                $sql = $conn->execute(sprintf("SELECT * FROM `venue` where venue_id = %s", $cid));
                if(!$res = $sql->fetch('assoc')){return ["message"=>'organisation not found'];}
                if($res['cover_photo'] != '' && $res['cover_photo'] != null){$res['cover_photo'] = $this->getImgFullPath($res['cover_photo'] ,1);}
                $res['logo'] = $res['cover_photo'];
                $res['cover_photo'] = '';
                $res["i_follow"] = 0;
                $sql = $conn->execute(sprintf("SELECT * FROM `users_venue` where user_id = %s and venue_id = %s",$uid, $cid));
                if($re = $sql->fetch('assoc')){$res['i_follow'] = 1;}
                array_push($arr, $res);
            }elseif($user_type == 'user' || $user_type == 'User' ){
                $sql = sprintf("SELECT user_id, user_email, user_fullname, user_firstname, user_lastname, user_gender, user_picture, user_work, user_work_title, user_work_place from users where user_id = %s", $cid);
                    Log::debug("SQL : ".$sql);
                    $s = $conn->execute($sql);
                    if($r=$s->fetch('assoc')){
                        $r['user_picture'] = $this->getImgFullPath($r['user_picture'] ,1);
                        //connection variable add here
                        $r['i_follow'] = 0;
                        $r['interest_percent'] = "".$this->round($mi->get_mutual_interests($uid , $cid , $codes->RC_CONST) , 2);
                        //$r['interest_percent'] = "".round(2.575552 , 3);
                        $r['points_data'] = $points->getPointsAndBadgesByUserid($cid);
                        $f = $conn->execute(sprintf("SELECT * from `followings` where user_id = %s and  following_id = %s" , $uid , $cid));
                        if($a = $f->fetch("assoc")){$r['i_follow'] = 1; }
                        array_push($arr, $r);
                    }
            }elseif($user_type == 'community' || $user_type == 'Community'){
                $com = $conn->execute(sprintf("SELECT * FROM `communities` where id = %s"  , $communityId));
                if($r = $com->fetch("assoc")){
                    if($r['picture'] != null and $r['picture'] != ''){$r['picture'] = $this->getImgFullPath($r['picture'] , 1);}
                    if($r['cover_picture']!= null and $r['cover_picture'] != ''){$r['cover_picture'] = $this->getImgFullPath($r['cover_picture'] , 1);}
                    //$result['community_data'] = $r;
                    $r['i_follow'] = 0;
                    $f = $conn->execute(sprintf("SELECT * from `communities_members` where user_id = %s and  community_id = %s" , $uid , $cid));
                    if($a = $f->fetch("assoc")){$r['i_follow'] = (int)$a['role']; }
                    $ut = 'user';
                    $u = $this->userData($uid , $cid , $ut , $communityId);
                    array_push($arr, $u[0]);
                    array_push($arr, $r);
                }else{return ["message" => 'something went wrong'];}
            }else{
                return $arr;
            }
            return $arr;
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }
    private function getImgFullPath($img = '', $newPath = 0){
        $codes = new Codes;
        return !empty($img) ?$codes->SYSTEM_URL.'/'.($newPath?'api/':'').$codes->UPLOADS_DIRECTORY.'/'.$img:'';
    }
    public function round($float , $to){
        $temp = $float * pow(10  , $to);
        $temp = (int)$temp;
        //return $temp;
        return $temp / pow(10  , $to);
        
    }

    public function mysql_real_escape_string($inp) {
        if(is_array($inp))
            return array_map(__METHOD__, $inp);

        if(!empty($inp) && is_string($inp)) {
            return str_replace(array('\\', "\0", "\n", "\r", "'", '"', "\x1a"), array('\\\\', '\\0', '\\n', '\\r', "\\'", '\\"', '\\Z'), $inp);
        }

        return $inp;
    }
    public function getData($contentId , $type , $args , $sql = ''){
        try{
            $conn = ConnectionManager::get("default");
            $arr  = array();
            $art = new ArticlesDao;
            extract($args);
            switch($type){
                case "articles":
                    if($sql != ''){
                        $st = $conn->execute($sql);
                        while($res = $st->fetch('assoc')){
                            $res['cover_photo'] = $this->getImgFullPath($res['cover_photo'] ,1);
                            $dat = $this->userData($args['userid'] , $res['created_by'] , $res['user_type'] , $res['community_id']);
                            $res['created_by'] = (isset($dat[0])? $dat[0]  : null);
                            $res['community_data'] = (isset($dat[1])? $dat[1] : null);
                            $res['points'] = $art->articlePoints('get' , 0 , $res['articles_id']);
                            array_push($arr , $res);
                        }
                        return $arr;
                        break;
                    }else{
                        $st = $conn->execute("SELECT * from articles where articles_id = $contentId");
                        if($res = $st->fetch('assoc')){
                            $res['cover_photo'] = $this->getImgFullPath($res['cover_photo'] ,1);
                            $dat = $this->userData($args['userid'] , $res['created_by'] , $res['user_type'] , $res['community_id']);
                            $res['created_by'] = (isset($dat[0])? $dat[0]  : null);
                            $res['community_data'] = (isset($dat[1])? $dat[1] : null);
                            $res['points'] = $art->articlePoints('get' , 0 , $res['articles_id']);
                            return $res;
                        }
                        break;
                    }
                    break;
                case "quizzes":
                    if($sql != ''){
                        $st = $conn->execute($sql);
                        while($res = $st->fetch('assoc')){
                            $res['picture'] = $this->getImgFullPath($res['picture'] ,1);
                            $dat = $this->userData($args['userid'] , $res['creator_id'] , $res['user_type'] , $res['community_id']);
                            $res['created_by'] = (isset($dat[0])? $dat[0] : null);
                            $res['community_data'] = (isset($dat[1])? $dat[1] : null);
                            $res['quizInterestArr'] = array();
                            if($res['interests']){

                                // checking interests
                                $sql=sprintf("SELECT interest_id, `text` as interest_name FROM interest_mst WHERE FIND_IN_SET(interest_id, '%s')", $res['interests']);

                                Log::debug("SQL : ".$sql);
                                $interestStmt = $conn->execute($sql);

                                while($interestResult = $interestStmt->fetch("assoc"))
                                    array_push($res['quizInterestArr'], $interestResult);

                            }
                            $res['participatingFriendsCount'] = 0;
                            $res['participatingFriend'] = null;
                            $sql="SELECT users.user_id, users.user_work, users.user_work_title, users.user_work_place, users.user_name, users.user_fullname, users.user_firstname,user_lastname, users.user_gender, users.user_picture, qp.is_attained, qp.score, qp.id as qp_id FROM friends INNER JOIN users ON ((friends.user_one_id = users.user_id AND friends.user_one_id != $userid) OR (friends.user_two_id = users.user_id AND friends.user_two_id != $userid)) left join quizzes_participants as qp on qp.id =(SELECT id FROM quizzes_participants WHERE is_last = 1 AND quiz_id = '$res[id]' AND user_id = users.user_id limit 0, 1) WHERE friends.status = 1 AND (user_one_id = $userid OR user_two_id = $userid) having qp_id > 0 ";
                            Log::debug("SQL : ".$sql);
                            $fellowsStmt = $conn->execute($sql);

                            while($fellowResult = $fellowsStmt->fetch("assoc")) {
                                if(empty($res['participatingFriend'])){

                                    $fellowResult['user_picture'] = $this->getImgFullPath($fellowResult['user_picture']);
                                    $fellowResult['is_attained'] = $fellowResult['is_attained']?1:0;
                                    $fellowResult['score'] = $fellowResult['score']?$fellowResult['score']:0;
                                    $res['participatingFriend'] = $fellowResult;
                                }
                                $res['participatingFriendsCount']++;
                            }
                            $sql = sprintf("select * from quizzes_participants where user_id = $userid and quiz_id = %s" , $res['id']);
                            $ip = $conn->execute($sql);
                            $res['qp_id'] = '0';
                            $res['is_attained'] = '0';
                            $res['is_submitted'] = '0';
                            $res['is_last'] = "0";
                            $res['score'] = '0';
                            $res['goingToParticipate'] = 0;
                            if($p = $ip->fetch('assoc')){
                                $res['goingToParticipate'] = ($p['id'] > 0)?1:0;
                                $res['qp_id'] = $p['id'];
                                $res['score'] = $p['score'];
                                $res['is_attained'] = $p['is_attained'];
                                $res['is_submitted'] = $p['is_submitted'];
                                $res['is_last'] = $p['is_last'];
                            }
                            $res['questionCount'] = 0;
                            $qc = $conn->execute(sprintf("select * from quizzes_questions where quiz_id = %s", $res['id']));
                            $res['questionCount'] = $qc->count();
                            array_push($arr , $res);
                        }
                        return $arr;
                        break;
                    }else{

                        $sql = "SELECT *,(case when DATE_ADD(start_time, INTERVAL `live_duration` second) < '".$date."' then 'after live' when start_time > '".$date."' then 'before live' else 'live' end) as quiz_status FROM quizzes where id = ".$contentId."";


                        $st = $conn->execute($sql);
                        if($res = $st->fetch('assoc')){
                            $res['picture'] = $this->getImgFullPath($res['picture'] ,1);
                            $dat = $this->userData($args['userid'] , $res['creator_id'] , $res['user_type'] , $res['community_id']);
                            $res['created_by'] = (isset($dat[0])? $dat[0] : null);
                            $res['community_data'] = (isset($dat[1])? $dat[1] : null);
                            $res['quizInterestArr'] = array();
                            if($res['interests'] != null && $res['interests'] != ''){

                                // checking interests
                                $sql=sprintf("SELECT interest_id, `text` as interest_name FROM interest_mst WHERE FIND_IN_SET(interest_id, '%s')", $res['interests']);

                                Log::debug("SQL : ".$sql);
                                $interestStmt = $conn->execute($sql);

                                while($interestResult = $interestStmt->fetch("assoc"))
                                    array_push($res['quizInterestArr'], $interestResult);

                            }
                            $res['participatingFriendsCount'] = 0;
                            $res['participatingFriend'] = null;
                            $sql="SELECT users.user_id, users.user_work, users.user_work_title, users.user_work_place, users.user_name, users.user_fullname, users.user_firstname,user_lastname, users.user_gender, users.user_picture, qp.is_attained, qp.score, qp.id as qp_id FROM friends INNER JOIN users ON ((friends.user_one_id = users.user_id AND friends.user_one_id != $userid) OR (friends.user_two_id = users.user_id AND friends.user_two_id != $userid)) left join quizzes_participants as qp on qp.id =(SELECT id FROM quizzes_participants WHERE is_last = 1 AND quiz_id = '$res[id]' AND user_id = users.user_id limit 0, 1) WHERE friends.status = 1 AND (user_one_id = $userid OR user_two_id = $userid) having qp_id > 0 ";
                            Log::debug("SQL : ".$sql);
                            $fellowsStmt = $conn->execute($sql);

                            while($fellowResult = $fellowsStmt->fetch("assoc")) {
                                if(empty($res['participatingFriend'])){

                                    $fellowResult['user_picture'] = $this->getImgFullPath($fellowResult['user_picture']);
                                    $fellowResult['is_attained'] = $fellowResult['is_attained']?1:0;
                                    $fellowResult['score'] = $fellowResult['score']?$fellowResult['score']:0;
                                    $res['participatingFriend'] = $fellowResult;
                                }
                                $res['participatingFriendsCount']++;
                            }
                            $sql = sprintf("select * from quizzes_participants where user_id = $userid and quiz_id = %s" , $res['id']);
                            $ip = $conn->execute($sql);
                            $res['qp_id'] = '0';
                            $res['is_attained'] = '0';
                            $res['is_submitted'] = '0';
                            $res['is_last'] = "0";
                            $res['score'] = '0';
                            $res['goingToParticipate'] = 0;
                            if($p = $ip->fetch('assoc')){
                                $res['goingToParticipate'] = ($p['id'] > 0)?1:0;
                                $res['qp_id'] = $p['id'];
                                $res['score'] = $p['score'];
                                $res['is_attained'] = $p['is_attained'];
                                $res['is_submitted'] = $p['is_submitted'];
                                $res['is_last'] = $p['is_last'];
                            }
                            $res['questionCount'] = 0;
                            $qc = $conn->execute(sprintf("select * from quizzes_questions where quiz_id = %s", $res['id']));
                            $res['questionCount'] = $qc->count();
                            return $res;
                        }else{return $id;}
                        //return $ans;
                        break;
                    }
                    break;

                case "debates":
                    if($sql != ''){
                        $st = $conn->execute($sql);
                        while($res = $st->fetch('assoc')){
                            $res['picture'] = $this->getImgFullPath($res['picture'] ,1);
                            $dat = $this->userData($args['userid'] , $res['creator_id'] , $res['user_type'] , $res['community_id']);
                            $res['created_by'] = (isset($dat[0])? $dat[0] : null);
                            $res['community_data'] = (isset($dat[1])? $dat[1] : null);
                            //change start
                            $res['debateInterestArr'] = array();
                            if($res['interests']){

                                // checking interests
                                $sql=sprintf("SELECT interest_id, `text` as interest_name FROM interest_mst WHERE FIND_IN_SET(interest_id, '%s')", $res['interests']);

                                Log::debug("SQL : ".$sql);
                                $interestStmt = $conn->execute($sql);

                                while($interestResult = $interestStmt->fetch("assoc"))
                                    array_push($res['debateInterestArr'], $interestResult);

                            }
                            $res['participatingFriendsCount'] = 0;
                            $res['participatingFriend'] = null;
                            $sql="SELECT users.user_id, users.user_work, users.user_work_title, users.user_work_place, users.user_name, users.user_fullname, users.user_firstname,user_lastname, users.user_gender, users.user_picture, qp.is_attained, qp.score, qp.id as qp_id FROM friends INNER JOIN users ON ((friends.user_one_id = users.user_id AND friends.user_one_id != $userid) OR (friends.user_two_id = users.user_id AND friends.user_two_id != $userid)) left join debates_participants as qp on qp.id =(SELECT id FROM debates_participants WHERE debate_id = '$res[id]' AND user_id = users.user_id limit 0, 1) WHERE friends.status = 1 AND (user_one_id = $userid OR user_two_id = $userid) having qp_id > 0 ";
                            Log::debug("SQL : ".$sql);
                            $fellowsStmt = $conn->execute($sql);

                            while($fellowResult = $fellowsStmt->fetch("assoc")) {
                                if(empty($res['participatingFriend'])){

                                    $fellowResult['user_picture'] = $this->getImgFullPath($fellowResult['user_picture']);
                                    $fellowResult['is_attained'] = $fellowResult['is_attained']?1:0;
                                    $fellowResult['score'] = $fellowResult['score']?$fellowResult['score']:0;
                                    $res['participatingFriend'] = $fellowResult;
                                }
                                $res['participatingFriendsCount']++;
                            }
                            $sql = sprintf("select * from debates_participants where user_id = $userid and debate_id = %s" , $res['id']);
                            $ip = $conn->execute($sql);
                            $res['dp_id'] = '0';
                            $res['is_attained'] = '0';
                            $res['is_submitted'] = '0';
                            $res['is_last'] = "0";
                            $res['prosCount'] = 0;
                            $res['consCount'] = 0;
                            $res['prosConsCount'] = 0;
                            //$res['score'] = '0';
                            $res['goingToParticipate'] = 0;
                            if($p = $ip->fetch('assoc')){
                                $res['goingToParticipate'] = ($p['id'] > 0)?1:0;
                                $res['dp_id'] = $p['id'];
                                //$res['score'] = $p['score'];
                                $res['is_attained'] = $p['is_attained'];
                                $res['is_submitted'] = $p['is_submitted'];
                                //$res['is_last'] = $p['is_last'];
                            }
                            $res['questionCount'] = 0;
                            $sql = $conn->execute(sprintf("SELECT * from debates_discussions where debate_id = %s", $res['id']));
                            while($r = $sql->fetch('assoc')){
                                if($r['is_pro'] == 1 || $r['is_pro'] == '1'){
                                    $res['prosCount']++;
                                }else{
                                    $res['consCount']++;
                                }
                            }
                            $res['prosConsCount'] = $res['consCount'] + $res['prosCount'];
                            //change end
                            array_push($arr , $res);
                        }
                        return $arr;
                        break;
                    }else{
                        $st = $conn->execute("SELECT *,(case when DATE_ADD(start_time, INTERVAL `live_duration` second) < '".$date."' then 'after live' when start_time > '".$date."' then 'before live' else 'live' end) as debate_status FROM debates where id = $contentId");
                        if($res = $st->fetch('assoc')){
                            $res['picture'] = $this->getImgFullPath($res['picture'] ,1);
                            $dat = $this->userData($args['userid'] , $res['creator_id'] , $res['user_type'] , $res['community_id']);
                            $res['created_by'] = (isset($dat[0])? $dat[0] : null);
                            $res['community_data'] = (isset($dat[1])? $dat[1] : null);
                            //change start
                            $res['debateInterestArr'] = array();
                            if($res['interests']){

                                // checking interests
                                $sql=sprintf("SELECT interest_id, `text` as interest_name FROM interest_mst WHERE FIND_IN_SET(interest_id, '%s')", $res['interests']);

                                Log::debug("SQL : ".$sql);
                                $interestStmt = $conn->execute($sql);

                                while($interestResult = $interestStmt->fetch("assoc"))
                                    array_push($res['debateInterestArr'], $interestResult);

                            }
                            $res['participatingFriendsCount'] = 0;
                            $res['participatingFriend'] = null;
                            $sql="SELECT users.user_id, users.user_work, users.user_work_title, users.user_work_place, users.user_name, users.user_fullname, users.user_firstname,user_lastname, users.user_gender, users.user_picture, qp.is_attained, qp.score, qp.id as qp_id FROM friends INNER JOIN users ON ((friends.user_one_id = users.user_id AND friends.user_one_id != $userid) OR (friends.user_two_id = users.user_id AND friends.user_two_id != $userid)) left join debates_participants as qp on qp.id =(SELECT id FROM debates_participants WHERE debate_id = '$res[id]' AND user_id = users.user_id limit 0, 1) WHERE friends.status = 1 AND (user_one_id = $userid OR user_two_id = $userid) having qp_id > 0 ";
                            Log::debug("SQL : ".$sql);
                            $fellowsStmt = $conn->execute($sql);

                            while($fellowResult = $fellowsStmt->fetch("assoc")) {
                                if(empty($res['participatingFriend'])){

                                    $fellowResult['user_picture'] = $this->getImgFullPath($fellowResult['user_picture']);
                                    $fellowResult['is_attained'] = $fellowResult['is_attained']?1:0;
                                    $fellowResult['score'] = $fellowResult['score']?$fellowResult['score']:0;
                                    $res['participatingFriend'] = $fellowResult;
                                }
                                $res['participatingFriendsCount']++;
                            }
                            $sql = sprintf("select * from debates_participants where user_id = $userid and debate_id = %s" , $res['id']);
                            $ip = $conn->execute($sql);
                            $res['dp_id'] = '0';
                            $res['is_attained'] = '0';
                            $res['is_submitted'] = '0';
                            $res['is_last'] = "0";
                            $res['prosCount'] = 0;
                            $res['consCount'] = 0;
                            $res['prosConsCount'] = 0;
                            //$res['score'] = '0';
                            $res['goingToParticipate'] = 0;
                            if($p = $ip->fetch('assoc')){
                                $res['goingToParticipate'] = ($p['id'] > 0)?1:0;
                                $res['dp_id'] = $p['id'];
                                //$res['score'] = $p['score'];
                                $res['is_attained'] = $p['is_attained'];
                                $res['is_submitted'] = $p['is_submitted'];
                                $res['is_last'] = $p['is_last'];
                            }
                            $res['questionCount'] = 0;
                            $sql = $conn->execute(sprintf("SELECT * from debates_discussions where debate_id = %s", $res['id']));
                            while($r = $sql->fetch('assoc')){
                                if($r['is_pro'] == 1 || $r['is_pro'] == '1'){
                                    $res['prosCount']++;
                                }else{
                                    $res['consCount']++;
                                }
                            }
                            $res['prosConsCount'] = $res['consCount'] + $res['prosCount'];
                            //change end
                            
                            return $res;
                        }
                    }
                    break;
                case "quickbits":
                    if($sql != ""){
                        $st = $conn->execute($sql);
                        while($res = $st->fetch("assoc")){
                            //$res['picture'] = $this->getImgFullPath($res['picture'] ,1);
                            $temp = array();
                            if($res['interests']){

                                // checking interests
                                $sql=sprintf("SELECT interest_id, `text` as interest_name FROM interest_mst WHERE FIND_IN_SET(interest_id, '%s')", $res['interests']);

                                Log::debug("SQL : ".$sql);
                                $interestStmt = $conn->execute($sql);

                                while($interestResult = $interestStmt->fetch("assoc"))
                                    array_push($temp, $interestResult);

                            }
                            $res['quickBitInterestArr'] = $temp;
                            $res['cards'] = array();
                            $sql = sprintf("select * from quickbits_cards where quickbit_id = %s", $res['id']);
                            $s = $conn->execute($sql);
                            while($a = $s->fetch('assoc')){
                                if($a['picture'] != ''){
                                    $a['picture'] = $this->getImgFullPath($a['picture'],1);
                                }
                                array_push($res['cards'], $a);
                            }
                            $dat = $this->userData($args['userid'] , $res['creator_id'] , $res['user_type'] , $res['community_id']);
                            $res['created_by'] = (isset($dat[0])? $dat[0] : null);
                            $res['community_data'] = (isset($dat[1])? $dat[1] : null);
                            array_push($arr , $res);
                        }
                        return $arr;
                        break;
                    }else{
                        $st = $conn->execute("SELECT * from quickbits where id = $contentId");
                        if($res = $st->fetch("assoc")){
                            //$res['picture'] = $this->getImgFullPath($res['picture'] ,1);
                            $temp = array();
                            if($res['interests']){

                                // checking interests
                                $sql=sprintf("SELECT interest_id, `text` as interest_name FROM interest_mst WHERE FIND_IN_SET(interest_id, '%s')", $res['interests']);

                                Log::debug("SQL : ".$sql);
                                $interestStmt = $conn->execute($sql);

                                while($interestResult = $interestStmt->fetch("assoc"))
                                    array_push($temp, $interestResult);

                            }
                            $res['quickBitInterestArr'] = $temp;
                            $res['cards'] = array();
                            $sql = sprintf("select * from quickbits_cards where quickbit_id = %s", $res['id']);
                            $s = $conn->execute($sql);
                            while($a = $s->fetch('assoc')){
                                if($a['picture'] != ''){
                                    $a['picture'] = $this->getImgFullPath($a['picture'],1);
                                }
                                array_push($res['cards'], $a);
                            }
                            $dat = $this->userData($args['userid'] , $res['creator_id'] , $res['user_type'] , $res['community_id']);
                            $res['created_by'] = (isset($dat[0])? $dat[0] : null);
                            $res['community_data'] = (isset($dat[1])? $dat[1] : null);
                            return $res;
                        }
                        return $res;
                        break;
                    }
                    break;
                case "community":
                    if($sql != ''){
                        $st = $conn->execute($sql);
                        while($res = $st->fetch("assoc")){
                            $res['picture'] = ($res['picture'] != '' ? $this->getImgFullPath($res['picture'],1) : "");
                            $res['cover_picture'] = ($res['cover_picture'] != '' ? $this->getImgFullPath($res['cover_picture'],1) : "");
                            $res["i_join"] = '0';
                            $res['follower_count'] = $conn->execute(sprintf("SELECT * FROM communities_members WHERE community_id = %s and role != 4 and role != 5", $res['id']))->count();
                            $sql = $conn->execute(sprintf("SELECT * FROM communities_members WHERE user_id = $userid and community_id = %s and role != 4", $res['id']));
                            if($r = $sql->fetch('assoc')){
                                $res['i_join'] = $r['role'];
                            }
                            array_push($arr , $res);
                        }
                        return $arr;
                    }else{
                        $st = $conn->execute("SELECT * from communities where id = $contentId");
                        if($res = $st->fetch("assoc")){
                            $res['picture'] = ($res['picture'] != '' ? $this->getImgFullPath($res['picture'],1) : "");
                            $res['cover_picture'] = ($res['cover_picture'] != '' ? $this->getImgFullPath($res['cover_picture'],1) : "");
                            $res["i_join"] = '0';
                            $res['follower_count'] = $conn->execute(sprintf("SELECT * FROM communities_members WHERE community_id = %s and role != 4 and role != 5", $res['id']))->count();
                            $sql = $conn->execute(sprintf("SELECT * FROM communities_members WHERE user_id = $userid and community_id = %s and role != 4", $res['id']));
                            if($r = $sql->fetch('assoc')){
                                $res['i_join'] = $r['role'];
                            }
                            return $res;
                        }
                    }
                    break;
                case "interests":
                    if($sql != ""){
                        $st = $conn->execute($sql);
                        while($res = $st->fetch("assoc")){
                            $res['follower_count'] = 0;
                            $res['i_follow'] = 0;
                            $res['image'] = ($res['image'] != '' ? $this->getImgFullPath($res['image']) : "");
                            $sql = $conn->execute(sprintf("SELECT * FROM user_interest where interest = %s", $res['interest_id']));
                            if($a = $sql->fetch("assoc")){
                                if($a['user_id'] == $userid){$res['i_follow'] = 1;}
                                $res['follower_count']++;
                            }
                            array_push($arr , $res);
                        }
                        return $arr;
                    }else{
                        $st = $conn->execute("SELECT * FROM interest_mst where interest_id = $contentId");
                        if($res = $st->fetch("assoc")){
                            $res['follower_count'] = 0;
                            $res['i_follow'] = 0;
                            $res['image'] = ($res['image'] != '' ? $this->getImgFullPath($res['image']) : "");
                            $sql = $conn->execute(sprintf("SELECT * FROM user_interest where interest = %s", $res['interest_id']));
                            if($a = $sql->fetch("assoc")){
                                if($a['user_id'] == $userid){$res['i_follow'] = 1;}
                                $res['follower_count']++;
                            }
                            return $res;
                        }
                    }
                    break;
                case "sessions":
                    if($sql != ""){
                        $st = $conn->execute($sql);
                        while($r = $st->fetch("assoc")){
                            $r['cover_photo'] = ($r['cover_photo'] != null || $r['cover_photo'] != '' ? $this->getImgFullPath($r['cover_photo']) : "");
                            $r['creator_id'] = $r['created_by'];
                            $dat = $this->userData($args['userid'] , $r['created_by'] , $r['user_type'] , $r['community_id']);
                            $r['created_by'] = (isset($dat[0])? $dat[0] : null);
                            $r['community_data'] = (isset($dat[1])? $dat[1] : null);
                            $r['venue_id'] = '0';
                            $r['topics'] = explode(",", $r['topics']);
                            $r['venue_name'] = '';
                            $r['Date_of_session'] = '';
                            $sql = sprintf("select * from venue_slot_book where session_id = %s" , $r['sessions_id']);
                            $vd = $conn->execute($sql);
                            if($v =$vd->fetch('assoc')){
                                $r['venue_id'] = $v['venue_id'];
                                $r['Date_of_session'] = $v['date'];
                                if($v['venue_id'] != -1 || $v['venue_id'] != '-1'){
                                    $sql = $conn->execute(sprintf("select venue_name from venue where venue_id = %s" , $r['venue_id']));
                                    if($a = $sql->fetch('assoc')){$r['venue_name'] = $a['venue_name'];}
                                }
                            }

                            $press = explode(",", $r['presentors']);
                            //$r['presentors'] = null;
                            $temp = array();
                            foreach ($press as $pres ) {
                                if($pres == ''){break;}
                                $dat = $this->userData($args['userid'] , $pres , "user" , 0);
                                $t = isset($dat[0])? $dat[0] : null;
                                if($t != null){
                                    array_push($temp , $t);
                                }
                            }
                            $r['presentors'] = $temp;
                            $sd = $conn->execute(sprintf("SELECT * from sessions_discussion where sessions_id = %s and parent_discussion_id = 0", $r['sessions_id']));
                            $r["discussions"] = $sd->count();
                            $r['session_attend_friends'] = 0;
                            $r['i_attend'] = 0;
                            $sa = $conn->execute(sprintf("SELECT sa.user_id as uid FROM `sessions_attends` as sa inner join `friends` as fr on (fr.user_one_id = sa.user_id) or (fr.user_two_id = sa.user_id)  where sessions_id = %s and fr.status = 1 and (fr.user_one_id = $userid or fr.user_two_id = $userid)" ,$r['sessions_id']));
                            while($sac = $sa->fetch("assoc")){
                                if($sac['uid'] == $userid){$r['i_attend']=1;continue;}
                                $r['session_attend_friends']++;
                            }
                            $r['i_thank'] = null;
                            //reactions
                            $react = array();
                            $rname = array();
                            $reactions = $conn->execute(sprintf("SELECT * from `reactions` "));
                            while($a = $reactions->fetch('assoc')){
                                $str = '' . $a['id'] . '';
                                //$react[$str] = 0;
                                array_push($react, 0);
                                array_push($rname, $a['name']);


                            }
                            $count = count($rname);
                            //$pc = $conn->execute(sprintf("SELECT * from `posts` where post_type = 'article' and article_id = %s"  , $articlesId));
                            
                            //if(!$p = $pc->fetch("assoc")){return "no post found";}
                            $l = $conn->execute(sprintf("SELECT * from `session_likes` where session_id = %s"  , $r['sessions_id']));
                            $t = 0;
                            while($a = $l->fetch("assoc")){
                                $t = $t + 1;
                                if($a['user_id'] == $userid){$r['i_thank'] = $a['reaction_type'];}
                                $str = $a['reaction_type'];
                                $react[$str] = $react[$str] + 1;
                            }
                            $fr = array();
                            for ($i=0; $i < $count; $i++) { 
                                # code...
                                $fr[$rname[$i]] = $react[$i];
                            }

                            $r['reactions'] = $fr;
                            $r['total_likes'] = $t;
                            $sql = $conn->execute(sprintf("SELECT * from sessions_qa where sessions_id = %s and parent_question_id = 0 " , $r['sessions_id']));
                            $r['total_questions'] = $sql->count();
                            $parray = array();
                            if($r['event_photos'] != null || $r['event_photos'] != ''){
                                $temp = explode(",", $r['event_photos']);
                                foreach($temp as $i){
                                    $i = $this->getImgFullPath($i , 1);
                                    array_push($parray , $i);
                                }
                            }
                            $r['event_photos'] = $parray;

                            array_push($arr , $r);
                        }
                        return $arr;
                    }else{
                        $st = $conn->execute("SELECT * from sessions where sessions_id = $contentId");
                        while($r = $st->fetch("assoc")){
                            $r['cover_photo'] = ($r['cover_photo'] != null || $r['cover_photo'] != '' ? $this->getImgFullPath($r['cover_photo']) : "");
                            $r['creator_id'] = $r['created_by'];
                            $dat = $this->userData($args['userid'] , $r['created_by'] , $r['user_type'] , $r['community_id']);
                            $r['created_by'] = (isset($dat[0])? $dat[0] : null);
                            $r['community_data'] = (isset($dat[1])? $dat[1] : null);
                            $r['venue_id'] = '0';
                            $r['topics'] = explode(",", $r['topics']);
                            $r['venue_name'] = '';
                            $r['Date_of_session'] = '';
                            $sql = sprintf("select * from venue_slot_book where session_id = %s" , $r['sessions_id']);
                            $vd = $conn->execute($sql);
                            if($v =$vd->fetch('assoc')){
                                $r['venue_id'] = $v['venue_id'];
                                $r['Date_of_session'] = $v['date'];
                                if($v['venue_id'] != -1 || $v['venue_id'] != '-1'){
                                    $sql = $conn->execute(sprintf("select venue_name from venue where venue_id = %s" , $r['venue_id']));
                                    if($a = $sql->fetch('assoc')){$r['venue_name'] = $a['venue_name'];}
                                }
                            }

                            $press = explode(",", $r['presentors']);
                            //$r['presentors'] = null;
                            $temp = array();
                            foreach ($press as $pres ) {
                                if($pres == ''){break;}
                                $dat = $this->userData($args['userid'] , $pres , "user" , 0);
                                $t = isset($dat[0])? $dat[0] : null;
                                if($t != null){
                                    array_push($temp , $t);
                                }
                            }
                            $r['presentors'] = $temp;
                            $sd = $conn->execute(sprintf("SELECT * from sessions_discussion where sessions_id = %s and parent_discussion_id = 0", $r['sessions_id']));
                            $r["discussions"] = $sd->count();
                            $r['session_attend_friends'] = 0;
                            $r['i_attend'] = 0;
                            $sa = $conn->execute(sprintf("SELECT sa.user_id as uid FROM `sessions_attends` as sa inner join `friends` as fr on (fr.user_one_id = sa.user_id) or (fr.user_two_id = sa.user_id)  where sessions_id = %s and fr.status = 1 and (fr.user_one_id = $userid or fr.user_two_id = $userid)" ,$r['sessions_id']));
                            while($sac = $sa->fetch("assoc")){
                                if($sac['uid'] == $userid){$r['i_attend']=1;continue;}
                                $r['session_attend_friends']++;
                            }
                            $r['i_thank'] = null;
                            //reactions
                            $react = array();
                            $rname = array();
                            $reactions = $conn->execute(sprintf("SELECT * from `reactions` "));
                            while($a = $reactions->fetch('assoc')){
                                $str = '' . $a['id'] . '';
                                //$react[$str] = 0;
                                array_push($react, 0);
                                array_push($rname, $a['name']);


                            }
                            $count = count($rname);
                            //$pc = $conn->execute(sprintf("SELECT * from `posts` where post_type = 'article' and article_id = %s"  , $articlesId));
                            
                            //if(!$p = $pc->fetch("assoc")){return "no post found";}
                            $l = $conn->execute(sprintf("SELECT * from `session_likes` where session_id = %s"  , $r['sessions_id']));
                            $t = 0;
                            while($a = $l->fetch("assoc")){
                                $t = $t + 1;
                                if($a['user_id'] == $userid){$r['i_thank'] = $a['reaction_type'];}
                                $str = $a['reaction_type'];
                                $react[$str] = $react[$str] + 1;
                            }
                            $fr = array();
                            for ($i=0; $i < $count; $i++) { 
                                # code...
                                $fr[$rname[$i]] = $react[$i];
                            }

                            $r['reactions'] = $fr;
                            $r['total_likes'] = $t;
                            $sql = $conn->execute(sprintf("SELECT * from sessions_qa where sessions_id = %s and parent_question_id = 0 " , $r['sessions_id']));
                            $r['total_questions'] = $sql->count();
                            $parray = array();
                            if($r['event_photos'] != null || $r['event_photos'] != ''){
                                $temp = explode(",", $r['event_photos']);
                                foreach($temp as $i){
                                    $i = $this->getImgFullPath($i , 1);
                                    array_push($parray , $i);
                                }
                            }
                            $r['event_photos'] = $parray;
                            //friend comments and likes
                            $user_id = $args['userid'];
                            $session_id = $r['sessions_id'];
                            $user = new UserModel($user_id);
                            $mf = $user->get_friends_ids($user_id);
                            $mf = implode(',', $mf);
                            if($co = $conn->execute(sprintf("SELECT * FROM sessions_qa WHERE sessions_id = $session_id and FIND_IN_SET(user_id , '$mf') order by sessions_qa_id desc"))->fetch('assoc')){
                                $friend = $pd->userData($user_id , $co['user_id'] , 'user' , 0)[0];
                                $co['user'] = $friend;
                                $r['friend_qa'] = $co;
                            }else{
                                $r['friend_qa'] = null;
                            }
                            if($co = $conn->execute(sprintf("SELECT * FROM sessions_comment WHERE sessions_id = $session_id and FIND_IN_SET(user_id , '$mf') order by sessions_comment_id desc"))->fetch('assoc')){
                                $friend = $pd->userData($user_id , $co['user_id'] , 'user' , 0)[0];
                                $co['user'] = $friend;
                                $r['friend_comment'] = $co;
                            }else{
                                $r['friend_comment'] = null;
                            }
                            if($co = $conn->execute(sprintf("SELECT * FROM sessions_discussion WHERE sessions_id = $session_id and FIND_IN_SET(user_id , '$mf') order by sessions_discussion_id desc"))->fetch('assoc')){
                                $friend = $pd->userData($user_id , $co['user_id'] , 'user' , 0)[0];
                                $co['user'] = $friend;
                                $r['friend_discussion'] = $co;
                            }else{
                                $r['friend_discussion'] = null;
                            }
                            if($il = $conn->execute(sprintf("SELECT * FROM session_likes WHERE session_id = $session_id and FIND_IN_SET(user_id , '$mf')"))->fetch('assoc')){
                                $friend = $pd->userData($user_id , $il['user_id'] , 'user' , 0)[0];
                                $il['user'] = $friend;
                                $r['friend_like'] = $il;
                            }else{
                                $r['friend_like'] = null;
                            }

                            return $r;
                        }
                    }
                    break;
                case "users":
                    if($sql != ''){
                        $st = $conn->execute($sql);
                        while($res = $st->fetch("assoc")){
                            //return $res;
                            $usid = $res['user_id'];
                            $dat = $this->userData($args['userid'] , $usid , 'user' , 0);
                            if(isset($dat[0])){
                                $rest = $dat[0];
                            }else{continue;}
                            array_push($arr , $rest);
                        }
                        return $arr;
                    }else{
                        $usid = $contentId;
                        $dat = $this->userData($args['userid'] , $usid , 'user' , 0);
                        if(isset($dat[0])){
                            $res = $dat[0];
                            return $res;
                        }
                        return $res;
                        
                    }
                    break;

            }
                    
            

        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }

    public function search($args){
        Log::debug("Started ...search Dao");

        try{

            $conn = ConnectionManager::get('default');
            $codes = new Codes;
            $du = new DateUtils;
            $rc = $codes->RC_CONST;
            $date = $du->getCurrentDate();
            $date = strval($date);
            $args['date'] = $date;
            $input = $args;
            $skips = 0;
            $arr = array();
            $arr['articles'] = array();
            $arr['quizzes'] = array();
            $arr['debates'] = array();
            $arr['polls'] = array();
            $arr['quickbits'] = array();
            $arr['community'] = array();
            $arr['interests'] = array();
            $arr['organization'] = array();
            $arr['venue'] = array();
            $arr['users'] = array();
            $arr['posts'] = array();
            $arr['sessions'] = array();
            $val = '4,5,6';
            $mi = new MutualInterestsDao();
            extract($args);
            //return $mi->get_mutual_interests_csvs("1,2,3" , "1,2,3" , $rc);

            switch ($args['type']) {
                case 'articles':
                   $sql = "SELECT *  FROM articles where (user_type = 'user' or user_type = 'institution' or user_type = 'organization' or user_type = 'venue' or (user_type = 'community' and approved_by > 0)) and";
                   //$sql = "SELECT * , '$val'new_val FROM articles where";
                   if($args['query'] != ''){
                        $sql .= " (description like '%".$args['query']."%' or title like '%".$args['query']."%') and";
                    }
                    if($args['usertype'] != 'user'){
                        $sql .= " user_type = '".$args['usertype']."' and";
                        if($args['usertype'] == 'community'){
                            $sql .= " community_id = ".$args['id']." and approved_by > 0 and";
                            if($args['myrole'] == "My Articles"){
                                $sql .= " created_by = ".$args['userid']." and";
                            }
                        }else{
                            $sql .= " created_by = ".$args['id']." and";
                        }
                    }else{

                        if($args['myrole'] == "My Articles"){
                            $sql .= " (user_type = 'user' or (user_type = 'community' and approved_by > 0)) and";
                            $sql .= " created_by = ".$args['userid']." and";
                        }

                    }
                    $sql = rtrim($sql , 'where'); // when no if else is hit
                    
                    $temp = array();
                    if(isset($args['myintrests']) && $args['myintrests'] == "1"){
                        $sql = rtrim($sql , 'and');
                        $sql .= " order by articles_id DESC ";
                        $m = $conn->execute($sql);
                        while($match = $m->fetch("assoc")){
                            $aid = "".$match['articles_id']."#";
                            $percent = $mi->get_mutual_interests_idncsv($userid , $match['interests'] , $rc);
                            $temp[$aid] = $percent;
                        }
                        arsort($temp);
                        $idar = array();
                        foreach ($temp as $key => $value) {
                            $key = rtrim($key , '#');
                            array_push($idar , $key);
                        }
                        for ($i=0; $i < count($idar); $i++) { 
                            if($lastid != 0){
                                if($idar[$i] == "$lastid"){
                                    $check = $args['pagesize'];
                                    $c = 0;
                                    while($c < $check ){
                                        $sql .= " articles_id = ".$idar[$i + $c]." or";
                                        if(!isset($idar[$i + $c + 1]))
                                            return $arr;
                                        array_push($arr['articles'] , $this->getData($idar[$i + $c + 1] , "articles" , $args));
                                        $c++;
                                    }
                                    break;
                                }else{continue;}
                            }else{
                                $check = $args['pagesize'];
                                while($i < $check ){
                                    $sql .= " articles_id = ".$idar[$i]." or";
                                    if(!isset($idar[$i]))
                                        return $arr;
                                    array_push($arr['articles'] , $this->getData($idar[$i] , "articles" , $args));
                                    $i++;
                                }
                                break;
                            }
                        }
                    }elseif(isset($args['intrest']) && $args['intrest'] != ""){
                        $sql = rtrim($sql , 'and');
                        $sql .= " order by articles_id DESC ";
                        $m = $conn->execute($sql);
                        //return $sql;
                        
                        while($match = $m->fetch("assoc")){
                            $aid = "".$match['articles_id']."#";
                            $percent = $mi->get_mutual_interests_csvs($match['interests'] , $args['intrest'] , $rc);
                            $temp[$aid] = $percent;
                        }
                        arsort($temp);
                        $idar = array();
                        foreach ($temp as $key => $value) {
                            $key = rtrim($key , '#');
                            array_push($idar , $key);
                        }
                        for ($i=0; $i < count($idar); $i++) { 
                            if($lastid != 0){
                                if($idar[$i] == "$lastid"){
                                    $check = $args['pagesize'];
                                    $c = 0;
                                    while($c < $check ){
                                        $sql .= " articles_id = ".$idar[$i + $c]." or";
                                        if(!isset($idar[$i + $c + 1]))
                                            return $arr;
                                        array_push($arr['articles'] , $this->getData($idar[$i + $c + 1] , "articles" , $args));
                                        $c++;
                                    }
                                    break;
                                }else{continue;}
                            }else{
                                $check = $args['pagesize'];
                                while($i < $check ){
                                    $sql .= " articles_id = ".$idar[$i]." or";
                                    if(!isset($idar[$i]))
                                        return $arr;
                                    array_push($arr['articles'] , $this->getData($idar[$i] , "articles" , $args));
                                    $i++;
                                }
                                break;
                            }
                        }
                    }else{
                        $sql = rtrim($sql , 'and');
                        if($lastid != 0)
                            $sql .= " and articles_id < $lastid order by articles_id DESC  limit ".$skips.", ".$args['pagesize'];
                        else
                            $sql .= " order by articles_id DESC  limit ".$skips.", ".$args['pagesize'];
                        $arr['articles'] = $this->getData(0 , "articles" , $args , $sql);
                    }
                    //return $sql;
                    break;
                case "quizzes":
                    switch($myrole){
                        case 'Any':
                            $sql = "SELECT *,(case when DATE_ADD(start_time, INTERVAL `live_duration` second) < '".$date."' then 'after live' when start_time > '".$date."' then 'before live' else 'live' end) as quiz_status FROM quizzes where (user_type = 'user' or user_type = 'institution' or user_type = 'organization' or user_type = 'venue' or (user_type = 'community' and approved_by > 0)) and";
                            if($args['query'] != ''){
                                $sql .= " (description like '%".$args['query']."%' or title like '%".$args['query']."%') and";
                            }
                            $sql .= " deleted = 0 and";
                            if($args['usertype'] != 'user'){
                                $sql .= " user_type = '".$args['usertype']."' and";
                                if($args['usertype'] == 'community'){
                                    $sql .= " community_id = ".$args['id']." and approved_by > 0 and";
                                }else{
                                    $sql .= " creator_id = ".$args['id']." and";
                                }
                            }
                            if(isset($args['live'])){
                                if($args['live'] == 1 ){
                                    $sql .= " start_time < '".$date."' and";
                                }elseif ($args['live'] == -1) {
                                    $sql .= " start_time > '".$date."' and";
                                }
                            }
                            if (isset($args['live_status']) && !empty($args['live_status'])) {
                                //options: 
                                // 1. Before live duration
                                // 2. During live duration
                                // 3. After live duration

                                if($args['live_status'] =='Before live duration')
                                    $sql .= " quiz_status = 'before live' and";
                                else if($args['live_status'] =='During live duration')
                                    $sql .= " quiz_status = 'live' and";
                                else if($args['live_status'] =='After live duration')
                                    $sql .= " quiz_status = 'after live' and";

                            }
                            $sql .= " deleted = 0 and";
                            $sql = rtrim($sql , 'where'); // when no if else is hit
                            
                            //return $sql;
                            break;

                        case 'Creator':
                            $sql = "SELECT *,(case when DATE_ADD(start_time, INTERVAL `live_duration` second) < '".$date."' then 'after live' when start_time > '".$date."' then 'before live' else 'live' end) as quiz_status FROM quizzes where (user_type = 'user' or user_type = 'institution' or user_type = 'organization' or user_type = 'venue' or (user_type = 'community' and approved_by > 0)) and";
                            $sql .= " deleted = 0 and";
                            if($args['query'] != ''){
                                $sql .= " (description like '%".$args['query']."%' or title like '%".$args['query']."%') and";
                            }
                            if($args['usertype'] != 'user'){
                                $sql .= " user_type = '".$args['usertype']."' and";
                                if($args['usertype'] == 'community'){
                                    $sql .= " community_id = ".$args['id']." and approved_by > 0 and";
                                }else{
                                    $sql .= " creator_id = ".$args['id']." and";
                                }
                            }else{
                                $sql .= " (user_type = 'user' or user_type = 'User' or ((user_type = 'community' or user_type = 'Community') and approved_by > 0)) and";
                                $sql .= " creator_id = ".$args['userid']." and";
                            }
                            if(isset($args['live'])){
                                if($args['live'] == 1 ){
                                    $sql .= " start_time < '".$date."' and";
                                }elseif ($args['live'] == -1) {
                                    $sql .= " start_time > '".$date."' and";
                                }
                            }
                            if (isset($args['live_status']) && !empty($args['live_status'])) {
                                //options: 
                                // 1. Before live duration
                                // 2. During live duration
                                // 3. After live duration

                                if($args['live_status'] =='Before live duration')
                                    $sql .= " quiz_status = 'before live' and";
                                else if($args['live_status'] =='During live duration')
                                    $sql .= " quiz_status = 'live' and";
                                else if($args['live_status'] =='After live duration')
                                    $sql .= " quiz_status = 'after live' and";

                            }
                            $sql .= " deleted = 0 and";
                            $sql = rtrim($sql , 'where'); // when no if else is hit
                            
                            //return $sql;
                            break;
                        case 'Participating' :
                            $sql = sprintf("SELECT * FROM quizzes_participants where user_id = $userid and is_submitted = 0");
                            $temp = array();
                            $st = $conn->execute($sql);
                            while($r = $st->fetch('assoc')){
                                array_push($temp , $r['quiz_id']);
                            }
                            $flag = false;
                            $sql = "SELECT *,(case when DATE_ADD(start_time, INTERVAL `live_duration` second) < '".$date."' then 'after live' when start_time > '".$date."' then 'before live' else 'live' end) as quiz_status FROM quizzes where deleted = 0 and (user_type = 'user' or user_type = 'institution' or user_type = 'organization' or user_type = 'venue' or (user_type = 'community' and approved_by > 0)) and (";
                            foreach ($temp as $i ) {
                                $flag = true;
                                $sql .= " id = ".$i." or";
                            }
                            if($flag){
                                $sql = rtrim($sql , "or");
                                $sql .= " ) and";
                            }else{
                                return $arr; //if no following have to return empty data can be replaced
                                $sql = rtrim($sql , "(");
                            }
                            if($args['query'] != ''){
                                $sql .= " (description like '%".$args['query']."%' or title like '%".$args['query']."%') and";
                            }
                            if($args['usertype'] != 'user'){
                                $sql .= " user_type = '".$args['usertype']."' and";
                                if($args['usertype'] == 'community'){
                                    $sql .= " community_id = ".$args['id']." and approved_by > 0 and";
                                }else{
                                    $sql .= " creator_id = ".$args['id']." and";
                                }
                            }
                            if(isset($args['live'])){
                                if($args['live'] == 1 ){
                                    $sql .= " start_time < '".$date."' and";
                                }elseif ($args['live'] == -1) {
                                    $sql .= " start_time > '".$date."' and";
                                }
                            }
                            if (isset($args['live_status']) && !empty($args['live_status'])) {
                                //options: 
                                // 1. Before live duration
                                // 2. During live duration
                                // 3. After live duration

                                if($args['live_status'] =='Before live duration')
                                    $sql .= " quiz_status = 'before live' and";
                                else if($args['live_status'] =='During live duration')
                                    $sql .= " quiz_status = 'live' and";
                                else if($args['live_status'] =='After live duration')
                                    $sql .= " quiz_status = 'after live' and";

                            }
                            $sql .= " deleted = 0 and";
                            $sql = rtrim($sql , 'where'); // when no if else is hit
                            
                            //return $sql;
                            break;
                        case 'Attempted' :
                            $sql = sprintf("SELECT * FROM quizzes_participants where user_id = $userid and is_submitted = 1");
                            $temp = array();
                            $st = $conn->execute($sql);
                            while($r = $st->fetch('assoc')){
                                array_push($temp , $r['quiz_id']);
                            }
                            $flag = false;
                            $sql = "SELECT *,(case when DATE_ADD(start_time, INTERVAL `live_duration` second) < '".$date."' then 'after live' when start_time > '".$date."' then 'before live' else 'live' end) as quiz_status FROM quizzes where deleted = 0 and (user_type = 'user' or user_type = 'institution' or user_type = 'organization' or user_type = 'venue' or (user_type = 'community' and approved_by > 0)) and (";
                            foreach ($temp as $i ) {
                                $flag = true;
                                $sql .= " id = ".$i." or";
                            }
                            if($flag){
                                $sql = rtrim($sql , "or");
                                $sql .= " ) and";
                            }else{
                                return $arr; //if no following have to return empty data can be replaced
                                $sql = rtrim($sql , "(");
                            }
                            if($args['query'] != ''){
                                $sql .= " (description like '%".$args['query']."%' or title like '%".$args['query']."%') and";
                            }
                            if($args['usertype'] != 'user'){
                                $sql .= " user_type = '".$args['usertype']."' and";
                                if($args['usertype'] == 'community'){
                                    $sql .= " community_id = ".$args['id']." and approved_by > 0 and";
                                }else{
                                    $sql .= " creator_id = ".$args['id']." and";
                                }
                            }

                            if(isset($args['live'])){
                                if($args['live'] == 1 ){
                                    $sql .= " start_time < '".$date."' and";
                                }elseif ($args['live'] == -1) {
                                    $sql .= " start_time > '".$date."' and";
                                }
                            }
                            if (isset($args['live_status']) && !empty($args['live_status'])) {
                                //options: 
                                // 1. Before live duration
                                // 2. During live duration
                                // 3. After live duration

                                if($args['live_status'] =='Before live duration')
                                    $sql .= " quiz_status = 'before live' and";
                                else if($args['live_status'] =='During live duration')
                                    $sql .= " quiz_status = 'live' and";
                                else if($args['live_status'] =='After live duration')
                                    $sql .= " quiz_status = 'after live' and";

                            }
                            $sql .= " deleted = 0 and";
                            $sql = rtrim($sql , 'where'); // when no if else is hit
                            
                            
                            //return $sql;
                            break;
                        default:
                            $args['myrole'] = "Any";
                            return $this->search($args);
                    }






                    $temp = array();
                    if(isset($args['myintrests']) && $args['myintrests'] == "1"){
                        $sql = rtrim($sql , 'and');
                        //$sql = rtrim($sql , "DESC  limit ".$skips.", ".$args['pagesize']);
                        $m = $conn->execute($sql);
                        while($match = $m->fetch("assoc")){
                            $aid = "".$match['id']."#";
                            $percent = 0;
                            if($match['interests'] != null)
                                $percent = $mi->get_mutual_interests_idncsv($userid , $match['interests'] , $rc);
                            $temp[$aid] = $percent;
                        }
                        arsort($temp);
                        $idar = array();
                        foreach ($temp as $key => $value) {
                            $key = rtrim($key , '#');
                            array_push($idar , $key);
                        }
                        for ($i=0; $i < count($idar); $i++) { 
                            if($lastid != 0){
                                if($idar[$i] == "$lastid"){
                                    $check = $args['pagesize'];
                                    $c = 0;
                                    while($c < $check ){
                                        if(!isset($idar[$i + $c + 1]))
                                            return $arr;
                                        array_push($arr['quizzes'] , $this->getData($idar[$i + $c + 1] , "quizzes" , $args));
                                        $c++;
                                    }
                                    break;
                                }else{continue;}
                            }else{
                                $check = $args['pagesize'];
                                while($i < $check ){
                                    if(!isset($idar[$i]))
                                        return $arr;
                                    array_push($arr['quizzes'] , $this->getData($idar[$i] , "quizzes" , $args));
                                    $i++;
                                }
                                break;
                            }
                        }
                    }elseif(isset($args['intrest']) && $args['intrest'] != ""){
                        $sql = rtrim($sql , 'and');
                        //$sql = rtrim($sql , "DESC  limit ".$skips.", ".$args['pagesize']);
                        $m = $conn->execute($sql);
                        //return $sql;
                        
                        while($match = $m->fetch("assoc")){
                            $aid = "".$match['id']."#";
                            $percent = 0;
                            if($match['interests'] != null)
                                $percent = $mi->get_mutual_interests_csvs($match['interests'] , $args['intrest'] , $rc);
                            $temp[$aid] = $percent;
                        }
                        arsort($temp);
                        //return $temp;
                        $idar = array();
                        foreach ($temp as $key => $value) {
                            $key = rtrim($key , '#');
                            array_push($idar , $key);
                        }
                        //return $idar;
                        for ($i=0; $i < count($idar); $i++) { 
                            if($lastid != 0){
                                if($idar[$i] == "$lastid"){
                                    $check = $args['pagesize'];
                                    $c = 0;
                                    while($c < $check ){
                                        if(!isset($idar[$i + $c + 1]))
                                            return $arr;
                                        $dat = $this->getData($idar[$i + $c + 1] , "quizzes" , $args);
                                        array_push($arr['quizzes'] , $dat);
                                        $c++;
                                    }
                                    break;
                                }else{continue;}
                            }else{
                                $check = $args['pagesize'];
                                while($i < $check ){
                                    //return $idar[$i];
                                    if(!isset($idar[$i]))
                                        return $arr;
                                    array_push($arr['quizzes'] , $this->getData($idar[$i] , "quizzes" , $args));
                                    $i++;
                                }
                                break;
                            }
                        }
                    }else{
                        $sql = rtrim($sql , 'and');
                        if($lastid != 0)
                            $sql .= " and id < $lastid order by id DESC  limit ".$skips.", ".$args['pagesize'];
                        else
                            $sql .= " order by id DESC  limit ".$skips.", ".$args['pagesize'];
                        //return $sql;
                        $arr['quizzes'] = $this->getData(0 , "quizzes" , $args , $sql);
                    }
                    break;


                case "debates":

                    switch($myrole){
                        case 'Any':
                            $sql = "SELECT *,(case when DATE_ADD(start_time, INTERVAL `live_duration` second) < '".$date."' then 'after live' when start_time > '".$date."' then 'before live' else 'live' end) as debate_status FROM debates where (user_type = 'user' or user_type = 'institution' or user_type = 'organization' or user_type = 'venue' or (user_type = 'community' and approved_by > 0)) and";
                            $sql .= " deleted = 0 and";
                            if($args['query'] != ''){
                                $sql .= " (description like '%".$args['query']."%' or title like '%".$args['query']."%') and";
                            }
                            if($args['usertype'] != 'user'){
                                $sql .= " user_type = '".$args['usertype']."' and";
                                if($args['usertype'] == 'community'){
                                    $sql .= " community_id = ".$args['id']." and approved_by > 0 and";
                                }else{
                                    $sql .= " creator_id = ".$args['id']." and";
                                }
                            }
                            if(isset($args['live'])){
                                if($args['live'] == 1 ){
                                    $sql .= " start_time < '".$date."' and";
                                }elseif ($args['live'] == -1) {
                                    $sql .= " start_time > '".$date."' and";
                                }
                            }
                            if (isset($args['live_status']) && !empty($args['live_status'])) {
                                //options: 
                                // 1. Before live duration
                                // 2. During live duration
                                // 3. After live duration

                                if($args['live_status'] =='Before live duration')
                                    $sql .= " debate_status = 'before live' and";
                                else if($args['live_status'] =='During live duration')
                                    $sql .= " debate_status = 'live' and";
                                else if($args['live_status'] =='After live duration')
                                    $sql .= " debate_status = 'after live' and";

                            }
                            $sql .= " deleted = 0 and";
                            $sql = rtrim($sql , 'where'); // when no if else is hit
                            
                            //return $sql;
                            break;

                        case 'Creator':
                            $sql = "SELECT *,(case when DATE_ADD(start_time, INTERVAL `live_duration` second) < '".$date."' then 'after live' when start_time > '".$date."' then 'before live' else 'live' end) as debate_status FROM debates where (user_type = 'user' or user_type = 'institution' or user_type = 'organization' or user_type = 'venue' or (user_type = 'community' and approved_by > 0)) and";
                            $sql .= " deleted = 0 and";
                            if($args['query'] != ''){
                                $sql .= " (description like '%".$args['query']."%' or title like '%".$args['query']."%') and";
                            }
                            if($args['usertype'] != 'user'){
                                $sql .= " user_type = '".$args['usertype']."' and";
                                if($args['usertype'] == 'community'){
                                    $sql .= " community_id = ".$args['id']." and approved_by > 0 and";
                                }else{
                                    $sql .= " creator_id = ".$args['id']." and";
                                }
                            }else{
                                $sql .= " (user_type = 'user' or user_type = 'User' or ((user_type = 'community' or user_type = 'Community') and approved_by > 0)) and";
                                $sql .= " creator_id = ".$args['userid']." and";
                            }
                            if(isset($args['live'])){
                                if($args['live'] == 1 ){
                                    $sql .= " start_time < '".$date."' and";
                                }elseif ($args['live'] == -1) {
                                    $sql .= " start_time > '".$date."' and";
                                }
                            }
                            if (isset($args['live_status']) && !empty($args['live_status'])) {
                                //options: 
                                // 1. Before live duration
                                // 2. During live duration
                                // 3. After live duration

                                if($args['live_status'] =='Before live duration')
                                    $sql .= " debate_status = 'before live' and";
                                else if($args['live_status'] =='During live duration')
                                    $sql .= " debate_status = 'live' and";
                                else if($args['live_status'] =='After live duration')
                                    $sql .= " debate_status = 'after live' and";

                            }
                            $sql .= " deleted = 0 and";
                            $sql = rtrim($sql , 'where'); // when no if else is hit
                            
                            //return $sql;
                            break;
                            
                        case 'Participating' :
                            $sql = sprintf("SELECT * FROM debates_participants where user_id = $userid and is_submitted = 0");
                            $temp = array();
                            $st = $conn->execute($sql);
                            while($r = $st->fetch('assoc')){
                                array_push($temp , $r['debate_id']);
                            }
                            $flag = false;
                            $sql = "SELECT *,(case when DATE_ADD(start_time, INTERVAL `live_duration` second) < '".$date."' then 'after live' when start_time > '".$date."' then 'before live' else 'live' end) as debate_status FROM debates where deleted = 0 and (user_type = 'user' or user_type = 'institution' or user_type = 'organization' or user_type = 'venue' or (user_type = 'community' and approved_by > 0)) and (";
                            foreach ($temp as $i ) {
                                $flag = true;
                                $sql .= " id = ".$i." or";
                            }
                            if($flag){
                                $sql = rtrim($sql , "or");
                                $sql .= " ) and";
                            }else{
                                return $arr; //if no following have to return empty data can be replaced
                                $sql = rtrim($sql , "(");
                            }
                            if($args['query'] != ''){
                                $sql .= " (description like '%".$args['query']."%' or title like '%".$args['query']."%') and";
                            }
                            if($args['usertype'] != 'user'){
                                $sql .= " user_type = '".$args['usertype']."' and";
                                if($args['usertype'] == 'community'){
                                    $sql .= " community_id = ".$args['id']." and approved_by > 0 and";
                                }else{
                                    $sql .= " creator_id = ".$args['id']." and";
                                }
                            }
                            if(isset($args['live'])){
                                if($args['live'] == 1 ){
                                    $sql .= " start_time < '".$date."' and";
                                }elseif ($args['live'] == -1) {
                                    $sql .= " start_time > '".$date."' and";
                                }
                            }
                            if (isset($args['live_status']) && !empty($args['live_status'])) {
                                //options: 
                                // 1. Before live duration
                                // 2. During live duration
                                // 3. After live duration

                                if($args['live_status'] =='Before live duration')
                                    $sql .= " debate_status = 'before live' and";
                                else if($args['live_status'] =='During live duration')
                                    $sql .= " debate_status = 'live' and";
                                else if($args['live_status'] =='After live duration')
                                    $sql .= " debate_status = 'after live' and";

                            }
                            $sql .= " deleted = 0 and";
                            $sql = rtrim($sql , 'where'); // when no if else is hit
                            
                            //return $sql;
                            break;
                        case 'Attempted' :
                            $sql = sprintf("SELECT * FROM debates_participants where user_id = $userid and is_submitted = 1");
                            $temp = array();
                            $st = $conn->execute($sql);
                            while($r = $st->fetch('assoc')){
                                array_push($temp , $r['debate_id']);
                            }
                            $flag = false;
                            $sql = "SELECT *,(case when DATE_ADD(start_time, INTERVAL `live_duration` second) < '".$date."' then 'after live' when start_time > '".$date."' then 'before live' else 'live' end) as debate_status FROM debates where (user_type = 'user' or user_type = 'organization' or user_type = 'institution' or user_type = 'venue' or (user_type = 'community' and approved_by > 0)) and deleted = 0 and (";
                            foreach ($temp as $i ) {
                                $flag = true;
                                $sql .= " id = ".$i." or";
                            }
                            if($flag){
                                $sql = rtrim($sql , "or");
                                $sql .= " ) and";
                            }else{
                                return $arr; //if no following have to return empty data can be replaced
                                $sql = rtrim($sql , "(");
                            }
                            if($args['query'] != ''){
                                $sql .= " (description like '%".$args['query']."%' or title like '%".$args['query']."%') and";
                            }
                            if($args['usertype'] != 'user'){
                                $sql .= " user_type = '".$args['usertype']."' and";
                                if($args['usertype'] == 'community'){
                                    $sql .= " community_id = ".$args['id']." and approved_by > 0 and";
                                }else{
                                    $sql .= " creator_id = ".$args['id']." and";
                                }
                            }

                            if(isset($args['live'])){
                                if($args['live'] == 1 ){
                                    $sql .= " start_time < '".$date."' and";
                                }elseif ($args['live'] == -1) {
                                    $sql .= " start_time > '".$date."' and";
                                }
                            }
                            if (isset($args['live_status']) && !empty($args['live_status'])) {
                                //options: 
                                // 1. Before live duration
                                // 2. During live duration
                                // 3. After live duration

                                if($args['live_status'] =='Before live duration')
                                    $sql .= " debate_status = 'before live' and";
                                else if($args['live_status'] =='During live duration')
                                    $sql .= " debate_status = 'live' and";
                                else if($args['live_status'] =='After live duration')
                                    $sql .= " debate_status = 'after live' and";

                            }
                            $sql .= " deleted = 0 and";
                            $sql = rtrim($sql , 'where'); // when no if else is hit
                            
                            break;
                        default:
                            $args['myrole'] = "Any";
                            return $this->search($args);
                    }




                    $temp = array();
                    if(isset($args['myintrests']) && $args['myintrests'] == "1"){
                        $sql = rtrim($sql , 'and');
                        //$sql = rtrim($sql , "DESC  limit ".$skips.", ".$args['pagesize']);
                        $m = $conn->execute($sql);
                        while($match = $m->fetch("assoc")){
                            $aid = "".$match['id']."#";
                            $percent = 0;
                            if($match['interests'] != null)
                                $percent = $mi->get_mutual_interests_idncsv($userid , $match['interests'] , $rc);
                            $temp[$aid] = $percent;
                        }
                        arsort($temp);
                        $idar = array();
                        foreach ($temp as $key => $value) {
                            $key = rtrim($key , '#');
                            array_push($idar , $key);
                        }
                        for ($i=0; $i < count($idar); $i++) { 
                            if($lastid != 0){
                                if($idar[$i] == "$lastid"){
                                    $check = $args['pagesize'];
                                    $c = 0;
                                    while($c < $check ){
                                        if(!isset($idar[$i + $c + 1]))
                                            return $arr;
                                        array_push($arr['debates'] , $this->getData($idar[$i + $c + 1] , "debates" , $args));
                                        $c++;
                                    }
                                    break;
                                }else{continue;}
                            }else{
                                $check = $args['pagesize'];
                                while($i < $check ){
                                    if(!isset($idar[$i]))
                                        return $arr;
                                    array_push($arr['debates'] , $this->getData($idar[$i] , "debates" , $args));
                                    $i++;
                                }
                                break;
                            }
                        }
                    }elseif(isset($args['intrest']) && $args['intrest'] != ""){
                        $sql = rtrim($sql , 'and');
                        //$sql = rtrim($sql , "DESC  limit ".$skips.", ".$args['pagesize']);
                        $m = $conn->execute($sql);
                        //return $sql;
                        
                        while($match = $m->fetch("assoc")){
                            $aid = "".$match['id']."#";
                            $percent = 0;
                            if($match['interests'] != null)
                                $percent = $mi->get_mutual_interests_csvs($match['interests'] , $args['intrest'] , $rc);
                            $temp[$aid] = $percent;
                        }
                        arsort($temp);
                        //return $temp;
                        $idar = array();
                        foreach ($temp as $key => $value) {
                            $key = rtrim($key , '#');
                            array_push($idar , $key);
                        }
                        //return $idar;
                        for ($i=0; $i < count($idar); $i++) { 
                            if($lastid != 0){
                                if($idar[$i] == "$lastid"){
                                    $check = $args['pagesize'];
                                    $c = 0;
                                    while($c < $check ){
                                        if(!isset($idar[$i + $c + 1]))
                                            return $arr;
                                        $dat = $this->getData($idar[$i + $c + 1] , "debates" , $args);
                                        array_push($arr['debates'] , $dat);
                                        $c++;
                                    }
                                    break;
                                }else{continue;}
                            }else{
                                $check = $args['pagesize'];
                                while($i < $check ){
                                    //return $idar[$i];
                                    if(!isset($idar[$i]))
                                        return $arr;
                                    array_push($arr['debates'] , $this->getData($idar[$i] , "debates" , $args));
                                    $i++;
                                }
                                break;
                            }
                        }
                    }else{
                        $sql = rtrim($sql , 'and');
                        if($lastid != 0)
                            $sql .= " and id < $lastid order by id DESC  limit ".$skips.", ".$args['pagesize'];
                        else
                            $sql .= " order by id DESC  limit ".$skips.", ".$args['pagesize'];
                        //return $sql;
                        $arr['debates'] = $this->getData(0 , "debates" , $args , $sql);
                    }
                    break;



                case "polls":
                    $sql = "SELECT * from polls where (user_type = 'user'  or user_type = 'organization' or user_type = 'institution' or (user_type = 'community' and approved_by > 0)) and";
                    if($args['query'] != ''){
                        $sql .= " (title like '%".$args['query']."%' ) and";
                    }
                    if($args['usertype'] == 'venue'){$args['usertype'] = 'institution';}//changes in poll user type
                    if($args['usertype'] != 'user'){
                        $sql .= " user_type = '".$args['usertype']."' and";
                        if($args['usertype'] == 'community'){
                            $sql .= " community_id = ".$args['id']." and approved_by > 0 and";
                        }else{
                            $sql .= " creator_id = ".$args['id']." and";
                        }
                    }else{
                        if($myrole == "My Poll"){
                            $sql .= " (user_type = 'user' or (user_type = 'community' and approved_by > 0)) and";
                            $sql .= " creator_id = ".$args['userid']." and";
                        }
                    }
                    $sql = rtrim($sql , 'where'); // when no if else is hit
                    $sql = rtrim($sql , 'and');

                    $temp = array();
                    if(isset($args['myintrests']) && $args['myintrests'] == "1"){
                        $sql = rtrim($sql , 'and');
                        $sql .= " order by id DESC  ";
                        $m = $conn->execute($sql);
                        while($match = $m->fetch("assoc")){
                            $aid = "".$match['id']."#";
                            $percent = 0;
                            if($match['interests'] != null)
                                $percent = $mi->get_mutual_interests_idncsv($userid , $match['interests'] , $rc);
                            $temp[$aid] = $percent;
                        }
                        arsort($temp);
                        $idar = array();
                        foreach ($temp as $key => $value) {
                            $key = rtrim($key , '#');
                            array_push($idar , $key);
                        }
                        for ($i=0; $i < count($idar); $i++) { 
                            if($lastid != 0){
                                if($idar[$i] == "$lastid"){
                                    $check = $args['pagesize'];
                                    $c = 0;
                                    while($c < $check ){
                                        if(!isset($idar[$i + $c + 1]))
                                            return $arr;
                                        array_push($arr['polls'] , $this->getPollData($idar[$i + $c + 1] , $userid));
                                        $c++;
                                    }
                                    break;
                                }else{continue;}
                            }else{
                                $check = $args['pagesize'];
                                while($i < $check ){
                                    if(!isset($idar[$i]))
                                        return $arr;
                                    array_push($arr['polls'] , $this->getPollData($idar[$i] , $userid));
                                    $i++;
                                }
                                break;
                            }
                        }
                    }elseif(isset($args['intrest']) && $args['intrest'] != ""){
                        $sql = rtrim($sql , 'and');
                        $sql .= " order by id DESC  ";
                        $m = $conn->execute($sql);
                        //return $sql;
                        
                        while($match = $m->fetch("assoc")){
                            $aid = "".$match['id']."#";
                            $percent = 0;
                            if($match['interests'] != null)
                                $percent = $mi->get_mutual_interests_csvs($match['interests'] , $args['intrest'] , $rc);
                            $temp[$aid] = $percent;
                        }
                        arsort($temp);
                        //return $temp;
                        $idar = array();
                        foreach ($temp as $key => $value) {
                            $key = rtrim($key , '#');
                            array_push($idar , $key);
                        }
                        //return $idar;
                        for ($i=0; $i < count($idar); $i++) { 
                            if($lastid != 0){
                                if($idar[$i] == "$lastid"){
                                    $check = $args['pagesize'];
                                    $c = 0;
                                    while($c < $check ){
                                        if(!isset($idar[$i + $c + 1]))
                                            return $arr;
                                        $dat = $this->getPollData($idar[$i + $c + 1] , $userid);
                                        array_push($arr['polls'] , $dat);
                                        $c++;
                                    }
                                    break;
                                }else{continue;}
                            }else{
                                $check = $args['pagesize'];
                                while($i < $check ){
                                    //return $idar[$i];
                                    if(!isset($idar[$i]))
                                        return $arr;
                                    array_push($arr['polls'] , $this->getPollData($idar[$i] , $userid));
                                    $i++;
                                }
                                break;
                            }
                        }
                    }else{
                        $sql = rtrim($sql , 'and');
                        if($lastid != 0)
                            $sql .= " and id < $lastid order by id DESC  limit ".$skips.", ".$args['pagesize'];
                        else
                            $sql .= " order by id DESC  limit ".$skips.", ".$args['pagesize'];
                        //return $sql;
                        $d = $conn->execute($sql);
                        while($dat = $d->fetch("assoc")){
                            array_push($arr['polls'] , $this->getPollData($dat['id'] , $userid));
                        }
                        //$arr['polls'] = $this->getData(0 , "polls" , $args , $sql);
                    }
                    break;




                case 'quickbits':
                    $sql = "SELECT * FROM quickbits where (user_type = 'user' or user_type = 'institution' or user_type = 'organization' or user_type = 'venue' or (user_type = 'community' and approved_by > 0)) and";
                    if($query != ''){
                        $query = $this->mysql_real_escape_string($query);
                        $hashset = array();
                        $set = array();
                        $sqlm = "SELECT * FROM quickbits_cards where card_text like '%".$query."%'";
                        $st = $conn->execute($sqlm);
                        while($r = $st->fetch('assoc')){
                            $value = $r['quickbit_id'];
                            if (!array_key_exists($value, $hashset)){
                                array_push($set, $value);
                                $hashset[$value] = true;
                            }
                        }
                        $flag = false;
                        //return $set;
                        $sql .= " (";
                        foreach ($set as $i ) {
                            $flag = true;
                            $sql .= " id = ".$i." or";
                        }
                        if($flag){
                            $sql = rtrim($sql , "or");
                            $sql .= " ) and";
                        }else{
                            return $arr; //if no following have to return empty data can be replaced
                            $sql = rtrim($sql , " (");
                        }

                    }
                    if($args['usertype'] != 'user'){
                        $sql .= " user_type = '".$args['usertype']."' and";
                        if($args['usertype'] == 'community'){
                            $sql .= " community_id = ".$args['id']." and approved_by > 0 and";
                            if($args['myrole'] == "My Articles"){
                                $sql .= " creator_id = ".$args['userid']." and";
                            }
                        }else{
                            $sql .= " creator_id = ".$args['id']." and";
                        }
                    }else{

                        if($args['myrole'] == "My Quickbits"){
                            $sql .= " (user_type = 'user' or (user_type = 'community' and approved_by > 0)) and";
                            $sql .= " creator_id = ".$args['userid']." and";
                        }

                    }
                    $sql = rtrim($sql , 'where'); // when no if else is hit
                    $sql = rtrim($sql , 'and');

                    $temp = array();
                    if(isset($args['myintrests']) && $args['myintrests'] == "1"){
                        $sql = rtrim($sql , 'and');
                        $sql .= " order by id DESC  ";
                        $m = $conn->execute($sql);
                        while($match = $m->fetch("assoc")){
                            $aid = "".$match['id']."#";
                            $percent = 0;
                            if($match['interests'] != null)
                                $percent = $mi->get_mutual_interests_idncsv($userid , $match['interests'] , $rc);
                            $temp[$aid] = $percent;
                        }
                        arsort($temp);
                        $idar = array();
                        foreach ($temp as $key => $value) {
                            $key = rtrim($key , '#');
                            array_push($idar , $key);
                        }
                        for ($i=0; $i < count($idar); $i++) { 
                            if($lastid != 0){
                                if($idar[$i] == "$lastid"){
                                    $check = $args['pagesize'];
                                    $c = 0;
                                    while($c < $check ){
                                        if(!isset($idar[$i + $c + 1]))
                                            return $arr;
                                        array_push($arr['quickbits'] , $this->getData($idar[$i + $c + 1] , "quickbits" , $args));
                                        $c++;
                                    }
                                    break;
                                }else{continue;}
                            }else{
                                $check = $args['pagesize'];
                                while($i < $check ){
                                    if(!isset($idar[$i]))
                                        return $arr;
                                    array_push($arr['quickbits'] , $this->getData($idar[$i] , "quickbits" , $args));
                                    $i++;
                                }
                                break;
                            }
                        }
                    }elseif(isset($args['intrest']) && $args['intrest'] != ""){
                        $sql = rtrim($sql , 'and');
                        $sql .= " order by id DESC  ";
                        $m = $conn->execute($sql);
                        //return $sql;
                        
                        while($match = $m->fetch("assoc")){
                            $aid = "".$match['id']."#";
                            $percent = 0;
                            if($match['interests'] != null)
                                $percent = $mi->get_mutual_interests_csvs($match['interests'] , $args['intrest'] , $rc);
                            $temp[$aid] = $percent;
                        }
                        arsort($temp);
                        //return $temp;
                        $idar = array();
                        foreach ($temp as $key => $value) {
                            $key = rtrim($key , '#');
                            array_push($idar , $key);
                        }
                        //return $idar;
                        for ($i=0; $i < count($idar); $i++) { 
                            if($lastid != 0){
                                if($idar[$i] == "$lastid"){
                                    $check = $args['pagesize'];
                                    $c = 0;
                                    while($c < $check ){
                                        if(!isset($idar[$i + $c + 1]))
                                            return $arr;
                                        $dat = $this->getData($idar[$i + $c + 1] , "quickbits" , $args);
                                        array_push($arr['quickbits'] , $dat);
                                        $c++;
                                    }
                                    break;
                                }else{continue;}
                            }else{
                                $check = $args['pagesize'];
                                while($i < $check ){
                                    //return $idar[$i];
                                    if(!isset($idar[$i]))
                                        return $arr;
                                    array_push($arr['quickbits'] , $this->getData($idar[$i] , "quickbits" , $args));
                                    $i++;
                                }
                                break;
                            }
                        }
                    }else{
                        $sql = rtrim($sql , 'and');
                        if($lastid != 0)
                            $sql .= " and id < $lastid order by id DESC  limit ".$skips.", ".$args['pagesize'];
                        else
                            $sql .= " order by id DESC  limit ".$skips.", ".$args['pagesize'];
                        //return $sql;
                        $arr['quickbits'] = $this->getData(0 , "quickbits" , $args , $sql);
                    }
                    break;
                case 'community':
                    $sql = "SELECT * FROM communities WHERE";
                    if($myrole == 'Joined'){
                        $sqlm = "SELECT * FROM communities_members WHERE user_id = $userid and role != 4 and role != 5";
                        $hashset = array();
                        $set = array();
                        $st = $conn->execute($sqlm);
                        while($r = $st->fetch('assoc')){
                            $value = $r['community_id'];
                            if (!array_key_exists($value, $hashset)){
                                array_push($set, $value);
                                $hashset[$value] = true;
                            }
                        }
                        $flag = false;
                        //return $set;
                        $sql .= " (";
                        foreach ($set as $i ) {
                            $flag = true;
                            $sql .= " id = ".$i." or";
                        }
                        if($flag){
                            $sql = rtrim($sql , "or");
                            $sql .= " ) and";
                        }else{
                            return $arr; //if no following have to return empty data can be replaced
                            $sql = rtrim($sql , " (");
                        }

                    }
                    if($query != ""){
                        $sql .= " title like '%".$query."%' and";
                    }
                    $sql = rtrim($sql , 'WHERE'); // when no if else is hit
                    $sql = rtrim($sql , 'and');


                    if(isset($args['myintrests']) && $args['myintrests'] == "1"){
                        $sql = rtrim($sql , 'and');
                        $sql .= " order by id DESC  ";
                        $m = $conn->execute($sql);
                        while($match = $m->fetch("assoc")){
                            $aid = "".$match['id']."#";
                            $percent = 0;
                            if($match['interests'] != null)
                                $percent = $mi->get_mutual_interests_idncsv($userid , $match['interests'] , $rc);
                            $temp[$aid] = $percent;
                        }
                        arsort($temp);
                        $idar = array();
                        foreach ($temp as $key => $value) {
                            $key = rtrim($key , '#');
                            array_push($idar , $key);
                        }
                        for ($i=0; $i < count($idar); $i++) { 
                            if($lastid != 0){
                                if($idar[$i] == "$lastid"){
                                    $check = $args['pagesize'];
                                    $c = 0;
                                    while($c < $check ){
                                        if(!isset($idar[$i + $c + 1]))
                                            return $arr;
                                        array_push($arr['community'] , $this->getData($idar[$i + $c + 1] , "community" , $args));
                                        $c++;
                                    }
                                    break;
                                }else{continue;}
                            }else{
                                $check = $args['pagesize'];
                                while($i < $check ){
                                    if(!isset($idar[$i]))
                                        return $arr;
                                    array_push($arr['community'] , $this->getData($idar[$i] , "community" , $args));
                                    $i++;
                                }
                                break;
                            }
                        }
                    }elseif(isset($args['intrest']) && $args['intrest'] != ""){
                        $sql = rtrim($sql , 'and');
                        $sql .= " order by id DESC  ";
                        $m = $conn->execute($sql);
                        //return $sql;
                        
                        while($match = $m->fetch("assoc")){
                            $aid = "".$match['id']."#";
                            $percent = 0;
                            if($match['interests'] != null)
                                $percent = $mi->get_mutual_interests_csvs($match['interests'] , $args['intrest'] , $rc);
                            $temp[$aid] = $percent;
                        }
                        arsort($temp);
                        //return $temp;
                        $idar = array();
                        foreach ($temp as $key => $value) {
                            $key = rtrim($key , '#');
                            array_push($idar , $key);
                        }
                        //return $idar;
                        for ($i=0; $i < count($idar); $i++) { 
                            if($lastid != 0){
                                if($idar[$i] == "$lastid"){
                                    $check = $args['pagesize'];
                                    $c = 0;
                                    while($c < $check ){
                                        if(!isset($idar[$i + $c + 1]))
                                            return $arr;
                                        $dat = $this->getData($idar[$i + $c + 1] , "community" , $args);
                                        array_push($arr['community'] , $dat);
                                        $c++;
                                    }
                                    break;
                                }else{continue;}
                            }else{
                                $check = $args['pagesize'];
                                while($i < $check ){
                                    //return $idar[$i];
                                    if(!isset($idar[$i]))
                                        return $arr;
                                    array_push($arr['community'] , $this->getData($idar[$i] , "community" , $args));
                                    $i++;
                                }
                                break;
                            }
                        }
                    }else{
                        $sql = rtrim($sql , 'and');
                        if($lastid != 0)
                            $sql .= " and id < $lastid order by id DESC  limit ".$skips.", ".$args['pagesize'];
                        else
                            $sql .= " order by id DESC  limit ".$skips.", ".$args['pagesize'];
                        //return $sql;
                        $arr['community'] = $this->getData(0 , "community" , $args , $sql);
                    }
                    break;

                case 'interests':
                    $sql = "SELECT * FROM interest_mst where";
                    if($query != ""){
                        $sql .= " text like '%".$query."%' and";
                    }
                    $sql = rtrim($sql , 'where'); // when no if else is hit
                    $sql = rtrim($sql , 'and');



                    if(isset($args['myintrests']) && $args['myintrests'] == "1"){
                        $sql = rtrim($sql , 'and');
                        $sql .= " order by interest_id DESC  ";
                        $m = $conn->execute($sql);
                        while($match = $m->fetch("assoc")){
                            $aid = "".$match['interest_id']."#";
                            $percent = 0;
                            if($match['interest_id'] != null)
                                $percent = $mi->get_mutual_interests_idncsv($userid , $match['interest_id'] , $rc);
                            $temp[$aid] = $percent;
                        }
                        arsort($temp);
                        $idar = array();
                        foreach ($temp as $key => $value) {
                            $key = rtrim($key , '#');
                            array_push($idar , $key);
                        }
                        for ($i=0; $i < count($idar); $i++) { 
                            if($lastid != 0){
                                if($idar[$i] == "$lastid"){
                                    $check = $args['pagesize'];
                                    $c = 0;
                                    while($c < $check ){
                                        if(!isset($idar[$i + $c + 1]))
                                            return $arr;
                                        array_push($arr['interests'] , $this->getData($idar[$i + $c + 1] , "interests" , $args));
                                        $c++;
                                    }
                                    break;
                                }else{continue;}
                            }else{
                                $check = $args['pagesize'];
                                while($i < $check ){
                                    if(!isset($idar[$i]))
                                        return $arr;
                                    array_push($arr['interests'] , $this->getData($idar[$i] , "interests" , $args));
                                    $i++;
                                }
                                break;
                            }
                        }
                    }elseif(isset($args['intrest']) && $args['intrest'] != ""){
                        $sql = rtrim($sql , 'and');
                        $sql .= " order by interest_id DESC  ";
                        $m = $conn->execute($sql);
                        //return $sql;
                        
                        while($match = $m->fetch("assoc")){
                            $aid = "".$match['interest_id']."#";
                            $percent = 0;
                            if($match['interest_id'] != null)
                                $percent = $mi->get_mutual_interests_csvs($match['interest_id'] , $args['intrest'] , $rc);
                            $temp[$aid] = $percent;
                        }
                        arsort($temp);
                        //return $temp;
                        $idar = array();
                        foreach ($temp as $key => $value) {
                            $key = rtrim($key , '#');
                            array_push($idar , $key);
                        }
                        //return $idar;
                        for ($i=0; $i < count($idar); $i++) { 
                            if($lastid != 0){
                                if($idar[$i] == "$lastid"){
                                    $check = $args['pagesize'];
                                    $c = 0;
                                    while($c < $check ){
                                        if(!isset($idar[$i + $c + 1]))
                                            return $arr;
                                        $dat = $this->getData($idar[$i + $c + 1] , "interests" , $args);
                                        array_push($arr['interests'] , $dat);
                                        $c++;
                                    }
                                    break;
                                }else{continue;}
                            }else{
                                $check = $args['pagesize'];
                                while($i < $check ){
                                    //return $idar[$i];
                                    if(!isset($idar[$i]))
                                        return $arr;
                                    array_push($arr['interests'] , $this->getData($idar[$i] , "interests" , $args));
                                    $i++;
                                }
                                break;
                            }
                        }
                    }else{
                        $sql = rtrim($sql , 'and');
                        if($lastid != 0)
                            $sql .= " and interest_id < $lastid order by interest_id DESC  limit ".$skips.", ".$args['pagesize'];
                        else
                            $sql .= " order by interest_id DESC  limit ".$skips.", ".$args['pagesize'];
                        //return $sql;
                        $arr['interests'] = $this->getData(0 , "interests" , $args , $sql);
                    }
                    break;
                case "organization" : 
                    $sql = "SELECT * from organization where";
                    if($myrole == 'Following'){
                        $sqlm = "SELECT * FROM users_organization WHERE user_id = $userid ";
                        $hashset = array();
                        $set = array();
                        $st = $conn->execute($sqlm);
                        while($r = $st->fetch('assoc')){
                            $value = $r['organization_id'];
                            if (!array_key_exists($value, $hashset)){
                                array_push($set, $value);
                                $hashset[$value] = true;
                            }
                        }
                        $flag = false;
                        //return $set;
                        $sql .= " (";
                        foreach ($set as $i ) {
                            $flag = true;
                            $sql .= " id = ".$i." or";
                        }
                        if($flag){
                            $sql = rtrim($sql , "or");
                            $sql .= " ) and";
                        }else{
                            return $arr; //if no following have to return empty data can be replaced
                            $sql = rtrim($sql , " (");
                        }

                    }
                    if($query != ""){
                        $sql .= " Name like '%".$query."%' and";
                    }
                    $sql = rtrim($sql , 'where'); // when no if else is hit
                    $sql = rtrim($sql , 'and');
                    $sql .= " order by id  limit ".$skips.", ".$args['pagesize'];
                    //return $sql;
                    $st = $conn->execute($sql);
                    while($res = $st->fetch("assoc")){
                        $res['Logo'] = ($res['Logo'] != '' ? $this->getImgFullPath($res['Logo']) : "");
                        $res['cover_photo'] = ($res['cover_photo'] != '' ? $this->getImgFullPath($res['cover_photo']) : "");
                        $res["i_join"] = 0;
                        $res['follower_count'] = 0;
                        $sql = $conn->execute(sprintf("SELECT * FROM users_organization WHERE organization_id = %s", $res['id']));
                        if($r = $sql->fetch('assoc')){
                            if($r['user_id'] == $userid){$res['i_join'] = 1;}
                            $res['follower_count']++;
                        }
                        array_push($arr['organization'] , $res);
                    }
                    break;

                case "venue" :
                    $sql = "SELECT * from venue where";
                    if($myrole == 'Following'){
                        $sqlm = "SELECT * FROM users_venue WHERE user_id = $userid ";
                        $hashset = array();
                        $set = array();
                        $st = $conn->execute($sqlm);
                        while($r = $st->fetch('assoc')){
                            $value = $r['venue_id'];
                            if (!array_key_exists($value, $hashset)){
                                array_push($set, $value);
                                $hashset[$value] = true;
                            }
                        }
                        $flag = false;
                        //return $set;
                        $sql .= " (";
                        foreach ($set as $i ) {
                            $flag = true;
                            $sql .= " venue_id = ".$i." or";
                        }
                        if($flag){
                            $sql = rtrim($sql , "or");
                            $sql .= " ) and";
                        }else{
                            return $arr; //if no following have to return empty data can be replaced
                            $sql = rtrim($sql , " (");
                        }

                    }
                    if($query != ""){
                        $sql .= " venue_name like '%".$query."%' and";
                    }
                    $sql = rtrim($sql , 'where'); // when no if else is hit
                    $sql = rtrim($sql , 'and');
                    $sql .= " order by venue_id  limit ".$skips.", ".$args['pagesize'];
                    //return $sql;
                    $st = $conn->execute($sql);
                    while($res = $st->fetch("assoc")){
                        //$res['Logo'] = ($res['Logo'] != '' ? $this->getImgFullPath($res['Logo']) : "");
                        $res['cover_photo'] = ($res['cover_photo'] != '' ? $this->getImgFullPath($res['cover_photo']) : "");
                        $res["i_join"] = '0';
                        $res['follower_count'] = $conn->execute(sprintf("SELECT * FROM users_venue WHERE venue_id = %s ", $res['venue_id']))->count();
                        $sql = $conn->execute(sprintf("SELECT * FROM users_venue WHERE user_id = $userid and venue_id = %s ", $res['venue_id']));
                        if($r = $sql->fetch('assoc')){
                            $res['i_join'] = '1';
                        }
                        array_push($arr['venue'] , $res);
                    }
                    break;

                case "users" :
                    $sql = "SELECT user_id, user_email, user_fullname, user_firstname, user_lastname, user_gender, user_picture, user_work, user_work_title, user_work_place from users where 1 and";
                    //$sql = "SELECT * from users where";
                    if($myrole == 'Following'){
                        $sqlm = "SELECT * FROM followings WHERE user_id = $userid ";
                        $hashset = array();
                        $set = array();
                        $st = $conn->execute($sqlm);
                        while($r = $st->fetch('assoc')){
                            $value = $r['following_id'];
                            if (!array_key_exists($value, $hashset)){
                                array_push($set, $value);
                                $hashset[$value] = true;
                            }
                        }
                        $flag = false;
                        //return $set;
                        $sql .= " (";
                        foreach ($set as $i ) {
                            $flag = true;
                            $sql .= " user_id = ".$i." or";
                        }
                        if($flag){
                            $sql = rtrim($sql , "or");
                            $sql .= " ) and";
                        }else{
                            return $arr; //if no following have to return empty data can be replaced
                            $sql = rtrim($sql , " (");
                        }

                    }
                    if($query != ""){
                        $sql .= " (user_name like '%".$query."%' or user_firstname like '%".$query."%' or user_lastname like '%".$query."%' ) and";
                    }
                    //$sql = rtrim($sql , 'where'); // when no if else is hit
                    //$sql = rtrim($sql , 'and');



                    if(isset($args['myintrests']) && $args['myintrests'] == "1"){
                        $sql = rtrim($sql , 'and');
                        $sql .= " order by user_id DESC  ";
                        $m = $conn->execute($sql);
                        while($match = $m->fetch("assoc")){
                            $aid = "".$match['user_id']."#";
                            $percent = 0;
                            if($match['user_id'] != null)
                                $percent = $mi->get_mutual_interests($userid , $match['user_id'] , $rc);
                            $temp[$aid] = $percent;
                        }
                        arsort($temp);
                        $idar = array();
                        foreach ($temp as $key => $value) {
                            $key = rtrim($key , '#');
                            array_push($idar , $key);
                        }
                        for ($i=0; $i < count($idar); $i++) { 
                            if($lastid != 0){
                                if($idar[$i] == "$lastid"){
                                    $check = $args['pagesize'];
                                    $c = 0;
                                    while($c < $check ){
                                        if(!isset($idar[$i + $c + 1]))
                                            return $arr;
                                        array_push($arr['users'] , $this->getData($idar[$i + $c + 1] , "users" , $args));
                                        $c++;
                                    }
                                    break;
                                }else{continue;}
                            }else{
                                $check = $args['pagesize'];
                                while($i < $check ){
                                    if(!isset($idar[$i]))
                                        return $arr;
                                    array_push($arr['users'] , $this->getData($idar[$i] , "users" , $args));
                                    $i++;
                                }
                                break;
                            }
                        }
                    }elseif(isset($args['intrest']) && $args['intrest'] != ""){
                        $sql = rtrim($sql , 'and');
                        $sql .= " order by user_id DESC  ";
                        $m = $conn->execute($sql);
                        //return $sql;
                        
                        while($match = $m->fetch("assoc")){
                            $aid = "".$match['user_id']."#";
                            $percent = 0;
                            if($match['user_id'] != null)
                                $percent = $mi->get_mutual_interests_idncsv($match['user_id'] , $args['intrest'] , $rc);
                            $temp[$aid] = $percent;
                        }
                        arsort($temp);
                        //return $temp;
                        $idar = array();
                        foreach ($temp as $key => $value) {
                            $key = rtrim($key , '#');
                            array_push($idar , $key);
                        }
                        //return $idar;
                        for ($i=0; $i < count($idar); $i++) { 
                            if($lastid != 0){
                                if($idar[$i] == "$lastid"){
                                    $check = $args['pagesize'];
                                    $c = 0;
                                    while($c < $check ){
                                        if(!isset($idar[$i + $c + 1]))
                                            return $arr;
                                        $dat = $this->getData($idar[$i + $c + 1] , "users" , $args);
                                        array_push($arr['users'] , $dat);
                                        $c++;
                                    }
                                    break;
                                }else{continue;}
                            }else{
                                $check = $args['pagesize'];
                                while($i < $check ){
                                    //return $idar[$i];
                                    if(!isset($idar[$i]))
                                        return $arr;
                                    array_push($arr['users'] , $this->getData($idar[$i] , "users" , $args));
                                    $i++;
                                }
                                break;
                            }
                        }
                    }else{
                        $sql = rtrim($sql , 'and');
                        if($lastid != 0)
                            $sql .= " and user_id < $lastid order by user_id DESC  limit ".$skips.", ".$args['pagesize'];
                        else
                            $sql .= " order by user_id DESC  limit ".$skips.", ".$args['pagesize'];
                        //return $sql;
                        $arr['users'] = $this->getData(0 , "users" , $args , $sql);
                    }
                    break;

                case "posts":
                    $sql = "SELECT * FROM posts where (post_type = 'quiz' or post_type = 'debate' or post_type = 'poll' or post_type = 'article' or post_type = 'quickbit')  and (user_type = 'user' or user_type = 'institution' or (user_type = 'community' and is_approved_by_admin > 0) or user_type = 'organization') or";
                    if(isset($args['lastpostid']) and $args['lastpostid'] != ''){
                        $sql .= " post_id < $lastpostid and";
                    }
                    if($query != ""){
                        $sql .= " (post_title like '%".$query."%' ) and";
                    }
                    $sql = rtrim($sql , 'where'); // when no if else is hit
                    $sql = rtrim($sql , 'and');
                    $sql .= " order by post_id DESC limit ".$skips.", ".$args['pagesize'];

                    $pos = 1;
                    //return $sql;
                    $stmt = $conn->execute($sql);
                    $res = array();
                    $res['quiz'] = array();
                    $res['debate'] = array();
                    $res['poll'] = array();
                    $res['article'] = array();
                    $res['quickbit'] = array();
                    $type = explode(',', $type);
                    while($result = $stmt->fetch('assoc')){
                        if($result['post_type'] == 'quiz'){
                            $sql=sprintf("select * from quizzes where id='%s'",$result['quiz_id']);          

                            Log::debug("SQL : ".$sql);

                            $st = $conn->execute($sql);
                            if($temp = $st->fetch('assoc')){
                                $temp['pos'] = $pos;
                                $pos = $pos +1;
                                //if($temp['user_type'] == "institution"){$temp['user_type'] = 'venue';}
                                $dat = $this->userData($args['userid'] , $temp['creator_id'] , $temp['user_type'] , $temp['community_id']);
                                $temp['created_by'] = (isset($dat[0])? $dat[0] : null);
                                $temp['community_data'] = (isset($dat[1])? $dat[1] : null);
                                $temp['picture'] = $this->getImgFullPath($temp['picture'],1);
                                array_push($res['quiz'], $temp);
                            }


                        }elseif($result['post_type'] == 'debate'){
                            $sql=sprintf("select * from debates where id='%s'",$result['debate_id']);          

                            Log::debug("SQL : ".$sql);

                            $st = $conn->execute($sql);
                            if($temp = $st->fetch('assoc')){
                                $temp['pos'] = $pos;
                                $pos = $pos +1;
                                $dat = $this->userData($args['userid'] , $temp['creator_id'] , $temp['user_type'] , $temp['community_id']);
                                $temp['created_by'] = (isset($dat[0])? $dat[0] : null);
                                $temp['community_data'] = (isset($dat[1])? $dat[1] : null);
                                $temp['picture'] = $this->getImgFullPath($temp['picture'],1);
                                array_push($res['debate'], $temp);
                            }
                        }elseif($result['post_type'] == 'poll'){
                            $poll = $result;
                            $poll['title'] = $result['text'];
                            $dat=$conn->execute(sprintf("select * from posts_polls where post_id='%s'",$result['post_id']));
                            if($i = $dat->fetch("assoc")){

                            }else{
                                return ["message"=> "something went wrong" , "status"=> 0];
                            }
                            $sql=sprintf("select * from posts_polls_options where poll_id='%s'",$i['poll_id']);          

                            Log::debug("SQL : ".$sql);

                            $s = $conn->execute($sql);
                            $i = 0;
                            $poll['options'] = array();
                            while($r = $s->fetch('assoc')){
                                        
                                array_push($poll['options'], $r);
                            }
                            $poll['pos'] = $pos;
                            $pos = $pos +1;
                            array_push($res['poll'], $poll);
                        }elseif($result['post_type'] == 'article'){
                            $sql=sprintf("select * from posts_articles where post_id='%s'",$result['post_id']);          

                            Log::debug("SQL : ".$sql);

                            $s = $conn->execute($sql);
                            while($r = $s->fetch('assoc')){
                                $sql=sprintf("select * from articles where articles_id='%s'",$r['article_id']);          

                                Log::debug("SQL : ".$sql);

                                $st = $conn->execute($sql);
                                if($temp = $st->fetch('assoc')){
                                    $temp['pos'] = $pos;
                                    $pos = $pos +1;
                                    $dat = $this->userData($args['userid'] , $temp['created_by'] , $temp['user_type'] , $temp['community_id']);
                                    $temp['created_by'] = (isset($dat[0])? $dat[0] : null);
                                    $temp['community_data'] = (isset($dat[1])? $dat[1] : null);
                                    $temp['cover_photo'] = $this->getImgFullPath($temp['cover_photo'],1);
                                    array_push($res['article'], $temp);
                                }
                            }
                        }else{
                            $sql=sprintf("select * from quickbits where id='%s'",$result['quickbit_id']);          

                            Log::debug("SQL : ".$sql);

                            $st = $conn->execute($sql);
                            if($temp = $st->fetch('assoc')){
                                $temp['pos'] = $pos;
                                $pos = $pos +1;
                                $temp['cards'] = array();
                                $sql = sprintf("select * from quickbits_cards where quickbit_id = %s", $temp['id']);
                                $s = $conn->execute($sql);
                                while($a = $s->fetch('assoc')){
                                    if($a['picture'] != ''){
                                        $a['picture'] = $this->getImgFullPath($a['picture'],1);
                                    }
                                    array_push($temp['cards'], $a);
                                }
                                $dat = $this->userData($args['userid'] , $temp['creator_id'] , $temp['user_type'] , $temp['community_id']);
                                $temp['created_by'] = (isset($dat[0])? $dat[0] : null);
                                $temp['community_data'] = (isset($dat[1])? $dat[1] : null);
                                array_push($res['quickbit'], $temp);
                            }
                        }
                    }
                    $arr['posts'] = $res;
                    break;
                case "sessions":
                    $sql = "SELECT * FROM sessions where (user_type = 'user' or user_type = 'organization' or user_type = 'venue' or user_type = 'institution' or (user_type = 'community' and approved_by > 0)) and status != 0 and";
                    if($args['query'] != ''){
                        $sql .= " (title like '%".$args['query']."%') and";
                    }
                    if($args['usertype'] != 'user'){
                        $sql .= " user_type = '".$args['usertype']."' and";
                        if($args['usertype'] == 'community'){
                            $sql .= " community_id = ".$args['id']." and approved_by > 0 and";
                            if($args['myrole'] == "Initiated"){
                                $sql .= " created_by = ".$args['userid']." and";
                            }
                        }else{
                            $sql .= " created_by = ".$args['userid']." and";
                        }
                    }else{

                        if($args['myrole'] == "My Sessions"){
                            $sql .= " (user_type = 'user' or (user_type = 'community' and approved_by > 0)) and";
                            $sql .= " created_by = ".$args['userid']." and";
                        }

                    }
                    $date = "".substr($date , 0,10)."";
                    if(isset($args['timestatus'])){
                        if($timestatus == 'upcoming'){
                            $sql .= " event_date > ".$date." and";
                        }elseif($timestatus == 'passed'){
                            $sql .= " event_date < ".$date." and";
                        }
                    }

                    if(isset($args['status']) && $args['status'] != ''){
                        $sql .= " status = $status and";
                    }
                    switch($myrole){
                        case "Initiated":
                            break;
                        case "Conducted":
                            $sql .= " (presentors like '%,$userid,%' or presentors like '$userid,%' or presentors like '%,$userid' or presentors like '$userid') and";
                            break;
                        case "Attended":
                            $sqlm = "select * from sessions_attends where user_id = $userid";
                            $s = $conn->execute($sqlm);
                            $hashset = array();
                            $set = array();
                            while($r = $s->fetch('assoc')){
                                $value = $r['sessions_id'];
                                if (!array_key_exists($value, $hashset)){
                                    array_push($set, $value);
                                    $hashset[$value] = true;
                                }
                            }
                            $flag = false;
                            //return $set;
                            $sql .= " (";
                            foreach ($set as $i ) {
                                $flag = true;
                                $sql .= " sessions_id = ".$i." or";
                            }
                            if($flag){
                                $sql = rtrim($sql , "or");
                                $sql .= " ) and";
                            }else{
                                return $arr; //if no following have to return empty data can be replaced
                                $sql = rtrim($sql , " (");
                            }
                            break;
                        case "Joined":
                            $sqlm = "select * from sessions_joins where user_id = $userid";
                            $hashset = array();
                            $set = array();
                            $s = $conn->execute($sqlm);
                            while($r = $s->fetch('assoc')){
                                $value = $r['sessions_id'];
                                if (!array_key_exists($value, $hashset)){
                                    array_push($set, $value);
                                    $hashset[$value] = true;
                                }
                            }
                            $flag = false;
                            //return $set;
                            $sql .= " (";
                            foreach ($set as $i ) {
                                $flag = true;
                                $sql .= " sessions_id = ".$i." or";
                            }
                            if($flag){
                                $sql = rtrim($sql , "or");
                                $sql .= " ) and";
                            }else{
                                return $arr; //if no following have to return empty data can be replaced
                                $sql = rtrim($sql , " (");
                            }
                            break;
                    }
                    if(isset($args['venue']) && $args['venue'] != ""){
                        $sql .= " (venue like '%,$venue,%' or venue like '$venue,%' or venue like '%,$venue' or venue like '$venue') and";
                    }
                    if(isset($args['time']) && $args['time'] != ''){
                        if($time == 'Within_10_days'){
                            $sql .= " event_date - 10 <= ".$date." and event_date > $date and";
                        }elseif($time == "Range"){
                            if(isset($args['starttime']) && $args['starttime'] != '' && isset($args['endtime']) && $args['endtime'] != ''){
                                $sql .= " event_date > $starttime and event_date < $endtime and";
                            }else{
                                return $arr;
                            }
                        }
                    }
                    if(!isset($args['location'])){$args['location'] = "Any";}
                    switch($args['location']){
                        case "Within_10KM":
                            $sqlm = $conn->execute("SELECT user_latitude , user_longitude FROM users where user_id = $userid");
                            if($r = $sqlm->fetch('assoc')){
                                $ulat = $r['user_latitude'];
                                $ulot = $r['user_longitude'];
                            }else{return $arr;}
                            $sqlm = "select sessions_id , longitude , lattitude from sessions";
                            $s = $conn->execute($sqlm);
                            $hashset = array();
                            $set = array();
                            while($r = $s->fetch('assoc') )
                            {
                                $value = $r['sessions_id'];
                                $temp = $this->distance($ulat , $ulot , $r['lattitude'] , $r['longitude']);
                                if($temp  <= 10.00 ){
                                    if (!array_key_exists($value, $hashset)){
                                        array_push($set, $value);
                                        $hashset[$value] = true;
                                    }
                                }
                            }
                            $flag = false;
                            //return $set;
                            $sql .= " (";
                            foreach ($set as $i ) {
                                $flag = true;
                                $sql .= " sessions_id = ".$i." or";
                            }
                            if($flag){
                                $sql = rtrim($sql , "or");
                                $sql .= " ) and";
                            }else{
                                return $arr; //if no following have to return empty data can be replaced
                                $sql = rtrim($sql , " (");
                            }
                            break;
                        case "10KM_From_lat_long_provided":
                            if(isset($args['latitude']) and !empty($args['latitude']) and isset($args['longitude']) and !empty($args['longitude'])){
                                $ulat = $latitude;
                                $ulot = $longitude;
                            }
                            $sqlm = "select sessions_id , longitude , lattitude from sessions";
                            $s = $conn->execute($sqlm);
                            $hashset = array();
                            $set = array();
                            while($r = $s->fetch('assoc')){
                                $value = $r['sessions_id'];
                                if($this->distance($ulat , $ulot , $r['lattitude'] , $r['longitude']) <= 10.00 ){
                                    if (!array_key_exists($value, $hashset)){
                                        array_push($set, $value);
                                        $hashset[$value] = true;
                                    }
                                }
                            }
                            $flag = false;
                            //return $set;
                            $sql .= " (";
                            foreach ($set as $i ) {
                                $flag = true;
                                $sql .= " sessions_id = ".$i." or";
                            }
                            if($flag){
                                $sql = rtrim($sql , "or");
                                $sql .= " ) and";
                            }else{
                                return $arr; //if no following have to return empty data can be replaced
                                $sql = rtrim($sql , " (");
                            }
                            break;

                    }
                    $sql = rtrim($sql , 'where'); // when no if else is hit
                    $sql = rtrim($sql , 'and');


                    if(isset($args['myintrests']) && $args['myintrests'] == "1"){
                        $sql = rtrim($sql , 'and');
                        $sql .= " order by sessions_id DESC  ";
                        $m = $conn->execute($sql);
                        while($match = $m->fetch("assoc")){
                            $aid = "".$match['sessions_id']."#";
                            $percent = 0;
                            $intr = $conn->execute(sprintf("SELECT * FROM `sessions_interest` where sessions_id = %s" , $match['sessions_id']));
                            $temp = array();
                            while($ma = $intr->fetch('assoc')){
                                array_push($temp , $ma['interest']);
                            }
                            $match['interests'] = implode(",", $temp);
                            if($match['interests'] != null)
                                $percent = $mi->get_mutual_interests_idncsv($userid , $match['interests'] , $rc);
                            $temp[$aid] = $percent;
                        }
                        arsort($temp);
                        $idar = array();
                        foreach ($temp as $key => $value) {
                            $key = rtrim($key , '#');
                            array_push($idar , $key);
                        }
                        for ($i=0; $i < count($idar); $i++) { 
                            if($lastid != 0){
                                if($idar[$i] == "$lastid"){
                                    $check = $args['pagesize'];
                                    $c = 0;
                                    while($c < $check ){
                                        if(!isset($idar[$i + $c + 1]))
                                            return $arr;
                                        array_push($arr['sessions'] , $this->getData($idar[$i + $c + 1] , "sessions" , $args));
                                        $c++;
                                    }
                                    break;
                                }else{continue;}
                            }else{
                                $check = $args['pagesize'];
                                while($i < $check ){
                                    if(!isset($idar[$i]))
                                        return $arr;
                                    array_push($arr['sessions'] , $this->getData($idar[$i] , "sessions" , $args));
                                    $i++;
                                }
                                break;
                            }
                        }
                    }elseif(isset($args['intrest']) && $args['intrest'] != ""){
                        $sql = rtrim($sql , 'and');
                        $sql .= " order by sessions_id DESC  ";
                        $m = $conn->execute($sql);
                        //return $sql;
                        
                        while($match = $m->fetch("assoc")){
                            $aid = "".$match['sessions_id']."#";
                            $percent = 0;
                            $intr = $conn->execute(sprintf("SELECT * FROM `sessions_interest` where sessions_id = %s" , $match['sessions_id']));
                            $temp = array();
                            while($ma = $intr->fetch('assoc')){
                                array_push($temp , $ma['interest']);
                            }
                            $match['interests'] = implode(",", $temp);
                            if($match['interests'] != null)
                                $percent = $mi->get_mutual_interests_csvs($match['interests'] , $args['intrest'] , $rc);
                            $temp[$aid] = $percent;
                        }
                        arsort($temp);
                        //return $temp;
                        $idar = array();
                        foreach ($temp as $key => $value) {
                            $key = rtrim($key , '#');
                            array_push($idar , $key);
                        }
                        //return $idar;
                        for ($i=0; $i < count($idar); $i++) { 
                            if($lastid != 0){
                                if($idar[$i] == "$lastid"){
                                    $check = $args['pagesize'];
                                    $c = 0;
                                    while($c < $check ){
                                        if(!isset($idar[$i + $c + 1]))
                                            return $arr;
                                        $dat = $this->getData($idar[$i + $c + 1] , "sessions" , $args);
                                        array_push($arr['sessions'] , $dat);
                                        $c++;
                                    }
                                    break;
                                }else{continue;}
                            }else{
                                $check = $args['pagesize'];
                                while($i < $check ){
                                    //return $idar[$i];
                                    if(!isset($idar[$i]))
                                        return $arr;
                                    array_push($arr['sessions'] , $this->getData($idar[$i] , "sessions" , $args));
                                    $i++;
                                }
                                break;
                            }
                        }
                    }else{
                        $sql = rtrim($sql , 'and');
                        if($lastid != 0)
                            $sql .= " and sessions_id < $lastid order by sessions_id DESC  limit ".$skips.", ".$args['pagesize'];
                        else
                            $sql .= " order by sessions_id DESC  limit ".$skips.", ".$args['pagesize'];
                        //return $sql;
                        $arr['sessions'] = $this->getData(0 , "sessions" , $args , $sql);
                    }
                    break;


            }
            return $arr;



            
            
            //return 1;
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }

    private function checkFriend($uid , $cid){
        $conn = ConnectionManager::get('default');
        if($uid == '' or $cid == ''){return 0;}
        $stmt = $conn->execute("SELECT * from friends where (user_one_id = $uid and user_two_id = $cid) or (user_one_id = $cid and user_two_id = $uid) and status = 1");
        if($r = $stmt->fetch('assoc')){
            return 1;
        }else{
            return 0;
        }
    }
    private function addPct($ans){
        $codes = new Codes;
        $ans += $ans * ($codes->MF_MULTI / 100);
        return $ans;
    }

    private function getInterestPercentByPost($userId , $res){
        try{
            $codes = new Codes;
            $mi = new MutualInterestsDao;
            $ans = 0;
            $conn = ConnectionManager::get('default');
            //if($res['post_type'] == 'debate'){return 1000;}
            //$stmt = $conn->execute("SELECT * from posts where post_id = $postId");
            //$res = $stmt->fetch('assoc');
            switch($res["post_type"]){
                case 'article':
                    //return 100;
                    $id = $res['article_id'];
                    $result = $conn->execute("SELECT * from articles where articles_id = $id")->fetch('assoc');
                    if(!isset($result['interests']) or $result['interests'] == null or $result['interests'] == '')
                        $result['interests'] = '';
                    $ans = $mi->get_mutual_interests_idncsv($userId , $result['interests'] , $codes->RC_CONST );
                    //return $ans;
                    if(!isset($result['created_by']) or $result['created_by'] == null or $result['created_by'] == '')
                        return -1;
                    $check = $this->checkFriend($userId, $result['created_by']);
                    if($check  == 1 and ($result['user_type'] == 'user' || $result['user_type'] == 'User' || $result['user_type'] == 'community' || $result['user_type'] == 'Community')){
                        $ans = $this->addPct($ans);
                    }
                    if($check == 0 and $ans < $codes->INTEREST_THRESHOLD){$ans = -1;}
                    return $ans;
                    break;
                case 'quickbit':
                    $id = $res['quickbit_id'];
                    $result = $conn->execute("SELECT * from quickbits where id = $id")->fetch('assoc');
                    if(!isset($result['interests']) or $result['interests'] == null or $result['interests'] == '')
                        $result['interests'] = '';
                    if(isset($result['deleted']) and $result['deleted'] == 1)
                        return -1;
                    $ans = $mi->get_mutual_interests_idncsv($userId , $result['interests'] , $codes->RC_CONST );
                    if(!isset($result['creator_id']) or $result['creator_id'] == null or $result['creator_id'] == '')
                        return -1;
                    $check = $this->checkFriend($userId, $result['creator_id']);
                    if( $check == 1 and ($result['user_type'] == 'user' || $result['user_type'] == 'User' || $result['user_type'] == 'community' || $result['user_type'] == 'Community')){
                        $ans = $this->addPct($ans);
                    }
                    if($check == 0 and $ans < $codes->INTEREST_THRESHOLD){$ans = -1;}
                    return $ans;
                    break;
                case 'quiz':
                    //return -1;
                    $id = $res['quiz_id'];
                    $result = $conn->execute("SELECT * from quizzes where id = $id")->fetch('assoc');
                    if(!isset($result['interests']) or $result['interests'] == null or $result['interests'] == '')
                        $result['interests'] = '';
                    if(isset($result['deleted']) and $result['deleted'] == 1)
                        return -1;
                    $ans = $mi->get_mutual_interests_idncsv($userId , $result['interests'] , $codes->RC_CONST );
                    if(!isset($result['creator_id']) or $result['creator_id'] == null or $result['creator_id'] == '')
                        return -1;
                    $check = $this->checkFriend($userId, $result['creator_id']);
                    if( $check == 1 and ($result['user_type'] == 'user' || $result['user_type'] == 'User' || $result['user_type'] == 'community' || $result['user_type'] == 'Community')){
                        $ans = $this->addPct($ans);
                    }
                    if($check == 0 and $ans < $codes->INTEREST_THRESHOLD){$ans = -1;}
                    return $ans;
                    break;
                case 'debate':
                    $id = $res['debate_id'];
                    $result = $conn->execute("SELECT * from debates where id = $id")->fetch('assoc');
                    if(!isset($result['interests']) or $result['interests'] == null or $result['interests'] == '')
                        $result['interests'] = '';
                    if(isset($result['deleted']) and $result['deleted'] == 1)
                        return -1;
                    $ans = $mi->get_mutual_interests_idncsv($userId , $result['interests'] , $codes->RC_CONST );
                    if(!isset($result['creator_id']) or $result['creator_id'] == null or $result['creator_id'] == '')
                        return -1;
                    $check = $this->checkFriend($userId, $result['creator_id']);
                    if( $check == 1 and ($result['user_type'] == 'user' || $result['user_type'] == 'User' || $result['user_type'] == 'community' || $result['user_type'] == 'Community')){
                        $ans = $this->addPct($ans);
                    }
                    if($check == 0 and $ans < $codes->INTEREST_THRESHOLD){$ans = -1;}
                    return $ans;
                    break;
                case 'session':
                    $id = $res['session_id'];
                    $temp = array();
                    $result = $conn->execute("SELECT * from sessions_interest where sessions_id = $id");
                    while($r = $result->fetch('assoc')){array_push($temp , $r['interest']);}
                    $ans = $mi->get_mutual_interests_idncsv($userId , implode(',', $temp) , $codes->RC_CONST );
                    $result = $conn->execute("SELECT * from sessions where sessions_id = $id")->fetch('assoc');
                    if(!isset($result['creator_id']) or $result['creator_id'] == null or $result['creator_id'] == '')
                        return -1;
                    $check = $this->checkFriend($userId, $result['created_by']);
                    if( $check == 1 and ($result['user_type'] == 'user' || $result['user_type'] == 'User' || $result['user_type'] == 'community' || $result['user_type'] == 'Community')){
                        $ans = $this->addPct($ans);
                    }
                    if($check == 0 and $ans < $codes->INTEREST_THRESHOLD){$ans = -1;}
                    return $ans;
                    break;
                case 'poll':
                    $id = $res['poll_id'];
                    $result = $conn->execute("SELECT * from polls where id = $id")->fetch('assoc');
                    if(!isset($result['interests']) or $result['interests'] == null or $result['interests'] == '')
                        $result['interests'] = '';
                    $ans = $mi->get_mutual_interests_idncsv($userId , $result['interests'] , $codes->RC_CONST );
                    if(!isset($result['creator_id']) or $result['creator_id'] == null or $result['creator_id'] == '')
                        return -1;
                    $check = $this->checkFriend($userId, $result['creator_id']);
                    if( $check == 1 and ($result['user_type'] == 'user' || $result['user_type'] == 'User' || $result['user_type'] == 'community' || $result['user_type'] == 'Community')){
                        $ans = $this->addPct($ans);
                    }
                    if($check == 0 and $ans < $codes->INTEREST_THRESHOLD){$ans = -1;}
                    return $ans;
                    break;


            }
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }
    private function getDataByPost($userId , $res){
        try{
            $codes = new Codes;
            $mi = new MutualInterestsDao;
            $ans = 0;
            $dat = new DateUtils;
            $date = $dat->getCurrentDate();
            $articles = new ArticlesDao;
            $quiz = new QuizzesDao;
            $debate = new DebatesDao;
            $qb = new QuickBitsDao;
            $conn = ConnectionManager::get('default');
            //$stmt = $conn->execute("SELECT * from posts where post_id = $postId");
            //$res = $stmt->fetch('assoc');
            switch($res["post_type"]){
                case 'article':
                    $ans = $articles->getArticlesByArticlesId($res['article_id'] , $userId);
                    return $ans;
                    break;
                case 'quickbit':
                    $id = $res['quickbit_id'];
                    $ans = $qb->getQuickBitByUserAndId($id , $userId , $date);
                    return $ans;
                    break;
                case 'quiz':
                    $id = $res['quiz_id'];
                    $ans = $quiz->getQwizByUserAndId($id , $userId , $date);
                    unset($ans['quizQuestionArr']);
                    return $ans;
                    break;
                case 'debate':
                    $id = $res['debate_id'];
                    $ans = $debate->getDebateByUserAndId($id , $userId , $date);
                    return $ans;
                    break;
                case 'session':
                    $id = $res['session_id'];
                    $args = array('userid' => $userId);
                    $ans = $this->getData($id , 'sessions' , $args );
                    return $ans;
                    break;
                case 'poll':
                    $id = $res['poll_id'];
                    $ans = $this->getPollData($id , $userId)['poll'];
                    return $ans;
                    break;
            }
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }
    public function cmp($a, $b)
    {
        if ($a['interest_match'] == $b['interest_match']) {
            return ($a['post_id'] > $b['post_id']) ? -1 : 1;
        }
        return ($a['interest_match'] > $b['interest_match']) ? -1 : 1;
    }


    public function newsFeed($userId , $csv = '', $viewCheck = 1 , $max_results = 10){
        try{
            $codes = new Codes;
            //return $viewCheck;
            $conn = ConnectionManager::get('default');
            //$max_results = $codes->MAX_RESULTS_NF;
            $dat = new DateUtils;
            $date = $dat->getCurrentDate();
            $dates = "".$date;
            $date_threshold = $codes->DATE_THRESHOLD;
            $str = "-$date_threshold days";
            $date_m = date('Y-m-d', strtotime($str, strtotime($dates))); 
            $date_ms = "".$date_m;
            if($csv != ''){
                $arr = explode(',',$csv);
                $stmt = $conn->execute("SELECT posts from posts_seen where user_id = $userId");
                if($res = $stmt->fetch('assoc')){
                    $seens = explode(',', $res['posts']);
                    $add = array_unique ( array_merge ( $seens , $arr ));
                    $add = trim(implode(",", $add) , ',');
                    $conn->execute("update posts_seen set posts = '$add' where user_id = $userId");
                }else{
                    $add = $csv;
                    $conn->execute("INSERT into posts_seen set posts = '$add' , user_id = $userId");
                }
            }
            $vCheck = '';
            if($viewCheck == 1){
                $stmt = $conn->execute("SELECT posts from posts_seen where user_id = $userId");
                if($res = $stmt->fetch('assoc')){
                    $seens = $res['posts'];
                }else{
                    $conn->execute("insert into posts_seen set user_id = $userId");
                    $seens = "";
                }
                $vCheck = " and not FIND_IN_SET(post_id , '$seens')";
            }
            //return $vCheck;
            $ch = implode(',' , array('quickbit' , 'article' , 'poll' , 'debate' , 'quiz' , 'session'));
            $result = array();
            $sql = sprintf("SELECT * from posts where updated_at > '$date_ms' $vCheck and FIND_IN_SET(post_type , '$ch') order by updated_at desc");
            //return $sql;
            $stmt = $conn->execute($sql);
            while($res = $stmt->fetch('assoc')){
                $res['interest_match'] = $this->getInterestPercentByPost($userId , $res);
                if($res['interest_match'] == -1){continue;}
                array_push($result , $res);
            }

            //sorting array according to interest_match feid we compute earlier
            usort($result , function($a , $b) use ($result){
                if ($a['interest_match'] == $b['interest_match']) {
                    return ($a['post_id'] > $b['post_id']) ? -1 : 1;
                }
                return ($a['interest_match'] > $b['interest_match']) ? -1 : 1;
            }); 
            //array_multisort(array_column($result , 'interest_match' , 'post_id'), SORT_DESC , $result);

            //testing if next view offset is fetch
            $skips = 0;
            if($viewCheck == 0){
                if($stmt = $conn->execute("SELECT * from posts_seen where user_id = $userId")->fetch('assoc')){
                    $skips = $max_results * $stmt['offset'];
                    $conn->execute("UPDATE posts_seen set view_check = 0 , offset = offset + 1 WHERE user_id = $userId");
                }else{
                    $conn->execute("insert into posts_seen set view_check = 0 , offset = 1 , user_id = $userId");
                }
            }else{
                $conn->execute("UPDATE posts_seen set view_check = 1, offset = 0 where user_id = $userId");
            }
            $count = count($result);
            //return $viewCheck;
            if($result == [] and $viewCheck == 1 and $count < $max_results){
                return $this->newsFeed($userId , '', 0);
            }
            //return $viewCheck;
            if($skips != 0)
                $result = array_slice($result, $skips , $count );
            $count = count($result);
            $result = array_slice($result , 0 , $max_results);//taking out the data that is to be sent
            //return $result;
            $results = array();
            foreach($result as $res){
                $res[$res['post_type'].'_data'] = $this->getDataByPost($userId , $res);
                $f = array();
                $friends = array();
                if($res['total_user_ids'] != null and $res['total_user_ids'] != ''){
                    $f = explode(',', $res['total_user_ids']);
                    $get_friends = $conn->execute(sprintf('SELECT users.user_id FROM friends INNER JOIN users ON (friends.user_one_id = users.user_id AND friends.user_one_id != %1$s) OR (friends.user_two_id = users.user_id AND friends.user_two_id != %1$s) WHERE status = 1 AND (user_one_id = %1$s OR user_two_id = %1$s)', $userId));
                    if ($get_friends->count() > 0) {
                        while ($friend = $get_friends->fetch("assoc")) {
                            $friends[] = $friend['user_id'];
                        }
                    }
                }
                $ids = array_intersect($f, $friends);
                $share_count = count($ids);
                $fsd = [];
                if($share_count != 0){
                    foreach ($ids as $i ) {
                        $fsd[] = $this->userData($userId , $i, 'user', 0)[0];
                    }
                }
                $res[$res['post_type'].'_data']['friend_share_count'] = $share_count;
                $res[$res['post_type'].'_data']['friend_share_data'] = $fsd;
                array_push($results , $res);
            }
            return $results;

        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }



    
}