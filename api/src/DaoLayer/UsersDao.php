<?php

namespace App\DaoLayer;

use App\Utils\Codes;
use App\Utils\DateUtils;
use App\DaoLayer\PostsDao;
use App\Model\UserModel;
use Cake\Core\Exception\Exception;
use Cake\Datasource\ConnectionManager;
use Cake\Log\Log;
use DateInterval;
use DateTime;


class UsersDao
{
    static $conn = null;
    /**
     * 
     */
    private function getImgFullPath($img = '', $newPath = 0){
        $codes = new Codes;
        return !empty($img) ?$codes->SYSTEM_URL.'/'.($newPath?'api/':'').$codes->UPLOADS_DIRECTORY.'/'.$img:'';
    }






    public function updateStatusByUserId($user_id)
    {
        Log::debug("Started ...updateStatusByUserId Dao : User Id : ".$user_id);

        try{
            
            $conn = ConnectionManager::get('default');

            $sql=sprintf("UPDATE `users` SET `user_activated`='1' WHERE `user_id`='%s'",$user_id);

            Log::debug("SQL : ".$sql);

            $conn->execute($sql);

            Log::debug("Ended ...updateStatusByUserId Dao");

        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }


    public function getFriendIdsByUserId($userId)
    {
        Log::debug("Started ...getFriendIdsByUserId Dao");

        try{
            
            $conn = ConnectionManager::get('default');

            $sql=sprintf('SELECT users.user_id FROM friends INNER JOIN users 
            ON (friends.user_one_id = users.user_id 
            AND friends.user_one_id != %1$s) 
            OR (friends.user_two_id = users.user_id 
            AND friends.user_two_id != %1$s) 
            WHERE status = 1 
            AND (user_one_id = %1$s OR user_two_id = %1$s)',$userId);

            Log::debug("SQL : ".$sql);

            $stmt = $conn->execute($sql);

            $results = array();

            while($result = $stmt->fetch("assoc")) {
                array_push($results,$result);
            }

            Log::debug("Ended ...getFriendIdsByUserId Dao");

            return $results;
            
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }

    public function updateLiveNotificationCounter($userId)
    {
        Log::debug("Started ...updateLiveNotificationCounter Dao");

        try{
            
            $conn = ConnectionManager::get('default');

            $conn->execute(sprintf("UPDATE users SET user_live_notifications_counter = user_live_notifications_counter + 1 WHERE user_id = %s", $userId));   

            Log::debug("Ended ...updateLiveNotificationCounter Dao");

            return true;
            
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }


    /**
     * 
     */
    public function getFriendRequestsToUserByUserId($userId,$offset,$maxResults, $lastRequestId)
    {
        Log::debug("Started ...getFriendRequestsToUserByUserId Dao");

        try{
            
            $conn = ConnectionManager::get('default');

            if ($lastRequestId !== null) {
                $sql = sprintf("SELECT friends.id, friends.user_one_id as user_id, 
                users.user_name, users.user_fullname, users.user_gender, 
                users.user_picture,users.user_work,users.user_work_title,
                users.user_work_place,users.user_current_city 
                FROM friends INNER JOIN users ON friends.user_one_id = users.user_id 
                WHERE friends.status = 0 AND friends.user_two_id = %s 
                AND friends.id > %s ORDER BY friends.id DESC",
                 $userId, 
                 $lastRequestId);
            } else {
                $sql = sprintf("SELECT friends.id, friends.user_one_id as user_id, 
                users.user_name, users.user_fullname, 
                users.user_gender, users.user_picture,users.user_work,
                users.user_work_title,users.user_work_place,
                users.user_current_city FROM friends INNER JOIN 
                users ON friends.user_one_id = users.user_id 
                WHERE friends.status = 0 AND friends.user_two_id = %s 
                ORDER BY friends.id DESC LIMIT %s, %s", 
                $userId, 
                $offset, 
                $maxResults);
            }
    

            Log::debug("SQL : ".$sql);

            $stmt = $conn->execute($sql);

            $results = array();

            while($result = $stmt->fetch("assoc")) {
                array_push($results,$result);
            }

            Log::debug("Ended ...getFriendRequestsToUserByUserId Dao");

            return $results;
            
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }

    /**
     * 
     */
    public function getFriendRequestsSentByUserId($userId,$offset,$maxResults)
    {
        Log::debug("Started ...getFriendRequestsSentByUserId Dao");

        try{
            
            $conn = ConnectionManager::get('default');

            $sql=sprintf("SELECT friends.user_two_id as user_id, users.user_name, 
            users.user_fullname, users.user_gender, users.user_picture,
            users.user_work,users.user_work_title,users.user_work_place,
            users.user_current_city 
            FROM friends INNER JOIN users 
            ON friends.user_two_id = users.user_id 
            WHERE friends.status = 0 AND friends.user_one_id = %s 
            LIMIT %s, %s", 
            $userId, 
            $offset, 
            $maxResults);

            Log::debug("SQL : ".$sql);

            $stmt = $conn->execute($sql);

            $results = array();

            while($result = $stmt->fetch("assoc")) {
                array_push($results,$result);
            }

            Log::debug("Ended ...getFriendRequestsSentByUserId Dao");

            return $results;
            
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }



    public function getUserByUserId($userId)
    {
        Log::debug("Started ...updateUserByUserId Dao");

        try{
            
            $conn = ConnectionManager::get('default');

            $sql=sprintf("SELECT * from users where user_id = '%s'", $userId);

            Log::debug("SQL : ".$sql);

            $stmt = $conn->execute($sql);

            $result = $stmt->fetch("assoc");

            Log::debug("Ended ...updateUserByUserId Dao");

            return $result;
            
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }

    }

    /**
     * 
     */
    public function getUserByUserIdAndLoginOtp($userId,$loginOtp)
    {
        Log::debug("Started ...getUserByUserIdAndLoginOtp Dao : User Id : ".$userId.", "." Login OTP : ".$loginOtp);

        try{
            
            $conn = ConnectionManager::get('default');

            $sql=sprintf("SELECT user_id,user_phone,user_fullname,user_email,user_started,two_step_verifications from users where user_id = '%s' AND login_otp = '%s'", $userId,$loginOtp);

            Log::debug("SQL : ".$sql);

            $stmt = $conn->execute($sql);

            $result = $stmt->fetch("assoc");

            Log::debug("Ended ...getUserByUserIdAndLoginOtp Dao");

            return $result;
            
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }

    }
    


    /**
     * 
     */
    public function updateUserByUserId($args,$venue_id,$user_id)
    {
        Log::debug("Started ...updateUserByUserId Dao");

        try{
            
            $conn = ConnectionManager::get('default');

            $sql=sprintf("UPDATE users SET user_work = '%s' ,
            user_work_title = '%s' ,  user_work_place = '%s',organisation_id='%s'
            WHERE user_id = '%s'", $args['user_work'],$args['work_title'],
            $args['work_place'],$venue_id, 
            $user_id);

            Log::debug("SQL : ".$sql);

            $stmt = $conn->execute($sql);

            Log::debug("Ended ...updateUserByUserId Dao");
            
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }


    /**
     * 
     */
    public function updateLoginOtp($userId,$otp)
    {
        Log::debug("Started ...updateLoginOtp Dao : User Id : ".$userId.", OTP : ".$otp);

        try{            
            $conn = ConnectionManager::get('default');

            $sql=sprintf("UPDATE `users` SET login_otp = '%s' WHERE user_id = '%s' ", 
            $otp, $userId);

            Log::debug("SQL : ".$sql);

            $conn->execute($sql);

            Log::debug("Ended ...updateLoginOtp Dao");
            
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }

    /**
     * 
     */
    public function updateUserResetKeyByUserEmail($userEmail,$resetKey)
    {
        Log::debug("Started ...updateUserResetKeyByUserEmail Dao : User Email : ".$userEmail.", Reset Key : ".$resetKey);

        try{           
            $conn = ConnectionManager::get('default');

            $sql=sprintf("UPDATE users SET user_reset_key = '%s', user_reseted = '1' WHERE user_email = '%s'", $resetKey,$userEmail);

            Log::debug("SQL : ".$sql);

            $conn->execute($sql);

            Log::debug("Ended ...updateUserResetKeyByUserEmail Dao");
            
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }

    /**
     * 
     */
    public function updateUserResetKeyByUserMobile($userMobile,$resetKey)
    {
        Log::debug("Started ...updateUserResetKeyByUserMobile Dao : User Mobile : ".$userMobile.", Reset Key : ".$resetKey);

        try{           
            $conn = ConnectionManager::get('default');

            $sql=sprintf("UPDATE users SET user_reset_key = '%s', user_reseted = '1' WHERE user_phone = '%s'", $resetKey,$userMobile);

            Log::debug("SQL : ".$sql);

            $conn->execute($sql);

            Log::debug("Ended ...updateUserResetKeyByUserMobile Dao");
            
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }

    public function updateLastLoginByUserId($userId)
    {
        Log::debug("Started ...updateLastLoginByUserId Dao : User Id : ".$userId);

        try{

            $dateUtils = new DateUtils;
            
            $conn = ConnectionManager::get('default');

            $sql=sprintf("UPDATE users SET user_last_login ='%s'
            WHERE user_id = '%s'", $dateUtils->getCurrentDate(), $userId);

            Log::debug("SQL : ".$sql);

            $conn->execute($sql);

            Log::debug("Ended ...updateLastLoginByUserId Dao");
            
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }


    /**
     * 
     */
    public function updateUserPasswordByUserEmail($userEmail,$password)
    {
        Log::debug("Started ...updateUserPasswordByUserEmail Dao : User Email : ".$userEmail.", Password : ".$password);

        try{

            $dateUtils = new DateUtils;
            
            $conn = ConnectionManager::get('default');

            $sql=sprintf("UPDATE users SET user_password = '%s', user_reseted = '0' 
            WHERE user_email = '%s'", md5($password), $userEmail);

            Log::debug("SQL : ".$sql);

            $conn->execute($sql);

            Log::debug("Ended ...updateUserPasswordByUserEmail Dao");
            
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }


        /**
     * 
     */
    public function updateUserPasswordByUserPhone($userMobile,$password)
    {
        Log::debug("Started ...updateUserPasswordByUserPhone Dao : User Phone : ".$userMobile.", Password : ".$password);

        try{

            $dateUtils = new DateUtils;
            
            $conn = ConnectionManager::get('default');

            $sql=sprintf("UPDATE users SET user_password = '%s', user_reseted = '0' 
            WHERE user_phone = '%s'", md5($password), $userMobile);

            Log::debug("SQL : ".$sql);

            $conn->execute($sql);

            Log::debug("Ended ...updateUserPasswordByUserPhone Dao");
            
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }


    /**
     * 
     */
    public function getUsersByUserEmailOrUserPhoneAndPassword($emPhone,$password)
    {
        Log::debug("Started ...getUsersByUserEmailOrUserPhoneAndPassword Dao : Email / Phone  : ".$emPhone." : Password : ".$password);

        try{
            
            $conn = ConnectionManager::get('default');

            $sql=sprintf("SELECT * FROM users WHERE  (user_email = '%s' OR user_phone= '%s') AND user_password = '%s'", $emPhone,$emPhone,md5($password));

            Log::debug("SQL : ".$sql);

            $stmt = $conn->execute($sql);

            if(!$result = $stmt->fetch("assoc")){return null;}
            $result['user_cover'] = $this->getImgFullPath($result['user_cover'], 1);
            $result['friend_count'] = 0;
            $sql = sprintf("SELECT * FROM friends WHERE (user_one_id = %s or user_two_id = %s) and status = 1",$result['user_id'],$result['user_id']);
            $st = $conn->execute($sql);
            $result['friend_count'] = "". $st->count() ."";

            $result['interest_count'] = 0;
            $sql = sprintf("SELECT * FROM user_interest WHERE user_id = %s ",$result['user_id']);
            $st = $conn->execute($sql);
            $result['interest_count'] = "". $st->count() ."";


            $arr = [];

            $sql=sprintf("SELECT * FROM organization WHERE user_id = '%s' ", $result['user_id']);

            Log::debug("SQL : ".$sql);

            $stmt = $conn->execute($sql);
            while($res = $stmt->fetch('assoc')){
                $res['Logo'] = $this->getImgFullPath($res['Logo'], 1);
                if($res['cover_photo'] != '' && $res['cover_photo'] != null){$res['cover_photo'] = $this->getImgFullPath($res['cover_photo'], 1); }

                array_push($arr, $res);
            }

            if($arr == []){
                $result['organisation_data'] = null;
            }else{
                $result['organisation_data'] = $arr;
            }

            $arr = [];

            $sql=sprintf("SELECT * FROM communities_members WHERE user_id = '%s' and (role = 2 or role = 1) ", $result['user_id']);

            Log::debug("SQL : ".$sql);
            $st = $conn->execute($sql);

            while($re = $st->fetch('assoc')){
                $sql=sprintf("SELECT * FROM communities WHERE id = '%s'  ", $re['community_id']);
                Log::debug("SQL : ".$sql);
                $s = $conn->execute($sql);
                $r = $s->fetch('assoc');
                $r['picture'] = $this->getImgFullPath($r['picture'], 1);
                $r['cover_picture'] = $this->getImgFullPath($r['cover_picture'], 1);
                $r['role'] = $re['role'];
                array_push($arr, $r);
            }
            $result['community_data'] = $arr;

            $result['venue'] = array();
            $sql = sprintf("SELECT * FROM venue where userid = %s" , $result['user_id']);
            Log::debug("SQL : ".$sql);
            $st = $conn->execute($sql);
            while($re = $st->fetch('assoc')){
                $re['logo'] = $this->getImgFullPath($re['cover_photo'],1);
                $re['cover_photo'] = array();
                $sql = sprintf("SELECT photo , id FROM venue_image where venue_id = %s" , $re['venue_id']);
                $s = $conn->execute($sql);
                while($r = $s->fetch('assoc')){
                    $r['photo'] = $this->getImgFullPath($r['photo'] , 1);
                    array_push($re['cover_photo'], $r);
                }
                array_push($result['venue'], $re);
            }

            Log::debug("Ended ...getUsersByUserEmailOrUserPhoneAndPassword Dao");
            $result["luneblaze_points"] = '0';//to be set later
            return $result;
            
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }        

    /**
     * 
     */
    public function getUsersByUserEmail($email)
    {
        Log::debug("Started ...getUsersByUserEmail Dao");

        try{
            
            $conn = ConnectionManager::get('default');

            $sql=sprintf("SELECT * FROM users WHERE  user_email = '%s' and user_activated='1'", $email);

            Log::debug("SQL : ".$sql);

            $stmt = $conn->execute($sql);

            $result = $stmt->fetch("assoc");

            Log::debug("Ended ...getUsersByUserEmail Dao");

            return $result;
            
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }        
    /**
     * 
     */
    public function getUsersByUserPhone($mobile)
    {
        Log::debug("Started ...getUsersByUserPhone Dao");

        try{
            
            $conn = ConnectionManager::get('default');

            $sql=sprintf("SELECT * FROM users WHERE  user_phone = '%s' and user_activated='1'", $mobile);

            Log::debug("SQL : ".$sql);

            $stmt = $conn->execute($sql);

            $result = $stmt->fetch("assoc");

            Log::debug("Ended ...getUsersByUserPhone Dao");

            return $result;
            
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }        

    /**
     * Save user
     */
    public function saveUser($user_name, $args, $user_full_name, $date, $activation_key)
    {   
        Log::debug("Started ...saveUser Dao:  Username : ".$user_name.", User Full Name : ".$user_full_name." Date : ".$date.", Activation Key : ".$activation_key.", Arguments : ".json_encode($args));

        try{

        $conn = ConnectionManager::get('default');

        $sql=sprintf("INSERT INTO users (user_name, user_email, user_phone, user_password, current_FOI, user_firstname, user_lastname, user_fullname, user_gender, user_registered, user_activation_key, user_birthdate,privacy_connection_request,user_privacy_friends,privacy_tag,privacy_invite_session,privacy_invite_conduct_session,privacy_invite_answer,privacy_invite_interest,privacy_message,privacy_choices,user_privacy_basic,privacy_content) VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s','public','friends','friends','friends','friends','friends','friends','public','public','public','public')", 
        $user_name.uniqid(), $args['email'], $args['phone'], md5($args['password']), $args['current_FOI'], ucwords($args['first_name']), ucwords($args['last_name']), $user_full_name, $args['gender'], $date, $activation_key, $args['birth_date']);

        Log::debug("SQL : ".$sql);

        $conn->execute($sql);
        
        $sql="SELECT LAST_INSERT_ID()";
        
        $stmt = $conn->execute($sql);
        
        $res = $stmt->fetch();
       
        Log::debug("Ended ...saveUser Dao");
        
        return $res[0];
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }


    	/**
	 * Update Users phone and email using user_id
	 */
	public function updateUsersPhoneAndEmailByUserId($userPhone, $userEmail, $userId)
	{
		Log::debug("Started ...updateUsersPhoneAndEmailByUserId Dao : User Phone : ".$userPhone.", User Email : ".$userEmail.", User Id : ".$userId);

		try{
			
			$conn = ConnectionManager::get('default');

			$sql=sprintf("UPDATE users set user_phone = COALESCE(%s,user_phone) , user_email = COALESCE(%s,user_email) WHERE user_id = %s", $userPhone, $userEmail, $userId);

			Log::debug("SQL : ".$sql);

			$conn->execute($sql);

			Log::debug("Ended ...updateUsersPhoneAndEmailByUserId Dao");

		}catch(\Exception $e){
			Log::debug($e);
			throw new Exception($e);
		}
	}


    /**
     * 
     */
    public function updateUserQuizCountByUserId($total, $user_id)
    {
        Log::debug("Started ...updateUserQuizCountByUserId Dao");

        try{
            
            $conn = ConnectionManager::get('default');

            $sql=sprintf("UPDATE users SET quiz_count = '%s'
                WHERE user_id = '%s'", $total, $user_id);

            Log::debug("SQL : ".$sql);

            $stmt = $conn->execute($sql);

            Log::debug("Ended ...updateUserQuizCountByUserId Dao");
            
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }

    /**
     * 
     */
    public function updateUserDebateCountByUserId($total, $user_id)
    {
        Log::debug("Started ...updateUserDebateCountByUserId Dao");

        try{
            
            $conn = ConnectionManager::get('default');

            $sql=sprintf("UPDATE users SET debate_count = '%s'
                WHERE user_id = '%s'", $total, $user_id);

            Log::debug("SQL : ".$sql);

            $stmt = $conn->execute($sql);

            Log::debug("Ended ...updateUserDebateCountByUserId Dao");
            
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }

    /*NEW CODE*/
    
    public function getSystem(){
        Log::debug("Started ... getSystem Dao");

        try{
            $conn = ConnectionManager::get('default');

            $sql=sprintf('SELECT * FROM system_options');
            $stmt = $conn->execute($sql);

            $results = array();
            while($result = $stmt->fetch("assoc")) {
                array_push($results,$result);
            }

            return $results;

        } catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }   

    public function getAssessmentTypes($userId){
        Log::debug("Started ... getAssessmentTypes Dao");

        try{
            $conn = ConnectionManager::get('default');

            $sql=sprintf('SELECT id, type as name FROM assessmenttype');
            $stmt = $conn->execute($sql);

            $results = array();
            while($result = $stmt->fetch("assoc")) {
                array_push($results, $result);
            }

            return $results;

        } catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }

    public function getCategoriesByParent($parentId){
        Log::debug("Started ... getAssessmentCategories Dao");

        try{
            $conn = ConnectionManager::get('default');

            $sql = sprintf('SELECT distinct cm.category_id,cm.text,cm.parent_id,cm.status FROM category_mst cm WHERE cm.parent_id = %s', $parentId);
            $stmt = $conn->execute($sql);

            $results = array();
            $i = 0;
            while($result = $stmt->fetch("assoc")) {
                
                $results[$i] = $result;
                $results[$i]['child'] = array();
                $i++;
            }
            

            return $results;

        } catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }
    
    public function getAssessmentCategories($userId){
        Log::debug("Started ... getAssessmentCategories Dao");

        try{
            $conn = ConnectionManager::get('default');

            $parentId = 0;
            $results = $this->getCategoriesByParent($parentId);
            
            $i = 0;
            foreach($results as $r){
                $results[$i]['child'] = $this->getCategoriesByParent($r['category_id']);
                $j = 0;
                foreach($results[$i]['child'] as $rs){
                    $results[$i]['child'][$j]['child'] = $this->getCategoriesByParent($rs['category_id']);
                    $j++;
                }
                $i++;
            }



            return $results;

        } catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }



    public function getAssessmentList($userId){
        Log::debug("Started ... getAssessmentList Dao");

        try{
            $codes = new Codes;
            $conn = ConnectionManager::get('default');

            $notZero = 0;
            $sql=sprintf('SELECT ap.*,at.type  FROM assessment_payment ap LEFT JOIN assessmenttype at ON at.id = ap.assessment_type_id WHERE ap.user_id = %s  and paymentstatus=1 ORDER BY ap.ondate DESC', $userId);
            $stmt = $conn->execute($sql);

            $response = array();
            $count=$stmt->count();
            if($count==0)
            {
                $response['status'] = 1;
                $response['message'] = 'Payment not done';
                $response['amount']=$system['assessment_fee'];
            }
            else
            {
                $i=0;
                while($res = $stmt->fetch("assoc"))
                {
                    if($res['assessment_report']=='')
                    {
                        $data[$i]['status'] = 2;
                        $data[$i]['message'] = 'Pending Report';
                    }
                    else
                    {
                        $data[$i]['status'] = 3;
                        $data[$i]['message'] = 'Success';
                        $data[$i]['report']= $codes->SYSTEM_URL.'/content/uploads/'.$res['assessment_report'];
                    }
                        $data[$i]['assessment_type_id'] = $res['assessment_type_id'];
                        $data[$i]['name'] = $res['type'];
                        $data[$i]['amount']=$res['amount'];
                        $data[$i]['transaction_id']=$res['transaction_id'];
                        $data[$i]['order_id']=$res['order_id'];
                        $data[$i]['payment_id']=$res['payment_id'];
                        $data[$i]['date']=$res['ondate'];
                    $i++;
                }
				$response['status'] = 1;
				$response['message'] = 'Assessment List';
                $response = $data;
            }
           

            return $response;

        } catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }

    public function getAssessmentPaymentStatus($userId, $paymentId){
        Log::debug("Started ... getAssessmentPaymentStatus Dao");

        try{
            $conn = ConnectionManager::get('default');
            $codes = new Codes;

            $sql=sprintf('SELECT ap.*,at.type as name FROM assessment_payment ap LEFT JOIN assessmenttype as at ON at.id = ap.assessment_type_id WHERE ap.user_id=%s and ap.payment_id="%s"', $userId, $paymentId);
            $stmt = $conn->execute($sql);

            $response = array();
            $count=$stmt->count();
            if($count==0)
            {
                $response['status'] = 1;
                $response['message'] = 'Payment not done';
                $response['amount'] = $codes->CAMPUS_DRIVE_FEE;
            }
            else
            {
                $res = $stmt->fetch("assoc");
               // print_r($res);
                if($res['assessment_report']=='')
                {
                    $response['status'] = 2;
                    $response['assessment_type_id'] = $res['assessment_type_id'];
                    $response['name'] = $res['name'];
                    $response['message'] = 'Pending Report';
                    $response['amount']=$res['amount'];
                    $response['date']=$res['ondate'];
                    $response['transaction_id']=$res['transaction_id'];
                    $response['order_id']=$res['order_id'];
                    $response['payment_id']=$res['payment_id'];
                }
                else
                {
                    $response['status'] = 3;
                    $response['assessment_type_id'] = $res['assessment_type_id'];
                    $response['name'] = $res['name'];
                    $response['message'] = 'Success';
                    $response['amount']=$res['amount'];
                    $response['date']=$res['ondate'];
                    $response['transaction_id']=$res['transaction_id'];
                    $response['order_id']=$res['order_id'];
                    $response['payment_id']=$res['payment_id'];
                    $response['report']= $codes->SYSTEM_URL .'/content/uploads/'.$res['assessment_report'];
                }
            }

            return $response;

        } catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }

    public function getCampusDrivePrice($userId, $organizationId){
        Log::debug("Started ... getCampusDrivePrice Dao");

        try{
            $code = new Codes;
            $conn = ConnectionManager::get('default');

            $sql=sprintf('SELECT * FROM schedule_campus sh WHERE sh.organization = %s', $organizationId);
            $stmt = $conn->execute($sql);

            $count = $stmt->count();
            $data['campus_drive_price'] =  $code->CAMPUS_DRIVE_FEE;
            $data['discount_percentage'] = $code->DISCOUNT_PERCENTAGE;
            $data['offcampusfee'] =  $code->OFF_CAMPUS_FEE;
            $data['campus_drive_created'] = $count;
            
            
            $response['message'] = 'Success';
            $response['data'] = $data;
            $response['status'] = 1;


            return $response;

        } catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }

    public function get_picture($picture, $type)
    {
        global $system;
        $codes = new Codes;
		$codes->SYSTEM_THEME = 'default';
        if ($picture == "") {
            switch ($type) {
                case 'male':
                    $picture = $codes->SYSTEM_URL . '/content/themes/' . $codes->SYSTEM_THEME . '/images/blank_profile_male.jpg';
                    break;

                case 'female':
                    $picture = $codes->SYSTEM_URL . '/content/themes/' . $codes->SYSTEM_THEME . '/images/blank_profile_female.jpg';
                    break;

                case 'page':
                    $picture = $codes->SYSTEM_URL . '/content/themes/' . $codes->SYSTEM_THEME . '/images/blank_page.png';
                    break;

                case 'group':
                    $picture = $codes->SYSTEM_URL . '/content/themes/' . $codes->SYSTEM_THEME . '/images/blank_group.png';
                    break;

                case 'game':
                    $picture = $codes->SYSTEM_URL . '/content/themes/' . $codes->SYSTEM_THEME . '/images/blank_game.png';
                    break;

                case 'package':
                    $picture = $codes->SYSTEM_URL . '/content/themes/' . $codes->SYSTEM_THEME . '/images/blank_package.png';
                    break;
            }
        } else {
            $picture = $codes->UPLOADS_DIRECTORY . '/' . $picture;
        }
        return $picture;
    }

    public function getCollege($input){
        Log::debug("Started ... getCollege Dao");

        try{
           
            $status = 1;
            $data = array();
            $code = new Codes;
            $conn = ConnectionManager::get('default');

            if(isset($input['categories'])){
                $categories = $input['categories'];
            } 

            if(isset($input['dependency'])){
                $dependency = $input['dependency'];
            } else {

            }
            
            if(isset($input['and_or'])){
                $and_or = $input['and_or'];
            }

            if(isset($input['session_roles'])){
                $session_roles = $input['session_roles'];
            }else{
                $session_roles = 'Both';
            } 

            if(isset($input['noofsession'])){
                $noofsession = $input['noofsession'];
            } else {
                $noofsession = 0;
            }

            

		if (isset($input['categories']) && !empty($input['categories']) && is_array($input['categories'])) {
			$category_search_a = implode(',', $input['categories']);
			$userids = $anduser = array();
			foreach ($input['categories'] as $ck => $cval) {
				//$get_rows =  $conn->execute("SELECT * FROM `category_mst` WHERE  parent_id=$cval") or _apiError('SQL_ERROR_THROWEN');

				$qwe = $conn->execute(sprintf("SELECT * FROM `category_mst` WHERE `category_id`=%s", $cval));
				$re = $qwe->fetch("assoc");
				if (isset($dependency) && $dependency[$ck] == 1) {
					$qww = $conn->execute(sprintf("select user_id from `assessment_testmark` where `mark` >='%s' and `category`=%s", $re['average'], $cval));
				} else if (isset($dependency) && $dependency[$ck] == 0) {
					$qww = $conn->execute(sprintf("select user_id from `assessment_testmark` where `category`='%s'",$cval));
				}
				while ($res = $qww->fetch("assoc")) {
                    // print_r($res);
                    // exit;
					$quser = $conn->execute(sprintf("select interest_block from users where user_id=%s", $res['user_id']));
					$bockuser = $quser->fetch("assoc");
					if (isset($dependency) && $dependency[$ck] == 1 && $and_or[$ck] == 1) {
						$anduser[$res['user_id']] = (isset($anduser[$res['user_id']])) ? ($anduser[$res['user_id']] + 1) : 1;
                    }
                    
                    if($bockuser){
                        if ((($and_or[$ck] == 0) || ($and_or[$ck] == 1 && count($categories) == 1))
                         && !in_array($res['user_id'], $userids)
                          && $bockuser['interest_block'] == 0) {
                            $userids[] = $res['user_id'];
                        }
                    }
				}
			}
			$and_user = array();
			if ($anduser) {
				foreach ($anduser as $ab => $ba) {
					if ($ba > 1) {
						$and_user[] = $ab;
					}
				}
			}
            $userarray = array_unique(array_merge($userids, $and_user));
            $offset = 0;
					$limit = 10;
					if (!empty($input['limit'])) {
						$limit = $input['limit'];
					}
					if (!empty($input['offset'])) {
						$offset = $input['offset'] * $limit;
					}
			foreach ($userarray as $_ => $userid) {
				if (isset($session_roles)) {
					if (($session_roles == 'Conducted' || $session_roles == 'Conducting') && $userid != '') {
						$query_venue = sprintf("SELECT sessions.* FROM sessions  WHERE (`presentors` IN ('%s')) AND sessions.status=4 group by sessions_id ORDER BY event_date ASC limit %s, %s", $userid, $offset,$limit);
					} elseif (($session_roles == 'Attending' || $session_roles == 'Attended') && $userid != '') {
						$query_venue = sprintf("SELECT sessions.* FROM sessions 
						INNER JOIN sessions_attends ON (sessions_attends.sessions_id = sessions.sessions_id)
						WHERE sessions_attends.status =1 AND sessions.status=4 and sessions_attends.user_id IN ('%s')  group by sessions_id ORDER BY event_date ASC limit  %s, %s", $userid, $offset,$limit);
					} elseif ($session_roles == 'Both' && $userid != '') {
						$query_venue = sprintf("select * from (SELECT sessions.* FROM sessions  WHERE  (`presentors` IN ('%s')) AND sessions.status=4
						UNION ALL
						SELECT sessions.* FROM sessions 
						INNER JOIN sessions_attends ON (sessions_attends.sessions_id = sessions.sessions_id)
						WHERE sessions_attends.status =1 AND sessions.status=4 and sessions_attends.user_id IN ('%s')) a 
                        group by sessions_id ORDER BY event_date ASC limit %s, %s", $userid, $userid, $offset,$limit);
                        
					} else {
						$query_venue = '';
					}
					
					if ($query_venue != '') {
						//$query_venue .= " group by sessions_id ORDER BY event_date ASC limit $offset,$limit";
						 //echo $query_venue . '<br/>';
						// die();
						$get_sessions = $conn->execute($query_venue);
					}
				} else {
					$query_venue = "SELECT sessions.* FROM sessions WHERE sessions.status=4";
					$offset = 0;
					$limit = 10;
					if (!empty($input['limit'])) {
						$limit = $input['limit'];
					}
					if (!empty($input['offset'])) {
						$offset = $input['offset'] * $limit;
                    }
                   
					$query_venue = sprintf($query_venue." group by sessions_id ORDER BY event_date ASC limit %s,%s", $offset, $limit);
					$get_sessions = $conn->execute($query_venue);
				}

                
				$userdetail = $conn->execute(sprintf("select oraganisation_id from users where user_id = %s", $userid));
				$user_detail = $userdetail->fetch("assoc");
				if($user_detail['oraganisation_id'] != '') {
				$get_venue =  $conn->execute(sprintf("SELECT * FROM venue AS v WHERE v.venue_id = %s",  $user_detail['oraganisation_id']));
				if ($get_venue->count() > 0) {
					$venue_data = $get_venue->fetch("assoc");
					$venue_data['cover_photo'] = $code->SYSTEM_URL . '/'. $this->get_picture($venue_data['cover_photo'], 'page');
					$venue_data['description'] = nl2br($venue_data['description']);
					$venue_id = $venue_data['venue_id'];

					$key = array_search($venue_id, array_column($data, 'venue_id'));
					if ($key === false) {
						$venue_data['user_ids'][] = $userid;
						$venue_data['noofstudent'] = 1;
						if (isset($get_sessions) && $get_sessions->count() >= $noofsession) {
							$data[] = $venue_data;
						}
					} else {
						$data[$key]['noofstudent'] = $data[$key]['noofstudent'] + 1;
						$data[$key]['user_ids'][] = $userid;
					}
				}
				}
			}
		}

	


            return $data;

        } catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }

    /**
     * initiateAssessmentPayment
     */
	public function initiateAssessmentPayment($data, $date)
	{
		Log::debug("Started ... initiate Assessment Payment");

		try{
			
			$conn = ConnectionManager::get('default');

			$sql=sprintf("INSERT INTO assessment_payment (user_id, amount, assessment_type_id)
             VALUES (%s, %s, %s)", $data['user_id'], $data['amount'], $date['assessment_type_id']);
			
			Log::debug("SQL : ".$sql);

			$conn->execute($sql);

			Log::debug("Ended ...initiate Assessment Payment");

		}catch(\Exception $e){
			Log::debug($e);
			throw new Exception($e);
		}
    }

     /**
     * makeAssessmentPayment
     */
	public function assessmentPayment($data, $date)
	{
		Log::debug("Started ... make Assessment Payment");

		try{
			
            $conn = ConnectionManager::get('default');
            $dateUtils = new DateUtils;
            extract($data);
            if(!isset($data['transaction_id'])) {$transaction_id='';}
		if(!isset($data['order_id'])) {$order_id='';}
		if(!isset($data['payment_id'])) {$payment_id='';}
		if(!isset($data['remarks'])) {$remarks='';}
        if(!isset($data['status'])) {$status='';}
        
        $response = array();

        if($transaction_id=='' || $order_id=='' || $payment_id=='')
        { 
			$sql=sprintf("INSERT INTO assessment_payment (user_id, amount, assessment_type_id, transaction_id, order_id, payment_id, ondate)
             VALUES (%s, %s, '%s', '%s', '%s', '%s', '%s')", $user_id, $amount, $assessment_type_id, $transaction_id, $order_id, $payment_id, $dateUtils->getCurrentDate());
			
			Log::debug("SQL : ".$sql);

            $conn->execute($sql);

            $sql="SELECT LAST_INSERT_ID()";
                $stmt = $conn->execute($sql);
                $res = $stmt->fetch();
                
                $id = $res[0];
                $response['status'] = 1;
                $response['message'] = 'Record added successfully';
                $response['data']=$id;

        }else{
            $id=$order_id;
            if($status=='Completed')
            {
            $qwe =  $conn->execute(sprintf("update assessment_payment set transaction_id='%s',order_id='%s',payment_id='%s',paymentstatus=1  where id=%s", $transaction_id, $order_id, $payment_id, $order_id));
            }
            $type='Assessment';
             $conn->execute(sprintf("insert into transaction set user_id='%s',transaction_id='%s',order_id='%s',payment_id='%s',ondate='%s',amount='%s',type='%s',status='%s',remarks='%s',payment_gateway='instamojo'", $user_id, $transaction_id, $order_id, $payment_id, $date, $amount, $type, $status,$remarks));
             
             $response['status'] = 2;
             $response['message'] = 'Record added successfully';
             $response['data']=$id;

        }   

 
            
            return $response;
			Log::debug("Ended ...make Assessment Payment");

		}catch(\Exception $e){
			Log::debug($e);
			throw new Exception($e);
		}
    }

    public function getUserOnlineStatus(){
        Log::debug("Started ... getCampusDrivePrice Dao");

        try{
            $conn = ConnectionManager::get('default');

            $sql=sprintf('SELECT * FROM users u LEFT JOIN users_online uo ON uo.user_id = u.user_id');
            $stmt = $conn->execute($sql);

            $results = array();
            while($result = $stmt->fetch("assoc")) {
                $results = $result;
            }

            return $results;

        } catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }

    public function deleteConversation($userId, $conversationId){
        Log::debug("Started ... deleteConversation Dao");

        try{
            $conn = ConnectionManager::get('default');
            $deleted = '1';
            $sql=sprintf('UPDATE `conversations_users` SET `deleted` = %s WHERE conversation_id = %s AND user_id = %s',  $deleted, $userId, $conversationId);
            $stmt = $conn->execute($sql);

            $results = array();
            while($result = $stmt->fetch("assoc")) {
                $results = $result;
            }

            return $results;

        } catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }

    public function getUserChatMessages($userId, $conversationId, $lastMessageId, $limit){
        Log::debug("Started ... getUserChatMessages Dao");

        try{
            $conn = ConnectionManager::get('default');

            $sql=sprintf('SELECT * FROM conversations_messages WHERE user_id = %s, conversation_id = %s, message_id = %s', $userId, $conversationId, $lastMessageId);
            $stmt = $conn->execute($sql);

            $results = array();
            while($result = $stmt->fetch("assoc")) {
                $results = $result;
            }

            return $results;

        } catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }


	public function createChatGroup($userId, $groupName)
	{
		Log::debug("Started ... createChatGroup");

		try{
			
			$conn = ConnectionManager::get('default');

			$sql=sprintf("INSERT INTO conversation_groups (group_owner, group_name)
             VALUES (%s, %s)", $userId, $groupName);
			
			Log::debug("SQL : ".$sql);

			$conn->execute($sql);

			Log::debug("Ended ...createChatGroup");

		}catch(\Exception $e){
			Log::debug($e);
			throw new Exception($e);
		}
    }

    public function editChatGroup($userId, $groupId, $groupName)
	{
		Log::debug("Started ... editChatGroup");

		try{
			
			$conn = ConnectionManager::get('default');

			$sql=sprintf("UPDATE conversation_groups SET `group_name` = %s WHERE group_id = %s AND group_owner = %s",  $groupName, $groupId, $userId);
			
			Log::debug("SQL : ".$sql);

			$conn->execute($sql);

			Log::debug("Ended ...editChatGroup");

		}catch(\Exception $e){
			Log::debug($e);
			throw new Exception($e);
		}
    }

    public function createConversation($userId, $recipientId)
	{
		Log::debug("Started ... createConversation");

		try{
			
			$conn = ConnectionManager::get('default');

			$sql=sprintf("INSERT INTO conversations_users (conversation_user_id, user_id)
             VALUES (%s, %s)", $userId, $recipientId);
			
			Log::debug("SQL : ".$sql);

			$conn->execute($sql);

			Log::debug("Ended ...createConversation");

		}catch(\Exception $e){
			Log::debug($e);
			throw new Exception($e);
		}
    }

    public function replyToMessage($data)
	{
		Log::debug("Started ... replyToMessage");

		try{
			
			
            foreach($data['recipients'] as $recipientId){
                $conn = ConnectionManager::get('default');
                $sql=sprintf("INSERT INTO conversations_users (conversation_user_id, conversation_id, user_id)
                VALUES (%s, %s, %s)", $data['user_id'], $data['conversation_id'], $recipientId);
                
                Log::debug("SQL : ".$sql);

                $conn->execute($sql);
            }

            $conn = ConnectionManager::get('default');

            $sql=sprintf("INSERT INTO conversations_messages (temp_id, conversation_id, user_id, message)
            VALUES (%s, %s, %s, %s)", $data['temp_msg_id'], $data['conversation_id'], $data['user_id'], $data['message']);
            
            Log::debug("SQL : ".$sql);

            $conn->execute($sql);

			Log::debug("Ended ...replyToMessage");

		}catch(\Exception $e){
			Log::debug($e);
			throw new Exception($e);
		}
    }

    public function uploadChatImage($data)
	{
		Log::debug("Started ... uploadChatImage");

		try{
			
			
            foreach($data['recipients'] as $recipientId){
                $conn = ConnectionManager::get('default');
                $sql=sprintf("INSERT INTO conversations_users (conversation_user_id, conversation_id, user_id)
                VALUES (%s, %s, %s)", $data['user_id'], $data['conversation_id'], $recipientId);
                
                Log::debug("SQL : ".$sql);

                $conn->execute($sql);
            }

            $conn = ConnectionManager::get('default');

            $imgData = addslashes(file_get_contents($_FILES['file']['tmp_name']));

            $sql=sprintf("INSERT INTO conversations_messages (temp_id, conversation_id, user_id, message)
            VALUES (%s, %s, %s, %s)", $data['temp_msg_id'], $data['conversation_id'], $data['user_id'], $imgData);
            
            Log::debug("SQL : ".$sql);

            $conn->execute($sql);

			Log::debug("Ended ...uploadChatImage");

		}catch(\Exception $e){
			Log::debug($e);
			throw new Exception($e);
		}
    }

    public function forwardChatMessage($data)
	{
		Log::debug("Started ... forwardChatMessage");

		try{
			
			
            foreach($data['recipients'] as $recipientId){
                $conn = ConnectionManager::get('default');
                $sql=sprintf("INSERT INTO conversations_users (conversation_user_id, conversation_id, user_id)
                VALUES (%s, %s, %s)", $data['user_id'], $data['conversation_id'], $recipientId);
                
                Log::debug("SQL : ".$sql);

                $conn->execute($sql);
            }

            $conn = ConnectionManager::get('default');

            $sql=sprintf('SELECT * FROM conversations_messages WHERE user_id = %s, conversation_id = %s, message_id = %s', $data['user_id'], $data['conversation_id'], $data['message_id']);
            $stmt = $conn->execute($sql);

            $results = array();
            while($result = $stmt->fetch("assoc")) {
                $sql=sprintf("INSERT INTO conversations_messages (conversation_id, user_id, message)
                VALUES (%s, %s, %s)", $data['conversation_id'], $data['user_id'], $result['message']);
                
                Log::debug("SQL : ".$sql);

            }

           
            $conn->execute($sql);

			Log::debug("Ended ...forwardChatMessage");

		}catch(\Exception $e){
			Log::debug($e);
			throw new Exception($e);
		}
    }

    public function conversationSeen($data)
	{
		Log::debug("Started ... conversationSeen");

		try{
			
			$conn = ConnectionManager::get('default');

            $seen =1;
			$sql=sprintf("UPDATE conversations_messages SET `seen` = %s WHERE user_id = %s AND conversation_id = %s AND message_id = %s",  $seen, $data['user_id'], $data['conversation_id'], $data['convmessage_idersation_id']);
			
			Log::debug("SQL : ".$sql);

			$conn->execute($sql);

			Log::debug("Ended ...conversationSeen");

		}catch(\Exception $e){
			Log::debug($e);
			throw new Exception($e);
		}
    }

    /**
     * 
     */
    public function leaveGroup($userId, $groupId)
    {
        Log::debug("Started ...leaveGroup Dao");

        try{
            
            $conn = ConnectionManager::get('default');

            $sql=sprintf("DELETE groups_members WHERE user_id = '%s' and group_id = %s", $userId, $groupId);

            Log::debug("SQL : ".$sql);

            $stmt = $conn->execute($sql);

            Log::debug("Ended ...leaveGroup Dao");
            
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }

    /**
     * 
     */
    public function addMemberToGroup($userId, $groupId)
    {
        Log::debug("Started ...addMemberToGroup Dao");

        try{
            
            $conn = ConnectionManager::get('default');

            $sql=sprintf("INSERT INTO groups_members (group_id, user_id)
            VALUES (%s, %s)", $groupId, $userId);

            Log::debug("SQL : ".$sql);

            $stmt = $conn->execute($sql);

            Log::debug("Ended ...addMemberToGroup Dao");
            
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }

    /**
     * 
     */
    public function removeGroupMember($userId, $groupId)
    {
        Log::debug("Started ...removeGroupMember Dao");

        try{
            
            $conn = ConnectionManager::get('default');

            $sql=sprintf("DELETE groups_members WHERE user_id = '%s' and group_id = %s", $userId, $groupId);

            Log::debug("SQL : ".$sql);

            $stmt = $conn->execute($sql);

            Log::debug("Ended ...removeGroupMember Dao");
            
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }

    public function makeUserAdmin($memberId, $groupId)
	{
		Log::debug("Started ... makeUserAdmin");

		try{
			
			$conn = ConnectionManager::get('default');

            $seen =1;
			$sql=sprintf("UPDATE groups SET `group_admin` = %s WHERE group_id = %s",  $memberId, $groupId);
			
			Log::debug("SQL : ".$sql);

			$conn->execute($sql);

			Log::debug("Ended ...makeUserAdmin");

		}catch(\Exception $e){
			Log::debug($e);
			throw new Exception($e);
		}
    }

    public function revokeAdminStatus($memberId, $groupId)
	{
		Log::debug("Started ... revokeAdminStatus");

		try{
			
			$conn = ConnectionManager::get('default');

            $seen =1;
			$sql=sprintf("DELETE groups WHERE group_id = %s AND group_admin = %s", $groupId,  $memberId);
			
			Log::debug("SQL : ".$sql);

			$conn->execute($sql);

			Log::debug("Ended ...revokeAdminStatus");

		}catch(\Exception $e){
			Log::debug($e);
			throw new Exception($e);
		}
    }

    public function getGroupInfo($groupId){
        Log::debug("Started ... getGroupInfo Dao");

        try{
            $conn = ConnectionManager::get('default');

            $sql=sprintf('SELECT * FROM groups WHERE groups.group_id = %s', $groupId);
            $stmt = $conn->execute($sql);

            $results = array();
            while($result = $stmt->fetch("assoc")) {
                $results = $result;
            }

            return $results;

        } catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }

    public function profile($uid , $fid){
        Log::debug("Started ... getGroupInfo Dao");

        try{
            $conn = ConnectionManager::get('default');
            $result = array();
            $pd = new PostsDao;
            $pod = new PointsDao;
            $dat = $pd->userData($fid , $uid , 'user' , 0);
            if(isset($dat[0])){
                $data = $dat[0];
            }else{$data = array();}
            $user = new UserModel($uid);
            $sql=sprintf("SELECT users.*, packages.name as package_name, packages.color as package_color FROM users LEFT JOIN packages ON users.user_subscribed = '1' AND users.user_package = packages.package_id WHERE users.user_id = %s", $uid);
            $stmt = $conn->execute($sql);
            if($res = $stmt->fetch('assoc')){
                if ( $res['user_id'] != $fid) {
                    /* get the connection */
                    $res['we_friends'] = (in_array($fid, $user->_data['friends_ids'])) ? true : false;
                    $res['he_request'] = (in_array($fid, $user->_data['friend_requests_ids'])) ? true : false;
                    $res['i_request'] = (in_array($fid, $user->_data['friend_requests_sent_ids'])) ? true : false;
                    $res['i_follow'] = (in_array($fid, $user->_data['followings_ids'])) ? true : false;
                    
                    /* get mutual friends */
                    if ($user->_data['user_id'] != $fid) {
                        $res['mutual_friends_count'] = $user->get_mutual_friends_count($fid);
                        
                        /* Arnab mohanty : mutual friend only show for type=dashboard on 17th dec*/
                        
                        $mf= $user->get_mutual_friends($fid,0);
                        if(count($mf) > 0){
                            $mf = $mf[0]['user_id'];
                            $dat = $pd->userData($fid , $mf , 'user', 0);
                            $res['mutual_friend'] = $dat[0];
                        }
                        
                    }
                }
                $res = array_merge($res , $data);
                $result['profile'] = $res;
            }
            $result['questions_count'] = $user->get_user_questions_count($res['user_id']);
            $result['answers_count'] = $user->get_user_answers_count($res['user_id']);
            $result['articles_count'] = $conn->execute("SELECT * from articles WHERE (user_type = 'user' or user_type = 'User' or user_type = 'Community' or user_type = 'community') and created_by = $uid")->count();
            $result['quickbit_count'] = $conn->execute("SELECT * from quickbits WHERE (user_type = 'user' or user_type = 'User' or user_type = 'Community' or user_type = 'community') and creator_id = $uid")->count();
            $result['quiz_count'] = $conn->execute("SELECT * from quizzes WHERE (user_type = 'user' or user_type = 'User' or user_type = 'Community' or user_type = 'community') and creator_id = $uid")->count();
            $result['debate_count'] = $conn->execute("SELECT * from debates WHERE (user_type = 'user' or user_type = 'User' or user_type = 'Community' or user_type = 'community') and creator_id = $uid")->count();
            //$result['posts_count'] = $user->get_user_posts_count($res['user_id']);
            $result['polls_count'] = $conn->execute("SELECT * from polls WHERE (user_type = 'user' or user_type = 'User' or user_type = 'Community' or user_type = 'community') and creator_id = $uid")->count();
            //$result['activity_count'] = $user->get_user_activity_count($res['user_id']);
            $result['following_ids_count'] = $conn->execute("SELECT users.user_id,users.privacy_connection_request, users.user_name, users.user_fullname, users.user_gender, users.user_picture FROM followings INNER JOIN users ON (followings.following_id = users.user_id) WHERE followings.user_id = $uid ")->count();
            //$result['following_ids'] = $user->get_followings($uid);
            $result['nooffriends'] = $conn->execute("SELECT * from friends where user_one_id = $uid or user_two_id = $uid")->count();
            $result['noofinterests'] = $conn->execute("SELECT * from user_interest where user_id = $uid")->count();
            $result['points'] = $pod->getPointsAndBadgesByUserid($uid);



            return $result;

        } catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }


}