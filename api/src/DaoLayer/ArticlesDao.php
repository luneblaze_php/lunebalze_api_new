<?php

namespace App\DaoLayer;

use App\Utils\Codes;
use Cake\Core\Exception\Exception;
use Cake\Datasource\ConnectionManager;
use App\DaoLayer\PostsDao;
use App\DaoLayer\PointsDao;
use Cake\Log\Log;
use App\Model\UserModel;
use App\Utils\DateUtils;

class ArticlesDao
{

	private function getImgFullPath($img = '', $newPath = 0){
        $codes = new Codes;
        return !empty($img) ?$codes->SYSTEM_URL.'/'.($newPath?'api/':'').$codes->UPLOADS_DIRECTORY.'/'.$img:'';
    }

    public function mysql_real_escape_string($inp) {
	    if(is_array($inp))
	        return array_map(__METHOD__, $inp);

	    if(!empty($inp) && is_string($inp)) {
	        return str_replace(array('\\', "\0", "\n", "\r", "'", '"', "\x1a"), array('\\\\', '\\0', '\\n', '\\r', "\\'", '\\"', '\\Z'), $inp);
	    }

	    return $inp;
	}
    public function articlePoints($case , $userId , $articlesId = 0){
        $pod = new PointsDao;
        $conn = ConnectionManager::get('default');
        $codes = new Codes;
        switch ($case) {
            case 'const':
                $k = $codes->ARTICLES_K;
                $ret = $pod->checkAndDeduct($userId, $codes->DEDUCT_ARTICLE);
                if($ret == 0){return $ret;}
                $pod->addPoints('articles', $userId , $k);
                return $k;
                break;
            case 'by_content':
                $k = $codes->ARTICLES_K;
                $l = $conn->execute(sprintf("SELECT * from `articles` where articles_id = %s"  , $articlesId));
                $res = $l->fetch('assoc');
                $ret = $pod->checkAndDeduct($res['created_by'], $codes->DEDUCT_ARTICLE);
                if($ret == 0){return $ret;}
                $pod->addPoints('articles', $res['created_by'] , $k);
                return $k;
                break;
            case 'like':
                $k = $codes->ARTICLES_C;
                $pod->addPoints('articles', $userId , $k);
                return $k;
                break;
            case 'unlike':
                $k = $codes->ARTICLES_C;
                $pod->addPoints('articles', $userId , -$k);
                return $k;
                break;
            case 'by_content_like':
                $k = $codes->ARTICLES_C;
                $l = $conn->execute(sprintf("SELECT * from `articles` where articles_id = %s"  , $articlesId));
                $res = $l->fetch('assoc');
                $pod->addPoints('articles', $res['created_by'] , $k);
                return $k;
                break;
            case 'by_content_unlike':
                $k = $codes->ARTICLES_C;
                $l = $conn->execute(sprintf("SELECT * from `articles` where articles_id = %s"  , $articlesId));
                $res = $l->fetch('assoc');
                $pod->addPoints('articles', $res['created_by'] , -$k);
                return $k;
                break;
            case 'remove':
                $k = $codes->ARTICLES_K;
                $c = $codes->ARTICLES_C;
                $l = $conn->execute(sprintf("SELECT * from `articles_likes` where articles_id = %s"  , $articlesId));
                $n = $l->count();
                $total = $k + ($n * $c);
                $l = $conn->execute(sprintf("SELECT * from `articles` where articles_id = %s"  , $articlesId));
                $res = $l->fetch('assoc');
                $pod->addPoints('articles', $res['created_by'] , -$total);
                return -$total;
                break;
            case 'get':
                $k = $codes->ARTICLES_K;
                $c = $codes->ARTICLES_C;
                $l = $conn->execute(sprintf("SELECT * from `articles_likes` where articles_id = %s"  , $articlesId));
                $n = $l->count();
                $total = $k + ($n * $c);
                return $total;
                break;
            default:
                return null;
                break;
        }
    }
	/**
	 * Get Articles By articles id
	 */
	public function getArticlesByArticlesId($articlesId , $userId)
	{
		Log::debug("Started ...getArticlesByArticlesId Dao : Id : ".$articlesId);

		try{
			
			$conn = ConnectionManager::get('default');
            $pd = new PostsDao;
			$sql=sprintf("SELECT * FROM `articles` WHERE `articles_id` = %s ", $articlesId);

			

			Log::debug("SQL : ".$sql);

			$stmt = $conn->execute($sql);

			if(!$result = $stmt->fetch("assoc")){return "no article found";}

			if($result['cover_photo'] != ''){$result['cover_photo'] = $this->getImgFullPath($result['cover_photo'] ,1);}

			$cid = $result['created_by'];
            $fet = $pd->userData($userId , $cid , $result['user_type'] , $result['community_id']);
            $result['created_by'] = (isset($fet[0]) ? $fet[0] : null);
            $result['community_data'] = (isset($fet[1]) ? $fet[1] : null);
            $result['creator_id'] = $cid;
			
			$react = array();
			$rname = array();
			$reactions = $conn->execute(sprintf("SELECT * from `reactions` "));
			while($r = $reactions->fetch('assoc')){
				$str = '' . $r['id'] . '';
				//$react[$str] = 0;
				array_push($react, 0);
				array_push($rname, $r['name']);


			}
			$count = count($rname);
			//$pc = $conn->execute(sprintf("SELECT * from `posts` where post_type = 'article' and article_id = %s"  , $articlesId));

			//if(!$p = $pc->fetch("assoc")){return "no post found";}
			$l = $conn->execute(sprintf("SELECT * from `articles_likes` where articles_id = %s"  , $articlesId));
			$t = 0;
			while($r = $l->fetch("assoc")){
				$t = $t + 1;
				$str = $r['reaction_type'];
				$react[$str] = $react[$str] + 1;
			}
			$fr = array();
			for ($i=0; $i < $count; $i++) { 
				# code...
				$fr[$rname[$i]] = $react[$i];
			}

			$result['reactions'] = $fr;
			$result['total_likes'] = $t;
			
			$il = $conn->execute(sprintf("SELECT * FROM articles_likes WHERE articles_id = $articlesId and user_id = $userId"));
            if($li = $il->fetch("assoc")){
                $result["i_like"] = $li['reaction_type'];
            }else{$result["i_like"] = null;}

            $co = $conn->execute(sprintf("SELECT * FROM articles_discussion WHERE articles_id = $articlesId and parent_comment_id = 0"));
            $result['comments_count'] = $co->count();
            $co = $conn->execute(sprintf("SELECT * FROM articles_discussion WHERE articles_id = $articlesId and user_id = $userId"));
            $result['my_comment'] = "";
            if($r = $co->fetch('assoc')){$result['my_comment'] = $r['post'];}
            //$temp = array();
            $arr = array();
            if($result['interests'] != "" and $result['interests'] != null){
                $temp = explode(",", $result['interests']);
                foreach ($temp as $i ) {
                    $in = $conn->execute(sprintf("SELECT interest_id , text , parent_id , image , status FROM interest_mst WHERE interest_id = $i")); 
                    while($r = $in->fetch("assoc")){
                        if($r['image'] == null){
                            $r['image'] = '';
                        }elseif($r['image'] != ""){
                            $r['image'] = $this->getImgFullPath($r['image'],1);
                        }
                        array_push($arr , $r);
                    }
                }
            }
            $result['interests'] = $arr;
            $result['points'] = $this->articlePoints('get', $userId , $articlesId);

            $user = new UserModel($userId);
            $mf = $user->get_friends_ids($userId);
            $mf = implode(',', $mf);
            if($co = $conn->execute(sprintf("SELECT * FROM articles_discussion WHERE articles_id = $articlesId and FIND_IN_SET(user_id , '$mf') order by articles_discussion_id desc"))->fetch('assoc')){
                $friend = $pd->userData($userId , $co['user_id'] , 'user' , 0)[0];
                $co['user'] = $friend;
                $result['friend_comment'] = $co;
            }else{
                $result['friend_comment'] = null;
            }
            if($il = $conn->execute(sprintf("SELECT * FROM articles_likes WHERE articles_id = $articlesId and FIND_IN_SET(user_id , '$mf')"))->fetch('assoc')){
                $friend = $pd->userData($userId , $il['user_id'] , 'user' , 0)[0];
                $il['user'] = $friend;
                $result['friend_like'] = $il;
            }else{
                $result['friend_like'] = null;
            }

			Log::debug("Ended ...getArticlesByArticlesId Dao");

			return $result;

		}catch(\Exception $e){
			Log::debug($e);
			throw new Exception($e);
		}
	}

	public function createArticle($inputJson)
	{
		Log::debug("Started ...createArticle Dao : Id : ".$inputJson['creator_id']);

		try{
			
			$conn = ConnectionManager::get('default');
			$dateUtils = new DateUtils;
            $pd = new PostsDao;
            $codes = new Codes;
			$date = $dateUtils->getCurrentDate();
            $id = $inputJson['creator_id'];
            if($inputJson['user_type'] == 'user' || $inputJson['user_type'] == 'User' || $inputJson['user_type'] == 'community' || $inputJson['user_type'] == 'Community'){
                $sql = $conn->execute(sprintf("SELECT total FROM users_points where user_id = %s", $inputJson['creator_id']));
                if($sql = $sql->fetch('assoc')){
                    if($sql['total'] >= $codes->DEDUCT_ARTICLE){

                    }else{
                        throw new Exception("not enough points");
                    }
                }else{
                    $conn->execute("INSERT into users_points set user_id = $id , total = $codes->INITIAL_PTS");
                    return $this->createArticle($inputJson);
                }
            }
			$file_name='';//LOGO PATH SAVED IN THIS
            $file_size = '';
            $height = 0;
            $width = 0;
            if(isset($_FILES['cover_photo']) && $_FILES['cover_photo']['size'] != 0) {
                        // valid inputs
                        if(!isset($_FILES["cover_photo"]) || $_FILES["cover_photo"]["error"] != UPLOAD_ERR_OK) {
                            throw new Exception("Something wrong with upload! Is 'upload_max_filesize' set correctly?");
                        }

                        // check file size
                        $max_allowed_size = $codes->MAX_FILE_SIZE * 1024;
                        if($_FILES["cover_photo"]["size"] > $max_allowed_size) {
                            throw new Exception("The file size is so big");
                        }

                        $fileinfo = getimagesize($_FILES["cover_photo"]["tmp_name"]);
                        $width = $fileinfo[0];
                        $height = $fileinfo[1];
                        $extension = pathinfo($_FILES['cover_photo']['name'], PATHINFO_EXTENSION);
                        /* check & create uploads dir */

                        $folder = 'photos';
                            $depth = '../';
                            if(!file_exists($depth.$codes->UPLOADS_DIRECTORY.'/'.$folder)) {
                                @mkdir($depth.$codes->UPLOADS_DIRECTORY.'/'.$folder, 0777, true);
                            }
                            if(!file_exists($depth.$codes->UPLOADS_DIRECTORY.'/'.$folder.'/' . date('Y'))) {
                                @mkdir($depth.$codes->UPLOADS_DIRECTORY.'/'.$folder.'/' . date('Y'), 0777, true);
                            }
                            if(!file_exists($codes->UPLOADS_DIRECTORY.'/'.$folder.'/' . date('Y') . '/' . date('m'))) {
                                @mkdir($depth.$codes->UPLOADS_DIRECTORY.'/'.$folder.'/' . date('Y') . '/' . date('m'), 0777, true);
                            }

                        /* prepare new file name */
                        $directory = $folder.'/'. date('Y') . '/' . date('m') . '/';
                        $prefix =$codes->UPLOADES_PREFIX.'_'.md5(time()*rand(1, 9999));
                        $file_name = $directory.$prefix.'.'.$extension;
                        $path = $depth.$codes->UPLOADS_DIRECTORY.'/'.$file_name;
                                            
                        /* check if the file uploaded successfully */
                        if(!@move_uploaded_file($_FILES['cover_photo']['tmp_name'], $path)) {
                            throw new Exception("Sorry, can not upload the photo");
                        }
                        
                        /* upload to amazon s3 */

                        if($codes->S3_ENABLED) {
                            aws_s3_upload($path, $file_name);
                        }
                        $file_size = ''.$height .'X'.$width.'' ;
            }
            if(!isset($inputJson['intrests'])){$inputJson['intrests'] = '';}
            if(!isset($inputJson['community_id'])){$inputJson['community_id'] = 0;}
            extract($inputJson);
            $sql = sprintf("INSERT INTO articles set title = '%s' , description = '%s' , cover_photo = '%s' , img_dimensions = '%s' , user_type = '%s' , 	created_by = '%s' , community_id = %s , interests = '%s'" , $this->mysql_real_escape_string($title) , $this->mysql_real_escape_string($description) , $file_name , $file_size , $user_type , $creator_id ,$community_id , $intrests );
            if(!$conn->execute($sql)){return ["messge" => "something went wrong"];}

            $sql="SELECT LAST_INSERT_ID()";
            $stmt = $conn->execute($sql);
            $res = $stmt->fetch();
            $id = $res[0];
            $args = $inputJson;
            //if($args['user_type'] == 'venue' || $args['user_type']=='Venue'){$user_type = 'institution';}
            $sql=sprintf("INSERT INTO posts ( article_id, user_id, post_title, community_id, post_type, privacy, text_tagged, origin_type, organisation_id, payment,total,updated_at, time, user_type) VALUES ( %s , '%s', '%s', '%s', 'article', 'public', '', '', 0, 0, 0,'%s', '%s', '%s')", $res[0], $args['creator_id'], $args['title'], (isset($args['community_id'])?$args['community_id']:0), $date, $date, $user_type);
            Log::debug("Ended ...saveDebate Dao");
            if(!$conn->execute($sql)){return ["messge" => "something went wrong"];}
            $sql="SELECT LAST_INSERT_ID()";
            $stmt = $conn->execute($sql);
            $res = $stmt->fetch();
            $pid = $res[0];//post id

            //return $id;
            if($user_type == 'user' || $user_type == 'User'){
                $ret = $this->articlePoints('const' , $creator_id , $id);
                if($ret == 0){throw new Exception("Not enough points.");}
            }
            
            $postdata = $conn->execute(sprintf("select * from posts where post_id = %s", $pid));
            if(!$result = $postdata->fetch('assoc')){return ["messge" => "something went wrong"];}
            $sql = sprintf("SELECT * FROM articles where articles_id = %s" , $id);
            $st = $conn->execute($sql);
            if($r = $st->fetch("assoc")){
                $r['cover_photo'] = $this->getImgFullPath($r['cover_photo'] , 1);
                $dat = $pd->userData($creator_id , $creator_id , $user_type , $community_id);
                $r['created_by'] = (isset($dat[0])? $dat[0] : null);
                $r['community_data'] = (isset($dat[1])? $dat[1] : null);
                $result['article_data'] = $r;
            }
            return $result;

		}catch(\Exception $e){
			Log::debug($e);
			throw new Exception($e);
		}
	}

	public function editArticle($inputJson , $articlesId , $userId)
	{
		Log::debug("Started ...createArticle Dao : Id : ".$inputJson['creator_id']);

		try{
			
			$conn = ConnectionManager::get('default');
			$dateUtils = new DateUtils;
			$date = $dateUtils->getCurrentDate();
			$ch = $conn->execute(sprintf("SELECT * FROM articles where articles_id = %s and created_by = %s" , $articlesId , $userId));
			if(!$r = $ch->fetch('assoc')){return ["messge" => "article id wrong or the creator doesn't match"];}
			$file_name=$r['cover_photo'];//LOGO PATH SAVED IN THIS
            $file_size = $r['img_dimensions'];
            if(!isset($inputJson['title'])){$inputJson['title'] = $r['title'];}
            if(!isset($inputJson['description'])){$inputJson['description'] = $r['description'];}
            if(!isset($inputJson['interests'])){$inputJson['interests'] = $r['interests'];}
            $height = 0;
            $width = 0;
            if(isset($_FILES['cover_photo']) && $_FILES['cover_photo']['size'] != 0) {
                        // valid inputs
                        if(!isset($_FILES["cover_photo"]) || $_FILES["cover_photo"]["error"] != UPLOAD_ERR_OK) {
                            throw new Exception("Something wrong with upload! Is 'upload_max_filesize' set correctly?");
                        }

                        // check file size
                        $max_allowed_size = $codes->MAX_FILE_SIZE * 1024;
                        if($_FILES["cover_photo"]["size"] > $max_allowed_size) {
                            throw new Exception("The file size is so big");
                        }

                        $fileinfo = getimagesize($_FILES["cover_photo"]["tmp_name"]);
                        $width = $fileinfo[0];
                        $height = $fileinfo[1];
                        $extension = pathinfo($_FILES['cover_photo']['name'], PATHINFO_EXTENSION);
                        /* check & create uploads dir */

                        $folder = 'photos';
                            $depth = '../';
                            if(!file_exists($depth.$codes->UPLOADS_DIRECTORY.'/'.$folder)) {
                                @mkdir($depth.$codes->UPLOADS_DIRECTORY.'/'.$folder, 0777, true);
                            }
                            if(!file_exists($depth.$codes->UPLOADS_DIRECTORY.'/'.$folder.'/' . date('Y'))) {
                                @mkdir($depth.$codes->UPLOADS_DIRECTORY.'/'.$folder.'/' . date('Y'), 0777, true);
                            }
                            if(!file_exists($codes->UPLOADS_DIRECTORY.'/'.$folder.'/' . date('Y') . '/' . date('m'))) {
                                @mkdir($depth.$codes->UPLOADS_DIRECTORY.'/'.$folder.'/' . date('Y') . '/' . date('m'), 0777, true);
                            }

                        /* prepare new file name */
                        $directory = $folder.'/'. date('Y') . '/' . date('m') . '/';
                        $prefix =$codes->UPLOADES_PREFIX.'_'.md5(time()*rand(1, 9999));
                        $file_name = $directory.$prefix.'.'.$extension;
                        $path = $depth.$codes->UPLOADS_DIRECTORY.'/'.$file_name;
                                            
                        /* check if the file uploaded successfully */
                        if(!@move_uploaded_file($_FILES['cover_photo']['tmp_name'], $path)) {
                            throw new Exception("Sorry, can not upload the photo");
                        }
                        
                        /* upload to amazon s3 */

                        if($codes->S3_ENABLED) {
                            aws_s3_upload($path, $file_name);
                        }
                        $file_size = ''.$width .'px * '.$height.'px' ;
            }
            //if(!isset($inputJson['intrests'])){$inputJson['intrests'] = '';}
            //if(!isset($inputJson['community_id'])){$inputJson['community_id'] = 0;}
            extract($inputJson);
            $sql = sprintf("UPDATE articles set title = '%s' , description = '%s' , cover_photo = '%s' , img_dimensions = '%s' ,  interests = '%s'" , $this->mysql_real_escape_string($title) , $this->mysql_real_escape_string($description) , $file_name , $file_size ,  $intrests );
            if(!$conn->execute($sql)){return ["messge" => "something went wrong"];}

            
            return ["messge" => "success"];

            


		}catch(\Exception $e){
			Log::debug($e);
			throw new Exception($e);
		}
	}


    public function getArticleDiscussions( $userId , $articlesId , $commentId , $targetId , $od = 'desc')
    {
        Log::debug("Started ...getArticleDiscussions Dao : Id : ".$articlesId);

        try{
            
            $conn = ConnectionManager::get('default');
            $result = array();
            $pd = new PostsDao;
            $sql=sprintf("SELECT * FROM `articles` WHERE `articles_id` = %s ", $articlesId);
            
            Log::debug("SQL : ".$sql);

            $stmt = $conn->execute($sql);

            if(!$re = $stmt->fetch("assoc")){return "no article found";}
            $target = null;
            if($commentId == 0)
                $sql = sprintf("SELECT * FROM `articles_discussion` where  articles_id = $articlesId and parent_comment_id = 0 order by articles_discussion_id $od limit 0,15");
            else
                $sql = sprintf("SELECT * FROM `articles_discussion` where articles_discussion_id < $commentId and articles_id = $articlesId and parent_comment_id = 0 order by articles_discussion_id $od limit 0,15");
            $stmt = $conn->execute($sql);
            while($res = $stmt->fetch('assoc')){
                $dat = $pd->userData($userId , $res['user_id'] , $res['user_type'] , $res['community_id']);
                $res['user'] = (isset($dat[0])?$dat[0]:null);
                $dvcount = 0;
                $uvcount = 0;
                $i_follow = 0;
                $sql = sprintf("SELECT * from articles_discussion_likes where articles_id = $articlesId and articles_discussion_id = %s",$res["articles_discussion_id"]);
                $s = $conn->execute($sql);
                while($r = $s->fetch('assoc')){
                    if($r['vote_type'] == 1 || $r['vote_type'] == "1"){
                        $uvcount++;
                    }
                    if($r['vote_type'] == -1 || $r['vote_type'] == "-1"){
                        $dvcount++;
                    }
                    if($r['user_id'] == $userId){
                        $i_follow = $r['vote_type'];
                    }
                }
                $res["total_upvotes"] = $uvcount;
                $res['total_downvotes'] = $dvcount;
                $res["i_vote"] = $i_follow;
                $sql = $sql = sprintf("SELECT * FROM `articles_discussion` where articles_id = $articlesId and parent_comment_id = %s" , $res['articles_discussion_id']);
                $co = $conn->execute($sql);
                $res['total_replies'] = $co->count(); 
                $res['replies'] = array();
                //while($a = $co->fetch("assoc") ){
                    $rep = $this->getArticleDiscussionsReplies( $userId , $articlesId , $res['articles_discussion_id'] , 0 , 0);
                    $rep = $rep['discussions'];
                    $i = 0;
                    foreach ($rep as $key ) {
                        if($i == 2){break;}
                        array_push($res['replies'] , $key);
                        $i++;
                    }
                //} 
                

                if($targetId != 0 && $targetId == $res['articles_discussion_id']){
                    $target = $res;
                    continue;
                }
                
                array_push($result , $res);

            }
            $results["discussions"] = $result;
            $results['target'] = $target; 
            

            return $results;

        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }

    public function getArticleDiscussionsReplies( $userId , $articlesId , $commentId , $targetId , $replyId , $od = 'desc')
    {
        Log::debug("Started ...getArticleDiscussions Dao : Id : ".$articlesId);

        try{
            
            $conn = ConnectionManager::get('default');
            $result = array();
            $pd = new PostsDao;
            $sql=sprintf("SELECT * FROM `articles` WHERE `articles_id` = %s ", $articlesId);
            
            Log::debug("SQL : ".$sql);

            $stmt = $conn->execute($sql);

            if(!$re = $stmt->fetch("assoc")){return "no article found";}
            $target = null;
            $parent = null;
            $sql = sprintf("SELECT * FROM `articles_discussion` where articles_id = $articlesId and parent_comment_id = 0 and articles_discussion_id = $commentId");
            $s = $conn->execute($sql);
            if($res = $s->fetch("assoc")){
                $dat = $pd->userData($userId , $res['user_id'] , $res['user_type'] , $res['community_id']);
                $res['user'] = (isset($dat[0])?$dat[0]:null);
                $dvcount = 0;
                $uvcount = 0;
                $i_follow = 0;
                $sql = sprintf("SELECT * from articles_discussion_likes where articles_id = $articlesId and articles_discussion_id = %s",$res["articles_discussion_id"]);
                $s = $conn->execute($sql);
                while($r = $s->fetch('assoc')){
                    if($r['vote_type'] == 1 || $r['vote_type'] == "1"){
                        $uvcount++;
                    }
                    if($r['vote_type'] == -1 || $r['vote_type'] == "-1"){
                        $dvcount++;
                    }
                    if($r['user_id'] == $userId){
                        $i_follow = $r['vote_type'];
                    }
                }
                $res["total_upvotes"] = $uvcount;
                $res['total_downvotes'] = $dvcount;
                $res["i_vote"] = $i_follow;
                $sql = $sql = sprintf("SELECT * FROM `articles_discussion` where articles_id = $articlesId and parent_comment_id = %s " , $res['articles_discussion_id']);
                $co = $conn->execute($sql);
                $res['total_replies'] = $co->count(); 
                $parent = $res;
            }
            if($replyId == 0)
                $sql = sprintf("SELECT * FROM `articles_discussion` where articles_id = $articlesId and parent_comment_id = $commentId order by articles_discussion_id $od limit 0,15");
            else
                $sql = sprintf("SELECT * FROM `articles_discussion` where articles_discussion_id < $replyId and articles_id = $articlesId and parent_comment_id = $commentId order by articles_discussion_id $od limit 0,15");
            $stmt = $conn->execute($sql);
            while($res = $stmt->fetch('assoc')){
                $dat = $pd->userData($userId , $res['user_id'] , $res['user_type'] , $res['community_id']);
                $res['user'] = (isset($dat[0])?$dat[0]:null);
                $dvcount = 0;
                $uvcount = 0;
                $i_follow = 0;
                $sql = sprintf("SELECT * from articles_discussion_likes where articles_id = $articlesId and articles_discussion_id = %s",$res["articles_discussion_id"]);
                $s = $conn->execute($sql);
                while($r = $s->fetch('assoc')){
                    if($r['vote_type'] == 1 || $r['vote_type'] == "1"){
                        $uvcount++;
                    }
                    if($r['vote_type'] == -1 || $r['vote_type'] == "-1"){
                        $dvcount++;
                    }
                    if($r['user_id'] == $userId){
                        $i_follow = $r['vote_type'];
                    }
                }
                if($targetId != 0 && $targetId == $res['articles_discussion_id']){
                    $target = $res;
                    continue;
                }
                $res["total_upvotes"] = $uvcount;
                $res['total_downvotes'] = $dvcount;
                $res["i_vote"] = $i_follow;
                array_push($result , $res);

            }
            $results["discussions"] = $result;
            $results['target'] = $target; 
            $results['parent'] = $parent;
            

            return $results;

        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }


}
