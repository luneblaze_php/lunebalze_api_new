<?php

namespace App\DaoLayer;

use App\Utils\Codes;
use Cake\Core\Exception\Exception;
use Cake\Datasource\ConnectionManager;
use Cake\Log\Log;

class UsersExtraInfoDao
{
    public function currentVersion($version , $type)
    {
        Log::debug("Started ...currentVersion Dao : User Id : ".$type);

        try{
            $conn = ConnectionManager::get('default');
            if($type == 0){
                $sql=sprintf("SELECT version , system_live , system_message FROM `system_options` where ID = 1");

                Log::debug("SQL : ".$sql);

                $s = $conn->execute($sql);
                if($r = $s->fetch('assoc')){
                    return $r;
                }
            }elseif($type == 1){
                $sql=sprintf("UPDATE `system_options` set version = %s where ID = 1" ,$version);

                Log::debug("SQL : ".$sql);

                if($conn->execute($sql)){
                    $sql=sprintf("SELECT version , system_live , system_message FROM `system_options` where ID = 1");

                    Log::debug("SQL : ".$sql);

                    $s = $conn->execute($sql);
                    if($r = $s->fetch('assoc')){
                        return $r;
                    }
                }
            }
            Log::debug("Ended ...updateStatusByUserId Dao");

        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }

    /**
     * 
     */
    public function updateStatusByUserId($user_id)
    {
        Log::debug("Started ...updateStatusByUserId Dao : User Id : ".$user_id);

        try{
            
            $conn = ConnectionManager::get('default');

            $sql=sprintf("UPDATE `users_extra_info` SET `status`='1' WHERE `user_id`='%s'",$user_id);

            Log::debug("SQL : ".$sql);

            $conn->execute($sql);

            Log::debug("Ended ...updateStatusByUserId Dao");

        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }
   

    /**
     * 
     */
    public function updateActivationKeyAddedOnByUserId($activationKey,$addedOn, $user_id, $type)
    {
        Log::debug("Started ...updateActivationKeyAddedOnByUserId Dao : Activation Key : ".$activationKey.", User Id : ".$user_id.
        " Added On : ".$addedOn." User Type : ".$type);

        try{
            
            $conn = ConnectionManager::get('default');

            $sql=sprintf("UPDATE `users_extra_info` SET `activation_key`='%s',`added_on`='%s' WHERE `user_id`='%s' and type='%s'",$activationKey,$addedOn,$user_id,$type);

            Log::debug("SQL : ".$sql);

            $conn->execute($sql);

            Log::debug("Ended ...updateActivationKeyAddedOnByUserId Dao");

        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }

        /**
     * 
     */
    public function getUsersExtraInfoByActivationKeyAndUserId($activationKey,$userId)
    {
        Log::debug("Started ...getUsersExtraInfoByActivationKeyAndUserId Dao");

        try{
            
            $conn = ConnectionManager::get('default');

            $sql=sprintf("SELECT * FROM users_extra_info WHERE activation_key = '%s' and user_id = %s", $activationKey,$userId);

            Log::debug("SQL : ".$sql);

            $stmt = $conn->execute($sql);

            $result = $stmt->fetch("assoc");

            Log::debug("Ended ...getUsersExtraInfoByActivationKeyAndUserId Dao");

            return $result;

        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }        
    

    /**
     * 
     */
    public function getUsersExtraInfoByDataAndTypeAndStatus($data,$type,$status)
    {
        Log::debug("Started ...getUsersExtraInfoByDataAndTypeAndStatus Dao");

        try{
            
            $conn = ConnectionManager::get('default');

            $sql=sprintf("SELECT * FROM users_extra_info WHERE type = '%s' and data ='%s' and status='%s'", $type,$data,$status);

            Log::debug("SQL : ".$sql);

            $stmt = $conn->execute($sql);

            $result = $stmt->fetch("assoc");

            Log::debug("Ended ...getUsersExtraInfoByDataAndTypeAndStatus Dao");

            return $result;

        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }        


    /**
     * 
     */
    public function saveUsersExtraInfo($args,$user_id,$date,$activation_key)
    {
        Log::debug("Started ...saveUsersExtraInfo Dao");

        try{
            
            $conn = ConnectionManager::get('default');
            $codes = new Codes;

            if($args['email'] != ""){
                $conn->execute(sprintf("INSERT INTO `users_extra_info`(`type`, `data`, `user_id`, `added_on`,`activation_key`) VALUES ('user_email','%s','%s','%s','%s')",$args['email'],$user_id,$date,$activation_key));
                }
                if($args['phone'] != ""){
                 $conn->execute(sprintf("INSERT INTO `users_extra_info`(`type`, `data`, `user_id`, `added_on`,`activation_key`) VALUES ('user_mobile','%s','%s','%s','%s')",$args['phone'],$user_id,$date,$activation_key));
              }

              Log::debug("Ended ...saveUsersExtraInfo Dao");
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }
    

    /**
     * Upsert Users Extra Info
     * Insert 
     * on duplicate key
     * update
     */
    public function upsertUsersExtraInfo($cityId)
    {   
        Log::debug("Started ...upsertUsersExtraInfo Dao");

        try{
            
            $conn = ConnectionManager::get('default');
            $codes = new Codes;


        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }

    public function faq($parentId , $query)
    {   
        Log::debug("Started ...upsertUsersExtraInfo Dao");

        try{
            
            $conn = ConnectionManager::get('default');
            $codes = new Codes;
            $arr = array();

            $sql = "SELECT * FROM faq where 1";

            if($parentId != -1){
                $sql .= " and parent_id = $parentId";
            }

            if($query != ''){
                $sql .= " and title like '%$query%' or answer like '%$query%'";
            }
            $sql .= " order by id asc";
            return $conn->execute($sql)->fetchAll('assoc');


        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }




    public function signupAdditional($input)
    {   
        Log::debug("Started ...upsertUsersExtraInfo Dao");

        try{
            
            $conn = ConnectionManager::get('default');
            $codes = new Codes;
            $sql=sprintf("SELECT * FROM `users` WHERE `user_id` = %s ", $input['user_id']);

            Log::debug("SQL : ".$sql);

            $stmt = $conn->execute($sql);

            if(!($r = $stmt->fetch('assoc'))){return ['status' => 0 , 'message'=> 'user_id does not exist'];}
            if(!isset($input['user_biography'])){$input['user_biography']=$r['user_biography'];}
            if(!isset($input['user_birthdate'])){$input['user_birthdate']=$r['user_birthdate'];}
            if(!isset($input['user_gender'])){$input['user_gender']=$r['user_gender'];}
            
            $file_name='';//LOGO PATH SAVED IN THIS
            $file_size = '';
            $height = 0;
            $width = 0;
            if(isset($_FILES['user_picture']) && $_FILES['user_picture']['size'] != 0) {
                        // valid inputs
                        if(!isset($_FILES["user_picture"]) || $_FILES["user_picture"]["error"] != UPLOAD_ERR_OK) {
                            throw new Exception("Something wrong with upload! Is 'upload_max_filesize' set correctly?");
                        }

                        // check file size
                        $max_allowed_size = $codes->MAX_FILE_SIZE * 1024;
                        if($_FILES["user_picture"]["size"] > $max_allowed_size) {
                            throw new Exception("The file size is so big");
                        }

                        $fileinfo = getimagesize($_FILES["user_picture"]["tmp_name"]);
                        $width = $fileinfo[0];
                        $height = $fileinfo[1];
                        $extension = pathinfo($_FILES['user_picture']['name'], PATHINFO_EXTENSION);
                        /* check & create uploads dir */

                        $folder = 'photos';
                            $depth = '../';
                            if(!file_exists($depth.$codes->UPLOADS_DIRECTORY.'/'.$folder)) {
                                @mkdir($depth.$codes->UPLOADS_DIRECTORY.'/'.$folder, 0777, true);
                            }
                            if(!file_exists($depth.$codes->UPLOADS_DIRECTORY.'/'.$folder.'/' . date('Y'))) {
                                @mkdir($depth.$codes->UPLOADS_DIRECTORY.'/'.$folder.'/' . date('Y'), 0777, true);
                            }
                            if(!file_exists($codes->UPLOADS_DIRECTORY.'/'.$folder.'/' . date('Y') . '/' . date('m'))) {
                                @mkdir($depth.$codes->UPLOADS_DIRECTORY.'/'.$folder.'/' . date('Y') . '/' . date('m'), 0777, true);
                            }

                        /* prepare new file name */
                        $directory = $folder.'/'. date('Y') . '/' . date('m') . '/';
                        $prefix =$codes->UPLOADES_PREFIX.'_'.md5(time()*rand(1, 9999));
                        $file_name = $directory.$prefix.'.'.$extension;
                        $path = $depth.$codes->UPLOADS_DIRECTORY.'/'.$file_name;
                                            
                        /* check if the file uploaded successfully */
                        if(!@move_uploaded_file($_FILES['user_picture']['tmp_name'], $path)) {
                            throw new Exception("Sorry, can not upload the photo");
                        }
                        
                        /* upload to amazon s3 */

                        if($codes->S3_ENABLED) {
                            aws_s3_upload($path, $file_name);
                        }
                        $file_size = ''.$height .'X'.$width.'' ;
            }
            
            //return $file_name;
            $sql = sprintf("UPDATE users set user_biography = '%s', user_birthdate = '%s' , user_gender = '%s' , user_picture = '%s' , user_pic_dimensions = '%s' WHERE user_id = %s" , $this->mysql_real_escape_string($input['user_biography']) , $input['user_birthdate'] , $input['user_gender'] , $file_name , $file_size , $input['user_id']);
            if($conn->execute($sql)){
                return ["status" => 1 , "message" => "success"];
            }else{
                return ["status" => 0 , "message" => "something went wrong"];
            }
            



        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }

    public function mysql_real_escape_string($inp) {
        if(is_array($inp))
            return array_map(__METHOD__, $inp);

        if(!empty($inp) && is_string($inp)) {
            return str_replace(array('\\', "\0", "\n", "\r", "'", '"', "\x1a"), array('\\\\', '\\0', '\\n', '\\r', "\\'", '\\"', '\\Z'), $inp);
        }

        return $inp;
    }

}