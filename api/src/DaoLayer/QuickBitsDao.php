<?php

namespace App\DaoLayer;

use App\Utils\Codes;
use Cake\Core\Exception\Exception;
use Cake\Datasource\ConnectionManager;
use App\DaoLayer\PointsDao;
use App\DaoLayer\PostsDao;
use App\Model\UserModel;
use Cake\Log\Log;

class QuickBitsDao
{

    private function getImgFullPath($img = '', $newPath = 0){
        $codes = new Codes;
        return !empty($img) ?$codes->SYSTEM_URL.'/'.($newPath?'api/':'').$codes->UPLOADS_DIRECTORY.'/'.$img:'';
    }

    
    public function mysql_real_escape_string($inp) {
        if(is_array($inp))
            return array_map(__METHOD__, $inp);

        if(!empty($inp) && is_string($inp)) {
            return str_replace(array('\\', "\0", "\n", "\r", "'", '"', "\x1a"), array('\\\\', '\\0', '\\n', '\\r', "\\'", '\\"', '\\Z'), $inp);
        }

        return $inp;
    }
    public function quickbitPoints($case , $userId , $qbId = 0){
        $pod = new PointsDao;
        $conn = ConnectionManager::get('default');
        $codes = new Codes;
        switch ($case) {
            case 'const':
                $k = $codes->QUICKBITS_k;
                $pod->addPoints('quickbits', $userId , $k);
                $ret = $pod->checkAndDeduct($userId, $codes->DEDUCT_QB);
                if($ret == 0){return $ret;}
                return $k;
                break;
            case 'by_content':
                $k = $codes->QUICKBITS_k;
                $l = $conn->execute(sprintf("SELECT * from `quickbits` where id = %s"  , $qbId));
                $res = $l->fetch('assoc');
                $ret = $pod->checkAndDeduct($res['creator_id'], $codes->DEDUCT_QB);
                if($ret == 0){return $ret;}
                $pod->addPoints('quickbits', $res['creator_id'] , $k);
                return $k;
                break;
            case 'like':
                $k = $codes->QUICKBITS_c;
                $pod->addPoints('quickbits', $userId , $k);
                return $k;
                break;
            case 'unlike':
                $k = $codes->QUICKBITS_c;
                $pod->addPoints('quickbits', $userId , -$k);
                return $k;
                break;
            case 'by_content_like':
                $k = $codes->QUICKBITS_c;
                $l = $conn->execute(sprintf("SELECT * from `quickbits` where id = %s"  , $qbId));
                $res = $l->fetch('assoc');
                $pod->addPoints('quickbits', $res['creator_id'] , $k);
                return $k;
                break;
            case 'by_content_unlike':
                $k = $codes->QUICKBITS_c;
                $l = $conn->execute(sprintf("SELECT * from `quickbits` where id = %s"  , $qbId));
                $res = $l->fetch('assoc');
                $pod->addPoints('quickbits', $res['creator_id'] , -$k);
                return $k;
                break;
            case 'remove':
                $k = $codes->QUICKBITS_k;
                $c = $codes->QUICKBITS_c;
                $l = $conn->execute(sprintf("SELECT * from `posts_likes` where post_id = (SELECT post_id from posts where quickbit_id = %s )"  , $qbId));
                $n = $l->count();
                $total = $k + ($n * $c);
                $l = $conn->execute(sprintf("SELECT * from `quickbits` where id = %s"  , $qbId));
                $res = $l->fetch('assoc');
                $pod->addPoints('quickbits', $res['creator_id'] , -$total);
                return -$total;
                break;
            case 'get':
                $k = $codes->QUICKBITS_k;
                $c = $codes->QUICKBITS_c;
                $l = $conn->execute(sprintf("SELECT * from `posts_likes` where post_id = (SELECT post_id from posts where quickbit_id = %s )"  , $qbId));
                $n = $l->count();
                $total = $k + ($n * $c);
                return $total;
                break;
            default:
                return null;
                break;
        }
    }
    /**
     * Save QuickBit
     */
    public function saveQuickBits($args, $card_pictures, $date)
    {

        try{
            $codes = new Codes;
            $conn = ConnectionManager::get('default');
            $id = $args['creator_id'];
            if($args['user_type'] == 'user' || $args['user_type'] == 'User' || $args['user_type'] == 'community' || $args['user_type'] == 'Community'){
                $sql = $conn->execute(sprintf("SELECT total FROM users_points where user_id = %s", $args['creator_id']));
                if($sql = $sql->fetch('assoc')){
                    if($sql['total'] >= $codes->DEDUCT_QB){

                    }else{
                        throw new Exception("not enough points");
                    }
                }else{
                    $conn->execute("INSERT into users_points set user_id = $id , total = $codes->INITIAL_PTS");
                    return $this->saveQuickBits($args, $card_pictures, $date);
                }
            }
            Log::debug("Started ...saveQuickBit Dao");
            //if(!isset($args['community_id'])){$args['community_id'] = 0;}
            $sql=sprintf("INSERT INTO `quickbits`(`title`, `user_type`, `creator_id`,  `interests`, no_of_cards , community_id) VALUES ('%s', '%s', '%s', '%s', '%s' , %s)", 
            $args['title'], $args['user_type'], $args['creator_id'], @$args['interests'], count($args['card_text']) ,(isset($args['community_id'])?$args['community_id']:0) );

            Log::debug("SQL : ".$sql);

            $conn->execute($sql);
            
            $sql="SELECT LAST_INSERT_ID()";
            
            $stmt = $conn->execute($sql);
            
            $res = $stmt->fetch();
            $arr = [];
            foreach($args['color_code'] as $i => $imgbg){
                array_push($arr, $imgbg);
            }
            Log::debug("Ended ...saveQuickBit Dao");
            if($res[0]){
                $itr = 0;
                foreach ($args['card_text'] as $i => $card_text ) {
                    $sql=sprintf("INSERT INTO `quickbits_cards`(`card_text`, `picture`, `picture_dimensions`, quickbit_id , color_code) VALUES ('%s', '%s', '%s', '%s' , '%s')", $this->mysql_real_escape_string($card_text), @$card_pictures[$i]['image'], @$card_pictures[$i]['dimensions'], $res[0], $arr[$itr] );
                    Log::debug("SQL : ".$sql);

                    $conn->execute($sql);
                    $itr = $itr +1;
                }

                $sql=sprintf("INSERT INTO posts (quickbit_id, user_id, post_title, community_id, post_type, privacy, text_tagged, origin_type, organisation_id, payment,total,updated_at, time , user_type) VALUES (%s , '%s', '%s', '%s', 'quickbit', 'public', '', '', 0, 0, 0, '%s', '%s' , '%s')", $res[0], $args['creator_id'], $args['title'], (isset($args['community_id'])?$args['community_id']:0), $date, $date, $args['user_type']);
                
                Log::debug("SQL : ".$sql);

                $conn->execute($sql);
                if($args['user_type'] == 'user' || $args['user_type'] == 'User'){
                    $ret = $this->quickbitPoints('const' , $args['creator_id'] , $res[0]);
                    if($ret == 0){throw new Exception("not enough points.");}
                }

                Log::debug("Ended ...savePosts Dao");

            }
            
            return $res[0];
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }    
    /**
     * Update QuickBit
     */
    public function updateQuickBits($args, $card_pictures, $date)
    {
        Log::debug("Started ...updateQuickBits Dao");

        try{

            $conn = ConnectionManager::get('default');

            $sql=sprintf("UPDATE quickbits SET title = '%s', user_type = '%s', creator_id = '%s', color_code = '#ffffff', interests = '%s', no_of_cards = '%s'
                WHERE id = '%s' AND creator_id = '%s'", $args['title'], $args['user_type'], $args['creator_id'], @$args['interests'], count($args['card_text']), $args['quickbit_id'], $args['creator_id']);

            Log::debug("SQL : ".$sql);

            if($conn->execute($sql)){

                //deleting old card
                $sql=sprintf("DELETE FROM `quickbits_cards` WHERE quickbit_id = '%s'", 
                $args['quickbit_id']);
                Log::debug("SQL : ".$sql);
                $conn->execute($sql);

                $arr = [];
                foreach($args['color_code'] as $i => $imgbg){
                array_push($arr, $imgbg);
                }
                $itr = 0;
                //inserting new card
                foreach ($args['card_text'] as $i => $card_text) {
                    $sql=sprintf("INSERT INTO `quickbits_cards`(`card_text`, `picture`, `picture_dimensions`, quickbit_id , color_code) VALUES ('%s', '%s', '%s', '%s', '%s')", $this->mysql_real_escape_string($card_text), @$card_pictures[$i]['image'], @$card_pictures[$i]['dimensions'], $args['quickbit_id'], $arr[$itr]);
                    Log::debug("SQL : ".$sql);

                    $conn->execute($sql);
                    $itr = $itr +1;
                }

                $sql=sprintf("UPDATE posts SET post_title = '%s', community_id = '%s' WHERE quickbit_id = '%s'", $args['title'], (isset($args['community_id'])?$args['community_id']:0), $args['quickbit_id']);
                
                Log::debug("SQL : ".$sql);

                $conn->execute($sql);

                Log::debug("Ended ...savePosts Dao");

            }
           
            Log::debug("Ended ...updateQuickBits Dao");
            
            return 1;
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }
    /**
     * Delete QuickBit
     */
    public function deleteQuickBits($quickbit_id, $creator_id)
    {
        Log::debug("Started ...deleteQuickBits Dao");

        try{

            $conn = ConnectionManager::get('default');

            //deleting questions
            $sql=sprintf("UPDATE quickbits SET deleted = 1 WHERE id = '%s' AND creator_id = '%s'", $quickbit_id, $creator_id);
            Log::debug("SQL : ".$sql);
            
            if ($conn->execute($sql)) {
                //deleting questions
                $sql=sprintf("DELETE FROM `posts` WHERE quickbit_id = '%s'", 
                $quickbit_id);
                Log::debug("SQL : ".$sql);
                $conn->execute($sql);
            }
           
            Log::debug("Ended ...deleteQuickBits Dao");
            
            return 1;
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }
    /**
     * Get QuickBits
     */
    public function getQuickBitsList($args, $date)
    {
        Log::debug("Started ...getQuickBitsList Dao");
        $codes = new Codes;

        try{

            $conn = ConnectionManager::get('default');
            $cond = "";
            //checking filter params
            if (isset($args['interests']) && !empty($args['interests'])) {
                //options: 
                // 1. My interests
                // 2. Custom list of interest Ids
                $interestCond = '';
                if($args['interests'] == 'My interests'){
                    
                    $sql=sprintf("SELECT * FROM user_interest where user_id= '%s'", $args['user_id']);
                    Log::debug("SQL : ".$sql);
                    $stmt = $conn->execute($sql);
                    $key = 0;
                    while($result = $stmt->fetch("assoc")) {
                        $interestCond .= ($key?' OR':'')." FIND_IN_SET('".$result['interest']."', qb.interests)";
                        $key++;
                    }
                }
                else if(is_array($args['interests'])){
                    $interestCond = '';
                    foreach ($args['interests'] as $key => $interest) {
                        $interestCond .= ($key?' OR':'')." FIND_IN_SET('".$interest."', qb.interests) > 0";
                    }
                }
                // condition according to intrest
                $cond .= ($interestCond)?" AND (".$interestCond.")":'';

            }
            // condition for query text
            if (isset($args['query_text']) && !empty($args['query_text'])) {
                $cond .= " AND qb.title like '%".$args['query_text']."%'";
            }


            //checking keys for pagination
            $args['page'] = (isset($args['page']) && $args['page'] > 1)?$args['page']:1;
            $args['pageSize'] = (isset($args['pageSize']) && $args['pageSize'] > 0)?$args['pageSize']:10;

            $cond .= " order by id desc limit ".(($args['page']- 1) * $args['pageSize']).", ".$args['pageSize'];

            //quickBit query
            $sql="SELECT qb.*, us.user_fullname
                FROM  `quickbits` as qb left join users as us on us.user_id = qb.creator_id WHERE qb.deleted = 0 $cond";
            Log::debug("SQL : ".$sql);
            // echo $sql; exit;
            $stmt = $conn->execute($sql);
            
            $results = array();

            while($result = $stmt->fetch("assoc")) {
                //seting image full path

                $result['quickBitInterestArr'] = [];

                // checking creater info
                $sql=sprintf("SELECT user_id, user_email, user_name, user_fullname, user_firstname, user_lastname, user_gender, user_picture, user_work, user_work_title , user_work_place , '' as connection FROM users WHERE user_id = '%s' limit 0, 1", $result['creator_id']);

                Log::debug("SQL : ".$sql);
                $createrStmt = $conn->execute($sql);
                $result['created_by'] = $createrStmt->fetch("assoc");
                $result['created_by'] = $result['created_by']?$result['created_by']:[];
                //setting user image
                if(isset($result['created_by']['user_picture']) && !empty($result['created_by']['user_picture']))
                    $result['created_by']['user_picture'] = $this->getImgFullPath(@$result['created_by']['user_picture']);
                //getting cards array
                $carr = array();
                $sql = sprintf("SELECT * FROM quickbits_cards WHERE quickbit_id = %s",$result['id']);
                Log::debug("SQL : ".$sql);
                $st = $conn->execute($sql);
                while($r = $st->fetch('assoc')){
                    if(isset($r['picture']) && !empty($r['picture'])){
                        $r['picture'] = $this->getImgFullPath($r['picture'],1); 
                    }
                    array_push($carr, $r);
                }
                $result['cardArr'] = $carr;

                if($result['interests']){

                    // checking interests
                    $sql=sprintf("SELECT interest_id, `text` as interest_name FROM interest_mst WHERE FIND_IN_SET(interest_id, '%s')", $result['interests']);

                    Log::debug("SQL : ".$sql);
                    $interestStmt = $conn->execute($sql);

                    while($interestResult = $interestStmt->fetch("assoc"))
                        array_push($result['quickBitInterestArr'], $interestResult);

                }
                array_push($results,$result);
            }
           
            Log::debug("Ended ...getQuickBitsList Dao");

            return $results;
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }
    public function getQuickBitByUserAndId($quickbit_id, $user_id, $date)
    {
        Log::debug("Started ...getQuickBitByUserAndId Dao");
        $codes = new Codes;
        $pd = new PostsDao;

        try{

            $conn = ConnectionManager::get('default');

            //getting quickBit details
            $sql=sprintf("SELECT qb.*, us.user_fullname
                FROM `quickbits` as qb left join users as us on us.user_id = qb.creator_id WHERE qb.id = '%s' limit 0, 1", $quickbit_id);

            Log::debug("SQL : ".$sql);
            $quickBitStmt = $conn->execute($sql);
            $quickBitData = $quickBitStmt->fetch("assoc");
            if($quickBitData){
                //checking quickBit is deleted
                if($quickBitData['deleted'])
                    return ['deleted'=>1];

                $quickBitData['quickBitInterestArr'] = [];
                $quickBitData['cardArr'] = [];


                // checking creater info
                //$sql=sprintf("SELECT user_id, user_email, user_name, user_fullname, user_firstname, user_lastname, user_gender, user_picture, user_work, user_work_title , user_work_place , '' as connection FROM users WHERE user_id = '%s' limit 0, 1", $quickBitData['creator_id']);
                $dat = $pd->userData($user_id , $quickBitData['creator_id'] , $quickBitData['user_type'] , $quickBitData['community_id']);
                $quickBitData['created_by'] = (isset($dat[0])? $dat[0]  : null);
                $quickBitData['community_data'] = (isset($dat[1])? $dat[1] : null);
                Log::debug("SQL : ".$sql);

                //setting user image
                if(isset($quickBitData['created_by']['user_picture']) && !empty($quickBitData['created_by']['user_picture']))
                    $quickBitData['created_by']['user_picture'] = $this->getImgFullPath($quickBitData['created_by']['user_picture']);



                // checking cards
                $sql=sprintf("SELECT * FROM quickbits_cards WHERE quickbit_id = '%s' order by id", $quickBitData['id']);

                Log::debug("SQL : ".$sql);
                $cardStmt = $conn->execute($sql);

                while($result = $cardStmt->fetch("assoc")){
                    $result['picture'] = $this->getImgFullPath($result['picture'], 1);

                    array_push($quickBitData['cardArr'], $result);
                }

                $pd = $conn->execute("SELECT post_id FROM posts where quickbit_id = $quickbit_id");
                $pd = $pd->fetch('assoc');
                $pid = $pd["post_id"];

                //reactions
                $react = array();
                $rname = array();
                $reactions = $conn->execute(sprintf("SELECT * from `reactions` "));
                while($r = $reactions->fetch('assoc')){
                    $str = '' . $r['id'] . '';
                    //$react[$str] = 0;
                    array_push($react, 0);
                    array_push($rname, $r['name']);


                }
                $count = count($rname);
                //$pc = $conn->execute(sprintf("SELECT * from `posts` where post_type = 'article' and article_id = %s"  , $articlesId));

                //if(!$p = $pc->fetch("assoc")){return "no post found";}
                $l = $conn->execute(sprintf("SELECT * from `posts_likes` where post_id = $pid"));
                $t = 0;
                while($r = $l->fetch("assoc")){
                    $t = $t + 1;
                    $str = $r['reaction_type'];
                    $react[$str] = $react[$str] + 1;
                }
                $fr = array();
                for ($i=0; $i < $count; $i++) { 
                    # code...
                    $fr[$rname[$i]] = $react[$i];
                }

                $quickBitData['reactions'] = $fr;
                $quickBitData['total_likes'] = $t;
                $quickBitData['points'] = $this->quickbitPoints('get', $quickBitData['creator_id'] , $quickbit_id);
                
                $il = $conn->execute(sprintf("SELECT * FROM posts_likes WHERE post_id = $pid and user_id = $user_id"));
                if($li = $il->fetch("assoc")){
                    $quickBitData["i_like"] = $li['reaction_type'];
                }else{$quickBitData["i_like"] = null;}

                //setting interests
                if($quickBitData['interests']){

                    // checking interests
                    $sql=sprintf("SELECT interest_id, `text` as interest_name FROM interest_mst WHERE FIND_IN_SET(interest_id, '%s')", $quickBitData['interests']);

                    Log::debug("SQL : ".$sql);
                    $interestStmt = $conn->execute($sql);

                    while($result = $interestStmt->fetch("assoc"))
                        array_push($quickBitData['quickBitInterestArr'], $result);

                }
                $user = new UserModel($user_id);
                $mf = $user->get_friends_ids($user_id);
                $mf = implode(',', $mf);
                if($co = $conn->execute(sprintf("SELECT * FROM posts_comments WHERE node_id = $pid and node_type = 'user' and FIND_IN_SET(user_id , '$mf') order by comment_id desc"))->fetch('assoc')){
                    $friend = $pd->userData($user_id , $co['user_id'] , 'user' , 0)[0];
                    $co['user'] = $friend;
                    $quickBitData['friend_comment'] = $co;
                }else{
                    $quickBitData['friend_comment'] = null;
                }
                if($il = $conn->execute(sprintf("SELECT * FROM posts_likes WHERE post_id = $pid and FIND_IN_SET(user_id , '$mf')"))->fetch('assoc')){
                    $friend = $pd->userData($user_id , $il['user_id'] , 'user' , 0)[0];
                    $il['user'] = $friend;
                    $quickBitData['friend_like'] = $il;
                }else{
                    $quickBitData['friend_like'] = null;
                }


            }

            Log::debug("Ended ...getQuickBitByUserAndId Dao");

            return $quickBitData;
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }
    
    /**
     * getQuickBitCountByUserId
     */
    public function getQuickBitCountByUserId($userId)
    {
        Log::debug("Started ...getQuickBitCountByUserId Dao : User Id : ".$userId);

        try{
            
            $conn = ConnectionManager::get('default');

            $sql=sprintf("SELECT count(*) as total FROM `quickbits` WHERE `creator_id` =  '%s'", $userId);

            Log::debug("SQL : ".$sql);

            $stmt = $conn->execute($sql);

            $result = $stmt->fetch("assoc");

            Log::debug("Ended ...getUserByUserIdAndLoginOtp Dao");

            return $result;
            
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }

    }

    private function connection($logged_in_user_id, $user_id, $followings_ids = [], $friends_ids = [], $friend_requests_ids = [], $friend_requests_sent_ids = [])
    {
        /* check if the viewer is the target */
        if ($user_id == $logged_in_user_id) {
            return "me";
        }
        // Arnab mohanty:: on 2nd Jan 2019
        if (in_array($user_id, $followings_ids)) {
            /* the viewer follow the target */
            return "followed";
        }
        /* check if the viewer & the target are friends */
        if (in_array($user_id, $friends_ids)) {
            return "remove";
        }
        /* check if the target sent a request to the viewer */
        if (in_array($user_id, $friend_requests_ids)) {
            return "request";
        }
        /* check if the viewer sent a request to the target */
        if (in_array($user_id, $friend_requests_sent_ids)) {
            return "cancel";
        }
        
        
        /* there is no relation between the viewer & the target */
        return "add";
        
    }
    



}