<?php

namespace App\DaoLayer;

use App\Utils\Codes;
use Cake\Core\Exception\Exception;
use Cake\Datasource\ConnectionManager;
use Cake\Log\Log;

class ArticlesDiscussionDao
{

	/**
	 * Save Articles Discussion
	 */
	public function saveArticlesDiscussion($id, $discussionPost, $userId, $date, $articleCommentId)
	{
		Log::debug("Started ...saveArticlesDiscussion Dao : Articles Id : ".$id.", Post : ".$discussionPost.", User Id : ".$userId.", Status : 1, Added On : ".$date.", Modified On : ".$date.", Parent Comment Id : ".$articleCommentId);

		try{
			
			$conn = ConnectionManager::get('default');

			$sql=sprintf("INSERT INTO `articles_discussion` (articles_id, post,user_id,status,added_on,modified_on,parent_comment_id) VALUES (%s, %s, %s,1, %s, %s,%s)", $id, $discussionPost, $userId, $date, $date,$articleCommentId);

			Log::debug("SQL : ".$sql);

			$conn->execute($sql);

			Log::debug("Ended ...saveArticlesDiscussion Dao");

		}catch(\Exception $e){
			Log::debug($e);
			throw new Exception($e);
		}
	}

   
}
