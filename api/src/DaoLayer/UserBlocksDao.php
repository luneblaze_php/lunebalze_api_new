<?php

namespace App\DaoLayer;

use App\Utils\Codes;
use Cake\Core\Exception\Exception;
use Cake\Datasource\ConnectionManager;
use Cake\Log\Log;

class UserBlocksDao
{
	/**
	 * Get Users Blocks
	 */
	public function getUsersBlocksByUserIdAndBlockId($userId=0, $blockedId=0)
	{
		Log::debug("Started ...saveUsersBlocks Dao : User Id : ".$userId.", Block Id : ".$blockedId);

		try{
			
			$conn = ConnectionManager::get('default');

			$sql=sprintf("SELECT * FROM users_blocks WHERE (user_id = %1$s AND blocked_id = %2$s) OR (user_id = %2$s AND blocked_id = %1$s)", $userId, $blockedId);
			
			Log::debug("SQL : ".$sql);

			$conn->execute($sql);

			Log::debug("Ended ...saveUsersBlocks Dao");

		}catch(\Exception $e){
			Log::debug($e);
			throw new Exception($e);
		}
	}

	/**
	 * Save Users Blocks
	 */
	public function saveUsersBlocks($userId, $blockedId)
	{
		Log::debug("Started ...saveUsersBlocks Dao : User Id : ".$userId.", Block Id : ".$blockedId);

		try{
			
			$conn = ConnectionManager::get('default');

			$sql=sprintf("INSERT INTO users_blocks (user_id, blocked_id) VALUES (%s, %s)", $userId, $blockedId);
			
			Log::debug("SQL : ".$sql);

			$conn->execute($sql);

			Log::debug("Ended ...saveUsersBlocks Dao");

		}catch(\Exception $e){
			Log::debug($e);
			throw new Exception($e);
		}
	}

	/**
	 * Delete Users Blocks using user_id and blocked_id
	 */
	public function deleteUsersBlocksByUserIdAndBlockedId($userId, $blockedId)
	{
		Log::debug("Started ...deleteUsersBlocksByUserIdAndBlockedId Dao : User Id : ".$userId.", Block Id : ".$blockedId);

		try{
			
			$conn = ConnectionManager::get('default');

			$sql=sprintf("DELETE FROM users_blocks WHERE user_id = %s AND blocked_id = %s", $userId, $blockedId);

			Log::debug("SQL : ".$sql);

			$conn->execute($sql);

			Log::debug("Ended ...deleteUsersBlocksByUserIdAndBlockedId Dao");

		}catch(\Exception $e){
			Log::debug($e);
			throw new Exception($e);
		}
	}
   
    
}