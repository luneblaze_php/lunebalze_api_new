<?php

namespace App\DaoLayer;

use App\Utils\Codes;
use Cake\Core\Exception\Exception;
use Cake\Datasource\ConnectionManager;
use Cake\Log\Log;

class PostsCommentsDao
{

	/**
	 * Get Posts Comments by Comment Id
	 */
	public function getPostsCommentsByCommentId($commentId)
	{
		Log::debug("Started ...getPostsCommentsByCommentId Dao : Comment Id : ".$commentId);

		try{
			
			$conn = ConnectionManager::get('default');

			$sql=sprintf("SELECT * FROM posts_comments WHERE comment_id = %s", $commentId);
		
			Log::debug("SQL : ".$sql);

			$stmt = $conn->execute($sql);

			$result = $stmt->fetch("assoc");

			Log::debug("Ended ...getPostsCommentsByCommentId Dao");

			return $result;

		}catch(\Exception $e){
			Log::debug($e);
			throw new Exception($e);
		}
	} 



    /**
     * get_comments
     *
     * @param integer $node_id
     * @param integer $offset
     * @param boolean $is_post
     * @param boolean $pass_privacy_check
     * @param array $post
     * @return array
     */
    public function getPostComments($node_id, $offset = 0, $is_post = true, $pass_privacy_check = true, $post = array())
    {
        $conn = ConnectionManager::get('default');
        $stmt = $conn->execute(sprintf("SELECT * FROM (SELECT posts_comments.*, users.user_name, users.user_fullname, users.user_gender, users.user_picture, users.user_verified, pages.* FROM posts_comments LEFT JOIN users ON posts_comments.user_id = users.user_id AND posts_comments.user_type = 'user' LEFT JOIN pages ON posts_comments.user_id = pages.page_id AND posts_comments.user_type = 'page' WHERE NOT (users.user_name <=> NULL AND pages.page_name <=> NULL) AND posts_comments.node_type = 'post' AND posts_comments.node_id = %s ORDER BY posts_comments.comment_id DESC LIMIT %s, %s) comments ORDER BY comments.comment_id ASC", (int)$node_id, (int)$offset, $system['min_results']));
		$comments = $stmt->fetch("assoc");
		return $comments;
    }

    public function insertPostComment($comment=[])
    {
        $conn = ConnectionManager::get('default');
        $stmt = $conn->execute(sprintf("INSERT INTO posts_comments (node_id, node_type, user_id, user_type, text, image, time) VALUES (%s, %s, %s, %s, %s, %s, %s)", $comment['node_id'], $comment['node_type'], $comment['user_id'], $comment['user_type'], $comment['text'], $comment['image'], $comment['time']));
        $conn->execute($sql);
        
        $sql="SELECT LAST_INSERT_ID()";
        $stmt = $conn->execute($sql);
        $res = $stmt->fetch();
        return $res[0];
    }

    public function getPhotoComments($node_id, $offset = 0, $is_post = true, $pass_privacy_check = true, $post = array())
    {
        $conn = ConnectionManager::get('default');
        $stmt = $conn->execute(sprintf("SELECT * FROM (SELECT posts_comments.*, users.user_name, users.user_fullname, users.user_gender, users.user_picture, users.user_verified, pages.* FROM posts_comments LEFT JOIN users ON posts_comments.user_id = users.user_id AND posts_comments.user_type = 'user' LEFT JOIN pages ON posts_comments.user_id = pages.page_id AND posts_comments.user_type = 'page' WHERE NOT (users.user_name <=> NULL AND pages.page_name <=> NULL) AND posts_comments.node_type = 'photo' AND posts_comments.node_id = %s ORDER BY posts_comments.comment_id DESC LIMIT %s, %s) comments ORDER BY comments.comment_id ASC", (int)$node_id, (int)$offset, $system['min_results'] ));
		$comments = $stmt->fetch("assoc");

		return $comments;
    }
    
    /**
     * get_comments
     *
     * @param integer $node_id
     * @param integer $offset
     * @param boolean $is_post
     * @param boolean $pass_privacy_check
     * @param array $post
     * @return array
     */
    public function get_comments_next_prev($node_id, $offset = 0, $is_post = true, $pass_privacy_check = true, $post = array(),$comment_id = 0,$order='prev',$target=0)
    {
        global $db, $system;
        $comments = array();
        
        $operator = $order == "next" ? ">=" : "<=";
        
        //self::$system['min_results'] = 15;
        //$offset *= self::$system['min_results'];
        /* get comments */
        if ($is_post) {
            /* get post comments */
            if (!$pass_privacy_check) {
                /* (check|get) post */
                $post = self::_check_post($node_id, false);
                if (!$post) {
                    return false;
                }
            }
            $post = self::get_post($node_id,false);
			$commet_id=$comment_id;
			if($target==1 && $order == 'prev' && $comment_id!=0){$comment_id=0;}
			            if($comment_id == '0' &&  $order == 'prev') {
			if($target==1 && $commet_id!=0){
                $get_comments = self::$conn->execute(sprintf("Select * from (select * from (
			    SELECT
			        posts_comments.*,
			        users.user_name,
			        users.user_fullname,
			        users.user_gender,
			        users.user_picture,
			        users.user_verified,
			        reports.report_id
			    FROM
			        posts_comments
			    LEFT JOIN users ON posts_comments.user_id = users.user_id AND posts_comments.user_type = 'user'
			    LEFT JOIN reports ON(
			            reports.node_id = posts_comments.comment_id AND reports.node_type = 'comment' AND reports.user_id = '".self::$_data['user_id']."'
			        )
			    WHERE posts_comments.comment_id = $commet_id)comments UNION SELECT * FROM (SELECT posts_comments.*, users.user_name, users.user_fullname, users.user_gender, users.user_picture, users.user_verified,reports.report_id FROM posts_comments LEFT JOIN users ON posts_comments.user_id = users.user_id AND posts_comments.user_type = 'user' LEFT JOIN pages ON posts_comments.user_id = pages.page_id AND posts_comments.user_type = 'page' LEFT JOIN reports on (reports.node_id = posts_comments.comment_id AND reports.node_type='comment' AND reports.user_id = '".self::$_data['user_id']."') WHERE NOT (users.user_name <=> NULL AND pages.page_name <=> NULL) AND posts_comments.node_type = 'post' AND posts_comments.node_id = %s ORDER BY posts_comments.comment_id DESC LIMIT 15) comments) comm limit 15", self::secure($node_id, 'int'))) or self::_apiError('SQL_apiError_THROWEN');
			}
			else
			{
		$get_comments = self::$conn->execute(sprintf("SELECT * FROM (SELECT posts_comments.*, users.user_name, users.user_fullname, users.user_gender, users.user_picture, users.user_verified,reports.report_id FROM posts_comments LEFT JOIN users ON posts_comments.user_id = users.user_id AND posts_comments.user_type = 'user' LEFT JOIN pages ON posts_comments.user_id = pages.page_id AND posts_comments.user_type = 'page' LEFT JOIN reports on (reports.node_id = posts_comments.comment_id AND reports.node_type='comment' AND reports.user_id = '".self::$_data['user_id']."') WHERE NOT (users.user_name <=> NULL AND pages.page_name <=> NULL) AND posts_comments.node_type = 'post' AND posts_comments.node_id = %s ORDER BY posts_comments.comment_id DESC LIMIT 15) comments ORDER BY comments.comment_id DESC", self::secure($node_id, 'int'))) or self::_apiError('SQL_apiError_THROWEN');
}
                
                $get_total_comments = self::$conn->execute(sprintf("SELECT * FROM (SELECT posts_comments.*, users.user_name, users.user_fullname, users.user_gender, users.user_picture, users.user_verified,reports.report_id FROM posts_comments LEFT JOIN users ON posts_comments.user_id = users.user_id AND posts_comments.user_type = 'user' LEFT JOIN pages ON posts_comments.user_id = pages.page_id AND posts_comments.user_type = 'page' LEFT JOIN reports on (reports.node_id = posts_comments.comment_id AND reports.node_type='comment' AND reports.user_id = '".self::$_data['user_id']."') WHERE NOT (users.user_name <=> NULL AND pages.page_name <=> NULL) AND posts_comments.node_type = 'post' AND posts_comments.node_id = %s ORDER BY posts_comments.comment_id DESC) comments ORDER BY comments.comment_id DESC", self::secure($node_id, 'int'))) or self::_apiError('SQL_apiError_THROWEN');

                $get_total_comments_prev=array();
            } else {
                if($order == 'next'){
                    $comment_id .= " ORDER BY posts_comments.comment_id ASC";
                } else {
                    $comment_id  .= " ORDER BY posts_comments.comment_id DESC";
                }
                
                $get_comments = self::$conn->execute(sprintf("SELECT * FROM (SELECT posts_comments.*, users.user_name, users.user_fullname, users.user_gender, users.user_picture, users.user_verified,reports.report_id FROM posts_comments LEFT JOIN users ON posts_comments.user_id = users.user_id AND posts_comments.user_type = 'user' LEFT JOIN pages ON posts_comments.user_id = pages.page_id AND posts_comments.user_type = 'page' LEFT JOIN reports on (reports.node_id = posts_comments.comment_id AND reports.node_type='comment' AND reports.user_id = '".self::$_data['user_id']."') WHERE NOT (users.user_name <=> NULL AND pages.page_name <=> NULL) AND posts_comments.node_type = 'post' AND posts_comments.node_id = %s AND posts_comments.comment_id $operator $comment_id LIMIT 15) comments ORDER BY comments.comment_id DESC", self::secure($node_id, 'int'))) or self::_apiError('SQL_apiError_THROWEN');
                
                $get_total_comments = self::$conn->execute(sprintf("SELECT * FROM (SELECT posts_comments.*, users.user_name, users.user_fullname, users.user_gender, users.user_picture, users.user_verified,reports.report_id FROM posts_comments LEFT JOIN users ON posts_comments.user_id = users.user_id AND posts_comments.user_type = 'user' LEFT JOIN pages ON posts_comments.user_id = pages.page_id AND posts_comments.user_type = 'page' LEFT JOIN reports on (reports.node_id = posts_comments.comment_id AND reports.node_type='comment' AND reports.user_id = '".self::$_data['user_id']."') WHERE NOT (users.user_name <=> NULL AND pages.page_name <=> NULL) AND posts_comments.node_type = 'post' AND posts_comments.node_id = %s AND posts_comments.comment_id $operator $comment_id) comments ORDER BY comments.comment_id DESC", self::secure($node_id, 'int'))) or self::_apiError('SQL_apiError_THROWEN');
                
                $get_total_comments_prev = self::$conn->execute(sprintf("SELECT * FROM (SELECT posts_comments.*, users.user_name, users.user_fullname, users.user_gender, users.user_picture, users.user_verified,reports.report_id FROM posts_comments LEFT JOIN users ON posts_comments.user_id = users.user_id AND posts_comments.user_type = 'user' LEFT JOIN pages ON posts_comments.user_id = pages.page_id AND posts_comments.user_type = 'page' LEFT JOIN reports on (reports.node_id = posts_comments.comment_id AND reports.node_type='comment' AND reports.user_id = '".self::$_data['user_id']."') WHERE NOT (users.user_name <=> NULL AND pages.page_name <=> NULL) AND posts_comments.node_type = 'post' AND posts_comments.node_id = %s AND posts_comments.comment_id > $comment_id) comments ORDER BY comments.comment_id DESC", self::secure($node_id, 'int'))) or self::_apiError('SQL_apiError_THROWEN');
            }
            
        } else {
            /* get photo comments */
            /* check privacy */
            if (!$pass_privacy_check) {
                /* (check|get) photo */
                $photo = self::get_photo($node_id);
                if (!$photo) {
                    self::_apiError(403);
                }
                $post = $photo['post'];
            }
            /* get photo comments */
            $get_comments = self::$conn->execute(sprintf("SELECT * FROM (SELECT posts_comments.*, users.user_name, users.user_fullname, users.user_gender, users.user_picture, users.user_verified, pages.* FROM posts_comments LEFT JOIN users ON posts_comments.user_id = users.user_id AND posts_comments.user_type = 'user' LEFT JOIN pages ON posts_comments.user_id = pages.page_id AND posts_comments.user_type = 'page' WHERE NOT (users.user_name <=> NULL AND pages.page_name <=> NULL) AND posts_comments.node_type = 'photo' AND posts_comments.node_id = %s ORDER BY posts_comments.comment_id DESC LIMIT %s, %s) comments ORDER BY comments.comment_id ASC", self::secure($node_id, 'int'), self::secure($offset, 'int', false), self::secure(self::$system['min_results'], 'int', false))) or self::_apiError('SQL_apiError_THROWEN');
            
            $get_total_comments = self::$conn->execute(sprintf("SELECT * FROM (SELECT posts_comments.*, users.user_name, users.user_fullname, users.user_gender, users.user_picture, users.user_verified, pages.* FROM posts_comments LEFT JOIN users ON posts_comments.user_id = users.user_id AND posts_comments.user_type = 'user' LEFT JOIN pages ON posts_comments.user_id = pages.page_id AND posts_comments.user_type = 'page' WHERE NOT (users.user_name <=> NULL AND pages.page_name <=> NULL) AND posts_comments.node_type = 'photo' AND posts_comments.node_id = %s ORDER BY posts_comments.comment_id DESC) comments ORDER BY comments.comment_id ASC", self::secure($node_id, 'int'))) or self::_apiError('SQL_apiError_THROWEN');
            $get_total_comments_prev=array();
        }
        if ($get_comments->num_rows > 0) {
       
            while ($comment = $get_comments->fetch('assoc')) {
                if(!self::blocked($comment['user_id'])){
                    $comment['liked_users'] = [];
                    $liked_users_query = self::$conn->execute(sprintf("SELECT * FROM posts_comments_likes WHERE comment_id = %s", self::secure($comment['comment_id'], 'int'))) or self::_apiError('SQL_apiError_THROWEN');
                    while($liked_users = $liked_users_query->fetch('assoc')){
                        if(!self::blocked($liked_users['user_id'])){
                            $comment['liked_users'][] = self::get_user_by_id($liked_users['user_id'])[0];
                        }
                    }
                    $comment['likes'] = count($comment['liked_users']);
                    /* parse_api text */
                    $comment['text_plain'] = $comment['text'];
                    $comment['text'] = parse_api($comment['text']);
                    /* get the comment author */
                    if ($comment['user_type'] == "user") {
                        /* user type */
                        $comment['author_id'] = $comment['user_id'];
                        $comment['author_picture'] = self::get_picture($comment['user_picture'], $comment['user_gender']);
                        $comment['author_url'] = self::$system['system_url'] . '/' . $comment['user_name'];
                        $comment['author_name'] = $comment['user_fullname'];
                        $comment['author_verified'] = $comment['user_verified'];
                        $comment['connection'] = self::connection1($comment['user_id']);
                    } else {
                        /* page type */
                        $comment['author_id'] = $comment['page_admin'];
                        $comment['author_picture'] = self::get_picture($comment['page_picture'], "page");
                        $comment['author_url'] = self::$system['system_url'] . '/pages/' . $comment['page_name'];
                        $comment['author_name'] = $comment['page_title'];
                        $comment['author_verified'] = $comment['page_verified'];
                    }
                    $comment['i_like'] = false;
                    $comment['i_reported'] = 0;
                    if($comment['report_id']){
                        $comment['i_reported'] = 1;
                    }
                    /* check if viewer user likes this comment */
                    if (self::$_logged_in && $comment['likes'] > 0) {
                        $check_like = self::$conn->execute(sprintf("SELECT * FROM posts_comments_likes WHERE user_id = %s AND comment_id = %s", self::secure(self::$_data['user_id'], 'int'), self::secure($comment['comment_id'], 'int'))) or self::_apiError('SQL_apiError_THROWEN');
                        $comment['i_like'] = ($check_like->num_rows > 0) ? true : false;
                    }
                    /* check if viewer can manage comment [Edit|Delete] */
                    $comment['edit_comment'] = false;
                    $comment['delete_comment'] = false;
                    if (self::$_logged_in) {
                        /* viewer is (admins|moderators)] */
                        if (self::$_data['user_group'] < 3) {
                            $comment['edit_comment'] = true;
                            $comment['delete_comment'] = true;
                        }
                        /* viewer is the author of comment */
                        if (self::$_data['user_id'] == $comment['author_id']) {
                            $comment['edit_comment'] = true;
                            $comment['delete_comment'] = true;
                        }
                        /* viewer is the author of post || page admin */
                        if (self::$_data['user_id'] == $post['author_id']) {
                            $comment['delete_comment'] = true;
                        }
                        /* viewer is the admin of the group of the post */
                        if ($post['in_group'] && $post['is_group_admin']) {
                            $comment['delete_comment'] = true;
                        }
                    }
                    $comments[] = $comment;
                }
            }
        }
        if(!$post)
        {
            $post['post_id']=NULL;
        }
        
        if($get_total_comments->num_rows>15)
        {
            $more_present=1;
        }
        else
        {
            $more_present=0;
        }
        
        $loadmore=0;
        if($get_total_comments_prev && $get_total_comments_prev->num_rows>0)
        {
            $loadmore=1;
        }
        if( ($commet_id == 0 &&  $order == 'prev') || $order == "prev") {
            //$comments=array_reverse($comments);
            if($target==1 && $commet_id!=0)
            {
            $ckey = array_search($commet_id, array_column($comments, 'comment_id'));
            $new_value = $comments[$ckey];
            unset($comments[$ckey]);
                array_unshift($comments, $new_value);
            }
             return ['post'=>$post,'comments'=>$comments,'more_present'=>$more_present,'load_more'=>$loadmore];
        }
        
        return ['post'=>$post,'comments'=>$comments,'more_present'=>$more_present,'load_more'=>$loadmore];
    }  

}