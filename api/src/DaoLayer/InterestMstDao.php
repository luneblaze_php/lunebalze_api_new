<?php

namespace App\DaoLayer;

use App\Utils\Codes;
use Cake\Core\Exception\Exception;
use Cake\Datasource\ConnectionManager;
use Cake\Log\Log;

class InterestMstDao
{


    /**
     * 
     */
    public function getInterestMasters($text, $offset)
    {
        Log::debug("Started ...getInterestMasters Dao");

        try{
            
            $conn = ConnectionManager::get('default');

            $sql=null;

            if($text==null)
            {
                $sql=sprintf("SELECT * FROM `interest_mst` AS im WHERE im.`status` = '1' 
                order by text asc LIMIT %s",$offset);
            }
            else 
            {
                $str = $text.'%';
                $sql=sprintf("SELECT * FROM `interest_mst` AS im WHERE im.`status` = '1' 
                AND im.`text` LIKE '%s' order by text asc LIMIT %s",$str,$offset);
            }

            Log::debug("SQL : ".$sql);

            $stmt = $conn->execute($sql);

            $results = array();

            while($result = $stmt->fetch("assoc")){
                array_push($results,$result);
            }

            Log::debug("Ended ...getInterestMasters Dao");

            return $results;
            
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }

    }



    public function reportMaster($text, $offset)
    {
        Log::debug("Started ...reportMaster Dao");

        try{
            
            $conn = ConnectionManager::get('default');
            $results = array();
            $sql= $conn->execute("select * FROM report_mst");
            while($r = $sql->fetch('assoc')){
                array_push($results , $r);
            }


            Log::debug("Ended ...reportMaster Dao");

            return $results;
            
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }

    }



    public function updateUserInterest($userId, $interests)
    {
        Log::debug("Started ...updateUserInterest Dao");

        try{
            
            $conn = ConnectionManager::get('default');
            $results = array();
            $results['status'] = 0;
            $results['msg'] = 'done';
            $conn->execute("DELETE FROM user_interest where user_id = $userId");
            foreach($interests as $i){
                if(!$conn->execute("INSERT into user_interest set user_id = $userId , interest = $i")){
                    $results['msg'] = "something went wrong $userId -> $i";return $results;
                }
            }
            $results['status'] = 1;
            return $results;


            Log::debug("Ended ...reportMaster Dao");

            return $results;
            
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }

    }


}