<?php

namespace App\DaoLayer;

use App\Utils\Codes;
use Cake\Core\Exception\Exception;
use Cake\Datasource\ConnectionManager;
use Cake\Log\Log;

class SessionsBlockedDao
{

	/**
	 * Get Sessions Blocked data using user_id and blocked_id
	 */
	public function getSessionsBlockedByUserIdAndBlockedId($userId, $blockedId)
	{
		Log::debug("Started ...getSessionsBlockedByUserIdAndBlockedId Dao : User Id : ".$userId.", Blocked Id : ".$blockedId);

		try{
			
			$conn = ConnectionManager::get('default');

			$sql=sprintf("SELECT * FROM sessions_blocked WHERE user_id = %s AND blocked_id = %s", $userId, $blockedId);
			
			Log::debug("SQL : ".$sql);

			$conn->execute($sql);

			Log::debug("Ended ...getSessionsBlockedByUserIdAndBlockedId Dao");

		}catch(\Exception $e){
			Log::debug($e);
			throw new Exception($e);
		}
	}

	/**
	 * Save Sessions Blocked
	 */
	public function saveSessionsBlocked($userId, $blockedId)
	{
		Log::debug("Started ...saveSessionsBlocked Dao : User Id : ".$userId.", Blocked Id : ".$blockedId);

		try{
			
			$conn = ConnectionManager::get('default');

			$sql=sprintf("INSERT INTO sessions_blocked (user_id, blocked_id) VALUES (%s, %s)", $userId, $blockedId);
			
			Log::debug("SQL : ".$sql);

			$conn->execute($sql);

			Log::debug("Ended ...saveSessionsBlocked Dao");

		}catch(\Exception $e){
			Log::debug($e);
			throw new Exception($e);
		}
	}

	/**
	 * Delete Sessions Blocked using user_id and blocked_id
	 */
	public function deleteSessionsBlockedByUserIdAndBlockedId($userId, $blockedId)
	{
		Log::debug("Started ...deleteSessionsBlockedByUserIdAndBlockedId Dao : User Id : ".$userId.", Blocked Id : ".$blockedId);

		try{
			
			$conn = ConnectionManager::get('default');

			$sql=sprintf("DELETE FROM sessions_blocked WHERE user_id = %s AND blocked_id = %s", $userId, $blockedId);

			Log::debug("SQL : ".$sql);

			$conn->execute($sql);

			Log::debug("Ended ...deleteSessionsBlockedByUserIdAndBlockedId Dao");

		}catch(\Exception $e){
			Log::debug($e);
			throw new Exception($e);
		}
	}
   
}