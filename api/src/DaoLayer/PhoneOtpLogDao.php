<?php

namespace App\DaoLayer;

use App\Utils\Codes;
use App\Utils\DateUtils;
use Cake\Core\Exception\Exception;
use Cake\Datasource\ConnectionManager;
use Cake\Log\Log;

class PhoneOtpLogDao
{

    /**
     * 
     */
    public function getNumberOfOtpsSentPast1Hour($phone)
    {
        Log::debug("Started ...getNumberOfOtpsSentPast1Hour Dao : Phone : ".$phone);

        try{
            
            $conn = ConnectionManager::get('default');

            $sql=sprintf("SELECT COUNT(*) as `count`
            FROM phone_otp_log where mobile_number = '%s' 
            AND 
            TIME_TO_SEC(TIMEDIFF(sms_sent_ts,CURRENT_TIMESTAMP))/3600 <= 1
            ", $phone);

            Log::debug("SQL : ".$sql);

            $stmt = $conn->execute($sql);

            $result = $stmt->fetch("assoc");

            Log::debug("Started ...getNumberOfOtpsSentPast1Hour Dao : ");

            return $result['count'];
            
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }

    }


    /**
     * 
     */
    public function savePhoneOtpSentLog($phone)
    {
        Log::debug("Started ...savePhoneOtpSentLog Dao : Phone : ".$phone);

        try{
            
            $conn = ConnectionManager::get('default');

            $dateUtils = new DateUtils;

            $sql=sprintf("INSERT INTO phone_otp_log
            (mobile_number, sent_sms, sms_sent_ts)
            VALUES('%s', 0, '%s')", $phone,
            $dateUtils->getCurrentDate());

            Log::debug("SQL : ".$sql);

            $stmt = $conn->execute($sql);

            Log::debug("Started ...savePhoneOtpSentLog Dao : ");
            
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }

    }
}