<?php

namespace App\DaoLayer;

use App\Utils\Codes;
use Cake\Core\Exception\Exception;
use Cake\Datasource\ConnectionManager;
use Cake\DaoLayer\ArticlesDao;
use Cake\DaoLayer\PostsDao;
use Cake\DaoLayer\QuizzesDao;
use Cake\DaoLayer\QuickBitsDao;
use Cake\DaoLayer\PointsDao;
use Cake\Log\Log;

class CommunityDao
{

    private function getImgFullPath($img = '', $newPath = 0){
        $codes = new Codes;
        return !empty($img) ?$codes->SYSTEM_URL.'/'.($newPath?'api/':'').$codes->UPLOADS_DIRECTORY.'/'.$img:'';  //1. solution: getting $newPath=1 gives wrong path /Luneblaze-API appears twice in code wrong path.
    }
    //adding dao 1
    public function getCommunityAdminModList($community_id)
    {
        try{
            $conn = ConnectionManager::get('default');

            Log::debug("Started ...getCommunityAdminModList Dao");
            $sql = sprintf("select * from communities_members as cmm where cmm.role = 2 or cmm.role = 1 and cmm.community_id = '%s'" , $community_id);
            Log::debug("SQL : ".$sql);
            $arr=[];
            $adminStmt = $conn->execute($sql);
            while($result = $adminStmt->fetch('assoc')){
                $sql=sprintf("SELECT cmm.*,us.user_id, us.user_fullname, us.user_email, us.user_name, us.user_firstname, us.user_lastname, us.user_gender, us.user_picture, us.user_work, us.user_work_title  FROM communities_members as cmm left join users as us on us.user_id = cmm.user_id WHERE cmm.community_id = '%s' and us.user_id = '%s'", $community_id , $result['user_id']);

                    Log::debug("SQL : ".$sql);
                    $participantsStmt = $conn->execute($sql);
                    while($res = $participantsStmt->fetch('assoc')){
                        $res['role'] = ($res['role'] == 1)? "Admin" : "Moderator" ;
                        $res['user_picture'] = $this->getImgFullPath($res['user_picture']);
                    array_push($arr, $res);
                    }

            }        
        return $arr;
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }
    //adding dao 2
    public function getBlockedCommunityUsers($community_id)
    {
        try{
            $conn = ConnectionManager::get('default');

            Log::debug("Started ...getBlockedCommunityUsers Dao");
            $sql = sprintf("select * from communities_members as cmm where cmm.role = 5 and cmm.community_id = '%s'" , $community_id);
            Log::debug("SQL : ".$sql);
            $arr=[];
            $adminStmt = $conn->execute($sql);
            while($result = $adminStmt->fetch('assoc')){
                $sql=sprintf("SELECT cmm.*,us.user_id, us.user_fullname, us.user_email, us.user_name, us.user_firstname, us.user_lastname, us.user_gender, us.user_picture, us.user_work, us.user_work_title  FROM communities_members as cmm left join users as us on us.user_id = cmm.user_id WHERE cmm.community_id = '%s' and us.user_id = '%s'", $community_id , $result['user_id']);

                    Log::debug("SQL : ".$sql);
                    $participantsStmt = $conn->execute($sql);
                    while($res = $participantsStmt->fetch('assoc')){
                        $res['role'] = ($res['role'] == 1)? "Admin" : "Moderator" ;
                        $res['user_picture'] = $this->getImgFullPath($res['user_picture']);
                    array_push($arr, $res);
                    }

            }        
        return $arr;
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }



    /**
     * Save Community
     */
    public function saveCommunity($args, $community_picture, $cover_picture)
    {

        try{

            if(!isset($args['interests'])){
                $args['interests'] = '';
            }

            $conn = ConnectionManager::get('default');

            Log::debug("Started ...saveCommunity Dao");
            $sql=sprintf("INSERT INTO `communities`(`title`, `description`, `bio`, `picture`, `picture_dimensions`, `cover_picture`, `cover_dimensions`, `user_type`, `creator_id`, `interests`) VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')", 
            $args['title'], $args['description'], $args['bio'], $community_picture['image'], $community_picture['dimensions'], @$cover_picture['image'], @$cover_picture['dimensions'], $args['user_type'], $args['creator_id'], $args['interests']);

            Log::debug("SQL : ".$sql);

            $conn->execute($sql);
            
            $sql="SELECT LAST_INSERT_ID()";            
            $stmt = $conn->execute($sql);            
            $res = $stmt->fetch();

            if($res[0]){
                $sql=sprintf("INSERT INTO `communities_members`(`community_id`, `user_id`, `role`) VALUES ('%s', '%s', '%s')", $res[0], $args['creator_id'],1);
                Log::debug("SQL : ".$sql);
                $conn->execute($sql);
            }
           
            Log::debug("Ended ...saveCommunity Dao");
            
            return $res[0];
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }    
    /**
     * Update Community
     */
    public function updateCommunity($args, $community_picture = '', $cover_picture = '')
    {
        Log::debug("Started ...updateCommunitys Dao");

        try{
            if(!isset($args['interests'])){
                $args['interests'] = '';
            }

            $conn = ConnectionManager::get('default');

            $imgQry = (@$community_picture['image'])?", picture = '".$community_picture['image']."', picture_dimensions ='".$community_picture['dimensions']."'":"";
            $imgQry .= (@$cover_picture['image'])?", cover_picture = '".$cover_picture['image']."', cover_dimensions ='".$cover_picture['dimensions']."'":"";
            $sql=sprintf("UPDATE communities SET title = '%s', description = '%s', user_type = '%s', bio = '%s', `interests` = '%s' $imgQry
                WHERE id = '%s' AND creator_id = '%s'", $args['title'], $args['description'], $args['user_type'], $args['bio'], @$args['interests'], $args['community_id'], $args['creator_id']);

            Log::debug("SQL : ".$sql);

            $conn->execute($sql);
           
            Log::debug("Ended ...updateCommunitys Dao");
            
            return 1;
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }
    /**
     * Delete Community
     */
    public function deleteCommunity($community_id, $creator_id)
    {
        Log::debug("Started ...deleteCommunitys Dao");

        try{

            $conn = ConnectionManager::get('default');

            //deleting questions
            $sql=sprintf("UPDATE communities SET deleted = 1 WHERE id = '%s' AND creator_id = '%s'", 
            $community_id, $creator_id);
            Log::debug("SQL : ".$sql);
            $conn->execute($sql);
           
            Log::debug("Ended ...deleteCommunitys Dao");
            
            return 1;
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }
    /**
     * Get Communitys
     */
    public function getCommunityList($args, $date)
    {
        Log::debug("Started ...getCommunityList Dao");
        $codes = new Codes;

        try{

            $conn = ConnectionManager::get('default');
            $cond = "";
            $having = "";
            //checking filter params
            if (isset($args['my_role']) && !empty($args['my_role'])) { //no 'my_role' arguement in args
                //options: 
                //Member/Admin/Moderator
                if($args['my_role'] =='Member'){
                    $having = " cmm.role = 3";
                }
                elseif($args['my_role'] =='Admin'){
                    $having = " cmm.role = 1";
                }
                elseif($args['my_role'] =='Moderator'){
                    $having = " cmm.role = 2";
                }

            }
            if (isset($args['interests']) && !empty($args['interests'])) {
                //options: 
                // 1. My interests
                // 2. Custom list of interest Ids
                $interestCond = '';
                if($args['interests'] == 'My interests'){
                    
                    $sql=sprintf("SELECT * FROM user_interest where user_id= '%s'", $args['user_id']);
                    Log::debug("SQL : ".$sql);
                    $stmt = $conn->execute($sql);
                    $key = 0;
                    while($result = $stmt->fetch("assoc")) {
                        $interestCond .= ($key?' OR':'')." FIND_IN_SET('".$result['interest']."', cm.interests)";
                        $key++;
                    }
                }
                else if(is_array($args['interests'])){
                    $interestCond = '';
                    foreach ($args['interests'] as $key => $interest) {
                        $interestCond .= ($key?' OR':'')." FIND_IN_SET('".$interest."', cm.interests) > 0";
                    }
                }
                // condition according to intrest
                $cond .= ($interestCond)?" AND (".$interestCond.")":'';

            }
            // condition for query text
            if (isset($args['query_text']) && !empty($args['query_text'])) {
                $cond .= " AND cm.title like '%".$args['query_text']."%'";
            }

            //checking keys for pagination
            $args['page'] = (isset($args['page']) && $args['page'] > 1)?$args['page']:1;
            $args['pageSize'] = (isset($args['pageSize']) && $args['pageSize'] > 0)?$args['pageSize']:10;

            $cond .= ($having)?" HAVING ( ". $having." ) ":'';
            $cond .= " order by id desc limit ".(($args['page']- 1) * $args['pageSize']).", ".$args['pageSize'];

            //community query
            $sql="SELECT cm.*, us.user_fullname, cmm.role
                FROM  `communities` as cm left join users as us on us.user_id = cm.creator_id left join communities_members as cmm on (cmm.community_id = cm.id AND cmm.user_id = $args[user_id]) WHERE  deleted = 0 $cond"; //2. 3. add arguement for joined the community or not and join friends who have joined the community
            Log::debug("SQL : ".$sql);
            // echo $sql; exit;
            $stmt = $conn->execute($sql);
            
            $responseData = array();

            while($result = $stmt->fetch("assoc")) {
                $result['picture'] = $this->getImgFullPath($result['picture'],1);
                $result['cover_picture'] = $this->getImgFullPath($result['cover_picture'],1);
                $result['role'] = ($result['role'] == 1)?'Admin':(($result['role'] == 2) ? 'Moderator':(($result['role'] == 3)?'Member':(($result['role']==4)?'Deleted':'Blocked')));
                $result['communityInterestArr'] = [];

                //i_join
                
                $result['i_join'] =0;
                
                $sql = sprintf("SELECT us.user_id  FROM communities_members as cmm left join users as us on us.user_id = cmm.user_id WHERE cmm.community_id = '%s' and us.user_id= '%s'" , $result['id'] , $args['user_id']);
                Log::debug("SQL : ".$sql);
                $ijoinStmt = $conn->execute($sql);
                if($res = $ijoinStmt->fetch("assoc")){
                    $result['i_join'] =1;
                }

                //friend count and ones data
                $arr = [];
                $sql = sprintf("select * from communities_members where community_id = '%s' " , $result['id'] );
                Log::debug("SQL : ".$sql);
                $cmmStmt = $conn->execute($sql);
                while($res = $cmmStmt->fetch('assoc')){
                    array_push($arr, $res['user_id']);
                }



                $result['friendCount'] = 0;
                $sql = sprintf("select * from friends as f where (f.user_one_id = '%s' )or ( f.user_two_id= '%s')", $args['user_id'], $args['user_id']);
                Log::debug("SQL : ".$sql);
                $friendsStmt = $conn->execute($sql);
                $var_id =0;
                while($res = $friendsStmt->fetch("assoc")){
                    if($res["user_one_id"] == $args['user_id']){
                        if(in_array($res["user_two_id"], $arr)){
                            $var_id = $res["user_two_id"];
                            $result["friendCount"] = $result["friendCount"] + 1;
                        }
                    }else{
                        if(in_array($res["user_one_id"], $arr)){
                            $var_id = $res["user_one_id"];
                            $result["friendCount"] = $result["friendCount"] + 1;   
                        }
                    }
                }

                $i = $var_id;
                if($i != 0){
                    
                        $sql=sprintf("SELECT cmm.*, us.user_fullname, us.user_email, us.user_name, us.user_firstname, us.user_lastname, us.user_gender, us.user_picture, us.user_work, us.user_work_title  FROM communities_members as cmm left join users as us on us.user_id = cmm.user_id WHERE cmm.community_id = '%s' and us.user_id = '%s' and cmm.role != 4 and cmm.role != 5", $result["id"] , $i);

                    Log::debug("SQL : ".$sql);
                    $participantsStmt = $conn->execute($sql);

                    if($res = $participantsStmt->fetch("assoc")){             //2. member data array feching code How to remove?
                        $res['user_picture'] = $this->getImgFullPath($res['user_picture']);//1. contd. resolved once path rectified in first function
                        //4. solution : variable on loop inceremnt can be made inplying number of users or followers
                        $res['role'] = ($res['role'] == 1)?'Admin':($res['role'] == 2 ? 'Moderator':($res['role']== 3 ? 'Member' :($res['role']==4 ? 'Deleted':'Blocked')    ));
                        $result['friendData'] = $res ;
                        
                    }
                }
                else{
                    $result['friendData'] = null;
                }







                // checking creater info
                $sql=sprintf("SELECT user_id, user_email, user_name, user_fullname, user_firstname, user_lastname, user_gender, user_picture, user_work, user_work_title , user_work_place , '' as connection FROM users WHERE user_id = '%s' limit 0, 1", $result['creator_id']);

                Log::debug("SQL : ".$sql);
                $createrStmt = $conn->execute($sql);
                $result['created_by'] = $createrStmt->fetch("assoc");
                $result['created_by'] = $result['created_by']?$result['created_by']:[];
                //setting user image
                $result['created_by']['user_picture'] = $this->getImgFullPath(@$result['created_by']['user_picture']);

                //setting interests
                if($result['interests'] != ""){

                    // checking interests
                    $sql=sprintf("SELECT interest_id, `text` as interest_name FROM interest_mst WHERE FIND_IN_SET(interest_id, '%s')", $result['interests']);

                    Log::debug("SQL : ".$sql);
                    $interestStmt = $conn->execute($sql);

                    while($interestResult = $interestStmt->fetch("assoc"))
                        array_push($result['communityInterestArr'], $interestResult);

                }
                
                array_push($responseData,$result);
            }
           
            Log::debug("Ended ...getCommunityList Dao");

            return $responseData;
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }

    public function getCommunityByUserAndId($community_id, $user_id, $date)
    {
        Log::debug("Started ...getCommunityByUserAndId Dao");
        $codes = new Codes;

        try{

            $conn = ConnectionManager::get('default');
            $join_cond = " AND dp.user_id = '".$user_id."'";
            //getting community details
            $sql=sprintf("SELECT cm.*, us.user_fullname, cmm.role
                FROM  `communities` as cm left join users as us on us.user_id = cm.creator_id left join communities_members as cmm on (cmm.community_id = cm.id AND cmm.user_id = $user_id) WHERE cm.id = '%s' limit 0, 1", $community_id);

            //5. SOLIUTION : i_join can be made here by comparing community id and community memberslist.
            
            Log::debug("SQL : ".$sql);
            // echo $sql; exit;
            $communityStmt = $conn->execute($sql);
            $communityData = $communityStmt->fetch("assoc");
            if($communityData){
                //checking community is deleted
                if($communityData['deleted'] == 1)
                    return ['deleted'=>1];

                $communityData['picture'] = $this->getImgFullPath($communityData['picture'],1);
                $communityData['cover_picture'] = $this->getImgFullPath($communityData['cover_picture'],1);
                //$communityData['role'] = ($communityData['role'] == 1)?'Admin':($communityData['role'] == 2 ? 'Moderator':($communityData['role']== 3 ? 'Member':($communityData['role']==4 ? 'Deleted' : 'Blocked')    ));
                //$communityData['memberData'] = [];
                $communityData['communityInterestArr'] = [];
                $communityData['created_by'] = [];
                //$communityData['i_join']=0;
                // checking creater info
                $sql=sprintf("SELECT user_id, user_email, user_name, user_fullname, user_firstname, user_lastname, user_gender, user_picture, user_work, user_work_title , user_work_place , '' as connection FROM users WHERE user_id = '%s' limit 0, 1", $communityData['creator_id']);

                Log::debug("SQL : ".$sql);
                $createrStmt = $conn->execute($sql);
                $communityData['created_by'] = $createrStmt->fetch("assoc");
                $communityData['created_by'] = $communityData['created_by']?$communityData['created_by']:[];
                //setting user image
                $communityData['created_by']['user_picture'] = $this->getImgFullPath(@$communityData['created_by']['user_picture']);

                //setting interests
                if($communityData['interests']){ //column cm.interests empty so no data fetching, changes already made in createcommunity.json

                    // checking interests
                    $sql=sprintf("SELECT interest_id, `text` as interest_name FROM interest_mst WHERE FIND_IN_SET(interest_id, '%s')", $communityData['interests']);

                    Log::debug("SQL : ".$sql);
                    $interestStmt = $conn->execute($sql);

                    while($result = $interestStmt->fetch("assoc"))
                        array_push($communityData['communityInterestArr'], $result);

                }
                $sql = sprintf("SELECT cmm.*  FROM communities_members as cmm left join users as us on us.user_id = cmm.user_id WHERE cmm.community_id = '%s' and us.user_id= '%s' " , $community_id , $user_id);
                Log::debug("SQL : ".$sql);
                $ijoinStmt = $conn->execute($sql);
                if($result = $ijoinStmt->fetch('assoc')){
                    $communityData['role'] = $result['role'];
                }
                if($communityData['role'] == null){
                    $communityData['role'] = '4';
                }
                $arr = [];
                $communityData["followersCount"] = 0;
                // checking attempted user and top scorer
                $sql=sprintf("SELECT cmm.*, us.user_fullname, us.user_email, us.user_name, us.user_firstname, us.user_lastname, us.user_gender, us.user_picture, us.user_work, us.user_work_title  FROM communities_members as cmm left join users as us on us.user_id = cmm.user_id WHERE cmm.community_id = '%s' and cmm.role != 4 and cmm.role != 5", $community_id);

                Log::debug("SQL : ".$sql);
                $participantsStmt = $conn->execute($sql);

                while($result = $participantsStmt->fetch("assoc")){             //2. member data array feching code How to remove?
                    $result['user_picture'] = $this->getImgFullPath($result['user_picture']);//1. contd. resolved once path rectified in first function
                    //4. solution : variable on loop inceremnt can be made inplying number of users or followers
                    array_push($arr , $result['user_id']);
                    $communityData["followersCount"] = $communityData["followersCount"] +1;
                    //$result['role'] = ($result['role'] == 1)?'Admin':($result['role'] == 2 ? 'Moderator':'Member');
                    //array_push($communityData['memberData'], $result);
                }
                $communityData['friendCount'] = 0;
                $sql = sprintf("select * from friends as f where (f.user_one_id = '%s' )or ( f.user_two_id= '%s')", $user_id, $user_id);
                Log::debug("SQL : ".$sql);
                $friendsStmt = $conn->execute($sql);
                $varid =0;
                while($result = $friendsStmt->fetch("assoc")){
                    if($result["user_one_id"] == $user_id){
                        if(in_array($result["user_two_id"], $arr)){
                            $varid = $result["user_two_id"];
                            $communityData["friendCount"] = $communityData["friendCount"] + 1;
                        }
                    }else{
                        if(in_array($result["user_one_id"], $arr)){
                            $varid = $result["user_one_id"];
                            $communityData["friendCount"] = $communityData["friendCount"] + 1;   
                        }
                    }
                }


                //data of a friend
                //$communityData["friendData"] = [];
                $i = $varid;
                if($i != 0){
                    
                        $sql=sprintf("SELECT cmm.*, us.user_fullname, us.user_email, us.user_name, us.user_firstname, us.user_lastname, us.user_gender, us.user_picture, us.user_work, us.user_work_title  FROM communities_members as cmm left join users as us on us.user_id = cmm.user_id WHERE cmm.community_id = '%s' and us.user_id = '%s' and cmm.role != 4 and cmm.role != 5", $community_id , $i);

                    Log::debug("SQL : ".$sql);
                    $participantsStmt = $conn->execute($sql);

                    $result = $participantsStmt->fetch("assoc");             //2. member data array feching code How to remove?
                        $result['user_picture'] = $this->getImgFullPath($result['user_picture']);//1. contd. resolved once path rectified in first function
                        //4. solution : variable on loop inceremnt can be made inplying number of users or followers
                        $result['role'] = ($result['role'] == 1)?'Admin':($result['role'] == 2 ? 'Moderator':'Member');
                        $communityData['friendData'] = $result ;
                        
                    
                }
                
            }

            Log::debug("Ended ...getCommunityByUserAndId Dao");

            return $communityData;
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }
    
    //updating user community participation
    public function addRemoveCommunityParticipation($args, $date)
    {

        Log::debug("Started ...addRemoveCommunityParticipation Dao");
        try{
            $conn = ConnectionManager::get('default');
             $response = array("valid" => false, "msg"=>"Community not found.");

            //checking community
            $sql=sprintf("SELECT * FROM communities WHERE id = '%s'", $args['community_id']);
            Log::debug("SQL : ".$sql);
            $stmt = $conn->execute($sql);
            $communityData = $stmt->fetch("assoc");

            if($communityData){
                //checking community already participant
                $sql=sprintf("SELECT * FROM communities_members WHERE community_id = '%s' AND user_id = '%s' and role != 5", $args['community_id'], $args['user_id']);
                Log::debug("SQL : ".$sql);
                $stmt = $conn->execute($sql);
                $participantData = $stmt->fetch("assoc");

                if($args['status'] == 2){
                    if(!empty($participantData)){
                        //deleting participation of user
                        $sql=sprintf("UPDATE `communities_members` set role = 4 WHERE community_id = '%s' AND user_id = '%s'", $args['community_id'], $args['user_id']);
                        Log::debug("SQL : ".$sql);
                        if($conn->execute($sql)){
                            $response = array("valid" => true, "msg"=>"Participation removed");
                        }else
                            $response["msg"] = "Something went wrong.";
                    }else
                        $response["msg"] = "Not participated.";
                    
                }else if($args['status'] == 1){
                    if(empty($participantData)){
                        //inserting participation of user
                        $sql=sprintf("INSERT INTO `communities_members`(`community_id`, `user_id`) VALUES ('%s', '%s')", $args['community_id'], $args['user_id']);
                        Log::debug("SQL : ".$sql);
                        if($conn->execute($sql)){
                            $response = array("valid" => true, "msg"=>"Participated successfully");
                        }else
                            $response["msg"] = "Something went wrong.";
                    }else{
                        if($participantData['role']==4){
                            $sql=sprintf("UPDATE `communities_members` set role = 3 WHERE community_id = '%s' AND user_id = '%s'", $args['community_id'], $args['user_id']);
                        Log::debug("SQL : ".$sql);
                        if($conn->execute($sql)){
                            $response = array("valid" => true, "msg"=>"Participation re-added");
                        }else
                            $response["msg"] = "Something went wrong.";



                        }
                        else{
                        $response["msg"] = "Already participated.";
                    }
                }

                    
                }
            }

           
            Log::debug("Ended ...addRemoveCommunityParticipation Dao");
            
            return $response;
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }
    
    //updating user community participation role
    public function promoteDemoteCommunityUser($args, $date)
    {

        Log::debug("Started ...promoteDemoteCommunityUser Dao");
        try{
            $conn = ConnectionManager::get('default');
             $response = array("valid" => false, "msg"=>"You do not have authority.");

            //checking community
            $sql=sprintf("SELECT cm.*, us.user_fullname, cmm.role
                FROM  `communities` as cm left join users as us on us.user_id = cm.creator_id left join communities_members as cmm on (cmm.community_id = cm.id AND cmm.user_id = $args[user_id]) WHERE  deleted = 0 and id ='%s' limit 0, 1", $args['community_id']);
            Log::debug("SQL : ".$sql);
            $stmt = $conn->execute($sql);
            $communityData = $stmt->fetch("assoc");

            if($communityData && $communityData['role'] == 1 || $communityData['role'] == 1){

                //checking community already participant
                $sql=sprintf("SELECT * FROM communities_members WHERE community_id = '%s' AND user_id = '%s'", $args['community_id'], $args['member_id']);
                Log::debug("SQL : ".$sql);
                $stmt = $conn->execute($sql);
                $participantData = $stmt->fetch("assoc");

                if(!empty($participantData)){
                        if($participantData['role'] == 1){$response["msg"] = "you dont have the authority";return $response;}
                        //inserting participation role of user
                        $sql=sprintf("UPDATE communities_members SET role = '%s'  WHERE community_id = '%s' AND user_id = '%s'",$args['status'], $args['community_id'], $args['member_id']);
                        Log::debug("SQL : ".$sql);
                        if($conn->execute($sql)){
                            $response = array("valid" => true, "msg"=>"Updated successfully");
                        }else
                            $response["msg"] = "Something went wrong.";
                    
                }else{
                    $sql=sprintf("INSERT INTO `communities_members`(`community_id`, `user_id`, role) VALUES ('%s', '%s' ,'5')", $args['community_id'], $args['user_id'] );
                    Log::debug("SQL : ".$sql);
                    if($conn->execute($sql)){
                        $response = array("valid" => true, "msg"=>"followed and locked");
                    }else{$response["msg"] = "Something went wrong.";}
                }
                Log::debug("SQL : ".$sql);
                //$conn->execute($sql);
            }

           
            Log::debug("Ended ...promoteDemoteCommunityUser Dao");
            
            return $response;
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }


    //sending invitaion for community
    public function sendCommunityInvitaion($args, $date)
    {

        Log::debug("Started ...sendCommunityInvitaion Dao");
        $response = array("valid" => false, "msg"=>"This community is not open of invitation.");
        try{
            $conn = ConnectionManager::get('default');

            //checking community live duration
            // AND DATE_ADD(start_time, INTERVAL `live_duration` second) >= '%s'
            $sql=sprintf("SELECT * FROM communities WHERE deleted = 0 AND id = '%s'", $args['community_id']);
            Log::debug("SQL : ".$sql);
            $stmt = $conn->execute($sql);
            $communityData = $stmt->fetch("assoc");

            //checking community invitations
            $sql=sprintf("SELECT * FROM communities_invitations WHERE community_id = '%s' AND user_id = '%s' AND sender_user_id = '%s'", $args['community_id'], $args['user_id'], $args['sender_user_id']);
            Log::debug("SQL : ".$sql);
            $stmt = $conn->execute($sql);
            $invitationData = $stmt->fetch("assoc");

            if(!empty($communityData)){

                if(empty($invitationData)){               
                    //inserting invitation of user
                    $sql=sprintf("INSERT INTO `communities_invitations`(`community_id`, `user_id`, `sender_user_id`, `created`) VALUES ('%s', '%s','%s', '%s')", $args['community_id'], $args['user_id'], $args['sender_user_id'], $date);
                    Log::debug("SQL : ".$sql);

                    if($conn->execute($sql)){   
                        $response = array("valid" => true, "msg"=>"Updated");
                    }else
                        $response["msg"] = "Something went wrong.";              
                }else
                    $response["msg"] = "Already invited";
            }
           
            Log::debug("Ended ...sendCommunityInvitaion Dao");
            
            return $response;
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }

    //checking invitation for community
    public function getCommunityInvitedUserList($args)
    {

        Log::debug("Started ...getCommunityInvitedUserList Dao");
        try{
            $conn = ConnectionManager::get('default');
            $codes = new Codes;

            //checking keys for pagination
            $args['page'] = (isset($args['page']) && $args['page'] > 1)?$args['page']:1;
            $args['pageSize'] = (isset($args['pageSize']) && $args['pageSize'] > 0)?$args['pageSize']:10;

            //checking community invitations
            $cond = (isset($args['search_keyword']) && !empty($args['search_keyword']))?" AND (users.user_fullname LIKE '%".$args['search_keyword']."%' OR users.user_name LIKE '%".$args['search_keyword']."%')":"";
            $cond .= " limit ".(($args['page']- 1) * $args['pageSize']).", ".$args['pageSize'];

            $followings_ids = [];
            $friends_ids = [];
            $friend_requests_ids = [];
            $friend_requests_sent_ids = [];
            $fellowsArr = [];

            //checking community invitations users
            $sql=sprintf("SELECT following_id FROM followings WHERE user_id = '%s'", $args['user_id']);
            Log::debug("SQL : ".$sql);
            $followingsStmt = $conn->execute($sql);

            while($result = $followingsStmt->fetch("assoc")) {
                $followings_ids[] = $result['following_id'];
            }
            //checking community invitations users
            $sql=sprintf('SELECT users.user_id FROM friends INNER JOIN users ON (friends.user_one_id = users.user_id AND friends.user_one_id != %1$s) OR (friends.user_two_id = users.user_id AND friends.user_two_id != %1$s) WHERE status = 1 AND (user_one_id = %1$s OR user_two_id = %1$s)', $args['user_id']);
            Log::debug("SQL : ".$sql);
            $friendsStmt = $conn->execute($sql);

            while($result = $friendsStmt->fetch("assoc")) {
                $friends_ids[] = $result['user_id'];
            }
            //checking community invitations users
            $sql=sprintf('SELECT user_one_id FROM friends WHERE status = 0 AND user_two_id = %s', $args['user_id']);
            Log::debug("SQL : ".$sql);
            $friend_requestsStmt = $conn->execute($sql);

            while($result = $friend_requestsStmt->fetch("assoc")) {
                $friend_requests_ids[] = $result['user_one_id'];
            }

            //checking community friend_requests_sent users
            $sql=sprintf('SELECT user_one_id FROM friends WHERE status = 0 AND user_two_id = %s', $args['user_id']);
            Log::debug("SQL : ".$sql);
            $friend_requests_sentStmt = $conn->execute($sql);

            while($result = $friend_requests_sentStmt->fetch("assoc")) {
                $friend_requests_sent_ids[] = $result['user_one_id'];
            }

            $sql="SELECT users.user_id, users.user_work, users.user_work_title, users.user_work_place, users.user_name, users.user_fullname, users.user_firstname, user_lastname, users.user_gender, users.user_picture, cmm.role FROM friends INNER JOIN users ON ((friends.user_one_id = users.user_id AND friends.user_one_id != $args[user_id]) OR (friends.user_two_id = users.user_id AND friends.user_two_id != $args[user_id])) left join communities_members as cmm on (cmm.community_id = $args[community_id] AND cmm.user_id = $args[user_id]) WHERE friends.status = 1 AND (user_one_id = $args[user_id] OR user_two_id = $args[user_id]) $cond";
            
            Log::debug("SQL : ".$sql);
            // echo $sql; exit;
            $fellowsStmt = $conn->execute($sql);

            while($result = $fellowsStmt->fetch("assoc")) {
                $result['user_picture'] = $this->getImgFullPath($result['user_picture']);

                $result['connection'] = $this->connection($args['user_id'], $result['user_id'], $followings_ids, $friends_ids, $friend_requests_ids, $friend_requests_sent_ids);
                $result['is_friend'] = in_array($args['user_id'], $friends_ids)?1:0;
                $result['is_following'] = in_array($args['user_id'], $followings_ids)?1:0;
                $result['role'] = ($result['role'] == 1)?'Admin':($result['role'] == 2 ? 'Moderator':'Member');
                $fellowsArr[] = $result;
            }

            Log::debug("Ended ...getCommunityInvitedUserList Dao");
            
            return $fellowsArr;
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }
    
    private function connection($logged_in_user_id, $user_id, $followings_ids = [], $friends_ids = [], $friend_requests_ids = [], $friend_requests_sent_ids = [])
    {
        /* check if the viewer is the target */
        if ($user_id == $logged_in_user_id) {
            return "me";
        }
        // Arnab mohanty:: on 2nd Jan 2019
        if (in_array($user_id, $followings_ids)) {
            /* the viewer follow the target */
            return "followed";
        }
        /* check if the viewer & the target are friends */
        if (in_array($user_id, $friends_ids)) {
            return "remove";
        }
        /* check if the target sent a request to the viewer */
        if (in_array($user_id, $friend_requests_ids)) {
            return "request";
        }
        /* check if the viewer sent a request to the target */
        if (in_array($user_id, $friend_requests_sent_ids)) {
            return "cancel";
        }
        
        
        /* there is no relation between the viewer & the target */
        return "add";
        
    }


    //checking post requests for community
    public function getPostsRequestsForCommunity($args)
    {

        Log::debug("Started ...getPostsRequestsForCommunity Dao");
        try{
            $conn = ConnectionManager::get('default');
            $codes = new Codes;

            //checking keys for pagination
            $args['page'] = (isset($args['page']) && $args['page'] > 1)?$args['page']:1;
            $args['pageSize'] = (isset($args['pageSize']) && $args['pageSize'] > 0)?$args['pageSize']:10;

            //checking community invitations
            $cond = (isset($args['search_keyword']) && !empty($args['search_keyword']))?" AND (post_title LIKE '%".$args['search_keyword']."%')":"";
            $cond .= "  order by post_id limit ".(($args['page']- 1) * $args['pageSize']).", ".$args['pageSize'];

            $sql=sprintf("SELECT cm.*, us.user_fullname, cmm.role
                FROM  `communities` as cm left join users as us on us.user_id = cm.creator_id left join communities_members as cmm on (cmm.community_id = cm.id AND cmm.user_id = $args[user_id]) WHERE  deleted = 0 and id ='%s' limit 0, 1", $args['community_id']);
            Log::debug("SQL : ".$sql);
            $stmt = $conn->execute($sql);
            $communityData = $stmt->fetch("assoc");

            if(empty($communityData) || $communityData['role'] != 1)
                return [];

            $sql=sprintf("SELECT * FROM `posts` WHERE `community_id`='%s' and user_type='community' and is_approved_by_admin='0' $cond",$args['community_id']);

            Log::debug("SQL : ".$sql);

            $stmt = $conn->execute($sql);

            $results = array();

            while($result = $stmt->fetch("assoc"))
                array_push($results,$result);

            Log::debug("Ended ...getPostsRequestsForCommunity Dao");

            return $results;

        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }


    //giving points
    public function grantPoints($type , $contentId ){
        try{
            $art = new ArticlesDao;
            $pd = new PostsDao;
            $dd = new DebatesDao;
            $qb = new QuickBitsDao;
            $q = new QuizzesDao;
            switch($type){
                case 'articles':
                    $ret = $art->articlePoints('by_content' , 0 , $contentId);
                    if($ret == 0){throw new Exception("not enough points.");}
                    break;
                case 'polls':
                    $ret = $pd->pollPoints('by_content' , 0 , $contentId);
                    if($ret == 0){throw new Exception("not enough points.");}
                    break;
                case 'quickbits':
                    $ret = $qb->quickbitPoints('by_content' , 0 , $contentId);
                    if($ret == 0){throw new Exception("not enough points.");}
                    break;
                case 'debates':
                    $ret = $dd->giveDebateCreationPts($contentId);
                    if($ret == 0){throw new Exception("not enough points.");}
                    break;
                case 'quizzes':
                    $ret = $q->takeQuizpoints($contentId);
                    if($ret == 0){throw new Exception("not enough points.");}
                    break;
                default:
                    return null;
            }
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }

    //updating user community participation role
    public function approveOrDeclinePostsRequestsForCommunity($args, $date)
    {

        Log::debug("Started ...approveOrDeclinePostsRequestsForCommunity Dao");
        try{
            $conn = ConnectionManager::get('default');
            $response = array("valid" => false, "msg"=>"You do not have authority.");

            //checking community
            $sql=sprintf("select * from communities_members where community_id = %s and user_id = %s",$args["community_id"], $args['user_id'] );
            Log::debug("SQL : ".$sql);
            $stmt = $conn->execute($sql);
            $communityData = $stmt->fetch("assoc");
            extract($args);
            $check = array("article"  , "debate", "quiz" , "quickbit"  , "poll" , "session" );
            if(!in_array($posttype, $check)){return array("valid" => false, "msg"=>"posttype doesnt track");}
            $approval = 0;
            if($args['status'] == 1){
                $approval = $user_id;
            }else{
                $approval = (-1) * $user_id;
            }
            $tablename = array("article" => "articles" , "debate" => "debates", "quiz" => "quizzes", "quickbit" => "quickbits" , "session" => "sessions", "poll" => "polls");
            $idname = array("article" => "articles_id" , "debate" => "id", "quiz" => "id", "quickbit" => "id", "session" => "sessions_id" , "poll" => "id");
            $ptname = array("article" => "articles_count" , "debate" => "debate_count", "quiz" => "quiz_count", "quickbit" => "quickbit_count" , "session" => "session_count" , "poll" => "poll_count");
            $postname = array("article" => "article_id" , "debate" => "debate_id", "quiz" => "quiz_id", "quickbit" => "quickbit_id" , "session" => "session_id" , "poll" => "poll_id");
            $typename = array("article" => "article" , "debate" => "debate", "quiz" => "quiz", "quickbit" => "quickbit" , "poll" => "poll" ,"session" => "session");
            //return array("valid" => false, "msg"=>'here');
            if($communityData && ($communityData['role'] == 1 || $communityData['role'] == 2)){

                if($posttype != ''){

                    $sql = sprintf("select * from $tablename[$posttype] where $idname[$posttype] = %s ", $content_id);
                    $st = $conn->execute($sql);
                    if(!$a = $st->fetch("assoc")){return array("valid" => false, "msg"=>'no such post found');}
                    if($a['approved_by'] != 0){
                        if($a['approved_by'] > 0){
                            $status = $this->tell($community_id , $posttype);
                            return array("valid" => false, "msg"=>"Already Approved", "temp" => $status);
                        }else{
                            $status = $this->tell($community_id , $posttype);
                            return array("valid" => false, "msg"=>"Already Declined", "temp" => $status);
                        }
                    }
                    $sql = sprintf("UPDATE $tablename[$posttype] set approved_by = %s where $idname[$posttype] = %s ",$approval, $content_id);

                    if($conn->execute($sql) && $approval > 0 ){
                        $sql = $conn->execute(sprintf("UPDATE posts set is_approved_by_admin = %s where $postname[$posttype] = %s ",$approval, $content_id));
                        $sql = sprintf("UPDATE communities SET $ptname[$posttype] = $ptname[$posttype] + 1 where id = %s", $community_id);
                        if($conn->execute($sql)){
                            $this->grantPoints($tablename[$posttype] , $content_id);
                            $response = array("valid" => true, "msg"=>"approved");
                        }else{$response = array("valid" => false, "msg"=>"something went wrong");}
                    }else{
                        $msg = "something went wrong";
                        if($approval < 0 ){
                            $conn->execute(sprintf("UPDATE posts set is_approved_by_admin = %s where $postname[$posttype] = %s ",$approval, $content_id));
                            $msg = "Declined";
                        }
                        $response = array("valid" => false, "msg"=>$msg);
                    }

                }else{
                    $response = array("valid" => false, "msg"=>"somethin is wrong");
                }
            }
            //$arr = array('articles_remaining' , 'debates_remaining', 'quiz_remaining', 'quickbits_remaining' , 'polls_remaining');
            $typename = array("article" => "article" , "debate" => "debate", "quiz" => "quiz", "quickbit" => "quickbit" , "poll" => "poll" , "session" => "session");
            $arr = array("article" => "articles_remaining" , "debate" => "debates_remaining", "quiz" => "quiz_remaining", "quickbit" => "quickbits_remaining" , "poll" => "polls_remaining" ,"session" => "sessions_remaining");
            //if(!isset($typename[$posttype])){return $response;}
            $sql = sprintf("SELECT * FROM posts where community_id = %s and is_approved_by_admin = 0 and post_type = '%s' limit 0,1", $community_id , $typename[$posttype]);
            $st = $conn->execute($sql);
            if($st->count() > 0){
                $response[$arr[$posttype]] = true;

            }else{
                $response[$arr[$posttype]] = false;
            }
            $response['temp'] = $response[$arr[$posttype]];

           
            Log::debug("Ended ...approveOrDeclinePostsRequestsForCommunity Dao");
            
            return $response;
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }

    public function tell($community_id , $posttype){
        $conn = ConnectionManager::get("default");
        $typename = array("article" => "article" , "debate" => "debate", "quiz" => "quiz", "quickbit" => "quickbit" , "poll" => "poll" , "session" => "session");
        $arr = array("article" => "articles_remaining" , "debate" => "debates_remaining", "quiz" => "quiz_remaining", "quickbit" => "quickbits_remaining" , "poll" => "polls_remaining" ,"session" => "sessions_remaining");
        //if(!isset($typename[$posttype])){return $response;}
        $sql = sprintf("SELECT * FROM posts where community_id = %s and is_approved_by_admin = 0 and post_type = '%s' and user_type = 'community' limit 0,1", $community_id , $typename[$posttype]);
        $st = $conn->execute($sql);
        if($st->count() > 0){
            $response = true;

        }else{
            $response = false;
        }
        return $response;
    }


    public function userData($uid , $cid , $user_type , $communityId){
        try{
            $conn = ConnectionManager::get('default');
            $arr = array();
            if($user_type == 'organisation' || $user_type == 'organization' || $user_type == 'Organisation' || $user_type == 'Organization'){
                $sql = $conn->execute(sprintf("SELECT * FROM `organization` where id = %s", $cid));
                if($res = $sql->fetch('assoc')){
                    //return ["message"=>'organisation not found'];
                }else{return ["message"=>'organisation not found'];}
                if($res['Logo'] != '' && $res['Logo'] != null){$res['Logo'] = $this->getImgFullPath($res['Logo'] ,1);}
                if($res['cover_photo'] != '' && $res['cover_photo'] != null){$res['cover_photo'] = $this->getImgFullPath($res['cover_photo'] ,1);}
                $res["i_follow"] = 0;
                $sql = $conn->execute(sprintf("SELECT * FROM `users_organization` where user_id = %s and organization_id = %s",$uid, $cid));
                if(!$re = $sql->fetch('assoc')){$res['i_follow'] = 1;}
                array_push($arr, $res);
            }elseif($user_type == 'venue' || $user_type == 'Venue' || $user_type == 'institution' || $user_type == 'Institution') {
                $sql = $conn->execute(sprintf("SELECT * FROM `venue` where venue_id = %s", $cid));
                if(!$res = $sql->fetch('assoc')){return ["message"=>'organisation not found'];}
                if($res['cover_photo'] != '' && $res['cover_photo'] != null){$res['cover_photo'] = $this->getImgFullPath($res['cover_photo'] ,1);}
                $res['logo'] = $res['cover_photo'];
                $res['cover_photo'] = '';
                $res["i_follow"] = 0;
                $sql = $conn->execute(sprintf("SELECT * FROM `users_venue` where user_id = %s and venue_id = %s",$uid, $cid));
                if(!$re = $sql->fetch('assoc')){$res['i_follow'] = 1;}
                array_push($arr, $res);
            }elseif($user_type == 'user' || $user_type == 'User' ){
                $sql = sprintf("SELECT user_id, user_email, user_fullname, user_firstname, user_lastname, user_gender, user_picture, user_work, user_work_title, user_work_place from users where user_id = %s", $cid);
                    Log::debug("SQL : ".$sql);
                    $s = $conn->execute($sql);
                    if($r=$s->fetch('assoc')){
                        $r['user_picture'] = $this->getImgFullPath($r['user_picture'] ,1);
                        //connection variable add here
                        $r['i_follow'] = 0;
                        $f = $conn->execute(sprintf("SELECT * from `followings` where user_id = %s and  following_id = %s" , $uid , $cid));
                        if($a = $f->fetch("assoc")){$r['i_follow'] = 1; }
                        array_push($arr, $r);
                    }
            }elseif($user_type == 'community' || $user_type == 'Community'){
                $com = $conn->execute(sprintf("SELECT * FROM `communities` where id = %s"  , $communityId));
                if($r = $com->fetch("assoc")){
                    if($r['picture'] != null and $r['picture'] != ''){$r['picture'] = $this->getImgFullPath($r['picture'] , 1);}
                    if($r['cover_picture']!= null and $r['cover_picture'] != ''){$r['cover_picture'] = $this->getImgFullPath($r['cover_picture'] , 1);}
                    //$result['community_data'] = $r;
                    $r['i_follow'] = 0;
                    $f = $conn->execute(sprintf("SELECT * from `communities_members` where user_id = %s and  community_id = %s" , $uid , $cid));
                    if($a = $f->fetch("assoc")){$r['i_follow'] = $a['role']; }
                    $ut = 'user';
                    $u = $this->userData($uid , $cid , $ut , $communityId);
                    array_push($arr, $u[0]);
                    array_push($arr, $r);
                }else{return ["message" => 'something went wrong'];}
            }else{
                return $arr;
            }
            return $arr;
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }







    public function getCommunityPostsRequests($args)
    {

        Log::debug("Started ...getCommunityPostsRequests Dao");
        try{
            $conn = ConnectionManager::get('default');
            $pd = new PostsDao;
            $codes = new Codes;
            $args['page'] = $args['offset'];
            //checking keys for pagination
            //$args['page'] = (isset($args['page']) && $args['page'] > 1)?$args['page']:0;
            //$args['pageSize'] = (isset($args['pageSize']) && $args['pageSize'] > 0)?$args['pageSize']:10;
            $ans = array();
            $ans['articles'] = array();
            $ans['polls'] = array();
            $ans['debates'] = array();
            $ans['quickbits'] = array();
            $ans['sessions'] = array();
            $ans['quizzes'] = array();
            //checking community invitations
            $results = array();
            $cona = "where (user_type = 'community' or user_type = 'Community') and community_id = ". $args['community_id']." and approved_by = 0 order by articles_id DESC  limit ".(($args['page']) * $args['pagesize']).", ".$args['pagesize'];
            
            $cond = "where (user_type = 'community' or user_type = 'Community') and community_id = ". $args['community_id']." and approved_by = 0 order by id DESC  limit ".(($args['page']) * $args['pagesize']).", ".$args['pagesize'];
            $cons = "where (user_type = 'community' or user_type = 'Community') and community_id = ". $args['community_id']." and approved_by = 0 order by sessions_id DESC  limit ".(($args['page']) * $args['pagesize']).", ".$args['pagesize'];
            
            //$con = "where (user_type = 'community' and community_id = ". $args['community_id']." and is_approved_by_admin = 0 and post_type = 'poll') order by post_id DESC limit ".(($args['page']) * $args['pagesize']).", ".$args['pagesize'];
            switch($args['posttype']){
                case 'articles':
                    $sql = sprintf("SELECT * FROM articles $cona");
                    $st = $conn->execute($sql);
                    while($res = $st->fetch("assoc")){
                        $res['cover_photo'] = $this->getImgFullPath($res['cover_photo'] ,1);
                        $dat = $this->userData($args['user_id'] , $res['created_by'] , $res['user_type'] , $res['community_id']);
                        $res['created_by'] = $dat[0];
                        array_push($results , $res);
                    }

                    $ans['articles'] = $results;
                    break;
                case 'debates':
                    $sql = sprintf("SELECT * FROM debates $cond"); 
                    $st = $conn->execute($sql);
                    while($res = $st->fetch("assoc")){
                        $res['picture'] = $this->getImgFullPath($res['picture'] ,1);
                        $dat = $this->userData($args['user_id'] , $res['creator_id'] , $res['user_type'] , $res['community_id']);
                        $res['created_by'] = $dat[0];
                        array_push($results , $res);
                    }
                    $ans['debates'] = $results;
                    break;
                case 'quiz':
                    $sql = sprintf("SELECT * FROM quizzes $cond"); 
                    $st = $conn->execute($sql);
                    while($res = $st->fetch("assoc")){
                        $res['picture'] = $this->getImgFullPath($res['picture'] ,1);
                        $dat = $this->userData($args['user_id'] , $res['creator_id'] , $res['user_type'] , $res['community_id']);
                        $res['created_by'] = $dat[0];
                        array_push($results , $res);
                    }
                    $ans['quizzes'] = $results;
                    break;
                case 'quickbits':
                    $sql = sprintf("SELECT * FROM quickbits $cond"); 
                    $st = $conn->execute($sql);
                    while($res = $st->fetch("assoc")){
                        //$res['picture'] = $this->getImgFullPath($res['picture'] ,1);
                        $res['cards'] = array();
                        $sql = sprintf("select * from quickbits_cards where quickbit_id = %s", $res['id']);
                        $s = $conn->execute($sql);
                        while($a = $s->fetch('assoc')){
                            if($a['picture'] != ''){
                                $a['picture'] = $this->getImgFullPath($a['picture'],1);
                            }
                            array_push($res['cards'], $a);
                        }
                        $dat = $this->userData($args['user_id'] , $res['creator_id'] , $res['user_type'] , $res['community_id']);
                        $res['created_by'] = $dat[0];
                        array_push($results , $res);
                    }
                    $ans['quickbits'] = $results;
                    break;
                case 'polls':
                    $psql = $conn->execute(sprintf("SELECT * FROM polls $cond"));
                    while($p = $psql->fetch("assoc")){
                        array_push($results , $pd->getPollData($p['id'] , $args['user_id']) );
                    }

                    $ans['polls'] = $results;
                    break;
                
                case 'sessions':
                    $sql = "SELECT * FROM sessions $cons";
                    $st = $conn->execute($sql);
                    $arr = array();
                    while($r = $st->fetch("assoc")){
                        $r['cover_photo'] = ($r['cover_photo'] != null || $r['cover_photo'] != '' ? $this->getImgFullPath($r['cover_photo']) : "");
                        $r['creator_id'] = $r['created_by'];
                        $dat = $this->userData($args['user_id'] , $r['created_by'] , $r['user_type'] , $r['community_id']);
                        $r['created_by'] = (isset($dat[0])? $dat[0] : null);
                        //$r['community_data'] = (isset($dat[1])? $dat[1] : null);
                        array_push($arr , $r);
                    }
                    $ans['sessions'] = $arr;
                    //return $sql;
                    break;




                default:
                    $response = [
                    'status' => $codes->RC_ERROR,
                    'message' => "POSTTYPE INVALID",
                    'data' => null
                    ];
                    return $response;

            }
            $tablename = array("articles" => "article" , "debates" => "debate", "quiz" => "quiz", "quickbits" => "quickbit" , "polls" => "poll" , "sessions" => "session");
            $arr = array('articles_remaining' , 'debates_remaining', 'quiz_remaining', 'quickbits_remaining' , 'polls_remaining' , "sessions_remaining");
            $i = 0;
            foreach ($tablename as $key => $value) {
                $sql = sprintf("SELECT * FROM posts where community_id = %s and is_approved_by_admin = 0 and post_type = '%s' and user_type = 'community' limit 0,1", $args['community_id'] , $value);
                $st = $conn->execute($sql);
                if($st->count() > 0){
                    $ans[$arr[$i]] = true;
                }else{
                    $ans[$arr[$i]] = false;
                }
                $i = $i + 1;
            }
            

            return $ans;

        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }


}