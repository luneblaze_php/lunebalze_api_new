<?php

namespace App\DaoLayer;

use App\Utils\Codes;
use Cake\Core\Exception\Exception;
use Cake\Datasource\ConnectionManager;
use App\DaoLayer\PostsDao;
use App\DaoLayer\PointsDao;
use App\Model\UserModel;
use Cake\Log\Log;
use App\Utils\DateUtils;

class DebatesDao
{

    private function getImgFullPath($img = '', $newPath = 0){
        $codes = new Codes;
        return !empty($img) ?$codes->SYSTEM_URL.'/'.($newPath?'api/':'').$codes->UPLOADS_DIRECTORY.'/'.$img:'';
    }
    public function giveDebateCreationPts($id){
        $codes = new Codes;
        $pod = new PointsDao;
        $conn = ConnectionManager::get('default');
        $sql = $conn->execute("SELECT creator_id FROM `debates` WHERE id = $id");
        $res = $sql->fetch('assoc');
        $ret = $pod->checkAndDeduct($res['creator_id'], $codes->DEDUCT_DEBATE);
        if($ret == 0){return $ret;}
        $k = 1; //multiplier for future use
        $points = $k * $codes->DEBATES_K;
        
        $pod->addPoints('debates', $res['creator_id'], $points);
        return $points;
    }
    public function giveVotePoints( $type, $id , $up = 1){
        $codes = new Codes;
        $pod = new PointsDao;
        $points = 0;
        $conn = ConnectionManager::get('default');
        switch($type){
            case 'main_vote':
                $sql = $conn->execute("SELECT creator_id FROM `debates` WHERE id = $id");
                $res = $sql->fetch('assoc');
                $points = $codes->DEBATES_C2 * $up;
                $pod->addPoints('debates', $res['creator_id'], $points);
                break;
            case 'main_procon':
                $sql = $conn->execute("SELECT creator_id FROM `debates` WHERE id = $id");
                $res = $sql->fetch('assoc');
                $points = $codes->DEBATES_C1 * $up;
                $pod->addPoints('debates', $res['creator_id'], $points);
                break;
            case 'discussion_procon':
                $sql = $conn->execute("SELECT user_id FROM `debates_discussions` WHERE id = $id");//pass parent id
                $res = $sql->fetch('assoc');
                $points = $codes->DEBATES_C1 * $up;
                $pod->addPoints('debates', $res['user_id'], $points);
                break;
            case 'discussion_vote':
                //$sql = $conn->execute("SELECT user_id FROM `debates_discussions` WHERE id = $id");//pass parent id
                //$res = $sql->fetch('assoc');
                $points = $codes->DEBATES_C2 * $up;
                $pod->addPoints('debates', $id, $points);//directly sending userid
                break;
        }
        return $points;
    }
    
    /**
     * Save Debate
     */
    public function saveDebates($args, $debate_picture, $debate_picture_dimensions="")
    {

        try{

            $conn = ConnectionManager::get('default');
            $codes = new Codes;
            $dateUtils = new DateUtils;
            if($args['user_type'] == 'user' || $args['user_type'] == 'User' || $args['user_type'] == 'community' || $args['user_type'] == 'Community'){
                $sql = $conn->execute(sprintf("SELECT total FROM users_points where user_id = %s", $args['creator_id']));
                //return $sql;
                if($sql = $sql->fetch('assoc')){
                    if($sql['total'] >= $codes->DEDUCT_DEBATE){

                    }else{
                        throw new Exception("not enough points");
                    }
                }else{
                    $conn->execute("INSERT into users_points set user_id = $id , total = $codes->INITIAL_PTS");
                    return $this->saveDebates($args, $debate_picture, $debate_picture_dimensions);
                }

            }
            Log::debug("Started ...saveDebate Dao");
            $sql=sprintf("INSERT INTO `debates`(`title`, `description`, `picture`, `picture_dimensions`, `user_type`, `creator_id`, `start_time`, `live_duration`,community_id ,  `interests`) VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', %s, '%s' , '%s')", $args['title'], $args['description'], $debate_picture, $debate_picture_dimensions, $args['user_type'], $args['creator_id'], $args['start_time'], $args['live_duration'],(isset($args['community_id'])?$args['community_id']:0), $args['interests']);

            Log::debug("SQL : ".$sql);
            //return $sql;
            $conn->execute($sql);
            
            $sql="SELECT LAST_INSERT_ID()";

            $date = $dateUtils->getCurrentDate();
            $stmt = $conn->execute($sql);
            
            $res = $stmt->fetch();
            $sql=sprintf("INSERT INTO posts (debate_id, user_id, post_title, community_id, post_type, privacy, text_tagged, origin_type, organisation_id, payment,total,updated_at, time , user_type) VALUES (%s , '%s', '%s', '%s', 'debate', 'public', '', '', 0, 0, 0, '%s', '%s' , '%s')", $res[0], $args['creator_id'], $args['title'], (isset($args['community_id'])?$args['community_id']:0), $date, $date, $args['user_type']);
            Log::debug("Ended ...saveDebate Dao");
            $conn->execute($sql);
            if($args['user_type'] == 'user' || $args['user_type'] == 'User'){
                $ret = $this->giveDebateCreationPts($res[0]);
                if($ret == 0){throw new Exception("not enough points.");}
            }
            return $res[0];
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }    
    /**
     * Update Debate
     */
    public function updateDebates($args, $debate_picture = '', $debate_picture_dimensions = '')
    {
        Log::debug("Started ...updateDebates Dao");

        try{

            $conn = ConnectionManager::get('default');

            $imgQry = ($debate_picture)?", picture = '".$debate_picture."', picture_dimensions ='".$debate_picture_dimensions."'":"";
            $sql=sprintf("UPDATE debates SET title = '%s', description = '%s', user_type = '%s', start_time = '%s', live_duration = '%s', `interests` = '%s' $imgQry
                WHERE id = '%s' AND creator_id = '%s'", $args['title'], $args['description'], $args['user_type'], $args['start_time'], $args['live_duration'], @$args['interests'], $args['debate_id'], $args['creator_id']);

            Log::debug("SQL : ".$sql);

            $conn->execute($sql);
           
            Log::debug("Ended ...updateDebates Dao");
            
            return 1;
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }
    /**
     * Delete Debate
     */
    public function deleteDebates($debate_id, $creator_id)
    {
        Log::debug("Started ...deleteDebates Dao");

        try{

            $conn = ConnectionManager::get('default');

            //deleting questions
            $sql=sprintf("UPDATE debates SET deleted = 1 WHERE id = '%s' AND creator_id = '%s'", 
            $debate_id, $creator_id);
            Log::debug("SQL : ".$sql);
            $conn->execute($sql);
           
            Log::debug("Ended ...deleteDebates Dao");
            
            return 1;
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }
    /**
     * Get Debates
     */
    public function getDebatesList($args, $date)
    {
        Log::debug("Started ...getDebatesList Dao");
        $codes = new Codes;

        try{

            $conn = ConnectionManager::get('default');
            $cond = "";
            $having = "";
            $join_cond = " AND user_id = '".$args['user_id']."'";
            //checking filter params
            if (isset($args['my_role']) && !empty($args['my_role'])) {
                //options: 
                // 1. Attempted/submitted (already attempted)
                // 2. Participating (i am going to attempt)
                // 3. Created (i created the debate)
                if($args['my_role'] =='Attempted/submitted'){
                    $having = " is_attained = 1";
                }
                elseif($args['my_role'] =='Attempted'){
                    $having = " is_attained = 1";
                }
                elseif($args['my_role'] =='Submitted'){
                    $having = " is_submitted = 1";
                }
                else if($args['my_role'] =='Participating'){
                    $having = " dp_id > 0";
                }
                else if($args['my_role'] =='Created')
                    $cond .= " AND de.creator_id = '".$args['user_id']."'";

            }
            if (isset($args['interests']) && !empty($args['interests'])) {
                //options: 
                // 1. My interests
                // 2. Custom list of interest Ids
                $interestCond = '';
                if($args['interests'] == 'My interests'){
                    
                    $sql=sprintf("SELECT * FROM user_interest where user_id= '%s'", $args['user_id']);
                    Log::debug("SQL : ".$sql);
                    $stmt = $conn->execute($sql);
                    $key = 0;
                    while($result = $stmt->fetch("assoc")) {
                        $interestCond .= ($key?' OR':'')." FIND_IN_SET('".$result['interest']."', de.interests)";
                        $key++;
                    }
                }
                else if(is_array($args['interests'])){
                    $interestCond = '';
                    foreach ($args['interests'] as $key => $interest) {
                        $interestCond .= ($key?' OR':'')." FIND_IN_SET('".$interest."', de.interests) > 0";
                    }
                }
                // condition according to intrest
                $cond .= ($interestCond)?" AND (".$interestCond.")":'';

            }
            // condition for query text
            if (isset($args['query_text']) && !empty($args['query_text'])) {
                $cond .= " AND de.title like '%".$args['query_text']."%'";
            }
            // condition for live status of debate
            if (isset($args['live_status']) && !empty($args['live_status'])) {
                //options: 
                // 1. Before live duration
                // 2. During live duration
                // 3. After live duration

                if($args['live_status'] =='Before live duration')
                    $having .= ($having?' AND ':'')." debate_status = 'before live'";
                else if($args['live_status'] =='During live duration')
                    $having .= ($having?' AND ':'')." debate_status = 'live'";
                else if($args['live_status'] =='After live duration')
                    $having .= ($having?' AND ':'')." debate_status = 'after live'";

            }


            //checking keys for pagination
            $args['page'] = (isset($args['page']) && $args['page'] > 1)?$args['page']:1;
            $args['pageSize'] = (isset($args['pageSize']) && $args['pageSize'] > 0)?$args['pageSize']:10;

            $cond .= ($having)?" HAVING ( ". $having." ) ":'';
            $cond .= " order by debate_status desc limit ".(($args['page']- 1) * $args['pageSize']).", ".$args['pageSize'];

            //debate query
            $sql="SELECT de.*, us.user_fullname, IF(dp.is_attained = 1, 1, 0) as is_attained, dp.score, dp.id as dp_id,
                (case
                    when DATE_ADD(start_time, INTERVAL `live_duration` second) < '".$date."' then 'after live'
                    when start_time > '".$date."' then 'before live'
                else 'live' end) as debate_status,
                (SELECT count(*) as prosCount FROM debates_discussions as dd WHERE dd.is_pro  = 1 AND dd.debate_id = de.id) as prosCount,
                (SELECT count(*) as consCount FROM debates_discussions as dd WHERE dd.is_pro  = 0 AND dd.debate_id = de.id) as consCount
                FROM  `debates` as de left join users as us on us.user_id = de.creator_id left join debates_participants as dp on dp.id = (SELECT id FROM debates_participants WHERE debate_id = de.id $join_cond limit 0, 1) WHERE  deleted = 0 $cond";
            Log::debug("SQL : ".$sql);
            // echo $sql; exit;
            $stmt = $conn->execute($sql);
            
            $responseData = array();

            while($result = $stmt->fetch("assoc")) {
                $result['picture'] = $this->getImgFullPath($result['picture'],1);
                $result['goingToParticipate'] = ($result['dp_id'] > 0)?1:0;
                $result['debateInterestArr'] = [];
                $result['participatingFriendsCount'] = 0;
                $result['participatingFriend'] = null;
                $result['prosConsCount'] = $result['prosCount']+$result['consCount'];

                //checking friend participants
                $sql="SELECT users.user_id, users.user_work, users.user_work_title, users.user_work_place, users.user_name, users.user_fullname, users.user_firstname,user_lastname, users.user_gender, users.user_picture, dp.is_attained, dp.score, dp.id as dp_id FROM friends INNER JOIN users ON ((friends.user_one_id = users.user_id AND friends.user_one_id != $args[user_id]) OR (friends.user_two_id = users.user_id AND friends.user_two_id != $args[user_id])) left join debates_participants as dp on dp.id =(SELECT id FROM debates_participants WHERE debate_id = '$result[id]' AND user_id = users.user_id limit 0, 1) WHERE friends.status = 1 AND (user_one_id = $args[user_id] OR user_two_id = $args[user_id]) having dp_id > 0 ";
                Log::debug("SQL : ".$sql);
                $fellowsStmt = $conn->execute($sql);

                while($fellowResult = $fellowsStmt->fetch("assoc")) {
                    if(empty($result['participatingFriend'])){

                        $fellowResult['user_picture'] = $this->getImgFullPath($fellowResult['user_picture']);
                        $fellowResult['is_attained'] = $fellowResult['is_attained']?1:0;
                        $fellowResult['score'] = $fellowResult['score']?$fellowResult['score']:0;
                        $result['participatingFriend'] = $fellowResult;
                    }
                    $result['participatingFriendsCount']++;
                }
                // checking creater info
                $sql=sprintf("SELECT user_id, user_email, user_name, user_fullname, user_firstname, user_lastname, user_gender, user_picture, user_work, user_work_title , user_work_place , '' as connection FROM users WHERE user_id = '%s' limit 0, 1", $result['creator_id']);

                Log::debug("SQL : ".$sql);
                $createrStmt = $conn->execute($sql);
                $result['created_by'] = $createrStmt->fetch("assoc");
                $result['created_by'] = $result['created_by']?$result['created_by']:[];
                //setting user image
                $result['created_by']['user_picture'] = $this->getImgFullPath(@$result['created_by']['user_picture']);

                //setting interests
                if($result['interests']){

                    // checking interests
                    $sql=sprintf("SELECT interest_id, `text` as interest_name FROM interest_mst WHERE FIND_IN_SET(interest_id, '%s')", $result['interests']);

                    Log::debug("SQL : ".$sql);
                    $interestStmt = $conn->execute($sql);

                    while($interestResult = $interestStmt->fetch("assoc"))
                        array_push($result['debateInterestArr'], $interestResult);

                }

                array_push($responseData,$result);
            }
           
            Log::debug("Ended ...getDebatesList Dao");

            return $responseData;
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }

    public function getDebateByUserAndId($debate_id, $user_id, $date)
    {
        Log::debug("Started ...getDebateByUserAndId Dao");
        $codes = new Codes;
        $pod = new PostsDao;
        try{

            $conn = ConnectionManager::get('default');
            $join_cond = " AND dp.user_id = '".$user_id."'";
            //getting debate details
            $sql=sprintf("SELECT de.*, us.user_fullname, IF(dp.is_attained = 1, 1, 0) as is_attained, dp.score, dp.id as dp_id,
                (case
                    when DATE_ADD(start_time, INTERVAL `live_duration` second) < '%s' then 'after live'
                    when start_time > '%s' then 'before live'
                else 'live' end) as debate_status,
                (SELECT count(*) as prosCount FROM debates_discussions as dd WHERE dd.is_pro  = 1 AND dd.debate_id = dp.debate_id) as prosCount,
                (SELECT count(*) as consCount FROM debates_discussions as dd WHERE dd.is_pro  = 0 AND dd.debate_id = dp.debate_id) as consCount
                FROM `debates` as de left join users as us on us.user_id = de.creator_id left join debates_participants as dp on (dp.debate_id = de.id $join_cond) WHERE de.id = '%s' limit 0, 1", $date, $date, $debate_id);

            Log::debug("SQL : ".$sql);
            // echo $sql; exit;
            $debateStmt = $conn->execute($sql);
            $debateData = $debateStmt->fetch("assoc");
            if($debateData){
                //checking debate is deleted
                if($debateData['deleted'])
                    return ['deleted'=>1];

                $dat = $pod->userData($user_id , $debateData['creator_id'] , $debateData['user_type'] , $debateData['community_id']);
                $debateData['created_by'] = (isset($dat[0])? $dat[0]  : null);
                $debateData['community_data'] = (isset($dat[1])? $dat[1] : null);

                $debateData['picture'] = $this->getImgFullPath($debateData['picture'],1);
                $debateData['goingToParticipate'] = ($debateData['dp_id'] > 0)?1:0;
                $debateData['participantsData'] = [];
                $debateData['topParticipantsData'] = [];
                $debateData['prosConsData'] = [];
                $debateData['prosConsCount'] = 0;
                $debateData['debateInterestArr'] = [];
                //$debateData['created_by'] = [];
                $debateData['topProsData'] = [];
                $debateData['topConsData'] = [];

                $debateData['participatingFriendsCount'] = 0;
                $debateData['participatingFriend'] = null;

                //checking friend participants
                $sql="SELECT users.user_id, users.user_work, users.user_work_title, users.user_work_place, users.user_name, users.user_fullname, users.user_firstname,user_lastname, users.user_gender, users.user_picture, dp.is_attained, dp.score, dp.id as dp_id FROM friends INNER JOIN users ON ((friends.user_one_id = users.user_id AND friends.user_one_id != $user_id) OR (friends.user_two_id = users.user_id AND friends.user_two_id != $user_id)) left join debates_participants as dp on dp.id =(SELECT id FROM debates_participants WHERE debate_id = '$debateData[id]' AND user_id = users.user_id limit 0, 1) WHERE friends.status = 1 AND (user_one_id = $user_id OR user_two_id = $user_id) having dp_id > 0 ";
                Log::debug("SQL : ".$sql);
                $fellowsStmt = $conn->execute($sql);

                while($fellowResult = $fellowsStmt->fetch("assoc")) {
                    if(empty($debateData['participatingFriend'])){

                        $fellowResult['user_picture'] = $this->getImgFullPath($fellowResult['user_picture']);
                        $fellowResult['is_attained'] = $fellowResult['is_attained']?1:0;
                        $fellowResult['score'] = $fellowResult['score']?$fellowResult['score']:0;
                        $result['participatingFriend'] = $fellowResult;
                    }
                    $debateData['participatingFriendsCount']++;
                }

                // checking pros and crons

                $sql=sprintf("SELECT dd.*, us.user_name, us.user_fullname, us.user_firstname,user_lastname, us.user_gender, us.user_picture,
                    (SELECT id FROM debates_discussion_followers where discussion_id= dd.id AND user_id = '%s' limit 0,1) as following_id,
                (SELECT count(*) as prosCount FROM debates_discussions as sdd WHERE sdd.is_pro  = 1 AND sdd.parent_id = dd.id) as subProsCount,
                (SELECT count(*) as consCount FROM debates_discussions as sdd WHERE sdd.is_pro  = 0 AND sdd.parent_id = dd.id) as subConsCount FROM debates_discussions as dd left join users as us on us.user_id = dd.user_id WHERE dd.debate_id = '%s'",$user_id, $debate_id);
                Log::debug("SQL : ".$sql);
                $prosConsStmt = $conn->execute($sql);

                while($result = $prosConsStmt->fetch("assoc")){
                    $result['user_picture'] = $this->getImgFullPath($result['user_picture']);
                    $result['is_following'] = $result['following_id']?1:0;
                    array_push($debateData['prosConsData'], $result);
                }
                $debateData['prosConsCount'] = count($debateData['prosConsData']);                

                // checking top pros
                $sql=sprintf("SELECT dd.*, us.user_name, us.user_fullname, us.user_firstname,user_lastname, us.user_gender, us.user_picture, (SELECT id FROM debates_discussion_followers where discussion_id= dd.id AND user_id = '%s' limit 0,1) as following_id,
                    (SELECT count(*) as prosCount FROM debates_discussions as sdd WHERE sdd.is_pro  = 1 AND sdd.parent_id = dd.id) as subProsCount,
                    (SELECT count(*) as consCount FROM debates_discussions as sdd WHERE sdd.is_pro  = 0 AND sdd.parent_id = dd.id) as subConsCount FROM debates_discussions as dd left join users as us on us.user_id = dd.user_id WHERE is_pro = 1 AND dd.debate_id = '%s' order by rating desc limit 0, 2",$user_id, $debate_id);
                Log::debug("SQL : ".$sql);
                $prosConsStmt = $conn->execute($sql);
                while($result = $prosConsStmt->fetch("assoc")){
                    $result['user_picture'] = $this->getImgFullPath($result['user_picture']);
                    $result['is_following'] = $result['following_id']?1:0;
                    array_push($debateData['topProsData'], $result);
                }

                // checking top cons
                $sql=sprintf("SELECT dd.*, us.user_name, us.user_fullname, us.user_firstname,user_lastname, us.user_gender, us.user_picture, (SELECT id FROM debates_discussion_followers where discussion_id= dd.id AND user_id = '%s' limit 0,1) as following_id,
                (SELECT count(*) as prosCount FROM debates_discussions as sdd WHERE sdd.is_pro  = 1 AND sdd.parent_id = dd.id) as subProsCount,
                (SELECT count(*) as consCount FROM debates_discussions as sdd WHERE sdd.is_pro  = 0 AND sdd.parent_id = dd.id) as subConsCount FROM debates_discussions as dd left join users as us on us.user_id = dd.user_id WHERE is_pro = 0 AND dd.debate_id = '%s' order by rating desc limit 0, 2", $user_id, $debate_id);
                Log::debug("SQL : ".$sql);
                $prosConsStmt = $conn->execute($sql);
                while($result = $prosConsStmt->fetch("assoc")){
                    $result['user_picture'] = $this->getImgFullPath($result['user_picture']);
                    $result['is_following'] = $result['following_id']?1:0;
                    array_push($debateData['topConsData'], $result);
                }
                //setting interests
                if($debateData['interests']){

                    // checking interests
                    $sql=sprintf("SELECT interest_id, `text` as interest_name FROM interest_mst WHERE FIND_IN_SET(interest_id, '%s')", $debateData['interests']);

                    Log::debug("SQL : ".$sql);
                    $interestStmt = $conn->execute($sql);

                    while($result = $interestStmt->fetch("assoc"))
                        array_push($debateData['debateInterestArr'], $result);

                }

                // checking current user score
                // if($debateData['dpId']){
                //     // checking attempted user and top scorer
                //     $sql=sprintf("SELECT dp.*, us.user_fullname, (SELECT count(*) as prosCount FROM debates_discussions as dd WHERE dd.is_pro  = 1 AND dd.debate_id = dp.debate_id AND dd.user_id = dp.user_id ) as prosCount, (SELECT count(*) as consCount FROM debates_discussions as dd WHERE dd.is_pro  = 0 AND dd.debate_id = dp.debate_id AND dd.user_id = dp.user_id ) as consCount, (SELECT dd.id FROM debates_discussions as dd WHERE dd.debate_id = dp.debate_id AND dd.user_id = dp.user_id order by id desc limit 1) as lastNode FROM debates_participants as dp left join users as us on us.user_id = dp.user_id WHERE dp.id = '%s' limit 0, 1 ", $debateData['dpId']);

                //     Log::debug("SQL : ".$sql);
                //     $myStmt = $conn->execute($sql);
                //     $debateData['myData'] = $myStmt->fetch("assoc");
                   

                // }

                if($debateData['debate_status'] == 'after live'){
                    // checking attempted user and top scorer
                    $sql=sprintf("SELECT dp.*, us.user_fullname, (SELECT count(*) as prosCount FROM debates_discussions as dd WHERE dd.is_pro  = 1 AND dd.debate_id = dp.debate_id AND dd.user_id = dp.user_id ) as prosCount, (SELECT count(*) as consCount FROM debates_discussions as dd WHERE dd.is_pro  = 0 AND dd.debate_id = dp.debate_id AND dd.user_id = dp.user_id ) as consCount FROM debates_participants as dp left join users as us on us.user_id = dp.user_id WHERE dp.debate_id = '%s' order by dp.score desc", $debate_id);

                    Log::debug("SQL : ".$sql);
                    $participantsStmt = $conn->execute($sql);

                    while($result = $participantsStmt->fetch("assoc"))
                        array_push($debateData['participantsData'], $result);

                    if($debateData['participantsData']){
                        $debateData['topParticipantsData'] = array_slice($debateData['participantsData'], 0, 10);
                    }

                }

                $debateData['totalParticipants'] = count($debateData['participantsData']);

                $pd = $conn->execute("SELECT post_id FROM posts where debate_id = $debate_id");
                $pd = $pd->fetch('assoc');
                $pid = $pd["post_id"];

                //reactions
                $react = array();
                $rname = array();
                $reactions = $conn->execute(sprintf("SELECT * from `reactions` "));
                while($r = $reactions->fetch('assoc')){
                    $str = '' . $r['id'] . '';
                    //$react[$str] = 0;
                    array_push($react, 0);
                    array_push($rname, $r['name']);


                }
                $count = count($rname);
                //$pc = $conn->execute(sprintf("SELECT * from `posts` where post_type = 'article' and article_id = %s"  , $articlesId));

                //if(!$p = $pc->fetch("assoc")){return "no post found";}
                $l = $conn->execute(sprintf("SELECT * from `posts_likes` where post_id = $pid"));
                $t = 0;
                while($r = $l->fetch("assoc")){
                    $t = $t + 1;
                    $str = $r['reaction_type'];
                    $react[$str] = $react[$str] + 1;
                }
                $fr = array();
                for ($i=0; $i < $count; $i++) { 
                    # code...
                    $fr[$rname[$i]] = $react[$i];
                }

                $debateData['reactions'] = $fr;
                $debateData['total_likes'] = $t;
                
                $il = $conn->execute(sprintf("SELECT * FROM posts_likes WHERE post_id = $pid and user_id = $user_id"));
                if($li = $il->fetch("assoc")){
                    $debateData["i_like"] = $li['reaction_type'];
                }else{$debateData["i_like"] = null;}
                //new
                $user = new UserModel($user_id);
                $mf = $user->get_friends_ids($user_id);
                $mf = implode(',', $mf);
                if($co = $conn->execute(sprintf("SELECT * FROM posts_comments WHERE node_id = $pid and node_type = 'user' and FIND_IN_SET(user_id , '$mf') order by comment_id desc"))->fetch('assoc')){
                    $friend = $pod->userData($user_id , $co['user_id'] , 'user' , 0)[0];
                    $co['user'] = $friend;
                    $debateData['friend_comment'] = $co;
                }else{
                    $debateData['friend_comment'] = null;
                }
                if($il = $conn->execute(sprintf("SELECT * FROM posts_likes WHERE post_id = $pid and FIND_IN_SET(user_id , '$mf')"))->fetch('assoc')){
                    $friend = $pod->userData($user_id , $il['user_id'] , 'user' , 0)[0];
                    $il['user'] = $friend;
                    $debateData['friend_like'] = $il;
                }else{
                    $debateData['friend_like'] = null;
                }
                if($il = $conn->execute(sprintf("SELECT * FROM debates_discussions WHERE debate_id = $debate_id and FIND_IN_SET(user_id , '$mf')"))->fetch('assoc')){
                    $friend = $pod->userData($user_id , $il['user_id'] , 'user' , 0);
                    $il['user'] = $friend[0];
                    $debateData['friend_discussion'] = $il;
                }else{
                    $debateData['friend_discussion'] = null;
                }

            }

            Log::debug("Ended ...getDebateByUserAndId Dao");

            return $debateData;
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }
    
    /**
     * getDebateCountByUserId
     */
    public function getDebateCountByUserId($userId)
    {
        Log::debug("Started ...getDebateCountByUserId Dao : User Id : ".$userId);

        try{
            
            $conn = ConnectionManager::get('default');

            $sql=sprintf("SELECT count(*) as total FROM `debates` WHERE `creator_id` =  '%s'", $userId);

            Log::debug("SQL : ".$sql);

            $stmt = $conn->execute($sql);

            $result = $stmt->fetch("assoc");

            Log::debug("Ended ...getUserByUserIdAndLoginOtp Dao");

            return $result;
            
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }

    }

    //updating user debate participation
    public function addRemoveDebateParticipation($args, $date)
    {

        Log::debug("Started ...addRemoveDebateParticipation Dao");
        try{
            $conn = ConnectionManager::get('default');
             $response = array("valid" => false, "msg"=>"This debate has been expired.");

            //checking debate live duration
            $sql=sprintf("SELECT * FROM debates WHERE id = '%s' AND start_time >= '%s'", $args['debate_id'], $date);
            Log::debug("SQL : ".$sql);
            $stmt = $conn->execute($sql);
            $debateData = $stmt->fetch("assoc");

            if($debateData){
                //checking debate already participant
                $sql=sprintf("SELECT * FROM debates_participants WHERE debate_id = '%s' AND user_id = '%s'", $args['debate_id'], $args['user_id']);
                Log::debug("SQL : ".$sql);
                $stmt = $conn->execute($sql);
                $participantData = $stmt->fetch("assoc");

                if($args['status'] == 2){
                    if(!empty($participantData)){
                        //deleting participation of user
                        $sql=sprintf("DELETE FROM `debates_participants` WHERE debate_id = '%s' AND user_id = '%s'", $args['debate_id'], $args['user_id']);
                        Log::debug("SQL : ".$sql);
                        if($conn->execute($sql)){
                            $response = array("valid" => true, "msg"=>"Participation removed");
                        }else
                            $response["msg"] = "Something went wrong.";
                    }else
                        $response["msg"] = "Not participated.";
                    
                }else if($args['status'] == 1){
                    if(empty($participantData)){
                        //inserting participation of user
                        $sql=sprintf("INSERT INTO `debates_participants`(`debate_id`, `user_id`) VALUES ('%s', '%s')", $args['debate_id'], $args['user_id']);
                        Log::debug("SQL : ".$sql);
                        if($conn->execute($sql)){
                            $response = array("valid" => true, "msg"=>"Participated successfully");
                        }else
                            $response["msg"] = "Something went wrong.";
                    }else
                        $response["msg"] = "Already participated.";

                    
                }
                Log::debug("SQL : ".$sql);
                $conn->execute($sql);
            }

           
            Log::debug("Ended ...addRemoveDebateParticipation Dao");
            
            return $response;
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }
    //updating user debate participation answer
    public function sendDebateParticipationProsAndCons($args, $date)
    {
        // echo $date; exit;
        Log::debug("Started ...sendDebateParticipationProsAndCons Dao");
        $response = array("valid" => false, "msg"=>"This debate is not live currently.");
        try{
            $conn = ConnectionManager::get('default');
            //checking debate live duration
            // AND DATE_ADD(start_time, INTERVAL `live_duration` second) >= '%s'
            $sql=sprintf("SELECT * FROM debates WHERE id = '%s' AND start_time <= '%s'", $args['debate_id'], $date);
            Log::debug("SQL : ".$sql);

            $stmt = $conn->execute($sql);
            $debateData = $stmt->fetch("assoc");

            if($debateData){

                //inserting participation asnswer of user
                $sql=sprintf("INSERT INTO `debates_discussions`(`debate_id`, `user_id`, `debate_text`, `is_pro`, `parent_id`, `created`) VALUES ('%s', '%s','%s', '%s','%s','%s')", $args['debate_id'], $args['user_id'], $args['debate_text'], (($args['is_pro'] == 1)?1:0), (isset($args['parent_id'])?$args['parent_id']:0), $date);
                Log::debug("SQL : ".$sql);
                //return array("valid" => true, "msg"=>$sql, 'data'=> null);
                if($conn->execute($sql)){

                    $sql="SELECT LAST_INSERT_ID()";                    
                    $stmt = $conn->execute($sql);                    
                    $res = $stmt->fetch();
                    $discussion_id = @$res[0];

                    //checking if answer already participated
                    $sql=sprintf("SELECT * FROM debates_participants WHERE debate_id = '%s' AND user_id = '%s' limit 0, 1", $args['debate_id'], $args['user_id']);
                    Log::debug("SQL : ".$sql);
                    $participantsstmt = $conn->execute($sql);
                    $participantsData = $participantsstmt->fetch("assoc");
                    if(empty($participantsData)){
                        //inserting participation of user
                        $sql=sprintf("INSERT INTO `debates_participants`(`debate_id`, `user_id`) VALUES ('%s', '%s')", $args['debate_id'], $args['user_id']);
                        Log::debug("SQL : ".$sql);
                        $conn->execute($sql);
                    }
                    if(isset($args['parent_id']) && $args['parent_id'] != 0)
                        $this->giveVotePoints('discussion_procon' , $args['parent_id']);
                    else
                        $this->giveVotePoints('main_procon', $args['debate_id'] );

                    // $scoreData = $this->getDebateScoreCalculation($args, $date, 1);
                    $prosConsData = $this->getDebateProsAndConsById($discussion_id, $args['user_id']);
                    $response = array("valid" => true, "msg"=>"Updated", "data"=> $prosConsData);
                }
                else
                    $response["msg"] = "Something went wrong.";

            }
           
            Log::debug("Ended ...sendDebateParticipationProsAndCons Dao");
            
            return $response;
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }

    //updating user debate participation answer
    public function updateDebateParticipantsProsAndCons($args, $date)
    {
        // echo $date; exit;
        Log::debug("Started ...updateDebateParticipantsProsAndCons Dao");
        $response = array("valid" => false, "msg"=>"This discussion not found.");
        try{
            $conn = ConnectionManager::get('default');
            //checking debate live duration
            $sql=sprintf("SELECT * FROM debates_discussions WHERE id = '%s' AND user_id = '%s'", $args['discussion_id'], $args['user_id']);
            Log::debug("SQL : ".$sql);
            $stmt = $conn->execute($sql);
            $discussionsData = $stmt->fetch("assoc");

            if($discussionsData){
                $sql=sprintf("UPDATE debates_discussions SET debate_text = '%s', is_pro = '%s' WHERE id = '%s'", $args['debate_text'], (($args['is_pro'] == 1)?1:0), $args['discussion_id']);
                Log::debug("SQL : ".$sql);
                if($conn->execute($sql)){
                    $args['debate_id'] = @$discussionsData['debate_id'];
                    $scoreData = $this->getDebateScoreCalculation($args, $date, 1);
                    $response = array("valid" => true, "msg"=>"Updated", "data"=> $scoreData);
                }
                else
                    $response["msg"] = "Something went wrong.";

            }
           
            Log::debug("Ended ...updateDebateParticipantsProsAndCons Dao");
            
            return $response;
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }
    
    //updating user debate participation answer
    public function deleteDebateParticipantsProsAndCons($args, $date)
    {
        // echo $date; exit;
        Log::debug("Started ...deleteDebateParticipantsProsAndCons Dao");
        $response = array("valid" => false, "msg"=>"This discussion not found.");
        try{
            $conn = ConnectionManager::get('default');
            //checking debate live duration
            $sql=sprintf("SELECT * FROM debates_discussions WHERE id = '%s' AND user_id = '%s'", $args['discussion_id'], $args['user_id']);
            Log::debug("SQL : ".$sql);
            $stmt = $conn->execute($sql);
            $discussionsData = $stmt->fetch("assoc");

            if($discussionsData){
                $sql=sprintf("DELETE FROM `debates_discussions` WHERE id = '%s'", $args['discussion_id']);
                Log::debug("SQL : ".$sql);
                if($conn->execute($sql)){
                    $response = array("valid" => true, "msg"=>"Deleted", "data"=> null);
                }
                else
                    $response["msg"] = "Something went wrong.";

            }
           
            Log::debug("Ended ...deleteDebateParticipantsProsAndCons Dao");
            
            return $response;
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }

    //calculation of debate score 
    public function getDebateScoreCalculation($args, $date, $onlyCalculateScore = 0)
    {

        Log::debug("Started ...getDebateScoreCalculation Dao");
        try{
            $conn = ConnectionManager::get('default');
            //checking debate score 
            $sql=sprintf("SELECT count(*) as total FROM debates_discussions as dd WHERE dd.debate_id = '%s' AND dd.user_id = '%s'", $args['debate_id'], $args['user_id']);
            Log::debug("SQL : ".$sql);
            $stmt = $conn->execute($sql);
            $debateData = $stmt->fetch("assoc");

            if($debateData){

                //setting as submitted debate
                $subQry = $onlyCalculateScore?"":" is_submitted = 1, ";
                $sql=sprintf("UPDATE debates_participants SET score = '%s', $subQry is_attained = 1 WHERE user_id = '%s' AND debate_id = '%s'", $debateData['total'], $args['user_id'], $args['debate_id']);

                Log::debug("SQL : ".$sql);
                $conn->execute($sql);
                
            }
           
            Log::debug("Ended ...getDebateScoreCalculation Dao");
            
            return isset($debateData['total'])?$debateData['total']:0;
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }
    //sending invitaion for debate
    public function sendDebateInvitaion($args, $date)
    {

        Log::debug("Started ...sendDebateInvitaion Dao");
        $response = array("valid" => false, "msg"=>"This debate is not open of invitation.");
        try{
            $conn = ConnectionManager::get('default');

            //checking debate live duration
            // AND DATE_ADD(start_time, INTERVAL `live_duration` second) >= '%s'
            $sql=sprintf("SELECT * FROM debates WHERE deleted = 0 AND id = '%s'", $args['debate_id']);
            Log::debug("SQL : ".$sql);
            $stmt = $conn->execute($sql);
            $debateData = $stmt->fetch("assoc");

            //checking debate invitations
            $sql=sprintf("SELECT * FROM debates_invitations WHERE debate_id = '%s' AND user_id = '%s' AND sender_user_id = '%s'", $args['debate_id'], $args['user_id'], $args['sender_user_id']);
            Log::debug("SQL : ".$sql);
            $stmt = $conn->execute($sql);
            $invitationData = $stmt->fetch("assoc");

            if(!empty($debateData)){

                if(empty($invitationData)){               
                    //inserting invitation of user
                    $sql=sprintf("INSERT INTO `debates_invitations`(`debate_id`, `user_id`, `sender_user_id`, `created`) VALUES ('%s', '%s','%s', '%s')", $args['debate_id'], $args['user_id'], $args['sender_user_id'], $date);
                    Log::debug("SQL : ".$sql);

                    if($conn->execute($sql)){   
                        $response = array("valid" => true, "msg"=>"Updated");
                    }else
                        $response["msg"] = "Something went wrong.";              
                }else
                    $response["msg"] = "Already invited";
            }
           
            Log::debug("Ended ...sendDebateInvitaion Dao");
            
            return $response;
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }

    //checking invitation for debate
    public function getDebateInvitedUserList($args)
    {

        Log::debug("Started ...getDebateInvitedUserList Dao");
        try{
            $conn = ConnectionManager::get('default');
            $codes = new Codes;

            //checking keys for pagination
            $args['page'] = (isset($args['page']) && $args['page'] > 1)?$args['page']:1;
            $args['pageSize'] = (isset($args['pageSize']) && $args['pageSize'] > 0)?$args['pageSize']:10;

            //checking debate invitations
            $cond = (isset($args['search_keyword']) && !empty($args['search_keyword']))?" AND (users.user_fullname LIKE '%".$args['search_keyword']."%' OR users.user_name LIKE '%".$args['search_keyword']."%')":"";
            $cond .= " limit ".(($args['page']- 1) * $args['pageSize']).", ".$args['pageSize'];

            // $sql=sprintf("SELECT di.*, us.user_fullname, IF(dp.is_attained = 1, 1, 0) as is_attained, dp.score FROM debates_invitations as di left join users as us on us.user_id = di.user_id left join debates_participants as dp on (dp.user_id = di.user_id AND dp.debate_id = di.debate_id) WHERE di.debate_id = '%s' AND di.sender_user_id = '%s'".$cond, $args['debate_id'], $args['user_id']);
            // Log::debug("SQL : ".$sql);
            // $stmt = $conn->execute($sql);

            // $results = array();

            // while($result = $stmt->fetch("assoc")) {
            //     array_push($results,$result);
            // }

            $followings_ids = [];
            $friends_ids = [];
            $friend_requests_ids = [];
            $friend_requests_sent_ids = [];
            $fellowsArr = [];

            //checking debate invitations users
            $sql=sprintf("SELECT following_id FROM followings WHERE user_id = '%s'", $args['user_id']);
            Log::debug("SQL : ".$sql);
            $followingsStmt = $conn->execute($sql);

            while($result = $followingsStmt->fetch("assoc")) {
                $followings_ids[] = $result['following_id'];
            }
            //checking debate invitations users
            $sql=sprintf('SELECT users.user_id FROM friends INNER JOIN users ON (friends.user_one_id = users.user_id AND friends.user_one_id != %1$s) OR (friends.user_two_id = users.user_id AND friends.user_two_id != %1$s) WHERE status = 1 AND (user_one_id = %1$s OR user_two_id = %1$s)', $args['user_id']);
            Log::debug("SQL : ".$sql);
            $friendsStmt = $conn->execute($sql);

            while($result = $friendsStmt->fetch("assoc")) {
                $friends_ids[] = $result['user_id'];
            }
            //checking debate invitations users
            $sql=sprintf('SELECT user_one_id FROM friends WHERE status = 0 AND user_two_id = %s', $args['user_id']);
            Log::debug("SQL : ".$sql);
            $friend_requestsStmt = $conn->execute($sql);

            while($result = $friend_requestsStmt->fetch("assoc")) {
                $friend_requests_ids[] = $result['user_one_id'];
            }

            //checking debate friend_requests_sent users
            $sql=sprintf('SELECT user_one_id FROM friends WHERE status = 0 AND user_two_id = %s', $args['user_id']);
            Log::debug("SQL : ".$sql);
            $friend_requests_sentStmt = $conn->execute($sql);

            while($result = $friend_requests_sentStmt->fetch("assoc")) {
                $friend_requests_sent_ids[] = $result['user_one_id'];
            }

            // $sql=sprintf("SELECT di.*, us.user_fullname, IF(dp.is_attained = 1, 1, 0) as is_attained, dp.score FROM debates_invitations as di left join users as us on us.user_id = di.user_id left join debates_participants as dp on (dp.user_id = di.user_id AND dp.debate_id = di.debate_id) WHERE di.debate_id = '%s' AND di.sender_user_id = '%s'".$cond, $args['debate_id'], $args['user_id']);

            // $sql=sprintf("SELECT di.user_id, us.user_work,us.user_work_title,us.user_work_place, us.user_name, us.user_fullname,us.user_firstname,user_lastname, us.user_gender, us.user_picture, dp.is_attained, dp.score FROM debates_invitations as di left join users as us on us.user_id = di.user_id left join debates_participants as dp on (dp.user_id = di.user_id AND dp.debate_id = di.debate_id) WHERE di.debate_id = '%s' AND di.sender_user_id = '%s'".$cond, $args['debate_id'], $args['user_id']);

            $sql="SELECT users.user_id, users.user_work, users.user_work_title, users.user_work_place, users.user_name, users.user_fullname, users.user_firstname,user_lastname, users.user_gender, users.user_picture, dp.is_attained, dp.score FROM friends INNER JOIN users ON ((friends.user_one_id = users.user_id AND friends.user_one_id != $args[user_id]) OR (friends.user_two_id = users.user_id AND friends.user_two_id != $args[user_id])) left join debates_participants as dp on dp.id =(SELECT id FROM debates_participants WHERE debate_id = '$args[debate_id]' AND user_id = users.user_id limit 0, 1) WHERE friends.status = 1 AND (user_one_id = $args[user_id] OR user_two_id = $args[user_id]) $cond";
            
            Log::debug("SQL : ".$sql);
            // echo $sql; exit;
            $fellowsStmt = $conn->execute($sql);

            while($result = $fellowsStmt->fetch("assoc")) {
                $result['user_picture'] = $this->getImgFullPath($result['user_picture']);

                $result['connection'] = $this->connection($args['user_id'], $result['user_id'], $followings_ids, $friends_ids, $friend_requests_ids, $friend_requests_sent_ids);
                $result['is_friend'] = in_array($args['user_id'], $friends_ids)?1:0;
                $result['is_following'] = in_array($args['user_id'], $followings_ids)?1:0;
                $result['is_attained'] = $result['is_attained']?1:0;
                $result['score'] = $result['score']?$result['score']:0;
                $fellowsArr[] = $result;
            }

            Log::debug("Ended ...getDebateInvitedUserList Dao");
            
            return $fellowsArr;
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }
    
    private function connection($logged_in_user_id, $user_id, $followings_ids = [], $friends_ids = [], $friend_requests_ids = [], $friend_requests_sent_ids = [])
    {
        /* check if the viewer is the target */
        if ($user_id == $logged_in_user_id) {
            return "me";
        }
        // Arnab mohanty:: on 2nd Jan 2019
        if (in_array($user_id, $followings_ids)) {
            /* the viewer follow the target */
            return "followed";
        }
        /* check if the viewer & the target are friends */
        if (in_array($user_id, $friends_ids)) {
            return "remove";
        }
        /* check if the target sent a request to the viewer */
        if (in_array($user_id, $friend_requests_ids)) {
            return "request";
        }
        /* check if the viewer sent a request to the target */
        if (in_array($user_id, $friend_requests_sent_ids)) {
            return "cancel";
        }
        
        
        /* there is no relation between the viewer & the target */
        return "add";
        
    }


    //updating user debate Pros And Cons Followers
    public function addRemoveDebateProsAndConsFollowers($args, $date)
    {

        Log::debug("Started ...addRemoveDebateProsAndConsFollowers Dao");
        try{
            $conn = ConnectionManager::get('default');
             $response = array("valid" => false, "msg"=>"This debate is not active till now.");

            //checking discussion
            $sql=sprintf("SELECT * FROM debates_discussions WHERE id = '%s' limit 0, 1", $args['discussion_id']);
            Log::debug("SQL : ".$sql);
            $stmt = $conn->execute($sql);
            $discussionData = $stmt->fetch("assoc");
            if(empty($discussionData))
                return array("valid" => false, "msg"=>"Invalid discussion.");
            // echo $discussionData['debate_id']; exit;
            //checking debate live duration
            $sql=sprintf("SELECT * FROM debates WHERE id = '%s' AND start_time <= '%s'", $discussionData['debate_id'], $date);
            Log::debug("SQL : ".$sql);
            $stmt = $conn->execute($sql);
            $debateData = $stmt->fetch("assoc");

            if($debateData){

                //checking debate already following
                $sql=sprintf("SELECT * FROM debates_discussion_followers WHERE discussion_id = '%s' AND user_id = '%s'", $args['discussion_id'], $args['user_id']);
                Log::debug("SQL : ".$sql);
                $stmt = $conn->execute($sql);
                $followingData = $stmt->fetch("assoc");

                if($args['status'] == 2){
                    if(!empty($followingData)){
                        //deleting participation of user
                        $sql=sprintf("DELETE FROM `debates_discussion_followers` WHERE discussion_id = '%s' AND user_id = '%s'", $args['discussion_id'], $args['user_id']);
                        Log::debug("SQL : ".$sql);
                        if($conn->execute($sql)){
                            $response = array("valid" => true, "msg"=>"Following removed");
                        }else
                            $response["msg"] = "Something went wrong.";
                    }else
                        $response["msg"] = "Not following.";
                    
                }else if($args['status'] == 1){
                    if(empty($followingData)){
                        //inserting participation of user
                        $sql=sprintf("INSERT INTO `debates_discussion_followers`(`debate_id`, `user_id`, `discussion_id`) VALUES ('%s', '%s', '%s')", $discussionData['debate_id'], $args['user_id'], $args['discussion_id']);
                        Log::debug("SQL : ".$sql);
                        if($conn->execute($sql)){
                            $response = array("valid" => true, "msg"=>"Following successfully");
                        }else
                            $response["msg"] = "Something went wrong.";
                    }else
                        $response["msg"] = "Already following.";

                    
                }
                Log::debug("SQL : ".$sql);
                $conn->execute($sql);
            }

           
            Log::debug("Ended ...addRemoveDebateProsAndConsFollowers Dao");
            
            return $response;
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }

    //updating user debate Pros And Cons Ratings
    public function addDebateProsAndConsRating($args, $date)
    {

        Log::debug("Started ...addDebateProsAndConsRating Dao");
        try{
            $conn = ConnectionManager::get('default');
             $response = array("valid" => false, "msg"=>"This debate is not active till now.");

            //checking discussion
            $sql=sprintf("SELECT * FROM debates_discussions WHERE id = '%s' limit 0, 1", $args['discussion_id']);
            Log::debug("SQL : ".$sql);
            $stmt = $conn->execute($sql);
            $discussionData = $stmt->fetch("assoc");
            if(empty($discussionData))
                return array("valid" => false, "msg"=>"Invalid discussion.");
            // echo $discussionData['debate_id']; exit;
   
            //checking debate already rated
            $sql=sprintf("SELECT * FROM debates_discussions_rating WHERE debate_id = '%s' AND discussion_id = '%s' AND user_id = '%s'",$discussionData['debate_id'], $args['discussion_id'], $args['user_id']);
            Log::debug("SQL : ".$sql);
            $stmt = $conn->execute($sql);
            $ratingData = $stmt->fetch("assoc");
            if(empty($ratingData)){
                //inserting rating of user
                $sql=sprintf("INSERT INTO `debates_discussions_rating`(`debate_id`, `user_id`, `discussion_id`, `rating`) VALUES ('%s', '%s', '%s', '%s')", $discussionData['debate_id'], $args['user_id'], $args['discussion_id'], $args['rating']);$this->giveVotePoints('discussion_vote', $discussionData['user_id'], $args['rating']);
                Log::debug("SQL : ".$sql);
                if($conn->execute($sql)){
                    $response = array("valid" => true, "msg"=>"Rating successfull.");
                }else
                    $response["msg"] = "Something went wrong.";
            }else{
                //inserting rating of user
                $sql=sprintf("UPDATE `debates_discussions_rating` set rating = '%s' WHERE debate_id = '%s' AND discussion_id = '%s' AND user_id = '%s'", $args['rating'], $discussionData['debate_id'], $args['discussion_id'], $args['user_id']);
                Log::debug("SQL : ".$sql);
                if($conn->execute($sql)){
                    $response = array("valid" => true, "msg"=>"Rating updated successfull.");
                }else
                    $response["msg"] = "Something went wrong.";

            }

                
            if($response['valid']){
                $sql=sprintf("SELECT avg(rating) as rating FROM debates_discussions_rating WHERE debate_id = '%s' AND discussion_id = '%s' AND user_id = '%s' limit 1",$discussionData['debate_id'], $args['discussion_id'], $args['user_id']);
                Log::debug("SQL : ".$sql);
                $stmt = $conn->execute($sql);
                $ratingData = $stmt->fetch("assoc");
                if(!empty($ratingData) && $ratingData['rating'] > 0){
                    $sql=sprintf("UPDATE debates_discussions SET rating = '%s' WHERE id = '%s'", $ratingData['rating'], $args['discussion_id']);
                    Log::debug("SQL : ".$sql);
                    $conn->execute($sql);
                }
            }
            
            Log::debug("SQL : ".$sql);
            $conn->execute($sql);
            

           
            Log::debug("Ended ...addDebateProsAndConsRating Dao");
            
            return $response;
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }

    /**
     * Get Debates pro cons
     */
    public function getDebateProsAndCons($args, $date)
    {
        Log::debug("Started ...getDebateProsAndCons Dao");
        $codes = new Codes;

        try{

            $conn = ConnectionManager::get('default');
            $cond = "";
            // $having = "";
            $orderByArr = [0=> 'dd.id', 1=> 'dd.rating', 2=> 'dd.id', 3=> 'dd.id' ];
            $args['orderBy'] = (isset($args['orderBy']) && isset($orderByArr[$args['orderBy']]))?$args['orderBy']:0;
            $args['orderDir'] = (@$args['orderDir'] == 'asc')?'asc':'desc';
            $args['is_pro'] = isset($args['is_pro'])?$args['is_pro']:0;
            
            //checking keys for pagination
            $args['page'] = (isset($args['page']) && $args['page'] > 1)?$args['page']:1;
            $args['pageSize'] = (isset($args['pageSize']) && $args['pageSize'] > 0)?$args['pageSize']:10;

            // $cond .= ($having)?" HAVING ( ". $having." ) ":'';
            $cond .= " order by ".$orderByArr[$args['orderBy']]." ".$args['orderDir']." limit ".(($args['page']- 1) * $args['pageSize']).", ".$args['pageSize'];

            // condition for query text
            if (isset($args['parent_id']) && !empty($args['parent_id'])) {
                //debate query
                $sql=sprintf("SELECT dd.*, us.user_name, us.user_fullname, us.user_firstname,user_lastname, us.user_gender, us.user_picture, (SELECT id FROM debates_discussion_followers where discussion_id= dd.id AND user_id = '%s' limit 0,1) as following_id,
                (SELECT count(*) as prosCount FROM debates_discussions as sdd WHERE sdd.is_pro  = 1 AND sdd.parent_id = dd.id) as subProsCount,
                (SELECT count(*) as consCount FROM debates_discussions as sdd WHERE sdd.is_pro  = 0 AND sdd.parent_id = dd.id) as subConsCount FROM debates_discussions AS dd INNER JOIN (SELECT id FROM debates_discussions WHERE parent_id = '%s') AS dd2 ON (dd2.id = dd.parent_id OR dd.parent_id = '%s')  left join users as us on us.user_id = dd.user_id WHERE dd.is_pro = '%s'  AND dd.debate_id = '%s' GROUP BY dd.id $cond", $args['user_id'], $args['parent_id'], $args['parent_id'], $args['is_pro'], $args['debate_id'] );
            }else{
                //debate query
                $sql=sprintf("SELECT dd.*, us.user_name, us.user_fullname, us.user_firstname,user_lastname, us.user_gender, us.user_picture, (SELECT id FROM debates_discussion_followers where discussion_id= dd.id AND user_id = '%s' limit 0,1) as following_id,
                (SELECT count(*) as prosCount FROM debates_discussions as sdd WHERE sdd.is_pro  = 1 AND sdd.parent_id = dd.id) as subProsCount,
                (SELECT count(*) as consCount FROM debates_discussions as sdd WHERE sdd.is_pro  = 0 AND sdd.parent_id = dd.id) as subConsCount FROM debates_discussions as dd left join users as us on us.user_id = dd.user_id WHERE dd.is_pro = '%s' AND dd.debate_id = '%s' $cond", $args['user_id'], $args['is_pro'], $args['debate_id']);
            }
            Log::debug("SQL : ".$sql);
            // echo $sql; exit;
            $stmt = $conn->execute($sql);
            
            $proConsData = array();
            $parentData = null;
            $targetData = null;

            while($result = $stmt->fetch("assoc")) {
                $result['user_picture'] = $this->getImgFullPath($result['user_picture']);
                $result['is_following'] = $result['following_id']?1:0;

                array_push($proConsData,$result);
            }

            //checking parent data
            if (isset($args['parent_id']) && !empty($args['parent_id'])) {
                //debate query
                $sql=sprintf("SELECT dd.*, us.user_name, us.user_fullname, us.user_firstname,user_lastname, us.user_gender, us.user_picture, (SELECT id FROM debates_discussion_followers where discussion_id= dd.id AND user_id = '%s' limit 0,1) as following_id FROM debates_discussions as dd left join users as us on us.user_id = dd.user_id WHERE parent_id='%s'", $args['user_id'], $args['parent_id']);
                Log::debug("SQL : ".$sql);
                $stmt = $conn->execute($sql);
                $parentData = $stmt->fetch("assoc");
                if(isset($parentData['user_picture'])){
                    $parentData['user_picture'] = $this->getImgFullPath($parentData['user_picture']);
                    $parentData['is_following'] = $parentData['following_id']?1:0;
                }
            }

            //checking target data
            if (isset($args['target_id']) && !empty($args['target_id'])) {
                //debate query
                $sql=sprintf("SELECT dd.*, us.user_name, us.user_fullname, us.user_firstname,user_lastname, us.user_gender, us.user_picture, (SELECT id FROM debates_discussion_followers where discussion_id= dd.id AND user_id = '%s' limit 0,1) as following_id FROM debates_discussions as dd left join users as us on us.user_id = dd.user_id WHERE id='%s'", $args['user_id'], $args['target_id']);
                Log::debug("SQL : ".$sql);
                $stmt = $conn->execute($sql);
                $targetData = $stmt->fetch("assoc");
                //return $sql;
                if(isset($targetData) and $targetData != false){
                    $targetData['user_picture'] = $this->getImgFullPath($targetData['user_picture']);
                    $targetData['is_following'] = isset($targetData['following_id'])?1:0;
                }
            }
           
            Log::debug("Ended ...getDebateProsAndCons Dao");

            return ["listing" => $proConsData, "parent"=> $parentData?$parentData:null, "target"=> $targetData?$targetData:null];
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }


    /**
     * Get Debates pro cons by id
     */
    public function getDebateProsAndConsById($discussion_id, $user_id = 0)
    {
        Log::debug("Started ...getDebateProsAndConsById Dao");
        $codes = new Codes;

        try{

            $conn = ConnectionManager::get('default');

            //debate query
            $sql=sprintf("SELECT dd.*, us.user_name, us.user_fullname, us.user_firstname,user_lastname, us.user_gender, us.user_picture, (SELECT id FROM debates_discussion_followers where discussion_id= dd.id AND user_id = '%s' limit 0,1) as following_id,
            (SELECT count(*) as prosCount FROM debates_discussions as sdd WHERE sdd.is_pro  = 1 AND sdd.parent_id = dd.id) as subProsCount,
            (SELECT count(*) as consCount FROM debates_discussions as sdd WHERE sdd.is_pro  = 0 AND sdd.parent_id = dd.id) as subConsCount FROM debates_discussions as dd left join users as us on us.user_id = dd.user_id WHERE dd.id = '%s' ",$user_id, $discussion_id);
            
            Log::debug("SQL : ".$sql);
            // echo $sql; exit;
            $stmt = $conn->execute($sql);
            $result = $stmt->fetch("assoc");
            if($result){
                $result['user_picture'] = $this->getImgFullPath($result['user_picture']);
                $result['is_following'] = $result['following_id']?1:0;
            }

           
            Log::debug("Ended ...getDebateProsAndConsById Dao");

            return $result;
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }




}