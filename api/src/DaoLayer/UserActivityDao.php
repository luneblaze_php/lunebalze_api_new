<?php

namespace App\DaoLayer;

use App\Utils\Codes;
use Cake\Core\Exception\Exception;
use Cake\Datasource\ConnectionManager;
use Cake\Log\Log;

class UserActivityDao
{

	/**
	 * Update User Activity By Post Id and User Id
	 */
	public function updateUserActivityByPostIdAndUserId($date, $commentId, $postId, $userId)
	{
		Log::debug("Started ...updateUserActivityByPostIdAndUserId Dao : Action : article_comment, time : ".$date.", comment_id : ".$commentId.", post_id : ".$postId.", User Id : ".$userId);

		try{
			
			$conn = ConnectionManager::get('default');

			$sql=sprintf("update user_activity set action='article_comment',time=%s,comment_id=%s where post_id=%s and user_id=%s", $date, $commentId, $postId, $userId);

			Log::debug("SQL : ".$sql);

			$conn->execute($sql);

			Log::debug("Ended ...updateUserActivityByPostIdAndUserId Dao");

		}catch(\Exception $e){
			Log::debug($e);
			throw new Exception($e);
		}
	}


	public function insertUserActivity(){
		$conn = ConnectionManager::get('default');

		$check=$conn->execute(sprintf("select * from user_activity where post_id=%s and user_id=%s",$comment['node_id'], $comment['user_id']));

		$stmt = $conn->execute($sql);
      	$res = $stmt->fetch();

        if(isset($res[0])){
        	$conn->execute(sprintf("insert into user_activity set post_id=%s,post_type=%s,user_id=%s,action='comment',time=%s,comment_id=%s", $comment['node_id'], $post['post_type'], $comment['user_id'], date("Y-m-d H:i:s"), $comment['comment_id'] ));
        }
        else {
			$conn->execute(sprintf("update user_activity set action='comment',time=%s,comment_id=%s where post_id=%s and user_id=%s", date("Y-m-d H:i:s"), $comment['comment_id'], $comment['node_id'], $comment['user_id'] ));
        }
	}
	
}