<?php

namespace App\DaoLayer;

use App\Utils\Codes;
use App\Utils\DateUtils;
use Cake\Core\Exception\Exception;
use Cake\Datasource\ConnectionManager;
use Cake\Log\Log;
use App\DaoLayer\PostsDao;
use App\Utils\DataValidation;

use App\Model\UserModel;

use App\DaoLayer\NotificationsDao;

class CampusDetailDao
{
    /**
     * 
     */
    public function getCampusDetailByScheduleId($scheduleId, $offset)
    {
        Log::debug("Started ...getCampusDetailByScheduleId Dao : Schedule Id : ".$scheduleId);

        try{

            $codes = new Codes;
            
            $conn = ConnectionManager::get('default');

            $sql=sprintf("SELECT * FROM `campus_detail` WHERE `scheduleid`='%s' order by id desc limit %s ,%s",$scheduleId,$offset,$codes->MAX_RESULTS);          

            Log::debug("SQL : ".$sql);

            $stmt = $conn->execute($sql);

            $results = array();

            while($result = $stmt->fetch("assoc"))
                array_push($results,$result);

            Log::debug("Ended ...getCampusDetailByScheduleId Dao");

            return $results;
            
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }

    }

    public function getCampusDetailByOrganizationId($userid ,$organizationId, $offset , $venueId , $clusterId){
      
        
        Log::debug("Started ...getCampusDetailByOrganizationId Dao : organization Id : ".$organizationId);

        try{

            $codes = new Codes;
            $pd = new PostsDao;
            $conn = ConnectionManager::get('default');
            if($venueId == 0 && $clusterId == 0){
                $sql=sprintf("SELECT * FROM `schedule_campus` WHERE `organization`=%s order by added_on desc limit %s ,%s",$organizationId,$offset * $codes->MAX_RESULTS,$codes->MAX_RESULTS);  
            }elseif($organizationId == 0 && $clusterId == 0){
                $sql=sprintf("SELECT * FROM `schedule_campus` WHERE `college`=%s order by added_on desc limit %s ,%s",$venueId,$offset * $codes->MAX_RESULTS,$codes->MAX_RESULTS); 
            }else{
                $cluster = $conn->execute("SELECT * from cluster where id = $clusterId ");
                $c = $cluster->fetch('assoc');
                $ccsv = $c['college_ids'];
                $sql=sprintf("SELECT * FROM `schedule_campus` WHERE FIND_IN_SET(college , '%s') order by added_on desc limit %s ,%s",$ccsv,$offset * $codes->MAX_RESULTS,$codes->MAX_RESULTS); 
            }


            Log::debug("SQL : ".$sql);

            $stmt = $conn->execute($sql);
            $count = $stmt->count();
           

            $data = array();

            if($count > 0){
                while($res = $stmt->fetch('assoc')){
                    $res['i_join'] = 0;
                    $res['campus_detail']=[];
                    $sql = sprintf("SELECT * FROM `campus_detail` WHERE `scheduleid`=%s ",$res['scheduleid']);
                    $st = $conn->execute($sql);
                    if($r = $st->fetch('assoc')){
                        $res['campus_detail'] = $r;
                    }
                    $sql = sprintf("SELECT * FROM `hiring` WHERE `scheduleid`=%s and user_id = %s ",$res['scheduleid'], $userid);
                    $st = $conn->execute($sql);
                    if($r = $st->fetch('assoc')){
                        $res['i_join'] = $r['status'];
                    }
                    
                    $res['eligible'] = intval($res['eligible']);
                    //interest
                    if ($res['interests'] != '') {
                        $interest = explode(',', $res['interests']);
                        foreach ($interest as $ik => $ival) {

                            $qin = $conn->execute(sprintf("select * from `interest_mst` where interest_id=%s", $ival));
                            $rinn = $qin->fetch("assoc");
                            $rinn['image'] = $codes->SYSTEM_URL  . '/content/uploads/' . $rinn['image'];
                            //$rinn['interest_name'] = $rinn['text'];
                            //unset($rinn['text']);
                            unset($rinn['description']);
                            $temp[] = $rinn;
                            $res['interests'] = $temp;
                        }
                    } else {
                        $res['interests'] = [];
                    }  

                    //categories
                    if ($res['categories'] != '') {
                        $categories = explode(',', $res['categories']);
                        foreach ($categories as $ik => $ival) {
                            $qin = $conn->execute(sprintf("select * from `category_mst` where category_id=%s", $ival));
                            $rcat =  $qin->fetch("assoc");
                            $rcat['image'] = $codes->SYSTEM_URL . '/content/uploads/' . $rcat['image'];
                            $rcat['category_name'] = $rcat['text'];
                            unset($rcat['text']);
                            unset($rcat['description']);
                            $row_category[] = $rcat;
                            $res['categories'] = $row_category;
                        }
                    } else {
                        $res['categories'] = [];
                    }
                    $dat = $pd->userData($userid , $res['organization'] , 'organization' , 0);
                    $res['organizaitonData'] = isset($dat[0])? $dat[0] : null;
                    //$res['organizaitonData'] = $dat;
                    $dat = $pd->userData($userid , $res['college'] , 'venue' , 0);
                    $res['collegeData'] = isset($dat[0]) ? $dat[0] : null;

                    


                    array_push($data, $res);
                }

            }    
            Log::debug("Ended ...getCampusDetailByOrganizationId Dao");
           // print_r($data);

            $response['message'] = 'Success';
            $response['data'] = $data;
            $response['status'] = 1;

            return $data;
            
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }


    public function userCampusInterest($userId, $campusscheduleId, $status = null)
    {
        Log::debug("Started ...userCampusInterest Dao : userId  : ".$userId.", campusscheduleId : ". $campusscheduleId);

        try{

            
            $codes = new Codes;
            $dateUtils = new DateUtils;
            
            $conn = ConnectionManager::get('default');

            $sql=sprintf("SELECT * FROM `user_campus_interest` WHERE `user_id`='%s' AND `campusdriveid`='%s'",$userId,$campusscheduleId);          
            
            Log::debug("SQL : ".$sql);

            $stmt = $conn->execute($sql);

            $count=$stmt->count();
            
            if($count==0)
            {

                $stmt = $conn->execute(sprintf("insert into user_campus_interest set user_id=%s,campusdriveid=%s,status=%s,ondate='%s'", $userId, $campusscheduleId,$status, $dateUtils->getCurrentDate()));
                
                $sql="SELECT LAST_INSERT_ID()";
                $stmt = $conn->execute($sql);
                $res = $stmt->fetch();
                
                $id = $res[0];
            } else {
                $res = $stmt->fetch("assoc");
                $id = $res['id'];
                $qwe = $conn->execute(sprintf("update user_campus_interest set user_id=%s,campusdriveid=%s,status=%s where id=%s", $userId, $campusscheduleId, $status, $id));
            }

        
            Log::debug("Ended ...getCampusDetailByScheduleId Dao");

            return $id;
            
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }

    }

    public function sheduleCampus($input, $date)
    {

        try{


            $codes = new Codes;
            $dateUtils = new DateUtils;
            $dataValidation = new DataValidation;
            $conn = ConnectionManager::get('default');


            $qwe = $conn->execute(sprintf("select * from organization where id=%s", $input['organization_id']));
            $reso = $qwe->fetch("assoc");
            if ($reso['IsActive'] == 0) {
                throw new Exception('Organization is deactivated, Please contact to Administrative');
            }
            if ($reso['IsBlock'] == 1) {
                throw new Exception('Organization is blocked, Please contact to Administrative');
            }

            
            $usersDao = new UsersDao;
            $input['session_roles'] = 'Both';
            $venues = $usersDao->getCollege($input);
            $totalstd = 0;
            $userids = [];
            foreach($venues as $val){
                $totalstd = $totalstd + $val['noofstudent'] ;
                foreach ($val['user_ids'] as $key) {
                    # code...
                    array_push($userids, $key);
                }
            }
            $usridar = $userids;
            $userids = implode(",", $userids);
            extract($input);
            /*
            user_id
            organization_id
            venuetype
            companyaddress
            campusdate
            job_position = array()
            job_description = array()
            noofvacancies
            categories = array()
            dependency = array()
            and_or = array()
            jobtype
            candidates_allowed
            joblocation
            venue_address
            clusterid
            studyfeilds
            course
            skills
            yearofpassing
            percentage
            pointsonluneblaze
            stipend
            duration
            assessment_req
            ppo
            ctcmin
            ctcmax
            lattitude
            longitude
            interests = array()
            */

            $bal = 0;
            $cost = 0;
            if($noofvacancies > $totalstd){
                $response['msg'] = 'vacancies exceeds eligible students';
                //$response['data'] = $userids;
                $response['status'] = 0;  
                //$response['userids'] = $venues ;
                return $response;
            }else{
                $sql = sprintf("SELECT * FROM `plan_credits` where orgnazation_id = %s" , $organization_id );
                $st = $conn->execute($sql);
                if($re = $st->fetch('assoc')){
                    $bal = $re['balance'];
                    if($jobtype == 'internship'){
                        $cost = $noofvacancies * $codes->intern_credits;
                    }else{
                        $cost = $noofvacancies * $codes->job_credits;
                    }
                    if($re['balance'] < $cost){
                        $response['msg'] = 'low balance';
                        //$response['data'] = $userids;
                        $response['status'] = 0;  
                        //$response['userids'] = $venues ;
                    }
                }else{
                    $response['msg'] = 'org plan not found';
                    //$response['data'] = $userids;
                    $response['status'] = 0;  
                    //$response['userids'] = $venues ;
                }
            }


            if(!isset($input['clusterid'])){
                $clusterid = 0;
            }
            if(!isset($input['joblocation'])){
                $joblocation = '';
            }
            if(!isset($input['studyfeilds'])){
                $studyfeilds = '';
            }
            if(!isset($input['course'])){
                $course = '';
            }
            if(!isset($input['skills'])){
                $skills = '';
            }
            if( !isset($input['yearofpassing']) || $dataValidation->isEmpty($input['yearofpassing'])) {
                $yearofpassing = 0;
            }
            if(!isset($input['percentage']) || $dataValidation->isEmpty($input['percentage'])){
                $percentage = 0;
            }
            if(!isset($input['pointsonluneblaze']) || $dataValidation->isEmpty($input['pointsonluneblaze'])){
                $pointsonluneblaze = 0;
            }
            if(!isset($input['interests']) || $input['interests'] == [] ){
                $interests = array();
            }else{
               
            }
            if (!isset($input['order_id'])) {
                $order_id = '';
            }
            if (!isset($input['lattitude']) || !isset($input['longitude']) || $dataValidation->isEmpty($input['lattitude'])  || $dataValidation->isEmpty($input['longitude'])) {
                $lattitude = 0;
                $longitude = 0;
            }
            if (!isset($input['paymentid'])) {
                $paymentid = '';
            }
            if (!isset($input['transaction_id'])) {
                $transaction_id = '';
            }
            $college = 0;

            $categories = implode(",", $categories);
            $interests = implode(",", $interests);
            $qwe =  $conn->execute(sprintf("insert into schedule_campus set organization=%s,college=%s,date='%s',userid=%s,added_on='%s',order_id='%s',payment_id='%s',transaction_id='%s',interests='%s',categories='%s',companyaddress='%s',eligible='%s',eigible_users='%s' , venue_address = '%s', cluster_id = %s, lattitude=%s, longitude = %s", $organization_id, $college, $campusdate, $user_id, $date, $order_id, $paymentid, $transaction_id, $interests, $categories, $companyaddress, $totalstd, $userids , $venue_address, $clusterid , $lattitude , $longitude));
            //$id = $db->insert_id;
                
            $sql="SELECT LAST_INSERT_ID()";
        
            $stmt = $conn->execute($sql);
                
            $res = $stmt->fetch();
            $id = $res[0];
    
            foreach ($job_position as $jk => $jval) {
                if ($jval != '') {
                    if($jobtype == 'internship'){
                    $conn->execute(sprintf("insert into campus_detail set scheduleid=%s,jobname='%s',description='%s',noofvacancies='%s', job_type='%s' , candidates_allowed= %s , job_location='%s', study_feilds = '%s',course = '%s', skills='%s', yearofpassing = %s, percentage = %s, pointsonluneblaze = %s, stipend = %s, duration = %s, assessment_req= %s, ppo = %s, interests = '%s', eligible = %s, eligible_users = '%s' ,ctc_min = %s, ctc_max = %s  ", $id, $jval, $job_description[$jk], $noofvacancies , $jobtype , $candidates_allowed , $joblocation, $studyfeilds , $course , $skills , $yearofpassing , $percentage , $pointsonluneblaze , $stipend , $duration , $assessment_req ,$ppo , $interests , $totalstd, $userids , $ctcmin , $ctcmax  ));
                }else{//if job
                    $conn->execute(sprintf("insert into campus_detail set scheduleid=%s,jobname='%s',description='%s',noofvacancies='%s', job_type='%s' , candidates_allowed= %s , job_location='%s', study_feilds = '%s',course = '%s', skills='%s', yearofpassing = %s, percentage = %s, pointsonluneblaze = %s, ctc_min = %s, ctc_max = %s , interests = '%s', eligible = %s, eligible_users = '%s'  ", $id, $jval, $job_description[$jk], $noofvacancies , $jobtype , $candidates_allowed , $joblocation, $studyfeilds , $course , $skills , $yearofpassing , $percentage , $pointsonluneblaze , $ctcmin , $ctcmax , $interests , $totalstd, $userids ));
                }
            }
        }
            //subtracting credits
        $v = $bal - $cost ;
        $conn->execute(sprintf("update plan_credits set balance = %s where orgnazation_id = %s", $v , $organization_id ));
            





            $message = [
                'message' => '',
                'session_data' => ''

            ];

            $type = '';

            $um = new UserModel();
            $um->massNotify($usridar , $message , $type);
            
            $response['message'] = 'Success';
            $response['data'] = $id;
            $response['status'] = 1;  
            $response['userids'] = $venues ;

            return $response;



            
        }catch(\Exception $e){
         // print_r($e);
            Log::debug($e);
            throw new Exception($e);
        }

    }

    //adding dao 4 rhythm
    public function getDriveDetail($userid ,$scheduleid ){
      
        
        Log::debug("Started ...getDriveDetail Dao : organization Id : ".$scheduleid);

        try{

            $codes = new Codes;
            
            $conn = ConnectionManager::get('default');

            $sql=sprintf("SELECT * FROM `schedule_campus` WHERE `scheduleid`=%s",$scheduleid);          

            Log::debug("SQL : ".$sql);

            $stmt = $conn->execute($sql);
            $count = $stmt->count();
           

            $data = array();
            $hired = 0;
            $present = 0;
            if($count > 0){
                while($res = $stmt->fetch('assoc')){
                    $res['i_join'] = 0;
                    $res['campus_detail']=[];
                    $sql = sprintf("SELECT * FROM `campus_detail` WHERE `scheduleid`=%s ",$res['scheduleid']);
                    $st = $conn->execute($sql);
                    if($r = $st->fetch('assoc')){
                        $sql = sprintf("SELECT * FROM `hiring` WHERE `scheduleid`=%s and status = 3 ",$res['scheduleid']);
                        $sta = $conn->execute($sql);
                        while($h = $sta->fetch('assoc')){
                            $hired = $hired +1;
                        }
                        $sql = sprintf("SELECT * FROM `hiring` WHERE `scheduleid`=%s and status = 2 ",$res['scheduleid']);
                        $sta = $conn->execute($sql);
                        while($h = $sta->fetch('assoc')){
                            $present = $present +1;
                        }
                        $r['hired'] = $hired;
                        $r['present'] = $present;
                        $res['campus_detail'] = $r;
                    }
                    $sql = sprintf("SELECT * FROM `hiring` WHERE `scheduleid`=%s and user_id = %s ",$res['scheduleid'], $userid);
                    $st = $conn->execute($sql);
                    if($r = $st->fetch('assoc')){
                        $res['i_join'] = $r['status'];
                    }
                    
                    $res['eligible'] = intval($res['eligible']);
                    //interest
                    if ($res['interests'] != '') {
                        $interest = explode(',', $res['interests']);
                        foreach ($interest as $ik => $ival) {

                            $qin = $conn->execute(sprintf("select * from `interest_mst` where interest_id=%s", $ival));
                            $rinn = $qin->fetch("assoc");
                            $rinn['image'] = $codes->SYSTEM_URL  . '/content/uploads/' . $rinn['image'];
                            $rinn['interest_name'] = $rinn['text'];
                            unset($rinn['text']);
                            unset($rinn['description']);
                            $temp[] = $rinn;
                            $res['interests'] = $temp;
                        }
                    } else {
                        $res['interests'] = [];
                    }  

                    //categories
                    if ($res['categories'] != '') {
                        $categories = explode(',', $res['categories']);
                        foreach ($categories as $ik => $ival) {
                            $qin = $conn->execute(sprintf("select * from `category_mst` where category_id=%s", $ival));
                            $rcat =  $qin->fetch("assoc");
                            $rcat['image'] = $codes->SYSTEM_URL . '/content/uploads/' . $rcat['image'];
                            $rcat['category_name'] = $rcat['text'];
                            unset($rcat['text']);
                            unset($rcat['description']);
                            $row_category[] = $rcat;
                            $res['categories'] = $row_category;
                        }
                    } else {
                        $res['categories'] = [];
                    }
                    


                    array_push($data, $res);
                }

            }    
            Log::debug("Ended ...getDriveDetail Dao");
           // print_r($data);

            $response['message'] = 'Success';
            $response['data'] = $data;
            $response['status'] = 1;

            return $data;
            
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }

    //adding dao 5 rhythm
    public function getCandidatesList($userid ,$scheduleid ){
      
        
        Log::debug("Started ...getCandidatesList Dao : organization Id : ".$scheduleid);

        try{

            $codes = new Codes;
            
            $conn = ConnectionManager::get('default');

            $sql=sprintf("SELECT * FROM `hiring` WHERE `scheduleid`=%s",$scheduleid);          

            Log::debug("SQL : ".$sql);

            $stmt = $conn->execute($sql);
            $count = $stmt->count();
           

            $data = array();
            //$hired = 0;
            //$present = 0;
            if($count > 0){
                while($res = $stmt->fetch('assoc')){
                    
                    if($res['status'] == 1){
                        $res['status_txt']='Applied';
                    }elseif ($res['status'] == 2) {
                        $res['status_txt'] = 'Present';
                    }else{
                        $res['status_txt'] = 'Hired';
                    }
                    array_push($data, $res);
                }

            }    
            Log::debug("Ended ...getCandidatesList Dao");
           // print_r($data);

            $response['message'] = 'Success';
            $response['data'] = $data;
            $response['status'] = 1;

            return $data;
            
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }

    //adding dao 6 rhythm
    public function updateCandidateStatus($user , $schedule , $target , $status){
        Log::debug("Started ...updateCandidateStatus Dao : organization Id : ".$schedule);

        try{
            $codes = new Codes;
            
            $conn = ConnectionManager::get('default');

            $response = [
                'status' => 0,
                'msg' => 'something went wrong'
            ];

            //return $response;
            $sql = sprintf("select * from schedule_campus where scheduleid = %s and userid = %s",$schedule , $user );
            Log::debug("SQL : ".$sql);

            $stmt = $conn->execute($sql);
            $count = $stmt->count();
            if($count > 0){
                $sql = sprintf("select * from hiring where scheduleid = %s and user_id = %s",$schedule , $target );
                Log::debug("SQL : ".$sql);

                $stmt = $conn->execute($sql);
                if($res = $stmt->fetch('assoc')){
                    $pstatus = $res['status'];
                    if($pstatus == 1){
                        if($status == 2){
                            $sql = sprintf("update hiring set status = 2 where scheduleid = %s and user_id = %s",$schedule , $target );
                            Log::debug("SQL : ".$sql);

                            if($stmt = $conn->execute($sql)){
                                $response = [
                                    'status' => 1,
                                    'msg' => 'status changed to participated successfully'
                                ];
                            }
                        }else{
                            $response['msg']= 'not allowed';
                        }
                    }elseif($pstatus == 2){
                        if($status == 3){
                            $sql = sprintf("update hiring set status = 3 where scheduleid = %s and user_id = %s",$schedule , $target );
                            Log::debug("SQL : ".$sql);

                            if($stmt = $conn->execute($sql)){
                                $response = [
                                    'status' => 1,
                                    'msg' => 'status changed to hired successfully'
                                ];
                                //fcm here
                            }
                        }else{
                            $response['msg']= 'not allowed';
                        }
                    }else{
                        $response['msg'] = 'pstatus problem';
                    }
                }else{
                    $response['msg'] = 'hiring problem';
                }
            }else{
                $response['msg'] = 'count problem';
            }
            return $response;
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }

        //adding dao 7 rhythm
    public function applyCampus($user , $schedule ){
        Log::debug("Started ...applyCampus Dao : organization Id : ".$schedule);

        try{
            $codes = new Codes;
            
            $conn = ConnectionManager::get('default');

            $response = [
                'status' => 0,
                'msg' => 'something went wrong'
            ];

            //return $response;
            $sql = sprintf("select * from schedule_campus where scheduleid = %s",$schedule );
            Log::debug("SQL : ".$sql);

            $stmt = $conn->execute($sql);
            //$count = $stmt->count();
            if($res = $stmt->fetch('assoc')){
                $arr = $res['eigible_users'];
                $arr = explode(',', $arr);
                if(in_array($user, $arr)){
                    $sql = sprintf("select * from hiring where scheduleid = %s",$schedule, $user );
                    Log::debug("SQL : ".$sql);

                    $stmt = $conn->execute($sql);
                    $count = $stmt->count();
                    if($count == 0){
                        $sql = sprintf("insert into hiring set scheduleid = %s , user_id = %s , status = 1 , organization = %s",$schedule, $user, $res['organization']);
                        Log::debug("SQL : ".$sql);

                        if($stmt = $conn->execute($sql)){
                            $response['status'] = 1;
                            $response['msg'] = 'applied successfully';
                        }
                    }else{$response['msg'] = 'already participated problem';}
                }else{$response['msg'] = 'eligibility problem';}
            }else{$response['msg'] = 'schedule problem';}
            return $response;
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }



 //adding dao 8 rhythm
    public function getClusterList($query){
        Log::debug("Started ...getClusterList Dao : organization Id : ".$query);

        try{
            $codes = new Codes;
            
            $conn = ConnectionManager::get('default');

            
            $arr = array();
            //return $response;
            $sql = "select * from cluster where name like '%".$query."%'";
            Log::debug("SQL : ".$sql);

            $stmt = $conn->execute($sql);
            //$count = $stmt->count();
            while($res = $stmt->fetch('assoc')){
                array_push($arr, $res);
            }
            return $arr;
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }


    //adding dao 9 rhythm
    public function getSkillsList($query){
        Log::debug("Started ...getSkillsList Dao : organization Id : ".$query);

        try{
            $codes = new Codes;
            
            $conn = ConnectionManager::get('default');

            
            $arr = array();
            //return $response;
            $sql = "select * from skills where name like '%".$query."%'";
            Log::debug("SQL : ".$sql);

            $stmt = $conn->execute($sql);
            //$count = $stmt->count();
            while($res = $stmt->fetch('assoc')){
                array_push($arr, $res);
            }
            return $arr;
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }

    //adding dao 13 rhythm
    public function getCourseList($query){
        Log::debug("Started ...getCourseList Dao : organization Id : ".$query);

        try{
            $codes = new Codes;
            
            $conn = ConnectionManager::get('default');

            
            $arr = array();
            //return $response;
            $sql = "select * from course where name like '%".$query."%'";
            Log::debug("SQL : ".$sql);

            $stmt = $conn->execute($sql);
            //$count = $stmt->count();
            while($res = $stmt->fetch('assoc')){
                array_push($arr, $res);
            }
            return $arr;
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }



    //adding dao 14 rhythm
    public function getFieldofstudyList($query){
        Log::debug("Started ...getFieldofstudyList Dao : organization Id : ".$query);

        try{
            $codes = new Codes;
            
            $conn = ConnectionManager::get('default');

            
            $arr = array();
            //return $response;
            $sql = "select * from fieldofstudy where name like '%".$query."%'";
            Log::debug("SQL : ".$sql);

            $stmt = $conn->execute($sql);
            //$count = $stmt->count();
            while($res = $stmt->fetch('assoc')){
                array_push($arr, $res);
            }
            return $arr;
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }







    //adding dao 11 rhythm
    public function updateDrive($input){
        Log::debug("Started ...updateDrive Dao : organization Id : ".$input['date']);

        try{
            $codes = new Codes;
            
            $conn = ConnectionManager::get('default');

            extract($input);
            //$temp = $date;
            $arr = array();
            //return $response;
            $sql = sprintf("select * from schedule_campus where scheduleid = %s ", $schedule_id);
            Log::debug("SQL : ".$sql);

            $stmt = $conn->execute($sql);
            if($res = $stmt->fetch('assoc')){
                if($res['userid'] != $user_id){
                    return ['msg'=>'not authorised'];
                }
                $curr_date = date('Y-m-d');
                $date_campus = $res['date'];
                $date_campus = strtotime($date_campus);
                $temp = $date;
                //return $temp;
                //$date = strtotime($date);
                //$date = date('Y-m-d',$date);
                $date_campus = date('Y-m-d',$date_campus);
                //$temp = ''.$date.'';
                //return [$curr_date , $date_campus];
                if($date < $curr_date){
                    return ['msg'=>'cant be done' , 'status'=> false];
                }
                if($date < $date_campus ){
                    //return ['msg' => 'prepone'];
                    //$sql = sprintf("update schedule_campus set date = '%s' and status = 'open' where scheduleid = %s", $date , $schedule_id);
                    $sql = "update schedule_campus set date = ".$date." and status = 'open' where scheduleid = ".$schedule_id."";
                    if($conn->execute($sql)){
                        $s = $conn->execute(sprintf("select date from schedule_campus where scheduleid = %s",$schedule_id));
                        $r = $s->fetch('assoc');
                        return ['msg'=>'success' , 'status'=>true , 'date'=>$r];
                    }
                }else{
                    //return ['msg' => 'postpone'];
                    //$sql = sprintf("update schedule_campus set date = '%s' and status = 'open' where scheduleid = %s", $date , $schedule_id);
                    $sql = "update schedule_campus set date = ".$temp." and status = 'open' where scheduleid = ".$schedule_id."";
                    if($conn->execute($sql)){
                        $s = $conn->execute(sprintf("select date from schedule_campus where scheduleid = %s",$schedule_id));
                        $r = $s->fetch('assoc');
                        return ['msg'=>'success' , 'status'=>true , 'date'=>$r];
                    }
                }

            }else{
                return ['msg'=>'Campus Dosent Exist'];
            }
            
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }




    public function orgClusterData($organizationId)
    {
        Log::debug("Started ...userCampusInterest Dao : userId  : ");

        try{

            
            $codes = new Codes;
            $dateUtils = new DateUtils;
            
            $conn = ConnectionManager::get('default');
            $result = array();
            $sql=sprintf("SELECT * FROM `cluster` WHERE 1");          
            
            Log::debug("SQL : ".$sql);

            $stmt = $conn->execute($sql);
            while($res = $stmt->fetch('assoc')){
                $clid = $res['id'];
                $res['totalDrives'] = 0;
                $res['totalApplied'] = 0;
                $res['totalHired'] = 0;
                $s = $conn->execute("SELECT scheduleid from schedule_campus WHERE cluster_id = $clid and organization = $organizationId ");
                while($r = $s->fetch('assoc')){
                    $res['totalDrives']++;
                    $schid = $r['scheduleid'];
                    $co = $conn->execute("SELECT * from hiring where scheduleid = $schid ");
                    $res['totalApplied'] = $co->count();
                    $co = $conn->execute("SELECT * from hiring where scheduleid = $schid and status  = 3");
                    $res['totalHired'] = $co->count();
                }
                array_push($result , $res);
            }

        
            Log::debug("Ended ...getCampusDetailByScheduleId Dao");

            return $result;
            
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }

    }


    
}