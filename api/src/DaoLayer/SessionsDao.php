<?php

namespace App\DaoLayer;

use App\Utils\Codes;
use Cake\Core\Exception\Exception;
use Cake\Datasource\ConnectionManager;
use Cake\Log\Log;

class SessionsDao
{

	/**
	 * Delete Sessions using sessions_id
	 */
	public function deleteSessionsBySessionsId($sessionsId)
	{
		Log::debug("Started ...deleteSessionsBySessionsId Dao : Sessions Id : ".$sessionsId);

		try{
			
			$conn = ConnectionManager::get('default');

			$sql=sprintf("DELETE FROM sessions WHERE sessions_id = %s", $sessionsId);

			Log::debug("SQL : ".$sql);

			$conn->execute($sql);

			Log::debug("Ended ...deleteSessionsBySessionsId Dao");

		}catch(\Exception $e){
			Log::debug($e);
			throw new Exception($e);
		}
	}

	/**
	 * Delete Sessions Interest using sessions_id
	 */
	public function deleteSessionsInterestBySessionsId($sessionsId)
	{
		Log::debug("Started ...deleteSessionsInterestBySessionsId Dao : Sessions Id : ".$sessionsId);

		try{
			
			$conn = ConnectionManager::get('default');

			$sql=sprintf("DELETE FROM sessions_interest WHERE sessions_id = %s", $sessionsId);

			Log::debug("SQL : ".$sql);

			$conn->execute($sql);

			Log::debug("Ended ...deleteSessionsInterestBySessionsId Dao");

		}catch(\Exception $e){
			Log::debug($e);
			throw new Exception($e);
		}
	}

	/**
	 * Delete Sessions Discussion using sessions_id
	 */
	public function deleteSessionsDiscussionBySessionsId($sessionsId)
	{
		Log::debug("Started ...deleteSessionsDiscussionBySessionsId Dao : Sessions Id : ".$sessionsId);

		try{
			
			$conn = ConnectionManager::get('default');

			$sql=sprintf("DELETE FROM sessions_discussion WHERE sessions_id = %s", $sessionsId);

			Log::debug("SQL : ".$sql);

			$conn->execute($sql);

			Log::debug("Ended ...deleteSessionsDiscussionBySessionsId Dao");

		}catch(\Exception $e){
			Log::debug($e);
			throw new Exception($e);
		}
	}

	/**
	 * Delete Sessions Attends using sessions_id
	 */
	public function deleteSessionsAttendsBySessionsId($sessionsId)
	{
		Log::debug("Started ...deleteSessionsAttendsBySessionsId Dao : Sessions Id : ".$sessionsId);

		try{
			
			$conn = ConnectionManager::get('default');

			$sql=sprintf("DELETE FROM sessions_attends WHERE sessions_id = %s", $sessionsId);

			Log::debug("SQL : ".$sql);

			$conn->execute($sql);

			Log::debug("Ended ...deleteSessionsAttendsBySessionsId Dao");

		}catch(\Exception $e){
			Log::debug($e);
			throw new Exception($e);
		}
	}


		/**
	 * Get Sessions by Session Id
	 */
	public function getSessionsBySessionId($sessionId)
	{
		Log::debug("Started ...getSessionsBySessionId Dao : Session Id : ".$sessionId);

		try{
			
			$conn = ConnectionManager::get('default');

			$sql=sprintf("SELECT created_by,presentors FROM sessions WHERE sessions_id = %s", $sessionId);
		
			Log::debug("SQL : ".$sql);

			$stmt = $conn->execute($sql);

			$result = $stmt->fetch("assoc");

			Log::debug("Ended ...getSessionsBySessionId Dao");

			return $result;

		}catch(\Exception $e){
			Log::debug($e);
			throw new Exception($e);
		}
	}   



}
