<?php

namespace App\DaoLayer;

use App\Utils\Codes;
use Cake\Core\Exception\Exception;
use Cake\Datasource\ConnectionManager;
use Cake\Log\Log;

class NotificationsDao
{


	public function deleteNotification($nid)
	{
		Log::debug("Started ...updateNotifications Dao :");

		try{
			
			$conn = ConnectionManager::get('default');
			$sql=sprintf("SELECT * FROM notifications  WHERE notification_id = %s ", $nid);
			$st = $conn->execute($sql);
			if($a = $st->fetch("assoc")){

			}else{return ["status" => 0, "message" => "notification not found"];}

			$sql=sprintf("DELETE FROM notifications  WHERE notification_id = %s ", $nid);

			Log::debug("SQL : ".$sql);

			if($conn->execute($sql)){
				return ["status" => 1,"message" => "deleted"];
			}else{
				return ["status" => 0,"message" => "something went wrong"];
			}

			Log::debug("Ended ...updateNotifications Dao");

		}catch(\Exception $e){
			Log::debug($e);
			throw new Exception($e);
		}
	}

	/**
	 * Get Users Notifiactions by to_user_id and time
	 */
	public function getUsersNotificationsByToUserIdAndTime($toUserId, $time, $limitStart, $limitEnd)
	{
		Log::debug("Started ...getUsersNotificationsByToUserIdAndTime Dao : To User Id : ".$toUserId.", Time : ".$time.", Limit Start : ".$limitStart.
			", Limit End : ".$limitEnd);

		try{
			
			$conn = ConnectionManager::get('default');

			$sql=sprintf("SELECT notifications.*, users.user_id, users.user_name, users.user_firstname, users.user_lastname,users.user_fullname, users.user_gender, users.user_picture FROM notifications LEFT JOIN users ON notifications.from_user_id = users.user_id WHERE (notifications.to_user_id = %s OR notifications.to_user_id = '0') AND notifications.time > %s ORDER BY notifications.time DESC LIMIT %s, %s", $toUserId, $time, $limitStart, $limitEnd);
		
			Log::debug("SQL : ".$sql);

			$stmt = $conn->execute($sql);

			$result = $stmt->fetch("assoc");

			Log::debug("Ended ...getUsersNotificationsByToUserIdAndTime Dao");

			return $result;

		}catch(\Exception $e){
			Log::debug($e);
			throw new Exception($e);
		}
	}   

	/**
	 * Get Notifiaction by to_user_id and action and node_type and unique_id
	 */
	public function getNotificationByToUserIdAndActionAnsNodeTypeAndUniqueId($toUserId, $action, $nodeType, $uniqueId)
	{
		Log::debug("Started ...getNotificationByToUserIdAndActionAnsNodeTypeAndUniqueId Dao : To User Id : ".$toUserId.", Action : ".$action.", Node Type : ".$nodeType.
			", Unique Id : ".$uniqueId);

		try{
			
			$conn = ConnectionManager::get('default');

			$sql=sprintf("SELECT * FROM notifications WHERE to_user_id = %s and action=%s and node_type=%s and unique_id=%s", $toUserId, $action, $nodeType, $uniqueId);
	
			Log::debug("SQL : ".$sql);

			$stmt = $conn->execute($sql);

			$result = $stmt->fetch("assoc");

			Log::debug("Ended ...getNotificationByToUserIdAndActionAnsNodeTypeAndUniqueId Dao");

			return $result;

		}catch(\Exception $e){
			Log::debug($e);
			throw new Exception($e);
		}
	}   

	/**
	 * Save Notifications
	 */
	public function saveNotifications($toUserId, $userId, $action, $nodeType, $nodeUrl, $notifyId, $replyId, $date, $uniqueId)
	{
		Log::debug("Started ...saveNotifications Dao : To User Id : ".$toUserId.", From User Id : ".$userId.", Action : ".$action.", Node Type : ".$nodeType.", Node Url : ".$nodeUrl.", Notify Id : ".$notifyId.", Reply Id : ".$replyId.", Time : ".$date.", Total User Ids : ".$userId.", Unique Id : ".$uniqueId);

		try{
			
			$conn = ConnectionManager::get('default');

			$sql=sprintf("INSERT INTO notifications (to_user_id, from_user_id, action, node_type, node_url, notify_id, reply_id, time, total_user_ids,unique_id) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s,%s)", $toUserId, $userId, $action, $nodeType, $nodeUrl, $notifyId, $replyId, $date, $userId,$uniqueId);

			Log::debug("SQL : ".$sql);

			$conn->execute($sql);

			Log::debug("Ended ...saveNotifications Dao");

		}catch(\Exception $e){
			Log::debug($e);
			throw new Exception($e);
		}
	}

	public function updateNotifications($fromUserId, $comment_id, $node_url, $notification_id)
	{
		Log::debug("Started ...updateNotifications Dao :");

		try{
			
			$conn = ConnectionManager::get('default');

			$sql=sprintf("UPDATE notifications SET from_user_id = %s, time = %s, seen = '0', notify_id = %s,node_url=%s WHERE notification_id = %s ", $fromUserId, date("Y-m-d H:i:s"), $comment_id , $node_url, $notification_id );

			Log::debug("SQL : ".$sql);

			$conn->execute($sql);

			Log::debug("Ended ...updateNotifications Dao");

		}catch(\Exception $e){
			Log::debug($e);
			throw new Exception($e);
		}
	} 

	public function updateNotificationTotal($totalUserIds, $total, $fromUserId, $notify_id, $node_url,  $notification_id)
	{
		Log::debug("Started ...updateNotifications Dao :");

		try{
			
			$conn = ConnectionManager::get('default');

			$sql=sprintf("UPDATE notifications SET total_user_ids = %s, total = %s, from_user_id = %s, time = %s, seen = '0', notify_id = %s,node_url=%s WHERE notification_id = %s ", $totalUserIds , $total, $fromUserId, date("Y-m-d H:i:s"), $notify_id, $node_url, $notification_id);

			Log::debug("SQL : ".$sql);

			$conn->execute($sql);

			Log::debug("Ended ...updateNotifications Dao");

		}catch(\Exception $e){
			Log::debug($e);
			throw new Exception($e);
		}
	}
	
	public function post_notification($to_user_id, $action, $node_type = '', $node_url = '')
    {
        global $db, $date, $system;
        $codes = new Codes;
        $conn = ConnectionManager::get('default');
        /* if the viewer is the target */
        // if ($this->_data['user_id'] == $to_user_id) {
        //     return;
        // }
        /* get receiver user */
        $get_receiver = $conn->execute(sprintf("SELECT * FROM users WHERE user_id = %s", $to_user_id));
        if ($get_receiver->count() == 0) {
            return;
        }
        /* insert notification */
        $conn->execute(sprintf("INSERT INTO notifications (to_user_id, from_user_id, action, node_type, node_url, time) VALUES (%s, %s, %s, %s, %s, %s)", $to_user_id, $this->_data['user_id'], $action, $node_type, $node_url, $date));
        /* update notifications counter +1 */
        $conn->execute(sprintf("UPDATE users SET user_live_notifications_counter = user_live_notifications_counter + 1 WHERE user_id = %s", $to_user_id));

        // Email Notifications 
        if ($codes->EMAIL_NOTIFICATIONS) {
            /* prepare receiver */
            $receiver = $get_receiver->fetch("assoc");
            /* parse notification */
            switch ($action) {
                case 'friend':
                    if ($codes->EMAIL_FRIEND_NOTIFICATIONS && $receiver['email_friend_requests']) {
                        $notification['url'] = $codes->SYSTEM_URL . '/' . $this->_data['user_name'];
                        $notification['message'] = __("Accepted your friend request");
                    }
                    break;

                case 'like':
                    if ($codes->EMAIL_POST_LIKES && $receiver['email_post_likes']) {
                        if ($node_type == "post") {
                            $notification['url'] = $codes->SYSTEM_URL . '/posts/' . $node_url;
                            $notification['message'] = __("Likes your post");
                        } elseif ($node_type == "post_comment") {
                            $notification['url'] = $codes->SYSTEM_URL . '/posts/' . $node_url;
                            $notification['message'] = __("Likes your comment");
                        } elseif ($node_type == "photo") {
                            $notification['url'] = $codes->SYSTEM_URL . '/photos/' . $node_url;
                            $notification['message'] = __("Likes your photo");
                        } elseif ($node_type == "photo_comment") {
                            $notification['url'] = $codes->SYSTEM_URL . '/photos/' . $node_url;
                            $notification['message'] = __("Likes your comment");
                        } elseif ($node_type == "session") {
                            $notification['url'] = $codes->SYSTEM_URL . '/sessions/' . $node_url;
                            $notification['message'] = __("Likes your Session");
                        } elseif ($node_type == "session_photo_comment") {
                            $notification['url'] = $codes->SYSTEM_URL . "/" . $node_url . '/photos';
                            $notification['message'] = "Likes your comment";
                        }
                    }
                    break;

                case 'comment':
                    if ($codes->EMAIL_POST_COMMENTS && $receiver['email_post_comments']) {
                        if ($node_type == "post") {
                            $notification['url'] = $codes->SYSTEM_URL . '/posts/' . $node_url;
                            $notification['message'] = __("Commented on your post");
                        } elseif ($node_type == "photo") {
                            $notification['url'] = $codes->SYSTEM_URL . '/photos/' . $node_url;
                            $notification['message'] = __("Commented on your photo");
                        }
                    }
                    break;

                case 'share':
                    if ($codes->EMAIL_POST_SHARES && $receiver['email_post_shares']) {
                        $notification['url'] = $codes->SYSTEM_URL . '/posts/' . $node_url;
                        $notification['message'] = __("Shared your post");
                    }
                    break;

                case 'mention':
                    if ($codes->EMAIL_MENTIONS && $receiver['email_mentions']) {
                        $notification['icon'] = "fa-comment";
                        if ($node_type == "post") {
                            $notification['url'] = $codes->SYSTEM_URL . '/posts/' . $node_url;
                            $notification['message'] = __("Mentioned you in a post");
                        } elseif ($node_type == "comment") {
                            $notification['url'] = $codes->SYSTEM_URL . '/posts/' . $node_url;
                            $notification['message'] = __("Mentioned you in a comment");
                        } elseif ($node_type == "photo") {
                            $notification['url'] = $codes->SYSTEM_URL . '/photos/' . $node_url;
                            $notification['message'] = __("Mentioned you in a comment");
                        }
                    }
                    break;

                case 'profile_visit':
                    if ($codes->EMAIL_PROFILE_VISITS && $receiver['email_profile_visits']) {
                        $notification['url'] = $codes->SYSTEM_URL . '/' . $this->_data['user_name'];
                        $notification['message'] = __("Visited your profile");
                    }
                    break;

                case 'wall':
                    if ($codes->EMAIL_WALL_POSTS && $receiver['email_wall_posts']) {
                        $notification['url'] = $codes->SYSTEM_URL . '/posts/' . $node_url;
                        $notification['message'] = __("Posted on your wall");
                    }
                    break;

                default:
                    return;
                    break;
            }
            /* prepare notification email */
            if ($notification['message']) {
                $subject = __("New notification from") . " " . $codes->SYSTEM_TITLE;
                $body = __("Hi") . " " . ucwords($receiver['user_fullname']) . ",";
                $body .= "\r\n\r\n" . $this->_data['user_fullname'] . " " . $notification['message'];
                $body .= "\r\n\r\n" . $notification['url'];
				$body .= "\r\n\r" . $codes->SYSTEM_TITLE . " " . __("Team");
				
				$headers = "MIME-Version: 1.0" . "\r\n";
				$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
                /* send email */
                mail($receiver['usermail'], $subject, $body, $headers);
            }
        }
    }


    public function getReactions($type , $reaction){
    	Log::debug("Started ...updateNotifications Dao :");

		try{
			
			$conn = ConnectionManager::get('default');
			$ans = array();
			$ans['status'] = 0;
			$ans['message'] = 'something went wrong or wrong reaction id';
			$ans['data'] = null;
			if($type == 0){
				$ans['data'] = array();
				$sql=sprintf("SELECT * FROM reactions");
				$st = $conn->execute($sql);
				while($res = $st->fetch("assoc")){
					array_push($ans["data"], $res);
				}
				$ans['status'] = 1;
				$ans['message'] = 'All Reactions';
			}else{
				$arr = array();
				$sql=sprintf("SELECT * FROM reactions where id = $reaction");
				$st = $conn->execute($sql);
				if($res = $st->fetch("assoc")){
					array_push($arr, $res);
				}else{return $ans;}
				$ans['status'] = 1;
				$ans['message'] = 'Reaction';
				$ans['data'] = $arr[0];

			}
			return $ans;

			Log::debug("SQL : ".$sql);

			

			Log::debug("Ended ...updateNotifications Dao");

		}catch(\Exception $e){
			Log::debug($e);
			throw new Exception($e);
		}
    }

}