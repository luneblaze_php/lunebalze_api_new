<?php

namespace App\DaoLayer;

use App\Utils\Codes;
use Cake\Core\Exception\Exception;
use Cake\Datasource\ConnectionManager;
use App\DaoLayer\PostsDao;
use Cake\Log\Log;

class VenueDao
{
	private function getImgFullPath($img = '', $newPath = 0){
        $codes = new Codes;
        return !empty($img) ?$codes->SYSTEM_URL.'/'.($newPath?'api/':'').$codes->UPLOADS_DIRECTORY.'/'.$img:'';
    }

    public function mysql_real_escape_string($inp) {
	    if(is_array($inp))
	        return array_map(__METHOD__, $inp);

	    if(!empty($inp) && is_string($inp)) {
	        return str_replace(array('\\', "\0", "\n", "\r", "'", '"', "\x1a"), array('\\\\', '\\0', '\\n', '\\r', "\\'", '\\"', '\\Z'), $inp);
	    }

	    return $inp;
	}
	/**
	 * Get Venue by Venue Id
	 */
	public function getVenueByVenueId($venueId)
	{
		Log::debug("Started ...getVenueByVenueId Dao : Venue Id : ".$venueId);

		try{
			
			$conn = ConnectionManager::get('default');

			$sql=sprintf("SELECT * FROM `venue` WHERE `venue_id` = %s", $venueId);
		
			Log::debug("SQL : ".$sql);

			$stmt = $conn->execute($sql);

			$result = $stmt->fetch("assoc");

			Log::debug("Ended ...getVenueByVenueId Dao");

			return $result;

		}catch(\Exception $e){
			Log::debug($e);
			throw new Exception($e);
		}
	}
	
	/**
	 * Get Venue by Venue Id
	 */
	public function getCampusBookedDate($venueId)
	{
		Log::debug("Started ...getCampusBookedDate Dao : Venue Id : ".$venueId);

		try{
			
			$conn = ConnectionManager::get('default');

			$sql=sprintf("SELECT date FROM `schedule_campus` WHERE `college` = %s", $venueId);
		
			Log::debug("SQL : ".$sql);

			$stmt = $conn->execute($sql);


			$results = array();
            while($result = $stmt->fetch("assoc")) {
                array_push($results, $result['date']);
            }

			Log::debug("Ended ...getVenueByVenueId Dao");

			return $results;

		}catch(\Exception $e){
			Log::debug($e);
			throw new Exception($e);
		}
	}

	public function addVenue($input){
		Log::debug("Started ...getCampusBookedDate Dao : user Id : ".$input['user_id']);

		try{
			$codes = new Codes;
			$conn = ConnectionManager::get('default');

			$sql=sprintf("SELECT * FROM `venue` WHERE `venue_email` = '%s'", $input['venue_email']);
			Log::debug("SQL : ".$sql);

			$stmt = $conn->execute($sql);
			if($re = $stmt->fetch('assoc')){
				return ['message' => 'already exists' , 'status' => 0];
			}
			if(!isset($input['location'])){
				$input['location'] = '';
			}
			if(!isset($input['websitelink'])){
				$input['websitelink'] = '';
			}
			
			if(!isset($input['is_college'])){
				$input['is_college'] = 0;
			}
			$input['cityid'] = 0;
			$input['venue_type'] = 0;
			$otp = random_int(100000, 999999);
			extract($input);
			$sql = sprintf("INSERT INTO venue SET venue_name = '%s', venue_type = %s , userid = %s , cityid = %s , 	description = '%s', location = '%s' , venue_email = '%s', venue_contact = '%s', website_link = '%s' , otp = %s , is_college = %s , status = 0", $venue_name , $venue_type, $user_id , $cityid , $this->mysql_real_escape_string($description) , $location, $venue_email , $venue_contact , $websitelink , $otp , $is_college);

			if(!($conn->execute($sql))){
				return ['status'=>0,'message' => 'something went wrong'];
			}
			//mail
			
			$subject = "Just one more step to get started on" . " " . $codes->SYSTEM_URL;
                    $body = "Hi" . " " . ucwords($input['venue_name']). ",<br/><br/>";
                    $body .= "\r\n\r\n" . "Welcome to" . " " . $codes->SYSTEM_URL. "<br/><br/>";
                    $body .= "\r\n\r\n" . "To verify your email, Please enter the OTP:<strong>".$otp."</strong><br/><br/>";
                    $body .= "\r\n\r" . $codes->SYSTEM_URL . " " . "Team";

                    // Always set content-type when sending HTML email
                        $headers = "MIME-Version: 1.0" . "\r\n";
                        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";


                    
                    // send email 
                    if (!mail($venue_email, $subject, $body, $headers)) {
                        throw new Exception("Activation email could not be sent.");
                    }
					




			$sql="SELECT LAST_INSERT_ID()";
            $stmt = $conn->execute($sql);
            $res = $stmt->fetch();
            //$res = $res[0];
            return $res[0];

			
			

		}catch(\Exception $e){
			Log::debug($e);
			throw new Exception($e);
		}
	}

	public function venueOtpVerify($venueId ,$userid, $otp)
	{
		Log::debug("Started ...venueOtpVerify Dao : Venue Id : ".$venueId);

		try{
			
			$conn = ConnectionManager::get('default');

			$sql=sprintf("SELECT * FROM venue WHERE venue_id = %s and userid = %s", $venueId , $userid);
		
			Log::debug("SQL : ".$sql);

			$stmt = $conn->execute($sql);
            if($result = $stmt->fetch("assoc")) {
                if($result['otp'] == $otp){
                	$sql = sprintf("UPDATE venue set status = 1 where venue_id = %s",$venueId);
                	if($conn->execute($sql)){
                		return ['status' => 1 , 'message'=> 'verified'];
                	}else{
                		return ['status' => 0 , 'message'=> 'something went wrong'];
                	}
                }else{
                	return ['status' => 0 , 'message'=> 'wrong otp'];
                }
            }else{
            	return ['status' => 0 , 'message'=> 'institution not found or userid does not match'];
            }

			Log::debug("Ended ...getVenueByVenueId Dao");


		}catch(\Exception $e){
			Log::debug($e);
			throw new Exception($e);
		}
	}


	public function editVenue($input)
	{
		Log::debug("Started ...editVenue Dao : Venue Id : ");

		try{
			
			$conn = ConnectionManager::get('default');
			$results = array();
			$codes = new Codes;
			$sql=sprintf("SELECT * FROM `venue` WHERE `venue_id` = %s and userid = %s", $input['venue_id'] , $input['user_id']);
		
			Log::debug("SQL : ".$sql);

			$stmt = $conn->execute($sql);

            if(!($r = $stmt->fetch('assoc'))){return ['status' => 0 , 'message'=> 'institution not found or userid does not match'];}
            if(!isset($input['bio'])){$input['bio']=$r['bio'];}
            if(!isset($input['address'])){$input['address']=$r['address'];}
            if(!isset($input['description'])){$input['description']=$r['description'];}
            if(!isset($input['lattitude'])){$input['lattitude']=$r['latitude'];}
            if(!isset($input['longitude'])){$input['longitude']=$r['longitude'];}
            if(!isset($input['public_email'])){$input['public_email']=$r['public_email'];}
            if(!isset($input['public_contact'])){$input['public_contact']=$r['public_contact'];}
            if(!isset($input['courses'])){$input['courses']=$r['courses'];}
            if(!isset($input['avaliability_slots'])){$input['avaliability_slots']=$r['avaliability_slots'];}
            if(!isset($input['college_drive_capacity'])){$input['college_drive_capacity']=$r['college_drive_capacity'];}
            if(!isset($input['location'])){$input['location']=$r['location'];}
            if(!isset($input['venue_name'])){$input['venue_name']=$r['venue_name'];}
            if(!isset($input['websitelink'])){$input['websitelink']=$r['website_link'];}


            $file_name='';//LOGO PATH SAVED IN THIS
            $file_cov='';//cover path saved in this
            
            if(isset($_FILES['logo']) && $_FILES['logo']['size'] != 0) {
                        // valid inputs
                        if(!isset($_FILES["logo"]) || $_FILES["logo"]["error"] != UPLOAD_ERR_OK) {
                            throw new Exception("Something wrong with upload! Is 'upload_max_filesize' set correctly?");
                        }

                        // check file size
                        $max_allowed_size = $codes->MAX_FILE_SIZE * 1024;
                        if($_FILES["logo"]["size"] > $max_allowed_size) {
                            throw new Exception("The file size is so big");
                        }
                        $extension = pathinfo($_FILES['logo']['name'], PATHINFO_EXTENSION);
                        /* check & create uploads dir */
                        $folder = 'photos';
                            $depth = '../';
                            if(!file_exists($depth.$codes->UPLOADS_DIRECTORY.'/'.$folder)) {
                                @mkdir($depth.$codes->UPLOADS_DIRECTORY.'/'.$folder, 0777, true);
                            }
                            if(!file_exists($depth.$codes->UPLOADS_DIRECTORY.'/'.$folder.'/' . date('Y'))) {
                                @mkdir($depth.$codes->UPLOADS_DIRECTORY.'/'.$folder.'/' . date('Y'), 0777, true);
                            }
                            if(!file_exists($codes->UPLOADS_DIRECTORY.'/'.$folder.'/' . date('Y') . '/' . date('m'))) {
                                @mkdir($depth.$codes->UPLOADS_DIRECTORY.'/'.$folder.'/' . date('Y') . '/' . date('m'), 0777, true);
                            }

                        /* prepare new file name */
                        $directory = $folder.'/'. date('Y') . '/' . date('m') . '/';
                        $prefix =$codes->UPLOADES_PREFIX.'_'.md5(time()*rand(1, 9999));
                        $file_name = $directory.$prefix.'.'.$extension;
                        $path = $depth.$codes->UPLOADS_DIRECTORY.'/'.$file_name;
                                            
                        /* check if the file uploaded successfully */
                        if(!@move_uploaded_file($_FILES['logo']['tmp_name'], $path)) {
                            throw new Exception("Sorry, can not upload the photo");
                        }
                        
                        /* upload to amazon s3 */
                        if($codes->S3_ENABLED) {
                            aws_s3_upload($path, $file_name);
                        }
            }
            $count=0;

            $carr = array();
            $cover = array();
            
            if(isset($_FILES['cover_photo'])){
            	$cov = $_FILES['cover_photo'];
            	foreach ($cov['name'] as $key) {
            		# code...

            		array_push($carr, $count);
            		$count = $count + 1;
            	}
            	
            	foreach ($carr as $i ) {
            	
            	if(!isset($_FILES["cover_photo"]) || $_FILES["cover_photo"]["error"][$i] != UPLOAD_ERR_OK) {
                            throw new Exception("Something wrong with upload! Is 'upload_max_filesize' set correctly?");
                        }

                        // check file size
                        $max_allowed_size = $codes->MAX_FILE_SIZE * 1024;
                        if($_FILES["cover_photo"]["size"][$i] > $max_allowed_size) {
                            throw new Exception("The file size is so big");
                        }
                        $extension = pathinfo($_FILES['cover_photo']['name'][$i], PATHINFO_EXTENSION);
                        /* check & create uploads dir */
                        $folder = 'photos';
                            $depth = '../';
                            if(!file_exists($depth.$codes->UPLOADS_DIRECTORY.'/'.$folder)) {
                                @mkdir($depth.$codes->UPLOADS_DIRECTORY.'/'.$folder, 0777, true);
                            }
                            if(!file_exists($depth.$codes->UPLOADS_DIRECTORY.'/'.$folder.'/' . date('Y'))) {
                                @mkdir($depth.$codes->UPLOADS_DIRECTORY.'/'.$folder.'/' . date('Y'), 0777, true);
                            }
                            if(!file_exists($codes->UPLOADS_DIRECTORY.'/'.$folder.'/' . date('Y') . '/' . date('m'))) {
                                @mkdir($depth.$codes->UPLOADS_DIRECTORY.'/'.$folder.'/' . date('Y') . '/' . date('m'), 0777, true);
                            }

                        /* prepare new file name */
                        $directory = $folder.'/'. date('Y') . '/' . date('m') . '/';
                        $prefix =$codes->UPLOADES_PREFIX.'_'.md5(time()*rand(1, 9999));
                        $file_cov = $directory.$prefix.'.'.$extension;
                        $path = $depth.$codes->UPLOADS_DIRECTORY.'/'.$file_cov;
                                            
                        /* check if the file uploaded successfully */
                        if(!@move_uploaded_file($_FILES['cover_photo']['tmp_name'][$i], $path)) {
                            throw new Exception("Sorry, can not upload the photo");
                        }
                        
                        /* upload to amazon s3 */
                        if($codes->S3_ENABLED) {
                            aws_s3_upload($path, $file_cov);
                        }
                        array_push($cover, $file_cov);
                    }
            }
            
            extract($input);
            if(!isset($_FILES['logo'])){
            	$file_name = $r["cover_photo"]; 
            }
            $sql = sprintf("UPDATE venue SET bio = '%s', address = '%s', courses = '%s', public_contact = '%s', public_email = '%s', avaliability_slots = '%s', college_drive_capacity = '%s', latitude = '%s', longitude = '%s', cover_photo = '%s', description = '%s',location='%s' , venue_name = '%s', website_link = '%s' where venue_id = %s",$this->mysql_real_escape_string($bio), $address, $courses, $public_contact, $public_email, $avaliability_slots, $college_drive_capacity, $lattitude , $longitude, $file_name, $this->mysql_real_escape_string($description),$location , $venue_name , $websitelink , $venue_id );

            Log::debug("SQL : ".$sql);

			if(!$conn->execute($sql)){
				return ['status'=> 0 , 'message'=> 'something went wrong'];
			}
			$dir = ['0' => 'Sunday' , '1' => 'Monday' , '2' => 'Tuesday' , '3' => 'Wednesday' , '4' => 'Thursday' , '5' => 'Friday' , '6' => 'Saturday'];
			if(isset($day) && is_array($day)){
				$sql = sprintf("DELETE from venue_slots where venue_id = %s", $venue_id);
				if(!$conn->execute($sql)){return ["message" => "something went wrong" , "status" => 0];}
				$count = count($day);
				for($i = 0 ; $i < $count ; $i++){
					$d = $day[$i];
					$day[$i] = $dir[$d];
					$sql = sprintf("INSERT into venue_slots SET day = '%s' , starttime = '%s' , endtime = '%s' , venue_id = %s", $day[$i] , $start[$i] , $end[$i] , $venue_id);
					$conn->execute($sql);
				}
			}
			foreach ($cover as $c) {
				# code...
				$sql = sprintf("INSERT INTO venue_image SET venue_id = %s, photo = '%s' ", $venue_id , $c);
				Log::debug("SQL : ".$sql);
				$conn->execute($sql);
			}
			$sql=sprintf("SELECT * FROM `venue` WHERE `venue_id` = %s", $input['venue_id']);
		
			Log::debug("SQL : ".$sql);

			$stmt = $conn->execute($sql);
			$results = $stmt->fetch('assoc');
			$sql=sprintf("SELECT * FROM `venue_image` WHERE `venue_id` = %s", $input['venue_id']);
		
			Log::debug("SQL : ".$sql);

			$stmt = $conn->execute($sql);
			$i = 0;
			$arr = array();
			while($res = $stmt->fetch("assoc")){
				$res["photo"] = $this->getImgFullPath($res["photo"] ,1);
				array_push($arr, $res);
			}
			$results['logo'] = $this->getImgFullPath($results['cover_photo'],1);
			$results['cover_photo'] = $arr;
			$results['message'] = "success";
			return $results;

		}catch(\Exception $e){
			Log::debug($e);
			throw new Exception($e);
		}
	}

	public function deleteVenueCover($photo ,$venueId , $user)
	{
		Log::debug("Started ...venueOtpVerify Dao : Venue Id : ".$venueId);

		try{
			
			$conn = ConnectionManager::get('default');

			$sql=sprintf("SELECT * FROM venue WHERE venue_id = %s and userid = %s", $venueId , $user);
			Log::debug("SQL : ".$sql);

			$stmt = $conn->execute($sql);

			if($results = $stmt->fetch('assoc')){

			}else{
				return ['status'=>0, 'message' => 'venue not found or userid doesent match'];
			}
			$sql= "DELETE FROM venue_image WHERE venue_id = ".$venueId." and id = ".$photo."";
			Log::debug("SQL : ".$sql);

			if(!$conn->execute($sql)){
				return ['status'=>0, 'message' => 'photo not found'];
			}else{
				return ['status'=>1, 'message' => 'success'];		
			}
			


		}catch(\Exception $e){
			Log::debug($e);
			throw new Exception($e);
		}
	}

	public function addVenueCover($input ,$venueId , $user)
	{
		Log::debug("Started ...venueOtpVerify Dao : Venue Id : ".$venueId);

		try{
			
			$conn = ConnectionManager::get('default');
			$codes = new Codes;
			$sql=sprintf("SELECT * FROM venue WHERE venue_id = %s and userid = %s", $venueId , $user);
			Log::debug("SQL : ".$sql);

			$stmt = $conn->execute($sql);

			if($results = $stmt->fetch('assoc')){

			}else{
				return ['status'=>0, 'message' => 'venue not found or userid doesent match'];
			}
			$sql= "select * FROM venue_image WHERE venue_id = ".$venueId."";
			Log::debug("SQL : ".$sql);
			$stmt = $conn->execute($sql);
			$i = 0;
			while($res = $stmt->fetch('assoc')){$i = $i + 1;}
			if($i > 9){return ['status'=>0, 'message' => 'more than 9 photos not allowed'];}
			$file_name = '';
			if(isset($_FILES['logo']) ) {
                        // valid inputs
                        if(!isset($_FILES["logo"]) || $_FILES["logo"]["error"] != UPLOAD_ERR_OK) {
                            throw new Exception("Something wrong with upload! Is 'upload_max_filesize' set correctly?");
                        }

                        // check file size
                        $max_allowed_size = $codes->MAX_FILE_SIZE * 1024;
                        if($_FILES["logo"]["size"] > $max_allowed_size) {
                            throw new Exception("The file size is so big");
                        }
                        $extension = pathinfo($_FILES['logo']['name'], PATHINFO_EXTENSION);
                        /* check & create uploads dir */
                        $folder = 'photos';
                            $depth = '../';
                            if(!file_exists($depth.$codes->UPLOADS_DIRECTORY.'/'.$folder)) {
                                @mkdir($depth.$codes->UPLOADS_DIRECTORY.'/'.$folder, 0777 , true);
                            }
                            if(!file_exists($depth.$codes->UPLOADS_DIRECTORY.'/'.$folder.'/' . date('Y'))) {
                                @mkdir($depth.$codes->UPLOADS_DIRECTORY.'/'.$folder.'/' . date('Y'), 0777 , true);
                            }
                            if(!file_exists($codes->UPLOADS_DIRECTORY.'/'.$folder.'/' . date('Y') . '/' . date('m'))) {
                                @mkdir($depth.$codes->UPLOADS_DIRECTORY.'/'.$folder.'/' . date('Y') . '/' . date('m'), 0777, true);
                            }

                        /* prepare new file name */
                        $directory = $folder.'/'. date('Y') . '/' . date('m') . '/';
                        $prefix =$codes->UPLOADES_PREFIX.'_'.md5(time()*rand(1, 9999));
                        $file_name = $directory.$prefix.'.'.$extension;
                        $path = $depth.$codes->UPLOADS_DIRECTORY.'/'.$file_name;
                                            
                        /* check if the file uploaded successfully */
                        if(!@move_uploaded_file($_FILES['logo']['tmp_name'], $path)) {
                            throw new Exception("Sorry, can not upload the photo");
                        }
                        
                        /* upload to amazon s3 */
                        if($codes->S3_ENABLED) {
                            aws_s3_upload($path, $file_name);
                        }
            }
            if($file_name != ''){
            	$sql = sprintf("INSERT INTO venue_image set venue_id = %s , photo = '%s'", $venueId , $file_name);
            	Log::debug("SQL : ".$sql);
            	if(!$conn->execute($sql)){
					return ['status'=>0, 'message' => 'photo not found'];
				}else{
					$sql = "SELECT LAST_INSERT_ID()";
					$stmt = $conn->execute($sql);
            		$res = $stmt->fetch();
					return ['message' => 'success' , 'path'=>$this->getImgFullPath($file_name,1) , 'id' => $res[0]];		
				}
            }else{
            	return ['status'=>0, 'message' => 'photo not found or something went wrong'];
            }
			if(!$conn->execute($sql)){
				return ['status'=>0, 'message' => 'photo not found'];
			}else{
				return ['status'=>1, 'message' => 'success'];		
			}
			
			return [];

		}catch(\Exception $e){
			Log::debug($e);
			throw new Exception($e);
		}
	}






	public function getVenue($venueId , $user)
	{
		Log::debug("Started ...getVenue Dao : Venue Id : ".$venueId);

		try{
			
			$conn = ConnectionManager::get('default');

			$sql=sprintf("SELECT * FROM venue WHERE venue_id = %s", $venueId);
			Log::debug("SQL : ".$sql);

			$stmt = $conn->execute($sql);
			$results = array();
			if($result = $stmt->fetch('assoc')){

				$sql = sprintf("select * from users_venue where user_id = %s and venue_id = %s", $user , $venueId);
				Log::debug("SQL : ".$sql);
				$st = $conn->execute($sql);
				if($a = $st->fetch('assoc')){
					$result['i_follow'] = 1;
				}else{
					$result['i_follow'] = 0;
				}
				$vf = array();
				$sql = sprintf("select user_id from users_venue where venue_id = %s and user_id != %s" , $venueId , $user);
				Log::debug("SQL : ".$sql);
				$st = $conn->execute($sql);
				$friend = array();
				$result['followers_count'] = $st->count(0);
				while($re = $st->fetch('assoc')){
					$sql = sprintf("SELECT user_id, user_email, user_fullname, user_firstname, user_lastname, user_gender, user_picture, user_work, user_work_title, user_work_place from users where user_id = %s", $re['user_id']);
					Log::debug("SQL : ".$sql);

					$s = $conn->execute($sql);
					if($r=$s->fetch('assoc')){
						$r['user_picture'] = $this->getImgFullPath($r['user_picture'] ,1);
						//connection variable add here
						//mutual count here
					}else{
						continue;
					}
					array_push($vf, $r);
					$sql = sprintf("select * from friends where (user_one_id = %s and user_two_id = %s) or (user_one_id = %s and user_two_id = %s) ",$r['user_id'] , $user , $user , $r['user_id']);
					$stt = $conn->execute($sql);
					if($a = $stt->fetch("assoc")){array_push($friend, $r);}
				}

				$result['followers_count'] = count($vf);
				if($result['followers_count'] > 0){
					$result['venue_follower'] = $vf[0];
				}else{
					$result['venue_follower'] = null;
				}




				$result['friendcount'] = count($friend);
				if($result['friendcount'] > 0){
					$result['friend_follower'] = $friend[0];
				}else{
					$result['friend_follower'] = null;
				}


				


			$sql = "SELECT * FROM `sessions` WHERE venue like '%,". $venueId .",%' or venue like '". $venueId .",%' or venue like '%,". $venueId ."' or venue like '". $venueId ."'";
			$s = $conn->execute($sql);
			$result["sessions_count"] = $s->count(0);


				//$result['followers_count'] = sizeof($result['venue_followers']);
			}else{
				return ['status'=> 0 , 'message'=>'not found venue'];
			}
			$result['cover_photo'] = $this->getImgFullPath($result['cover_photo'],1);
			$course = $result['courses'];
			if($course != null && $course != ""){
				$arr = explode(',', $course);
				$temp = array();
				foreach ($arr as $i) {
					# code...
					$sql = sprintf("SELECT * from course where id = %s", $i);
					$s = $conn->execute($sql);
					if($e = $s->fetch("assoc")){array_push($temp, $e);}
				}
				$result['courses'] = $temp;

			}else{
				$result['courses'] = array();
			}

			//quizzez
			$sql = sprintf("SELECT * FROM `quizzes` where (user_type = 'Institution' or user_type = 'venue' or user_type = 'Venue' or user_type = 'institution') and creator_id = %s",$venueId);
			Log::debug("SQL : ".$sql);
			$stmt = $conn->execute($sql);
			$result['quizzes_count'] = $stmt->count(0);
			//debates
			$sql = sprintf("SELECT * FROM `debates` where (user_type = 'Institution' or user_type = 'venue' or user_type = 'Venue' or user_type = 'institution') and creator_id = %s",$venueId);
			Log::debug("SQL : ".$sql);
			$stmt = $conn->execute($sql);
			$result['debates_count'] = $stmt->count(0);
			//quickbits
			$sql = sprintf("SELECT * FROM `quickbits` where (user_type = 'Institution' or user_type = 'venue' or user_type = 'Venue' or user_type = 'institution') and creator_id = %s",$venueId);
			Log::debug("SQL : ".$sql);
			$stmt = $conn->execute($sql);
			$result['quickbits_count'] = $stmt->count(0);
			//article
			$sql = sprintf("SELECT * FROM `articles` where (user_type = 'Institution' or user_type = 'venue' or user_type = 'Venue' or user_type = 'institution') and created_by = %s",$venueId);
			Log::debug("SQL : ".$sql);
			$stmt = $conn->execute($sql);
			$result['articles_count'] = $stmt->count(0);
			//polls
			$sql = sprintf("SELECT * FROM `posts` where post_type = 'poll' and (user_type = 'Institution' or user_type = 'venue' or user_type = 'Venue' or user_type = 'institution') and user_id = %s",$venueId);
			Log::debug("SQL : ".$sql);
			$stmt = $conn->execute($sql);
			$result['polls_count'] = $stmt->count(0);






			$results['venue_data'] = $result; //venue_data here added 
			$sql = sprintf("SELECT * FROM venue_teams where venue_id = %s", $venueId);
			Log::debug("SQL : ".$sql);
			$stmt = $conn->execute($sql);
			$temp = array();
			while($res = $stmt->fetch('assoc')){
				$sql = sprintf("SELECT user_id, user_email, user_fullname, user_firstname, user_lastname, user_gender, user_picture, user_work, user_work_title, user_work_place from users where user_id = %s", $res['user_id']);
				Log::debug("SQL : ".$sql);

				$st = $conn->execute($sql);
				if($r = $st->fetch('assoc')){

					if($r['user_picture'] != null && $r['user_picture'] != ''){
						$r['user_picture'] = $this->getImgFullPath($r['user_picture'] ,1);
					}
					$res['user_detail'] = $r;
					array_push($temp, $res);
				}
				//connection variable add here 
				
			}
			$results['teams'] = $temp;//teams here added

			$temp = array();
			$sql = sprintf("SELECT photo , id from venue_image where venue_id = %s", $venueId);
			Log::debug("SQL : ".$sql);
			$stmt = $conn->execute($sql);
			while($res = $stmt->fetch('assoc')){
				$res['photo'] = $this->getImgFullPath($res['photo'], 1);
				array_push($temp, $res);
			}
			$results['allphoto'] = $temp; //all photo here

			$m = array();
			$t = $w = $th = $f = $s = $su = array();

			$sql = sprintf("SELECT id, day, starttime, endtime FROM venue_slots where venue_id = %s", $venueId);
			Log::debug("SQL : ".$sql);
			$stmt = $conn->execute($sql);
			while($res = $stmt->fetch('assoc')){
				$res['slotid'] = $res['id'];
				switch($res['day']){
					case 'Monday':
						array_push($m, $res);
						break;
					case 'Tuesday':
						array_push($t, $res);
						break;
					case 'Wednesday':
						array_push($w, $res);
						break;
					case 'Thursday':
						array_push($th, $res);
						break;
					case 'Friday':
						array_push($f, $res);
						break;
					case 'Saturday':
						array_push($s, $res);
						break;
					case 'Sunday':
						array_push($su, $res);
						break;
					default:
						break;
				}
			}
			$temp = array();
			array_push($temp, ['slots'=>$su]);
			array_push($temp, ['slots'=>$m]);
			array_push($temp, ['slots'=>$t]);
			array_push($temp, ['slots'=>$w]);
			array_push($temp, ['slots'=>$th]);
			array_push($temp, ['slots'=>$f]);
			array_push($temp, ['slots'=>$s]);
			$results['days'] = $temp;//days here


			
		
			
			
			
			//drive data
			$sql = sprintf("SELECT scheduleid ,organization , college , date , userid ,interests, categories ,companyaddress ,status   FROM `schedule_campus` where college = %s",$venueId);
			$temp = array();
			Log::debug("SQL : ".$sql);
			$stmt = $conn->execute($sql);
			while($r = $stmt->fetch('assoc')){
				array_push($temp, $r);
			}
			$results['campus_drives'] = $temp;//campus_drives here 
			


			return $results;
		}catch(\Exception $e){
			Log::debug($e);
			throw new Exception($e);
		}
	}




	public function getCampusDetailByOrganizationId($userid ,$organizationId, $offset){
      
        
        Log::debug("Started ...getCampusDetailByOrganizationId Dao : organization Id : ".$organizationId);

        try{

            $codes = new Codes;
            
            $conn = ConnectionManager::get('default');

            $sql=sprintf("SELECT * FROM `schedule_campus` WHERE `college`=%s order by added_on desc limit %s ,%s",$organizationId,$offset,$codes->MAX_RESULTS);          

            Log::debug("SQL : ".$sql);

            $stmt = $conn->execute($sql);
            $count = $stmt->count();
           

            $data = array();

            if($count > 0){
                while($res = $stmt->fetch('assoc')){
                    $res['i_join'] = 0;
                    $res['campus_detail']=[];
                    $sql = sprintf("SELECT * FROM `campus_detail` WHERE `scheduleid`=%s ",$res['scheduleid']);
                    $st = $conn->execute($sql);
                    if($r = $st->fetch('assoc')){
                        $res['campus_detail'] = $r;
                    }
                    $sql = sprintf("SELECT * FROM `hiring` WHERE `scheduleid`=%s and user_id = %s ",$res['scheduleid'], $userid);
                    $st = $conn->execute($sql);
                    if($r = $st->fetch('assoc')){
                        $res['i_join'] = $r['status'];
                    }
                    
                    $res['eligible'] = intval($res['eligible']);
                    //interest
                    if ($res['interests'] != '') {
                        $interest = explode(',', $res['interests']);
                        foreach ($interest as $ik => $ival) {

                            $qin = $conn->execute(sprintf("select * from `interest_mst` where interest_id=%s", $ival));
                            $rinn = $qin->fetch("assoc");
                            $rinn['image'] = $codes->SYSTEM_URL  . '/content/uploads/' . $rinn['image'];
                            $rinn['interest_name'] = $rinn['text'];
                            unset($rinn['text']);
                            unset($rinn['description']);
                            $temp[] = $rinn;
                            $res['interests'] = $temp;
                        }
                    } else {
                        $res['interests'] = [];
                    }  

                    //categories
                    if ($res['categories'] != '') {
                        $categories = explode(',', $res['categories']);
                        foreach ($categories as $ik => $ival) {
                            $qin = $conn->execute(sprintf("select * from `category_mst` where category_id=%s", $ival));
                            $rcat =  $qin->fetch("assoc");
                            $rcat['image'] = $codes->SYSTEM_URL . '/content/uploads/' . $rcat['image'];
                            $rcat['category_name'] = $rcat['text'];
                            unset($rcat['text']);
                            unset($rcat['description']);
                            $row_category[] = $rcat;
                            $res['categories'] = $row_category;
                        }
                    } else {
                        $res['categories'] = [];
                    }
                    


                    array_push($data, $res);
                }

            }    
            Log::debug("Ended ...getCampusDetailByOrganizationId Dao");
           // print_r($data);

            $response['message'] = 'Success';
            $response['data'] = $data;
            $response['status'] = 1;

            return $data;
            
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }



    public function getVenuePosts($inputJson , $type)
    {
        Log::debug("Started ...getOrganizationPosts Dao : input  : ");

        try{
                $conn = ConnectionManager::get('default');
                
                //return $type;
                $sql = "select * from posts where (";
                foreach ($type as $post) {
                    # code...
                    $sql .= " post_type = '". $post ."' or";
                }
                $sql = rtrim($sql , 'or');
                $sql .= ") and user_id = ". $inputJson['venue_id']  ." and user_type = 'institution'";

                //return $sql;
                
                Log::debug("SQL : ".$sql);
                $pos = 1;
                $stmt = $conn->execute($sql);
                $res = array();
                $res['quiz'] = array();
                $res['debate'] = array();
                $res['poll'] = array();
                $res['article'] = array();
                $res['quickbit'] = array();
                $type = explode(',', $type);
                while($result = $stmt->fetch('assoc')){
                        if($result['post_type'] == 'quiz'){
                            $sql=sprintf("select * from quizzes where id='%s'",$result['quiz_id']);          

                            Log::debug("SQL : ".$sql);

                            $st = $conn->execute($sql);
                            if($temp = $st->fetch('assoc')){
                                $temp['pos'] = $pos;
                                $pos = $pos +1;
                                $temp['picture'] = $this->getImgFullPath($temp['picture'],1);
                                array_push($res['quiz'], $temp);
                            }


                        }elseif($result['post_type'] == 'debate'){
                            $sql=sprintf("select * from debates where id='%s'",$result['debate_id']);          

                            Log::debug("SQL : ".$sql);

                            $st = $conn->execute($sql);
                            if($temp = $st->fetch('assoc')){
                                $temp['pos'] = $pos;
                                $pos = $pos +1;
                                $temp['picture'] = $this->getImgFullPath($temp['picture'],1);
                                array_push($res['debate'], $temp);
                            }
                        }elseif($result['post_type'] == 'poll'){
                        	$poll = array();
                        	$poll['title'] = $result['text'];
                        	$dat=$conn->execute(sprintf("select * from posts_polls where post_id='%s'",$result['post_id']));
                        	if($i = $dat->fetch("assoc")){

                        	}else{
                        		return ["message"=> "something went wrong" , "status"=> 0];
                        	}
                            	$sql=sprintf("select * from posts_polls_options where poll_id='%s'",$i['poll_id']);          

                                Log::debug("SQL : ".$sql);

                                $s = $conn->execute($sql);
                                $i = 0;
                                $poll['options'] = array();
                                while($r = $s->fetch('assoc')){
                                	
                                    array_push($poll['options'], $r);
                                }
                                $poll['pos'] = $pos;
                                $pos = $pos +1;
                                array_push($res['poll'], $poll);
                            }elseif($result['post_type'] == 'article'){
                                $sql=sprintf("select * from posts_articles where post_id='%s'",$result['post_id']);          

                                Log::debug("SQL : ".$sql);

                                $s = $conn->execute($sql);
                                while($r = $s->fetch('assoc')){
                                    $sql=sprintf("select * from articles where articles_id='%s'",$r['article_id']);          

                                    Log::debug("SQL : ".$sql);

                                    $st = $conn->execute($sql);
                                    if($temp = $st->fetch('assoc')){
                                        $temp['pos'] = $pos;
                                        $pos = $pos +1;
                                        $temp['cover_photo'] = $this->getImgFullPath($temp['cover_photo'],1);
                                        array_push($res['article'], $temp);
                                    }
                                }
                            }else{
                                $sql=sprintf("select * from quickbits where id='%s'",$result['quickbit_id']);          

                                Log::debug("SQL : ".$sql);

                                $st = $conn->execute($sql);
                                if($temp = $st->fetch('assoc')){
                                    $temp['pos'] = $pos;
                                    $pos = $pos +1;
                                    $temp['cards'] = array();
                                    $sql = sprintf("select * from quickbits_cards where quickbit_id = %s", $temp['id']);
                                    $s = $conn->execute($sql);
                                    while($a = $s->fetch('assoc')){
                                    	if($a['picture'] != ''){
                                    		$a['picture'] = $this->getImgFullPath($a['picture'],1);
                                    	}
                                    	array_push($temp['cards'], $a);
                                    }
                                    array_push($res['quickbit'], $temp);
                                }
                            }
                }

            return $res;
        
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }



    public function collegeStudentData($userId , $venueId , $type , $year)
	{
		Log::debug("Started ...collegeStudentData Dao : Venue Id : ".$venueId);

		try{
			
			$conn = ConnectionManager::get('default');
			$pd = new PostsDao;
			$ids = array();
			$result = array();
			$result['totalAssessed'] = 0;
			$result['totalApplied'] = 0;
			$result['totalHired'] = 0;
			$dat = $pd->userData($userId , $venueId , "venue" , 0);
			$result['venueData'] = isset($dat[0]) ? $dat[0] : null;
			$year1 = $year + 1;
			$sql = sprintf("SELECT userid FROM `user_education_info` WHERE `venue` = %s", $venueId);
			$sql=$conn->execute($sql);
			while($r = $sql->fetch("assoc")){
				array_push($ids ,$r['userid'] );
			}
			//return $ids;
			if($ids != [])
				$csv = implode(",", $ids);
			else
				$csv = '';

			if($csv != '')
				$sql = sprintf("SELECT user_id FROM assessment_payment where FIND_IN_SET(user_id, '%s') and", $csv);
			else
				return $result;
			$given_assessment = array();
			if($year != 0){
				$sql .= " ondate >= '".$year."-01-01' and ondate < '".$year1."-01-01' and";
			}
			if($type != 0){
				$sql .= " assessment_type_id = '$type' and";
			}
			
			Log::debug("SQL : ".$sql);
			$sql = rtrim($sql , 'and');
			//return $sql;
			$stmt = $conn->execute($sql);
			//$temp = array();
			//$res = $stmt->fetch("assoc");
			while($r = $stmt->fetch('assoc')){
				if(!in_array($r['user_id'], $given_assessment)){
					array_push($given_assessment , $r['user_id']);
					$result['totalAssessed']++;
				}
			}
			if($given_assessment != [])
				$given = implode(",", $given_assessment);
			else
				return $result;
			//return $given;
			$sql = $conn->execute(sprintf("SELECT * FROM `hiring` where FIND_IN_SET(user_id , '$given')"));
			while($r = $sql->fetch('assoc')){
				$result['totalApplied']++;
				if($r['status'] == '3' || $r['status'] == 3){$result['totalHired']++;}
			}


			Log::debug("Ended ...collegeStudentData Dao");

			return $result;

		}catch(\Exception $e){
			Log::debug($e);
			throw new Exception($e);
		}
	}

	public function collegeStudentList($userId , $venueId , $assessmenttype , $year , $type , $offset)
	{
		Log::debug("Started ...collegeStudentList Dao : Venue Id : ".$venueId);

		try{
			
			$conn = ConnectionManager::get('default');
			$pd = new PostsDao;
			$ids = array();
			$result = array();
			switch($type){
				case "0":
					$ids = array();
					$sql=$conn->execute(sprintf("SELECT userid FROM `user_education_info` WHERE `venue` = %s", $venueId));
					while($r = $sql->fetch("assoc")){
						if(!in_array($r['userid'], $ids)){array_push($ids ,$r['userid']);}
					}
					if($ids != [])
					$csv = implode(",", $ids);
					else
						$csv = '';

					if($csv != '')
						$sql = sprintf("SELECT * FROM assessment_payment where FIND_IN_SET(user_id, '%s') and", $csv);
					else
						return $result;
					$given_assessment = array();
					if($year != 0){
						$sql .= " ondate >= '".$year."-01-01' and ondate < '".$year1."-01-01' and";
					}
					if($type != 0){
						$sql .= " assessment_type_id = '$type' and";
					}

					Log::debug("SQL : ".$sql);
					$sql = rtrim($sql , 'and');
					//return $sql;
					$arr = array();
					$stmt = $conn->execute($sql);
					//$temp = array();
					//$res = $stmt->fetch("assoc");
					//return $sql;
					$ids = array();
					while($r = $stmt->fetch('assoc')){
						if(!in_array($r['user_id'], $ids)){array_push($ids ,$r['user_id']);}
					}
					//return $ids;
					$ids = implode(',', $ids);
					$idnass = array();
					$get = $conn->execute("SELECT * from assessment_testmark where FIND_IN_SET(user_id , '$ids')");
					while($r = $get->fetch('assoc')){
						$key = "#".$r['user_id'];
						if(isset($idnass[$key])){
							if(!in_array($r['assessment_id'], $idnass[$key]) ){array_push($idnass[$key] , $r['assessment_id']);}
						}else{
							$idnass[$key] = array();
							array_push($idnass[$key] , $r['assessment_id']);
						}
					}
					//return $idnass;
					$arr = array();
					foreach($idnass as $key => $value){
						//return $r;
						//implementation
						//return ltrim($key , "#");
						//$id = $r['assessment_type_id'];
						$student_report = [];
						$user_id = ltrim($key , "#");
						$income = 0;
						$count = 0;
						$expenses = 0;
						foreach($value as $id){
							$tempData = [];
							$tempDatas = [];
							$tempDatass = [];
							
							$percentage = $percentile = 0;
							$total_marks_per_questions = 0;
							$results = $conn->execute(" SELECT COUNT(category)as question_per_cat, category,(SELECT per_q_mark FROM lztopics WHERE id =" . $id . ") as per_q_mark  FROM `lzquestions` WHERE topic_id=" . $id . " GROUP by category");
							$results = $results->fetchAll('assoc');
							//return $results;
							$max_marks = $conn->execute("SELECT MAX(mark) as max_marks,category FROM assessment_testmark WHERE assessment_id=" . $id . " GROUP BY category");
							$max_marks = $max_marks->fetchAll('assoc');

							$all_students = $conn->execute("SELECT category,mark,user_id FROM assessment_testmark WHERE assessment_id=" . $id . " AND user_id=" . $user_id);
							$all_students = $all_students->fetchAll('assoc');
							//$user_name = \DB::table('users')
							    //->select('user_fullname')->where('user_id', '=', $user_id)->get();

							foreach ($results as $res) {
							    $tempDatas[$res["category"]] = array(
							        'question_per_cat' => $res["question_per_cat"],
							        'per_q_mark' => $res["per_q_mark"],
							    );
							}
							foreach ($max_marks as $value) {
							    $tempDatass[$value["category"]] = array(
							        'max_marks' => $value["max_marks"],
							    );
							}
							foreach ($all_students as $student) {
							    $tempData[$student["user_id"]][$student["category"]] = array(
							        'mark' => $student["mark"],
							    );
							}
							
							        //    echo "<pre>";
							        //    print_r($tempData);
							        //    print_r($tempDatas);
							        //    print_r($tempDatass);
							        //    die;

							foreach ($tempData as $key => $students) {
							    foreach ($students as $key1 => $student) {

							        if (array_key_exists($key1, $tempDatas)) {

							            $total_marks_per_questions = $tempDatas[$key1]['question_per_cat'] * $tempDatas[$key1]['per_q_mark'];

							            if ($total_marks_per_questions) {
							                $percentage = round(($students[$key1]['mark'] * 100) / $total_marks_per_questions, 2);
							            } else {
							                $percentage = 0;
							            }

							            if ($tempDatass[$key1]['max_marks']) {
							                $percentile = round(($students[$key1]['mark'] * 100) / $tempDatass[$key1]['max_marks'], 2);
							            } else {
							                $percentile = 0;
							            }
							            $income += $percentage;
							            $expenses += $percentile;
							            $count++;
							            $student_report[] = array(
							                'year' => $conn->execute("SELECT text FROM category_mst WHERE category_id=" . $key1)->fetchAll('assoc')[0]["text"],
							                'income' => $percentage,
							                'expenses' => $percentile,
							            );
							        }
							    }
							}
						}
						$r['percentage'] = "".round($income/$count , 2 );
						$r['percentile'] = "".round($expenses/$count , 2 );
						$r['student_report'] = $student_report;
						$dat = $pd->userData($userId , $key , 'user' , 0);
						$r['creator_data'] = isset($dat[0]) ? $dat[0] : null;
						array_push($arr , $r);
					}
					return $arr;
				case "1":
					$ids = array();
					$sql=$conn->execute(sprintf("SELECT userid FROM `user_education_info` WHERE `venue` = %s", $venueId));
					while($r = $sql->fetch("assoc")){
						if(!in_array($r['userid'], $ids)){array_push($ids ,$r['userid']);}
					}
					if($ids != [])
					$csv = implode(",", $ids);
					else
						$csv = '';

					if($csv != '')
						$sql = sprintf("SELECT * FROM assessment_payment where FIND_IN_SET(user_id, '%s') and", $csv);
					else
						return $result;
					$given_assessment = array();
					if($year != 0){
						$sql .= " ondate >= '".$year."-01-01' and ondate < '".$year1."-01-01' and";
					}
					if($type != 0){
						$sql .= " assessment_type_id = '$type' and";
					}

					Log::debug("SQL : ".$sql);
					$sql = rtrim($sql , 'and');
					//return $sql;
					$arr = array();
					$stmt = $conn->execute($sql);
					//$temp = array();
					//$res = $stmt->fetch("assoc");
					//return $sql;
					$ids = array();
					while($r = $stmt->fetch('assoc')){
						if(!in_array($r['user_id'], $ids)){array_push($ids ,$r['user_id']);}
					}
					//return $ids;
					$ids = implode(',', $ids);
					$get = $conn->execute("SELECT * from hiring where FIND_IN_SET(user_id , '$ids')");
					$ids = array();
					while($r = $get->fetch('assoc')){
						if(!in_array($r["user_id"] , $ids)){array_push($ids , $r['user_id']);}
					}
					$ids = implode(',', $ids);
					$idnass = array();
					$get = $conn->execute("SELECT * from assessment_testmark where FIND_IN_SET(user_id , '$ids')");
					while($r = $get->fetch('assoc')){
						$key = "#".$r['user_id'];
						if(isset($idnass[$key])){
							if(!in_array($r['assessment_id'], $idnass[$key]) ){array_push($idnass[$key] , $r['assessment_id']);}
						}else{
							$idnass[$key] = array();
							array_push($idnass[$key] , $r['assessment_id']);
						}
					}
					//return $idnass;
					$arr = array();
					foreach($idnass as $key => $value){
						//return $r;
						//implementation
						//return ltrim($key , "#");
						//$id = $r['assessment_type_id'];
						$student_report = [];
						$user_id = ltrim($key , "#");
						$income = 0;
						$count = 0;
						$expenses = 0;
						foreach($value as $id){
							$tempData = [];
							$tempDatas = [];
							$tempDatass = [];
							
							$percentage = $percentile = 0;
							$total_marks_per_questions = 0;
							$results = $conn->execute(" SELECT COUNT(category)as question_per_cat, category,(SELECT per_q_mark FROM lztopics WHERE id =" . $id . ") as per_q_mark  FROM `lzquestions` WHERE topic_id=" . $id . " GROUP by category");
							$results = $results->fetchAll('assoc');
							//return $results;
							$max_marks = $conn->execute("SELECT MAX(mark) as max_marks,category FROM assessment_testmark WHERE assessment_id=" . $id . " GROUP BY category");
							$max_marks = $max_marks->fetchAll('assoc');

							$all_students = $conn->execute("SELECT category,mark,user_id FROM assessment_testmark WHERE assessment_id=" . $id . " AND user_id=" . $user_id);
							$all_students = $all_students->fetchAll('assoc');
							//$user_name = \DB::table('users')
							    //->select('user_fullname')->where('user_id', '=', $user_id)->get();

							foreach ($results as $res) {
							    $tempDatas[$res["category"]] = array(
							        'question_per_cat' => $res["question_per_cat"],
							        'per_q_mark' => $res["per_q_mark"],
							    );
							}
							foreach ($max_marks as $value) {
							    $tempDatass[$value["category"]] = array(
							        'max_marks' => $value["max_marks"],
							    );
							}
							foreach ($all_students as $student) {
							    $tempData[$student["user_id"]][$student["category"]] = array(
							        'mark' => $student["mark"],
							    );
							}
							
							        //    echo "<pre>";
							        //    print_r($tempData);
							        //    print_r($tempDatas);
							        //    print_r($tempDatass);
							        //    die;

							foreach ($tempData as $key => $students) {
							    foreach ($students as $key1 => $student) {

							        if (array_key_exists($key1, $tempDatas)) {

							            $total_marks_per_questions = $tempDatas[$key1]['question_per_cat'] * $tempDatas[$key1]['per_q_mark'];

							            if ($total_marks_per_questions) {
							                $percentage = round(($students[$key1]['mark'] * 100) / $total_marks_per_questions, 2);
							            } else {
							                $percentage = 0;
							            }

							            if ($tempDatass[$key1]['max_marks']) {
							                $percentile = round(($students[$key1]['mark'] * 100) / $tempDatass[$key1]['max_marks'], 2);
							            } else {
							                $percentile = 0;
							            }
							            $income += $percentage;
							            $expenses += $percentile;
							            $count++;
							            $student_report[] = array(
							                'year' => $conn->execute("SELECT text FROM category_mst WHERE category_id=" . $key1)->fetchAll('assoc')[0]["text"],
							                'income' => $percentage,
							                'expenses' => $percentile,
							            );
							        }
							    }
							}
						}
						$r['percentage'] = "".round($income/$count , 2 );
						$r['percentile'] = "".round($expenses/$count , 2 );
						$r['student_report'] = $student_report;
						$dat = $pd->userData($userId , $key , 'user' , 0);
						$r['creator_data'] = isset($dat[0]) ? $dat[0] : null;
						array_push($arr , $r);
					}
					return $arr;
				

			}

			Log::debug("Ended ...collegeStudentList Dao");

			//return $result;

		}catch(\Exception $e){
			Log::debug($e);
			throw new Exception($e);
		}
	}


	

}