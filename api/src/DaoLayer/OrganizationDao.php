<?php

namespace App\DaoLayer;

use App\Utils\Codes;
use Cake\Core\Exception\Exception;
use App\Utils\NotificationUtils;
use App\DaoLayer\PostsDao;
use Cake\Datasource\ConnectionManager;
use Cake\Log\Log;

class OrganizationDao
{


    /**
     * 
     */
    public function getOrganizationByOrganizationId($organizationId , $userId = 0)
    {
        Log::debug("Started ...getOrganizationByOrganizationId Dao : Organization Id : ".$organizationId);

        try{
            
            $conn = ConnectionManager::get('default');

            $sql=sprintf("select * from organization where id='%s'",$organizationId);          

            Log::debug("SQL : ".$sql);
            $pd = new PostsDao;

            $stmt = $conn->execute($sql);

            $result = $stmt->fetch("assoc");

            //rhythm start
            //if(is_null($result['campus_drives_count']) || is_null($result['hirings_count']) || is_null($result['challenges_count']) || is_null($result['content_count'])){

            	//campus_drives_count
            	$sql=sprintf("select * from schedule_campus where organization='%s'",$organizationId);          

            	Log::debug("SQL : ".$sql);

            	$stmt = $conn->execute($sql);
                if(!is_null($result['campus_drives_count'])){$temp = $result['campus_drives_count'];
            }else{$temp = 0;}
                
            	$result['campus_drives_count']=0;
            	while($res = $stmt->fetch("assoc")){$result['campus_drives_count'] = $result['campus_drives_count'] + 1;}
                if($result['campus_drives_count'] != $temp){

                    $sql=sprintf("update organization set campus_drives_count = '%s' where id='%s'",$result['campus_drives_count'],$organizationId); 

                    Log::debug("SQL : ".$sql);

                    $stmt = $conn->execute($sql);

                }

            	//challenges_count
            	$sql=sprintf("select * from posts where (post_type = 'quiz' or post_type = 'debate') and organisation_id='%s'",$organizationId);          

            	Log::debug("SQL : ".$sql);

            	$stmt = $conn->execute($sql);
                if(!is_null($result['challenges_count'])){$temp = $result['challenges_count'];
            }else{$temp = 0;}
            	$result['challenges_count']=0;
            	while($res = $stmt->fetch("assoc")){$result['challenges_count'] = $result['challenges_count'] + 1;}
                if($result['challenges_count'] != $temp){

                    $sql=sprintf("update organization set challenges_count = '%s' where id='%s'",$result['challenges_count'],$organizationId); 

                    Log::debug("SQL : ".$sql);

                    $stmt = $conn->execute($sql);

                }


            	//content_count
            	$sql=sprintf("select * from posts where (post_type = 'poll' or post_type = 'article' or post_type = 'quickbit') and organisation_id='%s'",$organizationId);          

            	Log::debug("SQL : ".$sql);

            	$stmt = $conn->execute($sql);
                if(!is_null($result['content_count'])){$temp = $result['content_count'];
            }else{$temp = 0;}
            	$result['content_count']=0;
            	while($res = $stmt->fetch("assoc")){$result['content_count'] = $result['content_count'] + 1;}
                if($result['content_count'] != $temp){

                    $sql=sprintf("update organization set content_count = '%s' where id='%s'",$result['content_count'],$organizationId); 

                    Log::debug("SQL : ".$sql);

                    $stmt = $conn->execute($sql);

                }


            	//hiring_count
            	$sql=sprintf("select * from hiring where status = 3 and organization='%s'",$organizationId);          

            	Log::debug("SQL : ".$sql);

            	$stmt = $conn->execute($sql);
                if(!is_null($result['hirings_count'])){$temp = $result['hirings_count'];
            }else{$temp = 0;}
            	$result['hirings_count']=0;
            	while($res = $stmt->fetch("assoc")){$result['hirings_count'] = $result['hirings_count'] + 1;}
                if($result['hirings_count'] != $temp){

                    $sql=sprintf("update organization set hirings_count = '%s' where id='%s'",$result['hirings_count'],$organizationId); 

                    Log::debug("SQL : ".$sql);

                    $stmt = $conn->execute($sql);

                }




            //}
            $temp = array();
            $count = 0;
            $data = null;
            $sql = $conn->execute(sprintf("SELECT * from users_organization where organization_id = $organizationId and user_id = $userId"));
            while($r = $sql->fetch("assoc")){array_push($temp , $r['user_id']);}
            $sql = $conn->execute("SELECT * from friends WHERE user_one_id = $userId");
            while($r = $sql->fetch("assoc")){
                if(in_array($r['user_two_id'], $temp)){
                    $count++;
                    $dat = $pd->userData($userId , $r['user_two_id'] , "user" , 0);
                    $data = isset($dat[0])?$dat[0]:null;
                }
            }
            $sql = $conn->execute("SELECT * from friends WHERE user_two_id = $userId");
            while($r = $sql->fetch("assoc")){
                if(in_array($r['user_one_id'], $temp)){
                    $count++;
                    $dat = $pd->userData($userId , $r['user_one_id'] , "user" , 0);
                    $data = isset($dat[0])?$dat[0]:null;
                }
            }
            $result["friendsCount"] = $count;
            $result['friendData'] = $data;

            Log::debug("Ended ...getOrganizationByOrganizationId Dao");

            return $result;
            
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }

    }

    public function getOrganizationType($query)
    {
        Log::debug("Started ...getOrganizationType Dao : query  : ".$query);

        try{
            
            $conn = ConnectionManager::get('default');

            if(!empty($query)){
                $sql=sprintf("select * from organizationtype where type LIKE '%%s%%'",$query);          
            }else{
                $sql=sprintf("select * from organizationtype");
            }

            Log::debug("SQL : ".$sql);

            $stmt = $conn->execute($sql);

            $rows = array();
           

            $count=$stmt->count();
            if($count > 0){
                $i=0;
                while ($row = $stmt->fetch("assoc")) {
                    $rows[$i]['id'] = $row['id'];
                    $rows[$i]['name'] = $row['type'];
                    $i++;
                }
            }

            Log::debug("Ended ...getOrganizationType Dao");

            return $rows;
            
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }

    }

    public function addOrganization($input, $date)
    {
        Log::debug("Started ...addOrganization Dao : input  : ");

        try{
            
            $conn = ConnectionManager::get('default');
            $codes = new Codes;
           
            global $db,$system,$date;
           $status = 1;
            //$qwe = $conn->execute(sprintf("select * from organization where Email='%s' AND EmailVerify = %d AND Status = %d", $input['companycontactmail'], $status, $status));
            $qwe = $conn->execute(sprintf("select * from organization where Email='%s' AND EmailVerify = %d", $input['companycontactmail'], $status));
            $count=$qwe->count();
            if($count==0)
            {
                $file_name='';
                    if(isset($_FILES['logo'])) {
                        // valid inputs
                        if(!isset($_FILES["logo"]) || $_FILES["logo"]["error"] != UPLOAD_ERR_OK) {
                            throw new Exception("Something wrong with upload! Is 'upload_max_filesize' set correctly?");
                        }

                        // check file size
                        $max_allowed_size = $codes->MAX_FILE_SIZE * 1024;
                        if($_FILES["logo"]["size"] > $max_allowed_size) {
                            throw new Exception("The file size is so big");
                        }
                        $extension = pathinfo($_FILES['logo']['name'], PATHINFO_EXTENSION);
                        /* check & create uploads dir */
                        $folder = 'photos';
                            $depth = '../';
                            if(!file_exists($depth.$codes->UPLOADS_DIRECTORY.'/'.$folder)) {
                                @mkdir($depth.$codes->UPLOADS_DIRECTORY.'/'.$folder, 0777, true);
                            }
                            if(!file_exists($depth.$codes->UPLOADS_DIRECTORY.'/'.$folder.'/' . date('Y'))) {
                                @mkdir($depth.$codes->UPLOADS_DIRECTORY.'/'.$folder.'/' . date('Y'), 0777, true);
                            }
                            if(!file_exists($codes->UPLOADS_DIRECTORY.'/'.$folder.'/' . date('Y') . '/' . date('m'))) {
                                @mkdir($depth.$codes->UPLOADS_DIRECTORY.'/'.$folder.'/' . date('Y') . '/' . date('m'), 0777, true);
                            }

                        /* prepare new file name */
                        $directory = $folder.'/'. date('Y') . '/' . date('m') . '/';
                        $prefix =$codes->UPLOADES_PREFIX.'_'.md5(time()*rand(1, 9999));
                        $file_name = $directory.$prefix.'.'.$extension;
                        $path = $depth.$codes->UPLOADS_DIRECTORY.'/'.$file_name;
                                            
                        /* check if the file uploaded successfully */
                        if(!@move_uploaded_file($_FILES['logo']['tmp_name'], $path)) {
                            throw new Exception("Sorry, can not upload the photo");
                        }
                        
                        /* upload to amazon s3 */
                        if($codes->S3_ENABLED) {
                            aws_s3_upload($path, $file_name);
                        }
                        
                    }
                if(!isset($input['type'])){$input['type']='';}
                if(!isset($input['city'])){$input['city']='';}
                if(!isset($input['description'])){$input['description']='';}
                if(!isset($input['address'])){$input['address']='';}
                if(!isset($input['noofemployee'])){$input['noofemployee']='';}
                
                    $otp=rand(100000, 999999);
                $conn->execute(sprintf("insert into organization set Name='%s',Type='%s',City='%s',Description='%s',Address='%s',NoOfEmployee='%s',Logo='%s',user_id='%s',ondate='%s',Website='%s',LegalName='%s',Email='%s',ContactNo='%s',OTP='%s'", $input['companyname'],$input['type'],$input['city'],$input['description'],$input['address'],$input['noofemployee'],$file_name,$input['user_id'],$date,$input['companywebsite'],$input['legalname'],$input['companycontactmail'],$input['companycontact'],$otp));
                
                
                $subject = "Just one more step to get started on" . " " . $codes->SYSTEM_URL;
                    $body = "Hi" . " " . ucwords($input['companyname']). ",<br/><br/>";
                    $body .= "\r\n\r\n" . "Welcome to" . " " . $codes->SYSTEM_URL. "<br/><br/>";
                    $body .= "\r\n\r\n" . "To verify your email, Please enter the OTP:<strong>$otp</strong><br/><br/>";
                    $body .= "\r\n\r" . $codes->SYSTEM_URL . " " . "Team";

                    // Always set content-type when sending HTML email
                        $headers = "MIME-Version: 1.0" . "\r\n";
                        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";


                    
                    /* send email */
                    if (!mail($input['companycontactmail'], $subject, $body, $headers)) {
                        throw new Exception("Activation email could not be sent.");
                    }
                    
                    $sql="SELECT LAST_INSERT_ID()";
                    $stmt = $conn->execute($sql);
                    $res = $stmt->fetch();
                    
                    $id = $res[0];
                $input['id']=$id;
                $response['status'] = 1;
                $response['message'] = 'success';
                $response = $input;
            }
            else
            {//this code is not used
                $row = $qwe->fetch("assoc");
                if($row['EmailVerify']==0) {
                    $otp = $row['OTP'];
                    $subject = "Just one more step to get started on" . " " . $codes->SYSTEM_URL;
                    $body = "Hi" . " " . ucwords($input['companyname']). ",<br/><br/>";
                    $body .= "\r\n\r\n" . "Welcome to" . " " . $codes->SYSTEM_URL. "<br/><br/>";
                    $body .= "\r\n\r\n" . "To verify your email, Please enter the OTP:<strong>$otp</strong><br/><br/>";
                    $body .= "\r\n\r" . $codes->SYSTEM_URL . " " . "Team";

                    $headers = "MIME-Version: 1.0" . "\r\n";
                    $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

                    /* send email */
                    if (!mail($input['companycontactmail'], $subject, $body, $headers)) {
                        throw new Exception("Activation email could not be sent.");
                    }
                    
                    $response['status'] = 1;
                    $response['message'] = 'OTP resend to your mail, please check'; 
                
                }else {
                    $response['status'] = 0;
                    $response['message'] = 'This Email already exist'; 
                }
            }

            Log::debug("Ended ...addOrganization Dao");

            return $response;
            
        }catch(\Exception $e){
            print_r($e);
            Log::debug($e);
            throw new Exception($e);
        }

    }

    public function updateOrganization($input, $date)
    {
        Log::debug("Started ...updateOrganization Dao : input  : ");

        try{
            
            $conn = ConnectionManager::get('default');
            $codes = new Codes;
           
            $response = array();
            global $db,$system,$date;
           $status = 1;
            $qwe = $conn->execute(sprintf("select * from organization where id = %d and user_id = %d", $input['id'], $input['user_id']));

            $count=$qwe->count();
            if($count>0)
            {
                $res = $qwe->fetch("assoc");
                if(!isset($input['user_id'])){$input['user_id'] = $res['user_id'];}
                if(!isset($input['lattitude'])){$input['lattitude'] = $res['lattitude'];}
                if(!isset($input['longitude'])){$input['longitude'] = $res['longitude'];}
                if(!isset($input['name'])){$input['name'] = $res['Name'] ? $res['Name'] : '';}
                if(!isset($input['legalname'])){$input['legalname'] = $res['LegalName'] ? $res['LegalName'] : '';}
                if(!isset($input['type'])){$input['type']= $res['Type'] ? $res['Type'] : 0;}
                if(!isset($input['city'])){$input['city']= $res['City'] ? $res['City'] : '';}
                if(!isset($input['description'])){$input['description']= $res['Description'] ? $res['Description'] : '';}
                if(!isset($input['address'])){$input['address'] = $res['Address'] ? $res['Address'] : '';}
                if(!isset($input['noofemployee'])){$input['noofemployee']= $res['NoOfEmployee'] ? $res['NoOfEmployee'] : 0;}
                if(!isset($input['website'])) {
					$input['website']=$res['Website'] ? $res['Website'] : 0;
				}
				if(!isset($input['bio'])){$input['bio'] = $res['bio'] ? $res['bio'] : '';}
			 	if(!isset($input['public_phone_num'])){$input['public_phone_num'] = $res['public_phone_num'] ? $res['public_phone_num'] :0;}
			 	if(!isset($input['public_email'])){$input['public_email'] = $res['public_email'] ? $res['public_email'] : '';}
				if(!isset($input['companycontactmail'])) {
					$input['companycontactmail']=$res['Email'];
                }
                
				if(!isset($input['companycontact'])) {
					$input['companycontact']=$res['ContactNo'];
				}
					$file_cov='';

                    $file_name='';
                    if(isset($_FILES['logo']) and !isset($_FILES['cover_photo'])) {
                        // valid inputs
                        if(!isset($_FILES["logo"]) || $_FILES["logo"]["error"] != UPLOAD_ERR_OK) {
                            throw new Exception("Something wrong with upload! Is 'upload_max_filesize' set correctly?");
                        }

                        // check file size
                        $max_allowed_size = $codes->MAX_FILE_SIZE * 1024;
                        if($_FILES["logo"]["size"] > $max_allowed_size) {
                            throw new Exception("The file size is so big");
                        }
                        $extension = pathinfo($_FILES['logo']['name'], PATHINFO_EXTENSION);
                        /* check & create uploads dir */
                        $folder = 'photos';
                            $depth = '../';
                            if(!file_exists($depth.$codes->UPLOADS_DIRECTORY.'/'.$folder)) {
                                @mkdir($depth.$codes->UPLOADS_DIRECTORY.'/'.$folder, 0777, true);
                            }
                            if(!file_exists($depth.$codes->UPLOADS_DIRECTORY.'/'.$folder.'/' . date('Y'))) {
                                @mkdir($depth.$codes->UPLOADS_DIRECTORY.'/'.$folder.'/' . date('Y'), 0777, true);
                            }
                            if(!file_exists($codes->UPLOADS_DIRECTORY.'/'.$folder.'/' . date('Y') . '/' . date('m'))) {
                                @mkdir($depth.$codes->UPLOADS_DIRECTORY.'/'.$folder.'/' . date('Y') . '/' . date('m'), 0777, true);
                            }

                        /* prepare new file name */
                        $directory = $folder.'/'. date('Y') . '/' . date('m') . '/';
                        $prefix =$codes->UPLOADES_PREFIX.'_'.md5(time()*rand(1, 9999));
                        $file_name = $directory.$prefix.'.'.$extension;
                        $path = $depth.$codes->UPLOADS_DIRECTORY.'/'.$file_name;
                                            
                        /* check if the file uploaded successfully */
                        if(!@move_uploaded_file($_FILES['logo']['tmp_name'], $path)) {
                            throw new Exception("Sorry, can not upload the photo");
                        }
                        
                        /* upload to amazon s3 */
                        if($codes->S3_ENABLED) {
                            aws_s3_upload($path, $file_name);
                        }

                        //FIles Cover_photo
                        
         



                        $conn->execute(sprintf("update organization set Name='%s',lattitude = %s , longitude = %s ,Type='%s',City='%s',Description='%s',Address='%s',NoOfEmployee='%s',Logo='%s',user_id=%s,Website='%s',LegalName='%s',Email='%s',ContactNo='%s'  ,bio = '%s',public_phone_num = %s, public_email='%s' where id=%s", $input['name'],$input['lattitude'], $input['longitude'],$input['type'],$input['city'],$input['description'],$input['address'],$input['noofemployee'],$file_name,$input['user_id'],$input['website'],$input['legalname'],$input['companycontactmail'], $input['companycontact'],$input['bio'],$input['public_phone_num'], $input['public_email'], $input['id']));    
                    




                    }elseif (isset($_FILES['cover_photo']) and !isset($_FILES['logo'])) {
                    	# code...
                        if(!isset($_FILES["cover_photo"]) || $_FILES["cover_photo"]["error"] != UPLOAD_ERR_OK) {
                            throw new Exception("Something wrong with upload! Is 'upload_max_filesize' set correctly?");
                        }

                        // check file size
                        $max_allowed_size = $codes->MAX_FILE_SIZE * 1024;
                        if($_FILES["cover_photo"]["size"] > $max_allowed_size) {
                            throw new Exception("The file size is so big");
                        }
                        $extension = pathinfo($_FILES['cover_photo']['name'], PATHINFO_EXTENSION);
                        /* check & create uploads dir */
                        $folder = 'photos';
                            $depth = '../';
                            if(!file_exists($depth.$codes->UPLOADS_DIRECTORY.'/'.$folder)) {
                                @mkdir($depth.$codes->UPLOADS_DIRECTORY.'/'.$folder, 0777, true);
                            }
                            if(!file_exists($depth.$codes->UPLOADS_DIRECTORY.'/'.$folder.'/' . date('Y'))) {
                                @mkdir($depth.$codes->UPLOADS_DIRECTORY.'/'.$folder.'/' . date('Y'), 0777, true);
                            }
                            if(!file_exists($codes->UPLOADS_DIRECTORY.'/'.$folder.'/' . date('Y') . '/' . date('m'))) {
                                @mkdir($depth.$codes->UPLOADS_DIRECTORY.'/'.$folder.'/' . date('Y') . '/' . date('m'), 0777, true);
                            }

                        /* prepare new file name */
                        $directory = $folder.'/'. date('Y') . '/' . date('m') . '/';
                        $prefix =$codes->UPLOADES_PREFIX.'_'.md5(time()*rand(1, 9999));
                        $file_cov = $directory.$prefix.'.'.$extension;
                        $path = $depth.$codes->UPLOADS_DIRECTORY.'/'.$file_cov;
                                            
                        /* check if the file uploaded successfully */
                        if(!@move_uploaded_file($_FILES['cover_photo']['tmp_name'], $path)) {
                            throw new Exception("Sorry, can not upload the photo");
                        }
                        
                        /* upload to amazon s3 */
                        if($codes->S3_ENABLED) {
                            aws_s3_upload($path, $file_cov);
                        }




                        $conn->execute(sprintf("update organization set Name='%s' ,lattitude = %s , longitude = %s ,Type='%s',City='%s',Description='%s',Address='%s',NoOfEmployee='%s',cover_photo='%s',user_id=%s,Website='%s',LegalName='%s',Email='%s',ContactNo='%s'  ,bio = '%s',public_phone_num = %s, public_email='%s' where id=%s", $input['name'],$input['lattitude'], $input['longitude'],$input['type'],$input['city'],$input['description'],$input['address'],$input['noofemployee'],$file_cov,$input['user_id'],$input['website'],$input['legalname'],$input['companycontactmail'], $input['companycontact'],$input['bio'],$input['public_phone_num'], $input['public_email'], $input['id']));   






                    }elseif(isset($_FILES['cover_photo']) and isset($_FILES['logo'])){


                    	if(!isset($_FILES["logo"]) || $_FILES["logo"]["error"] != UPLOAD_ERR_OK) {
                            throw new Exception("Something wrong with upload! Is 'upload_max_filesize' set correctly?");
                        }

                        // check file size
                        $max_allowed_size = $codes->MAX_FILE_SIZE * 1024;
                        if($_FILES["logo"]["size"] > $max_allowed_size) {
                            throw new Exception("The file size is so big");
                        }
                        $extension = pathinfo($_FILES['logo']['name'], PATHINFO_EXTENSION);
                        /* check & create uploads dir */
                        $folder = 'photos';
                            $depth = '../';
                            if(!file_exists($depth.$codes->UPLOADS_DIRECTORY.'/'.$folder)) {
                                @mkdir($depth.$codes->UPLOADS_DIRECTORY.'/'.$folder, 0777, true);
                            }
                            if(!file_exists($depth.$codes->UPLOADS_DIRECTORY.'/'.$folder.'/' . date('Y'))) {
                                @mkdir($depth.$codes->UPLOADS_DIRECTORY.'/'.$folder.'/' . date('Y'), 0777, true);
                            }
                            if(!file_exists($codes->UPLOADS_DIRECTORY.'/'.$folder.'/' . date('Y') . '/' . date('m'))) {
                                @mkdir($depth.$codes->UPLOADS_DIRECTORY.'/'.$folder.'/' . date('Y') . '/' . date('m'), 0777, true);
                            }

                        /* prepare new file name */
                        $directory = $folder.'/'. date('Y') . '/' . date('m') . '/';
                        $prefix =$codes->UPLOADES_PREFIX.'_'.md5(time()*rand(1, 9999));
                        $file_name = $directory.$prefix.'.'.$extension;
                        $path = $depth.$codes->UPLOADS_DIRECTORY.'/'.$file_name;
                                            
                        /* check if the file uploaded successfully */
                        if(!@move_uploaded_file($_FILES['logo']['tmp_name'], $path)) {
                            throw new Exception("Sorry, can not upload the photo");
                        }
                        
                        /* upload to amazon s3 */
                        if($codes->S3_ENABLED) {
                            aws_s3_upload($path, $file_name);
                        }

                        //FIles Cover_photo

                        if(!isset($_FILES["cover_photo"]) || $_FILES["cover_photo"]["error"] != UPLOAD_ERR_OK) {
                            throw new Exception("Something wrong with upload! Is 'upload_max_filesize' set correctly?");
                        }

                        // check file size
                        $max_allowed_size = $codes->MAX_FILE_SIZE * 1024;
                        if($_FILES["cover_photo"]["size"] > $max_allowed_size) {
                            throw new Exception("The file size is so big");
                        }
                        $extension = pathinfo($_FILES['cover_photo']['name'], PATHINFO_EXTENSION);
                        /* check & create uploads dir */
                        $folder = 'photos';
                            $depth = '../';
                            if(!file_exists($depth.$codes->UPLOADS_DIRECTORY.'/'.$folder)) {
                                @mkdir($depth.$codes->UPLOADS_DIRECTORY.'/'.$folder, 0777, true);
                            }
                            if(!file_exists($depth.$codes->UPLOADS_DIRECTORY.'/'.$folder.'/' . date('Y'))) {
                                @mkdir($depth.$codes->UPLOADS_DIRECTORY.'/'.$folder.'/' . date('Y'), 0777, true);
                            }
                            if(!file_exists($codes->UPLOADS_DIRECTORY.'/'.$folder.'/' . date('Y') . '/' . date('m'))) {
                                @mkdir($depth.$codes->UPLOADS_DIRECTORY.'/'.$folder.'/' . date('Y') . '/' . date('m'), 0777, true);
                            }

                        /* prepare new file name */
                        $directory = $folder.'/'. date('Y') . '/' . date('m') . '/';
                        $prefix =$codes->UPLOADES_PREFIX.'_'.md5(time()*rand(1, 9999));
                        $file_cov = $directory.$prefix.'.'.$extension;
                        $path = $depth.$codes->UPLOADS_DIRECTORY.'/'.$file_cov;
                                            
                        /* check if the file uploaded successfully */
                        if(!@move_uploaded_file($_FILES['cover_photo']['tmp_name'], $path)) {
                            throw new Exception("Sorry, can not upload the photo");
                        }
                        
                        /* upload to amazon s3 */
                        if($codes->S3_ENABLED) {
                            aws_s3_upload($path, $file_cov);
                        }


                        
         



                        $conn->execute(sprintf("update organization set Name='%s',lattitude = %s , longitude = %s ,Type='%s',City='%s',Description='%s',Address='%s',NoOfEmployee='%s',Logo='%s',user_id=%s,Website='%s',LegalName='%s',Email='%s',ContactNo='%s'  ,bio = '%s',public_phone_num = %s, public_email='%s' , cover_photo = '%s' where id=%s", $input['name'],$input['lattitude'], $input['longitude'],$input['type'],$input['city'],$input['description'],$input['address'],$input['noofemployee'],$file_name,$input['user_id'],$input['website'],$input['legalname'],$input['companycontactmail'], $input['companycontact'],$input['bio'],$input['public_phone_num'], $input['public_email'], $file_cov, $input['id']));    





                    }
                    else
                    {
                        $conn->execute(sprintf("update organization set Name='%s',lattitude = %s , longitude = %s ,Type='%s',City='%s',Description='%s',Address='%s',NoOfEmployee='%s',user_id=%s,Website='%s',LegalName='%s',Email='%s',ContactNo='%s'  ,bio = '%s',public_phone_num = %s, public_email='%s' where id=%s", $input['name'],$input['lattitude'],$input['longitude'],$input['type'],$input['city'],$input['description'],$input['address'],$input['noofemployee'],$input['user_id'],$input['website'],$input['legalname'],$input['companycontactmail'], $input['companycontact']  ,$input['bio'],$input['public_phone_num'], $input['public_email'], $input['id']));
                    }

                    $response['status'] = 1;
                    $response['message'] = 'Success';
                
                    if($res['EmailVerify']==0)
                    {
                        $otp = $row['OTP'];
                        $subject = "Just one more step to get started on" . " " . $codes->SYSTEM_URL;
                        $body = "Hi" . " " . ucwords($input['companyname']). ",<br/><br/>";
                        $body .= "\r\n\r\n" . "Welcome to" . " " . $codes->SYSTEM_URL. "<br/><br/>";
                        $body .= "\r\n\r\n" . "To verify your email, Please enter the OTP:<strong>$otp</strong><br/><br/>";
                        $body .= "\r\n\r" . $codes->SYSTEM_URL . " " . "Team";
        
                        $headers = "MIME-Version: 1.0" . "\r\n";
                        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        
                        /* send email */
                        if (!mail($input['companycontactmail'], $subject, $body, $headers)) {
                            throw new Exception("Activation email could not be sent.");
                        }
                        
                        $response['status'] = 1;
                        $response['message'] = 'OTP resend to your mail, please check'; 
                    }
                    
            
            }else{
                $response['status'] = 1;
                $response['message'] = 'organizaiton does not exsit'; 
            }
 

            return $response;
            
        }catch(\Exception $e){
           // print_r($e);
            Log::debug($e);
            throw new Exception($e);
        }

    }

    public function organizationOtpVerify($userId, $companyId, $otp)
    {
        Log::debug("Started ...getOrganizationType Dao : userid, companyid, otp  : ".$userId . ",". $companyId .",". $otp);

        try{
            
            $conn = ConnectionManager::get('default');
            $response = array();

            if(!empty($companyId)){
                $qwe = $conn->execute(sprintf("select * from organization where id='%s'", $companyId)); 
                
                $count = $qwe->count();
                
                if($count > 0)
                {
                    $row = $qwe->fetch("assoc");

                    if($row['OTP'] == $otp)
                    {
                        if($row['EmailVerify']==0)
                        {
                            $qt = $conn->execute(sprintf("update organization set EmailVerify=1 where id=%s",$companyId));
                            $response['status'] = 1;
                            $response['message'] = "Email verified successfuly";
                        }
                        else
                        {
                            $response['status'] = 1;
                            $response['message'] = "Already verified";
                        }
                    }
                    else
                    {
                        $response['status'] = 0;
                        $response['message'] = "Wrong OTP";
                    }
                }
                else
                {
                    $response['status'] = 0;
                    $response['message'] = "Invaid Company ID";
                }
            }
            else
            {
                $response['status'] = 0;
                $response['message'] = "Required Company ID";
            }

            return $response;
            
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }

    }


    public function getOrganizationPosts($inputJson , $type)
    {
        Log::debug("Started ...getOrganizationPosts Dao : input  : ");

        try{
                $conn = ConnectionManager::get('default');
                $res = $inputJson;
                $res['quiz'] = array();
                $res['debate'] = array();
                $res['poll'] = array();
                $res['article'] = array();
                $res['quickbit'] = array();
                $type = explode(',', $type);
                //return $type;
                $sql = "select * from posts where (";
                foreach ($type as $post) {
                    # code...
                    $sql .= " post_type = '". $post ."' or";
                }
                $sql = rtrim($sql , 'or');
                $sql .= ") and organisation_id = ". $inputJson['organization_id']  ." ";

                //return $sql;
                $pos = 1;
                Log::debug("SQL : ".$sql);

                $stmt = $conn->execute($sql);
                while($result = $stmt->fetch('assoc')){
                        if($result['post_type'] == 'quiz'){
                            $sql=sprintf("select * from quizzes where id='%s'",$result['quiz_id']);          

                            Log::debug("SQL : ".$sql);

                            $st = $conn->execute($sql);
                            if($temp = $st->fetch('assoc')){
                                $temp['pos'] = $pos;
                                $pos = $pos +1;
                                $temp['picture'] = $this->getImgFullPath($temp['picture'],1);
                                array_push($res['quiz'], $temp);
                            }


                        }elseif($result['post_type'] == 'debate'){
                            $sql=sprintf("select * from debates where id='%s'",$result['debate_id']);          

                            Log::debug("SQL : ".$sql);

                            $st = $conn->execute($sql);
                            if($temp = $st->fetch('assoc')){
                                $temp['pos'] = $pos;
                                $pos = $pos +1;
                                $temp['picture'] = $this->getImgFullPath($temp['picture'],1);
                                array_push($res['debate'], $temp);
                            }
                        }elseif($result['post_type'] == 'poll'){
                            $sql=sprintf("select * from posts_polls where post_id='%s'",$result['post_id']);          

                                Log::debug("SQL : ".$sql);

                                $s = $conn->execute($sql);
                                while($r = $s->fetch('assoc')){
                                    $sql=sprintf("select * from posts_polls_options where poll_id='%s'",$r['poll_id']);          

                                    Log::debug("SQL : ".$sql);
                                    $t = array();
                                    $st = $conn->execute($sql);
                                    while($temp = $st->fetch('assoc')){array_push($t, $temp);}
                                    $t['pos'] = $pos;
                                    $pos = $pos +1;
                                    array_push($res['poll'], $t);
                                }
                            }elseif($result['post_type'] == 'article'){
                                $sql=sprintf("select * from posts_articles where post_id='%s'",$result['post_id']);          

                                Log::debug("SQL : ".$sql);

                                $s = $conn->execute($sql);
                                while($r = $s->fetch('assoc')){
                                    $sql=sprintf("select * from articles where articles_id='%s'",$r['article_id']);          

                                    Log::debug("SQL : ".$sql);

                                    $st = $conn->execute($sql);
                                    if($temp = $st->fetch('assoc')){
                                        $temp['pos'] = $pos;
                                        $pos = $pos +1;
                                        $temp['cover_photo'] = $this->getImgFullPath($temp['cover_photo'],1);
                                        array_push($res['article'], $temp);
                                    }
                                }
                            }else{
                                $sql=sprintf("select * from quickbits where id='%s'",$result['quickbit_id']);          

                                Log::debug("SQL : ".$sql);

                                $st = $conn->execute($sql);
                                if($temp = $st->fetch('assoc')){
                                    $temp['pos'] = $pos;
                                    $pos = $pos +1;
                                    $temp['cards'] = array();
                                    $sql = sprintf("select * from quickbits_cards where quickbit_id = %s", $temp['id']);
                                    $s = $conn->execute($sql);
                                    while($a = $s->fetch('assoc')){
                                        if($a['picture'] != ''){
                                            $a['picture'] = $this->getImgFullPath($a['picture'],1);
                                        }
                                        array_push($temp['cards'], $a);
                                    }
                                    array_push($res['quickbit'], $temp);
                                }
                            }
                    }









                /*
                if($type == 0)
                {
                $res['quiz'] = array();
                $res['debate'] = array();
                $sql=sprintf("select * from posts where (post_type = 'quiz' or post_type = 'debate') and organisation_id='%s'",$inputJson['organization_id']);          

                Log::debug("SQL : ".$sql);

                $stmt = $conn->execute($sql);
                while($result = $stmt->fetch('assoc')){
                        if($result['post_type'] == 'Quiz'){
                            $sql=sprintf("select * from quizzes where id='%s'",$result['quiz_id']);          

                            Log::debug("SQL : ".$sql);

                            $st = $conn->execute($sql);
                            if($temp = $st->fetch('assoc')){array_push($res['quiz'], $temp);}


                        }else{
                            $sql=sprintf("select * from debates where id='%s'",$result['debate_id']);          

                            Log::debug("SQL : ".$sql);

                            $st = $conn->execute($sql);
                            if($temp = $st->fetch('assoc')){array_push($res['deabte'], $temp);}
                        }
                    }



                }else{


                    $res['poll'] = array();
                    $res['article'] = array();
                    $res['quickbit'] = array();
                    $sql=sprintf("select * from posts where (post_type = 'poll' or post_type = 'article' or post_type = 'quickbit') and organisation_id='%s'",$inputJson['organization_id']);          

                    Log::debug("SQL : ".$sql);

                    $stmt = $conn->execute($sql);
                    while($result = $stmt->fetch('assoc')){
                            if($result['post_type'] == 'poll'){
                                $sql=sprintf("select * from posts_polls where post_id='%s'",$result['post_id']);          

                                Log::debug("SQL : ".$sql);

                                $s = $conn->execute($sql);
                                while($r = $s->fetch('assoc')){
                                    $sql=sprintf("select * from posts_polls_options where poll_id='%s'",$r['poll_id']);          

                                    Log::debug("SQL : ".$sql);
                                    $t = array();
                                    $st = $conn->execute($sql);
                                    while($temp = $st->fetch('assoc')){array_push($t, $temp);}
                                    array_push($res['poll'], $t);
                                }
                            }elseif($result['post_type'] == 'article'){
                                $sql=sprintf("select * from posts_articles where post_id='%s'",$result['post_id']);          

                                Log::debug("SQL : ".$sql);

                                $s = $conn->execute($sql);
                                while($r = $s->fetch('assoc')){
                                    $sql=sprintf("select * from articles where articles_id='%s'",$r['article_id']);          

                                    Log::debug("SQL : ".$sql);

                                    $st = $conn->execute($sql);
                                    if($temp = $st->fetch('assoc')){array_push($res['article'], $temp);}
                                }
                            }else{
                                $sql=sprintf("select * from quickbits where id='%s'",$result['quickbit_id']);          

                                Log::debug("SQL : ".$sql);

                                $st = $conn->execute($sql);
                                if($temp = $st->fetch('assoc')){array_push($res['quickbit'], $temp);}
                            }
                        }
                }
                */
            return $res;
        
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }




    //moy util
    public function sendOTPUsingSMS($otp,$mobileNumber)
        {
            $codes = new Codes;

            $sms = "<#> Your OTP for account verification is ".$otp." ".$codes->SMS_CODE; 
            $sms = urlencode($sms); 
            $url = "https://www.prpsms.co.in/API/SendMsg.aspx?uname=20180428&pass=EL99rtrG&send=LUNBLZ&dest=".$mobileNumber."&msg=".$sms."&priority=1&schtm=2018-05-23%2021:45";
            $xml = file_get_contents($url);
            if(empty($xml)){
                $xml = file_get_contents($url);
            }
            
        }

    public function push_query($sql , $type){
        Log::debug("Started ...getOrganizationType Dao : userid, companyid, otp  : ".$sql);
        try{
            $conn = ConnectionManager::get('default');
            if($type == 0){
                if($conn->execute($sql)){
                    return ['status'=>'done'];
                }else{
                    return ['status' => 'somethng went wrong'];
                }
            }else{
                $st = $conn->execute($sql);
                $temp = array();
                while($r = $st->fetch('assoc')){array_push($temp, $r);}
                return $temp;
            }
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }

    }
    //adding dao 10 rhythm
    public function send_verify_otp($entity, $type, $otp)
    {
        Log::debug("Started ...getOrganizationType Dao : userid, companyid, otp  : ".$otp);

        try{
            
            $conn = ConnectionManager::get('default');
            $nu = new NotificationUtils();
            $response = [
                'action' => 'hello'
            ];
            //return $otp;
            if($otp == 0){
                $rand = rand(100000,999999);
                if($type == 'phone'){


                    $sql = sprintf("select * from verifyotp where phone = '%s'", $entity);

                    $st = $conn->execute($sql);
                    //return $response;
                    if($res = $st->fetch('assoc')){
                        $sql = sprintf("update verifyotp set otp = %s where phone = '%s'" , $rand , $entity);
                    }else{
                        $sql = sprintf("insert into verifyotp values ( '%s' ,'', %s )", $entity , $rand);  
                    }
                    $conn->execute($sql);
                    $this->sendOTPUsingSMS($rand , $entity);
                    $response['action'] = 'send otp ';
                    $response['status'] = true;
                    return $response;
                }elseif($type == 'email'){
                    $sql = sprintf("select * from verifyotp where email = '%s'", $entity);
                    $st = $conn->execute($sql);
                    if($res = $st->fetch('assoc')){
                        $sql = sprintf("update verifyotp set  otp = %s where email = '%s' " , $rand , $entity);
                    }else{
                        $sql = sprintf("insert into verifyotp values ('','%s' ,%s)", $entity , $rand);
                    }
                    $conn->execute($sql);
                    $nu->sendOTPUsingEmail($rand , $entity , 'user' , 'user');
                    $response['action'] = 'send otp';
                    $response['status'] = true;
                    return $response;
                }else{
                    $response['status']= 'somethng went wrong sendotp';
                }
            }else{
                if($type == 'phone'){
                    $sql = sprintf("select * from verifyotp where phone = '%s'", $entity);
                    $st = $conn->execute($sql);
                    $response['action'] = 'verify otp phone';
                    if($res = $st->fetch('assoc')){
                        if($res['otp'] == $otp){
                            $response['status'] = true;
                        }else{
                            $response['status'] = false;
                        }
                    }else{
                        $response['status'] = 'no otp found phone';
                    }
                }elseif($type == 'email'){
                    $sql = sprintf("select * from verifyotp where email = '%s'", $entity);
                    $st = $conn->execute($sql);
                    $response['action'] = 'verify otp email';
                    if($res = $st->fetch('assoc')){
                        if($res['otp'] == $otp){
                            $response['status'] = true;
                        }else{
                            $response['status'] = false;
                        }
                    }else{
                        $response['status'] = 'no otp found email';
                    }
                }else{
                    $response['status']= 'somethng went wrong otp verify';
                }
            }
            return $response;
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }

    }


    //adding dao 10 rhythm
    public function saveEducationDetails($input, $case)
    {
        Log::debug("Started ...saveEducationDetails Dao : userid, companyid, otp  : ".$case);

        try{
            
            $conn = ConnectionManager::get('default');
            $nu = new NotificationUtils();
            $response = [
                'msg' => 'hello'
            ];
            extract($input);
            if($case == 0){
                $sql  = sprintf("insert into user_education_info set userid = %s , type = '%s' , fieldofstudy = '%s' , course = '%s' , skills = '%s' , yearofstart = %s , venue = %s", $userid , $type , $fieldofstudy , $course ,$skills , $yearofstart , $college);
                Log::debug("SQL : ".$sql);

                if($conn->execute($sql)){
                    return ['msg' => 'success' , 'status'=> true];
                }

            }else{
                $sql  = sprintf("insert into user_education_info set userid = %s , type = '%s' , fieldofstudy = '%s' , course = '%s' , skills = '%s' , yearofpassing = %s , percentage = %s , venue = %s", $userid , $type , $fieldofstudy , $course ,$skills , $yearofpassing , $percentage, $college);
                Log::debug("SQL : ".$sql);

                if($conn->execute($sql)){
                    return ['msg' => 'success' , 'status'=> true];
                }
            }
            return $response;
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }

    }

    public function org_follow_unfollow($organizationId , $userId , $type){
        Log::debug("Started ...saveEducationDetails Dao : userid, companyid, otp  : ");

        try{
            
            $conn = ConnectionManager::get('default');
            //extract($input);
            if($type != "follow" && $type != "unfollow"){return ["message" => "enter valid message type"];}
            if($type == "follow"){
                $sql = sprintf("SELECT * from users_organization where organization_id = '%d' and user_id = '%d'",$organizationId , $userId);
                $f = $conn->execute($sql );
                if($a = $f->fetch('assoc')){return ['message'=>'already following'];}
                $sql = sprintf("INSERT INTO `users_organization` ( `user_id`, `organization_id`) VALUES ( '%d', '%d');", $userId,$organizationId );
                if($conn->execute($sql)){
                    return ["message" => "followed"];
                }else{return ["message" => "something went wrong"];}
            }else{
                $f = $conn->execute(sprintf("SELECT * from users_organization where organization_id = %s and user_id = %s",$organizationId , $userId) );
                if(!($a = $f->fetch('assoc'))){return ['message'=>'not following'];}
                if($conn->execute(sprintf("delete from users_organization where organization_id = %s and user_id = %s",$organizationId , $userId) )){
                    return ["message" => "unfollowed"];
                }else{return ["message" => "something went wrong"];}
            }   
        }catch(\Exception $e){
            Log::debug($e);
            throw new Exception($e);
        }
    }


}